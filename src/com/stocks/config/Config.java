package com.stocks.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Config {

	String fileName="config.prop";
	Properties properties;
	
	public final static String ENV="environment";
	public final static String STOCK_HIBERNATE_DEV_CONFIG="stock_hibernate_dev_config";
	public final static String STOCK_HIBERNATE_PROD_CONFIG="stock_hibernate_prod_config";
	
	public Config() throws IOException
	{
		properties=new Properties();
		FileInputStream fis=new FileInputStream(Config.class.getResource(fileName).getPath());
		properties.load(fis);		
	}
	
	public String getProperty(String key)
	{
		return properties.getProperty(key);
	}
}
