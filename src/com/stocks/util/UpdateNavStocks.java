package com.stocks.util;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.stocks.dao.NavS;
import com.stocks.dao.NavStocks;
import com.stocks.dao.Stocks;

public class UpdateNavStocks {
	
	//public static void main(String[] args)
	public void updateStocks()
	{
		// TODO Auto-generated method stub

		Session pythonSession=null;
		Session mySession=null;
		Transaction tx=null;
		int scCode=0;
		try
		{
			
			mySession=com.apidoc.util.HibernateBridge.getSessionFactory().openSession();
			
			pythonSession=HibernateBridge.getSessionFactory().openSession();			
			
			storeStocksOnly(mySession,pythonSession);

			storeNavsOnly(mySession,pythonSession);
			
		}		
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			pythonSession.close();
			mySession.close();
		}		
	}	

	static void storeStocksOnly(Session mySession,Session pythonSession)
	{
		Transaction tx=null;
		try
		{
			List<Integer> scCodeList=mySession.createQuery("select distinct scCode from Stocks").list();
			List<Object[]> stocksList=null;
			if(scCodeList!=null && scCodeList.size()>0)
			{
				//reason of doing max(tradingDate) in below query is to get the latest name for stock since there can be more than 1 scName for single scCode
				stocksList=pythonSession.createQuery("select scCode,scName,scGroup,scType,isinCode From NavStocks where (scCode,tradingDate) in (select scCode,max(tradingDate) from NavStocks where scCode not in (:scCodeList) group by scCode)").setParameterList("scCodeList",scCodeList).list();
			}
			else
			{
				//reason of doing max(tradingDate) in below query is to get the latest name for stock since there can be more than 1 scName for single scCode
				stocksList=pythonSession.createQuery("select scCode,scName,scGroup,scType,isinCode From NavStocks where (scCode,tradingDate) in (select scCode,max(tradingDate) from NavStocks group by scCode)").list();
			}
			if(stocksList!=null && stocksList.size()>0)
			{	
				int count=0;	
				int scCode;
				tx=mySession.beginTransaction();
				for(Object[] pythonStocks:stocksList)
				{			
					scCode=(Integer)pythonStocks[0];
					
					System.out.println("SC_CODE="+scCode);
					
					Stocks stocks=new Stocks();
					stocks.setScCode(scCode);
					stocks.setScName((String)pythonStocks[1]);
					stocks.setScGroup((String)pythonStocks[2]);
					stocks.setScType((String)pythonStocks[3]);
					stocks.setIsinCode((String)pythonStocks[4]);
					mySession.saveOrUpdate(stocks);
					count++;
				}
				tx.commit();
				System.out.println(count);
			}
			else
			{
				System.out.println("No new Stocks to store in stocks table.");
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();			
		}
	}
	
	static void storeNavsOnly(Session mySession,Session pythonSession)
	{
		Transaction tx=null;
		try
		{
			List<Integer> scCodeList=mySession.createQuery("select distinct scCode from Stocks order by scCode").list();
			List<Object[]> scCodeAndMaxDatePair=mySession.createQuery("select scCode,max(tradeDate) from NavS group by scCode order by scCode)").list();
			List<Integer> tempStocks=new LinkedList<Integer>();
			for(Object[] obj:scCodeAndMaxDatePair)
			{
				tempStocks.add((int)obj[0]);
				//System.out.println("Stocks in tempStocks="+(Integer)obj[0]);
			}			
			for(int i=0;i<scCodeList.size();i++)
			{
				int scCode=scCodeList.get(i);
				System.out.println("OHCL=  "+"sc_code="+scCode+" count="+i);
				Object[] obj=null;
				int scCode2=0,index=0;
				List<Object[]> navStocksList=null;
				if(tempStocks.contains(scCode))
				{
					for(int k=0;k<tempStocks.size();k++)
					{						
						if(scCode==tempStocks.get(k))
						{
							scCode2=tempStocks.get(k);
							index=k;
							tempStocks.remove(k);
							break;
						}
					}
				}
				if(scCode2!=0)
				{
					Object[] obj1=scCodeAndMaxDatePair.get(index);
					Date maxDate=(Date)obj1[1];
					//System.out.println("Existing stocks in Navs table="+scCode+"  maxDate="+new SimpleDateFormat("yyyy-MM-dd").format(maxDate));
					navStocksList=pythonSession.createQuery("select tradingDate,open,high,close,low,previousClose from NavStocks where scCode=? and tradingDate>? order by tradingDate").setParameter(0, scCode).setParameter(1,maxDate).list();
				}
				else
				{
					//System.out.println("NON Existing stock in Navs table="+scCode);
					navStocksList=pythonSession.createQuery("select tradingDate,open,high,close,low,previousClose from NavStocks where scCode=? order by tradingDate").setParameter(0, scCode).list();
				}
				if(navStocksList!=null)
				{
					tx=mySession.beginTransaction();
					for(Object[] object:navStocksList)
					{
						Date tradeDate=(Date)object[0];
						
						double open=(Double)object[1];
						double high=(Double)object[2];
						double close=(Double)object[3];
						double low=(Double)object[4];
						double previousClose=(Double)object[5];
					
						//System.out.println(tradeDate+" "+scCode+" "+open+" "+high+" "+close+" "+low);
						
						NavS navS=new NavS();
						navS.setTradeDate(tradeDate);
						navS.setScCode(scCode);
						navS.setOpen(open);
						navS.setHigh(high);
						navS.setClose(close);
						navS.setLow(low);
						navS.setPreviousClose(previousClose);
						mySession.saveOrUpdate(navS);
					}
					tx.commit();
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
	}
}
