package com.stocks.util;

import java.io.IOException;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.stocks.config.Config;

public class HibernateBridge 
{
	 private static volatile SessionFactory sessionFactory;

	    private HibernateBridge() {
	    }

	    public static SessionFactory getSessionFactory() {
	        if (sessionFactory == null) {
	            synchronized (HibernateBridge.class) {
	                if (sessionFactory == null) {
	                	try {
							Config config=new Config();
							if(config.getProperty(Config.ENV).equalsIgnoreCase("development"))
							{
								System.out.println("Stocks Development Environment");
								sessionFactory = (new Configuration().configure(config.getProperty(Config.STOCK_HIBERNATE_DEV_CONFIG))).buildSessionFactory();
							}
							else if(config.getProperty(Config.ENV).equalsIgnoreCase("production"))
							{
								System.out.println("Stocks Production Environment");
								sessionFactory = (new Configuration().configure(config.getProperty(Config.STOCK_HIBERNATE_PROD_CONFIG))).buildSessionFactory();
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	                	
	                   
	                }
	            }
	        }
	        return sessionFactory;
	    }
	    
	    public static void buildSessionFactory(){
	    	sessionFactory.close();
	    	sessionFactory=null;
	    	sessionFactory = (new Configuration().configure()).buildSessionFactory();
	    }
}
