package com.stocks.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Holidays;
import com.stocks.dao.EveryDayCandleStocks;
import com.stocks.dao.NavS;

public class EveryDayCandleStocksController {

	public void updateEveryDayCandle() throws Exception
	{
		Session hSession = null;
		String noAvailableData="";
		Transaction tx=null;
		try
		{
			
			// since oneWeekNavList contains nav for whole one week starting from monday we need to adjust firstDate(below) so that it represents the start date of that week
			// difference b/w startDate and firstDate is startDate can be any within the range of monday-friday but firstDate is the date of Monday
			hSession = HibernateBridge.getSessionFactory().openSession();
			Date lastDateInEveryDayCandle=null;
			//counts the difference between max(tradeDate)as m from Everydaycandle and min(tradeDate) > m from Nav
			
			List scCodeList = hSession.createQuery("select distinct scCode from Stocks order by scCode").setMaxResults(100).list();
			Iterator itr = scCodeList.iterator();
			while (itr.hasNext())
			{
				int scCode = (int) itr.next();
				
				lastDateInEveryDayCandle=(Date)hSession.createQuery("select max(tradeDate) from EveryDayCandleStocks where scCode=?").setParameter(0,scCode).setMaxResults(1).uniqueResult();
				System.out.println("Last date for scCode="+scCode+"  "+lastDateInEveryDayCandle);
				tx=hSession.beginTransaction();
				if(lastDateInEveryDayCandle!=null)
				{
					Date afterLastDateInEverDayCandleInNav=(Date)hSession.createQuery("select min(tradeDate) from NavS where scCode=? and tradeDate>?").setParameter(0, scCode).setParameter(1, lastDateInEveryDayCandle).setMaxResults(1).uniqueResult();
					if(afterLastDateInEverDayCandleInNav==null)
					{
						continue;
					}			
				}
				if(lastDateInEveryDayCandle==null)
				{
					System.out.println("No Date in EverydaycandleStocks so use min(tradeDate) from nav for scCode="+scCode);
					lastDateInEveryDayCandle=(Date)hSession.createQuery("select min(tradeDate) from NavS where scCode=?").setParameter(0,scCode).setMaxResults(1).uniqueResult();
				}
				if(lastDateInEveryDayCandle==null)
				{
					noAvailableData=noAvailableData+","+String.valueOf(scCode);
					continue;
				}
				List navListTemp = hSession.createQuery("from NavS where scCode=? and tradeDate>=? order by tradeDate asc").setParameter(0, scCode).setParameter(1, lastDateInEveryDayCandle).list();
				
				NavS nav1 = (NavS) navListTemp.get(0);
				Date tempFirstDate = nav1.getTradeDate();
				int decrementToGetMondayDateTemp = getIncrement(tempFirstDate) - 4;
				Calendar firstDateCalendarTemp = Calendar.getInstance();
				firstDateCalendarTemp.setTime(tempFirstDate);
				firstDateCalendarTemp.add(Calendar.DATE, decrementToGetMondayDateTemp);
				tempFirstDate = firstDateCalendarTemp.getTime();

				List navList = hSession.createQuery("from NavS where scCode=? and tradeDate>=? order by tradeDate asc").setParameter(0, scCode).setParameter(1, tempFirstDate).list();
				//lastDateToBeDeleted is that date which is weekly day after generating candles
				Date lastDateToBeDeleted=null;
				for (int i = 0; i < navList.size(); i++)
				{
					NavS navS = (NavS) navList.get(i);
					Date firstDate = navS.getTradeDate();
					int incrementToGetFridayDate = getIncrement(firstDate);

					Calendar fridayDateCalendar = Calendar.getInstance();
					fridayDateCalendar.setTime(firstDate);
					fridayDateCalendar.add(Calendar.DATE, incrementToGetFridayDate);
					Date fridayDate = fridayDateCalendar.getTime();

					List<NavS> oneWeekNavList = hSession.createQuery("from NavS where scCode=? and tradeDate>=? and tradeDate<=? order by tradeDate").setParameter(0, scCode).setParameter(1, firstDate).setParameter(2, fridayDate).list();
					Iterator itr2 = oneWeekNavList.iterator();
					
					i += oneWeekNavList.size() - 1;
					if(i == -1)
						break;
					//System.out.println("i=" + i);
					storeCandle(oneWeekNavList, hSession);
					if(i==navList.size()-1)
					{
						NavS nav2=(NavS)oneWeekNavList.get(oneWeekNavList.size()-1);
						lastDateToBeDeleted=nav2.getTradeDate();
						//System.out.println("Last date to be deleted="+lastDateToBeDeleted+"  mfId="+mfId);
						if(!new SimpleDateFormat("EEEEE").format(lastDateToBeDeleted).equalsIgnoreCase("Friday"))
						{
							//System.out.println("last date not equal to friday");
							Holidays holidays=(Holidays)hSession.createQuery("from Holidays where holiday=?").setParameter(0, fridayDate).setMaxResults(1).uniqueResult();
							if(holidays==null)
							{
								int deleteResult=hSession.createQuery("DELETE from EveryDayCandleStocks where scCode=? and tradeDate=? and candleType=0").setParameter(0, scCode).setParameter(1, lastDateToBeDeleted).executeUpdate();					
								//System.out.println("deleteResult="+deleteResult);
							}
						}
					}
				}			
				hSession.flush();
				tx.commit();
			}	
			if(noAvailableData.length()!=0)
			{
				throw new Exception("There is no extra nav's for "+noAvailableData);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	
	}
	
	int getIncrement(Date date)
	{
		//System.out.println("(Date)getIncrement=" + date);
		int i = 0;
		switch (new SimpleDateFormat("EEEEE").format(date))
		{
			// assuming database has no date for saturday or sunday
			case "Monday":
				i = 4;
				break;
			case "Tuesday":
				i = 3;
				break;
			case "Wednesday":
				i = 2;
				break;
			case "Thursday":
				i = 1;
				break;
			case "Friday":
				i = 0;
				break;
		}
		return i;
	}
	
	public int storeCandle(List listOfWeekNav,Session hSession) throws Exception
	{

		double dOpen=0, dHigh, dClose = -1, dLow;
		double wOpen=0, wHigh=0, wClose, wLow=0;
		if(listOfWeekNav.isEmpty())
		{
			return 0;
		}
		else
		{	
			try
			{
				EveryDayCandleStocks everyDayCandleStocks = null;
				//System.out.println("MFID\t" + "ope\t" + "clos\t" + "low\t" + "hig\t" + "TradeDate");
				Iterator itr = listOfWeekNav.iterator();
				NavS navS = null;
				Date currentDate=null;
				Date previousDate=null;				
				//Date nextDate=calendar.getTime();
				while (itr.hasNext())
				{					
					everyDayCandleStocks = new EveryDayCandleStocks();
					navS = (NavS) itr.next();
					currentDate=navS.getTradeDate();									
					dOpen = navS.getOpen();
					if(wOpen==0)
						wOpen=dOpen;
					dClose = navS.getClose();
					dHigh = navS.getHigh();
					if(dHigh>wHigh)
						wHigh=dHigh;
					dLow = navS.getLow();
					if(dLow<wLow || wLow==0)
						wLow=dLow;
					everyDayCandleStocks.setScCode(navS.getScCode());
					everyDayCandleStocks.setTradeDate(currentDate);
					everyDayCandleStocks.setCandleType(1);
					everyDayCandleStocks.setOpen(dOpen);
					everyDayCandleStocks.setHigh(dHigh);
					everyDayCandleStocks.setClose(dClose);;
					everyDayCandleStocks.setLow(dLow);
				//	System.out.println(nav.getMutualfundId() + "\t" + dOpen + "\t" + dClose + "\t" + dLow + "\t" + dHigh + "\t" + nav.getTradeDate() + "\t::Daily");
					hSession.saveOrUpdate(everyDayCandleStocks);
				}
				//currentDate will be the last date of week after completion of above loop similarly for dClose
				Date lastNavOfWeekDate=currentDate;
				wClose=dClose;
				everyDayCandleStocks = new EveryDayCandleStocks();

				//System.out.println(navS.getScCode() + "\t" + wOpen + "\t" + wClose + "\t" + wLow + "\t" + wHigh + "\t" + navS.getTradeDate() + "\t::WEEKLY");
				
				everyDayCandleStocks.setScCode(navS.getScCode());
				everyDayCandleStocks.setTradeDate(lastNavOfWeekDate);
				everyDayCandleStocks.setCandleType(0);
				everyDayCandleStocks.setOpen(wOpen);
				everyDayCandleStocks.setHigh(wHigh);
				everyDayCandleStocks.setClose(wClose);
				everyDayCandleStocks.setLow(wLow);

				hSession.saveOrUpdate(everyDayCandleStocks);
				return 1;
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new Exception(e.getMessage());
			}
		}
	
	}

	
}
