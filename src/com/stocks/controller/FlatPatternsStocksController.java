package com.stocks.controller;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.stocks.dao.FlatPatternsStocks;
import com.stocks.dao.FractalStocks;
import com.stocks.dao.IchimokuStocks;

public class FlatPatternsStocksController {

	public void createLatestStocksSSBFlatPatterns(String flatdays,String cType) throws Exception
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			int flatDays=Integer.parseInt(flatdays);
			int candleType=Integer.parseInt(cType);
			if(candleType!=1 && candleType!=0)
			{
				throw new Exception("Invalid candleType");
			}
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Integer> scCodeList=hSession.createQuery("select distinct scCode from IchimokuStocks").list();
			Iterator itr=scCodeList.iterator();
			while(itr.hasNext())
			{
				tx=hSession.beginTransaction();
				int scCode=(Integer)itr.next();
				//System.out.println("Flat pattern SSB for scCode="+scCode);
				Date maxEndDate=(Date)hSession.createQuery("select max(patternEndDate) from FlatPatternsStocks where scCode=? and patternName like '%SSB%' and candleType=?").setParameter(0,scCode).setParameter(1, candleType).setMaxResults(1).uniqueResult();
				if(maxEndDate==null)
				{
					maxEndDate=(Date)hSession.createQuery("select min(tradeDate) from IchimokuStocks where scCode=? and candleType=?").setParameter(0,scCode).setParameter(1, candleType).setMaxResults(1).uniqueResult();
				}
				List<IchimokuStocks> ichimokuList=hSession.createQuery("from IchimokuStocks where scCode=? and candleType=? and senkouSpanB!=0 and tradeDate>? order by tradeDate").setParameter(0, scCode).setParameter(1,candleType).setParameter(2, maxEndDate).list();
				
				for(int i=0;i<ichimokuList.size();i++)
				{
					IchimokuStocks ichimoku=ichimokuList.get(i);
					double senkouSpanB=ichimoku.getSenkouSpanB();
					Date maxTradeDate=null;
					int count=1;
					int j=i+1;
					if(j>ichimokuList.size()-1)
						break;
					IchimokuStocks nextIchimoku=ichimokuList.get(j);
					double nextSenkouSpanB=nextIchimoku.getSenkouSpanB();
					
					while(senkouSpanB==nextSenkouSpanB)
					{
						count++;
						j++;
						if(j>ichimokuList.size()-1)
							break;
						i=j-1;
						
						//System.out.println("i="+i+"\tj="+j+"\tichimokuSize="+ichimokuList.size());
						nextIchimoku=ichimokuList.get(j);
						//System.out.println(ichimoku.getTradeDate()+"\t"+nextIchimoku.getTradeDate()+"\t"+nextIchimoku.getSenkouSpanB());
						nextSenkouSpanB=nextIchimoku.getSenkouSpanB();
						
					}
					if(count>=flatDays)
					{
						IchimokuStocks lastIchimoku=ichimokuList.get(i);
						maxTradeDate=lastIchimoku.getTradeDate();
						System.out.println("SSB  "+candleType+"\tscCode="+scCode+"\tFlat found startDate="+ichimoku.getTradeDate()+"  endDate="+maxTradeDate+"  count="+count);
						FlatPatternsStocks flatPatterns=new FlatPatternsStocks();
						flatPatterns.setScCode(scCode);
						flatPatterns.setCandleType(candleType);
						flatPatterns.setPatternName("SSB");
						flatPatterns.setPatternValue(ichimoku.getSenkouSpanB());
						flatPatterns.setFlatDays(flatDays);
						flatPatterns.setPatternStartDate(ichimoku.getTradeDate());
						flatPatterns.setPatternEndDate(maxTradeDate);
						flatPatterns.setNumberOfDays(count);
						hSession.saveOrUpdate(flatPatterns);
						
					}
					
				}
				tx.commit();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}
	
	//remember to use period(column if fractal) parameter  in query since we generate fractal for period=1,2 but we use tcfl(1 and 3) patterns for period=2(or generate tcfl1,3 only for period 2)
	public void createLatestStocksTCFL1FlatPatterns(String flatdays,String cType)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			int flatDays=Integer.parseInt(flatdays);
			int candleType=Integer.parseInt(cType);
			if(candleType!=1 && candleType!=0)
			{
				throw new Exception("Invalid candleType");
			}
			
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Integer> scCodeList=hSession.createQuery("select distinct scCode from FractalStocks").list();
			Iterator itr=scCodeList.iterator();
			while(itr.hasNext())
			{
				tx=hSession.beginTransaction();
				int scCode=(Integer)itr.next();
				Date maxEndDate=(Date)hSession.createQuery("select max(patternEndDate) from FlatPatternsStocks where scCode=? and patternName like '%TCFL1%' and candleType=?").setParameter(0,scCode).setParameter(1, candleType).setMaxResults(1).uniqueResult();
				if(maxEndDate==null)
				{
					maxEndDate=(Date)hSession.createQuery("select min(tradeDate) from FractalStocks where scCode=? and candleType=? and period=2").setParameter(0, scCode).setParameter(1, candleType).setMaxResults(1).uniqueResult();
				}
				List<FractalStocks> fractalList=hSession.createQuery("from FractalStocks where scCode=? and candleType=? and tcfl1!=0 and period=? and tradeDate>? order by tradeDate").setParameter(0, scCode).setParameter(1,candleType).setParameter(2,2).setParameter(3, maxEndDate).list();
				
				for(int i=0;i<fractalList.size();i++)
				{
					FractalStocks fractal=fractalList.get(i);
					double tcfl1=fractal.getTcfl1();
					Date maxTradeDate=null;
					int count=1;
					int j=i+1;
					if(j>fractalList.size()-1)
						break;
					FractalStocks nextFractal=fractalList.get(j);
					double nextTcfl1=nextFractal.getTcfl1();
					
					while(tcfl1==nextTcfl1)
					{
						count++;
						j++;
						if(j>fractalList.size()-1)
							break;
						i=j-1;
						
						//System.out.println("i="+i+"\tj="+j+"\tfractalSize="+fractalList.size());
						nextFractal=fractalList.get(j);
						nextTcfl1=nextFractal.getTcfl1();
						
					}
					if(count>=flatDays)
					{
						FractalStocks lastFractal=fractalList.get(i);
						maxTradeDate=lastFractal.getTradeDate();
						System.out.println("TCFL1 "+candleType+"\tscCode="+scCode+"\tFlat found startDate="+fractal.getTradeDate()+"  endDate="+maxTradeDate+"  count="+count);
						FlatPatternsStocks flatPatterns=new FlatPatternsStocks();
						flatPatterns.setScCode(scCode);
						flatPatterns.setCandleType(candleType);
						flatPatterns.setPatternName("TCFL1");
						flatPatterns.setPatternValue(fractal.getTcfl1());
						flatPatterns.setFlatDays(flatDays);
						flatPatterns.setPatternStartDate(fractal.getTradeDate());
						flatPatterns.setPatternEndDate(maxTradeDate);
						flatPatterns.setNumberOfDays(count);
						hSession.saveOrUpdate(flatPatterns);
						
					}
					
				}
				tx.commit();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}
	
	public void createLatestStocksTCFL3FlatPatterns(String flatdays,String cType)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			int flatDays=Integer.parseInt(flatdays);
			int candleType=Integer.parseInt(cType);
			if(candleType!=1 && candleType!=0)
			{
				throw new Exception("Invalid candleType");
			}
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Integer> scCodeList=hSession.createQuery("select distinct scCode from EveryDayCandleStocks").list();
			Iterator itr=scCodeList.iterator();
			while(itr.hasNext())
			{
				tx=hSession.beginTransaction();
				int scCode=(Integer)itr.next();
				//System.out.println("Flat pattern TCFL3 for scCode="+scCode);
				Date maxEndDate=(Date)hSession.createQuery("select max(patternEndDate) from FlatPatternsStocks where scCode=? and patternName like '%TCFL3%' and candleType=?").setParameter(0,scCode).setParameter(1, candleType).setMaxResults(1).uniqueResult();
				if(maxEndDate==null)
				{
					maxEndDate=(Date)hSession.createQuery("select min(tradeDate) from FractalStocks where scCode=? and candleType=?").setParameter(0, scCode).setParameter(1, candleType).setMaxResults(1).uniqueResult();
				}
				List<FractalStocks> fractalList=hSession.createQuery("from FractalStocks where scCode=? and candleType=? and tcfl3!=0 and period=? and tradeDate>? order by tradeDate").setParameter(0, scCode).setParameter(1,candleType).setParameter(2,2).setParameter(3,maxEndDate).list();
				
				for(int i=0;i<fractalList.size();i++)
				{
					FractalStocks fractal=fractalList.get(i);
					double tcfl3=fractal.getTcfl3();
					Date maxTradeDate=null;
					int count=1;
					int j=i+1;
					if(j>fractalList.size()-1)
						break;
					FractalStocks nextFractal=fractalList.get(j);
					double nextTcfl3=nextFractal.getTcfl3();
					
					while(tcfl3==nextTcfl3)
					{
						count++;
						j++;
						if(j>fractalList.size()-1)
							break;
						i=j-1;
						
						//System.out.println("i="+i+"\tj="+j+"\tfractalSize="+fractalList.size());
						nextFractal=fractalList.get(j);
						nextTcfl3=nextFractal.getTcfl3();
						
					}
					if(count>=flatDays)
					{
						FractalStocks lastFractal=fractalList.get(i);
						maxTradeDate=lastFractal.getTradeDate();
						System.out.println("TCFL3 "+candleType+"\tscCode"+scCode+"\tFlat found startDate="+fractal.getTradeDate()+"  endDate="+maxTradeDate+"  count="+count);
						FlatPatternsStocks flatPatterns=new FlatPatternsStocks();
						flatPatterns.setScCode(scCode);
						flatPatterns.setCandleType(candleType);
						flatPatterns.setPatternName("TCFL3");
						flatPatterns.setPatternValue(fractal.getTcfl3());
						flatPatterns.setFlatDays(flatDays);
						flatPatterns.setPatternStartDate(fractal.getTradeDate());
						flatPatterns.setPatternEndDate(maxTradeDate);
						flatPatterns.setNumberOfDays(count);
						hSession.saveOrUpdate(flatPatterns);
					}
				}
				tx.commit();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}


}
