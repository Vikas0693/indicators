package com.stocks.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.apidoc.util.ImageLinks;
import com.stocks.dao.EveryDayCandleStocks;

public class TaLibStocksController {

	public void updateDailyEveryDayStocksAfterDate() throws Exception
	{
		//call updateEveryDayCandleAfterDate() of EveryDayCandleController and then update the talibIndicators
		Session hSession = null;
		Transaction tx = null;
		String missedScCode = "";
		Date lastUpdatedDate=null;
		try
		{			
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List scCodeList = hSession.createQuery("select distinct(scCode) from EveryDayCandleStocks where candleType=? and scCode=500400").setParameter(0, 1).list();
			Iterator itr = scCodeList.iterator();
			while (itr.hasNext())
			{
				int scCode = (int) itr.next();							
				
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from EveryDayCandleStocks where candleType=1 and scCode=? and ema200!=0 and rsi!=0 and macd!=0 and upperBand!=0 and adx!=0 and psar!=0").setParameter(0, scCode).setMaxResults(1).uniqueResult();
				if(lastUpdatedDate==null)
				{
					System.out.print("last updatedDate==null so will take min date from Everydaycandle");
					List days36NextToMinTradeDate=hSession.createQuery("select tradeDate from EveryDayCandleStocks where candleType=1 and scCode=? order by tradeDate").setParameter(0, scCode).setMaxResults(37).list();
					//the value 37 denotes that atlest 37 past values are needed to calculate ema's , upperband , macd etc..
					if(days36NextToMinTradeDate.size()<37)
						continue;
					lastUpdatedDate=(Date)days36NextToMinTradeDate.get(36);
				}
				System.out.println("scCode="+scCode+" lastUpdatedDate="+lastUpdatedDate+" Daily TAlibStocks");
				List everyDayOfMfList = hSession.createQuery("from EveryDayCandleStocks where scCode=? and tradeDate>=? and candleType=? order by tradeDate").setParameter(0, scCode).setParameter(1, lastUpdatedDate).setParameter(2, 1).list();
				Iterator itr1 = everyDayOfMfList.iterator();
				
				while (itr1.hasNext())
				{
					tx = hSession.beginTransaction();
					double atr = 0, ema20 = 0, ema50 = 0, ema200 = 0;
					double bands[] = null;
					float psar = 0, adx, macd, macdSignal, macdHistogram, rsi;
					EveryDayCandleStocks edc1 = (EveryDayCandleStocks) itr1.next();
					Date tradeDate = edc1.getTradeDate();
					//System.out.println(tradeDate);
					
					
					List date200DaysData = hSession.createQuery("select tradeDate from EveryDayCandleStocks where scCode=? and tradeDate<=? and candleType=? order by tradeDate desc").setParameter(0, scCode).setParameter(1, tradeDate).setParameter(2, 1).setMaxResults(300).list();
					Date fromDate = (Date) date200DaysData.get(date200DaysData.size() - 1);
					Date tillDate = (Date) date200DaysData.get(0);
					Calendar fromCalendar = Calendar.getInstance();
					fromCalendar.setTime(fromDate);
					Calendar tillCalendar = Calendar.getInstance();
					tillCalendar.setTime(tillDate);
					

					TaLibStocksEngine newTALibStocksEngine = new TaLibStocksEngine(scCode, fromCalendar, tillCalendar);
					
					psar = newTALibStocksEngine.calculatePsarCustom(scCode, fromCalendar, tillCalendar, .02, .2);
					edc1.setPsar(ImageLinks.getRoundedFloatValue(psar, 2));
					//System.out.println("PSAR done");
					bands = newTALibStocksEngine.calculateBBandsCustom(scCode, fromCalendar, tillCalendar, 20, 2.0, 2.0);
					if(bands != null)
					{
						edc1.setUpperBand(ImageLinks.getRoundedValue(bands[0], 2));
						edc1.setMiddleBand(ImageLinks.getRoundedValue(bands[1], 2));
						edc1.setLowerBand(ImageLinks.getRoundedValue(bands[2], 2));
					}
					//System.out.println("Boilinger Bands done");

					atr = newTALibStocksEngine.calculateAtr(scCode, fromCalendar, tillCalendar, 14);
					edc1.setAtr(atr);
					//System.out.println("ATR done");
					
					adx = newTALibStocksEngine.calculateADXCustom(scCode, fromCalendar, tillCalendar, 14);
					edc1.setAdx(adx);
					//System.out.println("ADX done");

					macd = newTALibStocksEngine.calculateMacdCustom(scCode, fromCalendar, tillCalendar, 12, 26, 9);
					edc1.setMacd(ImageLinks.getRoundedFloatValue(macd, 4));
					//System.out.println("MACD done");
					
					macdSignal = newTALibStocksEngine.calculateMacdSignalCustom(scCode, fromCalendar, tillCalendar, 12, 26, 9);
					edc1.setMacdSignal(ImageLinks.getRoundedFloatValue(macdSignal, 4));
					//System.out.println("MACD signal done");
					
					macdHistogram = newTALibStocksEngine.calculateMacdHistogramCustom(scCode, fromCalendar, tillCalendar, 12, 26, 9);
					edc1.setMacdHistogram(ImageLinks.getRoundedFloatValue(macdHistogram, 4));
					//System.out.println("MACD histogram done");
					
					rsi = newTALibStocksEngine.calculateRSICustom(scCode, fromCalendar, tillCalendar, 14);
					edc1.setRsi(ImageLinks.getRoundedFloatValue(rsi, 1));
					//System.out.println("RSI done");
					
					ema20 = newTALibStocksEngine.calculateEMACustom(scCode, fromCalendar, tillCalendar, 20);
					edc1.setEma20(ImageLinks.getRoundedValue(ema20, 2));				
					//System.out.println("EMA20 done");
					
					if(date200DaysData.size()>=50)
					{
						ema50 = newTALibStocksEngine.calculateEMACustom(scCode, fromCalendar, tillCalendar, 50);
						edc1.setEma50(ImageLinks.getRoundedValue(ema50, 2));
					}
					else
					{
						edc1.setEma50(0.0d);
					}
					//System.out.println("EMA50 done");
					
					if(date200DaysData.size()>=200)
					{
						ema200 = newTALibStocksEngine.calculateEMACustom(scCode, fromCalendar, tillCalendar, 200);
						edc1.setEma200(ImageLinks.getRoundedValue(ema200, 2));
					}
					else
					{
						edc1.setEma200(0.0d);
					}
					//System.out.println("EMA200 done");
					
					hSession.saveOrUpdate(edc1);
					tx.commit();
				}
				
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}
	
	public void updateWeeklyEveryDayAfterDate() throws Exception
	{
		// before this function call updateEveryDayCandleAfterDate() of EveryDayCandleController and then update the talibIndicators
		Session hSession = null;
		Transaction tx = null;
		String missedMfs = "";
		Date lastUpdatedDate=null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List mfList = hSession.createQuery("select distinct(scCode) from EveryDayCandleStocks where candleType=?").setParameter(0, 0).list();
			Iterator itr = mfList.iterator();
			while (itr.hasNext())
			{
				tx = hSession.beginTransaction();
				int scCode = (int) itr.next();
				
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from EveryDayCandleStocks where scCode=? and candleType=0 and ema20!=0 and macd!=0 and rsi!=0 and atr!=0").setParameter(0, scCode).setMaxResults(1).uniqueResult();
				if(lastUpdatedDate==null)
				{
					//reason of 37 in setMaxResults() is because in updateAllEveryDayCandle() we started the loop from 36th index i.e 37th day
					List days36NextToMinTradeDate=hSession.createQuery("select tradeDate from EveryDayCandleStocks where candleType=0 and scCode=? order by tradeDate").setParameter(0, scCode).setMaxResults(37).list();
					if(days36NextToMinTradeDate.size()<37)
						continue;
					lastUpdatedDate=(Date)days36NextToMinTradeDate.get(36);
				}
				System.out.println("scCode="+scCode+" lastUpdatedDate="+lastUpdatedDate+" Weekly");
				List everyDayOfScCodeList = hSession.createQuery("from EveryDayCandleStocks where scCode=? and tradeDate>=? and candleType=0 order by tradeDate").setParameter(0, scCode).setParameter(1, lastUpdatedDate).list();
				Iterator itr1 = everyDayOfScCodeList.iterator();
				while (itr1.hasNext())
				{
					double atr = 0, ema20 = 0, ema50 = 0, ema200 = 0;
					double bands[] = null;
					float psar = 0, adx, macd, macdSignal, macdHistogram, rsi;
					EveryDayCandleStocks edc1 = (EveryDayCandleStocks) itr1.next();
					Date tradeDate = edc1.getTradeDate();
					//System.out.println(tradeDate);
					List date200DaysData = hSession.createQuery("select tradeDate from EveryDayCandleStocks where scCode=? and tradeDate<=? and candleType=? order by tradeDate desc").setParameter(0, scCode).setParameter(1, tradeDate).setParameter(2, 0).setMaxResults(200).list();
					Date fromDate = (Date) date200DaysData.get(date200DaysData.size() - 1);
					Date tillDate = (Date) date200DaysData.get(0);
					Calendar fromCalendar = Calendar.getInstance();
					fromCalendar.setTime(fromDate);
					Calendar tillCalendar = Calendar.getInstance();
					tillCalendar.setTime(tillDate);					

					TaLibStocksEngine tALibStocksEngine = new TaLibStocksEngine(scCode, fromCalendar, tillCalendar);
					psar = tALibStocksEngine.calculateWeeklyPsarCustom(scCode, fromCalendar, tillCalendar, .02, .2);
					edc1.setPsar(ImageLinks.getRoundedFloatValue(psar, 2));
					
					bands = tALibStocksEngine.calculateWeeklyBBandsCustom(scCode, fromCalendar, tillCalendar, 20, 2.0, 2.0);
					if(bands != null)
					{
						edc1.setUpperBand(ImageLinks.getRoundedValue(bands[0], 2));
						edc1.setMiddleBand(ImageLinks.getRoundedValue(bands[1], 2));
						edc1.setLowerBand(ImageLinks.getRoundedValue(bands[2], 2));
					}

					atr = tALibStocksEngine.calculateWeeklyAtr(scCode, fromCalendar, tillCalendar, 14);
					edc1.setAtr(atr);

					// System.out.println("klo");
					adx = tALibStocksEngine.calculateWeeklyADXCustom(scCode, fromCalendar, tillCalendar, 14);
					edc1.setAdx(adx);
					// System.out.println(adx + " value=" + NewTALibEngine.calculateADXCustom(mfId, fromCalendar, tillCalendar, 14));

					macd = tALibStocksEngine.calculateWeeklyMacdCustom(scCode, fromCalendar, tillCalendar, 12, 26, 9);
					edc1.setMacd(ImageLinks.getRoundedFloatValue(macd, 4));

					macdSignal = tALibStocksEngine.calculateWeeklyMacdSignalCustom(scCode, fromCalendar, tillCalendar, 12, 26, 9);
					edc1.setMacdSignal(ImageLinks.getRoundedFloatValue(macdSignal, 4));

					macdHistogram = tALibStocksEngine.calculateWeeklyMacdHistogramCustom(scCode, fromCalendar, tillCalendar, 12, 26, 9);
					edc1.setMacdHistogram(ImageLinks.getRoundedFloatValue(macdHistogram, 4));

					rsi = tALibStocksEngine.calculateWeeklyRSICustom(scCode, fromCalendar, tillCalendar, 14);
					edc1.setRsi(ImageLinks.getRoundedFloatValue(rsi, 1));					

					if(date200DaysData.size()>=20)
					{
						ema20 = tALibStocksEngine.calculateWeeklyEMACustom(scCode, fromCalendar, tillCalendar, 20);
						edc1.setEma20(ImageLinks.getRoundedValue(ema20, 2));
					}
					else
					{
						edc1.setEma20(0.0d);
					}					
					if(date200DaysData.size()>=50)
					{
						ema50 = tALibStocksEngine.calculateWeeklyEMACustom(scCode, fromCalendar, tillCalendar, 50);
						edc1.setEma50(ImageLinks.getRoundedValue(ema50, 2));
					}
					else
					{
						edc1.setEma50(0.0d);
					}
					if(date200DaysData.size()==200)
					{
						ema200 = tALibStocksEngine.calculateWeeklyEMACustom(scCode, fromCalendar, tillCalendar, 200);
						edc1.setEma200(ImageLinks.getRoundedValue(ema200, 2));
					}
					else
					{
						edc1.setEma200(0.0d);
					}
					hSession.saveOrUpdate(edc1);
				}
				tx.commit();
			}		
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}			
		}
		finally
		{
			hSession.close();
		}
	}
}
