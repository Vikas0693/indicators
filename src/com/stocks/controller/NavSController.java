package com.stocks.controller;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;

public class NavSController {

	public int getWorkingDaysBetween(int scCode, Date fromDate, Date tillDate) 
	{
		Session hSession = null;
		try
		{
			//
			hSession = HibernateBridge.getSessionFactory().openSession();
			int nod = ((Long) hSession.createQuery("select count(*) from NavS where scCode=? and tradeDate>=? and tradeDate<?").setParameter(0, scCode).setParameter(1, fromDate).setParameter(2, tillDate).uniqueResult()).intValue();
			return nod;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return 0;
		}
		finally
		{
			hSession.close();
		}		
	}

	public int getWeeklyWorkingDaysBetween(int scCode, Date fromDate, Date tillDate) 
	{
		Session hSession = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			int nod = ((Long) hSession.createQuery("select count(*) from EveryDayCandleStocks where scCode=? and tradeDate>=? and tradeDate<=? and candleType=?").setParameter(0, scCode).setParameter(1, fromDate).setParameter(2, tillDate).setParameter(3, 0).uniqueResult()).intValue();
			return nod;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return 0;
		}
		finally
		{
			hSession.close();
		}
		
	}
	
	public double[] getDailyClose(int scCode, int nod, Date tillDate)
	{
		Session hSession = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List<Double> price = hSession.createQuery("select close from NavS where scCode=? and tradeDate<=? order by tradeDate desc").setParameter(0, scCode).setParameter(1, tillDate).setMaxResults(nod).list();
			Collections.reverse(price);
			double nav[] = price.stream().mapToDouble(i -> i).toArray();
			
			return nav;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public double[] getDailyHigh(int scCode, int nod, Date tillDate)
	{
		Session hSession = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List<Double> highNav = hSession.createQuery("select high from NavS where scCode=? and tradeDate<=? order by tradeDate desc").setParameter(0, scCode).setParameter(1, tillDate).setMaxResults(nod).list();
			Collections.reverse(highNav);
			double high[] = highNav.stream().mapToDouble(i -> i).toArray();
			
			return high;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}

	public double[] getDailyLow(int scCode, int nod, Date tillDate)
	{
		Session hSession = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List<Double> lowNav = hSession.createQuery("select low from NavS where scCode=? and tradeDate<=? order by tradeDate desc").setParameter(0, scCode).setParameter(1, tillDate).setMaxResults(nod).list();
			Collections.reverse(lowNav);
			double low[] = lowNav.stream().mapToDouble(i -> i).toArray();
			
			return low;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}

	public double[] getWeeklyClose(int scCode, int nod, Date tillDate)
	{
		Session hSession = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List<Double> price = hSession.createQuery("select close from EveryDayCandleStocks where scCode=? and tradeDate<=? and candleType=? order by tradeDate desc").setParameter(0, scCode).setParameter(1, tillDate).setParameter(2,0).setMaxResults(nod).list();
			Collections.reverse(price);
			double nav[] = price.stream().mapToDouble(i -> i).toArray();
			
			return nav;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public double[] getWeeklyHigh(int scCode, int nod, Date tillDate)
	{
		Session hSession = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List<Double> highNav = hSession.createQuery("select high from EveryDayCandleStocks where scCode=? and tradeDate<=? and candleType=? order by tradeDate desc").setParameter(0, scCode).setParameter(1, tillDate).setParameter(2, 0).setMaxResults(nod).list();
			Collections.reverse(highNav);
			double high[] = highNav.stream().mapToDouble(i -> i).toArray();
			
			return high;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}

	public double[] getWeeklyLow(int scCode, int nod, Date tillDate)
	{
		Session hSession = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List<Double> lowNav = hSession.createQuery("select low from EveryDayCandleStocks where scCode=? and tradeDate<=? and candleType=? order by tradeDate desc").setParameter(0, scCode).setParameter(1, tillDate).setParameter(2, 0).setMaxResults(nod).list();
			Collections.reverse(lowNav);
			double low[] = lowNav.stream().mapToDouble(i -> i).toArray();
			
			return low;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}

	
}
