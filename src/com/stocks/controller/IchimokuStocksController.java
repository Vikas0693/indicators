package com.stocks.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.apidoc.util.ImageLinks;
import com.stocks.dao.EveryDayCandleStocks;
import com.stocks.dao.IchimokuStocks;

public class IchimokuStocksController {

	public void updateDailyIchimokuStocksAfterDate() throws Exception
	{

		Session hSession = null;
		Transaction tx = null;
		Date lastUpdatedDate=null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();		

			List scCodeList = hSession.createQuery("select distinct(scCode) from EveryDayCandleStocks").list();
			Iterator itr = scCodeList.iterator();
			String missedScCode = "";
			while (itr.hasNext())
			{
				int scCode = (int) itr.next();
				
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from IchimokuStocks where scCode=? and candleType=1 and tenkanSen!=0 and kijunSen!=0").setParameter(0, scCode).setMaxResults(1).uniqueResult();
				if(lastUpdatedDate==null)
				{
					lastUpdatedDate=(Date)hSession.createQuery("select min(tradeDate) from EveryDayCandleStocks where scCode=? and candleType=1").setParameter(0, scCode).setMaxResults(1).uniqueResult();
				}
				System.out.print("inserting daily ohlc into ichimoku Stocks for scCode="+scCode+" lstUpdatedDate="+lastUpdatedDate);
				List everyDayFromStartDate = hSession.createQuery("from EveryDayCandleStocks where scCode=? and tradeDate>=? and candleType=? order by tradeDate asc").setParameter(0, scCode).setParameter(1, lastUpdatedDate).setParameter(2, 1).list();
				// update ichimokuFromStartDate with closePrice
				Iterator itr1 = everyDayFromStartDate.iterator();
				try
				{
					tx = hSession.beginTransaction();
					while (itr1.hasNext())
					{
						EveryDayCandleStocks edc = (EveryDayCandleStocks) itr1.next();
						IchimokuStocks ichimoku = new IchimokuStocks();
						ichimoku.setOpen(edc.getOpen());
						ichimoku.setHigh(edc.getHigh());
						ichimoku.setClose(edc.getClose());
						ichimoku.setLow(edc.getLow());
						ichimoku.setScCode(edc.getScCode());
						ichimoku.setTradeDate(edc.getTradeDate());
						ichimoku.setCandleType(1);
						hSession.saveOrUpdate(ichimoku);
					}
					tx.commit();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					if(tx != null)
					{
						tx.rollback();
					}
					throw new Exception("Not able to persist ichimoku close price from start Date");
				}
				System.out.println("updating daily ichimoku for scCode="+scCode);
				List ichimokuFromStartDate = hSession.createQuery("from IchimokuStocks where scCode=? and tradeDate>=? and candleType=? order by tradeDate asc").setParameter(0, scCode).setParameter(1, lastUpdatedDate).setParameter(2, 1).list();
				tx = hSession.beginTransaction();
				// update tenkanSen
				Iterator itr3 = ichimokuFromStartDate.iterator();
				while (itr3.hasNext())
				{
					IchimokuStocks ichimoku = (IchimokuStocks) itr3.next();
					Date ichimokuDate = ichimoku.getTradeDate();
					List last9DaysMaxList = hSession.createQuery("select high from IchimokuStocks where tradeDate<=? and scCode=? and candleType=? order by tradeDate desc").setParameter(0, ichimokuDate).setParameter(1, scCode).setParameter(2, 1).setMaxResults(9).list();
					List last9DaysMinList = hSession.createQuery("select low from IchimokuStocks where tradeDate<=? and scCode=? and candleType=? order by tradeDate desc").setParameter(0, ichimokuDate).setParameter(1, scCode).setParameter(2, 1).setMaxResults(9).list();
					
					double maxFrom9Days = (double) Collections.max(last9DaysMaxList);
					double minFrom9Days = (double) Collections.min(last9DaysMinList);
					double tenkanSen = (maxFrom9Days + minFrom9Days) / 2.0;
					tenkanSen = ImageLinks.getRoundedValue(tenkanSen, 5);
					ichimoku.setTenkanSen(tenkanSen);

					List<IchimokuStocks> last26DaysIchimokuList = hSession.createQuery("from IchimokuStocks where tradeDate<=? and scCode=? and candleType=? order by tradeDate desc").setParameter(0, ichimokuDate).setParameter(1, scCode).setParameter(2, 1).setMaxResults(26).list();
					List<Double> last26DaysMaxList = new ArrayList<Double>();
					List<Double> last26DaysMinList = new ArrayList<Double>();
					Iterator itr2 = last26DaysIchimokuList.iterator();
					while (itr2.hasNext())
					{
						IchimokuStocks ichimokuStocks = (IchimokuStocks) itr2.next();
						double high = ichimokuStocks.getHigh();
						double low = ichimokuStocks.getLow();
						last26DaysMaxList.add(high);
						last26DaysMinList.add(low);
					}
					double maxFromPrevious26Days = (double)Collections.max(last26DaysMaxList);
					double minFromPrevious26Days = (double)Collections.min(last26DaysMinList);
					double kijunSen = (maxFromPrevious26Days + minFromPrevious26Days) / 2.0;
					kijunSen = ImageLinks.getRoundedValue(kijunSen, 5);
					ichimoku.setKijunSen(kijunSen);
					double kijunSen26DaysBefore=0;
					double tenkanSen26DaysBefore=0;
					if(last26DaysIchimokuList.size()==26)
					{
						//System.out.println("Date="+ichimokuDate+" last26DaysIchimokuList.size()="+last26DaysIchimokuList.size());
						kijunSen26DaysBefore = last26DaysIchimokuList.get(25).getKijunSen();
						tenkanSen26DaysBefore = last26DaysIchimokuList.get(25).getTenkanSen();
					}
					else
					{
						//System.out.println("Not 26 days"+"Date="+ichimokuDate+" last26DaysIchimokuList.size()="+last26DaysIchimokuList.size());
						kijunSen26DaysBefore=0;
						tenkanSen26DaysBefore=0;
					}
					double senkouSpanA = (tenkanSen26DaysBefore + kijunSen26DaysBefore) / 2.0;
					senkouSpanA = ImageLinks.getRoundedValue(senkouSpanA, 5);
					ichimoku.setSenkouSpanA(senkouSpanA);

					// since ssb is avg of previous 52 days and the value is placed after 26 days we get previous 52+26=78 but since that list should also include current date we get 25+52=77 days from ichimoku
					List<Double> last78DaysMaxList = hSession.createQuery("select high from IchimokuStocks where scCode=? and tradeDate <= ? and candleType=? order by tradeDate desc").setParameter(0, scCode).setParameter(1, ichimokuDate).setParameter(2, 1).setMaxResults(77).list();
					List<Double> last78DaysMinList = hSession.createQuery("select low from IchimokuStocks where scCode=? and tradeDate <= ? and candleType=? order by tradeDate desc").setParameter(0, scCode).setParameter(1, ichimokuDate).setParameter(2, 1).setMaxResults(77).list();
					// since the last78DaysList is in descending order we use sublist from 26 to 77
					double senkouSpanB=0;
					if(last78DaysMaxList!=null && last78DaysMinList.size()==77)
					{
						List<Double> first52Of78DaysMaxList = last78DaysMaxList.subList(25, 77);
						List<Double> first52Of78DaysMinList = last78DaysMinList.subList(25, 77);
						double maxFrom52Days = Collections.max(first52Of78DaysMaxList);
						double minFrom52Days = Collections.min(first52Of78DaysMinList);
						senkouSpanB = (maxFrom52Days + minFrom52Days) / 2.0;
						senkouSpanB = ImageLinks.getRoundedValue(senkouSpanB, 5);
						ichimoku.setSenkouSpanB(senkouSpanB);
					}
					else
					{
						ichimoku.setSenkouSpanB(senkouSpanB);
					}
					double kumoTop = senkouSpanA > senkouSpanB ? senkouSpanA : senkouSpanB;
					double kumoBottom = senkouSpanA < senkouSpanB ? senkouSpanA : senkouSpanB;
					ichimoku.setKumoTop(kumoTop);
					ichimoku.setKumoBottom(kumoBottom);
					hSession.saveOrUpdate(ichimoku);
					//System.out.println("ten="+tenkanSen+" kijun="+kijunSen+" ssA="+senkouSpanA+" ssB="+senkouSpanB+" kumoTop="+kumoTop+" kumoBottom="+kumoBottom);
				}
				tx.commit();
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	
	}
	
	public void updateWeeklyIchimokuStocksAfterDate() throws Exception
	{

		System.out.println("updating Weekly Ichimoku Stocks");
		Session hSession = null;
		Transaction tx = null;
		Date lastUpdatedDate=null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			List scCodeList = hSession.createQuery("select distinct(scCode) from EveryDayCandleStocks").list();
			Iterator itr = scCodeList.iterator();
			String missedMutualFundId = "";
			while (itr.hasNext())
			{
				int scCode = (int) itr.next();
				System.out.print("inserting weekly ohlc into ichimoku for scCode="+scCode);
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from IchimokuStocks where scCode=? and candleType=0 and tenkanSen!=0 and kijunSen!=0").setParameter(0, scCode).setMaxResults(1).uniqueResult();
				if(lastUpdatedDate==null)
				{
					lastUpdatedDate=(Date)hSession.createQuery("select min(tradeDate) from EveryDayCandleStocks where scCode=? and candleType=0").setParameter(0, scCode).setMaxResults(1).uniqueResult();
				}

				List everyDayFromStartDate = hSession.createQuery("from EveryDayCandleStocks where scCode=? and tradeDate>=? and candleType=? order by tradeDate asc").setParameter(0, scCode).setParameter(1, lastUpdatedDate).setParameter(2, 0).list();

				// update ichimokuFromStartDate with closePrice
				Iterator itr1 = everyDayFromStartDate.iterator();
				try
				{
					tx = hSession.beginTransaction();
					while (itr1.hasNext())
					{
						EveryDayCandleStocks edc = (EveryDayCandleStocks) itr1.next();
						IchimokuStocks ichimoku = new IchimokuStocks();
						ichimoku.setOpen(edc.getOpen());
						ichimoku.setHigh(edc.getHigh());
						ichimoku.setClose(edc.getClose());
						ichimoku.setLow(edc.getLow());
						ichimoku.setScCode(edc.getScCode());
						ichimoku.setTradeDate(edc.getTradeDate());
						ichimoku.setCandleType(0);
						hSession.saveOrUpdate(ichimoku);
					}
					tx.commit();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					if(tx != null)
					{
						tx.rollback();
					}
					throw new Exception("Not able to persist ichimoku Stocks close price from start Date for weekly");
				}
				System.out.println("updating weekly ichimoku for scCode="+scCode);
				List ichimokuFromStartDate = hSession.createQuery("from IchimokuStocks where scCode=? and tradeDate>=? and candleType=0 order by tradeDate asc").setParameter(0, scCode).setParameter(1, lastUpdatedDate).list();
				
				
				// update tenkanSen
				Iterator itr3 = ichimokuFromStartDate.iterator();
				while (itr3.hasNext())
				{
					tx = hSession.beginTransaction();
					IchimokuStocks ichimoku = (IchimokuStocks) itr3.next();
					Date ichimokuDate = ichimoku.getTradeDate();
					// System.out.println(ichimokuDate);
					List last9HighDaysList = hSession.createQuery("select high from IchimokuStocks where tradeDate<=? and scCode=? and candleType=? order by tradeDate desc").setParameter(0, ichimokuDate).setParameter(1, scCode).setParameter(2, 0).setMaxResults(9).list();
					List last9LowDaysList = hSession.createQuery("select low from IchimokuStocks where tradeDate<=? and scCode=? and candleType=? order by tradeDate desc").setParameter(0, ichimokuDate).setParameter(1, scCode).setParameter(2, 0).setMaxResults(9).list();
					double maxFrom9HighDays = (Double) Collections.max(last9HighDaysList);
					double minFrom9LowDays = (Double) Collections.min(last9LowDaysList);
					double tenkanSen = (maxFrom9HighDays + minFrom9LowDays) / 2.0;
					tenkanSen = ImageLinks.getRoundedValue(tenkanSen, 5);
					ichimoku.setTenkanSen(tenkanSen);

					List<IchimokuStocks> last26DaysIchimokuList = hSession.createQuery("from IchimokuStocks where tradeDate<=? and scCode=? and candleType=? order by tradeDate desc").setParameter(0, ichimokuDate).setParameter(1, scCode).setParameter(2, 0).setMaxResults(26).list();
					List<Double> last26HighDaysList = new ArrayList<Double>();
					List<Double> last26LowDaysList = new ArrayList<Double>();
					Iterator itr2 = last26DaysIchimokuList.iterator();
					while (itr2.hasNext())
					{
						IchimokuStocks ichimokuForHighLow = (IchimokuStocks) itr2.next();
						double high = ichimokuForHighLow.getHigh();
						double low = ichimokuForHighLow.getLow();
						last26HighDaysList.add(high);
						last26LowDaysList.add(low);
					}
					double maxFromPrevious26Days = Collections.max(last26HighDaysList);
					double minFromPrevious26Days = Collections.min(last26LowDaysList);
					double kijunSen = (maxFromPrevious26Days + minFromPrevious26Days) / 2.0;
					kijunSen = ImageLinks.getRoundedValue(kijunSen, 5);
					ichimoku.setKijunSen(kijunSen);

					double tenkanSen26DaysBefore=0;
					double kijunSen26DaysBefore=0;
					if(last26DaysIchimokuList.size()==26)
					{
						tenkanSen26DaysBefore = last26DaysIchimokuList.get(25).getTenkanSen();
						kijunSen26DaysBefore = last26DaysIchimokuList.get(25).getKijunSen();
					}
					else
					{
						tenkanSen26DaysBefore=0;
						kijunSen26DaysBefore=0;
					}
					double senkouSpanA = (tenkanSen26DaysBefore + kijunSen26DaysBefore) / 2.0;
					senkouSpanA = ImageLinks.getRoundedValue(senkouSpanA, 5);
					ichimoku.setSenkouSpanA(senkouSpanA);

					// since ssb is avg of previous 52 days and the value is placed after 26 days
					List<Double> last78HighDaysList = hSession.createQuery("select high from IchimokuStocks where scCode=? and tradeDate <= ? and candleType=? order by tradeDate desc").setParameter(0, scCode).setParameter(1, ichimokuDate).setParameter(2, 0).setMaxResults(77).list();
					List<Double> last78LowDaysList = hSession.createQuery("select low from IchimokuStocks where scCode=? and tradeDate <= ? and candleType=? order by tradeDate desc").setParameter(0, scCode).setParameter(1, ichimokuDate).setParameter(2, 0).setMaxResults(77).list();
					// since the last78DaysList is in descending order we use sublist from 25(inclusive) to 77(exclusive)
					double senkouSpanB=0;
					if(last78HighDaysList!=null && last78HighDaysList.size()==77)
					{
						List<Double> first52Of78HighDaysList = last78HighDaysList.subList(25, 77);
						List<Double> first52Of78LowDaysList = last78LowDaysList.subList(25, 77);
						double maxFrom52Days = Collections.max(first52Of78HighDaysList);
						double minFrom52Days = Collections.min(first52Of78LowDaysList);
						senkouSpanB = (maxFrom52Days + minFrom52Days) / 2.0;
						senkouSpanB = ImageLinks.getRoundedValue(senkouSpanB, 5);
						ichimoku.setSenkouSpanB(senkouSpanB);
					}
					else
					{
						ichimoku.setSenkouSpanB(senkouSpanB);
					}
					double kumoTop = senkouSpanA > senkouSpanB ? senkouSpanA : senkouSpanB;
					double kumoBottom = senkouSpanA < senkouSpanB ? senkouSpanA : senkouSpanB;
					ichimoku.setKumoTop(kumoTop);
					ichimoku.setKumoBottom(kumoBottom);
					hSession.saveOrUpdate(ichimoku);
					//System.out.println("ten="+tenkanSen+" kijun="+kijunSen+" ssA="+senkouSpanA+" ssB="+senkouSpanB+" kumoTop="+kumoTop+" kumoBottom="+kumoBottom);
					tx.commit();
				}
				
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}

	
	}

}
