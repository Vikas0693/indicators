package com.stocks.controller;

import java.util.Calendar;

import com.apidoc.util.ImageLinks;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MAType;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

public class TaLibStocksEngine {

	int dailyWorkingDays=0;
	int weeklyWorkingDays=0;
	public TaLibStocksEngine(int scCode, Calendar fromDate, Calendar tillDate)
	{
		
		NavSController navController = new NavSController();
		dailyWorkingDays=navController.getWorkingDaysBetween(scCode, fromDate.getTime(), tillDate.getTime());
		weeklyWorkingDays=navController.getWeeklyWorkingDaysBetween(scCode, fromDate.getTime(), tillDate.getTime());
	}
		
	public Float calculatePsarCustom(int scCode, Calendar fromDate, Calendar tillDate, double accelerationFactor, double maximumFactor)
	{
		
		try
		{
			NavSController navSController = new NavSController();
			

			Core core = new Core();
			int minDays = 14;
			int NOD = minDays + dailyWorkingDays;
			// System.out.println("NOD=" + NOD + "=" + minDays + "+" + navController.getWorkingDaysBetween(mutualFundId, fromDate.getTime(), tillDate.getTime()));
			// float inHigh[] = cashHandler.getCashDatafromDB(symbol, NOD, 1, tillDate); //
			// {1025.8,1031.8,1047,1055.75,1054.8,1072.25,1069.4,1074,1081.5,1068,1064.95,1068.7,1064.4,1087.85,1093.8,1101.9,1100.95,1100.9,1107.2,1124.9,1095.4,1084.9,1107,1117.5,1119,1120.5,1116.9,1111.8,1087.4,1067.9,1059};
			// float inLow[] = cashHandler.getCashDatafromDB(symbol, NOD, 2, tillDate);//
			// {1014.95,1009,1026.95,1032.5,1030.55,1053.1,1046.15,1054.7,1054.2,1035.6,1033.25,1039.25,1038,1049.2,1074.35,1086,1086.1,1074.6,1080.5,1089.25,1070.3,1062.65,1075,1100.3,1100.55,1097.25,1099.2,1076,1057,1038.2,1026.55};
			// float inClose[] = cashHandler.getCashDatafromDB(symbol, NOD, 3,tillDate);
			//{1017.85,1023.85,1044.95,1039.15,1049.65,1069.2,1055.6,1073.05,1059,1040.35,1047.45,1044.4,1049.45,1081.65,1081.8,1090.35,1098.85,1082,1096.25,1093.2,1074.85,1065.3,1105.05,1105.7,1111.85,1108.55,1102.2,1079.9,1061.85,1054.7,1030.5};

			
			double inHigh[]=navSController.getDailyHigh(scCode, NOD, tillDate.getTime());
			double inLow[]=navSController.getDailyLow(scCode, NOD, tillDate.getTime());
			
			int startIdx = 0;
			int total_days_data = inHigh.length;
			int endIdx = total_days_data - 1;


			MInteger outBegIdxA = new MInteger();
			MInteger outNbElementA = new MInteger();


			double outReal[] = new double[inHigh.length];
			/*double inAcc = 0.02;
			double inMax = .2;*/
			// RetCode code = core.sar(startIdx, endIdx, inHigh, inLow, accelerationFactor, maximumFactor, outBegIdxA, outNbElementA, outReal);
			RetCode code = core.sar(startIdx, endIdx, inHigh, inLow, accelerationFactor, maximumFactor, outBegIdxA, outNbElementA, outReal);
			if(code != RetCode.Success)
			{

				return null;
			}
			// System.out.println("outNbElementA=" + outNbElementA.value);
			// System.out.println("outNbElementA.value [" +outNbElementA.value+ "] for trade date [" +fromDate.getTime()+ "]");
			if(outNbElementA.value > 0)
			{
				return new Float(outReal[(outNbElementA.value - 1)]);
			}

			return null;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}

	}
	
	public double[] calculateBBandsCustom(int scCode, Calendar fromDate, Calendar tillDate, int optInTimePeriod, double upMultiplier, double downMultiplier)
	{
		NavSController navSController = new NavSController();

		Core core = new Core();
		int minNOD = optInTimePeriod;
		int NOD = minNOD + dailyWorkingDays;
		// System.out.println("NOD "+NOD);

		// float inReal[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);
		double close[] = navSController.getDailyClose(scCode, NOD, tillDate.getTime());

		int total_days_data = close.length;

		int startIdx1 = optInTimePeriod;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outUpper[] = new double[close.length];
		double outMiddle[] = new double[close.length];
		double outLower[] = new double[close.length];

		RetCode code1 = core.bbands(startIdx1, endIdx1, close, optInTimePeriod, upMultiplier, downMultiplier, MAType.Sma, outBegIdx1, outNbElement1, outUpper, outMiddle, outLower);

		if(outNbElement1.value > 0)
		{
			double bands[] = new double[3];
			
			bands[0] = ImageLinks.getRoundedValue(outUpper[outNbElement1.value - 1], 5);
			bands[1] = ImageLinks.getRoundedValue(outMiddle[outNbElement1.value - 1], 5);
			bands[2] = ImageLinks.getRoundedValue(outLower[outNbElement1.value - 1], 5);
			return bands;
			
		}
		return null;
	}
	
	public double calculateAtr(int scCode, Calendar fromDate, Calendar tillDate, int optInTimePeriod)
	{
		//System.out.println("update Atr");
		

		Core core = new Core();
		int minNOD = optInTimePeriod;
		// int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);
		int NOD = minNOD + 250; // trying to smoothen the values. Changed on 01/09/2015

		NavSController navSController = new NavSController();


		double inReal[] = navSController.getDailyClose(scCode, NOD, tillDate.getTime());
		double inHigh[] = navSController.getDailyHigh(scCode, NOD, tillDate.getTime());
		double inLow[] = navSController.getDailyLow(scCode, NOD,tillDate.getTime());
		int total_days_data = inReal.length;

		int startIdx1 = optInTimePeriod;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[inReal.length];

		RetCode code1 = core.atr(startIdx1, endIdx1, inHigh, inLow, inReal, optInTimePeriod, outBegIdx1, outNbElement1, outReal1);
		if(code1 != RetCode.Success)
		{
			// System.out.println("lol1");
			return -1;
		}
		if(outNbElement1.value > 0)
		{
			// System.out.println("NOD=" + NOD + "   atr=" + ImageLinks.getRoundedValue((outReal1[(outNbElement1.value - 1)]), 5));
			return ImageLinks.getRoundedValue((outReal1[(outNbElement1.value - 1)]), 5);
		}
		// System.out.println("lol2");
		return -1;
	}
	
	public Float calculateADXCustom(int scCode, Calendar fromDate, Calendar tillDate, int optInTimePeriod)
	{
		NavSController navSController = new NavSController();

		Core core = new Core();
		int minNOD = (2 * optInTimePeriod) - 1;
		// System.out.println("get Working Days");
		int NOD = minNOD + dailyWorkingDays;

		double inHigh[] = navSController.getDailyHigh(scCode, NOD, tillDate.getTime()); // {1025.8,1031.8,1047,1055.75,1054.8,1072.25,1069.4,1074,1081.5,1068,1064.95,1068.7,1064.4,1087.85,1093.8,1101.9,1100.95,1100.9,1107.2,1124.9,1095.4,1084.9,1107,1117.5,1119,1120.5,1116.9,1111.8,1087.4,1067.9,1059};
		double inLow[] = navSController.getDailyLow(scCode, NOD, tillDate.getTime());// {1014.95,1009,1026.95,1032.5,1030.55,1053.1,1046.15,1054.7,1054.2,1035.6,1033.25,1039.25,1038,1049.2,1074.35,1086,1086.1,1074.6,1080.5,1089.25,1070.3,1062.65,1075,1100.3,1100.55,1097.25,1099.2,1076,1057,1038.2,1026.55};
		double inClose[] = navSController.getDailyClose(scCode, NOD, tillDate.getTime());// {1017.85,1023.85,1044.95,1039.15,1049.65,1069.2,1055.6,1073.05,1059,1040.35,1047.45,1044.4,1049.45,1081.65,1081.8,1090.35,1098.85,1082,1096.25,1093.2,1074.85,1065.3,1105.05,1105.7,1111.85,1108.55,1102.2,1079.9,1061.85,1054.7,1030.5};
		
	
		int startIdx = (2 * optInTimePeriod) - 1;
		int total_days_data = inHigh.length;
		int endIdx = total_days_data - 1;

		MInteger outBegIdx = new MInteger();
		MInteger outNbElement = new MInteger();
		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();
		MInteger outBegIdxB = new MInteger();
		MInteger outNbElementB = new MInteger();


		double outReal[] = new double[inHigh.length];
		double outReal2[] = new double[inHigh.length];
		double outReal3[] = new double[inHigh.length];

		RetCode code = core.adx(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdxA, outNbElementA, outReal);
		boolean errorOccurred = false;

		if(code != RetCode.Success)
		{
			// System.out.println("Error");
			// System.out.println("adx lol1");
			errorOccurred = true;
		}
		RetCode code2 = core.minusDI(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdx, outNbElement, outReal2);
		if(code2 != RetCode.Success)
		{
			// System.out.println("Error");
			// System.out.println("adx lol1.2");
			errorOccurred = true;
		}
		RetCode code3 = core.plusDI(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdxB, outNbElementB, outReal3);
		if(code3 != RetCode.Success)
		{
			// System.out.println("Error");
			// System.out.println("adx lol1.3");
			errorOccurred = true;
		}

		if(errorOccurred)
		{
			// System.out.println("adx lol1");
			return null;
		}
		else if(outNbElementA.value > 0)
		{
			return new Float(outReal[(outNbElementA.value - 1)]);
			// md.setMinusDi(outReal2[(outNbElementA.value - 1)]);
			// md.setPlusDi(outReal3[(outNbElementA.value - 1)]);
		}
		return null;
	}
	
	public  Float calculateMacdCustom(int scCode, Calendar fromDate, Calendar tillDate, int optInFastPeriod, int optInSlowPeriod, int optInSignalPeriod)
	{
		
		NavSController navSController = new NavSController();

		Core core = new Core();
		// 34 is the minNOD
		int minNOD = optInSlowPeriod - 1 + optInSignalPeriod - 1 + 1;
		int NOD = minNOD + dailyWorkingDays;
		double close[] = navSController.getDailyClose(scCode, NOD, tillDate.getTime());

		// pre -data before todays/first date= optInSlowPeriod-1+optInSignalPeriod-1
		// first data
		int total_days_data = close.length;
		int startIdxM = minNOD;
		//int startIdxM = total_days_data - 30;
		// int endIdxM =startIdxM;
		int endIdxM = total_days_data - 1;
		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();
		double outMACD[] = new double[close.length];
		double outMACDSignal[] = new double[close.length];
		double outMACDHist[] = new double[close.length];
		RetCode code = core.macd(startIdxM, endIdxM, close, optInFastPeriod, optInSlowPeriod, optInSignalPeriod, outBegIdxA, outNbElementA, outMACD, outMACDSignal, outMACDHist);
		if(outNbElementA.value > 0)
		{
			return new Float(outMACD[(outNbElementA.value - 1)]);
		}
		// System.out.println("macd lol1");
		return null;
	}
	
	public Float calculateMacdSignalCustom(int scCode, Calendar fromDate, Calendar tillDate, int optInFastPeriod, int optInSlowPeriod, int optInSignalPeriod)
	{
		
		Core core = new Core();
		NavSController navSController = new NavSController();

		int minNOD = optInSlowPeriod - 1 + optInSignalPeriod - 1 + 1;
		int NOD = minNOD + dailyWorkingDays;
		double close[] = navSController.getDailyClose(scCode, NOD, tillDate.getTime());

		// pre -data before todays/first date= optInSlowPeriod-1+optInSignalPeriod-1
		// first data
		int total_days_data = close.length;
		int startIdxM = minNOD;
		// int endIdxM =startIdxM;
		int endIdxM = total_days_data - 1;
		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();
		double outMACD[] = new double[close.length];
		double outMACDSignal[] = new double[close.length];
		double outMACDHist[] = new double[close.length];
		RetCode code = core.macd(startIdxM, endIdxM, close, optInFastPeriod, optInSlowPeriod, optInSignalPeriod, outBegIdxA, outNbElementA, outMACD, outMACDSignal, outMACDHist);
		if(outNbElementA.value > 0)
		{
			return new Float(outMACDSignal[(outNbElementA.value - 1)]);
		}
		return null;
	}

	public Float calculateMacdHistogramCustom(int scCode, Calendar fromDate, Calendar tillDate, int optInFastPeriod, int optInSlowPeriod, int optInSignalPeriod)
	{
		
		NavSController navSController = new NavSController();
		Core core = new Core();
		int minNOD = optInSlowPeriod - 1 + optInSignalPeriod - 1 + 1;
		int NOD = minNOD + dailyWorkingDays;
		double close[] = navSController.getDailyClose(scCode, NOD, tillDate.getTime());

		// pre -data before todays/first date= optInSlowPeriod-1+optInSignalPeriod-1
		// first data
		int total_days_data = close.length;
		int startIdxM = minNOD;
		// int endIdxM =startIdxM;
		int endIdxM = total_days_data - 1;
		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();
		double outMACD[] = new double[close.length];
		double outMACDSignal[] = new double[close.length];
		double outMACDHist[] = new double[close.length];
		RetCode code = core.macd(startIdxM, endIdxM, close, optInFastPeriod, optInSlowPeriod, optInSignalPeriod, outBegIdxA, outNbElementA, outMACD, outMACDSignal, outMACDHist);
		if(outNbElementA.value > 0)
		{
			return new Float(outMACDHist[(outNbElementA.value - 1)]);
		}
		return null;
	}
	
	public Float calculateEMACustom(int scCode, Calendar fromDate, Calendar tillDate, int optInTimePeriod1)
	{

		NavSController navSController = new NavSController();
		Core core = new Core();
		int minNOD = optInTimePeriod1;
		int NOD = minNOD + dailyWorkingDays;
		double close[] = navSController.getDailyClose(scCode, NOD, tillDate.getTime());
		int total_days_data = close.length;

		int startIdx1 = optInTimePeriod1 - 1;
		int endIdx1 = total_days_data - 1;

		System.out.println("starIdx1="+startIdx1+" endIdx1="+endIdx1+" close.length="+close.length+" Nod="+NOD+"dailyWorkingDays="+dailyWorkingDays);
		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[close.length];

		RetCode code1 = core.ema(startIdx1, endIdx1, close, optInTimePeriod1, outBegIdx1, outNbElement1, outReal1);
		if(code1 != RetCode.Success)
		{
			return null;
		}
		if(outNbElement1.value > 0)
		{
			return new Float(outReal1[(outNbElement1.value - 1)]);
		}
		return null;
	}
	
	public  Float calculateRSICustom(int scCode, Calendar fromDate, Calendar tillDate, int optInTimePeriod)
	{
		NavSController navSController = new NavSController();

		// double data[] = getCashData(51);
		// pre -data before todays/first date = optInTimePeriod1-1
		Core core = new Core();
		int minNOD = optInTimePeriod;
		int NOD = minNOD + dailyWorkingDays;
		// System.out.println("NOD "+NOD+ "] minNod [" +minNOD+ "] fromDate [" +fromDate.getTime()+ "] tillDate [" +tillDate.getTime()+ "]");
		double close[] = navSController.getDailyClose(scCode, NOD, tillDate.getTime());

		int total_days_data = close.length;
		int startIdx1 = optInTimePeriod;
		int endIdx1 = total_days_data - 1;

		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[close.length];

		RetCode code1 = core.rsi(startIdx1, endIdx1, close, optInTimePeriod, outBegIdx1, outNbElement1, outReal1);

		if(code1 != RetCode.Success)
		{
			// return null or 0
			return null;
		}
		// System.out.println("outNbElement1.value [" + outNbElement1.value + "]");
		if(outNbElement1.value > 0)
		{
			return new Float(outReal1[(outNbElement1.value - 1)]);
		}

		return null;
	}

	// this function(core.sar) expect fromDate to be more than or equal to 200 days before tillDate
	public Float calculateWeeklyPsarCustom(int scCode, Calendar fromDate, Calendar tillDate, double accelerationFactor, double maximumFactor)
	{
		//System.out.println("update weekly Psar");
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculatePsar()");
			return null;
		}*/
		// Session hSession = null;
		// Transaction tx = null;
		try
		{
			// Nav nav = (Nav) hSession.createQuery("from Nav where mutualfundId=? and tradedate=?").setParameter(0, mutualFundId).setParameter(1, tillDate).uniqueResult();
			NavSController navSController = new NavSController();
			/*Nav nav = null;
			
			nav = navController.getNavByMutualFundId(mutualFundId, tillDate.getTime());*/

			Core core = new Core();
			int minDays = 14;
			int NOD = minDays + weeklyWorkingDays;
			// System.out.println("NOD=" + NOD + "=" + minDays + "+" + navController.getWeeklyWorkingDaysBetween(mutualFundId, fromDate.getTime(), tillDate.getTime()));
			// float inHigh[] = cashHandler.getCashDatafromDB(symbol, NOD, 1, tillDate); //
			// {1025.8,1031.8,1047,1055.75,1054.8,1072.25,1069.4,1074,1081.5,1068,1064.95,1068.7,1064.4,1087.85,1093.8,1101.9,1100.95,1100.9,1107.2,1124.9,1095.4,1084.9,1107,1117.5,1119,1120.5,1116.9,1111.8,1087.4,1067.9,1059};
			// float inLow[] = cashHandler.getCashDatafromDB(symbol, NOD, 2, tillDate);//
			// {1014.95,1009,1026.95,1032.5,1030.55,1053.1,1046.15,1054.7,1054.2,1035.6,1033.25,1039.25,1038,1049.2,1074.35,1086,1086.1,1074.6,1080.5,1089.25,1070.3,1062.65,1075,1100.3,1100.55,1097.25,1099.2,1076,1057,1038.2,1026.55};
			// float inClose[] = cashHandler.getCashDatafromDB(symbol, NOD, 3,
			// tillDate);//{1017.85,1023.85,1044.95,1039.15,1049.65,1069.2,1055.6,1073.05,1059,1040.35,1047.45,1044.4,1049.45,1081.65,1081.8,1090.35,1098.85,1082,1096.25,1093.2,1074.85,1065.3,1105.05,1105.7,1111.85,1108.55,1102.2,1079.9,1061.85,1054.7,1030.5};

			// close[] in ascending order of tradeDate
			// System.out.println("Till date(psar)=" + tillDate.getTime());
			// double close[] = navController.getNav(mutualFundId, NOD, tillDate.getTime());
			double high[] = navSController.getWeeklyHigh(scCode, NOD, tillDate.getTime());
			double low[] = navSController.getWeeklyLow(scCode, NOD, tillDate.getTime());

			int startIdx = 0;
			int total_days_data = high.length;
			int endIdx = total_days_data - 1;
			MInteger outBegIdxA = new MInteger();
			MInteger outNbElementA = new MInteger();


			double outReal[] = new double[high.length];
			/*double inAcc = 0.02;
			double inMax = .2;*/
			// RetCode code = core.sar(startIdx, endIdx, inHigh, inLow, accelerationFactor, maximumFactor, outBegIdxA, outNbElementA, outReal);
			RetCode code = core.sar(startIdx, endIdx, high, low, accelerationFactor, maximumFactor, outBegIdxA, outNbElementA, outReal);
			if(code != RetCode.Success)
			{
				return null;
			}
			// System.out.println("outNbElementA=" + outNbElementA.value);
			// System.out.println("outNbElementA.value [" +outNbElementA.value+ "] for trade date [" +fromDate.getTime()+ "]");
			if(outNbElementA.value > 0)
			{
				return new Float(outReal[(outNbElementA.value - 1)]);
			}
			return null;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}

	}
	
	public double[] calculateWeeklyBBandsCustom(int scCode, Calendar fromDate, Calendar tillDate, int optInTimePeriod, double upMultiplier, double downMultiplier)
	{
		//System.out.println("update weekly BBands");
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateBBands()");
			return null;
		}*/

		// double data[] = getCashData(51);
		// pre -data before todays/first date= optInTimePeriod1-1
		NavSController navSController = new NavSController();

		Core core = new Core();
		int minNOD = optInTimePeriod;
		int NOD = minNOD + weeklyWorkingDays;
		// System.out.println("NOD "+NOD);

		// float inReal[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);
		double close[] = navSController.getWeeklyClose(scCode, NOD, tillDate.getTime());

		int total_days_data = close.length;

		int startIdx1 = optInTimePeriod;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outUpper[] = new double[close.length];
		double outMiddle[] = new double[close.length];
		double outLower[] = new double[close.length];

		RetCode code1 = core.bbands(startIdx1, endIdx1, close, optInTimePeriod, upMultiplier, downMultiplier, MAType.Sma, outBegIdx1, outNbElement1, outUpper, outMiddle, outLower);

		if(outNbElement1.value > 0)
		{
			double bands[] = new double[3];
			/*BBands bb = new BBands();
			bb.setSymbol(symbol);
			bb.setTradeDate(tillDate.getTime());
			bb.setPeriod(optInTimePeriod);
			*/
			/*bb.setUpperBand(ImageLinks.getRoundedValue((outUpper[outNbElement1.value - 1]), 5));
			bb.setLowerBand(ImageLinks.getRoundedValue((outLower[outNbElement1.value - 1]), 5));
			bb.setMiddleBand(ImageLinks.getRoundedValue((outMiddle[outNbElement1.value - 1]), 5));*/
			/*System.out.println("Out Upper" + ImageLinks.getRoundedValue(outUpper[outNbElement1.value - 1], 5));
			System.out.println("Out Lower" + (ImageLinks.getRoundedValue((outLower[outNbElement1.value - 1]), 5)));
			System.out.println("Out Middle" + ImageLinks.getRoundedValue((outMiddle[outNbElement1.value - 1]), 5));
			System.out.println("NOD=" + NOD);*/
			bands[0] = ImageLinks.getRoundedValue(outUpper[outNbElement1.value - 1], 5);
			bands[1] = ImageLinks.getRoundedValue(outMiddle[outNbElement1.value - 1], 5);
			bands[2] = ImageLinks.getRoundedValue(outLower[outNbElement1.value - 1], 5);
			return bands;
			// sreturn bb;
		}
		return null;

	}

	public double calculateWeeklyAtr(int scCode, Calendar fromDate, Calendar tillDate, int optInTimePeriod)
	{
		//System.out.println("update weekly atr");
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateADX()");
			return -1;
		}*/

		Core core = new Core();
		int minNOD = optInTimePeriod;
		// int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);
		int NOD = minNOD + 250; // trying to smoothen the values. Changed on 01/09/2015

		NavSController navSController = new NavSController();


		/*float inReal[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);
		float inHigh[] = cashHandler.getCashDatafromDB(symbol, NOD, 1, tillDate);
		float inLow[] = cashHandler.getCashDatafromDB(symbol, NOD, 2, tillDate);*/
		double close[] = navSController.getWeeklyClose(scCode, NOD, tillDate.getTime());
		double high[] = navSController.getWeeklyHigh(scCode, NOD, tillDate.getTime());
		double low[] = navSController.getWeeklyLow(scCode, NOD, tillDate.getTime());
		int total_days_data = close.length;

		int startIdx1 = optInTimePeriod;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[close.length];

		RetCode code1 = core.atr(startIdx1, endIdx1, high, low, close, optInTimePeriod, outBegIdx1, outNbElement1, outReal1);
		if(code1 != RetCode.Success)
		{
			// System.out.println("lol1");
			return -1;
		}
		if(outNbElement1.value > 0)
		{
			// System.out.println("NOD=" + NOD + "   atr=" + ImageLinks.getRoundedValue((outReal1[(outNbElement1.value - 1)]), 5));
			return ImageLinks.getRoundedValue((outReal1[(outNbElement1.value - 1)]), 5);
		}
		return -1;
	}

	public Float calculateWeeklyADXCustom(int scCode, Calendar fromDate, Calendar tillDate, int optInTimePeriod)
	{
		//System.out.println("update weekly Adx");
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateADXCustom()");
			return null;
		}*/
		NavSController navSController = new NavSController();

		Core core = new Core();
		int minNOD = (2 * optInTimePeriod) - 1;
		// System.out.println("get Working Days");
		int NOD = minNOD + weeklyWorkingDays;

		/*float inHigh[] = cashHandler.getCashDatafromDB(symbol, NOD, 1, tillDate); // {1025.8,1031.8,1047,1055.75,1054.8,1072.25,1069.4,1074,1081.5,1068,1064.95,1068.7,1064.4,1087.85,1093.8,1101.9,1100.95,1100.9,1107.2,1124.9,1095.4,1084.9,1107,1117.5,1119,1120.5,1116.9,1111.8,1087.4,1067.9,1059};
		float inLow[] = cashHandler.getCashDatafromDB(symbol, NOD, 2, tillDate);// {1014.95,1009,1026.95,1032.5,1030.55,1053.1,1046.15,1054.7,1054.2,1035.6,1033.25,1039.25,1038,1049.2,1074.35,1086,1086.1,1074.6,1080.5,1089.25,1070.3,1062.65,1075,1100.3,1100.55,1097.25,1099.2,1076,1057,1038.2,1026.55};
		float inClose[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);// {1017.85,1023.85,1044.95,1039.15,1049.65,1069.2,1055.6,1073.05,1059,1040.35,1047.45,1044.4,1049.45,1081.65,1081.8,1090.35,1098.85,1082,1096.25,1093.2,1074.85,1065.3,1105.05,1105.7,1111.85,1108.55,1102.2,1079.9,1061.85,1054.7,1030.5};
		*/

		double close[] = navSController.getWeeklyClose(scCode, NOD, tillDate.getTime());
		double high[] = navSController.getWeeklyHigh(scCode, NOD, tillDate.getTime());
		double low[] = navSController.getWeeklyLow(scCode, NOD, tillDate.getTime());

		/*for (double a : close)
			System.out.println("close=" + a);*/

		int total_days_data = close.length;
		// int startIdx = (2 * optInTimePeriod) - 1;
		// int startIdx = total_days_data - minNOD;
		int startIdx = (2 * optInTimePeriod) - 1;
		int endIdx = total_days_data - 1;
		// System.out.println("asfs" + endIdx);
		MInteger outBegIdx = new MInteger();
		MInteger outNbElement = new MInteger();
		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();
		MInteger outBegIdxB = new MInteger();
		MInteger outNbElementB = new MInteger();


		double outReal[] = new double[close.length];
		double outReal2[] = new double[close.length];
		double outReal3[] = new double[close.length];

		RetCode code = core.adx(startIdx, endIdx, high, low, close, optInTimePeriod, outBegIdxA, outNbElementA, outReal);
		boolean errorOccurred = false;

		if(code != RetCode.Success)
		{
			// System.out.println("Error");
			// System.out.println("adx code error");
			errorOccurred = true;
		}
		RetCode code2 = core.minusDI(startIdx, endIdx, high, low, close, optInTimePeriod, outBegIdx, outNbElement, outReal2);
		if(code2 != RetCode.Success)
		{
			// System.out.println("Error");
			// System.out.println("adx code2 error");
			errorOccurred = true;
		}
		RetCode code3 = core.plusDI(startIdx, endIdx, high, low, close, optInTimePeriod, outBegIdxB, outNbElementB, outReal3);
		if(code3 != RetCode.Success)
		{
			// System.out.println("Error");
			// System.out.println("adx code3 error");
			errorOccurred = true;
		}

		if(errorOccurred)
		{
			// System.out.println("adx Error Occurred");
			return null;
		}
		else if(outNbElementA.value > 0)
		{
			return new Float(outReal[(outNbElementA.value - 1)]);
			// md.setMinusDi(outReal2[(outNbElementA.value - 1)]);
			// md.setPlusDi(outReal3[(outNbElementA.value - 1)]);
		}

		return null;
	}

	public Float calculateWeeklyMacdCustom(int scCode, Calendar fromDate, Calendar tillDate, int optInFastPeriod, int optInSlowPeriod, int optInSignalPeriod)
	{
		//System.out.println("update weekly Macd");
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateMacd()");
			return null;
		}*/
		NavSController navSController = new NavSController();

		Core core = new Core();
		// 34 is the minNOD
		int minNOD = optInSlowPeriod - 1 + optInSignalPeriod - 1 + 1;
		int NOD = minNOD + weeklyWorkingDays;
		double close[] = navSController.getWeeklyClose(scCode, NOD, tillDate.getTime());

		// pre -data before todays/first date= optInSlowPeriod-1+optInSignalPeriod-1
		// first data
		int total_days_data = close.length;
		int startIdxM = minNOD;
		//int startIdxM = total_days_data - 30;
		//int endIdxM =startIdxM;
		int endIdxM = total_days_data - 1;
		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();
		double outMACD[] = new double[close.length];
		double outMACDSignal[] = new double[close.length];
		double outMACDHist[] = new double[close.length];
		RetCode code = core.macd(startIdxM, endIdxM, close, optInFastPeriod, optInSlowPeriod, optInSignalPeriod, outBegIdxA, outNbElementA, outMACD, outMACDSignal, outMACDHist);
		if(outNbElementA.value > 0)
		{
			return new Float(outMACD[(outNbElementA.value - 1)]);
		}
		// System.out.println("macd lol1");
		return null;
	}

	public Float calculateWeeklyMacdSignalCustom(int scCode, Calendar fromDate, Calendar tillDate, int optInFastPeriod, int optInSlowPeriod, int optInSignalPeriod)
	{
		//System.out.println("update weekly MacdSignal");
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateMacd()");
			return null;
		}*/
		Core core = new Core();
		NavSController navSController = new NavSController();

		int minNOD = optInSlowPeriod - 1 + optInSignalPeriod - 1 + 1;
		int NOD = minNOD + weeklyWorkingDays;
		double close[] = navSController.getWeeklyClose(scCode, NOD, tillDate.getTime());

		// pre -data before todays/first date= optInSlowPeriod-1+optInSignalPeriod-1
		// first data
		int total_days_data = close.length;
		int startIdxM = minNOD;
		// int endIdxM =startIdxM;
		int endIdxM = total_days_data - 1;
		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();
		double outMACD[] = new double[close.length];
		double outMACDSignal[] = new double[close.length];
		double outMACDHist[] = new double[close.length];
		RetCode code = core.macd(startIdxM, endIdxM, close, optInFastPeriod, optInSlowPeriod, optInSignalPeriod, outBegIdxA, outNbElementA, outMACD, outMACDSignal, outMACDHist);
		if(outNbElementA.value > 0)
		{
			return new Float(outMACDSignal[(outNbElementA.value - 1)]);
		}
		return null;
	}

	public Float calculateWeeklyMacdHistogramCustom(int scCode, Calendar fromDate, Calendar tillDate, int optInFastPeriod, int optInSlowPeriod, int optInSignalPeriod)
	{
		//System.out.println("update weekly macdHistogram");
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateMacd()");
			return null;
		}*/
		NavSController navSController = new NavSController();
		Core core = new Core();
		int minNOD = optInSlowPeriod - 1 + optInSignalPeriod - 1 + 1;
		int NOD = minNOD + weeklyWorkingDays;
		double close[] = navSController.getWeeklyClose(scCode, NOD, tillDate.getTime());

		// pre -data before todays/first date= optInSlowPeriod-1+optInSignalPeriod-1
		// first data
		int total_days_data = close.length;
		int startIdxM = minNOD;
		// int endIdxM =startIdxM;
		int endIdxM = total_days_data - 1;
		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();
		double outMACD[] = new double[close.length];
		double outMACDSignal[] = new double[close.length];
		double outMACDHist[] = new double[close.length];
		RetCode code = core.macd(startIdxM, endIdxM, close, optInFastPeriod, optInSlowPeriod, optInSignalPeriod, outBegIdxA, outNbElementA, outMACD, outMACDSignal, outMACDHist);
		if(outNbElementA.value > 0)
		{
			return new Float(outMACDHist[(outNbElementA.value - 1)]);
		}
		return null;
	}

	public Float calculateWeeklyRSICustom(int scCode, Calendar fromDate, Calendar tillDate, int optInTimePeriod)
	{
		//System.out.println("update weekly rsi");
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{	
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateRSICustom()");
			return null;
		}*/
		NavSController navSController = new NavSController();

		// double data[] = getCashData(51);
		// pre -data before todays/first date= optInTimePeriod1-1
		Core core = new Core();
		int minNOD = optInTimePeriod;
		int NOD = minNOD + weeklyWorkingDays;
		// System.out.println("NOD "+NOD+ "] minNod [" +minNOD+ "] fromDate [" +fromDate.getTime()+ "] tillDate [" +tillDate.getTime()+ "]");

		double close[] = navSController.getWeeklyClose(scCode, NOD, tillDate.getTime());

		int total_days_data = close.length;
		int startIdx1 = optInTimePeriod;
		int endIdx1 = total_days_data - 1;

		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[close.length];

		RetCode code1 = core.rsi(startIdx1, endIdx1, close, optInTimePeriod, outBegIdx1, outNbElement1, outReal1);

		if(code1 != RetCode.Success)
		{
			// return null or 0
			return null;
		}
		// System.out.println("outNbElement1.value [" + outNbElement1.value + "]");
		if(outNbElement1.value > 0)
		{
			return new Float(outReal1[(outNbElement1.value - 1)]);
		}

		return null;
	}

	public Float calculateWeeklyEMACustom(int scCode, Calendar fromDate, Calendar tillDate, int optInTimePeriod1)
	{
		//System.out.println("update weekly Ema20,ema50,ema200");
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateEMACustom()");
			return null;
		}*/
		// pre -data before todays/first date= optInTimePeriod1-1
		/*Core core = new Core();
		int minNOD = optInTimePeriod1 - 1;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);*/


		NavSController navSController = new NavSController();
		Core core = new Core();
		int minNOD = optInTimePeriod1;
		int NOD = minNOD + weeklyWorkingDays;
		double close[] = navSController.getWeeklyClose(scCode, NOD, tillDate.getTime());

		int total_days_data = close.length;

		int startIdx1 = optInTimePeriod1 - 1;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[close.length];

		RetCode code1 = core.ema(startIdx1, endIdx1, close, optInTimePeriod1, outBegIdx1, outNbElement1, outReal1);
		if(code1 != RetCode.Success)
		{
			return null;
		}
		if(outNbElement1.value > 0)
		{
			return new Float(outReal1[(outNbElement1.value - 1)]);
		}
		return null;
	}

}
