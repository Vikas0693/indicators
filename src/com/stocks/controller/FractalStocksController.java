package com.stocks.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.apidoc.util.ImageLinks;
import com.stocks.dao.EveryDayCandleStocks;
import com.stocks.dao.FractalStocks;

public class FractalStocksController {

	public void updateDailyFractalStocksAfterDate() throws Exception
	{
		Session hSession = null;
		Transaction tx = null;
		Date lastUpdatedDate=null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			System.out.println("Conn. established with indicators");
			List scCodeList = hSession.createQuery("select distinct(scCode) from EveryDayCandleStocks where candleType=?").setParameter(0, 1).list();
			Iterator itr = scCodeList.iterator();
			while (itr.hasNext())
			{
				int scCode = (int) itr.next();
				
				System.out.println("update daily Fractal for="+scCode);
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from FractalStocks where scCode=? and period=2 and candleType=1 and upFractal!=0").setParameter(0,scCode).setMaxResults(1).uniqueResult();
				Date startDate=lastUpdatedDate;
				if(lastUpdatedDate==null)
				{
					List l=hSession.createQuery("select tradeDate from EveryDayCandleStocks where scCode=? and candleType=1 order by tradeDate").setParameter(0, scCode).setMaxResults(5).list();
					//the reason of doing below is to bypass generating fractal for those funds for which there are less than 5 dates/ohcl data
					if(l.size()<5)
						continue;
					startDate=(Date)l.get(0);
					lastUpdatedDate=(Date)l.get(4);
				}				
				List everyDayFromStartDate = hSession.createQuery("from EveryDayCandleStocks where scCode=? and tradeDate>=? and candleType=? order by tradeDate asc").setParameter(0, scCode).setParameter(1, startDate).setParameter(2, 1).list();
				Iterator itr1 = everyDayFromStartDate.iterator();
				try
				{
					try
					{
						// tx for closePrice
						tx = hSession.beginTransaction();
						while (itr1.hasNext())
						{
							EveryDayCandleStocks edc = (EveryDayCandleStocks) itr1.next();
							FractalStocks f = new FractalStocks();
							f.setScCode(edc.getScCode());
							f.setCandleType(1);
							f.setTradeDate(edc.getTradeDate());
							f.setOpen(edc.getOpen());
							f.setHigh(edc.getHigh());
							f.setClose(edc.getClose());
							f.setLow(edc.getLow());
							f.setPeriod(2);
							hSession.saveOrUpdate(f);							
						}
						// System.out.println("fractal update for closePrice starting from :" + sDate);
						tx.commit();
					}
					catch (Exception i)
					{
						if(tx != null)
						{
							tx.rollback();
						}
						throw new Exception("Not able to persist fractal close price");
					}
					// transaction for upFractal and lowFractal
					tx = hSession.beginTransaction();
					List<Date> fourDaysBackFromSdateList = hSession.createQuery("select tradeDate from FractalStocks where scCode=? and period=? and tradeDate<? and candleType=? order by tradeDate desc").setParameter(0, scCode).setParameter(1, 2).setParameter(2, lastUpdatedDate).setParameter(3, 1)
							.setMaxResults(4).list();
					// System.out.println("start date=" + startDate + "fourDaysBackFromSdateList.size()=" + fourDaysBackFromSdateList.size());
					Date newStartDate = fourDaysBackFromSdateList.get(3);

					/*FractalStocks checkFractalForPreviousToStartDate = (FractalStocks) hSession.createQuery("from FractalStocks where scCode=? and period=? and tradeDate=? and candleType=?").setParameter(0, scCode).setParameter(1, 2).setParameter(2, ((Date) fourDaysBackFromSdateList.get(0))).setParameter(3, 1)
							.uniqueResult();*/
					
					List fractalList = hSession.createQuery("from FractalStocks where period=? and scCode=? and tradeDate>=? and candleType=? order by tradeDate").setParameter(0, 2).setParameter(1, scCode).setParameter(2, newStartDate).setParameter(3, 1).list();

					int size = fractalList.size();
					List<FractalStocks> fiveDaysFractalList = null;
					for (int i = 4; i < size; i++)
					{
						FractalStocks upFractal = null;
						FractalStocks lowFractal = null;
						fiveDaysFractalList = new ArrayList<FractalStocks>();
						for (int j = i - 4; j <= i; j++)
						{
							if(i - j == 2)
							{
								upFractal = (FractalStocks) fractalList.get(j);
								lowFractal = (FractalStocks) fractalList.get(j);
							}
							fiveDaysFractalList.add((FractalStocks) fractalList.get(j));
						}
			
						// insert previous maxFractal,maxFractalDate if there is no unique max date from the 5 days data in fiveDaysFractalList
						// flag for (equal or less),(equal or greater) than neighbour values in fractal list of 5
						//for upFractal
						boolean flagForEqualOrGreaterNeighborValuesInFractal = false;
						//for lowFractal
						boolean flagForEqualOrLessNeighborValuesInFractal = false;
						Iterator it = fiveDaysFractalList.iterator();
						
						double upFractalPrice = upFractal.getHigh();
						Date upFractalDate = upFractal.getTradeDate();
						double lowFractalPrice = lowFractal.getLow();
						Date lowFractalDate = lowFractal.getTradeDate();
						while (it.hasNext())
						{
							FractalStocks fr = (FractalStocks) it.next();
							if(upFractalPrice == fr.getHigh() && upFractalDate.compareTo(fr.getTradeDate()) == 0)
							{
								continue;
							}
							if(fr.getHigh() >= upFractalPrice)
							{
								flagForEqualOrGreaterNeighborValuesInFractal = true;
								break;
							}
						}
						it = fiveDaysFractalList.iterator();
						while (it.hasNext())
						{
							FractalStocks fr = (FractalStocks) it.next();
							if(lowFractalPrice == fr.getLow() && lowFractalDate.compareTo(fr.getTradeDate()) == 0)
							{
								continue;
							}
							if(fr.getLow() <= lowFractalPrice)
							{
								flagForEqualOrLessNeighborValuesInFractal = true;
								break;
							}
						}
						FractalStocks currentFractal = null;
						if(flagForEqualOrLessNeighborValuesInFractal == true)
						{
							FractalStocks previousToCurrentFractal = (FractalStocks) fiveDaysFractalList.get(3);
							currentFractal = (FractalStocks) fiveDaysFractalList.get(4);
							currentFractal.setLowFractal(previousToCurrentFractal.getLowFractal());
							currentFractal.setMinFractalDate(previousToCurrentFractal.getMinFractalDate());
							// System.out.println("Neighbour value is less or equal than lowFractal");
							// System.out.println(currentFractal.getLowFractal() + "\t" + currentFractal.getMinFractalDate());
						}
						else
						{
							currentFractal = (FractalStocks) fiveDaysFractalList.get(4);
							currentFractal.setLowFractal(lowFractalPrice);
							currentFractal.setMinFractalDate(lowFractalDate);
							// System.out.println("LowFractal price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getLowFractal() + "Date =" + currentFractal.getMinFractalDate());
						}
						if(flagForEqualOrGreaterNeighborValuesInFractal == true)
						{
							FractalStocks previousToCurrentFractal = (FractalStocks) fiveDaysFractalList.get(3);
							currentFractal = (FractalStocks) fiveDaysFractalList.get(4);
							currentFractal.setUpFractal(previousToCurrentFractal.getUpFractal());
							currentFractal.setMaxFractalDate(previousToCurrentFractal.getMaxFractalDate());
							// System.out.println("Neighbour value is greater or equal than upFractal");
							// System.out.println(currentFractal.getUpFractal() + "\t" + currentFractal.getMaxFractalDate());
						}
						else
						{
							currentFractal = (FractalStocks) fiveDaysFractalList.get(4);
							currentFractal.setUpFractal(upFractalPrice);
							currentFractal.setMaxFractalDate(upFractalDate);
							// System.out.println("UpFractal price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getUpFractal() + "Date =" + currentFractal.getMaxFractalDate());
						}
						flagForEqualOrGreaterNeighborValuesInFractal = false;
						flagForEqualOrLessNeighborValuesInFractal = false;
						// System.out.println("================================================");
						
						hSession.saveOrUpdate(currentFractal);
						//System.out.println("UpFractal="+currentFractal.getUpFractal());
						//System.out.println("LowFractal="+currentFractal.getLowFractal());
					}
					tx.commit();
				}
				catch (Exception e)
				{
					if(tx != null)
					{
						tx.rollback();
					}
				}
			}
		}
		catch (Exception e)
		{
			if(tx != null)
				tx.rollback();
			
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateWeeklyFractalStocksAfterDate() throws Exception
	{		
		Session hSession = null;
		Transaction tx = null;
		Date lastUpdatedDate=null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			List scCodeList = hSession.createQuery("select distinct(scCode) from EveryDayCandleStocks where candleType=?").setParameter(0, 0).list();
			Iterator itr = scCodeList.iterator();
			while (itr.hasNext())
			{
				int scCode = (int) itr.next();
				
				System.out.println("updating weekly fractal stocks scCode="+scCode);
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from FractalStocks where scCode=? and candleType=0 and period=2 and upFractal!=0").setParameter(0, scCode).setMaxResults(1).uniqueResult();
				Date startDate=lastUpdatedDate;
				if(lastUpdatedDate==null)
				{
					List l=hSession.createQuery("select tradeDate from EveryDayCandleStocks where scCode=? and candleType=0 order by tradeDate").setParameter(0, scCode).setMaxResults(5).list();
					if(l.size()<5)
						continue;
					startDate=(Date)l.get(0);
					lastUpdatedDate=(Date)l.get(4);
				}
				List everyDayFromStartDate = hSession.createQuery("from EveryDayCandleStocks where scCode=? and tradeDate>=? and candleType=? order by tradeDate asc").setParameter(0, scCode).setParameter(1, startDate).setParameter(2, 0).list();
				Iterator itr1 = everyDayFromStartDate.iterator();
				try
				{
					try
					{
						// tx for closePrice
						tx = hSession.beginTransaction();
						while (itr1.hasNext())
						{
							EveryDayCandleStocks edc = (EveryDayCandleStocks) itr1.next();
							FractalStocks f = new FractalStocks();
							f.setScCode(scCode);
							f.setCandleType(0);
							f.setTradeDate(edc.getTradeDate());
							f.setOpen(edc.getOpen());
							f.setHigh(edc.getHigh());
							f.setClose(edc.getClose());
							f.setLow(edc.getLow());
							f.setPeriod(2);
							hSession.saveOrUpdate(f);
						}
						// System.out.println("fractal update for closePrice starting from :" + sDate);
						tx.commit();
					}
					catch (Exception i)
					{
						if(tx != null)
						{
							tx.rollback();
						}
						throw new Exception("Not able to persist fractal close price");
					}
					// transaction for upFractal and lowFractal
					tx = hSession.beginTransaction();
					List<Date> fourDaysBackFromSdateList = hSession.createQuery("select tradeDate from FractalStocks where scCode=? and period=? and tradeDate<? and candleType=? order by tradeDate desc").setParameter(0, scCode).setParameter(1, 2).setParameter(2, lastUpdatedDate).setParameter(3, 0)
							.setMaxResults(4).list();
					// System.out.println("start date=" + startDate + "fourDaysBackFromSdateList.size()=" + fourDaysBackFromSdateList.size());
					Date newStartDate = fourDaysBackFromSdateList.get(3);

					/*Fractal checkFractalForPreviousToStartDate = (Fractal) hSession.createQuery("from Fractal where mutualFundId=? and period=? and tradeDate=? and candleType=?").setParameter(0, mfId).setParameter(1, 2).setParameter(2, ((Date) fourDaysBackFromSdateList.get(0))).setParameter(3, 0)
							.uniqueResult();*/
					
					List fractalList = hSession.createQuery("from FractalStocks where period=? and scCode=? and tradeDate>=? and candleType=? order by tradeDate").setParameter(0, 2).setParameter(1, scCode).setParameter(2, newStartDate).setParameter(3, 0).list();

					int size = fractalList.size();
					List<FractalStocks> fiveDaysFractalList = null;
					for (int i = 4; i < size; i++)
					{
						FractalStocks upFractal = null;
						FractalStocks lowFractal = null;
						fiveDaysFractalList = new ArrayList<FractalStocks>();
						for (int j = i - 4; j <= i; j++)
						{
							if(i - j == 2)
							{
								upFractal = (FractalStocks) fractalList.get(j);
								lowFractal = (FractalStocks) fractalList.get(j);
							}
							fiveDaysFractalList.add((FractalStocks) fractalList.get(j));
						}
						Iterator itr2 = fiveDaysFractalList.iterator();
						
						// insert previous maxFractal,maxFractalDate if there is no unique max date from the 5 days data in fiveDaysFractalList
						// flag for (equal or less),(equal or greater) than neighbour values in fractal list of 5
						boolean flagForEqualOrGreaterNeighborValuesInFractal = false;
						boolean flagForEqualOrLessNeighborValuesInFractal = false;
						Iterator it = fiveDaysFractalList.iterator();

						double upFractalPrice = upFractal.getHigh();
						Date upFractalDate = upFractal.getTradeDate();
						double lowFractalPrice = lowFractal.getLow();
						Date lowFractalDate = lowFractal.getTradeDate();
						while (it.hasNext())
						{
							FractalStocks fr = (FractalStocks) it.next();
							if(upFractalPrice == fr.getHigh() && upFractalDate.compareTo(fr.getTradeDate()) == 0)
							{
								continue;
							}
							if(fr.getHigh() >= upFractalPrice)
							{
								flagForEqualOrGreaterNeighborValuesInFractal = true;
								break;
							}
						}
						it = fiveDaysFractalList.iterator();
						while (it.hasNext())
						{
							FractalStocks fr = (FractalStocks) it.next();
							if(lowFractalPrice == fr.getLow() && lowFractalDate.compareTo(fr.getTradeDate()) == 0)
							{
								continue;
							}
							if(fr.getLow() <= lowFractalPrice)
							{
								flagForEqualOrLessNeighborValuesInFractal = true;
								break;
							}
						}
						FractalStocks currentFractal = null;
						if(flagForEqualOrLessNeighborValuesInFractal == true)
						{
							FractalStocks previousToCurrentFractal = (FractalStocks) fiveDaysFractalList.get(3);
							currentFractal = (FractalStocks) fiveDaysFractalList.get(4);
							currentFractal.setLowFractal(previousToCurrentFractal.getLowFractal());
							currentFractal.setMinFractalDate(previousToCurrentFractal.getMinFractalDate());
							// System.out.println("Neighbour value is less or equal than lowFractal");
							// System.out.println(currentFractal.getLowFractal() + "\t" + currentFractal.getMinFractalDate());
						}
						else
						{
							currentFractal = (FractalStocks) fiveDaysFractalList.get(4);
							currentFractal.setLowFractal(lowFractalPrice);
							currentFractal.setMinFractalDate(lowFractalDate);
							// System.out.println("LowFractal price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getLowFractal() + "Date =" + currentFractal.getMinFractalDate());
						}
						if(flagForEqualOrGreaterNeighborValuesInFractal == true)
						{
							FractalStocks previousToCurrentFractal = (FractalStocks) fiveDaysFractalList.get(3);
							currentFractal = (FractalStocks) fiveDaysFractalList.get(4);
							currentFractal.setUpFractal(previousToCurrentFractal.getUpFractal());
							currentFractal.setMaxFractalDate(previousToCurrentFractal.getMaxFractalDate());
							// System.out.println("Neighbour value is greater or equal than upFractal");
							// System.out.println(currentFractal.getUpFractal() + "\t" + currentFractal.getMaxFractalDate());
						}
						else
						{
							currentFractal = (FractalStocks) fiveDaysFractalList.get(4);
							currentFractal.setUpFractal(upFractalPrice);
							currentFractal.setMaxFractalDate(upFractalDate);
							// System.out.println("UpFractal price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getUpFractal() + "Date =" + currentFractal.getMaxFractalDate());
						}
						flagForEqualOrGreaterNeighborValuesInFractal = false;
						flagForEqualOrLessNeighborValuesInFractal = false;
						// System.out.println("================================================");
						hSession.saveOrUpdate(currentFractal);

					}
					tx.commit();
				}
				catch (Exception e)
				{
					if(tx != null)
					{
						tx.rollback();
					}
					
				}
			}
		}
		catch (Exception e)
		{
			if(tx != null)
				tx.rollback();
			
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}

	}

	public void updateWeeklyFractalStocksPeriod1AfterDate() throws Exception
	{

		System.out.println("Inserting ohlc into weekly Fractal period 1");
		Session hSession = null;
		Transaction tx = null;
		
		try
		{
			Date lastUpdatedDate=null;
			hSession = HibernateBridge.getSessionFactory().openSession();
			List everyDayWeeklyList = hSession.createQuery("select distinct scCode from EveryDayCandleStocks where candleType=0 order by scCode").list();
			FractalStocks fractal = null;
			EveryDayCandleStocks edc=null;
			int scCode=0;
			List<EveryDayCandleStocks> edcList=null;
			Iterator itr1=null;
			Iterator itr = everyDayWeeklyList.iterator();
			while (itr.hasNext())
			{
				scCode = (int) itr.next();
				
				System.out.println("update weeklyFractalStocks with period=1 for scCode="+scCode);
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from FractalStocks where scCode=? and candleType=0 and period=1 and upFractal!=0").setParameter(0, scCode).setMaxResults(1).uniqueResult();
				if(lastUpdatedDate==null)
				{
					lastUpdatedDate=(Date)hSession.createQuery("select min(tradeDate) from EveryDayCandleStocks where scCode=? and candleType=0").setParameter(0, scCode).setMaxResults(1).uniqueResult();
				}
				if(lastUpdatedDate==null)
				{
					//throw new Exception("(updateWeeklyFractalStocksPeriod1AfterDate method called)no data with Fractal is available in Fractal or EverydayCandleStocks for scCode="+scCode+" hence not able to update fractal weekly with period=1");
					continue;
				}
				edcList= hSession.createQuery("from EveryDayCandleStocks where candleType=0 and scCode=? and tradeDate>=? order by tradeDate").setParameter(0, scCode).setParameter(1,lastUpdatedDate).list();
				tx = hSession.beginTransaction();
				itr1 = edcList.iterator();
				while (itr1.hasNext())
				{
					edc= (EveryDayCandleStocks) itr1.next();
					fractal = new FractalStocks();
					fractal.setScCode(scCode);
					fractal.setCandleType(0);
					fractal.setPeriod(1);
					fractal.setTradeDate(edc.getTradeDate());
					fractal.setOpen(edc.getOpen());
					fractal.setClose(edc.getClose());
					fractal.setHigh(edc.getHigh());
					fractal.setLow(edc.getLow());
					hSession.saveOrUpdate(fractal);
				}
				hSession.flush();
				tx.commit();
				System.gc();
			}	
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	
	}

	public void updateWeeklyUpLowFractalStocksPeriod1AfterDate()
	{
		System.out.println("updating weekly upFractal/lowFractal Stocks for period = 1");
		Session hSession = null;
		Transaction tx = null;
		Date lastUpdatedDate=null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			List scCodeList = hSession.createQuery("select distinct(scCode) from EveryDayCandleStocks where candleType=?").setParameter(0, 0).list();
			Iterator itr = scCodeList.iterator();
			while (itr.hasNext())
			{
				int scCode = (int) itr.next();
				System.out.println("updating weekly upFractal/lowFractal Stocks for scCode="+scCode);
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from FractalStocks where scCode=? and candleType=0 and period=1 and upFractal!=0 and lowFractal!=0").setParameter(0, scCode).setMaxResults(1).uniqueResult();
				if(lastUpdatedDate==null)
				{
					List l=hSession.createQuery("select tradeDate from FractalStocks where scCode=? and candleType=0 and period=1 order by tradeDate").setParameter(0, scCode).setMaxResults(3).list();
					if(l.size()<3)
						continue;
					lastUpdatedDate=(Date)l.get(2);
				}			
				
				// transaction for upFractal and lowFractal
				tx = hSession.beginTransaction();
				List<Date> twoDaysBackFromSdateList = hSession.createQuery("select tradeDate from FractalStocks where scCode=? and period=1 and tradeDate<? and candleType=0 order by tradeDate desc").setParameter(0, scCode).setParameter(1, lastUpdatedDate).setMaxResults(2).list();

				Date newStartDate = twoDaysBackFromSdateList.get(1);

				//Fractal checkFractalForPreviousToStartate = (Fractal) hSession.createQuery("from Fractal where mutualFundId=? and period=1 and tradeDate=? and candleType=0").setParameter(0, mfId).setParameter(2, ((Date) fourDaysBackFromSdateList.get(0))).uniqueResult();
				List fractalList = hSession.createQuery("from FractalStocks where period=1 and scCode=? and tradeDate>=? and candleType=0 order by tradeDate").setParameter(0, scCode).setParameter(1, newStartDate).list();
				
				
				int size = fractalList.size();
				List<FractalStocks> threeDaysFractalList = null;
				for (int i = 2; i < size; i++)
				{
					FractalStocks upFractal = null;
					FractalStocks lowFractal = null;
					threeDaysFractalList = new ArrayList<FractalStocks>();
					for (int j = i - 2; j <= i; j++)
					{
						if(i - j == 1)
						{
							upFractal = (FractalStocks) fractalList.get(j);
							lowFractal = (FractalStocks) fractalList.get(j);
						}
						threeDaysFractalList.add((FractalStocks) fractalList.get(j));
					}
					
					// insert previous maxFractal,maxFractalDate if there is no unique max date from the 3 days data in threeDaysFractalList
					// flag for (equal or less),(equal or greater) than neighbour values in fractal list of 3
					boolean flagForEqualOrGreaterNeighborValuesInFractal = false;
					boolean flagForEqualOrLessNeighborValuesInFractal = false;
					

					double upFractalPrice = upFractal.getHigh();
					Date upFractalDate = upFractal.getTradeDate();
					double lowFractalPrice = lowFractal.getLow();
					Date lowFractalDate = lowFractal.getTradeDate();
					Iterator it = threeDaysFractalList.iterator();
					while (it.hasNext())
					{
						FractalStocks fr = (FractalStocks) it.next();
						if(upFractalPrice == fr.getHigh() && upFractalDate.compareTo(fr.getTradeDate()) == 0)
						{
							continue;
						}
						if(fr.getHigh() >= upFractalPrice)
						{
							flagForEqualOrGreaterNeighborValuesInFractal = true;
							break;
						}
					}
					it = threeDaysFractalList.iterator();
					while (it.hasNext())
					{
						FractalStocks fr = (FractalStocks) it.next();
						if(lowFractalPrice == fr.getLow() && lowFractalDate.compareTo(fr.getTradeDate()) == 0)
						{
							continue;
						}
						if(fr.getLow() <= lowFractalPrice)
						{
							flagForEqualOrLessNeighborValuesInFractal = true;
							break;
						}
					}
					FractalStocks currentFractal = null;
					if(flagForEqualOrLessNeighborValuesInFractal == true)
					{
						FractalStocks previousToCurrentFractal = (FractalStocks) threeDaysFractalList.get(1);
						currentFractal = (FractalStocks) threeDaysFractalList.get(2);
						currentFractal.setLowFractal(previousToCurrentFractal.getLowFractal());
						currentFractal.setMinFractalDate(previousToCurrentFractal.getMinFractalDate());
						// System.out.println("Neighbour value is less or equal than lowFractal");
						// System.out.println(currentFractal.getLowFractal() + "\t" + currentFractal.getMinFractalDate());
					}
					else
					{
						currentFractal = (FractalStocks) threeDaysFractalList.get(2);
						currentFractal.setLowFractal(lowFractalPrice);
						currentFractal.setMinFractalDate(lowFractalDate);
						// System.out.println("LowFractal price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getLowFractal() + "Date =" + currentFractal.getMinFractalDate());
					}
					if(flagForEqualOrGreaterNeighborValuesInFractal == true)
					{
						FractalStocks previousToCurrentFractal = (FractalStocks) threeDaysFractalList.get(1);
						currentFractal = (FractalStocks) threeDaysFractalList.get(2);
						currentFractal.setUpFractal(previousToCurrentFractal.getUpFractal());
						currentFractal.setMaxFractalDate(previousToCurrentFractal.getMaxFractalDate());
						// System.out.println("Neighbour value is greater or equal than upFractal");
						//System.out.println(currentFractal.getUpFractal() + "\t" + currentFractal.getMaxFractalDate()+"\t"+currentFractal.getMaxFractalDate());
					}
					else
					{
						currentFractal = (FractalStocks) threeDaysFractalList.get(2);
						currentFractal.setUpFractal(upFractalPrice);
						currentFractal.setMaxFractalDate(upFractalDate);
						//System.out.println("UpFractal with period=1 price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getUpFractal() + "Date =" + currentFractal.getMaxFractalDate());
					}
					
					flagForEqualOrGreaterNeighborValuesInFractal = false;
					flagForEqualOrLessNeighborValuesInFractal = false;
					// System.out.println("================================================");
					hSession.saveOrUpdate(currentFractal);

				}
				tx.commit();
			}
					

		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}	
	}

	public void updateDailySwingStocksAfterDate()
	{
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			String noUpFractalAvailable="";
			List scCodeList = hSession.createQuery("select distinct(scCode) from EveryDayCandleStocks where candleType=1").list();
			Iterator itr = scCodeList.iterator();
			Date lastUpdatedDate=null;
			while (itr.hasNext())
			{
				int scCode = (int) itr.next();
				
				System.out.println("updating daily swing Stocks for scCode="+scCode);
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from FractalStocks where scCode=? and candleType=1 and period=2 and swingHigh!=0 and swingLow!=0").setParameter(0, scCode).setMaxResults(1).uniqueResult();
				if(lastUpdatedDate==null)
				{
					//reason of upFractal!=0 is bcoz swing is dependent on it
					lastUpdatedDate=(Date)hSession.createQuery("select min(tradeDate) from FractalStocks where scCode=? and candleType=1 and period=2 and (upFractal!=0 or lowFractal!=0)").setParameter(0, scCode).setMaxResults(1).uniqueResult();
				}				
				if(lastUpdatedDate==null)
				{
					noUpFractalAvailable=noUpFractalAvailable+" ' "+scCode;
					continue;
				}
				/*System.out.println("Last Updated Date="+lastUpdatedDate);*/
				List<FractalStocks> fractalList = hSession.createQuery("from FractalStocks where scCode=? and tradeDate>=? and candleType=1 and period=2 order by tradeDate").setParameter(0, scCode).setParameter(1, lastUpdatedDate).list();
				tx = hSession.beginTransaction();
				FractalStocks fractal = null;
				for (int i = 0; i < fractalList.size(); i++)
				{
					
					boolean swingTrend = false;
					double tcfl1 = 0, tcfl2 = 0, tcfl3 = 0;
					fractal = fractalList.get(i);
					double swingHigh, swingLow;
					Date swingLowDate, swingHighDate;
					Date tradeDate = fractal.getTradeDate();
					Calendar cal = Calendar.getInstance();
					cal.setTime(tradeDate);
					cal.add(Calendar.MONTH, -3);
					Object[] fractalMax = (Object[]) hSession.createQuery("select max(upFractal),min(maxFractalDate) from FractalStocks where upFractal =(select max(upFractal) from FractalStocks where scCode=? and tradeDate between ? and ? and candleType=1 and period=2 and upFractal!=0 ) and period=2 and candleType=1 and scCode=? and tradeDate between ? and ?")
							.setParameter(0, scCode).setParameter(1, cal.getTime()).setParameter(2, tradeDate).setParameter(3, scCode).setParameter(4, cal.getTime()).setParameter(5, tradeDate).uniqueResult();
					if(fractalMax[0] == null && fractalMax[1] == null)
					{
						continue;
					}
					else
					{
						swingHigh = (double) fractalMax[0];
						swingHighDate = (Date) fractalMax[1];
						
						fractal.setSwingHigh(swingHigh);
						fractal.setSwingHighDate(swingHighDate);
					}
					Object[] fractalMin = (Object[]) hSession.createQuery("select min(lowFractal),min(minFractalDate) from FractalStocks where lowFractal = (select min(lowFractal) from FractalStocks where scCode=? and tradeDate between ? and ? and lowFractal!=0 and candleType=1 and period=2) and period=2 and candleType=1 and scCode=? and tradeDate between ? and ?")
							.setParameter(0, scCode).setParameter(1, cal.getTime()).setParameter(2, tradeDate).setParameter(3, scCode).setParameter(4, cal.getTime()).setParameter(5, tradeDate).uniqueResult();
					if(fractalMin[0] == null || fractalMin[1] == null)
					{
						continue;
					}
					else
					{
						swingLow = (double) fractalMin[0];
						swingLowDate = (Date) fractalMin[1];
						
						fractal.setSwingLow(swingLow);
						fractal.setSwingLowDate(swingLowDate);
					}
					if(swingHighDate.before(swingLowDate))
					{
						fractal.setSwingTrend("Low");
						tcfl1 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.382)).floatValue(), 3);
						tcfl2 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.5)).floatValue(), 3);
						tcfl3 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.618)).floatValue(), 3);
					}
					else if(swingLowDate.before(swingHighDate))
					{
						fractal.setSwingTrend("High");
						tcfl1 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.382)).floatValue(), 3);
						tcfl2 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.5)).floatValue(), 3);
						tcfl3 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.618)).floatValue(), 3);
					}
					fractal.setTcfl1(tcfl1);
					fractal.setTcfl2(tcfl2);
					fractal.setTcfl3(tcfl3);
					//System.out.println("Swing high Date="+swingHighDate+" swing High="+swingHigh+"swing low Date="+swingLow+" swing Low="+swingLow+ "tcfl1="+tcfl1+"  tcfl2="+tcfl2+"  tcfl3="+tcfl3);
					hSession.saveOrUpdate(fractal);
					
				}
				tx.commit();
			}			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}	
	}

	public void updateWeeklySwingStocksAfterDate()
	{

		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			String noUpFractalAvailable="";
			List scCodeList = hSession.createQuery("select distinct(scCode) from EveryDayCandleStocks where candleType=?").setParameter(0, 0).list();
			Iterator itr = scCodeList.iterator();
			Date lastUpdatedDate=null;
			while (itr.hasNext())
			{
				int scCode=(int) itr.next();
				
				System.out.println("updating weekly swing stocks for scCode="+scCode);
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from FractalStocks where scCode=? and candleType=0 and period=2 and upFractal!=0 and swingHigh!=0 and swingLow!=0").setParameter(0, scCode).setMaxResults(1).uniqueResult();
				if(lastUpdatedDate==null)
				{
					lastUpdatedDate=(Date)hSession.createQuery("select min(tradeDate) from FractalStocks where scCode=? and candleType=0 and period=2 and (upFractal!=0 or lowFractal!=0)").setParameter(0, scCode).setMaxResults(1).uniqueResult();
				}
				if(lastUpdatedDate==null)
				{
					noUpFractalAvailable=noUpFractalAvailable+","+scCode;
					continue;
				}				
				List<FractalStocks> fractalList = hSession.createQuery("from FractalStocks where scCode=? and tradeDate>=? and candleType=0 and period=2 order by tradeDate").setParameter(0, scCode).setParameter(1, lastUpdatedDate).list();
				FractalStocks fractal = null;
				tx = hSession.beginTransaction();
				for (int i = 0; i < fractalList.size(); i++)
				{
					boolean swingTrend = false;
					double tcfl1 = 0, tcfl2 = 0, tcfl3 = 0;
					fractal = fractalList.get(i);
					double swingHigh, swingLow;
					Date swingLowDate, swingHighDate;
					Date tradeDate = fractal.getTradeDate();
					Calendar cal = Calendar.getInstance();
					cal.setTime(tradeDate);
					cal.add(Calendar.MONTH, -3);
					Object[] fractalMax = (Object[]) hSession.createQuery("select max(upFractal),min(maxFractalDate) from FractalStocks where upFractal =(select max(upFractal) from FractalStocks where scCode=? and tradeDate between ? and ? and upFractal!=0 and candleType=0 and period=2 order by tradeDate) and period=2 and candleType=0 and scCode=? and tradeDate between ? and ?")
							.setParameter(0, scCode).setParameter(1, cal.getTime()).setParameter(2, tradeDate).setParameter(3, scCode).setParameter(4, cal.getTime()).setParameter(5, tradeDate).uniqueResult();
					if(fractalMax[0] == null)
					{
						continue;
					}
					else
					{
						swingHigh = (double) fractalMax[0];
						swingHighDate = (Date) fractalMax[1];
						
						fractal.setSwingHigh(swingHigh);
						fractal.setSwingHighDate(swingHighDate);

					}
					Object[] fractalMin = (Object[]) hSession.createQuery("select min(lowFractal),min(minFractalDate) from FractalStocks where lowFractal =(select min(lowFractal) from FractalStocks where scCode=? and tradeDate between ? and ?  and lowFractal!=0 and candleType=0 and period=2 order by tradeDate) and period=2 and candleType=0 and scCode=? and tradeDate between ? and ?")
							.setParameter(0, scCode).setParameter(1, cal.getTime()).setParameter(2, tradeDate).setParameter(3, scCode).setParameter(4, cal.getTime()).setParameter(5, tradeDate).uniqueResult();
					if(fractalMin[0] == null)
					{
						continue;
					}
					else
					{
						swingLow = (double) fractalMin[0];
						swingLowDate = (Date) fractalMin[1];

						fractal.setSwingLow(swingLow);
						fractal.setSwingLowDate(swingLowDate);
					}
					if(swingHighDate.before(swingLowDate))
					{
						fractal.setSwingTrend("Low");
						tcfl1 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.382)).floatValue(), 3);
						tcfl2 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.5)).floatValue(), 3);
						tcfl3 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.618)).floatValue(), 3);
					}
					else if(swingLowDate.before(swingHighDate))
					{
						fractal.setSwingTrend("High");
						tcfl1 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.382)).floatValue(), 3);
						tcfl2 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.5)).floatValue(), 3);
						tcfl3 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.618)).floatValue(), 3);
					}
					fractal.setTcfl1(tcfl1);
					fractal.setTcfl2(tcfl2);
					fractal.setTcfl3(tcfl3);
					//System.out.println("update weekly swing for mutualFundid="+mfId+" dated="+tradeDate);
					hSession.saveOrUpdate(fractal);
				}
				tx.commit();
			}			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	
	}
}
