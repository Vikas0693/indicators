package com.stocks.test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Holidays;
import com.stocks.dao.Stocks;

public class MissingStocks {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List stocksList=hSession.createQuery("from Stocks group by scCode").list();
		
			List<String> missingDates=new LinkedList<String>();
			int count=0;
			Iterator itr=stocksList.iterator();
			String all_names="";
			while(itr.hasNext())
			{
				count=0;
				Stocks stocks=(Stocks)itr.next();
				//System.out.println("scCode="+scCode+"-----------------------------------------------------------");
				List<Date> tradingDateList=hSession.createQuery("select tradeDate from NavS where scCode=? order by tradeDate").setParameter(0, stocks.getScCode()).list();
				System.out.println("tradingDateList.size()="+tradingDateList.size());
				if(tradingDateList!=null && tradingDateList.size()>0)
				{
					Date missingDate=null;
					Date previousDate=(Date)tradingDateList.get(0);
					String scName=stocks.getScName();
				//	all_names+=stocks.getScName()+"-"+stocks.getScCode()+"\n";
					
					for(int i=1;i<tradingDateList.size();i++)
					{
						
						Date currentDate=(Date)tradingDateList.get(i);
						//Date currentDate=tradingDateList.get(i);	
						//System.out.println(new SimpleDateFormat("dd-MM-yyyy").format(currentDate)+"   ");
						long diffOfDays=((currentDate.getTime()-previousDate.getTime())/(1000*60*60*24))-1;
						if(diffOfDays==0)
						{
							previousDate=currentDate;
							//System.out.println("Difference is 0 day");
							continue;
						}						
						else
						{
							for(int j=0;j<diffOfDays;j++)
							{
								Calendar cal=Calendar.getInstance();
								cal.setTime(previousDate);
								cal.add(Calendar.DAY_OF_MONTH,1);
								previousDate=cal.getTime();
							
								if(!new SimpleDateFormat("EEEEE").format(previousDate).equalsIgnoreCase("Saturday") && !new SimpleDateFormat("EEEEE").format(previousDate).equalsIgnoreCase("Sunday"))
								{
									Holidays holidays=(Holidays)hSession.createQuery("from Holidays where holiday=?").setParameter(0,previousDate).uniqueResult();
									if(holidays==null)
									{
										//System.out.println("MissedDate for mutualFundId="+mutualFundId+" on ="+previousDate);
										
										/*String miss=new SimpleDateFormat("yyyy-MM-dd").format(previousDate);
										if(missingDates.contains(miss))
										{
											System.out.println("contains missing date");
											continue;
										}
										else*/
										{
											//missingDates.add(new SimpleDateFormat("yyyy-MM-dd").format(previousDate));
											
											count++;
											//System.out.println("MissedDate for  ="+previousDate+"  SCNAME="+scName+"  count="+count);
											missingDate=previousDate;
										}
									}
									else
									{
										//System.out.println("Holiday on ="+previousDate);
									}
								}
								else
								{
									//System.out.println("SAT/SUN on ="+previousDate);
								}							
							}
							previousDate=currentDate;
							//System.out.println(mutualFundId+"\t\t"+currentDate);
						}
					}
					System.out.println("scCode="+stocks.getScCode()+"  scName="+stocks.getScName()+" count="+count+" lastMissedDate="+previousDate);
				}
			}
			System.out.println(all_names);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		
	}

}
