package com.stocks.test;

import org.hibernate.Session;

import com.stocks.util.HibernateBridge;

public class HibernateStocksSession {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				Session hSession = null;
				try
				{
					hSession = HibernateBridge.getSessionFactory().openSession();
					if(hSession.isConnected())
						System.out.println("true");
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				finally
				{
					System.out.println("closed the session");
					hSession.close();
				}
	}

}
