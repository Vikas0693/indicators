package com.stocks.dao;

import java.io.Serializable;
import java.util.Date;

public class Stocks implements Serializable {

	int scCode;	
	String scName;
	String scGroup;
	String scType;
	String isinCode;
	
	
	public int getScCode() {
		return scCode;
	}
	public void setScCode(int scCode) {
		this.scCode = scCode;
	}
	
	public String getScName() {
		return scName;
	}
	public void setScName(String scName) {
		this.scName = scName;
	}
	public String getScGroup() {
		return scGroup;
	}
	public void setScGroup(String scGroup) {
		this.scGroup = scGroup;
	}
	public String getScType() {
		return scType;
	}
	public void setScType(String scType) {
		this.scType = scType;
	}
	public String getIsinCode() {
		return isinCode;
	}
	public void setIsinCode(String isinCode) {
		this.isinCode = isinCode;
	}
}
