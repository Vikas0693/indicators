package com.stocks.dao;

import java.io.Serializable;
import java.util.Date;

public class EveryDayCandleStocks implements Serializable{

	
	private int scCode;		// replace symbol
	private int candleType;		// 1 for daily and 0 for for weekly i.e on Friday
	private Date tradeDate;

	private Double open;
	private Double high;
	private Double close;
	private Double low;

	private double ema20;
	private double ema50;
	private double ema200;
	private float macd;
	private float macdSignal;
	private float macdHistogram;

	private double upperBand;
	private double middleBand;
	private double lowerBand;
	private float rsi;
	private float psar;
	private float adx;
	
	private double atr;

	public int getScCode()
	{
		return scCode;
	}

	public void setScCode(int scCode)
	{
		this.scCode = scCode;
	}

	public int getCandleType()
	{
		return candleType;
	}

	public void setCandleType(int candleType)
	{
		this.candleType = candleType;
	}

	public double getClose()
	{
		return close;
	}

	public void setClose(double close)
	{
		this.close= close;
	}

	public Double getOpen()
	{
		return open;
	}

	public void setOpen(Double open)
	{
		this.open = open;
	}

	public Double getHigh()
	{
		return high;
	}

	public void setHigh(Double high)
	{
		this.high = high;
	}

	public Double getLow()
	{
		return low;
	}

	public void setLow(Double low)
	{
		this.low = low;
	}

	public Date getTradeDate()
	{
		return tradeDate;
	}

	public void setTradeDate(Date tradeDate)
	{
		this.tradeDate = tradeDate;
	}

	public double getEma20()
	{
		return ema20;
	}

	public void setEma20(double ema20)
	{
		this.ema20 = ema20;
	}

	public double getEma50()
	{
		return ema50;
	}

	public void setEma50(double ema50)
	{
		this.ema50 = ema50;
	}

	public double getEma200()
	{
		return ema200;
	}

	public void setEma200(double ema200)
	{
		this.ema200 = ema200;
	}

	public float getMacd()
	{
		return macd;
	}

	public void setMacd(float macd)
	{
		this.macd = macd;
	}

	public float getMacdSignal()
	{
		return macdSignal;
	}

	public void setMacdSignal(float macdSignal)
	{
		this.macdSignal = macdSignal;
	}

	public float getMacdHistogram()
	{
		return macdHistogram;
	}

	public void setMacdHistogram(float macdHistogram)
	{
		this.macdHistogram = macdHistogram;
	}

	public double getUpperBand()
	{
		return upperBand;
	}

	public void setUpperBand(double upperBand)
	{
		this.upperBand = upperBand;
	}

	public double getMiddleBand()
	{
		return middleBand;
	}

	public void setMiddleBand(double middleBand)
	{
		this.middleBand = middleBand;
	}

	public double getLowerBand()
	{
		return lowerBand;
	}

	public void setLowerBand(double lowerBand)
	{
		this.lowerBand = lowerBand;
	}

	public float getRsi()
	{
		return rsi;
	}

	public void setRsi(float rsi)
	{
		this.rsi = rsi;
	}

	public float getPsar()
	{
		return psar;
	}

	public void setPsar(float psar)
	{
		this.psar = psar;
	}

	public float getAdx()
	{
		return adx;
	}

	public void setAdx(float adx)
	{
		this.adx = adx;
	}

	public double getAtr()
	{
		return atr;
	}

	public void setAtr(double atr)
	{
		this.atr = atr;
	}
}
