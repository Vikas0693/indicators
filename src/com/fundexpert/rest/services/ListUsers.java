package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import net.sf.json.JSONArray;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fundexpert.controller.ListUsersController;
import com.fundexpert.controller.UserRiskProfileController;
import com.fundexpert.dao.User;

@Path("/user/list")
public class ListUsers 
{
	@POST
	/*@Path("/")*/
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response apiDocREST(InputStream incomingData, @Context HttpServletRequest request) throws JSONException 
	{
		JSONObject jobject=new JSONObject();
		String sessionId=null;
		String appId=null;
		String ipAddress=null;
		Long consumerId=null;
		List<User> list=new ArrayList<User>();
		StringBuilder builder=new StringBuilder();
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			while ((line=br.readLine())!=null){
				builder.append(line);
				// System.out.print(line+",  "+builder.toString());
			}
			
			JSONObject jObject  = new JSONObject(builder.toString());
		    JSONArray jsonArray = new JSONArray();
			
		    Map<String,String> map = new HashMap<String,String>();
		    Iterator iter = jObject.keys();
		    while(iter.hasNext()){
		        String key = (String)iter.next();
		        String value = jObject.getString(key);
		        map.put(key,value);
		    }
		    // System.out.println("Map: "+map+", "+map.get("appId"));
		    
		    appId= map.get("appId");
		    sessionId = map.get("sessionId");
		    ipAddress=request.getRemoteAddr();
		    //ipAddress="192.168.1.15";
		    
		    UserRiskProfileController ctrll=new UserRiskProfileController();
		    consumerId=ctrll.getClient(appId, ipAddress);
		    System.out.println("Consumer ID: "+consumerId);
		    ListUsersController ctrl=new ListUsersController();
		    if(consumerId!=null)
		    {
		    	list=ctrl.getList(appId, sessionId);
		    	if(list.size()!=0)
		    	{
		    		for(User user:list)
		    		{
		    			JSONObject detailsJson = new JSONObject();
		    			detailsJson.put("userId", user.getUserId());
		    			detailsJson.put("riskAppetite", user.getRiskAppetite());
		    			jsonArray.add(detailsJson);
		    			//System.out.println("user ID: "+id);
			    		//jobject.put("userId", id);
		    		}
		    		jobject.put("user", jsonArray);
					jobject.put("success", true);
					System.out.println("Login Service invoked Successfully.");
		    	}
		    	else{
					jobject.put("success", false);
					jobject.put("message", "Invalid Session ID");
					System.out.println("List User Service not invoked Successfully.");
				}
		    }else{
				jobject.put("success", false);
				jobject.put("message", "Invalid secret key or appId");
				System.out.println("List User not invoked Successfully.");
			}
			
		} catch (Exception e) {
			System.out.println("Error Parsing: - ");
		}
		System.out.println("Data Received: " + jobject.toString());
		
		/*for(int i=0;i<list.size();i++)
		{
			jobject.put("userId"+i, list.get(i));
		}
		jobject.put("status",true);*/
		
		// return HTTP response 200 in case of success
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService(InputStream incomingData) {
		String result = "APIDocumentationForFE Successfully started..";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
}