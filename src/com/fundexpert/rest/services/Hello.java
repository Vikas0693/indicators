package com.fundexpert.rest.services;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

// Plain old Java Object it does not extend as class or implements
// an interface

// The class registers its methods for the HTTP GET request using the @GET annotation.
// Using the @Produces annotation, it defines that it can deliver several MIME types,
// text, XML and HTML.

// The browser requests per default the HTML MIME type.

//Sets the path to base URL + /hello
@Path("/hello")
public class Hello {

    // This method is called if TEXT_PLAIN is request
    @GET
	@Path("/textPlain")
    @Produces(MediaType.TEXT_PLAIN)
    public String sayPlainTextHello(@QueryParam("FirstName")String firstName, @QueryParam("LastName")String lastName) {
        return "Hello "+firstName + " "+lastName;
    }
    		// Url To hit for this function= "http://localhost:8080/APIDocumentationForFE/rest/hello?FirstName=priya&LastName=sharma "

    // This method is called if XML is request
    @GET
	@Path("/textXML")
    @Produces(MediaType.TEXT_XML)
    public String sayXMLHello() {
        return "<?xml version=\"1.0\"?>" + "<hello> Hello FundExpert! " + "</hello>";
    }

    // This method is called if HTML is request
    @GET
	@Path("/textHTML")
    @Produces(MediaType.TEXT_HTML)
    public String sayHtmlHello(@Context HttpHeaders httpHeaders, @QueryParam("FirstName")String firstName, @QueryParam("LastName")String lastName) {
        return "<html> " + "<title>" + "Hello " +firstName+ "</title>"
                + "<body><h1>" + "Hello " + firstName+ ",  "+ httpHeaders.getRequestHeaders().get("TokenId")+"</body></h1>" + "</html> ";
    }
    
    @PathParam("{name}")
	public String sayWelcome(@PathParam("name") String name)
	{
		String welcome="<?xml version='1.0' ?>"
					+"<welcome> Welcome to "+name+ " </welcome>";
		return welcome;
	}
}