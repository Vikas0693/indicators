package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONObject;

import com.fundexpert.controller.RegisterUserController;
import com.fundexpert.controller.UserRiskProfileController;
import com.fundexpert.pojo.ConsumerDetails;

@Path("/user/create")
public class RegisterUser 
{
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response apiDocREST(InputStream incomingData, @Context HttpServletRequest request) 
	{
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String sessionId=null;
		String appId=null;
		Long consumerId=null;
		String userId = null;
		String ipAddress=null;
		ConsumerDetails cd=new ConsumerDetails();
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			while ((line=br.readLine())!=null){
				builder.append(line);
			} 
			JSONObject jsonObject=new JSONObject(builder.toString());
			Map<String,String> map = new HashMap<String,String>();
		    Iterator iter = jsonObject.keys();
		    while(iter.hasNext()){
		        String key = (String)iter.next();
		        String value = jsonObject.getString(key);
		        map.put(key,value);
		    }
			
		    appId= map.get("appId");
		    sessionId = map.get("sessionId");
		    ipAddress=request.getRemoteAddr();
		   // ipAddress="192.168.1.15";
		    
		    UserRiskProfileController ctrl=new UserRiskProfileController();
		    consumerId=ctrl.getClient(appId, ipAddress);
		    
		    userId=ctrl.getSessionClient(ipAddress, consumerId, sessionId);
		     
		    //userId=ctrl.checkUserLogin(appId, sessionId, ipAddress);
		    if(userId!=null)
		    {
		    	jobject.put("userId", userId);
		    	jobject.put("status", true);
				System.out.println("Register User Service invoked Successfully.");
		    }else{
				jobject.put("success", false);
				jobject.put("message", "Invalid secret key or appId");
				System.out.println("Register User not invoked Successfully.");
			}
		    
		} catch (Exception e) {
			System.out.println("Error Parsing: - ");
		}
		System.out.println("Data Received: " + builder.toString());
		System.out.println("Login Service invoked Successfully.");
		// return HTTP response 200 in case of success
		return Response.status(200).entity(jobject.toString()).build();
	}
}