package com.fundexpert.rest.services.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.codehaus.jettison.json.JSONObject;

import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.RegisterUserController;
import com.fundexpert.controller.UserRiskProfileController;

public class UserRiskProfileClient 
{
	public static void main(String[] args) 
	{
		String input = "";
		try 
		{
			input="{\"appId\": \"qwertyuiop12345\", \"sessionId\": \"lksjdafkjslfkjakfjsa\", \"userId\": \"user1\",\"riskAppetite\": \"01\"}";
			JSONObject jsonObject=new JSONObject(input);
			System.out.println("JSON Object: "+jsonObject);
			
			try {
				URL url = new URL("http://localhost:8080/APIDocumentationForFE/rest/user/riskprofile/update");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(5000);
				con.setReadTimeout(5000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while(line!=null)
				{
					System.out.println(line);
				}
				
				System.out.println("ApiDOc REST Service for LOGIN Invoked Successfully..");
				br.close();
			} catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST LOGIN Service");
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}