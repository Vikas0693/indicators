package com.fundexpert.rest.services.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.codehaus.jettison.json.JSONObject;

import com.fundexpert.controller.RegisterUserController;

public class RegisterUserClient 
{
	public static void main(String[] args)
	{
		//String appId="qwertyuiop12345";	String secretKey="mnbvcxz09876";	String ipAddress="";
		String input = "{\"appId\": \"qwertyuiop12345\",\"sessionId\": \"3a728ba3\"}";
		try {
			JSONObject jsonObject=new JSONObject(input);
			System.out.println("JSON Object: "+jsonObject);
			try {
				
				URL url=new URL("http://localhost:8080/APIDocumentationForFE/rest/user/create");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(5000);
				con.setReadTimeout(5000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while(line!=null){
					System.out.println(line);
					line=br.readLine();
				}
				System.out.println("ApiDOc REST Service for User LOGIN Invoked Successfully..");
				br.close();
			} catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST USER LOGIN Service");
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
