package com.fundexpert.rest.services.clients;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge; 
import com.fundexpert.dao.Sessionn;
public class SessionClient 
{
	public String getSessionClient(String ipAddress, Long consumerId)
	{
		Session session=HibernateBridge.getSessionFactory().openSession();
		try{
			List<Sessionn> list=session.createQuery("from Sessionn where ipAddress=? and consumerId=?").setString(0, ipAddress).setLong(1, consumerId).list();
			
			if(list.size()!=0)
			{		
					Iterator<Sessionn> it=list.iterator();
					while(it.hasNext())
					{
						Sessionn s =it.next();
						Calendar lastUsedCal=Calendar.getInstance();
						lastUsedCal.setTime(s.getLastUsedOn());
						
						Calendar nowCal=Calendar.getInstance();
						
						if((lastUsedCal.get(Calendar.MINUTE)-nowCal.get(Calendar.MINUTE))< 1000*60*15)
						{
							return s.getSessionId();
						}
					}
			}
			else
			{
				Sessionn s=new Sessionn();
				s.setConsumerId(consumerId);
				
				Calendar nowCal=Calendar.getInstance();
				s.setCreatedOn(nowCal.getTime());
				s.setIpAddress(ipAddress);
				s.setSessionId("");
				s.setLastUsedOn(nowCal.getTime());
				
				Transaction tx=session.beginTransaction();
				session.save(s);
				tx.commit();
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return null;
	}
}