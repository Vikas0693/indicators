package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fundexpert.controller.LoginController;
@Path("/consumer/login")
public class Login 
{
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnSessionId(InputStream incomingData, @Context HttpServletRequest request)
	{
		LoginController ctrl=new LoginController();
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		Long consumerId=null;
		String sessionId=null;
		String appId=null;
		String secretKey = null;
		String ipAddress=null;
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			JSONObject jsonObject=new JSONObject(builder.toString());
			Map<String,String> map = new HashMap<String,String>();
		    Iterator iter = jsonObject.keys();
		    while(iter.hasNext()){
		        String key = (String)iter.next();
		        String value = jsonObject.getString(key);
		        map.put(key,value);
		    }
			
		    appId= map.get("appId");
		    secretKey = map.get("secretKey");
		    ipAddress=request.getRemoteAddr();
		    // ipAddress="192.168.1.15";
		    
			consumerId=ctrl.checkLoginDetails(appId, secretKey, ipAddress);
			System.out.println("Consumer ID: "+consumerId);
			sessionId=ctrl.getSessionClient(ipAddress, consumerId);
			System.out.println("Session ID:"+sessionId);
			if(consumerId!=null && sessionId!=null){
				jobject.put("sessionId", sessionId);
				jobject.put("success", true);
				System.out.println("Login Service invoked Successfully.");
			}else{
				jobject.put("success", false);
				jobject.put("message", "Invalid secret key or appId");
				System.out.println("Login Service not invoked Successfully.");
			}
			
		} catch (Exception e) {
			System.out.println("Error Parsing: - ");
		}
		System.out.println("Data Received: " + jobject.toString());
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService(InputStream incomingData) {
		String result = "APIDocumentationForFE Successfully started..";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
}