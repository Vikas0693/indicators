package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.UserRiskProfileController;

@Path("/user/riskprofile/update")
public class UpdateRiskProfile 
{

	@POST
	/*@Path("/")*/
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response apiDocREST(InputStream incomingData, @Context HttpServletRequest request)throws JSONException 
	{
		JSONObject jobject=new JSONObject();
		Long consumerId=null;
		String sessionId=null;
		String appId=null;
		String userId = null;
		String ipAddress=null;
		int riskAppetite=0;
		StringBuilder builder=new StringBuilder();
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			
		    JSONObject jObject  = new JSONObject(builder.toString());

		    Map<String,String> map = new HashMap<String,String>();
		    Iterator<?> iter = jObject.keys();
		    while(iter.hasNext()){
		        String key = (String)iter.next();
		        String value = jObject.getString(key);
		        map.put(key,value);
		    }
		    
		    ipAddress = request.getRemoteAddr();		
		    
		    appId= map.get("appId");
		    sessionId = map.get("sessionId");
		    userId=map.get("userId");
		    riskAppetite=Integer.parseInt(map.get("riskAppetite"));
		    // ipAddress="192.168.1.15";
			
		    UserRiskProfileController ctrl=new UserRiskProfileController();
		  //  consumerId=ctrl.getClient(appId);
		   // sessionId=ctrl.getSessionClient(ipAddress, consumerId);
		    jobject.put("status", true);
		    jobject.put("status", false);
		    
		} catch (Exception e) {
			System.out.println("Error Parsing: - ");
		}
		System.out.println("Data Received: " + builder.toString());
		//System.out.println("Session Id: "+sessionId +", and ConsumerId: "+consumerId);
		System.out.println("Login Service invoked Successfully.");
		// return HTTP response 200 in case of success
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService(InputStream incomingData)
	{
		String result = "APIDocumentationForFE Successfully started..";
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
}   