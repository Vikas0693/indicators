package com.fundexpert.test;

import java.util.List;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.MutualFund;

public class EnterStrategies {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			MutualFund mf=(MutualFund)hSession.createQuery("from MutualFund where id=2").setMaxResults(1).uniqueResult();
			System.out.println(mf.getExitStrategies().size());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	}

}
