package com.fundexpert.test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.controller.TALibEngine;
import com.fundexpert.dao.Nav;

public class TALibIndicatorsSaving
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();

			// to get dummy psar based on some nav from Nav table
			/*CurrentDayCandle cdc = (CurrentDayCandle) hSession.createQuery("from CurrentDayCandle where mutualFundId=?").setParameter(0, 1l).uniqueResult();
			TALibEngine engine = new TALibEngine();
			Calendar tillDate = Calendar.getInstance();
			tillDate.setTime(new SimpleDateFormat("dd-MM-yyyy").parse("22-03-2017"));
			List till = hSession.createQuery("from Nav where mutualfundId=? and tradeDate<? order by tradeDate desc").setParameter(0, 1l).setParameter(1, tillDate.getTime()).setMaxResults(200).list();
			System.out.println("Size=" + till.size());
			Date from = ((Nav) till.get(till.size() - 1)).getTradeDate();
			Calendar fromDate = Calendar.getInstance();
			fromDate.setTime(from);
			float psarCurrent = engine.calculatePsarCustom(1l, fromDate, tillDate, .02, .2);
			System.out.println("psarCurrent=" + psarCurrent);*/


			// to get dummy bBands on some nav(for 200 days)
			/*CurrentDayCandle cdc = (CurrentDayCandle) hSession.createQuery("from CurrentDayCandle where mutualFundId=?").setParameter(0, 1l).uniqueResult();
			TALibEngine engine = new TALibEngine();
			Calendar tillDate = Calendar.getInstance();
			tillDate.setTime(new SimpleDateFormat("dd-MM-yyyy").parse("22-03-2017"));
			List till = hSession.createQuery("from Nav where mutualfundId=? and tradeDate<? order by tradeDate desc").setParameter(0, 1l).setParameter(1, tillDate.getTime()).setMaxResults(200).list();
			System.out.println("Size=" + till.size());
			Date from = ((Nav) till.get(till.size() - 1)).getTradeDate();
			Calendar fromDate = Calendar.getInstance();
			fromDate.setTime(from);
			engine.calculateBBandsCustom(1l, fromDate, tillDate, 20, 2.0, 2.0);*/


			// to get dummy values for atr in which NOD(number of days for nav's) is hardcoded in calculateAtr()
			/*CurrentDayCandle cdc = (CurrentDayCandle) hSession.createQuery("from CurrentDayCandle where mutualFundId=?").setParameter(0, 1l).uniqueResult();
			TALibEngine engine = new TALibEngine();
			Calendar tillDate = Calendar.getInstance();
			tillDate.setTime(new SimpleDateFormat("dd-MM-yyyy").parse("22-03-2017"));
			List till = hSession.createQuery("from Nav where mutualfundId=? and tradeDate<? order by tradeDate desc").setParameter(0, 1l).setParameter(1, tillDate.getTime()).setMaxResults(200).list();
			System.out.println("Size=" + till.size());
			Date from = ((Nav) till.get(till.size() - 1)).getTradeDate();
			Calendar fromDate = Calendar.getInstance();
			fromDate.setTime(from);
			double atr = engine.calculateAtr(1l, fromDate, tillDate, 14);
			System.out.print("AtrCustom=" + atr);*/

			// to get dummy value for macd using nav for NOD days so if optinPeriods is 14 then we have to pass date for 200 days before tillDate so that NOD=214 in calculateMacdCustom function
			/*CurrentDayCandle cdc = (CurrentDayCandle) hSession.createQuery("from CurrentDayCandle where mutualFundId=?").setParameter(0, 1l).uniqueResult();
			TALibEngine engine = new TALibEngine();
			Calendar tillDate = Calendar.getInstance();
			tillDate.setTime(new SimpleDateFormat("dd-MM-yyyy").parse("22-03-2017"));
			List till = hSession.createQuery("from Nav where mutualfundId=? and tradeDate<? order by tradeDate desc").setParameter(0, 1l).setParameter(1, tillDate.getTime()).setMaxResults(200).list();
			System.out.println("Size=" + till.size());
			Date from = ((Nav) till.get(till.size() - 1)).getTradeDate();
			Calendar fromDate = Calendar.getInstance();
			fromDate.setTime(from);
			float macd = engine.calculateMacdCustom(1l, fromDate, tillDate, 12, 26, 9);
			System.out.println("MacdCustom=" + macd);*/

			// to get dummy value for macdSignal using nav for NOD days so if optinPeriods is 14 then we have to pass date for 200 days before tillDate so that NOD=214 in calculateMacdCustom function
			/*	CurrentDayCandle cdc = (CurrentDayCandle) hSession.createQuery("from CurrentDayCandle where mutualFundId=?").setParameter(0, 1l).uniqueResult();
				TALibEngine engine = new TALibEngine();
				Calendar tillDate = Calendar.getInstance();
				tillDate.setTime(new SimpleDateFormat("dd-MM-yyyy").parse("22-03-2017"));
				List till = hSession.createQuery("from Nav where mutualfundId=? and tradeDate<? order by tradeDate desc").setParameter(0, 1l).setParameter(1, tillDate.getTime()).setMaxResults(200).list();
				System.out.println("Size=" + till.size());
				Date from = ((Nav) till.get(till.size() - 1)).getTradeDate();
				Calendar fromDate = Calendar.getInstance();
				fromDate.setTime(from);
				float macd = engine.calculateMacdSignalCustom(1l, fromDate, tillDate, 12, 26, 9);
				System.out.println("MacdSignalCustom=" + macd);*/

			// to get dummy value for macd using nav for NOD days so if optinPeriods is 14 then we have to pass date for 200 days before tillDate so that NOD=214 in calculateMacdCustom function
			/*CurrentDayCandle cdc = (CurrentDayCandle) hSession.createQuery("from CurrentDayCandle where mutualFundId=?").setParameter(0, 1l).uniqueResult();
			TALibEngine engine = new TALibEngine();
			Calendar tillDate = Calendar.getInstance();
			tillDate.setTime(new SimpleDateFormat("dd-MM-yyyy").parse("22-03-2017"));
			List till = hSession.createQuery("from Nav where mutualfundId=? and tradeDate<? order by tradeDate desc").setParameter(0, 1l).setParameter(1, tillDate.getTime()).setMaxResults(200).list();
			System.out.println("Size=" + till.size());
			Date from = ((Nav) till.get(till.size() - 1)).getTradeDate();
			Calendar fromDate = Calendar.getInstance();
			fromDate.setTime(from);
			float macdHistogram = engine.calculateMacdHistogramCustom(1l, fromDate, tillDate, 12, 26, 9);
			System.out.println("MacdHistogramCustom=" + macdHistogram);*/


			// to get dummy values of RSI using nav for NOD=days between fromDate,toDate + optInPeriod
			/*CurrentDayCandle cdc = (CurrentDayCandle) hSession.createQuery("from CurrentDayCandle where mutualFundId=?").setParameter(0, 1l).uniqueResult();
			TALibEngine engine = new TALibEngine();
			Calendar tillDate = Calendar.getInstance();
			tillDate.setTime(new SimpleDateFormat("dd-MM-yyyy").parse("22-03-2017"));
			List till = hSession.createQuery("from Nav where mutualfundId=? and tradeDate<? order by tradeDate desc").setParameter(0, 1l).setParameter(1, tillDate.getTime()).setMaxResults(200).list();
			System.out.println("Size=" + till.size());
			Date from = ((Nav) till.get(till.size() - 1)).getTradeDate();
			Calendar fromDate = Calendar.getInstance();
			fromDate.setTime(from);
			float rsi = engine.calculateRSICustom(1l, fromDate, tillDate, 14);
			System.out.println("RSICustom=" + rsi);*/




		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}
}
