package com.fundexpert.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Holidays;

public class HolidaysTableUpdationTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			Date date=new SimpleDateFormat("dd-MMM-yyyy").parse("19-Oct-2017");
			String reason="Diwali-Laxmi Pujan*";
			Holidays holiday=new Holidays();
			holiday.setHoliday(date);
			holiday.setReason(reason);
			
			hSession.save(holiday);
			tx.commit();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}

}
