package com.fundexpert.test;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;

public class HibernateTest
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		Session hSession = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			if(hSession.isConnected())
				System.out.println("true");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	}


}
