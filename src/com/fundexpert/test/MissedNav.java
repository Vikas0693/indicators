package com.fundexpert.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.controller.MutualFundController;

public class MissedNav {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator itr=mfList.iterator();
			int count=1;
			while(itr.hasNext())
			{
				Object object[]=(Object[])itr.next();
				Long mfId=(Long)object[0];
				String name=(String)object[1];
				Date lastDate=(Date)hSession.createQuery("select max(tradeDate) from EveryDayCandle where mutualfundId=?").setParameter(0, mfId).setMaxResults(1).uniqueResult();
				if(lastDate==null)
					continue;
				//currentDate is the date of begining date of excel from navIndia
				Date currentDate=new SimpleDateFormat("yyyy-MM-dd").parse("2017-07-01");
				long diff=(currentDate.getTime()-lastDate.getTime())/(1000*60*60*24);
				if(diff>60)
				{
					System.out.println(lastDate+"  mfId="+mfId+" "+name);
				}			
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}				
	}
}
