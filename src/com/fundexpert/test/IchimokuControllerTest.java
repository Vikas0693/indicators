package com.fundexpert.test;

import com.fundexpert.controller.IchimokuController;

public class IchimokuControllerTest
{
	public static void main(String[] args)
	{
		try
		{
			long start=System.nanoTime();
			IchimokuController ichimokuController = new IchimokuController();
			
			//ichimokuController.persistDailyIchimokuFromEveryDay();
			Long min=2l,max=80l;
			
			//ichimokuController.updateTenkanSen(min,max);
			//ichimokuController.updateKijunSen(min,max);
			//ichimokuController.updateSSB(min,max);
			//ichimokuController.updateSSA(min,max);
			//ichimokuController.updateKumo(min,max);
			
			/*long end=System.nanoTime();
			double timeTaken=end-start;
			System.out.println("Time for daily ichimoku="+timeTaken/60000000000.0);*/
			
			
			// System.out.println(BigDecimal.valueOf(2.3224).setScale(2, BigDecimal.ROUND_CEILING));

			// ichimokuController.updateCurrentIchimoku();
			// ichimokuController.updateIchimokuAfterDate("2017-06-14");
			
			ichimokuController.persistWeeklyIchimokuFromEveryDay(min,max);
			ichimokuController.updateWeeklyTenkanSen(min,max);
			ichimokuController.updateWeeklyKijunSen(min,max);
			ichimokuController.updateWeeklySSB(min,max);
			ichimokuController.updateWeeklySSA(min,max);
			ichimokuController.updateWeeklyKumo(min,max);
			// ichimokuController.updateWeeklyIchimokuAfterDate("2017-06-23");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
