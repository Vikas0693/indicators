package com.fundexpert.test;


import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;

public class TestHibernate
{
	public static void main(String[] args)
	{
		Session s = HibernateBridge.getSessionFactory().openSession();
		System.out.println("Session Opened.");
	}
}