package com.fundexpert.test;

import com.fundexpert.controller.FlatPatternsController;

public class FlatPatternsControllerTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FlatPatternsController fpc=new FlatPatternsController();
		
		try
		{
			//for daily candle of SSB use flatDays=20
			fpc.createSSBFlatPatterns(String.valueOf(20),String.valueOf(1));
			//for daily candle of tcfl1 use flatDays=15
			fpc.createTCFL1FlatPatterns(String.valueOf(15),String.valueOf(1));
			//for daily candle of tcfl3 use flatDays=15
			fpc.createTCFL3FlatPatterns(String.valueOf(15),String.valueOf(1));
			
			//for weekly candle of ssb candles use flatDays=15
			fpc.createSSBFlatPatterns(String.valueOf(15),String.valueOf(0));
			//for weekly candle of tcfl1 use flatDays=15 and period=2 in query
			fpc.createTCFL1FlatPatterns(String.valueOf(15),String.valueOf(0));
			//for weekly candle of tcfl3 use flatDays=15 and period=2 in query
			fpc.createTCFL3FlatPatterns(String.valueOf(15),String.valueOf(0));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}
}
