package com.fundexpert.test;

import java.text.ParseException;

import com.fundexpert.controller.FractalIndicatorController;

public class FractalControllerTest
{

	public static void main(String[] args) throws ParseException, Exception
	{
		// TODO Auto-generated method stub
		FractalIndicatorController fc = new FractalIndicatorController();
		// update upFractal requires closePrice in Fractal table		
		Long start = System.nanoTime();
		
		//fc.updateFractal(min,max);
		//fc.updateUpFractal(min,max);
		//fc.updateLowFractal(min,max);
		//fc.updateSwing(min,max);
		
		Long end = System.nanoTime();
		Long time = end - start;
		Double seconds = time / 1000000000.0;
		Double minutes=seconds/60;
		System.out.println("Time Taken for Daily Fractal funds is ="+minutes);
		System.gc();
		
		Long weeklyStart=System.nanoTime();
		
		//fc.updateWeeklyFractal(min,max);
		//fc.updateWeeklyUpFractal(min,max);
		//fc.updateWeeklyLowFractal(min,max);
		//fc.updateWeeklySwing(min,max);
		 
		//fc.updateWeeklyFractalPeriod1(min,max);
		//fc.updateWeeklyUpFractalPeriod1(min,max);
		//fc.updateWeeklyLowFractalPeriod1(min,max);
		//fc.updateWeeklySwingPeriod1(min,max);
		 
		
		 Long weeklyEnd=System.nanoTime();
		 Long weeklyTime=weeklyEnd-weeklyStart;
		 Double weeklyMinutes=weeklyTime/60000000000.0;
		 System.out.println("Time taken weekly for 10 funds="+weeklyMinutes);
		 
		 System.out.println("update fractalAfterStartDate");
		
		fc.updateWeeklyFractalAfterDate();
		fc.updateWeeklySwingAfterDate();
		fc.updateWeeklyFractalPeriod1AfterDate();
		fc.updateWeeklyUpLowFractalPeriod1AfterDate();
		
	}
}
	