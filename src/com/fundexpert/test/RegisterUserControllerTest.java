package com.fundexpert.test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;

import com.fundexpert.controller.RegisterUserController;
import com.fundexpert.controller.UserRiskProfileController;
import com.fundexpert.pojo.ConsumerDetails;

public class RegisterUserControllerTest 
{
	public static void main(String[] args) throws ParseException 
	{
		String appId="qwertyuiop12345";
		String userId=null;
		String ipAddress="127.0.0.1";
		String sessionId="aad5ef31";
		Long consumerId=null;
		boolean ipHardcoded=false;
		ConsumerDetails cd=new ConsumerDetails();
		
		UserRiskProfileController ctrl=new UserRiskProfileController();
		consumerId=ctrl.getClient(appId, ipAddress);
		//ipHardcoded=cd.isIpHardcoded();
		userId=ctrl.getSessionClient(ipAddress, consumerId, sessionId);
		System.out.println("started, CID: "+consumerId+",   User ID:  "+userId);
		
		//ctrl.checkUserLogin(appId, sessionId, ipAddress);
		System.out.println("Done.");
	}
}