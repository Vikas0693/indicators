package com.fundexpert.test;

import java.net.InetAddress;
import java.net.UnknownHostException;
 
import java.text.ParseException;

import com.fundexpert.controller.LoginController; 

public class LoginControllerTest 
{
	// AppID: qwertyuiop12345, SecretKey:	mnbvcxz09876
	
	public static void main(String[] args) throws ParseException 
	{
		String appId="qwertyuiop12345";
		String secretKey="mnbvcxz09876";
		String ipAddress="127.0.0.1";
		
		//	8eb790e4,	9a3ca815, 	
		
		LoginController ctrl=new LoginController();
		Long id=ctrl.checkLoginDetails(appId, secretKey, ipAddress);
		System.out.println("Consumer ID is: "+id);
		
		String sessionId=ctrl.getSessionClient(ipAddress, id);
		System.out.println("Session Id: "+sessionId);
		
		
		/*Calendar lastUsedCal=Calendar.getInstance();
		lastUsedCal.setTime(new Date());
		Calendar nowCal=Calendar.getInstance();
		nowCal.add(Calendar.DAY_OF_MONTH, -2);
		// System.out.println(nowCal.getTime()+",    "+lastUsedCal.getTime()+",   "+new Date());
		// System.out.println(new java.util.Date()+",       "+new java.sql.Date(new java.util.Date().getTime()));
*/		
		/* GET CREDENTIALS FROM DATABASE.
		Session s=HibernateBridge.getSessionFactory().openSession();
		System.out.println("Connection Opened.");
		Consumer c=(Consumer) s.createQuery("from Consumer").uniqueResult();
		System.out.println("AK: "+c.getAppId()+", SK   "+c.getSecretKey());*/
	}
}
