package com.fundexpert.test;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Sessionn;

public class SessionnTest 
{
	
	public static void main(String[] args) throws JSONException 
	{
		String s = "{\"1\":\"sql\", \"2\":\"android\", \"3\":\"mvc\"}";
	    JSONObject jObject  = new JSONObject(s);
	    //JSONObject  menu = jObject.getJSONObject("menu");

	    Map<String,String> map = new HashMap<String,String>();
	    Iterator iter = jObject.keys();
	    while(iter.hasNext()){
	        String key = (String)iter.next();
	        String value = jObject.getString(key);
	        map.put(key,value);
	    }
	    System.out.println(map);

				
		/*
		String appId="qwertyuiop12345";
		String secretKey="mnbvcxz09876";
		String ipAddress="192.168.1.15";
		Long consumerId=1l;
		
		Calendar limitCal=Calendar.getInstance();
		Calendar nowCal=Calendar.getInstance();
		nowCal.add(Calendar.MINUTE, -15);
		System.out.println(limitCal.getTime()+", 	Now Cal before 15 minutes: "+nowCal.getTime());
		
		org.hibernate.Session  s=HibernateBridge.getSessionFactory().openSession();
		List<Sessionn> list=s.createQuery("from Sessionn").list();
		for(Sessionn ss:list){
			if((ss.getLastUsedOn()).compareTo(nowCal.getTime())>=0)
			{	
				System.out.println("Inside Loop.");
				System.out.println("loop SI: "+ss.getSessionId()+", CI "+ss.getConsumerId()+", Co: "+ss.getCreatedOn()+", LUO: "+ss.getLastUsedOn());
			}
			else
			 System.out.println("SI: "+ss.getSessionId()+", SI "+ss.getId()+", Co: "+ss.getCreatedOn()+", LUO: "+ss.getLastUsedOn());
		}
		System.out.println("First Query executed.");
		
		Sessionn ses=(Sessionn) s.createQuery("from Sessionn where ipAddress=? and consumerId=? and lastUsedOn>=? and lastUsedOn<=? order by lastUsedOn")
				.setString(0, ipAddress).setLong(1, consumerId).setDate(2, nowCal.getTime()).setDate(3, limitCal.getTime()).setMaxResults(1).uniqueResult();			
		//System.out.println(ses.getLastUsedOn()+",   "+ses.getSessionId());
		
		Sessionn sess=(Sessionn) s.createQuery("from Sessionn where ipAddress=? and consumerId=? order by id DESC")
				.setString(0, ipAddress).setLong(1, consumerId).setMaxResults(1).uniqueResult();
		System.out.println(sess.getLastUsedOn()+", "+sess.getSessionId());
	*/}
}