package com.fundexpert.test;

import java.util.Calendar;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.MutualFundSubscription;
import com.fundexpert.dao.Sessionn;

public class SessionGenerateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Session hSession=HibernateBridge.getSessionFactory().openSession();
		Transaction tx=hSession.beginTransaction();
		try{
			System.out.println("started");
			MutualFundSubscription session=new MutualFundSubscription();
			session.setMutualfundId(100l);
			session.setUserId(101l);

			Calendar cal=Calendar.getInstance();
			System.out.println(cal.getTime());

			session.setEndDate(cal.getTime());
			session.setStartDate(cal.getTime());
/*
			session.setCreatedOn(cal.getTime());*/
			
			hSession.save(session);
			tx.commit();
		}catch(Exception e){
			tx.rollback();			
			e.printStackTrace();
		}finally{
			hSession.close();
		}
		System.out.println("Done");
	}

}
