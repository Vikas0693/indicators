package com.fundexpert.pojo;

public class ConsumerDetails {

	Long consumerId;
	public Long getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(Long consumerId) {
		this.consumerId = consumerId;
	}
	public boolean isIpHardcoded() {
		return ipHardcoded;
	}
	public void setIpHardcoded(boolean ipHardcoded) {
		this.ipHardcoded = ipHardcoded;
	}
	boolean ipHardcoded;

}
