package com.fundexpert.servlet;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.HTMLLayout;
import org.apache.log4j.Layout;
import org.apache.log4j.Logger;

import com.fundexpert.controller.CustomMFEntryAlertController;
import com.fundexpert.controller.CustomMFExitAlertController;
import com.fundexpert.controller.EveryDayCandleController;
import com.fundexpert.controller.FlatPatternsController;
import com.fundexpert.controller.FractalIndicatorController;
import com.fundexpert.controller.IchimokuController;
import com.fundexpert.controller.TALibController;

/**
 * Servlet implementation class GenerateAlerts
 */
@WebServlet("/generate-alerts")
public class GenerateAlerts extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GenerateAlerts() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		/*Logger logger = Logger.getLogger(GenerateAlerts.class);
		Layout layout=new HTMLLayout();
		Appender appender;
		try
		{
			//below 2 instructions will write logs in specified directory
			appender=new FileAppender(layout, "WebContent/log4j.html",false);
			logger.addAppender(appender);
			int a=90/0;		
			//used to generate daily and weekly candles only
			EveryDayCandleController edcc=new EveryDayCandleController();
			edcc.updateEveryDayCandleAfterStartDate();
			
			//below two instructions generate fractal,upFractal,lowFractal
			FractalIndicatorController fic1=new FractalIndicatorController();	
			fic1.updateFractalAfterDate();
			fic1.updateWeeklyFractalAfterDate();
			
			//below two instructions generate swingHigh,swingLow,tcfl's
			fic1.updateSwingAfterDate();
			fic1.updateWeeklySwingAfterDate();						
			
			//below two instructions generate fractal(only upFractal and lowFractal) with period=1  
			fic1.updateWeeklyFractalPeriod1AfterDate();
			fic1.updateWeeklyUpLowFractalPeriod1AfterDate();
			
			//below three instructions are used to generate all Ichimoku indicators
			IchimokuController ic=new IchimokuController();
			ic.updateIchimokuAfterDate();
			ic.updateWeeklyIchimokuAfterDate();
			
			//below three instructions are used to generate all TalibIndicators
			TALibController tALibController=new TALibController();
			tALibController.updateEveryDayAfterDate();
			tALibController.updateWeeklyEveryDayAfterDate();
			
			//below seven instructions are used to generate fractal patterns 
			//daily patterns
			FlatPatternsController fpc=new FlatPatternsController();
			fpc.createLatestSSBFlatPatterns(String.valueOf(20), String.valueOf(1));
			fpc.createLatestTCFL1FlatPatterns(String.valueOf(15),String.valueOf(1));
			fpc.createLatestTCFL3FlatPatterns(String.valueOf(15),String.valueOf(1));		
			//weekly patterns
			fpc.createLatestSSBFlatPatterns(String.valueOf(15),String.valueOf(0));
			fpc.createLatestTCFL1FlatPatterns(String.valueOf(15),String.valueOf(0));
			fpc.createLatestTCFL3FlatPatterns(String.valueOf(15),String.valueOf(0));
			
			//below two instrctions are used to generate exit alerts
			CustomMFExitAlertController customMFExitAlert=new CustomMFExitAlertController();
			customMFExitAlert.updateAllExitAlertsAfterDate();
			
			//below two instructions are used to generate entry alerts
			CustomMFEntryAlertController customMFEntryAlert=new CustomMFEntryAlertController();
			customMFEntryAlert.updateAllEntryAlertsAfterDate();		
			
			logger.debug("Successful");
		}
		catch(Exception e)
		{
			logger.fatal("Exception Occurred : "+e.getMessage());
			logger.shutdown();
		}*/
		Logger logger=Logger.getLogger(GenerateAlerts.class.getName());
		Layout layout=new HTMLLayout();
		Appender appender;
		try
		{
			String path = GenerateAlerts.class.getClassLoader().getResource("").getPath();
			String fullPath = URLDecoder.decode(path, "UTF-8");
			System.out.println("fullPath"+fullPath);
			String pathArr[] = fullPath.split("/WEB-INF/classes/");
			String pathToLog4j=pathArr[0]+"/log4j.html";
			System.out.println("pathToLog4j="+pathToLog4j);
			//String responsePath=new File(pathToLog4j).getPath();
			//appender=new FileAppender(layout, responsePath,false);
			//logger.addAppender(appender);
			throw new Exception("/ by zero"); 
		}
		catch(Exception e)
		{
			//logger.fatal("Exception occurred : "+e.getMessage());
			//logger.shutdown();
			e.printStackTrace();
		}
	}

}
