package com.fundexpert.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.OldExcelFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.json.JSONArray;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Holidays;
import com.fundexpert.dao.Nav;

public class MutualFundController
{
	public List getMutualFundList()
	{
		Session hSession = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			// list of equity mutualFund which have nav's present in Nav table
			List<Object[]> mfList = hSession.createQuery("select id,name from MutualFund where id in (select distinct mutualfundId from Nav group by mutualfundId) and category=1 and optionType=1 and direct!=1 and isRobo=1 order by name").list();
		
			return mfList;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
	//call this function when we want to track missing navs while updating them from excel of nav india 
	public void getNavUpdatedByLatest(String fileName)
	{
		
			String path="C:/opt/excel";
					
			System.out.println("Path in mf");
			File sourceFile = new File(path);
			Session hSession = HibernateBridge.getSessionFactory().openSession();
			Transaction tx=null;
			try
			{
				int physicalNumberOfRows=0;
				// Check weather source exists and it is folder.
				System.out.println(sourceFile.getAbsolutePath());
				if(sourceFile.exists())
				{
					File file=new File(path+File.separator+fileName);
					FileInputStream stream = null;
					if(file.isFile())
					{
						try
						{
							stream = new FileInputStream(file);

							XSSFWorkbook workbook = new XSSFWorkbook(stream);
							XSSFSheet sheet = workbook.getSheetAt(0);
							physicalNumberOfRows=sheet.getPhysicalNumberOfRows()-1;
							System.out.println("File name=" + file.getName()+"physical number of rows="+physicalNumberOfRows);

							Iterator<Row> itr = sheet.iterator();
							long amfiiCode = 0;
							long previousAmfiiCode=1;
							Date navDate = null;
							double navDouble = 0;
							boolean foundNullCell=false;
							int rowValue = 0;
							boolean counterFlag=false;
							Date previousDateFromExcel=null;
							boolean missedFirstDate=false;
							boolean missedDate=false;
							
							//below if skips the first line of excel which contains header names
							//TODO
							if(itr.hasNext())
							{
								itr.next();
								rowValue++;
							}
							List mutualFundIdList=null;
							while (itr.hasNext())
							{
								Row row = itr.next();

								// System.out.println("rowValue=" + rowValue);
								tx=hSession.beginTransaction();
								Cell cell= row.getCell(3,Row.RETURN_BLANK_AS_NULL);		
								if(cell==null)
								{
									System.out.println("RowValue="+rowValue+" PhysicalRows="+physicalNumberOfRows);
									if(rowValue==physicalNumberOfRows)
									{
										foundNullCell=true;								
									}
									else
									{
										throw new Exception("Found Null/Blank Cell at row="+rowValue);								
									}
								}
								if(foundNullCell==false)
								{
									
									amfiiCode=new Double(cell.getNumericCellValue()).longValue();
									//System.out.println("AmfiiCode="+amfiiCode+"  Date="+row.getCell(4).getDateCellValue());									
									if(amfiiCode!=previousAmfiiCode)
									{
										System.out.println("***************************************************  AmfiiCode "+amfiiCode+"  ******************************************************");
										//System.out.println("AmfiiMismatch");
										mutualFundIdList=hSession.createQuery("select id from MutualFund where amfiiCode=?").setParameter(0,String.valueOf(amfiiCode)).list();
										counterFlag=false;
										previousDateFromExcel=null;
										missedFirstDate=false;										
									}
									if(mutualFundIdList!=null && missedFirstDate==false)
									{
										Date date=row.getCell(4).getDateCellValue();
										double closePrice=(double) row.getCell(6).getNumericCellValue();
										for(int j=0;j<mutualFundIdList.size();j++)
										{
											Long mutualFundId=(Long)mutualFundIdList.get(j);										
											if(counterFlag==false)
											{
												Date lastTradeDate=(Date)hSession.createQuery("select max(tradeDate) from Nav where mutualFundId=?").setParameter(0, mutualFundId).uniqueResult();
												if(date.compareTo(lastTradeDate)<0)
												{
													Nav nav = new Nav();
													nav.setMutualfundId(mutualFundId);
													nav.setTradeDate(date);
													nav.setNav(closePrice);
													System.out.println(nav.getTradeDate()+"  inside counterFlag==flag(Excel First date is smaller than database lastTradeDate) for mf="+mutualFundId);
													hSession.saveOrUpdate(nav);
													previousDateFromExcel=date;
													
													
												}
												else if(date.compareTo(lastTradeDate)>0)
												{
													long diffBetweenDates=((date.getTime()-lastTradeDate.getTime())/(1000*60*60*24))-1;
													Calendar cal=Calendar.getInstance();
													cal.setTime(lastTradeDate);
													for(int i=0;i<diffBetweenDates;i++)
													{
														cal.add(Calendar.DAY_OF_MONTH, 1);
														Holidays holiday=(Holidays)hSession.createQuery("from Holidays where holiday=?").setParameter(0, cal.getTime()).uniqueResult();
														if(holiday==null)
														{
															if((new SimpleDateFormat("EEEEE").format(cal.getTime())).equalsIgnoreCase("Saturday") || (new SimpleDateFormat("EEEEE").format(cal.getTime())).equalsIgnoreCase("Sunday"))
															{		
																//continue in for loop bcoz the date is either saturday or sunday
																//System.out.println("Sat/Sun=(There should be two consecutive line of this print)"+cal.getTime()+" for mf="+mutualFundId);
															}
															else
															{
																//this means some date gets missed from last trade date for mutualfundId in EveryDayCandle to firstTradeDate in excel 
																if(tx!=null)
																	tx.rollback();
																missedFirstDate=true;
																//throw new Exception("Some date missing from Excel for mf="+mutualFundId+" (Either not a holiday or it was not from sunday or saturday) date="+cal.getTime());
															}
														}
														else
														{
															//System.out.println("It was holiday on ********************= "+cal.getTime()+" for mf="+mutualFundId);
														}
														if(missedFirstDate==true)
														{
															//System.out.println("Data missed bcoz of missing dates between lastTradeDate="+lastTradeDate+" and startExcelDate="+date+" for mutualFundId="+mutualFundId);
															//missedFirstDate=false;
															break;
														}
													}
													if(missedFirstDate==false)
													{
														Nav nav = new Nav();
														nav.setMutualfundId(mutualFundId);
														nav.setTradeDate(date);
														nav.setNav(closePrice);
														System.out.println("date="+ date+" One time for mutualfundId="+mutualFundId);
														hSession.saveOrUpdate(nav);
														previousDateFromExcel=date;
													}
												}
												else
												{
													Nav nav = new Nav();
													nav.setMutualfundId(mutualFundId);
													nav.setTradeDate(date);
													nav.setNav(closePrice);
													System.out.println("(ExcelFirstDate == DatabaseLastTradeDate) ="+ date+" One time for mutualfundId="+mutualFundId);
													hSession.saveOrUpdate(nav);
													previousDateFromExcel=date;	
												}
												if(j==mutualFundIdList.size()-1)
												{
													counterFlag=true;
													System.out.println("Counter set to true");
												}
											}	
											else if(counterFlag==true)
											{
												System.out.println("Inside counter flag==true");
												long diff=((date.getTime()-previousDateFromExcel.getTime())/(1000*60*60*24))-1;
												//System.out.println("date="+date+"  previousDateFromExcel="+previousDateFromExcel+"diff="+diff);
												if(diff==0)
												{
													Nav nav = new Nav();
													nav.setMutualfundId(mutualFundId);
													nav.setTradeDate(date);
													nav.setNav(closePrice);
													System.out.println("date="+ date+" for mf="+mutualFundId);
													//System.out.println("Inside else{ if(diff==0)else{} } date="+date+" - previousDate"+previousDateFromExcel);
													hSession.saveOrUpdate(nav);
													if(j==mutualFundIdList.size()-1)
													{
														previousDateFromExcel=date;
													}
												}
												else
												{
													Calendar cal=Calendar.getInstance();
													cal.setTime(previousDateFromExcel);
													for(int i=0;i<diff;i++)
													{
														cal.add(Calendar.DAY_OF_MONTH, 1);
														Holidays holiday=(Holidays)hSession.createQuery("from Holidays where holiday=?").setParameter(0, cal.getTime()).uniqueResult();
														if(holiday==null)
														{
															if((new SimpleDateFormat("EEEEE").format(cal.getTime())).equalsIgnoreCase("Saturday") || (new SimpleDateFormat("EEEEE").format(cal.getTime())).equalsIgnoreCase("Sunday"))
															{		
																//continue in for loop bcoz the date is either saturday or sunday
																System.out.println("Sat/Sun=(There should be two consecutive line of this print)"+cal.getTime());
															}
															else
															{
																//this means some date gets missed from last trade date for mutualfundId in EveryDayCandle to firstTradeDate in excel 
																//if(tx!=null)
																	//tx.rollback();
																//throw new Exception("Some date missing from Excel for mf="+mutualFundId+" (Either not a holiday or it was not from sunday or saturday) date="+cal.getTime());
																missedDate=true;
															}
														}
														else
														{
															System.out.println("It was holiday on********************= "+cal.getTime()+" for mf="+mutualFundId);
														}
														if(missedDate==true)
														{
															System.out.println("Missed Date for mutualFundId="+mutualFundId+" on dated="+cal.getTime());
															if(j==mutualFundIdList.size()-1)
															{
																previousDateFromExcel=date;
															}
															//missedDate=false;
															break;
														}
													}
													if(missedDate==false)
													{
														Nav nav = new Nav();
														nav.setMutualfundId(mutualFundId);
														nav.setTradeDate(date);
														nav.setNav(closePrice);
														hSession.saveOrUpdate(nav);
														System.out.println("date="+ date+" for mf="+mutualFundId);
														if(j==mutualFundIdList.size()-1)
														{
															previousDateFromExcel=date;
														}
													}
												}
											}
										}
									}
								}		
								tx.commit();
								rowValue++;
								previousAmfiiCode=amfiiCode;
							}						
							workbook.close();
							stream.close();
							System.out.println("----------File done---------");
						}
						catch (ConstraintViolationException e)
						{
							if(stream!=null)
								stream.close();
							if(tx!=null)
								tx.rollback();
						}
						catch (NonUniqueObjectException e)
						{
							if(stream!=null)
								stream.close();
							if(tx!=null)
								tx.rollback();
						}
						catch (OldExcelFormatException e)
						{
							if(stream!=null)
								stream.close();
							e.printStackTrace();
						}
						catch (Exception e)
						{
							e.printStackTrace();
							if(stream != null)
							{
								stream.close();
							}			
						}
					}
				}else
				{
					throw new FileNotFoundException("Excel "+path+" not found");
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				hSession.close();
			}		
	}
	
	public void updateNav(String fileName)
	{
	
		String path="C:/opt/excel";
				
		System.out.println("Path in mf");
		File sourceFile = new File(path);
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Transaction tx=null;
		try
		{
			int physicalNumberOfRows=0;
			// Check weather source exists and it is folder.
			System.out.println(sourceFile.getAbsolutePath());
			if(sourceFile.exists())
			{
				File file=new File(path+File.separator+fileName);
				FileInputStream stream = null;
				if(file.isFile())
				{
					try
					{
						stream = new FileInputStream(file);
	
						XSSFWorkbook workbook = new XSSFWorkbook(stream);
						XSSFSheet sheet = workbook.getSheetAt(0);
						physicalNumberOfRows=sheet.getPhysicalNumberOfRows()-1;
						System.out.println("File name=" + file.getName()+"physical number of rows="+physicalNumberOfRows);
	
						Iterator<Row> itr = sheet.iterator();
						long amfiiCode = 0;
						long previousAmfiiCode=1;
						Date navDate = null;
						double navDouble = 0;
						boolean foundNullCell=false;
						int rowValue = 0;
						boolean counterFlag=false;
						Date previousDateFromExcel=null;
						boolean missedFirstDate=false;
						boolean missedDate=false;
						
						//below if skips the first line of excel which contains header names
						//TODO
						if(itr.hasNext())
						{
							itr.next();
							rowValue++;
						}
						List mutualFundIdList=null;
						while (itr.hasNext())
						{
							Row row = itr.next();
	
							// System.out.println("rowValue=" + rowValue);
							tx=hSession.beginTransaction();
							Cell cell= row.getCell(3,Row.RETURN_BLANK_AS_NULL);		
							if(cell==null)
							{
								System.out.println("RowValue="+rowValue+" PhysicalRows="+physicalNumberOfRows);
								if(rowValue==physicalNumberOfRows)
								{
									foundNullCell=true;								
								}
								else
								{
									throw new Exception("Found Null/Blank Cell at row="+rowValue);								
								}
							}
							if(foundNullCell==false)
							{								
								amfiiCode=new Double(cell.getNumericCellValue()).longValue();
								//System.out.println("AmfiiCode="+amfiiCode+"  Date="+row.getCell(4).getDateCellValue());									
								if(amfiiCode!=previousAmfiiCode)
								{
									System.out.println("***************************************************  AmfiiCode "+amfiiCode+"  ******************************************************");
									//System.out.println("AmfiiMismatch");
									mutualFundIdList=hSession.createQuery("select id from MutualFund where amfiiCode=?").setParameter(0,String.valueOf(amfiiCode)).list();
									previousAmfiiCode=amfiiCode;
								}
								if(mutualFundIdList!=null)
								{
									Date date=row.getCell(4).getDateCellValue();
									double closePrice=(double) row.getCell(6).getNumericCellValue();
									for(int j=0;j<mutualFundIdList.size();j++)
									{
										Long mutualFundId=(Long)mutualFundIdList.get(j);	
										Nav nav = new Nav();
										nav.setMutualfundId(mutualFundId);
										nav.setTradeDate(date);
										nav.setNav(closePrice);
										hSession.saveOrUpdate(nav);
										System.out.println("date="+ date+" for mf="+mutualFundId+" nav"+closePrice+"  countrows="+(++rowValue));
									}									
								}
							}		
							tx.commit();
							
						}						
						workbook.close();
						stream.close();
						System.out.println("----------File done---------");
					}
					catch (ConstraintViolationException e)
					{
						if(stream!=null)
							stream.close();
						if(tx!=null)
							tx.rollback();
					}
					catch (NonUniqueObjectException e)
					{
						if(stream!=null)
							stream.close();
						if(tx!=null)
							tx.rollback();
					}
					catch (OldExcelFormatException e)
					{
						if(stream!=null)
							stream.close();
						e.printStackTrace();
					}
					catch (Exception e)
					{
						e.printStackTrace();
						if(stream != null)
						{
							stream.close();
						}			
					}
				}
			}else
			{
				throw new FileNotFoundException("Excel "+path+" not found");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}		
	}
	
	public JSONArray getNavJsonArray(long mutualFundId,String sDate,Long investedAmount)
	{
		Session hSession=null;
		double unitsAllocatedInitially=0;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Nav> navList=hSession.createQuery("from Nav where mutualFundId=? and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,new SimpleDateFormat("yyyy-MM-dd").parse(sDate)).list();
			JSONArray outerJsonArray=new JSONArray();
			JSONArray innerJsonArray=null;
			if(navList!=null && navList.size()>0)
			{
				Nav n=(Nav)navList.get(0);
				unitsAllocatedInitially=((investedAmount).doubleValue())/n.getNav();
				//beolow loop makes json of [{navDate:"",amount:""},{navDate:"",amount:""}]
				
				Iterator iterate=navList.iterator();
				while(iterate.hasNext())
				{
					innerJsonArray=new JSONArray();
					Nav nav=(Nav)iterate.next();
					double amountOnEachDay=nav.getNav()*unitsAllocatedInitially;
					innerJsonArray.put(0,nav.getTradeDate());
					innerJsonArray.put(1,amountOnEachDay);
					outerJsonArray.put(innerJsonArray);
				}
				
			
			}
			System.out.println(outerJsonArray.toString(1));
			return outerJsonArray;			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
}
