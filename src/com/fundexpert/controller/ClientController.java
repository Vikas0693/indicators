package com.fundexpert.controller;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.validation.ValidationException;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge; 
import com.fundexpert.dao.Sessionn;
public class ClientController 
{
	public String getSessionClient(String ipAddress, Long consumerId)
	{
		String sessionId="";
		Session session=HibernateBridge.getSessionFactory().openSession();
		try{
			List<Sessionn> list=session.createQuery("from Sessionn where ipAddress=? and consumerId=?").setString(0, ipAddress).setLong(1, consumerId).list();
			
			if(list.size()!=0)
			{	
				Iterator<Sessionn> it=list.iterator();
				while(it.hasNext())
				{
					Sessionn s =it.next();
					Calendar lastUsedCal=Calendar.getInstance();
					lastUsedCal.setTime(s.getLastUsedOn());
					
					Calendar nowCal=Calendar.getInstance();
					
					if((lastUsedCal.get(Calendar.MINUTE)-nowCal.get(Calendar.MINUTE))< 1000*60*15)
					{
						sessionId= s.getSessionId();
					}
					else
					{
						
					}
				}
			}
		}
		catch(ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return sessionId;
	}
}