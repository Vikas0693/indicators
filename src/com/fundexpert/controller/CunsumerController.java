package com.fundexpert.controller;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Consumer;

public class CunsumerController 
{
	public Boolean checkLoginDetails(String appId, String secretKey, String ipAddress)
	{
		Session hSession=HibernateBridge.getSessionFactory().openSession();
		Consumer c=(Consumer)hSession.createQuery("from Consumer where appId=? and secretKey=?")
				.setString(0, appId).setString(1, secretKey).uniqueResult();
		
		if(c.isIpHardcoded())
		{
			if(c.getIpAddress().equals(ipAddress) && c.getSecretKey().equals(secretKey))
				return true;
		}
		else if(!c.isIpHardcoded())
		{
			if(c.getSecretKey().equals(secretKey))
				return true;
		}
		return false;
	}
}