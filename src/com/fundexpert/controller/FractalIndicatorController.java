package com.fundexpert.controller;

import java.text.ParseException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.apidoc.util.ImageLinks;
import com.fundexpert.dao.EveryDayCandle;
import com.fundexpert.dao.Fractal;

public class FractalIndicatorController
{
	// updates only closePrice in Fractal Table
	public void updateFractal(Long min,Long max)
	{
		System.out.println("Inserting closePrice into fractal");
		Session hSession = null;
		Transaction transaction = null;
		final short period = 2;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			Fractal fractal = null;
			List fiveDaysFractalList = null;
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where mutualFundId>=? and mutualFundId<=? order by mutualFundId").setParameter(0,min).setParameter(1, max).list();
			//List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where mutualFundId>? order by mutualFundId").setParameter(0, maxId).setMaxResults(10).list();
			System.out.println(mutualFundList.size());
			Iterator itr = mutualFundList.iterator();
			long mfId=0;
			EveryDayCandle edc=null;
			Runtime r=Runtime.getRuntime();
			while (itr.hasNext())
			{			
				mfId = (Long) itr.next();
				System.out.println("inserting close for "+mfId);
				List<EveryDayCandle> edcList = hSession.createQuery("from EveryDayCandle where candleType=? and mutualFundId=? order by tradeDate asc").setParameter(0, 1).setParameter(1, mfId).list();
				int size = edcList.size();
				Iterator itr1 = edcList.iterator();
			
				transaction = hSession.beginTransaction();
				while (itr1.hasNext())
				{
					edc = (EveryDayCandle) itr1.next();
					fractal = new Fractal();
					fractal.setMutualFundId(mfId);
					fractal.setCandleType(1);
					fractal.setPeriod(period);
					fractal.setTradeDate(edc.getTradeDate());
					fractal.setClosePrice(edc.getTodaysClosePrice());
					hSession.saveOrUpdate(fractal);
				}
				hSession.flush();
				transaction.commit();
				System.gc();
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(transaction != null)
			{
				transaction.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public void  updateUpFractal(Long min,Long max)
	{
		System.out.println("Updating Up Fractal");
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			Fractal fractal = null;
			List fiveDaysFractalList = null;
			//List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from Fractal where mutualFundId>? ").setParameter(0, ).list();
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from Fractal where mutualFundId>=? and mutualFundId<=? ").setParameter(0,min).setParameter(1, max).list();
			Iterator itr = mutualFundList.iterator();
			while (itr.hasNext())
			{
				long mfId = (Long) itr.next();
				List fractalList = hSession.createQuery("from Fractal where candleType=? and period=? and mutualFundId=? order by tradeDate").setParameter(0, 1).setParameter(1, 2).setParameter(2, mfId).list();
				System.out.println("updating upFractal for "+mfId);
				int size = fractalList.size();
				// System.out.println("FractalSize=" + fractalList.size());
				tx = hSession.beginTransaction();
				for (int i = 4; i < size; i++)
				{
					Fractal upFractal = null;
					fiveDaysFractalList = new ArrayList<Fractal>();
					for (int j = i - 4; j <= i; j++)
					{
						if(i - j == 2)
						{
							upFractal = (Fractal) fractalList.get(j);
						}
						fiveDaysFractalList.add((Fractal) fractalList.get(j));
					}
					Iterator itr2 = fiveDaysFractalList.iterator();
					// System.out.println("Up Fractal Date = " + upFractal.getTradeDate());
					/*while (itr2.hasNext())
					{
						Fractal f = (Fractal) itr2.next();
						System.out.println(f.getTradeDate() + "\t\t" + f.getClosePrice());
					}*/

					// flag for equal or less than neighbour values in fractal list of 5
					boolean flagForEqualOrGreaterNeighborValuesInFractal = false;
					// List<Double> fiveDaysFractalPriceList = new ArrayList<Double>();
					// int fiveDaysFractalListSize = fiveDaysFractalList.size();
					Iterator it = fiveDaysFractalList.iterator();

					double upFractalPrice = upFractal.getClosePrice();
					Date upFractalDate = upFractal.getTradeDate();
					while (it.hasNext())
					{
						Fractal fr = (Fractal) it.next();
						if(upFractalPrice == fr.getClosePrice() && upFractalDate.compareTo(fr.getTradeDate()) == 0)
						{
							continue;
						}
						if(fr.getClosePrice() >= upFractalPrice)
						{
							flagForEqualOrGreaterNeighborValuesInFractal = true;
							break;
						}
					}
					Fractal currentFractal = null;
					if(flagForEqualOrGreaterNeighborValuesInFractal == true)
					{
						Fractal previousToCurrentFractal = (Fractal) fiveDaysFractalList.get(3);
						currentFractal = (Fractal) fiveDaysFractalList.get(4);
						currentFractal.setUpFractal(previousToCurrentFractal.getUpFractal());
						currentFractal.setMaxFractalDate(previousToCurrentFractal.getMaxFractalDate());
						// System.out.println("Neighbour value is greater or equal than upFractal");
						// System.out.println(currentFractal.getUpFractal() + "\t" + currentFractal.getMaxFractalDate());
					}
					else
					{
						currentFractal = (Fractal) fiveDaysFractalList.get(4);
						currentFractal.setUpFractal(upFractal.getClosePrice());
						currentFractal.setMaxFractalDate(upFractal.getTradeDate());
						// System.out.println("UpFractal price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getUpFractal() + "Date =" + currentFractal.getMaxFractalDate());
					}
					flagForEqualOrGreaterNeighborValuesInFractal = false;

					// System.out.println("================================================");
					hSession.saveOrUpdate(currentFractal);
				}
				hSession.flush();
				tx.commit();
				System.gc();
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateLowFractal(Long min,Long max)
	{
		System.out.println("updating lowFractal ");
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			Fractal fractal = null;
			List fiveDaysFractalList = null;
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from Fractal where mutualFundId>=? and mutualFundId<=?").setParameter(0,min).setParameter(1, max).list();
			Iterator itr = mutualFundList.iterator();
			while (itr.hasNext())
			{
				long mfId = (Long) itr.next();
				System.out.println("updating low fractal for ="+mfId);
				List fractalList = hSession.createQuery("from Fractal where period=? and mutualFundId=? order by tradeDate").setParameter(0, 2).setParameter(1, mfId).list();

				int size = fractalList.size();
				// System.out.println("FractalSize=" + fractalList.size());
				tx = hSession.beginTransaction();
				for (int i = 4; i < size; i++)
				{
					Fractal lowFractal = null;
					fiveDaysFractalList = new ArrayList<Fractal>();
					for (int j = i - 4; j <= i; j++)
					{
						if(i - j == 2)
						{
							lowFractal = (Fractal) fractalList.get(j);
						}
						fiveDaysFractalList.add((Fractal) fractalList.get(j));
					}
					Iterator itr2 = fiveDaysFractalList.iterator();
					// System.out.println("Up Fractal Date = " + upFractal.getTradeDate());
					/*while (itr2.hasNext())
					{
						Fractal f = (Fractal) itr2.next();
						System.out.println(f.getTradeDate() + "\t\t" + f.getClosePrice());
					}*/
					// flag for equal or less than neighbour values in fractal list of 5
					boolean flagForEqualOrLessNeighborValuesInFractal = false;
					Iterator it = fiveDaysFractalList.iterator();

					double lowFractalPrice = lowFractal.getClosePrice();
					Date lowFractalDate = lowFractal.getTradeDate();
					while (it.hasNext())
					{
						Fractal fr = (Fractal) it.next();
						if(lowFractalPrice == fr.getClosePrice() && lowFractalDate.compareTo(fr.getTradeDate()) == 0)
						{
							continue;
						}
						if(fr.getClosePrice() <= lowFractalPrice)
						{
							flagForEqualOrLessNeighborValuesInFractal = true;
							break;
						}
					}
					Fractal currentFractal = null;
					if(flagForEqualOrLessNeighborValuesInFractal == true)
					{
						Fractal previousToCurrentFractal = (Fractal) fiveDaysFractalList.get(3);
						currentFractal = (Fractal) fiveDaysFractalList.get(4);
						currentFractal.setLowFractal(previousToCurrentFractal.getLowFractal());
						currentFractal.setMinFractalDate(previousToCurrentFractal.getMinFractalDate());
						// System.out.println("Neighbour value is lesser or equal than lowFractal");
						// System.out.println(currentFractal.getLowFractal() + "\t" + currentFractal.getMinFractalDate());
					}
					else
					{
						currentFractal = (Fractal) fiveDaysFractalList.get(4);
						currentFractal.setLowFractal(lowFractal.getClosePrice());
						currentFractal.setMinFractalDate(lowFractal.getTradeDate());
						// System.out.println("LowFractal price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getUpFractal() + "Date =" + currentFractal.getMaxFractalDate());
					}
					flagForEqualOrLessNeighborValuesInFractal = false;
					// System.out.println("================================================");
					hSession.saveOrUpdate(currentFractal);
				}
				hSession.flush();
				tx.commit();
				System.gc();
			}
						
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	// min,max are the limits for mutualFunds ids
	public void updateSwing(Long min,Long max)
	{
		System.out.println("Updating swing");
		Session hSession = null;
		Transaction tx = null;
		try 
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from Fractal where mutualFundId>=? and mutualFundId<=?").setParameter(0,min).setParameter(1, max).list();
			
			Iterator itr = mutualFundList.iterator();

			while (itr.hasNext())
			{
				System.gc();
				long mfId = (Long) itr.next();
				
				List<Fractal> fractalList = hSession.createQuery("from Fractal where mutualFundId=? and candleType=? order by tradeDate").setParameter(0, mfId).setParameter(1, 1).list();
				System.out.println("updating Daily swing for ="+mfId);
				Fractal fractal = null;
				long end=0;
				tx = hSession.beginTransaction();
				for (int i = 0; i < fractalList.size(); i++)
				{										
					boolean swingTrend = false;
					double tcfl1 = 0, tcfl2 = 0, tcfl3 = 0;
					fractal = fractalList.get(i);
					Double swingHigh, swingLow;
					Date swingLowDate, swingHighDate;
					Date tradeDate = fractal.getTradeDate();
					Calendar cal = Calendar.getInstance();
					cal.setTime(tradeDate);
					
					cal.add(Calendar.MONTH, -3);
					Object[] fractalMax = (Object[]) hSession.createQuery("select max(upFractal),min(maxFractalDate) from Fractal where upFractal =(select max(upFractal) from Fractal where mutualFundId=? and tradeDate between ? and ? and upFractal!=? and candleType=? and period=2) and period=2 and candleType=1 and mutualFundId=? and tradeDate between ? and ?")
							.setParameter(0, mfId).setParameter(1, cal.getTime()).setParameter(2, tradeDate).setParameter(3, 0d).setParameter(4, 1).setParameter(5, mfId).setParameter(6, cal.getTime()).setParameter(7, tradeDate).uniqueResult();
					
					if(fractalMax[0] == null || fractalMax[1] == null)
					{
						continue;
					}
					else
					{
						swingHigh = (Double) fractalMax[0];
						swingHighDate = (Date) fractalMax[1];

						fractal.setSwingHigh(swingHigh);
						fractal.setSwingHighDate(swingHighDate);
					}
					Object[] fractalMin = (Object[]) hSession.createQuery("select min(lowFractal),min(minFractalDate) from Fractal where lowFractal =(select min(lowFractal) from Fractal where mutualFundId=? and tradeDate between ? and ? and lowFractal!=? and candleType=? and period=2) and period=2 and candleType=1 and mutualFundId=? and tradeDate between ? and ?")
							.setParameter(0, mfId).setParameter(1, cal.getTime()).setParameter(2, tradeDate).setParameter(3, 0d).setParameter(4, 1).setParameter(5, mfId).setParameter(6, cal.getTime()).setParameter(7, tradeDate).uniqueResult();
					if(fractalMin[0] == null || fractalMin[1] == null)
					{
						continue;
					}
					else
					{
						swingLow = (Double) fractalMin[0];
						swingLowDate = (Date) fractalMin[1];

						fractal.setSwingLow(swingLow);
						fractal.setSwingLowDate(swingLowDate);
						
					}
					if(swingHighDate.before(swingLowDate))
					{
						fractal.setSwingTrend("Low");
						tcfl1 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.382)).floatValue(), 3);
						tcfl2 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.5)).floatValue(), 3);
						tcfl3 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.618)).floatValue(), 3);
					}
					else if(swingLowDate.before(swingHighDate))
					{
						fractal.setSwingTrend("High");
						tcfl1 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.382)).floatValue(), 3);
						tcfl2 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.5)).floatValue(), 3);
						tcfl3 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.618)).floatValue(), 3);
					}
					fractal.setTcfl1(tcfl1);
					fractal.setTcfl2(tcfl2);
					fractal.setTcfl3(tcfl3);
					hSession.saveOrUpdate(fractal);
					//end=System.nanoTime();
					//double t=end-s;
					//System.out.println(t/1000000000);
				}
				hSession.flush();
				tx.commit();
				System.gc();				
			}			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	// to be updated simultaneously with nav
	// update fractal(up/low) for current day
	public void updateCurrentFractal() throws Exception
	{
		Session hSession = null;
		Transaction tx = null;
		final short period = 2;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();

			List mfList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle").list();
			long mfId = 0;
			Date lastFractalDate = null;
			Iterator itr = mfList.iterator();
			while (itr.hasNext())
			{
				mfId = (Long) itr.next();

				List subQuery = hSession.createQuery("select tradeDate from Fractal where mutualFundId=? and period =? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 2).setParameter(2, 1).setMaxResults(4).list();
				// System.out.println("size" + subQuery.size());
				List lastFourFractalList = hSession.createQuery("from Fractal where period=? and mutualFundId=? and candleType=? and tradeDate in (:subQuery) order by tradeDate").setParameter(0, 2).setParameter(1, mfId).setParameter(2, 1).setParameterList("subQuery", subQuery).list();
				// System.out.println("size" + lastFourFractalList.size());

				Fractal lastFractal = (Fractal) lastFourFractalList.get(3);
				lastFractalDate = lastFractal.getTradeDate();
				List everyDayCandleList = hSession.createQuery("from EveryDayCandle where candleType=? and  mutualFundId=? and tradeDate>?").setParameter(0, 1).setParameter(1, mfId).setParameter(2, lastFractalDate).list();
				if(everyDayCandleList.isEmpty())
				{
					throw new Exception("Update EveryDayCandle table with current date/Current Table is already updated");
				}
				if(everyDayCandleList.size() > 1)
				{
					throw new Exception("Fractal table missed for more than 1 day : update datewise with " + (new SimpleDateFormat("dd-MM-yyyy").format(lastFractalDate)));
				}
				else if(everyDayCandleList.size() == 1)
				{
					tx = hSession.beginTransaction();
					EveryDayCandle edc = (EveryDayCandle) everyDayCandleList.get(0);
					Fractal newFractal = new Fractal();
					newFractal.setMutualFundId(mfId);
					newFractal.setTradeDate(edc.getTradeDate());


					newFractal.setCandleType(1);
					newFractal.setPeriod(2);
					newFractal.setClosePrice(edc.getTodaysClosePrice());
					hSession.saveOrUpdate(newFractal);
					tx.commit();


					// below tx is used to store upFractal and lowFractal for currentDay of daily type i.e candleType=1
					tx = hSession.beginTransaction();
					boolean flagForEqualOrGreaterNeighborValuesInFractal = false;
					List lastFiveFractalList = lastFourFractalList;
					lastFiveFractalList.add(newFractal);
					Fractal upFractal = (Fractal) lastFiveFractalList.get(2);
					double upFractalPrice = upFractal.getClosePrice();
					Date upFractalDate = upFractal.getTradeDate();
					Iterator it = lastFiveFractalList.iterator();
					while (it.hasNext())
					{
						Fractal fr = (Fractal) it.next();
						if(upFractalPrice == fr.getClosePrice() && upFractalDate.compareTo(fr.getTradeDate()) == 0)
						{
							continue;
						}
						if(fr.getClosePrice() >= upFractalPrice)
						{
							flagForEqualOrGreaterNeighborValuesInFractal = true;
							break;
						}
					}
					Fractal currentFractal = null;
					if(flagForEqualOrGreaterNeighborValuesInFractal == true)
					{
						Fractal previousToCurrentFractal = (Fractal) lastFiveFractalList.get(3);
						currentFractal = (Fractal) lastFiveFractalList.get(4);
						currentFractal.setUpFractal(previousToCurrentFractal.getUpFractal());
						currentFractal.setMaxFractalDate(previousToCurrentFractal.getMaxFractalDate());
						// System.out.println("Neighbour value is greater or equal than upFractal");
					}
					else
					{
						currentFractal = (Fractal) lastFiveFractalList.get(4);
						currentFractal.setUpFractal(upFractal.getClosePrice());
						currentFractal.setMaxFractalDate(upFractal.getTradeDate());
						// System.out.println("UpFractal price=" + upFractal.getUpFractal());
					}
					hSession.save(currentFractal);

					// below to update fractal with lowFractal
					boolean flagForEqualOrLessNeighborValuesInFractal = false;

					Fractal lowFractal = (Fractal) lastFiveFractalList.get(2);
					double lowFractalPrice = upFractal.getClosePrice();
					Date lowFractalDate = upFractal.getTradeDate();
					it = lastFiveFractalList.iterator();
					while (it.hasNext())
					{
						Fractal fr = (Fractal) it.next();
						if(lowFractalPrice == fr.getClosePrice() && lowFractalDate.compareTo(fr.getTradeDate()) == 0)
						{
							continue;
						}
						if(fr.getClosePrice() <= lowFractalPrice)
						{
							flagForEqualOrLessNeighborValuesInFractal = true;
							break;
						}
					}
					currentFractal = null;
					if(flagForEqualOrLessNeighborValuesInFractal == true)
					{
						Fractal previousToCurrentFractal = (Fractal) lastFiveFractalList.get(3);
						currentFractal = (Fractal) lastFiveFractalList.get(4);
						currentFractal.setLowFractal(previousToCurrentFractal.getLowFractal());
						currentFractal.setMinFractalDate(previousToCurrentFractal.getMinFractalDate());
						// System.out.println("Neighbour value is lesser or equal than lowFractal");
					}
					else
					{
						currentFractal = (Fractal) lastFiveFractalList.get(4);
						currentFractal.setLowFractal(lowFractal.getClosePrice());
						currentFractal.setMinFractalDate(lowFractal.getTradeDate());
						// System.out.println("LowFractal price=" + lowFractal.getUpFractal());
					}
					hSession.save(currentFractal);
				}
				hSession.flush();
				tx.commit();
			}
			
			// on the assumption that last tradeDate in Fractal also has candleType=0 then we update weekly fractal
			List everyDayCandleList = hSession.createQuery("from EveryDayCandle where candleType=? and  mutualFundId=? and tradeDate>?").setParameter(0, 0).setParameter(1, mfId).setParameter(2, lastFractalDate).list();
			if(everyDayCandleList.size() == 1)
			{
				updateCurrentWeeklyFractal(hSession);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
				tx.rollback();
			if(e.getMessage().equals("start date is greater than the end date"))
			{
				throw new Exception(e.getMessage());
			}
		}
		finally
		{
			hSession.close();
		}
	}

	// to be updated with Nav,simultaneously
	// used to update swing value of newly added fractal row
	public void updateCurrentSwing()
	{
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			Date currentDate = null;
			long mfId = 0;
			List mfList = hSession.createQuery("select distinct(mutualFundId) from Fractal").list();
			Iterator itr = mfList.iterator();
			while (itr.hasNext())
			{
				mfId = (Long) itr.next();
				tx = hSession.beginTransaction();
				List f = hSession.createQuery("from Fractal where mutualFundId=? and period=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 2).setParameter(2, 1).setMaxResults(1).list();
				if(f.size() == 0)
				{
					// System.out.println("missing fund from fractal(while updating currennt swing)=" + mfId);
					continue;
				}
				// System.out.println("swing high=" + f.getSwingHigh());
				// System.out.println(((Fractal) f.get(0)).getClosePrice());
				Fractal fractal = (Fractal) f.get(0);
				currentDate = fractal.getTradeDate();
				Calendar cal = Calendar.getInstance();
				cal.setTime(currentDate);
				cal.add(Calendar.MONTH, -3);
				List fractalSwingHigh = hSession.createQuery("from Fractal where tradeDate<=? and tradedate>=? and mutualFundId=? and candleType=? order by upFractal desc").setParameter(0, currentDate).setParameter(1, cal.getTime()).setParameter(2, mfId).setParameter(3, 1).setMaxResults(1).list();
				double swingHigh = ((Fractal) fractalSwingHigh.get(0)).getUpFractal();
				Date swingHighDate = ((Fractal) fractalSwingHigh.get(0)).getMaxFractalDate();
				fractal.setSwingHigh(swingHigh);
				fractal.setSwingHighDate(swingHighDate);

				List fractalSwingLow = hSession.createQuery("from Fractal where tradeDate<=? and tradedate>=? and lowFractal!=? and mutualFundId=? and candleType=? order by lowFractal").setParameter(0, currentDate).setParameter(1, cal.getTime()).setParameter(2, 0d).setParameter(3, mfId)
						.setParameter(4, 1).setMaxResults(1).list();
				double swingLow = ((Fractal) fractalSwingLow.get(0)).getLowFractal();
				Date swingLowDate = ((Fractal) fractalSwingLow.get(0)).getMinFractalDate();
				fractal.setSwingLow(swingLow);
				fractal.setSwingLowDate(swingLowDate);

				// System.out.println("curr=" + currentDate + " 3 months before=" + cal.getTime());
				// System.out.println("swing high Date=" + swingHighDate + "swing Low Date=" + swingLowDate);
				double tcfl1 = 0, tcfl2 = 0, tcfl3 = 0;
				if(swingHighDate.before(swingLowDate))
				{
					fractal.setSwingTrend("Low");
					tcfl1 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.382)).floatValue(), 3);
					tcfl2 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.5)).floatValue(), 3);
					tcfl3 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.618)).floatValue(), 3);
				}
				else if(swingLowDate.before(swingHighDate))
				{
					fractal.setSwingTrend("High");
					tcfl1 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.382)).floatValue(), 3);
					tcfl2 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.5)).floatValue(), 3);
					tcfl3 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.618)).floatValue(), 3);
				}
				fractal.setTcfl1(tcfl1);
				fractal.setTcfl2(tcfl2);
				fractal.setTcfl3(tcfl3);
				hSession.saveOrUpdate(fractal);
			}
			hSession.flush();
			tx.commit();
			// we just need to check if weekly candle is available, if yes then update it
			List f = hSession.createQuery("from Fractal where mutualFundId=? and period=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 2).setParameter(2, 0).setMaxResults(1).list();
			if(f.size() == 1)
			{
				updateCurrentWeeklySwing(hSession);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateFractalAfterDate() throws Exception
	{
		Session hSession = null;
		Transaction tx = null;
		Date lastUpdatedDate=null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			List mfList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where candleType=?").setParameter(0, 1).list();
			Iterator itr = mfList.iterator();
			while (itr.hasNext())
			{
				long mfId = (Long) itr.next();
				
				System.out.println("update daily Fractal for="+mfId);
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from Fractal where mutualFundId=? and period=2 and candleType=1 and upFractal!=0").setParameter(0,mfId).setMaxResults(1).uniqueResult();
				Date startDate=lastUpdatedDate;
				if(lastUpdatedDate==null)
				{
					List l=hSession.createQuery("select tradeDate from EveryDayCandle where mutualFundId=? and candleType=1 order by tradeDate").setParameter(0, mfId).setMaxResults(5).list();
					if(l.size()<5)
						continue;
					startDate=(Date)l.get(0);
					lastUpdatedDate=(Date)l.get(4);
				}
				
				List everyDayFromStartDate = hSession.createQuery("from EveryDayCandle where mutualfundId=? and tradeDate>=? and candleType=? order by tradeDate asc").setParameter(0, mfId).setParameter(1, startDate).setParameter(2, 1).list();
				// suppose from (1s,2nd,3rd,4th) fractal's last date is 1st then if user updates with startDate as 3rd we will miss the 3rd data so check here
				Iterator itr1 = everyDayFromStartDate.iterator();
				try
				{
					try
					{
						// tx for closePrice
						tx = hSession.beginTransaction();
						while (itr1.hasNext())
						{
							EveryDayCandle edc = (EveryDayCandle) itr1.next();
							Fractal f = new Fractal();
							f.setMutualFundId(mfId);
							f.setCandleType(1);
							f.setTradeDate(edc.getTradeDate());
							f.setClosePrice(edc.getTodaysClosePrice());
							f.setPeriod(2);
							hSession.saveOrUpdate(f);
							
						}
						// System.out.println("fractal update for closePrice starting from :" + sDate);
						tx.commit();
					}
					catch (Exception i)
					{
						if(tx != null)
						{
							tx.rollback();
						}
						throw new Exception("Not able to persist fractal close price");
					}
					// transaction for upFractal and lowFractal
					tx = hSession.beginTransaction();
					List<Date> fourDaysBackFromSdateList = hSession.createQuery("select tradeDate from Fractal where mutualFundId=? and period=? and tradeDate<? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 2).setParameter(2, lastUpdatedDate).setParameter(3, 1)
							.setMaxResults(4).list();
					// System.out.println("start date=" + startDate + "fourDaysBackFromSdateList.size()=" + fourDaysBackFromSdateList.size());
					Date newStartDate = fourDaysBackFromSdateList.get(3);

					Fractal checkFractalForPreviousToStartDate = (Fractal) hSession.createQuery("from Fractal where mutualFundId=? and period=? and tradeDate=? and candleType=?").setParameter(0, mfId).setParameter(1, 2).setParameter(2, ((Date) fourDaysBackFromSdateList.get(0))).setParameter(3, 1)
							.uniqueResult();
					
					List fractalList = hSession.createQuery("from Fractal where period=? and mutualFundId=? and tradeDate>=? and candleType=? order by tradeDate").setParameter(0, 2).setParameter(1, mfId).setParameter(2, newStartDate).setParameter(3, 1).list();

					int size = fractalList.size();
					List<Fractal> fiveDaysFractalList = null;
					for (int i = 4; i < size; i++)
					{
						Fractal upFractal = null;
						Fractal lowFractal = null;
						fiveDaysFractalList = new ArrayList<Fractal>();
						for (int j = i - 4; j <= i; j++)
						{
							if(i - j == 2)
							{
								upFractal = (Fractal) fractalList.get(j);
								lowFractal = (Fractal) fractalList.get(j);
							}
							fiveDaysFractalList.add((Fractal) fractalList.get(j));
						}
			
						// insert previous maxFractal,maxFractalDate if there is no unique max date from the 5 days data in fiveDaysFractalList
						// flag for (equal or less),(equal or greater) than neighbour values in fractal list of 5
						//for upFractal
						boolean flagForEqualOrGreaterNeighborValuesInFractal = false;
						//for lowFractal
						boolean flagForEqualOrLessNeighborValuesInFractal = false;
						Iterator it = fiveDaysFractalList.iterator();

						
						double upFractalPrice = upFractal.getClosePrice();
						Date upFractalDate = upFractal.getTradeDate();
						double lowFractalPrice = lowFractal.getClosePrice();
						Date lowFractalDate = lowFractal.getTradeDate();
						while (it.hasNext())
						{
							Fractal fr = (Fractal) it.next();
							if(upFractalPrice == fr.getClosePrice() && upFractalDate.compareTo(fr.getTradeDate()) == 0)
							{
								continue;
							}
							if(fr.getClosePrice() >= upFractalPrice)
							{
								flagForEqualOrGreaterNeighborValuesInFractal = true;
								break;
							}
						}
						it = fiveDaysFractalList.iterator();
						while (it.hasNext())
						{
							Fractal fr = (Fractal) it.next();
							if(lowFractalPrice == fr.getClosePrice() && lowFractalDate.compareTo(fr.getTradeDate()) == 0)
							{
								continue;
							}
							if(fr.getClosePrice() <= lowFractalPrice)
							{
								flagForEqualOrLessNeighborValuesInFractal = true;
								break;
							}
						}
						Fractal currentFractal = null;
						if(flagForEqualOrLessNeighborValuesInFractal == true)
						{
							Fractal previousToCurrentFractal = (Fractal) fiveDaysFractalList.get(3);
							currentFractal = (Fractal) fiveDaysFractalList.get(4);
							currentFractal.setLowFractal(previousToCurrentFractal.getLowFractal());
							currentFractal.setMinFractalDate(previousToCurrentFractal.getMinFractalDate());
							// System.out.println("Neighbour value is less or equal than lowFractal");
							// System.out.println(currentFractal.getLowFractal() + "\t" + currentFractal.getMinFractalDate());
						}
						else
						{
							currentFractal = (Fractal) fiveDaysFractalList.get(4);
							currentFractal.setLowFractal(lowFractal.getClosePrice());
							currentFractal.setMinFractalDate(lowFractal.getTradeDate());
							// System.out.println("LowFractal price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getLowFractal() + "Date =" + currentFractal.getMinFractalDate());
						}
						if(flagForEqualOrGreaterNeighborValuesInFractal == true)
						{
							Fractal previousToCurrentFractal = (Fractal) fiveDaysFractalList.get(3);
							currentFractal = (Fractal) fiveDaysFractalList.get(4);
							currentFractal.setUpFractal(previousToCurrentFractal.getUpFractal());
							currentFractal.setMaxFractalDate(previousToCurrentFractal.getMaxFractalDate());
							// System.out.println("Neighbour value is greater or equal than upFractal");
							// System.out.println(currentFractal.getUpFractal() + "\t" + currentFractal.getMaxFractalDate());
						}
						else
						{
							currentFractal = (Fractal) fiveDaysFractalList.get(4);
							currentFractal.setUpFractal(upFractal.getClosePrice());
							currentFractal.setMaxFractalDate(upFractal.getTradeDate());
							// System.out.println("UpFractal price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getUpFractal() + "Date =" + currentFractal.getMaxFractalDate());
						}
						flagForEqualOrGreaterNeighborValuesInFractal = false;
						flagForEqualOrLessNeighborValuesInFractal = false;
						// System.out.println("================================================");
						
						hSession.saveOrUpdate(currentFractal);
						//System.out.println("UpFractal="+currentFractal.getUpFractal());
						//System.out.println("LowFractal="+currentFractal.getLowFractal());
					}
					tx.commit();
				}
				catch (Exception e)
				{
					if(tx != null)
					{
						tx.rollback();
					}
				}
			}
		}
		catch (Exception e)
		{
			if(tx != null)
				tx.rollback();
			if(e.getMessage().equals("greater than and equal to start date does not exists in EveryDayCandle table"))
			{
				throw new Exception(e.getMessage());
			}
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateSwingAfterDate() throws Exception
	{
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			String noUpFractalAvailable="";
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where candleType=1").list();
			Iterator itr = mutualFundList.iterator();
			Date lastUpdatedDate=null;
			while (itr.hasNext())
			{
				long mfId = (Long) itr.next();
				
				System.out.println("updating daily swing for="+mfId);
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from Fractal where mutualFundId=? and candleType=1 and period=2 and swingHigh is not null and swingLow is not null").setParameter(0, mfId).setMaxResults(1).uniqueResult();
				if(lastUpdatedDate==null)
				{
					lastUpdatedDate=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=1 and period=2 and upFractal!=0").setParameter(0, mfId).setMaxResults(1).uniqueResult();
				}				
				if(lastUpdatedDate==null)
				{
					noUpFractalAvailable=noUpFractalAvailable+" ' "+mfId;
					continue;
				}
				List<Fractal> fractalList = hSession.createQuery("from Fractal where mutualFundId=? and tradeDate>=? and candleType=1 and period=2 order by tradeDate").setParameter(0, mfId).setParameter(1, lastUpdatedDate).list();
				tx = hSession.beginTransaction();
				Fractal fractal = null;
				for (int i = 0; i < fractalList.size(); i++)
				{
					
					boolean swingTrend = false;
					double tcfl1 = 0, tcfl2 = 0, tcfl3 = 0;
					fractal = fractalList.get(i);
					Double swingHigh, swingLow;
					Date swingLowDate, swingHighDate;
					Date tradeDate = fractal.getTradeDate();
					Calendar cal = Calendar.getInstance();
					cal.setTime(tradeDate);
					cal.add(Calendar.MONTH, -3);
					Object[] fractalMax = (Object[]) hSession.createQuery("select max(upFractal),min(maxFractalDate) from Fractal where upFractal =(select max(upFractal) from Fractal where mutualFundId=? and tradeDate between ? and ? and candleType=1 and period=2 and upFractal!=0 ) and period=2 and candleType=1 and mutualFundId=? and tradeDate between ? and ?")
							.setParameter(0, mfId).setParameter(1, cal.getTime()).setParameter(2, tradeDate).setParameter(3, mfId).setParameter(4, cal.getTime()).setParameter(5, tradeDate).uniqueResult();
					if(fractalMax[0] == null && fractalMax[1] == null)
					{
						continue;
					}
					else
					{
						swingHigh = (Double) fractalMax[0];
						swingHighDate = (Date) fractalMax[1];

						fractal.setSwingHigh(swingHigh);
						fractal.setSwingHighDate(swingHighDate);
					}
					Object[] fractalMin = (Object[]) hSession.createQuery("select min(lowFractal),min(minFractalDate) from Fractal where lowFractal = (select min(lowFractal) from Fractal where mutualFundId=? and tradeDate between ? and ? and lowFractal!=0 and candleType=1 and period=2) and period=2 and candleType=1 and mutualFundId=? and tradeDate between ? and ?")
							.setParameter(0, mfId).setParameter(1, cal.getTime()).setParameter(2, tradeDate).setParameter(3, mfId).setParameter(4, cal.getTime()).setParameter(5, tradeDate).uniqueResult();
					if(fractalMin[0] == null || fractalMin[1] == null)
					{
						continue;
					}
					else
					{
						swingLow = (Double) fractalMin[0];
						swingLowDate = (Date) fractalMin[1];
						fractal.setSwingLow(swingLow);
						fractal.setSwingLowDate(swingLowDate);
					}
					if(swingHighDate.before(swingLowDate))
					{
						fractal.setSwingTrend("Low");
						tcfl1 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.382)).floatValue(), 3);
						tcfl2 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.5)).floatValue(), 3);
						tcfl3 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.618)).floatValue(), 3);
					}
					else if(swingLowDate.before(swingHighDate))
					{
						fractal.setSwingTrend("High");
						tcfl1 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.382)).floatValue(), 3);
						tcfl2 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.5)).floatValue(), 3);
						tcfl3 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.618)).floatValue(), 3);
					}
					fractal.setTcfl1(tcfl1);
					fractal.setTcfl2(tcfl2);
					fractal.setTcfl3(tcfl3);
					hSession.saveOrUpdate(fractal);
					
				}
				tx.commit();
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}


	}

	public void deleteFractalIndicatorsAfterStartDate(String sDate) throws ParseException
	{
		Session hSession = null;
		Transaction tx = null;
		Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(sDate);
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			tx = hSession.beginTransaction();
			int n = hSession.createQuery("delete from Fractal where tradeDate>=?").setParameter(0, startDate).executeUpdate();
			tx.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public int delete(String currentOrAll) throws Exception
	{
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			tx = hSession.beginTransaction();
			if(currentOrAll.equalsIgnoreCase("all"))
			{
				int n = hSession.createQuery("delete from Fractal").executeUpdate();
			}
			else if(currentOrAll.equalsIgnoreCase("current"))
			{
				Date currentDate = (Date) hSession.createQuery("select tradeDate from CurrentDayCandle").setMaxResults(1).uniqueResult();
				Date fractalLastDate = (Date) hSession.createQuery("select tradeDate from Fractal order by tradeDate desc").setMaxResults(1).uniqueResult();
				if(currentDate != null && fractalLastDate.compareTo(currentDate) == 0)
				{
					int n = hSession.createQuery("delete from Fractal where tradeDate=?").setParameter(0, currentDate).executeUpdate();
				}
				else if(currentDate == null)
				{
					throw new Exception("CurrentDayCandle table is currently empty (deletion is made on the basis of tradeDate in CurrentDayCandle table)");
				}
				else if(fractalLastDate.compareTo(currentDate) < 0)
				{
					throw new Exception("Fractal table does not contain CurrentDayCandle table tradeDate");
				}
				else if(fractalLastDate.compareTo(currentDate) > 0)
				{
					throw new Exception("CurrentDayCandle is not properly updated(Fractal table have extra dates)");
				}
			}
			tx.commit();
			return 1;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateWeeklyFractal(Long min,Long max)
	{
		System.out.println("Inserting ohlc into weekly Fractal");
		Session hSession = null;
		Transaction tx = null;
		final short period = 2;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			List everyDayWeeklyList = hSession.createQuery("select distinct mutualFundId from EveryDayCandle where candleType=? and mutualFundId>=? and mutualFundId<=? order by mutualFundId").setParameter(0, 0).setParameter(1,min).setParameter(2, max).list();
			System.out.println("size="+everyDayWeeklyList.size());
			Fractal fractal = null;
			EveryDayCandle edc=null;
			long mfId=0;
			List<EveryDayCandle> edcList=null;
			Iterator itr1=null;
			Iterator itr = everyDayWeeklyList.iterator();
			while (itr.hasNext())
			{
				mfId = (Long) itr.next();
				System.out.println("weekly fractal update for="+mfId);
				edcList= hSession.createQuery("from EveryDayCandle where candleType=? and mutualFundId=? order by tradeDate").setParameter(0, 0).setParameter(1, mfId).list();
				tx = hSession.beginTransaction();
				itr1 = edcList.iterator();
				while (itr1.hasNext())
				{
					 edc= (EveryDayCandle) itr1.next();
					fractal = new Fractal();
					fractal.setMutualFundId(mfId);
					fractal.setCandleType(0);
					fractal.setPeriod(period);
					fractal.setTradeDate(edc.getTradeDate());
					fractal.setOpen(edc.getOpen());
					fractal.setClosePrice(edc.getTodaysClosePrice());
					fractal.setHigh(edc.getHigh());
					fractal.setLow(edc.getLow());
					hSession.saveOrUpdate(fractal);
				}
				hSession.flush();
				tx.commit();
				System.gc();
			}	
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateWeeklyUpFractal(Long min,Long max)
	{
		System.out.println("Updating weekly UpFractal");
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			
			List fiveDaysFractalList = null;
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from Fractal where candleType=? and mutualFundId>=? and mutualFundId<=?").setParameter(0,0).setParameter(1,min).setParameter(2, max).list();
			Iterator itr = mutualFundList.iterator();
			long mfId=0;
			List fractalList=null;
			while (itr.hasNext())
			{
				mfId = (Long) itr.next();
				System.out.println("update weekly upFractal for = "+mfId);
				fractalList = hSession.createQuery("from Fractal where candleType=? and period=? and mutualFundId=? order by tradeDate").setParameter(0, 0).setParameter(1, 2).setParameter(2, mfId).list();

				int size = fractalList.size();

				// System.out.println("FractalSize=" + fractalList.size());
				Fractal fractal = null;
				tx = hSession.beginTransaction();
				for (int i = 4; i < size; i++)
				{
					
					Fractal upFractal = null;
					fiveDaysFractalList = new ArrayList<Fractal>();
					for (int j = i - 4; j <= i; j++)
					{
						if(i - j == 2)
						{
							upFractal = (Fractal) fractalList.get(j);
						}
						fiveDaysFractalList.add((Fractal) fractalList.get(j));
					}
					Iterator itr2 = fiveDaysFractalList.iterator();
					// System.out.println("Up Fractal Date = " + upFractal.getTradeDate());
					/*while (itr2.hasNext())
					{
						Fractal f = (Fractal) itr2.next();
						System.out.println(f.getTradeDate() + "\t\t" + f.getHigh());
					}*/

					// flag for equal or less than neighbour values in fractal list of 5
					boolean flagForEqualOrGreaterNeighborValuesInFractal = false;
					// List<Double> fiveDaysFractalPriceList = new ArrayList<Double>();
					// int fiveDaysFractalListSize = fiveDaysFractalList.size();
					Iterator it = fiveDaysFractalList.iterator();

					double upFractalPrice = upFractal.getHigh();
					Date upFractalDate = upFractal.getTradeDate();
					while (it.hasNext())
					{
						Fractal fr = (Fractal) it.next();
						if(upFractalPrice == fr.getHigh() && upFractalDate.compareTo(fr.getTradeDate()) == 0)
						{
							continue;
						}
						if(fr.getHigh() >= upFractalPrice)
						{
							flagForEqualOrGreaterNeighborValuesInFractal = true;
							break;
						}
					}
					Fractal currentFractal = null;
					if(flagForEqualOrGreaterNeighborValuesInFractal == true)
					{
						Fractal previousToCurrentFractal = (Fractal) fiveDaysFractalList.get(3);
						currentFractal = (Fractal) fiveDaysFractalList.get(4);
						currentFractal.setUpFractal(previousToCurrentFractal.getUpFractal());
						currentFractal.setMaxFractalDate(previousToCurrentFractal.getMaxFractalDate());
						// System.out.println("Neighbour value is greater or equal than upFractal");
						// System.out.println(currentFractal.getUpFractal() + "\t" + currentFractal.getMaxFractalDate());
					}
					else
					{
						currentFractal = (Fractal) fiveDaysFractalList.get(4);
						currentFractal.setUpFractal(upFractal.getHigh());
						currentFractal.setMaxFractalDate(upFractal.getTradeDate());
						// System.out.println("UpFractal price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getUpFractal() + "Date =" + currentFractal.getMaxFractalDate());
					}
					flagForEqualOrGreaterNeighborValuesInFractal = false;

					// System.out.println("================================================");
					hSession.saveOrUpdate(currentFractal);
				}
				hSession.flush();
				tx.commit();
				System.gc();
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateWeeklyLowFractal(Long min,Long max)
	{
		System.out.println("Updating weekly lowFractal");
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			Fractal fractal = null;
			List fiveDaysFractalList = null;
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from Fractal where candleType=? and mutualFundId>=? and mutualFundId<=?").setParameter(0,0).setParameter(1,min).setParameter(2,max).list();
			Iterator itr = mutualFundList.iterator();
			while (itr.hasNext())
			{
				long mfId = (Long) itr.next();
				System.out.println("update weekly lowFractal for = "+mfId);
				List fractalList = hSession.createQuery("from Fractal where period=? and mutualFundId=? and candleType=? order by tradeDate").setParameter(0, 2).setParameter(1, mfId).setParameter(2, 0).list();

				int size = fractalList.size();
				tx = hSession.beginTransaction();
				// System.out.println("FractalSize=" + fractalList.size());
				for (int i = 4; i < size; i++)
				{
					Fractal lowFractal = null;
					fiveDaysFractalList = new ArrayList<Fractal>();
					for (int j = i - 4; j <= i; j++)
					{
						if(i - j == 2)
						{
							lowFractal = (Fractal) fractalList.get(j);
						}
						fiveDaysFractalList.add((Fractal) fractalList.get(j));
					}
					Iterator itr2 = fiveDaysFractalList.iterator();
					// System.out.println("Up Fractal Date = " + upFractal.getTradeDate());
					/*while (itr2.hasNext())
					{
						Fractal f = (Fractal) itr2.next();
						System.out.println(f.getTradeDate() + "\t\t" + f.getLow());
					}*/
					// flag for equal or less than neighbour values in fractal list of 5
					boolean flagForEqualOrLessNeighborValuesInFractal = false;
					Iterator it = fiveDaysFractalList.iterator();

					double lowFractalPrice = lowFractal.getLow();
					Date lowFractalDate = lowFractal.getTradeDate();
					while (it.hasNext())
					{
						Fractal fr = (Fractal) it.next();
						if(lowFractalPrice == fr.getLow() && lowFractalDate.compareTo(fr.getTradeDate()) == 0)
						{
							continue;
						}
						if(fr.getLow() <= lowFractalPrice)
						{
							flagForEqualOrLessNeighborValuesInFractal = true;
							break;
						}
					}
					Fractal currentFractal = null;
					if(flagForEqualOrLessNeighborValuesInFractal == true)
					{
						Fractal previousToCurrentFractal = (Fractal) fiveDaysFractalList.get(3);
						currentFractal = (Fractal) fiveDaysFractalList.get(4);
						currentFractal.setLowFractal(previousToCurrentFractal.getLowFractal());
						currentFractal.setMinFractalDate(previousToCurrentFractal.getMinFractalDate());
						// System.out.println("Neighbour value is lesser or equal than lowFractal");
						// System.out.println(currentFractal.getLowFractal() + "\t" + currentFractal.getMinFractalDate());
					}
					else
					{
						currentFractal = (Fractal) fiveDaysFractalList.get(4);
						currentFractal.setLowFractal(lowFractal.getLow());
						currentFractal.setMinFractalDate(lowFractal.getTradeDate());
						// System.out.println("LowFractal price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getUpFractal() + "Date =" + currentFractal.getMaxFractalDate());
					}
					flagForEqualOrLessNeighborValuesInFractal = false;
					// System.out.println("================================================");
					hSession.saveOrUpdate(currentFractal);
				}
				hSession.flush();
				tx.commit();
				System.gc();
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}

	}

	public void updateWeeklySwing(Long min,Long max)
	{
		System.out.println("Updating weekly swing");
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from Fractal where candleType=? and mutualFundId>=? and mutualFundId<=?").setParameter(0,0).setParameter(1,min).setParameter(2,max).list();
			Iterator itr = mutualFundList.iterator();

			while (itr.hasNext())
			{
				System.gc();
				long mfId = (Long) itr.next();
				
				List<Fractal> fractalList = hSession.createQuery("from Fractal where mutualFundId=? and candleType=? and period=2 order by tradeDate").setParameter(0, mfId).setParameter(1, 0).list();
				System.out.println("Weekly swing update for ="+mfId);
				Fractal fractal = null;
				tx = hSession.beginTransaction();
				for (int i = 0; i < fractalList.size(); i++)
				{
					boolean swingTrend = false;
					double tcfl1 = 0, tcfl2 = 0, tcfl3 = 0;
					fractal = fractalList.get(i);
					Double swingHigh, swingLow;
					Date swingLowDate, swingHighDate;
					Date tradeDate = fractal.getTradeDate();
					Calendar cal = Calendar.getInstance();
					cal.setTime(tradeDate);
					cal.add(Calendar.MONTH, -3);
					Object[] fractalMax = (Object[]) hSession.createQuery("select max(upFractal),min(maxFractalDate) from Fractal where upFractal =(select max(upFractal) from Fractal where mutualFundId=? and tradeDate between ? and ? and upFractal!=? and candleType=? and period=2) and period=2 and candleType=0 and mutualFundId=? and tradeDate between ? and ?")
							.setParameter(0, mfId).setParameter(1, cal.getTime()).setParameter(2, tradeDate).setParameter(3, 0d).setParameter(4, 0).setParameter(5, mfId).setParameter(6, cal.getTime()).setParameter(7, tradeDate).uniqueResult();
					if(fractalMax[0] == null || fractalMax[1] == null)
					{
						continue;
					}
					else
					{
						swingHigh = (Double) fractalMax[0];
						swingHighDate = (Date) fractalMax[1];
						// System.out.println("shd=" + swingHighDate);

						fractal.setSwingHigh(swingHigh);
						fractal.setSwingHighDate(swingHighDate);
					}
					Object[] fractalMin = (Object[]) hSession.createQuery("select min(lowFractal),min(minFractalDate) from Fractal where lowFractal =(select min(lowFractal) from Fractal where mutualFundId=? and tradeDate between ? and ? and lowFractal!=? and candleType=? and period=2) and period=2 and candleType=0 and mutualFundId=? and tradeDate between ? and ?")
							.setParameter(0, mfId).setParameter(1, cal.getTime()).setParameter(2, tradeDate).setParameter(3, 0d).setParameter(4, 0).setParameter(5, mfId).setParameter(6, cal.getTime()).setParameter(7, tradeDate).uniqueResult();
					if(fractalMin[0] == null || fractalMin[1] == null)
					{
						continue;
					}
					else
					{
						swingLow = (Double) fractalMin[0];
						swingLowDate = (Date) fractalMin[1];
						fractal.setSwingLow(swingLow);
						fractal.setSwingLowDate(swingLowDate);

					}
					if(swingHighDate.before(swingLowDate))
					{
						fractal.setSwingTrend("Low");
						tcfl1 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.382)).floatValue(), 3);
						tcfl2 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.5)).floatValue(), 3);
						tcfl3 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.618)).floatValue(), 3);
					}
					else if(swingLowDate.before(swingHighDate))
					{
						fractal.setSwingTrend("High");
						tcfl1 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.382)).floatValue(), 3);
						tcfl2 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.5)).floatValue(), 3);
						tcfl3 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.618)).floatValue(), 3);
					}
					fractal.setTcfl1(tcfl1);
					fractal.setTcfl2(tcfl2);
					fractal.setTcfl3(tcfl3);
					
					hSession.save(fractal);
					
				}
				hSession.flush();
				tx.commit();
				System.gc();
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}

	}

	public void updateWeeklyFractalAfterDate() throws Exception
	{
		Session hSession = null;
		Transaction tx = null;
		Date lastUpdatedDate=null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			List mfList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where candleType=?").setParameter(0, 0).list();
			Iterator itr = mfList.iterator();
			while (itr.hasNext())
			{
				long mfId = (Long) itr.next();
				
				System.out.println("updating weekly fractal mutualFund="+mfId);
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from Fractal where mutualFundId=? and candleType=0 and period=2 and upFractal!=0").setParameter(0, mfId).setMaxResults(1).uniqueResult();
				Date startDate=lastUpdatedDate;
				if(lastUpdatedDate==null)
				{
					List l=hSession.createQuery("select tradeDate from EveryDayCandle where mutualFundId=? and candleType=0 order by tradeDate").setParameter(0, mfId).setMaxResults(5).list();
					if(l.size()<5)
						continue;
					startDate=(Date)l.get(0);
					lastUpdatedDate=(Date)l.get(4);
				}
				List everyDayFromStartDate = hSession.createQuery("from EveryDayCandle where mutualfundId=? and tradeDate>=? and candleType=? order by tradeDate asc").setParameter(0, mfId).setParameter(1, startDate).setParameter(2, 0).list();
				// suppose from (1s,2nd,3rd,4th) fractal's last date is 1st then if user updates with startDate as 3rd we will miss the 3rd data so check here
				Iterator itr1 = everyDayFromStartDate.iterator();
				try
				{
					try
					{
						// tx for closePrice
						tx = hSession.beginTransaction();
						while (itr1.hasNext())
						{
							EveryDayCandle edc = (EveryDayCandle) itr1.next();
							Fractal f = new Fractal();
							f.setMutualFundId(mfId);
							f.setCandleType(0);
							f.setTradeDate(edc.getTradeDate());
							f.setOpen(edc.getOpen());
							f.setClosePrice(edc.getTodaysClosePrice());
							f.setHigh(edc.getHigh());
							f.setLow(edc.getLow());
							f.setPeriod(2);
							hSession.saveOrUpdate(f);
						}
						// System.out.println("fractal update for closePrice starting from :" + sDate);
						tx.commit();
					}
					catch (Exception i)
					{
						if(tx != null)
						{
							tx.rollback();
						}
						throw new Exception("Not able to persist fractal close price");
					}
					// transaction for upFractal and lowFractal
					tx = hSession.beginTransaction();
					List<Date> fourDaysBackFromSdateList = hSession.createQuery("select tradeDate from Fractal where mutualFundId=? and period=? and tradeDate<? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 2).setParameter(2, lastUpdatedDate).setParameter(3, 0)
							.setMaxResults(4).list();
					// System.out.println("start date=" + startDate + "fourDaysBackFromSdateList.size()=" + fourDaysBackFromSdateList.size());
					Date newStartDate = fourDaysBackFromSdateList.get(3);

					Fractal checkFractalForPreviousToStartDate = (Fractal) hSession.createQuery("from Fractal where mutualFundId=? and period=? and tradeDate=? and candleType=?").setParameter(0, mfId).setParameter(1, 2).setParameter(2, ((Date) fourDaysBackFromSdateList.get(0))).setParameter(3, 0)
							.uniqueResult();
					
					List fractalList = hSession.createQuery("from Fractal where period=? and mutualFundId=? and tradeDate>=? and candleType=? order by tradeDate").setParameter(0, 2).setParameter(1, mfId).setParameter(2, newStartDate).setParameter(3, 0).list();

					int size = fractalList.size();
					List<Fractal> fiveDaysFractalList = null;
					for (int i = 4; i < size; i++)
					{
						Fractal upFractal = null;
						Fractal lowFractal = null;
						fiveDaysFractalList = new ArrayList<Fractal>();
						for (int j = i - 4; j <= i; j++)
						{
							if(i - j == 2)
							{
								upFractal = (Fractal) fractalList.get(j);
								lowFractal = (Fractal) fractalList.get(j);
							}
							fiveDaysFractalList.add((Fractal) fractalList.get(j));
						}
						Iterator itr2 = fiveDaysFractalList.iterator();
						
						// insert previous maxFractal,maxFractalDate if there is no unique max date from the 5 days data in fiveDaysFractalList
						// flag for (equal or less),(equal or greater) than neighbour values in fractal list of 5
						boolean flagForEqualOrGreaterNeighborValuesInFractal = false;
						boolean flagForEqualOrLessNeighborValuesInFractal = false;
						Iterator it = fiveDaysFractalList.iterator();

						double upFractalPrice = upFractal.getHigh();
						Date upFractalDate = upFractal.getTradeDate();
						double lowFractalPrice = lowFractal.getLow();
						Date lowFractalDate = lowFractal.getTradeDate();
						while (it.hasNext())
						{
							Fractal fr = (Fractal) it.next();
							if(upFractalPrice == fr.getHigh() && upFractalDate.compareTo(fr.getTradeDate()) == 0)
							{
								continue;
							}
							if(fr.getHigh() >= upFractalPrice)
							{
								flagForEqualOrGreaterNeighborValuesInFractal = true;
								break;
							}
						}
						it = fiveDaysFractalList.iterator();
						while (it.hasNext())
						{
							Fractal fr = (Fractal) it.next();
							if(lowFractalPrice == fr.getLow() && lowFractalDate.compareTo(fr.getTradeDate()) == 0)
							{
								continue;
							}
							if(fr.getLow() <= lowFractalPrice)
							{
								flagForEqualOrLessNeighborValuesInFractal = true;
								break;
							}
						}
						Fractal currentFractal = null;
						if(flagForEqualOrLessNeighborValuesInFractal == true)
						{
							Fractal previousToCurrentFractal = (Fractal) fiveDaysFractalList.get(3);
							currentFractal = (Fractal) fiveDaysFractalList.get(4);
							currentFractal.setLowFractal(previousToCurrentFractal.getLowFractal());
							currentFractal.setMinFractalDate(previousToCurrentFractal.getMinFractalDate());
							// System.out.println("Neighbour value is less or equal than lowFractal");
							// System.out.println(currentFractal.getLowFractal() + "\t" + currentFractal.getMinFractalDate());
						}
						else
						{
							currentFractal = (Fractal) fiveDaysFractalList.get(4);
							currentFractal.setLowFractal(lowFractal.getLow());
							currentFractal.setMinFractalDate(lowFractal.getTradeDate());
							// System.out.println("LowFractal price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getLowFractal() + "Date =" + currentFractal.getMinFractalDate());
						}
						if(flagForEqualOrGreaterNeighborValuesInFractal == true)
						{
							Fractal previousToCurrentFractal = (Fractal) fiveDaysFractalList.get(3);
							currentFractal = (Fractal) fiveDaysFractalList.get(4);
							currentFractal.setUpFractal(previousToCurrentFractal.getUpFractal());
							currentFractal.setMaxFractalDate(previousToCurrentFractal.getMaxFractalDate());
							// System.out.println("Neighbour value is greater or equal than upFractal");
							// System.out.println(currentFractal.getUpFractal() + "\t" + currentFractal.getMaxFractalDate());
						}
						else
						{
							currentFractal = (Fractal) fiveDaysFractalList.get(4);
							currentFractal.setUpFractal(upFractal.getHigh());
							currentFractal.setMaxFractalDate(upFractal.getTradeDate());
							// System.out.println("UpFractal price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getUpFractal() + "Date =" + currentFractal.getMaxFractalDate());
						}
						flagForEqualOrGreaterNeighborValuesInFractal = false;
						flagForEqualOrLessNeighborValuesInFractal = false;
						// System.out.println("================================================");
						hSession.saveOrUpdate(currentFractal);

					}
					tx.commit();
				}
				catch (Exception e)
				{
					if(tx != null)
					{
						tx.rollback();
					}
				}
			}
		}
		catch (Exception e)
		{
			if(tx != null)
				tx.rollback();
			
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}

	}

	public void updateWeeklySwingAfterDate() throws Exception
	{
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			String noUpFractalAvailable="";
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where candleType=?").setParameter(0, 0).list();
			Iterator itr = mutualFundList.iterator();
			Date lastUpdatedDate=null;
			while (itr.hasNext())
			{
				long mfId = (Long) itr.next();
				
				System.out.println("updating weekly swing mutualFund="+mfId);
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from Fractal where mutualFundId=? and candleType=0 and period=2 and upFractal!=0 and swingHigh is not null and swingLow is not null").setParameter(0, mfId).setMaxResults(1).uniqueResult();
				if(lastUpdatedDate==null)
				{
					lastUpdatedDate=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=0 and period=2 and upFractal!=0").setParameter(0, mfId).setMaxResults(1).uniqueResult();
				}
				if(lastUpdatedDate==null)
				{
					noUpFractalAvailable=noUpFractalAvailable+","+mfId;
					continue;
				}				
				List<Fractal> fractalList = hSession.createQuery("from Fractal where mutualFundId=? and tradeDate>=? and candleType=0 and period=2 order by tradeDate").setParameter(0, mfId).setParameter(1, lastUpdatedDate).list();
				
				Fractal fractal = null;
				tx = hSession.beginTransaction();
				for (int i = 0; i < fractalList.size(); i++)
				{
					boolean swingTrend = false;
					double tcfl1 = 0, tcfl2 = 0, tcfl3 = 0;
					fractal = fractalList.get(i);
					Double swingHigh, swingLow;
					Date swingLowDate, swingHighDate;
					Date tradeDate = fractal.getTradeDate();
					Calendar cal = Calendar.getInstance();
					cal.setTime(tradeDate);
					cal.add(Calendar.MONTH, -3);
					Object[] fractalMax = (Object[]) hSession.createQuery("select max(upFractal),min(maxFractalDate) from Fractal where upFractal =(select max(upFractal) from Fractal where mutualFundId=? and tradeDate between ? and ? and upFractal!=0 and candleType=0 and period=2 order by tradeDate) and period=2 and candleType=0 and mutualFundId=? and tradeDate between ? and ?")
							.setParameter(0, mfId).setParameter(1, cal.getTime()).setParameter(2, tradeDate).setParameter(3, mfId).setParameter(4, cal.getTime()).setParameter(5, tradeDate).uniqueResult();
					if(fractalMax[0] == null)
					{
						continue;
					}
					else
					{
						swingHigh = (Double) fractalMax[0];
						swingHighDate = (Date) fractalMax[1];
						
						fractal.setSwingHigh(swingHigh);
						fractal.setSwingHighDate(swingHighDate);

					}
					Object[] fractalMin = (Object[]) hSession.createQuery("select min(lowFractal),min(minFractalDate) from Fractal where lowFractal =(select min(lowFractal) from Fractal where mutualFundId=? and tradeDate between ? and ?  and lowFractal!=0 and candleType=0 and period=2 order by tradeDate) and period=2 and candleType=0 and mutualFundId=? and tradeDate between ? and ?")
							.setParameter(0, mfId).setParameter(1, cal.getTime()).setParameter(2, tradeDate).setParameter(3, mfId).setParameter(4, cal.getTime()).setParameter(5, tradeDate).uniqueResult();
					if(fractalMin[0] == null)
					{
						continue;
					}
					else
					{
						swingLow = (Double) fractalMin[0];
						swingLowDate = (Date) fractalMin[1];

						fractal.setSwingLow(swingLow);
						fractal.setSwingLowDate(swingLowDate);
					}
					if(swingHighDate.before(swingLowDate))
					{
						fractal.setSwingTrend("Low");
						tcfl1 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.382)).floatValue(), 3);
						tcfl2 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.5)).floatValue(), 3);
						tcfl3 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.618)).floatValue(), 3);
					}
					else if(swingLowDate.before(swingHighDate))
					{
						fractal.setSwingTrend("High");
						tcfl1 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.382)).floatValue(), 3);
						tcfl2 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.5)).floatValue(), 3);
						tcfl3 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.618)).floatValue(), 3);
					}
					fractal.setTcfl1(tcfl1);
					fractal.setTcfl2(tcfl2);
					fractal.setTcfl3(tcfl3);
					//System.out.println("update weekly swing for mutualFundid="+mfId+" dated="+tradeDate);
					hSession.saveOrUpdate(fractal);
				}
				tx.commit();
			}			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateWeeklyFractalPeriod1(Long min,Long max)
	{
		System.out.println("Inserting ohlc into weekly Fractal period 1");
		Session hSession = null;
		Transaction tx = null;
		final int period =1;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			List everyDayWeeklyList = hSession.createQuery("select distinct mutualFundId from EveryDayCandle where candleType=? and mutualFundId>=? and mutualFundId<=? order by mutualFundId").setParameter(0, 0).setParameter(1,min).setParameter(2, max).list();
			System.out.println("size="+everyDayWeeklyList.size());
			Fractal fractal = null;
			EveryDayCandle edc=null;
			long mfId=0;
			List<EveryDayCandle> edcList=null;
			Iterator itr1=null;
			Iterator itr = everyDayWeeklyList.iterator();
			while (itr.hasNext())
			{
				mfId = (Long) itr.next();
				System.out.println("weekly fractal update for period =1 ="+mfId);
				edcList= hSession.createQuery("from EveryDayCandle where candleType=? and mutualFundId=? order by tradeDate").setParameter(0, 0).setParameter(1, mfId).list();
				tx = hSession.beginTransaction();
				itr1 = edcList.iterator();
				while (itr1.hasNext())
				{
					 edc= (EveryDayCandle) itr1.next();
					fractal = new Fractal();
					fractal.setMutualFundId(mfId);
					fractal.setCandleType(0);
					fractal.setPeriod(period);
					fractal.setTradeDate(edc.getTradeDate());
					fractal.setOpen(edc.getOpen());
					fractal.setClosePrice(edc.getTodaysClosePrice());
					fractal.setHigh(edc.getHigh());
					fractal.setLow(edc.getLow());
					hSession.saveOrUpdate(fractal);
				}
				hSession.flush();
				tx.commit();
				System.gc();
			}	
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}
	
	public void updateWeeklyFractalPeriod1AfterDate()
	{
		System.out.println("Inserting ohlc into weekly Fractal period 1");
		Session hSession = null;
		Transaction tx = null;
		
		try
		{
			Date lastUpdatedDate=null;
			hSession = HibernateBridge.getSessionFactory().openSession();
			List everyDayWeeklyList = hSession.createQuery("select distinct mutualFundId from EveryDayCandle where candleType=0 order by mutualFundId").list();
			Fractal fractal = null;
			EveryDayCandle edc=null;
			long mfId=0;
			List<EveryDayCandle> edcList=null;
			Iterator itr1=null;
			Iterator itr = everyDayWeeklyList.iterator();
			while (itr.hasNext())
			{
				mfId = (Long) itr.next();
				
				System.out.println("update weeklyFractal with period=1 for mfId="+mfId);
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from Fractal where mutualFundId=? and candleType=0 and period=1 and upFractal!=0").setParameter(0, mfId).setMaxResults(1).uniqueResult();
				if(lastUpdatedDate==null)
				{
					lastUpdatedDate=(Date)hSession.createQuery("select min(tradeDate) from EveryDayCandle where mutualFundId=? and candleType=0").setParameter(0, mfId).setMaxResults(1).uniqueResult();
				}
				if(lastUpdatedDate==null)
				{
					throw new Exception("(updateWeeklyFractalperiod1AfterDate method called)no data with Fractal is available in Fractal for mfId="+mfId+" hence not able to update fractal weekly with period=1");
				}
				edcList= hSession.createQuery("from EveryDayCandle where candleType=0 and mutualFundId=? and tradeDate>=? order by tradeDate").setParameter(0, mfId).setParameter(1,lastUpdatedDate).list();
				tx = hSession.beginTransaction();
				itr1 = edcList.iterator();
				while (itr1.hasNext())
				{
					edc= (EveryDayCandle) itr1.next();
					fractal = new Fractal();
					fractal.setMutualFundId(mfId);
					fractal.setCandleType(0);
					fractal.setPeriod(1);
					fractal.setTradeDate(edc.getTradeDate());
					fractal.setOpen(edc.getOpen());
					fractal.setClosePrice(edc.getTodaysClosePrice());
					fractal.setHigh(edc.getHigh());
					fractal.setLow(edc.getLow());
					hSession.saveOrUpdate(fractal);
				}
				hSession.flush();
				tx.commit();
				System.gc();
			}	
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateWeeklyUpFractalPeriod1(Long min,Long max)
	{
		System.out.println("Updating weekly UpFractal for periodd=1");
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
						
			List threeDaysFractalList = null;
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from Fractal where candleType=? and mutualFundId>=? and mutualFundId<=?").setParameter(0,0).setParameter(1,min).setParameter(2, max).list();
			Iterator itr = mutualFundList.iterator();
			long mfId=0;
			List fractalList=null;
			while (itr.hasNext())
			{
				mfId = (Long) itr.next();
				System.out.println("update weekly upFractal for period=1 mutualFunId = "+mfId);
				fractalList = hSession.createQuery("from Fractal where candleType=? and period=? and mutualFundId=? order by tradeDate").setParameter(0, 0).setParameter(1, 1).setParameter(2, mfId).list();

				int size = fractalList.size();

				// System.out.println("FractalSize=" + fractalList.size());
				Fractal fractal = null;
				tx = hSession.beginTransaction();
				for (int i = 2; i < size; i++)
				{
					
					Fractal upFractal = null;
					threeDaysFractalList = new ArrayList<Fractal>();
					for (int j = i - 2; j <= i; j++)
					{
						if(i - j == 1)
						{
							upFractal = (Fractal) fractalList.get(j);
						}
						threeDaysFractalList.add((Fractal) fractalList.get(j));
					}
					Iterator itr2 = threeDaysFractalList.iterator();
					// System.out.println("Up Fractal Date = " + upFractal.getTradeDate());
					/*while (itr2.hasNext())
					{
						Fractal f = (Fractal) itr2.next();
						System.out.println(f.getTradeDate() + "\t\t" + f.getHigh());
					}*/

					// flag for equal or less than neighbour values in fractal list of 3
					boolean flagForEqualOrGreaterNeighborValuesInFractal = false;
					// List<Double> fiveDaysFractalPriceList = new ArrayList<Double>();
					// int fiveDaysFractalListSize = fiveDaysFractalList.size();
					Iterator it = threeDaysFractalList.iterator();

					double upFractalPrice = upFractal.getHigh();
					Date upFractalDate = upFractal.getTradeDate();
					while (it.hasNext())
					{
						Fractal fr = (Fractal) it.next();
						if(upFractalPrice == fr.getHigh() && upFractalDate.compareTo(fr.getTradeDate()) == 0)
						{
							continue;
						}
						if(fr.getHigh() >= upFractalPrice)
						{
							flagForEqualOrGreaterNeighborValuesInFractal = true;
							break;
						}
					}
					Fractal currentFractal = null;
					if(flagForEqualOrGreaterNeighborValuesInFractal == true)
					{
						Fractal previousToCurrentFractal = (Fractal) threeDaysFractalList.get(1);
						currentFractal = (Fractal) threeDaysFractalList.get(2);
						currentFractal.setUpFractal(previousToCurrentFractal.getUpFractal());
						currentFractal.setMaxFractalDate(previousToCurrentFractal.getMaxFractalDate());
						// System.out.println("Neighbour value is greater or equal than upFractal");
						// System.out.println(currentFractal.getUpFractal() + "\t" + currentFractal.getMaxFractalDate());
					}
					else
					{
						currentFractal = (Fractal) threeDaysFractalList.get(2);
						currentFractal.setUpFractal(upFractal.getHigh());
						currentFractal.setMaxFractalDate(upFractal.getTradeDate());
						// System.out.println("UpFractal price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getUpFractal() + "Date =" + currentFractal.getMaxFractalDate());
					}
					flagForEqualOrGreaterNeighborValuesInFractal = false;

					// System.out.println("================================================");
					hSession.saveOrUpdate(currentFractal);
				}
				hSession.flush();
				tx.commit();
				System.gc();
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}
	
	public void updateWeeklyUpLowFractalPeriod1AfterDate()
	{
		System.out.println("updating weekly upFractal/lowFractal for period = 1");
		Session hSession = null;
		Transaction tx = null;
		Date lastUpdatedDate=null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			List mfList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where candleType=?").setParameter(0, 0).list();
			Iterator itr = mfList.iterator();
			while (itr.hasNext())
			{
				long mfId = (Long) itr.next();
				
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from Fractal where mutualFundId=? and candleType=0 and period=1 and upFractal!=0 and lowFractal!=0").setParameter(0, mfId).setMaxResults(1).uniqueResult();
				if(lastUpdatedDate==null)
				{
					List l=hSession.createQuery("select tradeDate from Fractal where mutualFundId=? and candleType=0 and period=1 order by tradeDate").setParameter(0, mfId).setMaxResults(3).list();
					if(l.size()<3)
						continue;
					lastUpdatedDate=(Date)l.get(2);
				}			
				System.out.println("updating weekly upFractal/lowFractal with period=1 mutualFund="+mfId+"  lastUpdatedDate="+lastUpdatedDate);
				
				// transaction for upFractal and lowFractal
				tx = hSession.beginTransaction();
				List<Date> twoDaysBackFromSdateList = hSession.createQuery("select tradeDate from Fractal where mutualFundId=? and period=1 and tradeDate<? and candleType=0 order by tradeDate desc").setParameter(0, mfId).setParameter(1, lastUpdatedDate).setMaxResults(2).list();

				Date newStartDate = twoDaysBackFromSdateList.get(1);

				//Fractal checkFractalForPreviousToStartate = (Fractal) hSession.createQuery("from Fractal where mutualFundId=? and period=1 and tradeDate=? and candleType=0").setParameter(0, mfId).setParameter(2, ((Date) fourDaysBackFromSdateList.get(0))).uniqueResult();
				System.out.println("new startdate="+newStartDate);
				List fractalList = hSession.createQuery("from Fractal where period=1 and mutualFundId=? and tradeDate>=? and candleType=0 order by tradeDate").setParameter(0, mfId).setParameter(1, newStartDate).list();
				
				/*Iterator t=fractalList.iterator();
				while(t.hasNext())
				{
					Fractal f=(Fractal)t.next();
					System.out.println("Date="+f.getTradeDate()+" close="+f.getClosePrice()+" f.period="+f.getPeriod());
				}*/
				int size = fractalList.size();
				List<Fractal> threeDaysFractalList = null;
				for (int i = 2; i < size; i++)
				{
					Fractal upFractal = null;
					Fractal lowFractal = null;
					threeDaysFractalList = new ArrayList<Fractal>();
					for (int j = i - 2; j <= i; j++)
					{
						if(i - j == 1)
						{
							upFractal = (Fractal) fractalList.get(j);
							lowFractal = (Fractal) fractalList.get(j);
						}
						threeDaysFractalList.add((Fractal) fractalList.get(j));
					}
					
					// insert previous maxFractal,maxFractalDate if there is no unique max date from the 3 days data in threeDaysFractalList
					// flag for (equal or less),(equal or greater) than neighbour values in fractal list of 3
					boolean flagForEqualOrGreaterNeighborValuesInFractal = false;
					boolean flagForEqualOrLessNeighborValuesInFractal = false;
					

					double upFractalPrice = upFractal.getHigh();
					Date upFractalDate = upFractal.getTradeDate();
					double lowFractalPrice = lowFractal.getLow();
					Date lowFractalDate = lowFractal.getTradeDate();
					Iterator it = threeDaysFractalList.iterator();
					while (it.hasNext())
					{
						Fractal fr = (Fractal) it.next();
						if(upFractalPrice == fr.getHigh() && upFractalDate.compareTo(fr.getTradeDate()) == 0)
						{
							continue;
						}
						if(fr.getHigh() >= upFractalPrice)
						{
							flagForEqualOrGreaterNeighborValuesInFractal = true;
							break;
						}
					}
					it = threeDaysFractalList.iterator();
					while (it.hasNext())
					{
						Fractal fr = (Fractal) it.next();
						if(lowFractalPrice == fr.getLow() && lowFractalDate.compareTo(fr.getTradeDate()) == 0)
						{
							continue;
						}
						if(fr.getLow() <= lowFractalPrice)
						{
							flagForEqualOrLessNeighborValuesInFractal = true;
							break;
						}
					}
					Fractal currentFractal = null;
					if(flagForEqualOrLessNeighborValuesInFractal == true)
					{
						Fractal previousToCurrentFractal = (Fractal) threeDaysFractalList.get(1);
						currentFractal = (Fractal) threeDaysFractalList.get(2);
						currentFractal.setLowFractal(previousToCurrentFractal.getLowFractal());
						currentFractal.setMinFractalDate(previousToCurrentFractal.getMinFractalDate());
						// System.out.println("Neighbour value is less or equal than lowFractal");
						// System.out.println(currentFractal.getLowFractal() + "\t" + currentFractal.getMinFractalDate());
					}
					else
					{
						currentFractal = (Fractal) threeDaysFractalList.get(2);
						currentFractal.setLowFractal(lowFractal.getLow());
						currentFractal.setMinFractalDate(lowFractal.getTradeDate());
						// System.out.println("LowFractal price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getLowFractal() + "Date =" + currentFractal.getMinFractalDate());
					}
					if(flagForEqualOrGreaterNeighborValuesInFractal == true)
					{
						Fractal previousToCurrentFractal = (Fractal) threeDaysFractalList.get(1);
						currentFractal = (Fractal) threeDaysFractalList.get(2);
						currentFractal.setUpFractal(previousToCurrentFractal.getUpFractal());
						currentFractal.setMaxFractalDate(previousToCurrentFractal.getMaxFractalDate());
						// System.out.println("Neighbour value is greater or equal than upFractal");
						//System.out.println(currentFractal.getUpFractal() + "\t" + currentFractal.getMaxFractalDate()+"\t"+currentFractal.getMaxFractalDate());
					}
					else
					{
						currentFractal = (Fractal) threeDaysFractalList.get(2);
						currentFractal.setUpFractal(upFractal.getHigh());
						currentFractal.setMaxFractalDate(upFractal.getTradeDate());
						//System.out.println("UpFractal with period=1 price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getUpFractal() + "Date =" + currentFractal.getMaxFractalDate());
					}
					
					flagForEqualOrGreaterNeighborValuesInFractal = false;
					flagForEqualOrLessNeighborValuesInFractal = false;
					// System.out.println("================================================");
					hSession.saveOrUpdate(currentFractal);

				}
				tx.commit();
			}
					

		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}	
	}

	public void updateWeeklyLowFractalPeriod1(Long min,Long max)
	{
		System.out.println("Updating weekly lowFractal for period=1");
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			Fractal fractal = null;
			List threeDaysFractalList = null;
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from Fractal where candleType=? and mutualFundId>=? and mutualFundId<=?").setParameter(0,0).setParameter(1,min).setParameter(2,max).list();
			Iterator itr = mutualFundList.iterator();
			while (itr.hasNext())
			{
				long mfId = (Long) itr.next();
				System.out.println("update weekly lowFractal for period=1 mutualFundId = "+mfId);
				List fractalList = hSession.createQuery("from Fractal where period=? and mutualFundId=? and candleType=? order by tradeDate").setParameter(0, 1).setParameter(1, mfId).setParameter(2, 0).list();

				int size = fractalList.size();
				tx = hSession.beginTransaction();
				// System.out.println("FractalSize=" + fractalList.size());
				for (int i = 2; i < size; i++)
				{
					Fractal lowFractal = null;
					threeDaysFractalList = new ArrayList<Fractal>();
					for (int j = i - 2; j <= i; j++)
					{
						if(i - j == 1)
						{
							lowFractal = (Fractal) fractalList.get(j);
						}
						threeDaysFractalList.add((Fractal) fractalList.get(j));
					}
					Iterator itr2 = threeDaysFractalList.iterator();
					// System.out.println("Up Fractal Date = " + upFractal.getTradeDate());
					/*while (itr2.hasNext())
					{
						Fractal f = (Fractal) itr2.next();
						System.out.println(f.getTradeDate() + "\t\t" + f.getLow());
					}*/
					// flag for equal or less than neighbour values in fractal list of 5
					boolean flagForEqualOrLessNeighborValuesInFractal = false;
					Iterator it = threeDaysFractalList.iterator();

					double lowFractalPrice = lowFractal.getLow();
					Date lowFractalDate = lowFractal.getTradeDate();
					while (it.hasNext())
					{
						Fractal fr = (Fractal) it.next();
						if(lowFractalPrice == fr.getLow() && lowFractalDate.compareTo(fr.getTradeDate()) == 0)
						{
							continue;
						}
						if(fr.getLow() <= lowFractalPrice)
						{
							flagForEqualOrLessNeighborValuesInFractal = true;
							break;
						}
					}
					Fractal currentFractal = null;
					if(flagForEqualOrLessNeighborValuesInFractal == true)
					{
						Fractal previousToCurrentFractal = (Fractal) threeDaysFractalList.get(1);
						currentFractal = (Fractal) threeDaysFractalList.get(2);
						currentFractal.setLowFractal(previousToCurrentFractal.getLowFractal());
						currentFractal.setMinFractalDate(previousToCurrentFractal.getMinFractalDate());
						// System.out.println("Neighbour value is lesser or equal than lowFractal");
						// System.out.println(currentFractal.getLowFractal() + "\t" + currentFractal.getMinFractalDate());
					}
					else
					{
						currentFractal = (Fractal) threeDaysFractalList.get(2);
						//currentFractal.setLowFractal(lowFractal.getLow());
						//currentFractal.setMinFractalDate(lowFractal.getTradeDate());
						currentFractal.setLowFractal(lowFractalPrice);
						currentFractal.setMinFractalDate(lowFractalDate);
						// System.out.println("LowFractal price for Current" + currentFractal.getTradeDate() + " Fractal=" + currentFractal.getUpFractal() + "Date =" + currentFractal.getMaxFractalDate());
					}
					flagForEqualOrLessNeighborValuesInFractal = false;
					// System.out.println("================================================");
					hSession.saveOrUpdate(currentFractal);
				}
				hSession.flush();
				tx.commit();
				System.gc();
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}

	}
	
	//we dont want to generate tcfl1 or swing for period 1 but we might need in future
	
	/*public void updateWeeklySwingPeriod1(Long min,Long max)
	{
		System.out.println("Updating weekly swing for period=1");
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from Fractal where candleType=? and mutualFundId>=? and mutualFundId<=?").setParameter(0,0).setParameter(1,min).setParameter(2,max).list();
			Iterator itr = mutualFundList.iterator();

			while (itr.hasNext())
			{
				System.gc();
				long mfId = (Long) itr.next();
				List<Fractal> fractalList = hSession.createQuery("from Fractal where mutualFundId=? and candleType=? and period=1 order by tradeDate").setParameter(0, mfId).setParameter(1, 0).list();
				System.out.println("Weekly swing update for period=1 and mutualFundId ="+mfId);
				Fractal fractal = null;
				tx = hSession.beginTransaction();
				for (int i = 0; i < fractalList.size(); i++)
				{
					boolean swingTrend = false;
					double tcfl1 = 0, tcfl2 = 0, tcfl3 = 0;
					fractal = fractalList.get(i);
					Double swingHigh, swingLow;
					Date swingLowDate, swingHighDate;
					Date tradeDate = fractal.getTradeDate();
					Calendar cal = Calendar.getInstance();
					cal.setTime(tradeDate);
					cal.add(Calendar.MONTH, -3);
					System.out.println("Swing date period=1="+tradeDate+"  mutualFundId="+mfId);
					Object[] fractalMax = (Object[]) hSession.createQuery("select max(upFractal),min(maxFractalDate) from Fractal where upFractal =(select max(upFractal) from Fractal where mutualFundId=? and tradeDate>=? and tradeDate<=? and upFractal!=? and candleType=? and period=1 order by tradeDate) and mutualFundId=? and period=1")
							.setParameter(0, mfId).setParameter(1, cal.getTime()).setParameter(2, tradeDate).setParameter(3, 0d).setParameter(4, 0).setParameter(5, mfId).uniqueResult();
					if(fractalMax[0] == null || fractalMax[1] == null)
					{
						continue;
					}
					else
					{
						swingHigh = (Double) fractalMax[0];
						swingHighDate = (Date) fractalMax[1];
						// System.out.println("shd=" + swingHighDate);

						fractal.setSwingHigh(swingHigh);
						fractal.setSwingHighDate(swingHighDate);
					}
					Object[] fractalMin = (Object[]) hSession.createQuery("select min(lowFractal),min(minFractalDate) from Fractal where lowFractal =(select min(lowFractal) from Fractal where mutualFundId=? and tradeDate>=? and tradeDate<=? and lowFractal!=? and candleType=? and period=1 order by tradeDate desc) and mutualFundId=? and period=1")
							.setParameter(0, mfId).setParameter(1, cal.getTime()).setParameter(2, tradeDate).setParameter(3, 0d).setParameter(4, 0).setParameter(5, mfId).uniqueResult();
					if(fractalMin[0] == null || fractalMin[1] == null)
					{
						continue;
					}
					else
					{
						swingLow = (Double) fractalMin[0];
						swingLowDate = (Date) fractalMin[1];
						fractal.setSwingLow(swingLow);
						fractal.setSwingLowDate(swingLowDate);

					}
					if(swingHighDate.before(swingLowDate))
					{
						fractal.setSwingTrend("Low");
						tcfl1 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.382)).floatValue(), 3);
						tcfl2 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.5)).floatValue(), 3);
						tcfl3 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.618)).floatValue(), 3);
					}
					else if(swingLowDate.before(swingHighDate))
					{
						fractal.setSwingTrend("High");
						tcfl1 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.382)).floatValue(), 3);
						tcfl2 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.5)).floatValue(), 3);
						tcfl3 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.618)).floatValue(), 3);
					}
					fractal.setTcfl1(tcfl1);
					fractal.setTcfl2(tcfl2);
					fractal.setTcfl3(tcfl3);

					hSession.save(fractal);
				}
				hSession.flush();
				tx.commit();
				System.gc();
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}

	}
*/
	

	public void updateCurrentWeeklyFractal(Session hSession)
	{
		Transaction tx = null;
		try
		{

			List mfList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where candleType=?").setParameter(0, 0).list();

			Iterator itr = mfList.iterator();
			while (itr.hasNext())
			{
				long mfId = (Long) itr.next();
				List subQuery = hSession.createQuery("select tradeDate from Fractal where mutualFundId=? and period =? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 2).setParameter(2, 0).setMaxResults(4).list();
				// System.out.println("size" + subQuery.size());
				List lastFourFractalList = hSession.createQuery("from Fractal where period=? and mutualFundId=? and candleType=? and tradeDate in (:subQuery) order by tradeDate").setParameter(0, 2).setParameter(1, mfId).setParameter(2, 0).setParameterList("subQuery", subQuery).list();
				// System.out.println("size" + lastFourFractalList.size());

				Fractal lastFractal = (Fractal) lastFourFractalList.get(3);
				Date lastFractalDate = lastFractal.getTradeDate();
				List everyDayCandleList = hSession.createQuery("from EveryDayCandle where candleType=? and  mutualFundId=? and tradeDate>?").setParameter(0, 0).setParameter(1, mfId).setParameter(2, lastFractalDate).list();
				if(everyDayCandleList.isEmpty())
				{
					throw new Exception("(While updating weekly part of fractal)Update EveryDayCandle table with current date/Current Table is already updated (for weekly)");
				}
				if(everyDayCandleList.size() > 1)
				{
					throw new Exception("(While updating weekly part of fractal)Current table missed for more than 1 day : update datewise with " + (new SimpleDateFormat("dd-MM-yyyy").format(lastFractalDate)) + " inside updateCurrentFridayFractal");
				}
				else if(everyDayCandleList.size() == 1)
				{
					tx = hSession.beginTransaction();
					EveryDayCandle edc = (EveryDayCandle) everyDayCandleList.get(0);
					Fractal newFractal = new Fractal();
					newFractal.setMutualFundId(mfId);
					newFractal.setTradeDate(edc.getTradeDate());
					newFractal.setCandleType(0);
					newFractal.setPeriod(2);
					newFractal.setClosePrice(edc.getTodaysClosePrice());
					newFractal.setHigh(edc.getHigh());
					newFractal.setLow(edc.getLow());
					newFractal.setOpen(edc.getOpen());
					hSession.saveOrUpdate(newFractal);
					tx.commit();

					// below tx is used to store upFractal and lowFractal for currentDay of daily type i.e candleType=1
					tx = hSession.beginTransaction();
					boolean flagForEqualOrGreaterNeighborValuesInFractal = false;
					List lastFiveFractalList = lastFourFractalList;
					lastFiveFractalList.add(newFractal);
					Fractal upFractal = (Fractal) lastFiveFractalList.get(2);
					double upFractalPrice = upFractal.getHigh();
					Date upFractalDate = upFractal.getTradeDate();
					Iterator it = lastFiveFractalList.iterator();
					while (it.hasNext())
					{
						Fractal fr = (Fractal) it.next();
						if(upFractalPrice == fr.getHigh() && upFractalDate.compareTo(fr.getTradeDate()) == 0)
						{
							continue;
						}
						if(fr.getHigh() >= upFractalPrice)
						{
							flagForEqualOrGreaterNeighborValuesInFractal = true;
							break;
						}
					}
					Fractal currentFractal = null;
					if(flagForEqualOrGreaterNeighborValuesInFractal == true)
					{
						Fractal previousToCurrentFractal = (Fractal) lastFiveFractalList.get(3);
						currentFractal = (Fractal) lastFiveFractalList.get(4);
						currentFractal.setUpFractal(previousToCurrentFractal.getUpFractal());
						currentFractal.setMaxFractalDate(previousToCurrentFractal.getMaxFractalDate());
						System.out.println("Neighbour value is greater or equal than upFractal");
					}
					else
					{
						currentFractal = (Fractal) lastFiveFractalList.get(4);
						currentFractal.setUpFractal(upFractal.getHigh());
						currentFractal.setMaxFractalDate(upFractal.getTradeDate());
						System.out.println("UpFractal price=" + upFractal.getUpFractal());
					}
					flagForEqualOrGreaterNeighborValuesInFractal = false;


					// below to update fractal with lowFractal
					boolean flagForEqualOrLessNeighborValuesInFractal = false;


					Fractal lowFractal = (Fractal) lastFiveFractalList.get(2);
					double lowFractalPrice = upFractal.getLow();
					Date lowFractalDate = upFractal.getTradeDate();
					it = lastFiveFractalList.iterator();
					while (it.hasNext())
					{
						Fractal fr = (Fractal) it.next();
						if(lowFractalPrice == fr.getLow() && lowFractalDate.compareTo(fr.getTradeDate()) == 0)
						{
							continue;
						}
						if(fr.getLow() <= lowFractalPrice)
						{
							flagForEqualOrLessNeighborValuesInFractal = true;
							break;
						}
					}
					currentFractal = null;
					if(flagForEqualOrLessNeighborValuesInFractal == true)
					{
						Fractal previousToCurrentFractal = (Fractal) lastFiveFractalList.get(3);
						currentFractal = (Fractal) lastFiveFractalList.get(4);
						currentFractal.setLowFractal(previousToCurrentFractal.getLowFractal());
						currentFractal.setMinFractalDate(previousToCurrentFractal.getMinFractalDate());
						// System.out.println("Neighbour value is lesser or equal than lowFractal");
					}
					else
					{
						currentFractal = (Fractal) lastFiveFractalList.get(4);
						currentFractal.setLowFractal(lowFractal.getLow());
						currentFractal.setMinFractalDate(lowFractal.getTradeDate());
						// System.out.println("LowFractal price=" + lowFractal.getUpFractal());
					}
					flagForEqualOrLessNeighborValuesInFractal = false;
					hSession.saveOrUpdate(currentFractal);
				}
			}
			tx.commit();
		}
		catch (Exception e)
		{
			System.out.println("Error");
			e.printStackTrace();
			if(tx != null)
				tx.rollback();
		}
		// not closing hSession because it comes from calling function(this calling function closes the hSession)

	}

	
	public void updateCurrentWeeklySwing(Session hSession)
	{
		Transaction tx = hSession.beginTransaction();
		try
		{
			List mfList = hSession.createQuery("select distinct(mutualFundId) from Fractal").list();
			Iterator itr = mfList.iterator();
			while (itr.hasNext())
			{
				long mfId = (Long) itr.next();

				List f = hSession.createQuery("from Fractal where mutualFundId=? and period=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 2).setParameter(2, 0).setMaxResults(1).list();
				if(f.size() == 0)
				{
					System.out.println("(While updating weekly current swing)missing fund from fractal(while updating currennt swing)=" + mfId);
					continue;
				}
				// System.out.println("swing high=" + f.getSwingHigh());
				// System.out.println(((Fractal) f.get(0)).getClosePrice());
				Fractal fractal = (Fractal) f.get(0);
				Date currentDate = fractal.getTradeDate();
				Calendar cal = Calendar.getInstance();
				cal.setTime(currentDate);
				cal.add(Calendar.MONTH, -3);
				List fractalSwingHigh = hSession.createQuery("from Fractal where tradeDate<=? and tradedate>=? and mutualFundId=? and candleType=? order by upFractal desc").setParameter(0, currentDate).setParameter(1, cal.getTime()).setParameter(2, mfId).setParameter(3, 0).setMaxResults(1).list();
				double swingHigh = ((Fractal) fractalSwingHigh.get(0)).getUpFractal();
				Date swingHighDate = ((Fractal) fractalSwingHigh.get(0)).getMaxFractalDate();
				fractal.setSwingHigh(swingHigh);
				fractal.setSwingHighDate(swingHighDate);

				List fractalSwingLow = hSession.createQuery("from Fractal where tradeDate<=? and tradedate>=? and lowFractal!=? and mutualFundId=? and candleType=? order by lowFractal").setParameter(0, currentDate).setParameter(1, cal.getTime()).setParameter(2, 0d).setParameter(3, mfId)
						.setParameter(4, 0).setMaxResults(1).list();
				double swingLow = ((Fractal) fractalSwingLow.get(0)).getLowFractal();
				Date swingLowDate = ((Fractal) fractalSwingLow.get(0)).getMinFractalDate();
				fractal.setSwingLow(swingLow);
				fractal.setSwingLowDate(swingLowDate);

				// System.out.println("curr=" + currentDate + " 3 months before=" + cal.getTime());
				// System.out.println("swing high Date=" + swingHighDate + "swing Low Date=" + swingLowDate);
				double tcfl1 = 0, tcfl2 = 0, tcfl3 = 0;
				if(swingHighDate.before(swingLowDate))
				{
					fractal.setSwingTrend("Low");
					tcfl1 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.382)).floatValue(), 3);
					tcfl2 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.5)).floatValue(), 3);
					tcfl3 = ImageLinks.getRoundedValue(((Double) (swingLow + (swingHigh - swingLow) * 0.618)).floatValue(), 3);
				}
				else if(swingLowDate.before(swingHighDate))
				{
					fractal.setSwingTrend("High");
					tcfl1 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.382)).floatValue(), 3);
					tcfl2 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.5)).floatValue(), 3);
					tcfl3 = ImageLinks.getRoundedValue(((Double) (swingHigh - (swingHigh - swingLow) * 0.618)).floatValue(), 3);
				}
				fractal.setTcfl1(tcfl1);
				fractal.setTcfl2(tcfl2);
				fractal.setTcfl3(tcfl3);
				hSession.saveOrUpdate(fractal);
			}
			tx.commit();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
	}
	
}
