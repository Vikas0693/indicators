package com.fundexpert.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.CustomMFExitAlert;
import com.fundexpert.dao.EveryDayCandle;
import com.fundexpert.dao.FlatPatterns;
import com.fundexpert.dao.Fractal;
import com.robo.config.Config;

public class CustomMFExitAlertController {

	public CustomMFExitAlertController()
	{	}
	
	public List<CustomMFExitAlert> getExitAlerts(long mutualFundId,String sDate,String eDate)
	{
		Session hSession=null;
		
		try
		{
			Date startDate=new SimpleDateFormat("yyyy-MM-dd").parse(sDate);
			Date endDate=new SimpleDateFormat("yyyy-MM-dd").parse(eDate);
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<CustomMFExitAlert> exitList=hSession.createQuery("from CustomMFExitAlert where mutualFundId=? and tradeDate between ? and ? and strategyName in (:list) order by tradeDate").setParameter(0, mutualFundId).setParameter(1, startDate).setParameter(2, endDate).setParameterList("list", getValidExitList(mutualFundId,hSession)).list();
			return exitList;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
	
	public List getValidExitList(long mutualFundId,Session hSession)
	{
		List<String> exitList=new ArrayList<String>();
		String exit="";
		try
		{
			Config config=new Config();
			exit=config.getProperty("exit");
			
			if(!exit.isEmpty())
			{		
				String[] exitArray=exit.split(",");
				for(int i=0;i<exitArray.length;i++)
				{
					exitArray[i]="ExitStrategy"+exitArray[i];
				}
				Collections.addAll(exitList,exitArray);
				return exitList;
			}	
			else
			{
				return exitList;
			}
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return null;
	}
	
	public void updateAllExitAlertsAfterDate() throws Exception
	{
		updateExit1AfterDate();
		updateExit2AfterDate();
		updateExit3AfterDate();
		updateExit4AfterDate();
		updateExit5AfterDate();
		updateExit6AfterDate();
		updateExit7AfterDate();
	}
	
	private void updateExit1AfterDate() throws Exception
	{
		Session hSession=null;
		Transaction tx=null;
		System.out.println("updating exit 1");
		try {
				String strategyName="ExitStrategy1";
				MutualFundController mfc=new MutualFundController();
				List<Object[]> mfList=mfc.getMutualFundList();
				Iterator iterator=mfList.iterator();
				int count=0;
				hSession=HibernateBridge.getSessionFactory().openSession();
				while(iterator.hasNext())
				{
					
					tx=hSession.beginTransaction();
					count++;
					Object object[]=(Object[])iterator.next();
					Long mutualFundId=(Long)object[0];
					
					Date lastExit1Date=(Date)hSession.createQuery("SELECT max(tradeDate) from CustomMFExitAlert where mutualFundId=? and strategyName='ExitStrategy1'").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
					if(lastExit1Date==null)
					{
						lastExit1Date=(Date)hSession.createQuery("select min(tradeDate) from EveryDayCandle where mutualFundId=? and candleType=0").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
					}
					if(lastExit1Date==null)
					{
						continue;
					}					
					List<EveryDayCandle> weeklyCandleList=hSession.createQuery("from EveryDayCandle where mutualFundId=? and candleType=? and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,0).setParameter(2,lastExit1Date).list();
					// This you need to write on your own, i need Weekly RSI 70 value for the symbol
					// CustomMFExitAlert is the object in which we are persisting Exit dates for different exit startegy 
		            boolean watchActivatedCond1Met = false; // we have to persist this when this code moves to DataController
		            int countWatchCond1 = 0;
		            int countWatchTotalCond1 = 0;
		            boolean watchActivatedCond2 = false;
		
		            int countWatchTotalCond2 = 0;
					
		            if (weeklyCandleList != null && weeklyCandleList.size() != 0) {
		                for (EveryDayCandle weeklyCandle : weeklyCandleList) {
		                	//System.out.println("Date="+weeklyCandle.getTradeDate());
		                    if (watchActivatedCond2)  // Checking for final condition
		                    {
		                        if (weeklyCandle.getRsi() < 70) {
		                            countWatchTotalCond2++;
		                            
		                            if (countWatchTotalCond2 >= 4) {
		                                watchActivatedCond2 = false; //restting again for next check
		                                watchActivatedCond1Met = false;//restting again for next check
		                                // EXIT CONDIITON MET, TO Populate Database
		                                //strategyName - "ExitStrategy1"
		                                // symbol - symbol
		                                // tradeDate - weeklyCandleList.get(index).getTradDate()
		                                //reasonForAlert - "as per ExitStrategy1"
		                                // exitPrice  -  weeklyCandleList.get(index).getClosePrice()
		                                //nftyPrice - for Nifty same as weeklyCandleList.get(index).getClosePrice()
		                                Date currentDate=weeklyCandle.getTradeDate();
		                                Calendar cal=Calendar.getInstance();
		                                cal.setTime(currentDate);
		                                cal.add(Calendar.MONTH,-4);
		                                List exit1Within4Month=hSession.createQuery("from CustomMFExitAlert where strategyName = 'ExitStrategy1' and mutualFundId=? and tradeDate between ? and ?").setParameter(0,mutualFundId).setParameter(1,cal.getTime()).setParameter(2,currentDate).list();
		                                if(exit1Within4Month.isEmpty())
		                               	{
		                               		System.out.println("Found Exit 1 Alert for mfid="+mutualFundId+" on date="+currentDate);
			                                CustomMFExitAlert customMFExitAlert = new CustomMFExitAlert();
			                                customMFExitAlert.setMutualFundId(mutualFundId);
			                                customMFExitAlert.setStrategyName(strategyName);
			                                customMFExitAlert.setTradeDate(currentDate);
			                                customMFExitAlert.setReasonForAlert(1);
			                                customMFExitAlert.setExitPrice(weeklyCandle.getTodaysClosePrice());
			                             	hSession.saveOrUpdate(customMFExitAlert);
			                                //customMFExitAlert.setNiftyEntryPrice(weeklyCandle.getClosePrice());
		                               	}	
		                               	
		                            }
		                        } else {
		                            // Second COunter to be reset
		                            countWatchTotalCond2 = 0;
		                        }
		                    }
		
		                    if (!watchActivatedCond1Met && weeklyCandle.getRsi() > 70) {
		                        countWatchCond1++;
		                        if (countWatchCond1 == 1) {
		                            countWatchTotalCond1 = 0;
		                        } else if (countWatchCond1 >= 3) {
		                            watchActivatedCond2 = true;
		                            watchActivatedCond1Met = true;
		                            countWatchTotalCond1 = 0;
		                            countWatchCond1 = 0;
		                        }
		                    }
		
		                    if (!watchActivatedCond1Met)   // Not 70
		                    {
		                        countWatchTotalCond1++;
		                    }		
		                    if (!watchActivatedCond1Met && countWatchTotalCond1 >= 4) {
		                        watchActivatedCond1Met = false;
		                        countWatchCond1 = 0;
		                        countWatchTotalCond1 = 0;
		                    }
		
		                }
		            }
		            tx.commit();
	            }			           
	            BufferedWriter bw=null;
	            try
	            {
	            	File f=new File("/opt/strategy");
	            	if(!f.isDirectory())
	            	{
	            		f.mkdirs();
	            	}
	            	
	            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/exit.txt"),true));
	            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	            	System.out.println("Saving exit 1");
	            	bw.write(entry);
	            	bw.newLine();
	            }
	            catch(Exception e)
	            {
	            	e.printStackTrace();
	            }
	            finally
	            {
	            	bw.close();
	            }
	        } 
			catch (Exception e)
			{
	           e.printStackTrace();
	           if(tx!=null)
	        	   tx.rollback();
	           throw new Exception(e.getMessage());
	        }
			finally
			{
				hSession.close();
			}
		
	}
	
	private void updateExit2AfterDate() throws Exception
	{
		Session hSession=null;
		Transaction transaction=null;
		System.out.println("updating exit 2");
		try
		{
			String strategyName="ExitStrategy2";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			while(iterator.hasNext())
			{
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				
				hSession=HibernateBridge.getSessionFactory().openSession();
				Date lastExit2Date=(Date)hSession.createQuery("SELECT max(tradeDate) from CustomMFExitAlert where mutualFundId=? and strategyName='ExitStrategy2'").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				if(lastExit2Date==null)
				{
					lastExit2Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=1 and period=2 and tcfl1!=0").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				}
				if(lastExit2Date==null)
				{
					continue;
				}			
				List<Fractal> fractalList=hSession.createQuery("from Fractal where mutualFundId=? and period=? and candleType=? and tcfl1!=0 and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,2).setParameter(2,1).setParameter(3, lastExit2Date).list();
				transaction=hSession.beginTransaction();
				for(int i=3;i<fractalList.size();i++)
				{
					Fractal currentFractal=fractalList.get(i);
					double currentClosePrice=currentFractal.getClosePrice();
					Date currentTradeDate=currentFractal.getTradeDate();
					if(currentClosePrice<currentFractal.getTcfl1() && currentClosePrice<currentFractal.getTcfl2() && currentClosePrice<currentFractal.getTcfl3() && currentClosePrice<currentFractal.getLowFractal())
					{
						Fractal previousFractal=fractalList.get(i-1);
						double previousClosePrice=previousFractal.getClosePrice();
						if(previousClosePrice<previousFractal.getTcfl1() && previousClosePrice<previousFractal.getTcfl2() && previousClosePrice<previousFractal.getTcfl3())
						{
							Fractal pPreviousFractal=fractalList.get(i-2);
							double pPreviousClosePrice=pPreviousFractal.getClosePrice();
							double minTCFL=Math.min(Math.min(pPreviousFractal.getTcfl1(),pPreviousFractal.getTcfl2()),pPreviousFractal.getTcfl3());
							if(pPreviousFractal.getTcfl1()!=0 && pPreviousClosePrice>minTCFL)
							{
								boolean flatPatternSSBFound=false;
								Calendar calendar=Calendar.getInstance();
								calendar.setTime(currentTradeDate);
								calendar.add(Calendar.MONTH,-3);
								Date previousThreeMonthDate=calendar.getTime(); 
								
								List flatPatternBeforeTradeDateSsb=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"SSB").setParameter(1,mutualFundId).setParameter(2,20).setParameter(3, 1).setParameter(4,currentTradeDate).setMaxResults(1).list();
								if(flatPatternBeforeTradeDateSsb.size()!=0)
								{
									FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateSsb.get(0);
									Date lastFlatPatternDate=flatPattern.getPatternStartDate();
									
									
									if(lastFlatPatternDate.compareTo(previousThreeMonthDate)<0)
									{
										flatPatternSSBFound=true;
									//	System.out.println("pattern end date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
									}
									else
									{
										double ssb=flatPattern.getPatternValue();
										if(ssb>(currentClosePrice*1.03))
										{
											//System.out.println("ssb true "+mutualFundId+"tradeDate="+tradeDate);
											//System.out.println("pattern date found(Nav checked) alert for mf="+mutualFundId+" for "+tradeDate+"  lastFlatDate="+lastFlatPatternDate);
											flatPatternSSBFound=true;
										}
										else
										{
											//System.out.print("ssb pattern date found ( nav not verified)");
											flatPatternSSBFound=false;
											continue;
										}
									}
								}
								else
								{
									//flatPatternSSBFound=false;
									//System.out.println("No pattern date found alert for mf="+mutualFundId+" for "+tradeDate);
									flatPatternSSBFound=true;
								}
								if(flatPatternSSBFound==true)
								{
									System.out.println("Exit 2 alert for ="+mutualFundId+"  on ="+currentTradeDate);
									CustomMFExitAlert customMFExitAlert = new CustomMFExitAlert();
	                                customMFExitAlert.setMutualFundId(mutualFundId);
	                                customMFExitAlert.setStrategyName(strategyName);
	                                customMFExitAlert.setTradeDate(currentTradeDate);
	                                customMFExitAlert.setReasonForAlert(1);
	                                customMFExitAlert.setExitPrice(currentClosePrice);
	                               	hSession.saveOrUpdate(customMFExitAlert);
								}
								
							}
						}
					}
				}
				transaction.commit();
			}
			BufferedWriter bw=null;
			try
			{
				File f=new File("/opt/strategy");
				if(!f.isDirectory())
				{
					f.mkdirs();
				}
			
				bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/exit.txt"),true));
				String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
				System.out.println("Saving exit 2");
				bw.write(entry);
			  	bw.newLine();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				bw.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(transaction!=null)
				transaction.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}
	
	private void updateExit3AfterDate() throws Exception
	{
		Session hSession=null;
		Transaction tx=null;
		System.out.println("updating exit 3");
		try
		{
			String strategyName="ExitStrategy3";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			hSession=HibernateBridge.getSessionFactory().openSession();
			while(iterator.hasNext())
			{
				
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				//System.out.println("MfId="+mutualFundId);
				Date lastExit3Date=(Date)hSession.createQuery("SELECT max(tradeDate) from CustomMFExitAlert where mutualFundId=? and strategyName='ExitStrategy3'").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				if(lastExit3Date==null)
				{
					lastExit3Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=1 and period=2 and tcfl1!=0").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				}
				if(lastExit3Date==null)
				{
					continue;
				}	
				List<Fractal> fractalList=hSession.createQuery("from Fractal where mutualFundId=? and period=? and candleType=? and tcfl1!=? and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1, 2).setParameter(2,1).setParameter(3,0d).setParameter(4,lastExit3Date).list();
				if(fractalList!=null)
				{
					tx=hSession.beginTransaction();
					for(int i=1;i<fractalList.size();i++)
					{
						Fractal currentFractal=fractalList.get(i);
						Date currentTradeDate=currentFractal.getTradeDate();
						double currentClosePrice=currentFractal.getClosePrice();
						//System.out.println("Mf-"+mutualFundId+"CurrentDate="+currentTradeDate);
						if(currentFractal.getTcfl1()!=0 && currentClosePrice<currentFractal.getTcfl1() && currentClosePrice<currentFractal.getTcfl2() && currentClosePrice<currentFractal.getTcfl3())
						{
							//System.out.println("Date1="+currentTradeDate+"    \t"+mutualFundId);
							List flatPatternSSBList=hSession.createQuery("from FlatPatterns where mutualFundId=? and patternName=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,mutualFundId).setParameter(1,"SSB").setParameter(2,20).setParameter(3,1).setParameter(4,currentTradeDate).setMaxResults(1).list();
							if((flatPatternSSBList!=null && flatPatternSSBList.size()!=0))
							{
								double ssbPatternValue=((FlatPatterns)flatPatternSSBList.get(0)).getPatternValue();
								Date lastFlatPatternDate=((FlatPatterns)flatPatternSSBList.get(0)).getPatternStartDate();
								
								Calendar calendar=Calendar.getInstance();
								calendar.setTime(currentTradeDate);
								calendar.add(Calendar.MONTH,-4);
								Date previousThreeMonthDate=calendar.getTime(); 
															
								if(lastFlatPatternDate.compareTo(previousThreeMonthDate)>0 && currentClosePrice<ssbPatternValue)
								{
									//System.out.println("Date2="+currentTradeDate);
									Fractal previousFractal=fractalList.get(i-1);
									if(previousFractal.getClosePrice()>ssbPatternValue)
									{
										System.out.println("Exit 3 Alert for="+mutualFundId+" for date="+currentTradeDate);
										CustomMFExitAlert customMFExitAlert=new CustomMFExitAlert();
										customMFExitAlert.setMutualFundId(mutualFundId);
										customMFExitAlert.setStrategyName(strategyName);
										customMFExitAlert.setReasonForAlert(1);
										customMFExitAlert.setTradeDate(currentTradeDate);
										customMFExitAlert.setExitPrice(currentClosePrice);
										hSession.saveOrUpdate(customMFExitAlert);
									}
								}
							}
						}
					}
					hSession.flush();
					tx.commit();
				}
			}

			BufferedWriter bw=null;
			try
			{
        	  	File f=new File("/opt/strategy");
            	if(!f.isDirectory())
            	{
            		f.mkdirs();
            	}
            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/exit.txt"),true));
          		String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
          		System.out.println("Saving exit 3");
          		bw.write(entry);
          		bw.newLine();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{          	
				bw.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}
	
	private void updateExit4AfterDate() throws Exception
	{
		Session hSession=null;
		Transaction transaction=null;
		System.out.println("updating exit 4");
		try
		{
			String strategyName="ExitStrategy4";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			while(iterator.hasNext())
			{
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				
				hSession=HibernateBridge.getSessionFactory().openSession();
				Date lastExit4Date=(Date)hSession.createQuery("SELECT max(tradeDate) from CustomMFExitAlert where mutualFundId=? and strategyName='ExitStrategy4'").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				if(lastExit4Date==null)
				{
					lastExit4Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=0 and period=2 and tcfl1!=0").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				}
				if(lastExit4Date==null)
				{
					continue;
				}	
				
				List<Fractal> fractalList=hSession.createQuery("from Fractal where mutualFundId=? and period=? and candleType=? and tcfl1!=? and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,2).setParameter(2,0).setParameter(3,0d).setParameter(4,lastExit4Date).list();
				transaction=hSession.beginTransaction();
				if(fractalList!=null && fractalList.size()!=0)
				{
					for(int i=2;i<fractalList.size();i++)
					{
						Fractal currentFractal=fractalList.get(i);
						double currentClosePrice=currentFractal.getClosePrice();
						Date currentTradeDate=currentFractal.getTradeDate();
						if(currentClosePrice<currentFractal.getTcfl1() && currentClosePrice<currentFractal.getTcfl2() && currentClosePrice<currentFractal.getTcfl3())
						{
							Fractal previousFractal=fractalList.get(i-1);
							double previousClosePrice=previousFractal.getClosePrice();
							if(previousFractal.getTcfl1()!=0 && previousClosePrice<previousFractal.getTcfl1() && previousClosePrice<previousFractal.getTcfl2() && previousClosePrice<previousFractal.getTcfl3())
							{
								Fractal pPreviousFractal=fractalList.get(i-2);
								double pPreviousClosePrice=pPreviousFractal.getClosePrice();
								double minTCFL=Math.min(Math.min(pPreviousFractal.getTcfl1(),pPreviousFractal.getTcfl2()),pPreviousFractal.getTcfl3());
								if(pPreviousFractal.getTcfl1()!=0 && pPreviousClosePrice>minTCFL)
								{
										System.out.println("Exit 4 alert for ="+mutualFundId+"  on ="+currentTradeDate);
										CustomMFExitAlert customMFExitAlert = new CustomMFExitAlert();
		                                customMFExitAlert.setMutualFundId(mutualFundId);
		                                customMFExitAlert.setStrategyName(strategyName);
		                                customMFExitAlert.setTradeDate(currentTradeDate);
		                                customMFExitAlert.setReasonForAlert(1);
		                                customMFExitAlert.setExitPrice(currentClosePrice);
		                               	hSession.saveOrUpdate(customMFExitAlert);								
								}
							}
						}
					}
				}
				hSession.flush();
				transaction.commit();
			} 
			BufferedWriter bw=null;
			try
			{
				File f=new File("/opt/strategy");
				if(!f.isDirectory())
				{
					f.mkdirs();
				}
			
				bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/exit.txt"),true));
				String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
				System.out.println("Saving exit 4");
				bw.write(entry);
				bw.newLine();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				bw.close();
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception Occurred in Exit 4");
			e.printStackTrace();
			if(transaction!=null)
				transaction.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}

	}
	
	private void updateExit5AfterDate() throws Exception
	{
		Session hSession=null;
		Transaction transaction=null;
		System.out.println("updating exit 5");
		try
		{
			String strategyName="ExitStrategy5";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			while(iterator.hasNext())
			{
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				hSession=HibernateBridge.getSessionFactory().openSession();
				Date lastExit5Date=(Date)hSession.createQuery("SELECT max(tradeDate) from CustomMFExitAlert where mutualFundId=? and strategyName='ExitStrategy5'").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				if(lastExit5Date==null)
				{
					lastExit5Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=0 and period=2 and tcfl1!=0").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				}
				if(lastExit5Date==null)
				{
					continue;
				}
				
				List<Fractal> fractalList=hSession.createQuery("from Fractal where mutualFundId=? and period=? and candleType=? and tcfl1!=0 and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,2).setParameter(2,0).setParameter(3,lastExit5Date).list();
				transaction=hSession.beginTransaction();
				for(int i=3;i<fractalList.size();i++)
				{
					Fractal currentFractal=fractalList.get(i);
					double currentClosePrice=currentFractal.getClosePrice();
					Date currentTradeDate=currentFractal.getTradeDate();
					if(currentClosePrice<currentFractal.getTcfl1() && currentClosePrice<currentFractal.getTcfl2() && currentClosePrice<currentFractal.getTcfl3())
					{
						Fractal previousFractal=fractalList.get(i-1);
						double previousClosePrice=previousFractal.getClosePrice();
						if(previousClosePrice<previousFractal.getTcfl1() && previousClosePrice<previousFractal.getTcfl2() && previousClosePrice<previousFractal.getTcfl3())
						{
							
							Fractal pPreviousFractal=fractalList.get(i-2);
							double pPreviousClosePrice=pPreviousFractal.getClosePrice();
							double minTCFL=Math.min(Math.min(pPreviousFractal.getTcfl1(),pPreviousFractal.getTcfl2()),pPreviousFractal.getTcfl3());
							if(pPreviousFractal.getTcfl1()!=0 && pPreviousClosePrice>minTCFL)
							{
								Fractal currentPeriod1Fractal=(Fractal)hSession.createQuery("from Fractal where mutualFundId=? and candleType=? and period=? and tradeDate=?").setParameter(0,mutualFundId).setParameter(1,0).setParameter(2,1).setParameter(3,currentTradeDate).setMaxResults(1).uniqueResult();
								if(currentPeriod1Fractal.getClosePrice()<currentPeriod1Fractal.getLowFractal())
								{
									boolean flatPatternSSBFound=false;
									Calendar calendar=Calendar.getInstance();
									calendar.setTime(currentTradeDate);
									calendar.add(Calendar.MONTH,-3);
									Date previousThreeMonthDate=calendar.getTime();
									List flatPatternBeforeTradeDateSsb=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate").setParameter(0,"SSB").setParameter(1,mutualFundId).setParameter(2,15).setParameter(3, 0).setParameter(4,currentTradeDate).setMaxResults(1).list();
									if(flatPatternBeforeTradeDateSsb.size()!=0)
									{
										FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateSsb.get(0);
										Date lastFlatPatternDate=flatPattern.getPatternEndDate();
										
										
										if(lastFlatPatternDate.compareTo(previousThreeMonthDate)<0)
										{
											flatPatternSSBFound=true;
										//	System.out.println("pattern end date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
										}
										else
										{
											double ssb=flatPattern.getPatternValue();
											if(ssb>(currentClosePrice*1.03))
											{
												//System.out.println("ssb true "+mutualFundId+"tradeDate="+tradeDate);
												//System.out.println("pattern date found(Nav checked) alert for mf="+mutualFundId+" for "+tradeDate+"  lastFlatDate="+lastFlatPatternDate);
												flatPatternSSBFound=true;
											}
											else
											{
												//System.out.print("ssb pattern date found ( nav not verified)");
												flatPatternSSBFound=false;
												continue;
											}
										}
									}
									else
									{
										//flatPatternSSBFound=false;
										//System.out.println("No pattern date found alert for mf="+mutualFundId+" for "+tradeDate);
										flatPatternSSBFound=true;
									}
									if(flatPatternSSBFound==true)
									{
										System.out.println("Exit 5 alert for ="+mutualFundId+"  on ="+currentTradeDate);
										CustomMFExitAlert customMFExitAlert = new CustomMFExitAlert();
		                                customMFExitAlert.setMutualFundId(mutualFundId);
		                                customMFExitAlert.setStrategyName(strategyName);
		                                customMFExitAlert.setTradeDate(currentTradeDate);
		                                customMFExitAlert.setReasonForAlert(1);
		                                customMFExitAlert.setExitPrice(currentClosePrice);
		                              	hSession.saveOrUpdate(customMFExitAlert);
									}
								}
							}
						}
					}
				}
				transaction.commit();		
			}
			BufferedWriter bw=null;			
			try
			{
				File f=new File("/opt/strategy");
				if(!f.isDirectory())
				{
					f.mkdirs();
				}
			
				bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/exit.txt"),true));
				String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
				System.out.println("Saving exit 5");
				bw.write(entry);
				bw.newLine();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				bw.close();
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception Occurred in exit 5");
			e.printStackTrace();
			if(transaction!=null)
				transaction.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}
	
	private void updateExit6AfterDate() throws Exception
	{
		Session hSession=null;
		Transaction transaction=null;
		System.out.println("updating exit 6");
		try
		{
			String strategyName="ExitStrategy6";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			while(iterator.hasNext())
			{
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				
				hSession=HibernateBridge.getSessionFactory().openSession();
				Date lastExit6Date=(Date)hSession.createQuery("SELECT max(tradeDate) from CustomMFExitAlert where mutualFundId=? and strategyName='ExitStrategy6'").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				if(lastExit6Date==null)
				{
					lastExit6Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=1 and period=2 and tcfl1!=0 order by tradeDate").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				}
				if(lastExit6Date==null)
				{
					continue;
				}				
				//System.out.println("Mfid="+mutualFundId+"  lastExit6Date"+lastExit6Date);
				List<Fractal> fractalList=hSession.createQuery("from Fractal where mutualFundId=? and period=? and candleType=? and tcfl1!=0 and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,2).setParameter(2,1).setParameter(3, lastExit6Date).list();
				transaction=hSession.beginTransaction();
				for(int i=3;i<fractalList.size();i++)
				{
					
					Fractal currentFractal=fractalList.get(i);
					double currentClosePrice=currentFractal.getClosePrice();
					Date currentTradeDate=currentFractal.getTradeDate();
				//	System.out.println("Curr="+currentTradeDate);
					EveryDayCandle edc=(EveryDayCandle)hSession.createQuery("from EveryDayCandle where mutualFundId=? and tradeDate=? and candleType=?").setParameter(0, mutualFundId).setParameter(1, currentTradeDate).setParameter(2, 1).uniqueResult();
					double ema200=edc.getEma200();
					//System.out.println("Date="+currentTradeDate);
					if(currentClosePrice<currentFractal.getTcfl1() && currentClosePrice<currentFractal.getTcfl2() && currentClosePrice<currentFractal.getTcfl3() && currentClosePrice<currentFractal.getLowFractal() && (ema200==0 || currentClosePrice<ema200))
					{
						Fractal previousFractal=fractalList.get(i-1);
						double previousClosePrice=previousFractal.getClosePrice();
						if(previousClosePrice<previousFractal.getTcfl1() && previousClosePrice<previousFractal.getTcfl2() && previousClosePrice<previousFractal.getTcfl3())
						{
							Fractal pPreviousFractal=fractalList.get(i-2);
							double pPreviousClosePrice=pPreviousFractal.getClosePrice();
							double minTCFL=Math.min(Math.min(pPreviousFractal.getTcfl1(),pPreviousFractal.getTcfl2()),pPreviousFractal.getTcfl3());
							if(pPreviousFractal.getTcfl1()!=0 && pPreviousClosePrice>minTCFL)
							{
								boolean flatPatternSSBFound=false;
								Calendar calendar=Calendar.getInstance();
								calendar.setTime(currentTradeDate);
								calendar.add(Calendar.MONTH,-3);
								Date previousThreeMonthDate=calendar.getTime(); 
								List flatPatternBeforeTradeDateSsb=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"SSB").setParameter(1,mutualFundId).setParameter(2,20).setParameter(3, 1).setParameter(4,currentTradeDate).setMaxResults(1).list();
								if(flatPatternBeforeTradeDateSsb.size()!=0)
								{
									FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateSsb.get(0);
									Date lastFlatPatternDate=flatPattern.getPatternStartDate();
									
									if(lastFlatPatternDate.compareTo(previousThreeMonthDate)<0)
									{
										flatPatternSSBFound=true;
									//	System.out.println("pattern end date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
									}
									else
									{
										double ssb=flatPattern.getPatternValue();
										if(ssb>(currentClosePrice*1.03))
										{
											//System.out.println("ssb true "+mutualFundId+"tradeDate="+tradeDate);
											//System.out.println("pattern date found(Nav checked) alert for mf="+mutualFundId+" for "+tradeDate+"  lastFlatDate="+lastFlatPatternDate);
											flatPatternSSBFound=true;
										}
										else
										{
											//System.out.print("ssb pattern date found ( nav not verified)");
											flatPatternSSBFound=false;
											continue;
										}
									}
								}
								else
								{
									//flatPatternSSBFound=false;
									//System.out.println("No pattern date found alert for mf="+mutualFundId+" for "+tradeDate);
									flatPatternSSBFound=true;
								}
								if(flatPatternSSBFound==true)
								{
									System.out.println("Exit 6 alert for ="+mutualFundId+"  on ="+currentTradeDate);
									CustomMFExitAlert customMFExitAlert = new CustomMFExitAlert();
	                                customMFExitAlert.setMutualFundId(mutualFundId);
	                                customMFExitAlert.setStrategyName(strategyName);
	                                customMFExitAlert.setTradeDate(currentTradeDate);
	                                customMFExitAlert.setReasonForAlert(1);
	                                customMFExitAlert.setExitPrice(currentClosePrice);
	                               	hSession.saveOrUpdate(customMFExitAlert);
								}
								
							}
						}
					}
				}
				transaction.commit();
			
			}
			BufferedWriter bw=null;
	        try
	        {
	        	File f=new File("/opt/strategy");
	            if(!f.isDirectory())
	            {
	            	f.mkdirs();
	            }
	            	
	            bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/exit.txt"),true));
	          	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	          	System.out.println("Saving exit 6");
	          	bw.write(entry);
	          	bw.newLine();
	        }
	        catch(Exception e)
	        {
	        	e.printStackTrace();
	        }
	        finally
	        {
	        	bw.close();
	        }
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(transaction!=null)
				transaction.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}
	
	private void updateExit7AfterDate() throws Exception
	{
		Session hSession=null;
		Transaction transaction=null;
		System.out.println("updating exit 7");
		try
		{
			String strategyName="ExitStrategy7";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			while(iterator.hasNext())
			{
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				
				hSession=HibernateBridge.getSessionFactory().openSession();
				transaction=hSession.beginTransaction();
				Date previousAlert7Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFExitAlert where mutualFundId=? and strategyName='ExitStrategy7'").setParameter(0,mutualFundId).uniqueResult();
				List alerts=hSession.createQuery("from CustomMFExitAlert where mutualFundId=? and (strategyName='ExitStrategy3' or strategyName='ExitStrategy5' or strategyName='ExitStrategy6') order by tradeDate").setParameter(0,mutualFundId).list();
				for(int i=0;i<alerts.size();i++)
				{
					CustomMFExitAlert exitAlert=(CustomMFExitAlert)alerts.get(i);
					Date alertDate=exitAlert.getTradeDate();
					Calendar cal=Calendar.getInstance();
					cal.setTime(alertDate);
					cal.add(Calendar.MONTH,-4);
					//System.out.println("previousDate="+previousDate+"Current="+alertDate+" previous4MonthDate="+cal.getTime()+"AlertType="+entryAlert.getStrategyName());
					
					//System.out.println("List"+alert16Within6Month+"mf="+mutualFundId);			
					if(previousAlert7Date!=null)
					{
						if(previousAlert7Date.compareTo(cal.getTime())<0)
						{
							System.out.println("AlertExit 7 found for mfid="+mutualFundId+" on alertDate="+alertDate);
							CustomMFExitAlert customMFExitAlert=new CustomMFExitAlert();
							customMFExitAlert.setMutualFundId(mutualFundId);
							customMFExitAlert.setStrategyName(strategyName);
							customMFExitAlert.setTradeDate(alertDate);
							customMFExitAlert.setExitPrice(exitAlert.getExitPrice());
							customMFExitAlert.setReasonForAlert(1);
							hSession.saveOrUpdate(customMFExitAlert);
							previousAlert7Date=alertDate;
						}
					}
					else
					{
						System.out.println("AlertExit 7 found for mfid="+mutualFundId+" on alertDate="+alertDate+"                    with previousDate="+previousAlert7Date);
						CustomMFExitAlert customMFExitAlert=new CustomMFExitAlert();
						customMFExitAlert.setMutualFundId(mutualFundId);
						customMFExitAlert.setStrategyName(strategyName);
						customMFExitAlert.setTradeDate(alertDate);
						customMFExitAlert.setExitPrice(exitAlert.getExitPrice());
						customMFExitAlert.setReasonForAlert(1);
						hSession.saveOrUpdate(customMFExitAlert);
						previousAlert7Date=alertDate;
					}				
				}
				transaction.commit();
				
			}	
			BufferedWriter bw=null;
	        try
	        {
	        	File f=new File("/opt/strategy");
	      		if(!f.isDirectory())
	      		{
	      			f.mkdirs();
	      		}
	      	
	      		bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/exit.txt"),true));
	          	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	          	System.out.println("Saving exit 7");
	          	bw.write(entry);
	          	bw.newLine();
	        }
	        catch(Exception e)
	        {
	          	e.printStackTrace();
	        }
	        finally
	        {
	        	bw.close();
	        }
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(transaction!=null)
				transaction.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}

	}
}
