package com.fundexpert.controller;

/**
 * @author Amit Sharda

 * Date: February 20, 2011
 */



import java.util.ArrayList;
import java.util.Calendar;

import com.apidoc.util.HibernateBridge;
import com.apidoc.util.ImageLinks;
import com.fundexpert.dao.Nav;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MAType;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

public class TALibEngine
{

	// private static Logger logger = Logger.getLogger(TALibEngine.class);

	public static void calculateCS(String symbol, Calendar fromDate, Calendar tillDate, int optInTimePeriod)
	{
		Core core = new Core();
		// core.cd
	}

	/*public static void calculatePsar(String symbol, Calendar fromDate, Calendar tillDate, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		// Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			tx = hSession.beginTransaction();
			if(cash == null)
			{
				logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculatePsar()");
				return;
			}





			Core core = new Core();
			int minDays = 14;
			// int NOD = minDays + cashHandler.getWorkingDaysBetween(fromDate, tillDate);
			int NOD = 200;// As asked by Anuraag //Changed to 250 by Amit on 27/02/2014

			float inHigh[] = cashHandler.getCashDatafromDB(symbol, NOD, 1, tillDate); // {1025.8,1031.8,1047,1055.75,1054.8,1072.25,1069.4,1074,1081.5,1068,1064.95,1068.7,1064.4,1087.85,1093.8,1101.9,1100.95,1100.9,1107.2,1124.9,1095.4,1084.9,1107,1117.5,1119,1120.5,1116.9,1111.8,1087.4,1067.9,1059};
			float inLow[] = cashHandler.getCashDatafromDB(symbol, NOD, 2, tillDate);// {1014.95,1009,1026.95,1032.5,1030.55,1053.1,1046.15,1054.7,1054.2,1035.6,1033.25,1039.25,1038,1049.2,1074.35,1086,1086.1,1074.6,1080.5,1089.25,1070.3,1062.65,1075,1100.3,1100.55,1097.25,1099.2,1076,1057,1038.2,1026.55};
			// float inClose[] = cashHandler.getCashDatafromDB(symbol, NOD, 3,
			// tillDate);//{1017.85,1023.85,1044.95,1039.15,1049.65,1069.2,1055.6,1073.05,1059,1040.35,1047.45,1044.4,1049.45,1081.65,1081.8,1090.35,1098.85,1082,1096.25,1093.2,1074.85,1065.3,1105.05,1105.7,1111.85,1108.55,1102.2,1079.9,1061.85,1054.7,1030.5};

			int startIdx = 0;
			int total_days_data = inHigh.length;
			int endIdx = total_days_data - 1;


			MInteger outBegIdxA = new MInteger();
			MInteger outNbElementA = new MInteger();


			double outReal[] = new double[inHigh.length];
			double inAcc = 0.02;
			double inMax = .2;
			RetCode code = core.sar(startIdx, endIdx, inHigh, inLow, inAcc, inMax, outBegIdxA, outNbElementA, outReal);

			if(code != RetCode.Success)
			{
				System.out.println("Symbol [" +symbol+ "] Errorqqqq");
				System.out.println("Return count "+outNbElement1.value);
				Psar psr = new Psar();
				psr.setSymbol(symbol);
				psr.setTradeDate(tillDate.getTime());
				psr.setpSar(0);
				HibernateBridge.saveOrUpdate(psr);
				// System.out.println("Code != Success. Saving Psar for [" +psr.getTradeDate()+ "]");
			}

			// System.out.println("outNbElementA.value [" +outNbElementA.value+ "] for trade date [" +fromDate.getTime()+ "]");
			if(outNbElementA.value > 0)
			{
				Psar psr = new Psar();
				psr.setSymbol(symbol);
				psr.setTradeDate(tillDate.getTime());
				psr.setpSar(ImageLinks.getRoundedValue((outReal[(outNbElementA.value - 1)]), 5));
				HibernateBridge.saveOrUpdate(psr);
				// System.out.println("Saving Psar for [" +psr.getTradeDate()+ "]");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
			}
		}
		finally
		{
			hSession.close();
		}
	}*/

	// this function(core.sar) expect fromDate to be more than 200 days before tillDate
	public static Float calculatePsarCustom(long mutualFundId, Calendar fromDate, Calendar tillDate, double accelerationFactor, double maximumFactor)
	{
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculatePsar()");
			return null;
		}*/
		// Session hSession = null;
		// Transaction tx = null;
		try
		{
			// Nav nav = (Nav) hSession.createQuery("from Nav where mutualfundId=? and tradedate=?").setParameter(0, mutualFundId).setParameter(1, tillDate).uniqueResult();
			NavController navController = new NavController();
			/*Nav nav = null;
			
			*/
			Core core = new Core();
			int minDays = 14;
			int NOD = minDays + navController.getWorkingDaysBetween(mutualFundId, fromDate.getTime(), tillDate.getTime());
			System.out.println("NOD=" + NOD + "=" + minDays + "+" + navController.getWorkingDaysBetween(mutualFundId, fromDate.getTime(), tillDate.getTime()));
			// float inHigh[] = cashHandler.getCashDatafromDB(symbol, NOD, 1, tillDate); //
			// {1025.8,1031.8,1047,1055.75,1054.8,1072.25,1069.4,1074,1081.5,1068,1064.95,1068.7,1064.4,1087.85,1093.8,1101.9,1100.95,1100.9,1107.2,1124.9,1095.4,1084.9,1107,1117.5,1119,1120.5,1116.9,1111.8,1087.4,1067.9,1059};
			// float inLow[] = cashHandler.getCashDatafromDB(symbol, NOD, 2, tillDate);//
			// {1014.95,1009,1026.95,1032.5,1030.55,1053.1,1046.15,1054.7,1054.2,1035.6,1033.25,1039.25,1038,1049.2,1074.35,1086,1086.1,1074.6,1080.5,1089.25,1070.3,1062.65,1075,1100.3,1100.55,1097.25,1099.2,1076,1057,1038.2,1026.55};
			// float inClose[] = cashHandler.getCashDatafromDB(symbol, NOD, 3,
			// tillDate);//{1017.85,1023.85,1044.95,1039.15,1049.65,1069.2,1055.6,1073.05,1059,1040.35,1047.45,1044.4,1049.45,1081.65,1081.8,1090.35,1098.85,1082,1096.25,1093.2,1074.85,1065.3,1105.05,1105.7,1111.85,1108.55,1102.2,1079.9,1061.85,1054.7,1030.5};

			// close[] in ascending order of tradeDate
			System.out.println("Till date(psar)=" + tillDate.getTime());
			double close[] = navController.getNav(mutualFundId, NOD, tillDate.getTime());
			System.out.println("close.length=" + close.length);


			int startIdx = 0;
			int total_days_data = close.length;
			int endIdx = total_days_data - 1;


			MInteger outBegIdxA = new MInteger();
			MInteger outNbElementA = new MInteger();


			double outReal[] = new double[close.length];
			/*double inAcc = 0.02;
			double inMax = .2;*/
			// RetCode code = core.sar(startIdx, endIdx, inHigh, inLow, accelerationFactor, maximumFactor, outBegIdxA, outNbElementA, outReal);
			RetCode code = core.sar(startIdx, endIdx, close, close, accelerationFactor, maximumFactor, outBegIdxA, outNbElementA, outReal);
			if(code != RetCode.Success)
			{

				return null;
			}
			System.out.println("outNbElementA=" + outNbElementA.value);
			// System.out.println("outNbElementA.value [" +outNbElementA.value+ "] for trade date [" +fromDate.getTime()+ "]");
			if(outNbElementA.value > 0)
			{
				return new Float(outReal[(outNbElementA.value - 1)]);
			}

			return null;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}

	}

	/*public static void calculateBBands(long mutualFundId, Calendar fromDate, Calendar tillDate, int optInTimePeriod, double upMultiplier, double downMultiplier)
	{
		Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateBBands()");
			return;
		}
		NavController navController = new NavController();
		Nav nav = null;
		// double data[] = getCashData(51);
		// pre -data before todays/first date= optInTimePeriod1-1
		Core core = new Core();
		int minNOD = optInTimePeriod;
		int NOD = minNOD + navController.getWorkingDaysBetween(mutualFundId, fromDate.getTime(), tillDate.getTime());
		// System.out.println("NOD "+NOD);

		// float inReal[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);
		double close[] = navController.getNav(mutualFundId, NOD, tillDate.getTime());

		int total_days_data = close.length;

		int startIdx1 = optInTimePeriod;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outUpper[] = new double[inReal.length];
		double outMiddle[] = new double[inReal.length];
		double outLower[] = new double[inReal.length];

		RetCode code1 = core.bbands(startIdx1, endIdx1, inReal, optInTimePeriod, upMultiplier, downMultiplier, MAType.Sma, outBegIdx1, outNbElement1, outUpper, outMiddle, outLower);

		if(code1 != RetCode.Success)
		{
			System.out.println("Symbol [" +symbol+ "] Errorqqqq");
			System.out.println("Return count "+outNbElement1.value);
			BBands bb = new BBands();
			bb.setSymbol(symbol);
			bb.setTradeDate(tillDate.getTime());
			bb.setPeriod(optInTimePeriod);
			bb.setUpperBand(0);
			bb.setLowerBand(0);
			bb.setMiddleBand(0);

			HibernateBridge.saveOrUpdate(bb);
		}


		if(outNbElement1.value > 0)
		{
			BBands bb = new BBands();
			bb.setSymbol(symbol);
			bb.setTradeDate(tillDate.getTime());
			bb.setPeriod(optInTimePeriod);

			bb.setUpperBand(ImageLinks.getRoundedValue((outUpper[outNbElement1.value - 1]), 5));
			bb.setLowerBand(ImageLinks.getRoundedValue((outLower[outNbElement1.value - 1]), 5));
			bb.setMiddleBand(ImageLinks.getRoundedValue((outMiddle[outNbElement1.value - 1]), 5));


			HibernateBridge.saveOrUpdate(bb);
		}


		 if (code1 != RetCode.Success) {
		 System.out.println("Errorqqqq");
		}

		Calendar newDate = fromDate;
		if (!DatesUtil.isTradingDay(newDate))
		 newDate = cashHandler.getNextValidDate(newDate);

		List<BBands> bbList = new ArrayList<BBands>();
		//  System.out.println("Return count "+outNbElement1.value);
		for (int i = 0; i < outNbElement1.value; i++) {
		 //retRSA[i] = outReal1[i];
		 BBands md = new BBands();
		 md.setSymbol(symbol);
		 md.setTradeDate(newDate.getTime());
		 md.setPeriod(optInTimePeriod);
		 md.setUpperBand(outUpper[i]);
		 md.setLowerBand(outLower[i]);
		 md.setMiddleBand(outMiddle[i]);

		 bbList.add(md);
		//   System.out.println(newDate.getTime());
		 newDate = cashHandler.getNextValidDate(newDate);

		}


		HibernateBridge.storeBBandList(bbList);
		// HibernateBridge.storeBBands(bbList.get(bbList.size() - 1));


	}*/

	public static double[] calculateBBandsCustom(long mutualFundId, Calendar fromDate, Calendar tillDate, int optInTimePeriod, double upMultiplier, double downMultiplier)
	{
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateBBands()");
			return null;
		}*/

		// double data[] = getCashData(51);
		// pre -data before todays/first date= optInTimePeriod1-1
		NavController navController = new NavController();

		Core core = new Core();
		int minNOD = optInTimePeriod;
		int NOD = minNOD + navController.getWorkingDaysBetween(mutualFundId, fromDate.getTime(), tillDate.getTime());
		// System.out.println("NOD "+NOD);

		// float inReal[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);
		double close[] = navController.getNav(mutualFundId, NOD, tillDate.getTime());

		int total_days_data = close.length;

		int startIdx1 = optInTimePeriod;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outUpper[] = new double[close.length];
		double outMiddle[] = new double[close.length];
		double outLower[] = new double[close.length];

		RetCode code1 = core.bbands(startIdx1, endIdx1, close, optInTimePeriod, upMultiplier, downMultiplier, MAType.Sma, outBegIdx1, outNbElement1, outUpper, outMiddle, outLower);

		if(outNbElement1.value > 0)
		{
			double bands[] = new double[3];
			/*BBands bb = new BBands();
			bb.setSymbol(symbol);
			bb.setTradeDate(tillDate.getTime());
			bb.setPeriod(optInTimePeriod);
			*/
			/*bb.setUpperBand(ImageLinks.getRoundedValue((outUpper[outNbElement1.value - 1]), 5));
			bb.setLowerBand(ImageLinks.getRoundedValue((outLower[outNbElement1.value - 1]), 5));
			bb.setMiddleBand(ImageLinks.getRoundedValue((outMiddle[outNbElement1.value - 1]), 5));*/
			/*System.out.println("Out Upper" + ImageLinks.getRoundedValue(outUpper[outNbElement1.value - 1], 5));
			System.out.println("Out Lower" + (ImageLinks.getRoundedValue((outLower[outNbElement1.value - 1]), 5)));
			System.out.println("Out Middle" + ImageLinks.getRoundedValue((outMiddle[outNbElement1.value - 1]), 5));
			System.out.println("NOD=" + NOD);*/
			bands[0] = ImageLinks.getRoundedValue(outUpper[outNbElement1.value - 1], 5);
			bands[1] = ImageLinks.getRoundedValue(outMiddle[outNbElement1.value - 1], 5);
			bands[2] = ImageLinks.getRoundedValue(outLower[outNbElement1.value - 1], 5);
			return bands;
			// sreturn bb;
		}
		return null;
	}

	/*public static Float calculateBBandsWidthCustom(String symbol, Calendar fromDate, Calendar tillDate, int optInTimePeriod, double upMultiplier, double downMultiplier, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		 With a 20-day SMA and 20-day Standard Deviation, the standard deviation multiplier is set at 2. Bollinger suggests increasing the standard deviation multiplier to 2.1 for a 50-period SMA and decreasing the standard deviation multiplier to 1.9 for a 10-period SMA.
		Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateBBandsWidthCustom()");
			return null;
		}

		// double data[] = getCashData(51);
		// pre -data before todays/first date= optInTimePeriod1-1
		Core core = new Core();
		int minNOD = optInTimePeriod;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);
		// System.out.println("NOD "+NOD);

		float inReal[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);


		int total_days_data = inReal.length;

		int startIdx1 = optInTimePeriod;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outUpper[] = new double[inReal.length];
		double outMiddle[] = new double[inReal.length];
		double outLower[] = new double[inReal.length];

		RetCode code1 = core.bbands(startIdx1, endIdx1, inReal, optInTimePeriod, upMultiplier, downMultiplier, MAType.Sma, outBegIdx1, outNbElement1, outUpper, outMiddle, outLower);

		if(code1 != RetCode.Success)
		{
			return null;
		}


		if(outNbElement1.value > 0 && cash.getClosePrice() > 0)
		{
			return new Float(((outUpper[outNbElement1.value - 1] - outLower[outNbElement1.value - 1]) / cash.getClosePrice()) * 100);
		}

		return null;

	}*/

	public static double calculateAtr(long mutualFundId, Calendar fromDate, Calendar tillDate, int optInTimePeriod)
	{
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateADX()");
			return -1;
		}*/

		Core core = new Core();
		int minNOD = optInTimePeriod;
		// int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);
		int NOD = minNOD + 250; // trying to smoothen the values. Changed on 01/09/2015

		Nav nav = null;
		NavController navController = new NavController();


		/*float inReal[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);
		float inHigh[] = cashHandler.getCashDatafromDB(symbol, NOD, 1, tillDate);
		float inLow[] = cashHandler.getCashDatafromDB(symbol, NOD, 2, tillDate);*/
		double close[] = navController.getNav(mutualFundId, NOD, tillDate.getTime());
		int total_days_data = close.length;

		int startIdx1 = optInTimePeriod;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[close.length];

		RetCode code1 = core.atr(startIdx1, endIdx1, close, close, close, optInTimePeriod, outBegIdx1, outNbElement1, outReal1);
		if(code1 != RetCode.Success)
		{
			System.out.println("lol1");
			return -1;
		}
		if(outNbElement1.value > 0)
		{
			System.out.println("NOD=" + NOD + "   atr=" + ImageLinks.getRoundedValue((outReal1[(outNbElement1.value - 1)]), 5));
			return ImageLinks.getRoundedValue((outReal1[(outNbElement1.value - 1)]), 5);
		}
		System.out.println("lol2");
		return -1;


	}

	/*public static double calculateOBV(String symbol, Calendar fromDate, Calendar tillDate, int optInTimePeriod, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateADX()");
			return -1;
		}

		Core core = new Core();
		int minNOD = optInTimePeriod;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);

		float inReal[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);
		float inVolume[] = cashHandler.getCashDatafromDB(symbol, NOD, 4, tillDate);

		int total_days_data = inReal.length;

		int startIdx1 = optInTimePeriod;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[inReal.length];
		System.out.println("startIdx1 [" + startIdx1 + "] inRealLength [" + inReal.length + "] [" + core.obvLookback() + "]");
		RetCode code1 = core.obv(startIdx1, endIdx1, inReal, inVolume, outBegIdx1, outNbElement1, outReal1);
		// System.out.println("Code1 [" +code1+ "]");

		if(code1 != RetCode.Success || outNbElement1.value == 0)
		{
			return -1;
		}

		if(outNbElement1.value > 0)
		{
			return ImageLinks.getRoundedValue((outReal1[(outNbElement1.value - 1)]), 2);
		}

		return -1;

	}

	public static void calculateOBV(String symbol, Calendar tillDate, int NOD, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		try
		{
			Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
			if(cash == null)
			{
				logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateOBV()");
				return;
			}

			Core core = new Core();

			float inReal[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);
			float inVolume[] = cashHandler.getCashDatafromDB(symbol, NOD, 4, tillDate);

			if(inReal.length == 0)
			{
				return;
			}
			int startIdx1 = 0;
			int endIdx1 = inReal.length - 1;


			MInteger outBegIdx1 = new MInteger();
			MInteger outNbElement1 = new MInteger();
			double outReal1[] = new double[inReal.length];

			RetCode code1 = core.obv(startIdx1, endIdx1, inReal, inVolume, outBegIdx1, outNbElement1, outReal1);


			Obv obv = new Obv();
			obv.setSymbol(symbol);
			obv.setTradeDate(tillDate.getTime());

			if(code1 != RetCode.Success)
			{
				obv.setObv(0);
			}


			if(outNbElement1.value > 0)
			{
				obv.setObv(ImageLinks.getRoundedValue((outReal1[(outNbElement1.value - 1)]), 2));
			}
			else
			{
				obv.setObv(0);
			}

			HibernateBridge.saveOrUpdate(obv);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new StrategyLabsException("Exception occurred while calculating OBV for symbol [" + symbol + "]");
		}

	}*/

	/*	public static void calculateADX(String symbol, Calendar fromDate, Calendar tillDate, int optInTimePeriod, HibernateCashHandler cashHandler) throws StrategyLabsException
		{
			Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
			if(cash == null)
			{
				logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateADX()");
				return;
			}

			Core core = new Core();
			int minNOD = (2 * optInTimePeriod) - 1;
			int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);

			float inHigh[] = cashHandler.getCashDatafromDB(symbol, NOD, 1, tillDate); // {1025.8,1031.8,1047,1055.75,1054.8,1072.25,1069.4,1074,1081.5,1068,1064.95,1068.7,1064.4,1087.85,1093.8,1101.9,1100.95,1100.9,1107.2,1124.9,1095.4,1084.9,1107,1117.5,1119,1120.5,1116.9,1111.8,1087.4,1067.9,1059};
			float inLow[] = cashHandler.getCashDatafromDB(symbol, NOD, 2, tillDate);// {1014.95,1009,1026.95,1032.5,1030.55,1053.1,1046.15,1054.7,1054.2,1035.6,1033.25,1039.25,1038,1049.2,1074.35,1086,1086.1,1074.6,1080.5,1089.25,1070.3,1062.65,1075,1100.3,1100.55,1097.25,1099.2,1076,1057,1038.2,1026.55};
			float inClose[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);// {1017.85,1023.85,1044.95,1039.15,1049.65,1069.2,1055.6,1073.05,1059,1040.35,1047.45,1044.4,1049.45,1081.65,1081.8,1090.35,1098.85,1082,1096.25,1093.2,1074.85,1065.3,1105.05,1105.7,1111.85,1108.55,1102.2,1079.9,1061.85,1054.7,1030.5};

			int startIdx = (2 * optInTimePeriod) - 1;
			int total_days_data = inHigh.length;
			int endIdx = total_days_data - 1;

			MInteger outBegIdx = new MInteger();
			MInteger outNbElement = new MInteger();
			MInteger outBegIdxA = new MInteger();
			MInteger outNbElementA = new MInteger();
			MInteger outBegIdxB = new MInteger();
			MInteger outNbElementB = new MInteger();


			double outReal[] = new double[inHigh.length];
			double outReal2[] = new double[inHigh.length];
			double outReal3[] = new double[inHigh.length];

			RetCode code = core.adx(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdxA, outNbElementA, outReal);
			boolean errorOccurred = false;

			if(code != RetCode.Success)
			{
				// System.out.println("Error");
				errorOccurred = true;
			}
			RetCode code2 = core.minusDI(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdx, outNbElement, outReal2);
			if(code2 != RetCode.Success)
			{
				// System.out.println("Error");
				errorOccurred = true;
			}
			RetCode code3 = core.plusDI(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdxB, outNbElementB, outReal3);
			if(code3 != RetCode.Success)
			{
				// System.out.println("Error");
				errorOccurred = true;
			}

			if(errorOccurred)
			{
				Adx md = new Adx();
				md.setSymbol(symbol);
				md.setTradeDate(tillDate.getTime());
				md.setPeriod(optInTimePeriod);

				md.setAdx(0);
				md.setMinusDi(0);
				md.setPlusDi(0);

				HibernateBridge.saveOrUpdate(md);
			}
			else if(outNbElementA.value > 0)
			{
				Adx md = new Adx();
				md.setSymbol(symbol);
				md.setTradeDate(tillDate.getTime());
				md.setPeriod(optInTimePeriod);
				md.setAdx(ImageLinks.getRoundedValue((outReal[(outNbElementA.value - 1)]), 5));
				md.setMinusDi(ImageLinks.getRoundedValue((outReal2[(outNbElementA.value - 1)]), 5));
				md.setPlusDi(ImageLinks.getRoundedValue((outReal3[(outNbElementA.value - 1)]), 5));

				HibernateBridge.saveOrUpdate(md);
			}

			
			Calendar newDate = fromDate;

			if (!DatesUtil.isTradingDay(newDate)) {
			    newDate = cashHandler.getNextValidDate(newDate);
			}

			List<Adx> adxList = new ArrayList<Adx>();
			for (int i = 0; i < outNbElementA.value; i++) {
			    //retADX[i] = outReal[i];
			    Adx md = new Adx();
			    md.setSymbol(symbol);
			    md.setTradeDate(newDate.getTime());
			    md.setPeriod(optInTimePeriod);
			    md.setAdx(outReal[i]);
			    md.setPlusDi(outReal3[i]);

			    md.setMinusDi(outReal2[i]);

			    adxList.add(md);
			    newDate = cashHandler.getNextValidDate(newDate);
			}

			// HERE PERSIST THE Adx ARRAY
			// Commented by Amit on 23-05-2011 as we are only interpresting last value
			//        HibernateBridge.storeAdxList(adxList);
			//        System.out.println("Size of adx "+adxList.size());
			if(adxList.size() > 0) {
			     HibernateBridge.storeAdx(adxList.get(adxList.size()-1));
			}


		}*/

	public static Float calculateADXCustom(long mutualFundId, Calendar fromDate, Calendar tillDate, int optInTimePeriod)
	{
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateADXCustom()");
			return null;
		}*/
		Nav nav = null;
		NavController navController = new NavController();

		Core core = new Core();
		int minNOD = (2 * optInTimePeriod) - 1;
		System.out.println("get Working Days");
		int NOD = minNOD + navController.getWorkingDaysBetween(mutualFundId, fromDate.getTime(), tillDate.getTime());

		/*float inHigh[] = cashHandler.getCashDatafromDB(symbol, NOD, 1, tillDate); // {1025.8,1031.8,1047,1055.75,1054.8,1072.25,1069.4,1074,1081.5,1068,1064.95,1068.7,1064.4,1087.85,1093.8,1101.9,1100.95,1100.9,1107.2,1124.9,1095.4,1084.9,1107,1117.5,1119,1120.5,1116.9,1111.8,1087.4,1067.9,1059};
		float inLow[] = cashHandler.getCashDatafromDB(symbol, NOD, 2, tillDate);// {1014.95,1009,1026.95,1032.5,1030.55,1053.1,1046.15,1054.7,1054.2,1035.6,1033.25,1039.25,1038,1049.2,1074.35,1086,1086.1,1074.6,1080.5,1089.25,1070.3,1062.65,1075,1100.3,1100.55,1097.25,1099.2,1076,1057,1038.2,1026.55};
		float inClose[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);// {1017.85,1023.85,1044.95,1039.15,1049.65,1069.2,1055.6,1073.05,1059,1040.35,1047.45,1044.4,1049.45,1081.65,1081.8,1090.35,1098.85,1082,1096.25,1093.2,1074.85,1065.3,1105.05,1105.7,1111.85,1108.55,1102.2,1079.9,1061.85,1054.7,1030.5};
		*/
		System.out.println("get Closing values" + NOD);
		double close[] = navController.getNav(mutualFundId, NOD, tillDate.getTime());
		/*for (double a : close)
			System.out.println("close=" + a);*/
		// int startIdx = (2 * optInTimePeriod) - 1;
		int total_days_data = close.length;
		// int startIdx = total_days_data - minNOD;
		int startIdx = (2 * optInTimePeriod) - 1;
		int endIdx = total_days_data - 1;
		System.out.println("asfs" + endIdx);
		MInteger outBegIdx = new MInteger();
		MInteger outNbElement = new MInteger();
		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();
		MInteger outBegIdxB = new MInteger();
		MInteger outNbElementB = new MInteger();


		double outReal[] = new double[close.length];
		double outReal2[] = new double[close.length];
		double outReal3[] = new double[close.length];

		RetCode code = core.adx(startIdx, endIdx, close, close, close, optInTimePeriod, outBegIdxA, outNbElementA, outReal);
		boolean errorOccurred = false;

		if(code != RetCode.Success)
		{
			// System.out.println("Error");
			System.out.println("adx lol1");
			errorOccurred = true;
		}
		RetCode code2 = core.minusDI(startIdx, endIdx, close, close, close, optInTimePeriod, outBegIdx, outNbElement, outReal2);
		if(code2 != RetCode.Success)
		{
			// System.out.println("Error");
			System.out.println("adx lol1.2");
			errorOccurred = true;
		}
		RetCode code3 = core.plusDI(startIdx, endIdx, close, close, close, optInTimePeriod, outBegIdxB, outNbElementB, outReal3);
		if(code3 != RetCode.Success)
		{
			// System.out.println("Error");
			System.out.println("adx lol1.3");
			errorOccurred = true;
		}

		if(errorOccurred)
		{
			System.out.println("adx lol1");
			return null;
		}
		else if(outNbElementA.value > 0)
		{
			return new Float(outReal[(outNbElementA.value - 1)]);
			// md.setMinusDi(outReal2[(outNbElementA.value - 1)]);
			// md.setPlusDi(outReal3[(outNbElementA.value - 1)]);


		}
		System.out.println("adx lol2");
		return null;


	}

	/*public static Float calculateMinusDICustom(String symbol, Calendar fromDate, Calendar tillDate, int optInTimePeriod, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateMinusDICustom()");
			return null;
		}

		Core core = new Core();
		int minNOD = (2 * optInTimePeriod) - 1;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);

		float inHigh[] = cashHandler.getCashDatafromDB(symbol, NOD, 1, tillDate); // {1025.8,1031.8,1047,1055.75,1054.8,1072.25,1069.4,1074,1081.5,1068,1064.95,1068.7,1064.4,1087.85,1093.8,1101.9,1100.95,1100.9,1107.2,1124.9,1095.4,1084.9,1107,1117.5,1119,1120.5,1116.9,1111.8,1087.4,1067.9,1059};
		float inLow[] = cashHandler.getCashDatafromDB(symbol, NOD, 2, tillDate);// {1014.95,1009,1026.95,1032.5,1030.55,1053.1,1046.15,1054.7,1054.2,1035.6,1033.25,1039.25,1038,1049.2,1074.35,1086,1086.1,1074.6,1080.5,1089.25,1070.3,1062.65,1075,1100.3,1100.55,1097.25,1099.2,1076,1057,1038.2,1026.55};
		float inClose[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);// {1017.85,1023.85,1044.95,1039.15,1049.65,1069.2,1055.6,1073.05,1059,1040.35,1047.45,1044.4,1049.45,1081.65,1081.8,1090.35,1098.85,1082,1096.25,1093.2,1074.85,1065.3,1105.05,1105.7,1111.85,1108.55,1102.2,1079.9,1061.85,1054.7,1030.5};

		int startIdx = (2 * optInTimePeriod) - 1;
		int total_days_data = inHigh.length;
		int endIdx = total_days_data - 1;

		MInteger outBegIdx = new MInteger();
		MInteger outNbElement = new MInteger();
		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();
		MInteger outBegIdxB = new MInteger();
		MInteger outNbElementB = new MInteger();


		double outReal[] = new double[inHigh.length];
		double outReal2[] = new double[inHigh.length];
		double outReal3[] = new double[inHigh.length];

		RetCode code = core.adx(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdxA, outNbElementA, outReal);
		boolean errorOccurred = false;

		if(code != RetCode.Success)
		{
			// System.out.println("Error");
			errorOccurred = true;
		}
		RetCode code2 = core.minusDI(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdx, outNbElement, outReal2);
		if(code2 != RetCode.Success)
		{
			// System.out.println("Error");
			errorOccurred = true;
		}
		RetCode code3 = core.plusDI(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdxB, outNbElementB, outReal3);
		if(code3 != RetCode.Success)
		{
			// System.out.println("Error");
			errorOccurred = true;
		}

		if(errorOccurred)
		{
			return null;
		}
		else if(outNbElementA.value > 0)
		{
			// return new Float(outReal[(outNbElementA.value - 1)]);
			return new Float(outReal2[(outNbElementA.value - 1)]);
			// md.setPlusDi(outReal3[(outNbElementA.value - 1)]);


		}

		return null;


	}*/

	/*public static Float calculatePlusDICustom(String symbol, Calendar fromDate, Calendar tillDate, int optInTimePeriod, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculatePlusDICustom()");
			return null;
		}

		Core core = new Core();
		int minNOD = (2 * optInTimePeriod) - 1;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);

		float inHigh[] = cashHandler.getCashDatafromDB(symbol, NOD, 1, tillDate); // {1025.8,1031.8,1047,1055.75,1054.8,1072.25,1069.4,1074,1081.5,1068,1064.95,1068.7,1064.4,1087.85,1093.8,1101.9,1100.95,1100.9,1107.2,1124.9,1095.4,1084.9,1107,1117.5,1119,1120.5,1116.9,1111.8,1087.4,1067.9,1059};
		float inLow[] = cashHandler.getCashDatafromDB(symbol, NOD, 2, tillDate);// {1014.95,1009,1026.95,1032.5,1030.55,1053.1,1046.15,1054.7,1054.2,1035.6,1033.25,1039.25,1038,1049.2,1074.35,1086,1086.1,1074.6,1080.5,1089.25,1070.3,1062.65,1075,1100.3,1100.55,1097.25,1099.2,1076,1057,1038.2,1026.55};
		float inClose[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);// {1017.85,1023.85,1044.95,1039.15,1049.65,1069.2,1055.6,1073.05,1059,1040.35,1047.45,1044.4,1049.45,1081.65,1081.8,1090.35,1098.85,1082,1096.25,1093.2,1074.85,1065.3,1105.05,1105.7,1111.85,1108.55,1102.2,1079.9,1061.85,1054.7,1030.5};

		int startIdx = (2 * optInTimePeriod) - 1;
		int total_days_data = inHigh.length;
		int endIdx = total_days_data - 1;

		MInteger outBegIdx = new MInteger();
		MInteger outNbElement = new MInteger();
		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();
		MInteger outBegIdxB = new MInteger();
		MInteger outNbElementB = new MInteger();


		double outReal[] = new double[inHigh.length];
		double outReal2[] = new double[inHigh.length];
		double outReal3[] = new double[inHigh.length];

		RetCode code = core.adx(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdxA, outNbElementA, outReal);
		boolean errorOccurred = false;

		if(code != RetCode.Success)
		{
			// System.out.println("Error");
			errorOccurred = true;
		}
		RetCode code2 = core.minusDI(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdx, outNbElement, outReal2);
		if(code2 != RetCode.Success)
		{
			// System.out.println("Error");
			errorOccurred = true;
		}
		RetCode code3 = core.plusDI(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdxB, outNbElementB, outReal3);
		if(code3 != RetCode.Success)
		{
			// System.out.println("Error");
			errorOccurred = true;
		}

		if(errorOccurred)
		{
			return null;
		}
		else if(outNbElementA.value > 0)
		{
			// return new Float(outReal[(outNbElementA.value - 1)]);
			// return new Float(outReal2[(outNbElementA.value - 1)]);
			return new Float(outReal3[(outNbElementA.value - 1)]);


		}

		return null;


	}

	public static void calculateADXOrignal()
	{
		Core core = new Core();


		double inHigh[] =
		{ 1025.8, 1031.8, 1047, 1055.75, 1054.8, 1072.25, 1069.4, 1074, 1081.5, 1068, 1064.95, 1068.7, 1064.4, 1087.85, 1093.8, 1101.9, 1100.95, 1100.9, 1107.2, 1124.9, 1095.4, 1084.9, 1107, 1117.5, 1119, 1120.5, 1116.9, 1111.8, 1087.4, 1067.9, 1059 };

		double inLow[] =
		{ 1014.95, 1009, 1026.95, 1032.5, 1030.55, 1053.1, 1046.15, 1054.7, 1054.2, 1035.6, 1033.25, 1039.25, 1038, 1049.2, 1074.35, 1086, 1086.1, 1074.6, 1080.5, 1089.25, 1070.3, 1062.65, 1075, 1100.3, 1100.55, 1097.25, 1099.2, 1076, 1057, 1038.2, 1026.55 };

		double inClose[] =
		{ 1017.85, 1023.85, 1044.95, 1039.15, 1049.65, 1069.2, 1055.6, 1073.05, 1059, 1040.35, 1047.45, 1044.4, 1049.45, 1081.65, 1081.8, 1090.35, 1098.85, 1082, 1096.25, 1093.2, 1074.85, 1065.3, 1105.05, 1105.7, 1111.85, 1108.55, 1102.2, 1079.9, 1061.85, 1054.7, 1030.5 };
		int total_days_data = inHigh.length;


		int optInTimePeriod = 14;
		int startIdx = (2 * optInTimePeriod) - 1;
		int endIdx = total_days_data - 1;
		// System.out.println("StartIndex is "+startIdx +" EndIndex is "+endIdx);
		MInteger outBegIdx = new MInteger();
		MInteger outNbElement = new MInteger();
		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();
		double outReal[] = new double[inHigh.length];
		double outReal2[] = new double[inHigh.length];
		double outReal3[] = new double[inHigh.length];

		RetCode code = core.adx(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdxA, outNbElementA, outReal);
		if(code != RetCode.Success)
		{
			System.out.println("Error");
		}

		// System.out.println(code +" "+outNbElementA.value);

		double retADX[] = new double[outNbElementA.value];
		for (int i = 0; i < retADX.length; i++)
		{
			retADX[i] = outReal[i];
		}

		for( int i = 0; i < retADX.length; i++ ) {
		      //System.out.println(i + " ADX "+retADX[i]);
		  }

		// ////////////////////////////

		RetCode code2 = core.minusDI(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdx, outNbElement, outReal2);
		if(code2 != RetCode.Success)
		{
			System.out.println("Error");
		}

		// System.out.println(code2 +" "+outNbElement.value);
		double retminusdm[] = new double[outNbElement.value];
		for (int i = 0; i < retminusdm.length; i++)
		{
			retminusdm[i] = outReal2[i];

		}
		for( int i = 0; i < retminusdm.length; i++ ) {
		      //System.out.println(i + " -DM "+retminusdm[i]);
		  }

		// //////////////////////////////////
		RetCode code3 = core.plusDI(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdx, outNbElement, outReal3);
		if(code3 != RetCode.Success)
		{
			System.out.println("Error");
		}

		// System.out.println(code3 + " " + outNbElement.value);
		double retplusdm[] = new double[outNbElement.value];
		for (int i = 0; i < retplusdm.length; i++)
		{
			retplusdm[i] = outReal3[i];
		}
		for( int i = 0; i < retplusdm.length; i++ ) {
		      System.out.println(i + " +DM "+retplusdm[i]);
		  }


	}

	public static void calculateMacd(String symbol, Calendar fromDate, Calendar tillDate, int optInFastPeriod, int optInSlowPeriod, int optInSignalPeriod, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateMacd()");
			return;
		}

		Core core = new Core();

		int minNOD = optInSlowPeriod - 1 + optInSignalPeriod - 1 + 1;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);

		float inReal[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);


		// pre -data before todays/first date= optInSlowPeriod-1+optInSignalPeriod-1
		// first data
		int total_days_data = inReal.length;

		int startIdxM = minNOD;
		// int endIdxM =startIdxM;
		int endIdxM = total_days_data - 1;
		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();
		double outMACD[] = new double[inReal.length];
		double outMACDSignal[] = new double[inReal.length];
		double outMACDHist[] = new double[inReal.length];
		RetCode code = core.macd(startIdxM, endIdxM, inReal, optInFastPeriod, optInSlowPeriod, optInSignalPeriod, outBegIdxA, outNbElementA, outMACD, outMACDSignal, outMACDHist);
		if(code != RetCode.Success)
		{
			// System.out.println("Error");
			Macd md = new Macd();
			md.setSymbol(symbol);
			md.setTradeDate(tillDate.getTime());
			md.setFastPeriod(optInFastPeriod);
			md.setSlowPeriod(optInSlowPeriod);
			md.setSignalPeriod(optInSignalPeriod);

			md.setMacd(0);
			md.setMacdSignal(0);
			md.setMacdHistogram(0);
			HibernateBridge.saveOrUpdate(md);
		}

		Calendar newDate = fromDate;
		if(!DatesUtil.isTradingDay(newDate))
			newDate = cashHandler.getNextValidDate(newDate);

		List<Macd> macdList = new ArrayList<Macd>();
		// HashMap hm = new HashMap();

		for (int i = 0; i < outNbElementA.value; i++)
		{
			// Patch to only update the last element
			if(i != (outNbElementA.value - 1))
				continue;
			newDate = tillDate;
			// End Patch

			// System.out.println("Value of i is " + i);
			Macd md = new Macd();
			md.setSymbol(symbol);


			md.setTradeDate(newDate.getTime());


			md.setFastPeriod(optInFastPeriod);
			md.setSlowPeriod(optInSlowPeriod);
			md.setSignalPeriod(optInSignalPeriod);

			md.setMacd(ImageLinks.getRoundedValue((outMACD[i]), 5));
			md.setMacdSignal(ImageLinks.getRoundedValue((outMACDSignal[i]), 5));
			md.setMacdHistogram(ImageLinks.getRoundedValue((outMACDHist[i]), 5));

			// Added by amit on 27/05/2011
			Calendar oldDate = cashHandler.getPreviousValidDate(newDate);
			// System.out.println("Current Date " + newDate.getTime() + " OLD DATE " + oldDate.getTime());
			Macd prevMacd = HibernateBridge.getMACDByPrimaryKeys(symbol, oldDate.getTime(), optInFastPeriod, optInSlowPeriod, optInSignalPeriod);

			if(prevMacd != null)
			{
				double ydayHistoGramValue = prevMacd.getMacdHistogram();
				boolean ydayHistogramType = (ydayHistoGramValue > 0) ? true : false;
				if(ydayHistogramType)
				{
					// System.out.println("ydayHistogramType is POSITIVE");
					if(md.getMacdHistogram() < 0)
					{
						// SignalLineCrossover triggered
						// System.out.println("SignalLineCrossover DETECTED *************");
						md.setLastSignalLineCrossDate(newDate.getTime());
					}
					else
					{
						if(prevMacd.getLastSignalLineCrossDate() != null)
							md.setLastSignalLineCrossDate(prevMacd.getLastSignalLineCrossDate());
						else
						{
							// System.out.println("prevMacd.getLastSignalLineCrossDate is NULLLLLLLLLLLLLLLLLLL ");
						}
					}
				}
				else
				{
					// System.out.println("ydayHistogramType is NEGATIVE");
					if(md.getMacdHistogram() > 0)
					{

						// SignalLineCrossover triggered
						// System.out.println("SignalLineCrossover DETECTED *************");
						md.setLastSignalLineCrossDate(newDate.getTime());
					}
					else
					{
						if(prevMacd.getLastSignalLineCrossDate() != null)
							md.setLastSignalLineCrossDate(prevMacd.getLastSignalLineCrossDate());
						else
						{
							// System.out.println("prevMacd.getLastSignalLineCrossDate is NULLLLLLLLLLLLLLLLLLL ");
						}
					}
				}

				double ydayMACDValue = prevMacd.getMacd();
				boolean ydayMACDType = (ydayMACDValue > 0) ? true : false;

				if(ydayMACDType)
				{
					// System.out.println("ydayMACDType is POSITIVE");
					if(md.getMacd() < 0)
					{
						// CenterLineCrossover triggered
						// System.out.println("CenterLineCrossover &&&&&&&&&&&&");
						md.setLastCenterlineCrossover(newDate.getTime());
					}
					else
					{
						if(prevMacd.getLastCenterlineCrossover() != null)
							md.setLastCenterlineCrossover(prevMacd.getLastCenterlineCrossover());
					}
				}
				else
				{
					// System.out.println("ydayMACDType is NEGATIVE");
					if(md.getMacd() > 0)
					{
						// CenterLineCrossover triggered
						// System.out.println("CenterLineCrossover &&&&&&&&&&&&");
						md.setLastCenterlineCrossover(newDate.getTime());
					}
					else
					{
						if(prevMacd.getLastCenterlineCrossover() != null)
							md.setLastCenterlineCrossover(prevMacd.getLastCenterlineCrossover());
					}
				}


			}
			else
			{
				// System.out.println("PREV MACD OBJECT IS NULL &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
			}

			// END Added by amit on 27/05/2011

			macdList.add(md);
			// retMacd[i] = outMACD[i];

			// hm.put(newDate.getTime(), outMACD[i]);
			// retMacdSignal[i] = outMACDSignal[i];
			// retMacdHistogram[i] = outMACDHist[i];
			newDate = cashHandler.getNextValidDate(newDate);
			HibernateBridge.storeMacd(md);

		}
		// HERE PERSIST THE MACD ARRAY
		// amit - commenting temporarily
		// HibernateBridge.storeMacdList(macdList);


	}*/

	public static Float calculateMacdCustom(long mutualFundId, Calendar fromDate, Calendar tillDate, int optInFastPeriod, int optInSlowPeriod, int optInSignalPeriod)
	{
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateMacd()");
			return null;
		}*/
		Nav nav = null;
		NavController navController = new NavController();

		Core core = new Core();
		// 34 is the minNOD
		int minNOD = optInSlowPeriod - 1 + optInSignalPeriod - 1 + 1;
		int NOD = minNOD + navController.getWorkingDaysBetween(mutualFundId, fromDate.getTime(), tillDate.getTime());
		double close[] = navController.getNav(mutualFundId, NOD, tillDate.getTime());

		// pre -data before todays/first date= optInSlowPeriod-1+optInSignalPeriod-1
		// first data
		int total_days_data = close.length;
		// int startIdxM = minNOD;
		int startIdxM = total_days_data - 30;
		// int endIdxM =startIdxM;
		int endIdxM = total_days_data - 1;
		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();
		double outMACD[] = new double[close.length];
		double outMACDSignal[] = new double[close.length];
		double outMACDHist[] = new double[close.length];
		RetCode code = core.macd(startIdxM, endIdxM, close, optInFastPeriod, optInSlowPeriod, optInSignalPeriod, outBegIdxA, outNbElementA, outMACD, outMACDSignal, outMACDHist);
		if(outNbElementA.value > 0)
		{
			return new Float(outMACD[(outNbElementA.value - 1)]);
		}
		System.out.println("macd lol1");
		return null;
	}

	public static Float calculateMacdSignalCustom(long mutualFundId, Calendar fromDate, Calendar tillDate, int optInFastPeriod, int optInSlowPeriod, int optInSignalPeriod)
	{
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateMacd()");
			return null;
		}*/
		Core core = new Core();
		Nav nav = null;
		NavController navController = new NavController();

		int minNOD = optInSlowPeriod - 1 + optInSignalPeriod - 1 + 1;
		int NOD = minNOD + navController.getWorkingDaysBetween(mutualFundId, fromDate.getTime(), tillDate.getTime());
		double close[] = navController.getNav(mutualFundId, NOD, tillDate.getTime());

		// pre -data before todays/first date= optInSlowPeriod-1+optInSignalPeriod-1
		// first data
		int total_days_data = close.length;
		int startIdxM = minNOD;
		// int endIdxM =startIdxM;
		int endIdxM = total_days_data - 1;
		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();
		double outMACD[] = new double[close.length];
		double outMACDSignal[] = new double[close.length];
		double outMACDHist[] = new double[close.length];
		RetCode code = core.macd(startIdxM, endIdxM, close, optInFastPeriod, optInSlowPeriod, optInSignalPeriod, outBegIdxA, outNbElementA, outMACD, outMACDSignal, outMACDHist);
		if(outNbElementA.value > 0)
		{
			return new Float(outMACDSignal[(outNbElementA.value - 1)]);
		}
		return null;
	}

	public static Float calculateMacdHistogramCustom(long mutualFundId, Calendar fromDate, Calendar tillDate, int optInFastPeriod, int optInSlowPeriod, int optInSignalPeriod)
	{
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateMacd()");
			return null;
		}*/
		NavController navController = new NavController();
		Core core = new Core();
		int minNOD = optInSlowPeriod - 1 + optInSignalPeriod - 1 + 1;
		int NOD = minNOD + navController.getWorkingDaysBetween(mutualFundId, fromDate.getTime(), tillDate.getTime());
		double close[] = navController.getNav(mutualFundId, NOD, tillDate.getTime());

		// pre -data before todays/first date= optInSlowPeriod-1+optInSignalPeriod-1
		// first data
		int total_days_data = close.length;
		int startIdxM = minNOD;
		// int endIdxM =startIdxM;
		int endIdxM = total_days_data - 1;
		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();
		double outMACD[] = new double[close.length];
		double outMACDSignal[] = new double[close.length];
		double outMACDHist[] = new double[close.length];
		RetCode code = core.macd(startIdxM, endIdxM, close, optInFastPeriod, optInSlowPeriod, optInSignalPeriod, outBegIdxA, outNbElementA, outMACD, outMACDSignal, outMACDHist);
		if(outNbElementA.value > 0)
		{
			return new Float(outMACDHist[(outNbElementA.value - 1)]);
		}
		return null;
	}

	/*
	public static void main(String[] args)
	{

		 Calendar startCalendar = Calendar.getInstance();
		 startCalendar.set(2013, 2, 1);
		 Calendar endCalendar = Calendar.getInstance();
		 endCalendar.set(2013, 10, 2);


		 HibernateCashHandler hc = new HibernateCashHandler();

		 Calendar newStartCalendar = (Calendar) startCalendar.clone();
		 try {
		     while (newStartCalendar.getTimeInMillis() <= endCalendar.getTimeInMillis()) {
		         if (com.gumptionlabs.stock.operations.util.DatesUtil.isTradingDay(newStartCalendar)) {
		             Calendar adxCalendar = (Calendar) newStartCalendar.clone();
						adxCalendar.add(Calendar.DATE, -150);
						TALibEngine.calculateADX("INFY", adxCalendar, (Calendar) newStartCalendar.clone(), 14, hc);
						
							Stoch stoch= TALibEngine.calculateStochCustom("INFY", (Calendar) newStartCalendar.clone(), (Calendar) newStartCalendar.clone(), 14, 3, 3, hc);
							Stoch stoch2= TALibEngine.calculateStochCustom("INFY", (Calendar) newStartCalendar.clone(), (Calendar) newStartCalendar.clone(), 29, 3, 4, hc);

							//                    double obv = TALibEngine.calculateObv("TATASTEEL", newStartCalendar, newStartCalendar, 200, hc);
							System.out.println("For [" +newStartCalendar.getTime()+ "] stoch [" +(stoch == null ? "NULL" : stoch.getSlowK() + " -D- "+stoch.getSlowD())+ "]");
							System.out.println("For [" +newStartCalendar.getTime()+ "] stoch2 [" +(stoch2 == null ? "NULL" : stoch2.getSlowK() + " -D- "+stoch2.getSlowD())+ "]");
							}
							newStartCalendar = com.gumptionlabs.stock.operations.util.DatesUtil.incrementDate(newStartCalendar);
							}
							} catch (Exception e) {
							e.printStackTrace();
							}
	}

	public static void calculateSMADeliveryPercentage(String symbol, Calendar fromDate, Calendar tillDate, int optInTimePeriod1, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		DeliveryDetails dd = HibernateBridge.getDeliveryDetailsForSymbol(symbol, tillDate.getTime());
		if(dd == null)
		{
			logger.info("No DeliveryDetails data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateSMADeliveryPercentage()");
			return;
		}

		// pre -data before todays/first date= optInTimePeriod1-1
		Core core = new Core();
		int minNOD = optInTimePeriod1 - 1;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);
		// System.out.println("NOD "+NOD + " symbol [" +symbol+ "] tillDate [" +tillDate.getTime()+ "] fromDate [" +fromDate.getTime()+ "]");
		float inReal[] = cashHandler.getDeliveryPercentageDatafromDB(symbol, NOD, tillDate);


		int total_days_data = inReal.length;

		int startIdx1 = optInTimePeriod1 - 1;
		int endIdx1 = total_days_data - 1;

		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[inReal.length];

		RetCode code1 = core.sma(startIdx1, endIdx1, inReal, optInTimePeriod1, outBegIdx1, outNbElement1, outReal1);
		if(code1 != RetCode.Success)
		{
			// System.out.println("Errorqqqq");
		}
		Calendar newDate = fromDate;
		if(!DatesUtil.isTradingDay(newDate))
			newDate = cashHandler.getNextValidDate(newDate);
		// double retSMA[] = new double[outNbElement1.value];
		List<Averages> averagesList = new ArrayList<Averages>();
		for (int i = 0; i < outNbElement1.value; i++)
		{
			// retSMA[i] = outReal1[i];
			Averages a = HibernateBridge.getAveragesList(symbol, 1, optInTimePeriod1);// deleiverydetils is 1


			if(a == null) // First time , this condition will not be touched for dul updates
			{
				a = new Averages();
				a.setSymbol(symbol);
				a.setTrade_date(newDate.getTime());

				a.setType(1); // 1 for delivery percentage
				a.setAvgDuration(optInTimePeriod1);
				a.setValue(outReal1[i]);
				// a.setValue(outReal1[outNbElement1.value-1-i]);
				averagesList.add(a);

			}
			else
			{
				// Only update the value and date
				a.setTrade_date(newDate.getTime());
				a.setValue(outReal1[i]);
				averagesList.add(a);
			}


			// System.out.println(newDate.getTime());
			newDate = cashHandler.getNextValidDate(newDate);
		}

		// Now store the averages list
		HibernateBridge.updateAveragesList(averagesList);


	}

	public static void calculateSMADeliveryVolume(String symbol, Calendar fromDate, Calendar tillDate, int optInTimePeriod1, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		DeliveryDetails dd = HibernateBridge.getDeliveryDetailsForSymbol(symbol, tillDate.getTime());
		if(dd == null)
		{
			logger.info("No DeliveryDetails data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateSMADeliveryPercentage()");
			return;
		}

		// pre -data before todays/first date= optInTimePeriod1-1
		Core core = new Core();
		int minNOD = optInTimePeriod1 - 1;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);
		// System.out.println("NOD "+NOD + " symbol [" +symbol+ "] tillDate [" +tillDate.getTime()+ "] fromDate [" +fromDate.getTime()+ "]");
		float inReal[] = cashHandler.getDeliveryDetailsDatafromDB(symbol, NOD, tillDate);


		int total_days_data = inReal.length;

		int startIdx1 = optInTimePeriod1 - 1;
		int endIdx1 = total_days_data - 1;

		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[inReal.length];

		RetCode code1 = core.sma(startIdx1, endIdx1, inReal, optInTimePeriod1, outBegIdx1, outNbElement1, outReal1);
		if(code1 != RetCode.Success)
		{
			// System.out.println("Errorqqqq");
		}
		Calendar newDate = fromDate;
		if(!DatesUtil.isTradingDay(newDate))
			newDate = cashHandler.getNextValidDate(newDate);
		// double retSMA[] = new double[outNbElement1.value];
		List<Averages> averagesList = new ArrayList<Averages>();
		for (int i = 0; i < outNbElement1.value; i++)
		{
			// retSMA[i] = outReal1[i];
			Averages a = HibernateBridge.getAveragesList(symbol, 3, optInTimePeriod1);// deleiverydetils is 1


			if(a == null) // First time , this condition will not be touched for dul updates
			{
				a = new Averages();
				a.setSymbol(symbol);
				a.setTrade_date(newDate.getTime());

				a.setType(3); // 3 for delivery Quantity
				a.setAvgDuration(optInTimePeriod1);
				a.setValue(outReal1[i]);
				// a.setValue(outReal1[outNbElement1.value-1-i]);
				averagesList.add(a);

			}
			else
			{
				// Only update the value and date
				a.setTrade_date(newDate.getTime());
				a.setValue(outReal1[i]);
				averagesList.add(a);
			}


			// System.out.println(newDate.getTime());
			newDate = cashHandler.getNextValidDate(newDate);
		}

		// Now store the averages list
		HibernateBridge.updateAveragesList(averagesList);


	}

	public static void calculateSMAVolatality(String symbol, Calendar fromDate, Calendar tillDate, int optInTimePeriod1, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		FOStockVolatality fosv = HibernateBridge.getFOVolatality(symbol, tillDate.getTime());
		if(fosv == null)
		{
			logger.info("No FOStockVolatality found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateSMAVolatality()");
			return;
		}

		// pre -data before todays/first date= optInTimePeriod1-1
		Core core = new Core();
		int minNOD = optInTimePeriod1 - 1;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);
		// System.out.println("NOD "+NOD);
		float inReal[];
		inReal = cashHandler.getVolatalityfromDB(symbol, NOD, tillDate);


		int total_days_data = inReal.length;

		int startIdx1 = optInTimePeriod1 - 1;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[inReal.length];

		RetCode code1 = core.sma(startIdx1, endIdx1, inReal, optInTimePeriod1, outBegIdx1, outNbElement1, outReal1);
		if(code1 != RetCode.Success)
		{
			// System.out.println("Errorqqqq");
		}
		Calendar newDate = fromDate;
		if(!DatesUtil.isTradingDay(newDate))
		{
			newDate = cashHandler.getNextValidDate(newDate);
		}

		List<Averages> averagesList = new ArrayList<Averages>();

		for (int i = 0; i < outNbElement1.value; i++)
		{
			// retSMA[i] = outReal1[i];
			Averages a = HibernateBridge.getAveragesList(symbol, 2, optInTimePeriod1);// fostockvolatality is 2

			if(a == null) // First time , this condition will not be touched for dul updates
			{
				a = new Averages();
				a.setSymbol(symbol);
				a.setTrade_date(newDate.getTime());

				a.setType(2); // fostockvolatality is 2
				a.setAvgDuration(optInTimePeriod1);
				a.setValue(outReal1[i]);
				// a.setValue(outReal1[outNbElement1.value-1-i]);
				averagesList.add(a);

			}
			else
			{
				// System.out.println("&&&&&&&&&& UPDATE CONDITION ");
				// Only update the value and date
				a.setTrade_date(newDate.getTime());
				a.setValue(outReal1[i]);
				averagesList.add(a);
			}


			// System.out.println(newDate.getTime());
			newDate = cashHandler.getNextValidDate(newDate);
		}

		// Now store the averages list
		HibernateBridge.updateAveragesList(averagesList);


	}*/

	public static Float calculateEMACustom(long mutualFundId, Calendar fromDate, Calendar tillDate, int optInTimePeriod1)
	{
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateEMACustom()");
			return null;
		}*/
		// pre -data before todays/first date= optInTimePeriod1-1
		/*Core core = new Core();
		int minNOD = optInTimePeriod1 - 1;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);*/


		NavController navController = new NavController();
		Core core = new Core();
		int minNOD = optInTimePeriod1;
		int NOD = minNOD + navController.getWorkingDaysBetween(mutualFundId, fromDate.getTime(), tillDate.getTime());
		double close[] = navController.getNav(mutualFundId, NOD, tillDate.getTime());

		int total_days_data = close.length;

		int startIdx1 = optInTimePeriod1 - 1;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[close.length];

		RetCode code1 = core.ema(startIdx1, endIdx1, close, optInTimePeriod1, outBegIdx1, outNbElement1, outReal1);
		if(code1 != RetCode.Success)
		{
			return null;
		}
		if(outNbElement1.value > 0)
		{
			return new Float(outReal1[(outNbElement1.value - 1)]);
		}
		return null;


	}

	/*
	 * public static Float calculateDEMACustom(String symbol, Calendar fromDate, Calendar tillDate, int optInTimePeriod1, boolean closePrice, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateEMACustom()");
			return null;
		}
		// pre -data before todays/first date= optInTimePeriod1-1
		Core core = new Core();
		int minNOD = optInTimePeriod1 - 1;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);
		// System.out.println("NOD " + NOD);
		float inReal[];
		if(closePrice)
		{
			inReal = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);
		}
		else
		{
			inReal = cashHandler.getCashDatafromDB(symbol, NOD, 4, tillDate);
		}


		int total_days_data = inReal.length;


		int startIdx1 = optInTimePeriod1 - 1;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[inReal.length];

		RetCode code1 = core.dema(startIdx1, endIdx1, inReal, optInTimePeriod1, outBegIdx1, outNbElement1, outReal1);
		if(code1 != RetCode.Success)
		{
			return null;
		}
		if(outNbElement1.value > 0)
		{
			return new Float(outReal1[(outNbElement1.value - 1)]);
		}
		return null;


	}


	public static Float calculateSMACustom(String symbol, Calendar fromDate, Calendar tillDate, int optInTimePeriod1, boolean closePrice, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateSMACustom()");
			return null;
		}
		// pre -data before todays/first date= optInTimePeriod1-1
		Core core = new Core();
		int minNOD = optInTimePeriod1 - 1;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);
		// System.out.println("NOD " + NOD);
		float inReal[];
		if(closePrice)
		{
			inReal = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);
		}
		else
		{
			inReal = cashHandler.getCashDatafromDB(symbol, NOD, 4, tillDate);
		}


		int total_days_data = inReal.length;


		int startIdx1 = optInTimePeriod1 - 1;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[inReal.length];

		RetCode code1 = core.sma(startIdx1, endIdx1, inReal, optInTimePeriod1, outBegIdx1, outNbElement1, outReal1);

		if(code1 != RetCode.Success)
		{
			return null;
		}
		if(outNbElement1.value > 0)
		{
			return new Float(outReal1[(outNbElement1.value - 1)]);
		}
		return null;


	}

	public static Float calculateWMACustom(String symbol, Calendar fromDate, Calendar tillDate, int optInTimePeriod1, boolean closePrice, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateWMACustom()");
			return null;
		}
		// pre -data before todays/first date= optInTimePeriod1-1
		Core core = new Core();
		int minNOD = optInTimePeriod1 - 1;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);
		// System.out.println("NOD " + NOD);
		float inReal[];
		if(closePrice)
		{
			inReal = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);
		}
		else
		{
			inReal = cashHandler.getCashDatafromDB(symbol, NOD, 4, tillDate);
		}


		int total_days_data = inReal.length;


		int startIdx1 = optInTimePeriod1 - 1;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[inReal.length];

		RetCode code1 = core.wma(startIdx1, endIdx1, inReal, optInTimePeriod1, outBegIdx1, outNbElement1, outReal1);

		if(code1 != RetCode.Success)
		{
			return null;
		}
		if(outNbElement1.value > 0)
		{
			return new Float(outReal1[(outNbElement1.value - 1)]);
		}
		return null;


	}

	public static void calculateSMAOrignal()
	{
		// pre -data before todays/first date= optInTimePeriod1-1
		Core core = new Core();
		double inReal[] =
		{ 937.45, 925.7, 952.65, 962.3, 959.6, 958.55, 992.5, 988.15, 1013.9, 1000.35, 1027.35, 1041.4, 1035.15, 1020.05, 997.65, 1001.65, 1010.2, 998.5, 999, 987.25, 1007.9, 1017.85, 1023.85, 1044.95, 1039.15, 1049.65, 1069.2, 1055.6, 1073.05, 1059, 1040.35, 1047.45, 1044.4, 1049.45, 1081.65,
				1081.8, 1090.35, 1098.85, 1082, 1096.25, 1093.2, 1074.85, 1065.3, 1105.05, 1105.7, 1111.85, 1108.55, 1102.2, 1079.9, 1061.85, 1054.7, 1030.5 };
		int total_days_data = inReal.length;

		int optInTimePeriod1 = 50;
		int startIdx1 = optInTimePeriod1 - 1;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[inReal.length];

		RetCode code1 = core.sma(startIdx1, endIdx1, inReal, optInTimePeriod1, outBegIdx1, outNbElement1, outReal1);
		if(code1 != RetCode.Success)
		{
			System.out.println("Errorqqqq");
		}

		double retSMA[] = new double[outNbElement1.value];
		for (int i = 0; i < retSMA.length; i++)
		{
			retSMA[i] = outReal1[i];

		}


		for (int i = 0; i < retSMA.length; i++)
		{
			// ret[i] = outReal[i];
			System.out.println(+i + " " + retSMA[i]);

		}


	}

	public static void calculateEMAOrignal()
	{
		// pre -data before todays/first date= optInTimePeriod1-1
		Core core = new Core();
		double inReal[] =
		{ 937.45, 925.7, 952.65, 962.3, 959.6, 958.55, 992.5, 988.15, 1013.9, 1000.35, 1027.35, 1041.4, 1035.15, 1020.05, 997.65, 1001.65, 1010.2, 998.5, 999, 987.25, 1007.9, 1017.85, 1023.85, 1044.95, 1039.15, 1049.65, 1069.2, 1055.6, 1073.05, 1059, 1040.35, 1047.45, 1044.4, 1049.45, 1081.65,
				1081.8, 1090.35, 1098.85, 1082, 1096.25, 1093.2, 1074.85, 1065.3, 1105.05, 1105.7, 1111.85, 1108.55, 1102.2, 1079.9, 1061.85, 1054.7, 1030.5 };
		int total_days_data = inReal.length;
		int optInTimePeriod1 = 50;
		int startIdx1 = optInTimePeriod1 - 1;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[inReal.length];

		RetCode code1 = core.ema(startIdx1, endIdx1, inReal, optInTimePeriod1, outBegIdx1, outNbElement1, outReal1);
		if(code1 != RetCode.Success)
		{
			System.out.println("Errorqqqq");
		}

		double retEMA[] = new double[outNbElement1.value];
		for (int i = 0; i < retEMA.length; i++)
		{
			retEMA[i] = outReal1[i];

		}


		for (int i = 0; i < retEMA.length; i++)
		{
			// ret[i] = outReal[i];
			System.out.println(+i + " " + retEMA[i]);

		}


	}

	public static void calculateRSIOrignal()
	{

		// double data[] = getCashData(51);
		// pre -data before todays/first date= optInTimePeriod1-1
		Core core = new Core();
		double inReal[] =
		{ 937.45, 925.7, 952.65, 962.3, 959.6, 958.55, 992.5, 988.15, 1013.9, 1000.35, 1027.35, 1041.4, 1035.15, 1020.05, 997.65, 1001.65, 1010.2, 998.5, 999, 987.25, 1007.9, 1017.85, 1023.85, 1044.95, 1039.15, 1049.65, 1069.2, 1055.6, 1073.05, 1059, 1040.35, 1047.45, 1044.4, 1049.45, 1081.65,
				1081.8, 1090.35, 1098.85, 1082, 1096.25, 1093.2, 1074.85, 1065.3, 1105.05, 1105.7, 1111.85, 1108.55, 1102.2, 1079.9, 1061.85, 1054.7, 1030.5 };
		int total_days_data = inReal.length;
		int optInTimePeriod1 = 14;
		int startIdx1 = optInTimePeriod1 - 1;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[inReal.length];

		RetCode code1 = core.rsi(startIdx1, endIdx1, inReal, optInTimePeriod1, outBegIdx1, outNbElement1, outReal1);
		if(code1 != RetCode.Success)
		{
			System.out.println("Errorqqqq");
		}

		double retRSA[] = new double[outNbElement1.value];
		for (int i = 0; i < retRSA.length; i++)
		{
			retRSA[i] = outReal1[i];

		}


		for (int i = 0; i < retRSA.length; i++)
		{

			System.out.println(+i + " " + retRSA[i]);

		}


	}*/

	/*public static void calculateRSI(String symbol, Calendar fromDate, Calendar tillDate, int optInTimePeriod, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateRSI()");
			return;
		}

		// double data[] = getCashData(51);
		// pre -data before todays/first date= optInTimePeriod1-1
		Core core = new Core();
		int minNOD = optInTimePeriod;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);
		// System.out.println("NOD "+NOD+ "] minNod [" +minNOD+ "] fromDate [" +fromDate.getTime()+ "] tillDate [" +tillDate.getTime()+ "]");

		float inReal[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);


		int total_days_data = inReal.length;

		// int startIdx1 = optInTimePeriod - 1;
		int startIdx1 = optInTimePeriod;
		int endIdx1 = total_days_data - 1;


		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[inReal.length];

		RetCode code1 = core.rsi(startIdx1, endIdx1, inReal, optInTimePeriod, outBegIdx1, outNbElement1, outReal1);
		if(code1 != RetCode.Success)
		{
			System.out.println("Symbol [" +symbol+ "] Errorqqqq");
			System.out.println("Return count "+outNbElement1.value);
			Rsi md = new Rsi();
			md.setSymbol(symbol);
			md.setTradeDate(tillDate.getTime());
			md.setPeriod(optInTimePeriod);
			md.setRsi(0);
			HibernateBridge.saveOrUpdate(md);
		}


		if(outNbElement1.value > 0)
		{
			Rsi md = new Rsi();
			md.setSymbol(symbol);
			md.setTradeDate(tillDate.getTime());
			md.setPeriod(optInTimePeriod);
			md.setRsi(ImageLinks.getRoundedValue((outReal1[(outNbElement1.value - 1)]), 5));
			HibernateBridge.saveOrUpdate(md);
		}
		
		Calendar newDate = fromDate;
		if (!DatesUtil.isTradingDay(newDate))
		    newDate = cashHandler.getNextValidDate(newDate);

		List<Rsi> rsiList = new ArrayList<Rsi>();

		for (int i = 0; i < outNbElement1.value; i++) {
		    //retRSA[i] = outReal1[i];
		    Rsi md = new Rsi();
		    md.setSymbol(symbol);
		    md.setTradeDate(newDate.getTime()); //Commented
		//            md.setTradeDate(tillDate.getTime());
		    md.setPeriod(optInTimePeriod);
		    md.setRsi(outReal1[i]);

		    rsiList.add(md);
		//			System.out.println(newDate.getTime());
		    newDate = cashHandler.getNextValidDate(newDate);

		}

		// HERE PERSIST THE RSI List
		//  commented by Amit on 23/05/2011 as we wil only update the last value
		//HibernateBridge.storeRsiList(rsiList);
		if(rsiList.size() > 0) {
		    HibernateBridge.storeRsi(rsiList.get(rsiList.size() - 1));
		}


	}
	*/

	public static Float calculateRSICustom(long mutualFundId, Calendar fromDate, Calendar tillDate, int optInTimePeriod)
	{
		/*Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{	
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateRSICustom()");
			return null;
		}*/
		Nav nav = null;
		NavController navController = new NavController();

		// double data[] = getCashData(51);
		// pre -data before todays/first date= optInTimePeriod1-1
		Core core = new Core();
		int minNOD = optInTimePeriod;
		int NOD = minNOD + navController.getWorkingDaysBetween(mutualFundId, fromDate.getTime(), tillDate.getTime());
		// System.out.println("NOD "+NOD+ "] minNod [" +minNOD+ "] fromDate [" +fromDate.getTime()+ "] tillDate [" +tillDate.getTime()+ "]");

		double close[] = navController.getNav(mutualFundId, NOD, tillDate.getTime());

		int total_days_data = close.length;
		int startIdx1 = optInTimePeriod;
		int endIdx1 = total_days_data - 1;

		MInteger outBegIdx1 = new MInteger();
		MInteger outNbElement1 = new MInteger();
		double outReal1[] = new double[close.length];


		RetCode code1 = core.rsi(startIdx1, endIdx1, close, optInTimePeriod, outBegIdx1, outNbElement1, outReal1);

		if(code1 != RetCode.Success)
		{
			// return null or 0
			return null;
		}


		// System.out.println("outNbElement1.value [" + outNbElement1.value + "]");
		if(outNbElement1.value > 0)
		{
			return new Float(outReal1[(outNbElement1.value - 1)]);
		}

		return null;
	}

	/*public static void calculateStochRSI()
	{
		// pre -data before todays/first date= optInTimePeriod1-1
		Core core = new Core();
		double inReal[] =
		{ 937.45, 925.7, 952.65, 962.3, 959.6, 958.55, 992.5, 988.15, 1013.9, 1000.35, 1027.35, 1041.4, 1035.15, 1020.05, 997.65, 1001.65, 1010.2, 998.5, 999, 987.25, 1007.9, 1017.85, 1023.85, 1044.95, 1039.15, 1049.65, 1069.2, 1055.6, 1073.05, 1059, 1040.35, 1047.45, 1044.4, 1049.45, 1081.65,
				1081.8, 1090.35, 1098.85, 1082, 1096.25, 1093.2, 1074.85, 1065.3, 1105.05, 1105.7, 1111.85, 1108.55, 1102.2, 1079.9, 1061.85, 1054.7, 1030.5 };
		int total_days_data = inReal.length;
		int optInTimePeriod = 14;
		int startIdx = optInTimePeriod - 1;
		int endIdx = total_days_data - 1;

		int optInFastK_Period = 5;
		int optInFastD_Period = 3;
		MAType optInFastD_MAType = MAType.Sma;


		MInteger outBegIdx = new MInteger();
		MInteger outNbElement = new MInteger();
		double outFastK[] = new double[inReal.length];
		double outFastD[] = new double[inReal.length];

		RetCode code1 = core.stochRsi(startIdx, endIdx, inReal, optInTimePeriod, optInFastK_Period, optInFastD_Period, optInFastD_MAType, outBegIdx, outNbElement, outFastK, outFastD);

		if(code1 != RetCode.Success)
		{
			System.out.println("Errorqqqq");
		}

		double retFastK[] = new double[outNbElement.value];
		double retFastD[] = new double[outNbElement.value];
		for (int i = 0; i < retFastK.length; i++)
		{
			retFastK[i] = outFastK[i];
			retFastD[i] = outFastD[i];

		}


		for (int i = 0; i < retFastK.length; i++)
		{

			System.out.println(+i + " K " + retFastK[i]);
			System.out.println(+i + " D " + retFastD[i]);

		}


	}*/

	/*public static Stoch calculateStochCustom(String symbol, Calendar fromDate, Calendar tillDate, int optInFastK_Period, int optInSlowK_Period, int optInSlowD_Period, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateStoch()");
			return null;
		}

		Core core = new Core();
		int minNOD = optInFastK_Period + optInSlowK_Period + optInSlowK_Period;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);
		// System.out.println("NOD "+NOD);

		float inHigh[] = cashHandler.getCashDatafromDB(symbol, NOD, 1, tillDate);
		float inLow[] = cashHandler.getCashDatafromDB(symbol, NOD, 2, tillDate);
		float inClose[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);

		int total_days_data = inHigh.length;
		// System.out.println(inHigh.length);


		int startIdx = optInFastK_Period + optInSlowK_Period + optInSlowK_Period;
		int endIdx = total_days_data - 1;


		MInteger outNbElement = new MInteger();
		MInteger outBegIdxA = new MInteger();
		MInteger outNBElement = new MInteger();
		double outSlowK[] = new double[inHigh.length];
		double outSlowD[] = new double[inHigh.length];


		// This also can be configured - Will do LAter //AMIT
		MAType optInSlowK_MAType = MAType.Sma;
		MAType optInSlowD_MAType = MAType.Sma;


		RetCode code = core.stoch(startIdx, endIdx, inHigh, inLow, inClose, optInFastK_Period, optInSlowK_Period, optInSlowK_MAType, optInSlowD_Period, optInSlowD_MAType, outBegIdxA, outNBElement, outSlowK, outSlowD);
		if(code != RetCode.Success)
		{
			return null;
		}

		if(outNBElement.value > 0)
		{
			Stoch st = new Stoch();
			st.setSymbol(symbol);
			st.setTradeDate(tillDate.getTime());
			st.setFastkPeriod(optInFastK_Period);
			st.setSlowkPeriod(optInSlowK_Period);
			st.setSlowdPeriod(optInSlowD_Period);
			st.setSlowK(ImageLinks.getRoundedValue((outSlowK[(outNBElement.value - 1)]), 5));
			st.setSlowD(ImageLinks.getRoundedValue((outSlowD[(outNBElement.value - 1)]), 5));
			return st;

		}

		return null;

	}*/

	/*public static FastStoch calculateFastStochCustom(String symbol, Calendar fromDate, Calendar tillDate, int optInFastK_Period, int optInFastD_Period, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateStoch()");
			return null;
		}

		Core core = new Core();
		int minNOD = optInFastK_Period + optInFastD_Period + optInFastD_Period;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);
		// System.out.println("NOD "+NOD);

		float inHigh[] = cashHandler.getCashDatafromDB(symbol, NOD, 1, tillDate);
		float inLow[] = cashHandler.getCashDatafromDB(symbol, NOD, 2, tillDate);
		float inClose[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);

		int total_days_data = inHigh.length;
		// System.out.println(inHigh.length);


		int startIdx = optInFastK_Period + optInFastD_Period + optInFastD_Period;
		int endIdx = total_days_data - 1;


		MInteger outBegIdxA = new MInteger();
		MInteger outNBElement = new MInteger();
		double outFastK[] = new double[inHigh.length];
		double outFastD[] = new double[inHigh.length];


		// This also can be configured - Will do LAter //AMIT
		MAType optInFastD_MAType = MAType.Sma;


		RetCode code = core.stochF(startIdx, endIdx, inHigh, inLow, inClose, optInFastK_Period, optInFastD_Period, optInFastD_MAType, outBegIdxA, outNBElement, outFastK, outFastD);
		if(code != RetCode.Success)
		{
			return null;
		}

		if(outNBElement.value > 0)
		{
			FastStoch st = new FastStoch();
			st.setSymbol(symbol);
			st.setTradeDate(tillDate.getTime());
			st.setFastKPeriod(optInFastK_Period);
			st.setFastK(ImageLinks.getRoundedValue((outFastK[(outNBElement.value - 1)]), 2));
			st.setFastD(ImageLinks.getRoundedValue((outFastD[(outNBElement.value - 1)]), 2));
			return st;

		}

		return null;

	}*/

	/*public static void calculateStoch(String symbol, Calendar fromDate, Calendar tillDate, int optInFastK_Period, int optInSlowK_Period, int optInSlowD_Period, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateStoch()");
			return;
		}

		Core core = new Core();
		int minNOD = optInFastK_Period + optInSlowK_Period + optInSlowK_Period - 3;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);
		// System.out.println("NOD "+NOD);

		float inHigh[] = cashHandler.getCashDatafromDB(symbol, NOD, 1, tillDate);
		float inLow[] = cashHandler.getCashDatafromDB(symbol, NOD, 2, tillDate);
		float inClose[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);

		int total_days_data = inHigh.length;
		// System.out.println(inHigh.length);


		int startIdx = optInFastK_Period + optInSlowK_Period + optInSlowK_Period - 3;
		int endIdx = total_days_data - 1;


		MInteger outNbElement = new MInteger();
		MInteger outBegIdxA = new MInteger();
		MInteger outNBElement = new MInteger();
		double outSlowK[] = new double[inHigh.length];
		double outSlowD[] = new double[inHigh.length];


		// This also can be configured - Will do LAter //AMIT
		MAType optInSlowK_MAType = MAType.Sma;
		MAType optInSlowD_MAType = MAType.Sma;


		RetCode code = core.stoch(startIdx, endIdx, inHigh, inLow, inClose, optInFastK_Period, optInSlowK_Period, optInSlowK_MAType, optInSlowD_Period, optInSlowD_MAType, outBegIdxA, outNBElement, outSlowK, outSlowD);
		if(code != RetCode.Success)
		{
			System.out.println("Error");
		}
		Calendar newDate = fromDate;
		if(!DatesUtil.isTradingDay(newDate))
		{
			newDate = cashHandler.getNextValidDate(newDate);
		}
		// System.out.println(code +" *** "+outNBElement.value);
		List<Stoch> stochList = new ArrayList<Stoch>();
		for (int i = 0; i < outNBElement.value; i++)
		{
			Stoch st = new Stoch();
			st.setSymbol(symbol);
			st.setTradeDate(newDate.getTime());
			st.setFastkPeriod(optInFastK_Period);
			st.setSlowkPeriod(optInSlowK_Period);
			st.setSlowdPeriod(optInSlowD_Period);
			st.setSlowK(ImageLinks.getRoundedValue((outSlowK[i]), 5));
			st.setSlowD(ImageLinks.getRoundedValue((outSlowD[i]), 5));
			stochList.add(st);
			// retslowK[i] = outSlowK[i];
			// retslowD[i] = outSlowD[i];
			// System.out.println(newDate.getTime());
			newDate = cashHandler.getNextValidDate(newDate);
		}

		// Now store the toch List
		// HibernateBridge.storeStochList(stochList);
		if(stochList.size() > 0)
		{
			HibernateBridge.storeStoch(stochList.get(stochList.size() - 1));
		}


	}*/

	/*public static void calculateStochOrignal()
	{
		Core core = new Core();
		
		   *  lookbackK = optInFastK_Period-1;
		lookbackKSlow = movingAverageLookback ( optInSlowK_Period, optInSlowK_MAType );
		lookbackDSlow = movingAverageLookback ( optInSlowD_Period, optInSlowD_MAType );
		lookbackTotal = lookbackK + lookbackDSlow + lookbackKSlow;

		  double inHigh[] = {937.45,925.7,952.65,962.3,959.6,958.55,992.5,988.15,1013.9,1000.35,1027.35,1041.4,1035.15,1020.05,997.65,1001.65,1010.2,	998.5,999,987.25,1007.9,1017.85,1023.85,1044.95,1039.15,1049.65,1069.2,1055.6,1073.05,1059,1040.35,1047.45,1044.4,1049.45,1081.65,1081.8,1090.35,1098.85,1082,1096.25,1093.2,1074.85,1065.3,1105.05,1105.7,1111.85,1108.55,1102.2,1079.9,1061.85,1054.7,1030.5};
		  double inLow[] = {937.45,925.7,952.65,962.3,959.6,958.55,992.5,988.15,1013.9,1000.35,1027.35,1041.4,1035.15,1020.05,997.65,1001.65,1010.2,	998.5,999,987.25,1007.9,1017.85,1023.85,1044.95,1039.15,1049.65,1069.2,1055.6,1073.05,1059,1040.35,1047.45,1044.4,1049.45,1081.65,1081.8,1090.35,1098.85,1082,1096.25,1093.2,1074.85,1065.3,1105.05,1105.7,1111.85,1108.55,1102.2,1079.9,1061.85,1054.7,1030.5};
		  double inClose[] = {937.45,925.7,952.65,962.3,959.6,958.55,992.5,988.15,1013.9,1000.35,1027.35,1041.4,1035.15,1020.05,997.65,1001.65,1010.2,	998.5,999,987.25,1007.9,1017.85,1023.85,1044.95,1039.15,1049.65,1069.2,1055.6,1073.05,1059,1040.35,1047.45,1044.4,1049.45,1081.65,1081.8,1090.35,1098.85,1082,1096.25,1093.2,1074.85,1065.3,1105.05,1105.7,1111.85,1108.55,1102.2,1079.9,1061.85,1054.7,1030.5};
		  
		double inHigh[] =
		{ 1025.8, 1031.8, 1047, 1055.75, 1054.8, 1072.25, 1069.4, 1074, 1081.5, 1068, 1064.95, 1068.7, 1064.4, 1087.85, 1093.8, 1101.9, 1100.95, 1100.9, 1107.2, 1124.9, 1095.4, 1084.9, 1107, 1117.5, 1119, 1120.5, 1116.9, 1111.8, 1087.4, 1067.9, 1059 };

		double inLow[] =
		{ 1014.95, 1009, 1026.95, 1032.5, 1030.55, 1053.1, 1046.15, 1054.7, 1054.2, 1035.6, 1033.25, 1039.25, 1038, 1049.2, 1074.35, 1086, 1086.1, 1074.6, 1080.5, 1089.25, 1070.3, 1062.65, 1075, 1100.3, 1100.55, 1097.25, 1099.2, 1076, 1057, 1038.2, 1026.55 };

		double inClose[] =
		{ 1017.85, 1023.85, 1044.95, 1039.15, 1049.65, 1069.2, 1055.6, 1073.05, 1059, 1040.35, 1047.45, 1044.4, 1049.45, 1081.65, 1081.8, 1090.35, 1098.85, 1082, 1096.25, 1093.2, 1074.85, 1065.3, 1105.05, 1105.7, 1111.85, 1108.55, 1102.2, 1079.9, 1061.85, 1054.7, 1030.5 };
		int total_days_data = inHigh.length;
		System.out.println(inHigh.length);

		int optInFastK_Period = 5;
		int optInSlowK_Period = 3;
		int optInSlowD_Period = 3;
		int startIdx = optInFastK_Period + optInSlowK_Period + optInSlowK_Period - 3;
		int endIdx = total_days_data - 1;


		MInteger outNbElement = new MInteger();
		MInteger outBegIdxA = new MInteger();
		MInteger outNBElement = new MInteger();
		double outSlowK[] = new double[inHigh.length];
		double outSlowD[] = new double[inHigh.length];


		MAType optInSlowK_MAType = MAType.Sma;
		MAType optInSlowD_MAType = MAType.Sma;


		RetCode code = core.stoch(startIdx, endIdx, inHigh, inLow, inClose, optInFastK_Period, optInSlowK_Period, optInSlowK_MAType, optInSlowD_Period, optInSlowD_MAType, outBegIdxA, outNBElement, outSlowK, outSlowD);
		if(code != RetCode.Success)
		{
			System.out.println("Error");
		}

		System.out.println(code + " *** " + outNBElement.value);
		double retslowK[] = new double[outNBElement.value];
		double retslowD[] = new double[outNBElement.value];
		System.out.println(retslowK.length);
		for (int i = 0; i < retslowK.length; i++)
		{

			retslowK[i] = outSlowK[i];
			retslowD[i] = outSlowD[i];
		}

		for (int i = 0; i < retslowK.length; i++)
		{
			System.out.println(i + " K " + retslowK[i]);
			System.out.println(i + " D " + retslowD[i]);
		}


	}*/

	/*public static void calculateTrix(String symbol, Calendar fromDate, Calendar tillDate, int optInTimePeriod, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateTrix()");
			return;
		}

		Core core = new Core();
		int minNOD = 50 + optInTimePeriod;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);
		float inClose[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);

		int startIdx = optInTimePeriod;
		int total_days_data = inClose.length;
		int endIdx = total_days_data - 1;

		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();


		double outReal[] = new double[inClose.length];

		RetCode code = core.trix(startIdx, endIdx, inClose, optInTimePeriod, outBegIdxA, outNbElementA, outReal);


		if(code != RetCode.Success)
		{
			Trix t = new Trix();
			t.setSymbol(symbol);
			t.setTradeDate(tillDate.getTime());
			t.setPeriod(optInTimePeriod);
			t.setTrix(0);
			HibernateBridge.saveOrUpdate(t);
		}


		if(outNbElementA.value > 0)
		{
			Trix t = new Trix();
			t.setSymbol(symbol);
			t.setTradeDate(tillDate.getTime());
			t.setPeriod(optInTimePeriod);
			t.setTrix((float) ImageLinks.getRoundedValue((outReal[(outNbElementA.value - 1)]), 3));
			HibernateBridge.saveOrUpdate(t);
		}
	}*/

	/*public static Float calculateTrixCustom(String symbol, Calendar fromDate, Calendar tillDate, int optInTimePeriod, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
		if(cash == null)
		{
			logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateTrixCustom()");
			return null;
		}

		Core core = new Core();
		int minNOD = optInTimePeriod;
		int NOD = minNOD + cashHandler.getWorkingDaysBetween(fromDate, tillDate);

		float inClose[] = cashHandler.getCashDatafromDB(symbol, NOD, 3, tillDate);// {1017.85,1023.85,1044.95,1039.15,1049.65,1069.2,1055.6,1073.05,1059,1040.35,1047.45,1044.4,1049.45,1081.65,1081.8,1090.35,1098.85,1082,1096.25,1093.2,1074.85,1065.3,1105.05,1105.7,1111.85,1108.55,1102.2,1079.9,1061.85,1054.7,1030.5};

		int startIdx = optInTimePeriod;
		int total_days_data = inClose.length;
		int endIdx = total_days_data - 1;

		MInteger outBegIdxA = new MInteger();
		MInteger outNbElementA = new MInteger();


		double outReal[] = new double[inClose.length];

		RetCode code = core.trix(startIdx, endIdx, inClose, optInTimePeriod, outBegIdxA, outNbElementA, outReal);

		if(code != RetCode.Success)
		{
			return null;
		}


		if(outNbElementA.value > 0)
		{
			return new Float(outReal[(outNbElementA.value - 1)]);

		}
		return null;

	}*/

	/*public static Float calculateEMAOf(int optInTimePeriod, float[] inReal) throws StrategyLabsException
	{
		if(inReal == null)
		{
			return null;
		}

		try
		{
			Core core = new Core();
			int total_days_data = inReal.length;
			int startIdx1 = optInTimePeriod - 1;
			int endIdx1 = total_days_data - 1;


			MInteger outBegIdx1 = new MInteger();
			MInteger outNbElement1 = new MInteger();
			double outReal1[] = new double[inReal.length];

			RetCode code1 = core.ema(startIdx1, endIdx1, inReal, optInTimePeriod, outBegIdx1, outNbElement1, outReal1);
			if(code1 != RetCode.Success)
			{
				return null;
			}

			if(outNbElement1.value > 0)
			{
				return new Float(outReal1[(outNbElement1.value - 1)]);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;


	}*/

	/*public static Float calculateSMAOf(int optInTimePeriod, float[] inReal) throws StrategyLabsException
	{
		if(inReal == null)
		{
			return null;
		}

		try
		{
			Core core = new Core();
			int total_days_data = inReal.length;
			int startIdx1 = optInTimePeriod - 1;
			int endIdx1 = total_days_data - 1;


			MInteger outBegIdx1 = new MInteger();
			MInteger outNbElement1 = new MInteger();
			double outReal1[] = new double[inReal.length];

			RetCode code1 = core.sma(startIdx1, endIdx1, inReal, optInTimePeriod, outBegIdx1, outNbElement1, outReal1);
			if(code1 != RetCode.Success)
			{
				return null;
			}

			if(outNbElement1.value > 0)
			{
				return new Float(outReal1[(outNbElement1.value - 1)]);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;


	}*/

	/*public static Float calculateRoc(String symbol, Calendar tillDate, int optInPeriod, HibernateCashHandler cashHandler) throws StrategyLabsException
	{
		try
		{
			Cash cash = HibernateBridge.getCashByCashId(symbol, tillDate.getTime());
			if(cash == null)
			{
				logger.info("No cash data found for Symbol [" + symbol + "] trade date [" + tillDate.getTime() + "] in calculateRoc()");
				return 0f;
			}

			Core core = new Core();

			float inReal[] = cashHandler.getCashDatafromDB(symbol, optInPeriod + 1, 3, tillDate);


			if(inReal.length == 0)
			{
				return 0f;
			}
			int startIdx1 = 0;
			int endIdx1 = inReal.length - 1;


			MInteger outBegIdx1 = new MInteger();
			MInteger outNbElement1 = new MInteger();
			double outReal1[] = new double[inReal.length];

			RetCode code1 = core.roc(startIdx1, endIdx1, inReal, optInPeriod, outBegIdx1, outNbElement1, outReal1);

			if(code1 != RetCode.Success)
			{
				return 0f;
			}


			if(outNbElement1.value > 0)
			{
				return ((Double) ImageLinks.getRoundedValue((outReal1[(outNbElement1.value - 1)]), 2)).floatValue();
			}
			else
			{
				return 0f;
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new StrategyLabsException("Exception occurred while calculating OBV for symbol [" + symbol + "]");
		}

	}*/
}
