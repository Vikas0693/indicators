package com.fundexpert.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.apidoc.util.ImageLinks;
import com.fundexpert.dao.EveryDayCandle;
import com.fundexpert.dao.Ichimoku;
import com.fundexpert.dao.Nav;

public class IchimokuController
{
	public void persistDailyIchimokuFromEveryDay(Long min,Long max)
	{
		System.out.println("Insert closePrice into ichimoku");
		Transaction tx = null;
		Session hSession = null;

		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List<Long> mutualFundList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where mutualFundId>=? and mutualFundId<=?").setParameter(0, min).setParameter(1, max).list();
			Iterator itr = mutualFundList.iterator();
			while (itr.hasNext())
			{
				long mfId = (long) itr.next();
				System.out.println("insert daily close into ichimoku for="+mfId);
				tx = hSession.beginTransaction();
				List everyDayList = hSession.createQuery("from EveryDayCandle where mutualFundId=? and candleType=? order by tradeDate").setParameter(0, mfId).setParameter(1, 1).list();
				tx = hSession.beginTransaction();
				// persist ichimoku since we have to store ssb value after 26 days from current day
				Ichimoku ichimoku = null;
				Iterator itr1 = everyDayList.iterator();
				EveryDayCandle edc = null;
				while (itr1.hasNext())
				{
					ichimoku = new Ichimoku();
					edc = (EveryDayCandle) itr1.next();
					ichimoku.setMutualFundId(mfId);
					ichimoku.setClose(edc.getTodaysClosePrice());
					ichimoku.setTradeDate(edc.getTradeDate());
					ichimoku.setCandleType(1);
					hSession.saveOrUpdate(ichimoku);
				}
				hSession.flush();
				tx.commit();
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateSSB(Long minM,Long maxM)
	{
		System.out.println("Update SSB");
		Session hSession = null;
		Transaction tx = null;
		try
		{

			hSession = HibernateBridge.getSessionFactory().openSession();
			// tx = hSession.beginTransaction();
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where mutualFundId>=? and mutualFundId<=?").setParameter(0, minM).setParameter(1, maxM).list();
			Iterator itr = mutualFundList.iterator();
			long mfId = 0;

			while (itr.hasNext())
			{
				System.gc();
				mfId = (long) itr.next();
				System.out.println("update ssb for ="+mfId);
				List<Ichimoku> ichimokuList = hSession.createQuery("from Ichimoku where mutualFundId=? and candleType=? order by tradeDate").setParameter(0, mfId).setParameter(1, 1).list();
				// saving ichimoku with ssb values
				Ichimoku ichimoku = null;
				List<Double> previous52Days = null;
	
				tx = hSession.beginTransaction();
				for (int i = 0; i < ichimokuList.size(); i++)
				{
					// i<52
					if(i < 51)
					{
						// System.out.println("i=" + i);
						continue;
					}
					// ( ichimokuList.size()-1 ) - 26
					if(i > (ichimokuList.size() - 1) - 25)
					{
						// since ssb is the value of 26 days later from current we have to exit before 26th day from last day stored in our database
						// System.out.print("Break");
						break;
					}
					previous52Days = new ArrayList<Double>();
					double max = 0, min = 0, avg;
					// j >= i - 52
					for (int j = i; j >= i - 51; j--)
					{
						// System.out.println("j=" + j);
						previous52Days.add(ichimokuList.get(j).getClose());
					}
					max = Collections.max(previous52Days);
					min = Collections.min(previous52Days);
					avg = (max + min) / 2.0;
					avg = ImageLinks.getRoundedValue(avg, 5);
					// i+26;
					int indexAfter26Days = i + 25;// we have to consider current day also while calculating indexAfter26Days
					ichimoku = ichimokuList.get(indexAfter26Days);
					ichimoku.setSenkouSpanB(avg);

					hSession.saveOrUpdate(ichimoku);
				}
				hSession.flush();
				tx.commit();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateTenkanSen(Long minM,Long maxM)
	{
		System.out.println("Update tenkanSen");
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			// tx = hSession.beginTransaction();
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where mutualFundId>=? and mutualFundId<=?").setParameter(0, minM).setParameter(1, maxM).list();
			Iterator itr = mutualFundList.iterator();
			long mfId = 0;

			while (itr.hasNext())
			{
				System.gc();
				mfId = (long) itr.next();
				System.out.println("Update daily tenkanSen for ="+mfId);
				List<Ichimoku> ichimokuList = hSession.createQuery("from Ichimoku where mutualFundId=? and candleType=? order by tradeDate").setParameter(0, mfId).setParameter(1, 1).list();
				
				// saving ichimoku with ssb values
				Ichimoku ichimoku = null;
				List<Double> previous9Days = null;
				tx = hSession.beginTransaction();
				for (int i = 0; i < ichimokuList.size(); i++)
				{
					// i<9
					if(i < 8)
					{
						// System.out.println("i=" + i);
						continue;
					}
					previous9Days = new ArrayList<Double>();
					double max = 0, min = 0, avg;
					for (int j = i; j >= i - 8; j--)
					{
						previous9Days.add(ichimokuList.get(j).getClose());
					}
					max = Collections.max(previous9Days);
					min = Collections.min(previous9Days);
					avg = (max + min) / 2.0d;
					avg = ImageLinks.getRoundedValue(avg, 5);
					// System.out.println("max=" + max + "		min=" + min + "		avg=" + avg + "		at i=" + i);
					ichimoku = ichimokuList.get(i);
					ichimoku.setTenkanSen(avg);
					// saving ichimoku with ts indicators
					hSession.saveOrUpdate(ichimoku);
				}
				hSession.flush();
				tx.commit();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}

	}

	public void updateKijunSen(Long minM,Long maxM)
	{
		System.out.println("Update kijunSen");
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			// tx = hSession.beginTransaction();
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where mutualFundId>=? and mutualFundId<=?").setParameter(0, minM).setParameter(1, maxM).list();
			Iterator itr = mutualFundList.iterator();
			long mfId = 0;

			while (itr.hasNext())
			{
				System.gc();
				mfId = (long) itr.next();

				List<Ichimoku> ichimokuList = hSession.createQuery("from Ichimoku where mutualFundId=? and candleType=? order by tradeDate").setParameter(0, mfId).setParameter(1, 1).list();
				System.out.println("update daily KijunSen for ="+mfId);
				Ichimoku ichimoku = null;
				List<Double> previous26Days = null;
				tx = hSession.beginTransaction();
				for (int i = 0; i < ichimokuList.size(); i++)
				{
					// i<26
					if(i < 25)
					{
						// System.out.println("i=" + i);
						continue;
					}

					previous26Days = new ArrayList<Double>();
					double max = 0, min = 0, avg;
					// /j = i - 1; j >= i - 26;
					for (int j = i; j >= i - 25; j--)
					{
						previous26Days.add(ichimokuList.get(j).getClose());
					}
					max = Collections.max(previous26Days);
					min = Collections.min(previous26Days);
					avg = (max + min) / 2.0;
					avg = ImageLinks.getRoundedValue(avg, 5);
					// System.out.println("max=" + max + "		min=" + min + "		avg=" + avg + "		at i=" + i);

					ichimoku = ichimokuList.get(i);
					ichimoku.setKijunSen(avg);
					// saving ichimoku with ks values
					hSession.saveOrUpdate(ichimoku);
				}
				hSession.flush();
				tx.commit();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateSSA(Long minM,Long maxM)
	{
		System.out.println("Update SSA");
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			// tx = hSession.beginTransaction();
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where mutualFundId>=? and mutualFundId<=?").setParameter(0, minM).setParameter(1, maxM).list();
			Iterator itr = mutualFundList.iterator();
			long mfId = 0;

			while (itr.hasNext())
			{
				System.gc();
				mfId = (long) itr.next();
				System.out.println("Updae Daily SSA for ="+mfId);
				List<Ichimoku> ichimokuList = hSession.createQuery("from Ichimoku where mutualFundId=? and candleType=? order by tradeDate").setParameter(0, mfId).setParameter(1, 1).list();

				Ichimoku ichimoku = null;
				double tenkanSen = 0, kijunSen = 0, avg = 0;
				tx = hSession.beginTransaction();
				for (int i = 0; i < ichimokuList.size(); i++)
				{
					if(i > (ichimokuList.size() - 1) - 25)
					{
						// since ssa is the value of 26 days ahead from current we have to exit before 26th day from last day stored in our database
						// System.out.print("Break");
						break;
					}
					ichimoku = ichimokuList.get(i);
					tenkanSen = ichimoku.getTenkanSen();
					kijunSen = ichimoku.getKijunSen();
					avg = (tenkanSen + kijunSen) / 2.0;
					// i+26 (Now i+25 because we have to consider 26 days including 1 day of current day)
					ichimoku = ichimokuList.get(i + 25);
					avg = ImageLinks.getRoundedValue(avg, 5);
					ichimoku.setSenkouSpanA(avg);

					hSession.saveOrUpdate(ichimoku);
				}
				hSession.flush();
				tx.commit();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateKumo(Long minM,Long maxM)
	{
		System.out.println("Update Kumo");
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List mutualFundList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where mutualFundId>=? and mutualFundId<=?").setParameter(0, minM).setParameter(1,maxM).list();
			Iterator itr = mutualFundList.iterator();
			while (itr.hasNext())
			{
				System.gc();
				long mfId = (Long) itr.next();
				System.out.println("Update Daily Kumo for ="+mfId);
				List<Ichimoku> ichimokuList = hSession.createQuery("from Ichimoku where mutualFundId=? and candleType=? order by tradeDate").setParameter(0, mfId).setParameter(1, 1).list();
				Iterator<Ichimoku> itr2 = ichimokuList.iterator();
				Ichimoku ichimoku = null;
				double kumoTop = 0, kumoBottom = 0;
				tx = hSession.beginTransaction();
				while (itr2.hasNext())
				{
					ichimoku = itr2.next();
					kumoTop = ichimoku.getSenkouSpanA() > ichimoku.getSenkouSpanB() ? ichimoku.getSenkouSpanA() : ichimoku.getSenkouSpanB();
					kumoBottom = ichimoku.getSenkouSpanA() < ichimoku.getSenkouSpanB() ? ichimoku.getSenkouSpanA() : ichimoku.getSenkouSpanB();
					ichimoku.setKumoTop(kumoTop);
					ichimoku.setKumoBottom(kumoBottom);
					hSession.saveOrUpdate(ichimoku);
				}
				hSession.flush();
				tx.commit();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	// this method is to be called everyday
	// takes last ichimoku row and consider it as previous day
	// then from EveryDayCandle take all rows with tradeDate > date in last ichimoku row
	// if there are more than 1 nav that means we missed updating,so throw an exception
	public void updateCurrentIchimoku() throws Exception
	{
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();

			// look for distinct mutual funds from every day candle
			List mfList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle").list();
			Iterator itr = mfList.iterator();
			String missedId = "";
			long mfId = 0;
			Date lastIchimokuDate = null;
			while (itr.hasNext())
			{
				mfId = (long) itr.next();
				List ichimokuList = hSession.createQuery("from Ichimoku where mutualFundId=? and candleType=? and tenkanSen!=0 order by tradeDate desc").setParameter(0, mfId).setParameter(1, 1).setMaxResults(1).list();
				if(ichimokuList.isEmpty())
				{
					missedId = missedId + String.valueOf(mfId);
				}
				Ichimoku ichimoku1 = (Ichimoku) ichimokuList.get(0);
				lastIchimokuDate = ichimoku1.getTradeDate();
				Date currentDate = new Date();

				// System.out.println(lastIchimokuDate);
				List list = hSession.createQuery("from EveryDayCandle where mutualfundId=? and tradeDate>? and candleType=? order by tradeDate asc").setParameter(0, mfId).setParameter(1, lastIchimokuDate).setParameter(2, 1).list();
				// System.out.println("list.size=" + list.size());
				if(list.size() > 1)
				{
					String date = new SimpleDateFormat("dd-MM-yyyy").format(((Nav) list.get(0)).getTradeDate());
					throw new Exception("Ichimoku not updated for more than 1 day!Update using last date as " + lastIchimokuDate);
				}
				EveryDayCandle edc = null;
				if(!list.isEmpty() && list.size() == 1)
				{
					edc = (EveryDayCandle) list.get(0);

					// tx to persist ichimoku with close price first
					tx = hSession.beginTransaction();
					Ichimoku ichimoku = new Ichimoku();
					ichimoku.setMutualFundId(mfId);
					ichimoku.setClose(edc.getTodaysClosePrice());
					ichimoku.setTradeDate(edc.getTradeDate());
					ichimoku.setCandleType(1);
					hSession.saveOrUpdate(ichimoku);
					tx.commit();

					tx = hSession.beginTransaction();
					List<Double> last9DaysList = hSession.createQuery("select close from Ichimoku where mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 1).setMaxResults(9).list();

					double maxFromPrevious9Days = Collections.max(last9DaysList);
					double minFromPrevious9Days = Collections.min(last9DaysList);
					double tenkanSen = (maxFromPrevious9Days + minFromPrevious9Days) / 2.0;
					tenkanSen = ImageLinks.getRoundedValue(tenkanSen, 5);

					ichimoku.setTenkanSen(tenkanSen);

					/*List<Ichimoku> last26DaysIchimokuList = hSession.createQuery("select close from Ichimoku where mutualFundId=? order by tradeDate desc").setParameter(0, mfId).setMaxResults(26).list();
					List<Double> last26DaysList = new ArrayList<Double>();
					Iterator itr3 = last26DaysIchimokuList.iterator();
					while (itr3.hasNext())
					{
						Ichimoku ichimokuForClose = (Ichimoku) itr3.next();
						double close = ichimokuForClose.getClose();
						last26DaysList.add(close);
					}*/
					List<Double> last26DaysList = hSession.createQuery("select close from Ichimoku where mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 1).setMaxResults(26).list();
					double maxFromPrevious26Days = Collections.max(last26DaysList);
					double minFromPrevious26Days = Collections.min(last26DaysList);
					double kijunSen = (maxFromPrevious26Days + minFromPrevious26Days) / 2.0;
					kijunSen = ImageLinks.getRoundedValue(kijunSen, 5);
					ichimoku.setKijunSen(kijunSen);

					List ichimoku26DaysBackList = hSession.createQuery("from Ichimoku where mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 1).setMaxResults(26).list();
					// System.out.println("Ichimoku 26 days back index=" + ichimoku26DaysBackList.size());
					Ichimoku ichimoku26DaysBack = (Ichimoku) ichimoku26DaysBackList.get(25);
					double kijunSen26DaysBefore = ichimoku26DaysBack.getKijunSen();
					double tenkanSen26DaysBefore = ichimoku26DaysBack.getTenkanSen();
					double senkouSpanA = (tenkanSen26DaysBefore + kijunSen26DaysBefore) / 2.0;
					senkouSpanA = ImageLinks.getRoundedValue(senkouSpanA, 5);
					ichimoku.setSenkouSpanA(senkouSpanA);

					// since ssb is avg of previous 52 days and the value is placed after 26 days we use 78DaysList
					List<Double> last78DaysList = hSession.createQuery("select close from Ichimoku where mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 1).setMaxResults(78).list();
					List<Double> first52Of78DaysList = last78DaysList.subList(26, 77);
					double maxFrom52Days = Collections.max(first52Of78DaysList);
					double minFrom52Days = Collections.min(first52Of78DaysList);
					double senkouSpanB = (maxFrom52Days + minFrom52Days) / 2.0;
					senkouSpanB = ImageLinks.getRoundedValue(senkouSpanB, 5);
					ichimoku.setSenkouSpanB(senkouSpanB);


					double kumoTop = senkouSpanA > senkouSpanB ? senkouSpanA : senkouSpanB;
					double kumoBottom = senkouSpanA < senkouSpanB ? senkouSpanA : senkouSpanB;
					ichimoku.setKumoTop(kumoTop);
					ichimoku.setKumoBottom(kumoBottom);
					hSession.save(ichimoku);
				}
				else
				{
					throw new Exception("Ichimoku already updated for latest day in EveryDayCandle");
				}
			}
			// Production Environment
			/*if(new SimpleDateFormat("EEEEE").format(new Date()).equalsIgnoreCase("Friday"))
			{
				updateCurrentWeeklyIchimoku(hSession);
			}*/

			// Development Environment
			// updateCurrentWeeklyIchimoku(hSession);
			tx.commit();
			// System.out.println("hello");
			

			if(!missedId.isEmpty())
			{
				throw new Exception("Ichimoku currently doest not have data for( " + missedId + " )!Please update it first");
			}


		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
			throw new Exception(e.getMessage());
			/*if(e.getMessage().contains("Ichimoku not updated for more than 1 day!Update using last date as "))
			{
				throw new Exception(e.getMessage());
			}
			else if(e.getMessage().equals("Ichimoku already updated for latest day in Nav"))
			{
				throw new Exception(e.getMessage());
			}*/
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateIchimokuAfterDate() throws Exception
	{
		Session hSession = null;
		Transaction tx = null;
		Date lastUpdatedDate=null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();		

			List mfList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle").list();
			Iterator itr = mfList.iterator();
			String missedMutualFundId = "";
			while (itr.hasNext())
			{
				long mfId = (Long) itr.next();
				
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from Ichimoku where mutualFundId=? and candleType=1 and tenkanSen!=0 and kijunSen!=0").setParameter(0, mfId).setMaxResults(1).uniqueResult();
				if(lastUpdatedDate==null)
				{
					lastUpdatedDate=(Date)hSession.createQuery("select min(tradeDate) from EveryDayCandle where mutualFundId=? and candleType=1").setParameter(0, mfId).setMaxResults(1).uniqueResult();
				}
				System.out.println("updating daily ichimoku for="+mfId+" lstUpdatedDate="+lastUpdatedDate);
				List everyDayFromStartDate = hSession.createQuery("from EveryDayCandle where mutualfundId=? and tradeDate>=? and candleType=? order by tradeDate asc").setParameter(0, mfId).setParameter(1, lastUpdatedDate).setParameter(2, 1).list();
				// update ichimokuFromStartDate with closePrice
				Iterator itr1 = everyDayFromStartDate.iterator();
				try
				{
					tx = hSession.beginTransaction();
					while (itr1.hasNext())
					{
						EveryDayCandle edc = (EveryDayCandle) itr1.next();
						Ichimoku ichimoku = new Ichimoku();
						ichimoku.setClose(edc.getTodaysClosePrice());
						ichimoku.setMutualFundId(mfId);
						ichimoku.setTradeDate(edc.getTradeDate());
						ichimoku.setCandleType(1);
						hSession.saveOrUpdate(ichimoku);
					}
					tx.commit();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					if(tx != null)
					{
						tx.rollback();
					}
					throw new Exception("Not able to persist ichimoku close price from start Date");
				}

				List ichimokuFromStartDate = hSession.createQuery("from Ichimoku where mutualFundId=? and tradeDate>=? and candleType=? order by tradeDate asc").setParameter(0, mfId).setParameter(1, lastUpdatedDate).setParameter(2, 1).list();
				tx = hSession.beginTransaction();
				// update tenkanSen
				Iterator itr3 = ichimokuFromStartDate.iterator();
				while (itr3.hasNext())
				{
					Ichimoku ichimoku = (Ichimoku) itr3.next();
					Date ichimokuDate = ichimoku.getTradeDate();
					List last9DaysList = hSession.createQuery("select close from Ichimoku where tradeDate<=? and mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, ichimokuDate).setParameter(1, mfId).setParameter(2, 1).setMaxResults(9).list();
					double maxFrom9Days = (Double) Collections.max(last9DaysList);
					double minFrom9Days = (Double) Collections.min(last9DaysList);
					double tenkanSen = (maxFrom9Days + minFrom9Days) / 2.0;
					tenkanSen = ImageLinks.getRoundedValue(tenkanSen, 5);
					ichimoku.setTenkanSen(tenkanSen);

					List<Ichimoku> last26DaysIchimokuList = hSession.createQuery("from Ichimoku where tradeDate<=? and mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, ichimokuDate).setParameter(1, mfId).setParameter(2, 1).setMaxResults(26).list();
					List<Double> last26DaysList = new ArrayList<Double>();
					Iterator itr2 = last26DaysIchimokuList.iterator();
					while (itr2.hasNext())
					{
						Ichimoku ichimokuForClose = (Ichimoku) itr2.next();
						double close = ichimokuForClose.getClose();
						last26DaysList.add(close);
					}
					double maxFromPrevious26Days = Collections.max(last26DaysList);
					double minFromPrevious26Days = Collections.min(last26DaysList);
					double kijunSen = (maxFromPrevious26Days + minFromPrevious26Days) / 2.0;
					kijunSen = ImageLinks.getRoundedValue(kijunSen, 5);
					ichimoku.setKijunSen(kijunSen);
					double kijunSen26DaysBefore=0;
					double tenkanSen26DaysBefore=0;
					if(last26DaysIchimokuList.size()==26)
					{
						//System.out.println("Date="+ichimokuDate+" last26DaysIchimokuList.size()="+last26DaysIchimokuList.size());
						kijunSen26DaysBefore = last26DaysIchimokuList.get(25).getKijunSen();
						tenkanSen26DaysBefore = last26DaysIchimokuList.get(25).getTenkanSen();
					}
					else
					{
						//System.out.println("Not 26 days"+"Date="+ichimokuDate+" last26DaysIchimokuList.size()="+last26DaysIchimokuList.size());
						kijunSen26DaysBefore=0;
						tenkanSen26DaysBefore=0;
					}
					double senkouSpanA = (tenkanSen26DaysBefore + kijunSen26DaysBefore) / 2.0;
					senkouSpanA = ImageLinks.getRoundedValue(senkouSpanA, 5);
					ichimoku.setSenkouSpanA(senkouSpanA);

					// since ssb is avg of previous 52 days and the value is placed after 26 days
					List<Double> last78DaysList = hSession.createQuery("select close from Ichimoku where mutualFundId=? and tradeDate <= ? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, ichimokuDate).setParameter(2, 1).setMaxResults(77).list();
					// since the last78DaysList is in descending order we use sublist from 26 to 77
					double senkouSpanB=0;
					if(last78DaysList!=null && last78DaysList.size()==77)
					{
						List<Double> first52Of78DaysList = last78DaysList.subList(25, 77);
						double maxFrom52Days = Collections.max(first52Of78DaysList);
						double minFrom52Days = Collections.min(first52Of78DaysList);
						senkouSpanB = (maxFrom52Days + minFrom52Days) / 2.0;
						senkouSpanB = ImageLinks.getRoundedValue(senkouSpanB, 5);
						ichimoku.setSenkouSpanB(senkouSpanB);
					}
					else
					{
						ichimoku.setSenkouSpanB(senkouSpanB);
					}
					double kumoTop = senkouSpanA > senkouSpanB ? senkouSpanA : senkouSpanB;
					double kumoBottom = senkouSpanA < senkouSpanB ? senkouSpanA : senkouSpanB;
					ichimoku.setKumoTop(kumoTop);
					ichimoku.setKumoBottom(kumoBottom);
					hSession.saveOrUpdate(ichimoku);
				}
				tx.commit();
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}

	public int delete(String currentOrAll) throws Exception
	{
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			tx = hSession.beginTransaction();
			if(currentOrAll.equalsIgnoreCase("all"))
			{
				int n = hSession.createQuery("delete from Ichimoku").executeUpdate();
			}
			else if(currentOrAll.equalsIgnoreCase("current"))
			{
				Date currentDate = (Date) hSession.createQuery("select tradeDate from CurrentDayCandle").setMaxResults(1).uniqueResult();
				Date lastIchimokuDate = (Date) hSession.createQuery("select tradeDate from Ichimoku order by tradeDate desc").setMaxResults(1).uniqueResult();
				if(currentDate != null && lastIchimokuDate.compareTo(currentDate) == 0)
				{
					int n = hSession.createQuery("delete from Ichimoku where tradeDate=?").setParameter(0, currentDate).executeUpdate();
				}
				else if(currentDate == null)
				{
					throw new Exception("CurrentDayCandle table is currently empty (deletion is made on the basis of tradeDate in CurrentDayCandle table)");
				}
				else if(lastIchimokuDate.compareTo(currentDate) < 0)
				{
					throw new Exception("Ichimoku table does not contain tradeDate of CurrentDayCandle table.");
				}
				else if(lastIchimokuDate.compareTo(currentDate) > 0)
				{
					throw new Exception("CurrentDayCandle table is not properly updated(Ichimoku table have extra greater dates)");
				}
			}
			tx.commit();
			return 1;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}

	public void deleteIchimokuIndicatorsAfterStartDate(String sDate) throws ParseException
	{
		Session hSession = null;
		Transaction tx = null;
		Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(sDate);
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			tx = hSession.beginTransaction();
			int n = hSession.createQuery("delete from Ichimoku where tradeDate>=?").setParameter(0, startDate).executeUpdate();
			tx.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public void persistWeeklyIchimokuFromEveryDay(Long minM,Long maxM)
	{
		System.out.println("Insert ohlc into weekly Ichimoku");
		Transaction tx = null;
		Session hSession = null;

		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List<Long> mutualFundList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where mutualFundId>=? and mutualFundId<=?").setParameter(0, minM).setParameter(1, maxM).list();
			Iterator itr = mutualFundList.iterator();
			while (itr.hasNext())
			{
				long mfId = (long) itr.next();
				System.out.println("update weekly ichimoku for ohlc for ="+mfId);
				List everyDayList = hSession.createQuery("from EveryDayCandle where mutualFundId=? and candleType=? order by tradeDate").setParameter(0, mfId).setParameter(1, 0).list();
				// persist ichimoku since we have to store ssb value after 26 days from current day
				Ichimoku ichimoku = null;
				Iterator itr1 = everyDayList.iterator();
				EveryDayCandle edc = null;
				tx = hSession.beginTransaction();
				while (itr1.hasNext())
				{
					ichimoku = new Ichimoku();
					edc = (EveryDayCandle) itr1.next();
					ichimoku.setMutualFundId(mfId);
					ichimoku.setOpen(edc.getOpen());
					ichimoku.setClose(edc.getTodaysClosePrice());
					ichimoku.setHigh(edc.getHigh());
					ichimoku.setLow(edc.getLow());
					ichimoku.setTradeDate(edc.getTradeDate());
					ichimoku.setCandleType(0);
					hSession.saveOrUpdate(ichimoku);
				}
				hSession.flush();
				tx.commit();
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateWeeklySSB(Long minM,Long maxM)
	{
		System.out.println("Update weekly SSB");
		Session hSession = null;
		Transaction tx = null;
		try
		{

			hSession = HibernateBridge.getSessionFactory().openSession();
			List<Long> mutualFundList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where mutualFundId>=? and mutualFundId<=?").setParameter(0, minM).setParameter(1, maxM).list();
			Iterator itr = mutualFundList.iterator();
			long mfId = 0;

			while (itr.hasNext())
			{
				mfId = (long) itr.next();
				System.out.println("update weekly SSB for ="+mfId);
				List<Ichimoku> ichimokuList = hSession.createQuery("from Ichimoku where mutualFundId=? and candleType=? order by tradeDate").setParameter(0, mfId).setParameter(1, 0).list();
				// saving ichimoku with ssb values
				Ichimoku ichimoku = null;
				List<Double> previous52DaysHigh = null;
				List<Double> previous52DaysLow = null;
				tx = hSession.beginTransaction();
				for (int i = 0; i < ichimokuList.size(); i++)
				{
					// i<52
					if(i < 51)
					{
						// System.out.println("i=" + i);
						continue;
					}
					// ( ichimokuList.size()-1 ) - 26
					if(i > (ichimokuList.size() - 1) - 25)
					{
						// since ssb is the value of 26 days later from current we have to exit before 26th day from last day stored in our database
						// System.out.print("Break");
						break;
					}
					previous52DaysHigh = new ArrayList<Double>();
					previous52DaysLow = new ArrayList<Double>();
					double max = 0, min = 0, avg;
					// j >= i - 52
					for (int j = i; j >= i - 51; j--)
					{
						// System.out.println("j=" + j);
						previous52DaysHigh.add(ichimokuList.get(j).getHigh());
						previous52DaysLow.add(ichimokuList.get(j).getLow());
					}
					max = Collections.max(previous52DaysHigh);
					min = Collections.min(previous52DaysLow);
					avg = (max + min) / 2.0;
					avg = ImageLinks.getRoundedValue(avg, 5);
					// i+26;
					int indexAfter26Days = i + 25;// we have to consider current day also while calculating indexAfter26Days
					ichimoku = ichimokuList.get(indexAfter26Days);
					ichimoku.setSenkouSpanB(avg);

					hSession.saveOrUpdate(ichimoku);
				}
				hSession.flush();
				tx.commit();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateWeeklyTenkanSen(Long minM,Long maxM)
	{
		System.out.println("update weekly TenkanSen");
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			List<Long> mutualFundList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where mutualFundId>=? and mutualFundId<=?").setParameter(0, minM).setParameter(1, maxM).list();
			Iterator itr = mutualFundList.iterator();
			long mfId = 0;

			while (itr.hasNext())
			{
				mfId = (long) itr.next();
				System.out.println("Update weekly for TenkanSen for ="+mfId);
				List<Ichimoku> ichimokuList = hSession.createQuery("from Ichimoku where mutualFundId=? and candleType=? order by tradeDate").setParameter(0, mfId).setParameter(1, 0).list();

				// saving ichimoku with ssb values
				Ichimoku ichimoku = null;
				List<Double> previous9DaysHigh = null;
				List<Double> previous9DaysLow = null;
				tx = hSession.beginTransaction();
				for (int i = 0; i < ichimokuList.size(); i++)
				{
					// i<9
					if(i < 8)
					{
						// System.out.println("i=" + i);
						continue;
					}
					previous9DaysHigh = new ArrayList<Double>();
					previous9DaysLow = new ArrayList<Double>();
					double max = 0, min = 0, avg;
					for (int j = i; j >= i - 8; j--)
					{
						previous9DaysHigh.add(ichimokuList.get(j).getHigh());
						previous9DaysLow.add(ichimokuList.get(j).getLow());
					}
					max = Collections.max(previous9DaysHigh);
					min = Collections.min(previous9DaysLow);
					avg = (max + min) / 2.0d;
					avg = ImageLinks.getRoundedValue(avg, 5);
					// System.out.println("max=" + max + "		min=" + min + "		avg=" + avg + "		at i=" + i);
					ichimoku = ichimokuList.get(i);
					ichimoku.setTenkanSen(avg);
					// saving ichimoku with ts indicators
					hSession.saveOrUpdate(ichimoku);
				}
				hSession.flush();
				tx.commit();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateWeeklyKijunSen(Long minM,Long maxM)
	{
		System.out.println("Update Weekly KijunSen");
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			List<Long> mutualFundList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where mutualFundId>=? and mutualFundId<=?").setParameter(0, minM).setParameter(1, maxM).list();
			Iterator itr = mutualFundList.iterator();
			long mfId = 0;

			while (itr.hasNext())
			{
				mfId = (long) itr.next();
				System.out.println("Update weekly for KijunSen for ="+mfId);
				List<Ichimoku> ichimokuList = hSession.createQuery("from Ichimoku where mutualFundId=? and candleType=? order by tradeDate").setParameter(0, mfId).setParameter(1, 0).list();

				Ichimoku ichimoku = null;
				List<Double> previous26DaysHigh = null;
				List<Double> previous26DaysLow = null;
				tx = hSession.beginTransaction();
				for (int i = 0; i < ichimokuList.size(); i++)
				{
					// i<26
					if(i < 25)
					{
						// System.out.println("i=" + i);
						continue;
					}

					previous26DaysHigh = new ArrayList<Double>();
					previous26DaysLow = new ArrayList<Double>();
					double max = 0, min = 0, avg;
					// /j = i - 1; j >= i - 26;
					for (int j = i; j >= i - 25; j--)
					{
						previous26DaysHigh.add(ichimokuList.get(j).getHigh());
						previous26DaysLow.add(ichimokuList.get(j).getLow());
					}
					max = Collections.max(previous26DaysHigh);
					min = Collections.min(previous26DaysLow);
					avg = (max + min) / 2.0;
					avg = ImageLinks.getRoundedValue(avg, 5);
					// System.out.println("max=" + max + "		min=" + min + "		avg=" + avg + "		at i=" + i);

					ichimoku = ichimokuList.get(i);
					ichimoku.setKijunSen(avg);
					// saving ichimoku with ks values
					hSession.saveOrUpdate(ichimoku);
				}
				hSession.flush();
				tx.commit();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateWeeklySSA(Long minM,Long maxM)
	{
		System.out.println("update Weekly SSA");
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			List<Long> mutualFundList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where mutualFundId>=? and mutualFundId<=?").setParameter(0, minM).setParameter(1, maxM).list();
			Iterator itr = mutualFundList.iterator();
			long mfId = 0;

			while (itr.hasNext())
			{
				mfId = (long) itr.next();
				System.out.println("Update weeklySSA for ="+mfId);
				List<Ichimoku> ichimokuList = hSession.createQuery("from Ichimoku where mutualFundId=? and candleType=? order by tradeDate").setParameter(0, mfId).setParameter(1, 0).list();

				Ichimoku ichimoku = null;
				double tenkanSen = 0, kijunSen = 0, avg = 0;
				tx = hSession.beginTransaction();
				for (int i = 0; i < ichimokuList.size(); i++)
				{
					if(i > (ichimokuList.size() - 1) - 25)
					{
						// since ssa is the value of 26 days ahead from current we have to exit before 26th day from last day stored in our database
						// System.out.print("Break");
						break;

					}
					ichimoku = ichimokuList.get(i);
					tenkanSen = ichimoku.getTenkanSen();
					kijunSen = ichimoku.getKijunSen();
					avg = (tenkanSen + kijunSen) / 2.0;
					// i+26 (Now i+25 because we have to consider 26 days including 1 day of current day)
					ichimoku = ichimokuList.get(i + 25);
					avg = ImageLinks.getRoundedValue(avg, 5);
					ichimoku.setSenkouSpanA(avg);

					hSession.saveOrUpdate(ichimoku);
				}
				hSession.flush();
				tx.commit();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateWeeklyKumo(Long minM,Long maxM)
	{
		System.out.println("Update Weekly Kumo");
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List<Long> mutualFundList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where mutualFundId>=? and mutualFundId<=?").setParameter(0, minM).setParameter(1, maxM).list();
			Iterator itr = mutualFundList.iterator();
			while (itr.hasNext())
			{
				long mfId = (Long) itr.next();
				System.out.println("Update weekly kumo for ="+mfId);
				List<Ichimoku> ichimokuList = hSession.createQuery("from Ichimoku where mutualFundId=? and candleType=? order by tradeDate").setParameter(0, mfId).setParameter(1, 0).list();
				Iterator<Ichimoku> itr2 = ichimokuList.iterator();
				Ichimoku ichimoku = null;
				double kumoTop = 0, kumoBottom = 0;
				tx = hSession.beginTransaction();
				while (itr2.hasNext())
				{
					ichimoku = itr2.next();
					kumoTop = ichimoku.getSenkouSpanA() > ichimoku.getSenkouSpanB() ? ichimoku.getSenkouSpanA() : ichimoku.getSenkouSpanB();
					kumoBottom = ichimoku.getSenkouSpanA() < ichimoku.getSenkouSpanB() ? ichimoku.getSenkouSpanA() : ichimoku.getSenkouSpanB();
					ichimoku.setKumoTop(kumoTop);
					ichimoku.setKumoBottom(kumoBottom);
					hSession.saveOrUpdate(ichimoku);
				}
				hSession.flush();
				tx.commit();
			}			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateCurrentWeeklyIchimoku(Session hSession) throws Exception
	{
		Transaction tx = null;
		try
		{
			// look for distinct mutual funds from every day candle
			List mfList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle").list();
			Iterator itr = mfList.iterator();
			String missedId = "";
			while (itr.hasNext())
			{
				long mfId = (long) itr.next();
				List ichimokuList = hSession.createQuery("from Ichimoku where mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 0).setMaxResults(1).list();
				if(ichimokuList.isEmpty())
				{
					missedId = missedId + String.valueOf(mfId);
				}
				if(ichimokuList.size() == 0)
				{
					throw new Exception("old weekly data for ichimoku is not present so update it first.");
				}
				Ichimoku ichimoku1 = (Ichimoku) ichimokuList.get(0);
				Date lastIchimokuDate = ichimoku1.getTradeDate();
				Date currentDate = new Date();

				// System.out.println(lastIchimokuDate);
				List list = hSession.createQuery("from EveryDayCandle where mutualfundId=? and tradeDate >? and candleType=? order by tradeDate asc").setParameter(0, mfId).setParameter(1, lastIchimokuDate).setParameter(2, 0).list();
				if(list.size() > 1)
				{
					String date = new SimpleDateFormat("dd-MM-yyyy").format(((Nav) list.get(0)).getTradeDate());
					throw new Exception("Ichimoku not updated for more than 1 day!Update using last date as " + lastIchimokuDate);
				}
				EveryDayCandle edc = null;
				if(!list.isEmpty() && list.size() == 1)
				{
					edc = (EveryDayCandle) list.get(0);

					// tx to persist ichimoku with close price first
					tx = hSession.beginTransaction();
					Ichimoku ichimoku = new Ichimoku();
					ichimoku.setMutualFundId(mfId);
					ichimoku.setOpen(edc.getOpen());
					ichimoku.setClose(edc.getTodaysClosePrice());
					ichimoku.setHigh(edc.getHigh());
					ichimoku.setLow(edc.getLow());
					ichimoku.setTradeDate(edc.getTradeDate());
					ichimoku.setCandleType(0);
					hSession.saveOrUpdate(ichimoku);
					tx.commit();

					tx = hSession.beginTransaction();
					List<Double> last9DaysHighList = hSession.createQuery("select high from Ichimoku where mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 0).setMaxResults(9).list();
					List<Double> last9DaysLowList = hSession.createQuery("select low from Ichimoku where mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 0).setMaxResults(9).list();
					double maxFromPrevious9Days = Collections.max(last9DaysHighList);
					double minFromPrevious9Days = Collections.min(last9DaysLowList);
					double tenkanSen = (maxFromPrevious9Days + minFromPrevious9Days) / 2.0;
					tenkanSen = ImageLinks.getRoundedValue(tenkanSen, 5);

					ichimoku.setTenkanSen(tenkanSen);

					/*List<Ichimoku> last26DaysIchimokuList = hSession.createQuery("select close from Ichimoku where mutualFundId=? order by tradeDate desc").setParameter(0, mfId).setMaxResults(26).list();
					List<Double> last26DaysList = new ArrayList<Double>();
					Iterator itr3 = last26DaysIchimokuList.iterator();
					while (itr3.hasNext())
					{
						Ichimoku ichimokuForClose = (Ichimoku) itr3.next();
						double close = ichimokuForClose.getClose();
						last26DaysList.add(close);
					}*/
					List<Double> last26DaysHighList = hSession.createQuery("select high from Ichimoku where mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 0).setMaxResults(26).list();
					List<Double> last26DaysLowList = hSession.createQuery("select low from Ichimoku where mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 0).setMaxResults(26).list();
					double maxFromPrevious26Days = Collections.max(last26DaysHighList);
					double minFromPrevious26Days = Collections.min(last26DaysLowList);
					double kijunSen = (maxFromPrevious26Days + minFromPrevious26Days) / 2.0;
					kijunSen = ImageLinks.getRoundedValue(kijunSen, 5);
					ichimoku.setKijunSen(kijunSen);

					List ichimoku26DaysBackList = hSession.createQuery("from Ichimoku where mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 0).setMaxResults(26).list();
					// System.out.println("Ichimoku 26 days back index=" + ichimoku26DaysBackList.size());
					Ichimoku ichimoku26DaysBack = (Ichimoku) ichimoku26DaysBackList.get(25);
					double kijunSen26DaysBefore = ichimoku26DaysBack.getKijunSen();
					double tenkanSen26DaysBefore = ichimoku26DaysBack.getTenkanSen();
					double senkouSpanA = (tenkanSen26DaysBefore + kijunSen26DaysBefore) / 2.0;
					senkouSpanA = ImageLinks.getRoundedValue(senkouSpanA, 5);
					ichimoku.setSenkouSpanA(senkouSpanA);

					// since ssb is avg of previous 52 days and the value is placed after 26 days we use 78DaysList
					List<Double> last78HighDaysList = hSession.createQuery("select high from Ichimoku where mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 0).setMaxResults(78).list();
					List<Double> last78LowDaysList = hSession.createQuery("select low from Ichimoku where mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, 0).setMaxResults(78).list();
					List<Double> first52Of78DaysHighList = last78HighDaysList.subList(26, 77);
					List<Double> first52Of78DaysLowList = last78LowDaysList.subList(26, 77);
					double maxFrom52Days = Collections.max(first52Of78DaysHighList);
					double minFrom52Days = Collections.min(first52Of78DaysLowList);
					double senkouSpanB = (maxFrom52Days + minFrom52Days) / 2.0;
					senkouSpanB = ImageLinks.getRoundedValue(senkouSpanB, 5);
					ichimoku.setSenkouSpanB(senkouSpanB);


					double kumoTop = senkouSpanA > senkouSpanB ? senkouSpanA : senkouSpanB;
					double kumoBottom = senkouSpanA < senkouSpanB ? senkouSpanA : senkouSpanB;
					ichimoku.setKumoTop(kumoTop);
					ichimoku.setKumoBottom(kumoBottom);


					hSession.save(ichimoku);
				}
				else
				{
					throw new Exception("Ichimoku already updated for latest day in EveryDayCandle");
				}
			}
			tx.commit();
			if(!missedId.isEmpty())
			{
				throw new Exception("(While updating current weekly data)Ichimoku is currently empty for( " + missedId + " )!Please update it first");
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
			throw new Exception(e.getMessage());
			/*if(e.getMessage().contains("Ichimoku not updated for more than 1 day!Update using last date as "))
			{
				throw new Exception(e.getMessage());
			}
			else if(e.getMessage().equals("Ichimoku already updated for latest day in Nav"))
			{
				throw new Exception(e.getMessage());
			}*/
		}


	}

	public void updateWeeklyIchimokuAfterDate() throws Exception
	{
		System.out.println("updating Weekly Ichimoku Stocks");
		Session hSession = null;
		Transaction tx = null;
		Date lastUpdatedDate=null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			List mfList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle").list();
			Iterator itr = mfList.iterator();
			String missedMutualFundId = "";
			while (itr.hasNext())
			{
				long mfId = (Long) itr.next();
				System.out.println("updating weekly ichimoku for="+mfId);
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from Ichimoku where mutualFundId=? and candleType=0 and tenkanSen!=0 and kijunSen!=0").setParameter(0, mfId).setMaxResults(1).uniqueResult();
				if(lastUpdatedDate==null)
				{
					lastUpdatedDate=(Date)hSession.createQuery("select min(tradeDate) from EveryDayCandle where mutualFundId=? and candleType=0").setParameter(0, mfId).setMaxResults(1).uniqueResult();
				}

				List everyDayFromStartDate = hSession.createQuery("from EveryDayCandle where mutualfundId=? and tradeDate>=? and candleType=? order by tradeDate asc").setParameter(0, mfId).setParameter(1, lastUpdatedDate).setParameter(2, 0).list();

				// update ichimokuFromStartDate with closePrice
				Iterator itr1 = everyDayFromStartDate.iterator();
				try
				{
					tx = hSession.beginTransaction();
					while (itr1.hasNext())
					{
						EveryDayCandle edc = (EveryDayCandle) itr1.next();
						Ichimoku ichimoku = new Ichimoku();
						ichimoku.setOpen(edc.getOpen());
						ichimoku.setClose(edc.getTodaysClosePrice());
						ichimoku.setHigh(edc.getHigh());
						ichimoku.setLow(edc.getLow());
						ichimoku.setMutualFundId(mfId);
						ichimoku.setTradeDate(edc.getTradeDate());
						ichimoku.setCandleType(0);
						hSession.saveOrUpdate(ichimoku);
					}
					tx.commit();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					if(tx != null)
					{
						tx.rollback();
					}
					throw new Exception("Not able to persist ichimoku close price from start Date");
				}

				List ichimokuFromStartDate = hSession.createQuery("from Ichimoku where mutualFundId=? and tradeDate>=? and candleType=0 order by tradeDate asc").setParameter(0, mfId).setParameter(1, lastUpdatedDate).list();
				
				tx = hSession.beginTransaction();
				// update tenkanSen
				Iterator itr3 = ichimokuFromStartDate.iterator();
				while (itr3.hasNext())
				{
					Ichimoku ichimoku = (Ichimoku) itr3.next();
					Date ichimokuDate = ichimoku.getTradeDate();
					// System.out.println(ichimokuDate);
					List last9HighDaysList = hSession.createQuery("select high from Ichimoku where tradeDate<=? and mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, ichimokuDate).setParameter(1, mfId).setParameter(2, 0).setMaxResults(9).list();
					List last9LowDaysList = hSession.createQuery("select low from Ichimoku where tradeDate<=? and mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, ichimokuDate).setParameter(1, mfId).setParameter(2, 0).setMaxResults(9).list();
					double maxFrom9HighDays = (Double) Collections.max(last9HighDaysList);
					double minFrom9LowDays = (Double) Collections.min(last9LowDaysList);
					double tenkanSen = (maxFrom9HighDays + minFrom9LowDays) / 2.0;
					tenkanSen = ImageLinks.getRoundedValue(tenkanSen, 5);
					ichimoku.setTenkanSen(tenkanSen);

					List<Ichimoku> last26DaysIchimokuList = hSession.createQuery("from Ichimoku where tradeDate<=? and mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, ichimokuDate).setParameter(1, mfId).setParameter(2, 0).setMaxResults(26).list();
					List<Double> last26HighDaysList = new ArrayList<Double>();
					List<Double> last26LowDaysList = new ArrayList<Double>();
					Iterator itr2 = last26DaysIchimokuList.iterator();
					while (itr2.hasNext())
					{
						Ichimoku ichimokuForHighLow = (Ichimoku) itr2.next();
						double high = ichimokuForHighLow.getHigh();
						double low = ichimokuForHighLow.getLow();
						last26HighDaysList.add(high);
						last26LowDaysList.add(low);
					}
					double maxFromPrevious26Days = Collections.max(last26HighDaysList);
					double minFromPrevious26Days = Collections.min(last26LowDaysList);
					double kijunSen = (maxFromPrevious26Days + minFromPrevious26Days) / 2.0;
					kijunSen = ImageLinks.getRoundedValue(kijunSen, 5);
					ichimoku.setKijunSen(kijunSen);

					double tenkanSen26DaysBefore=0;
					double kijunSen26DaysBefore=0;
					if(last26DaysIchimokuList.size()==26)
					{
						tenkanSen26DaysBefore = last26DaysIchimokuList.get(25).getTenkanSen();
						kijunSen26DaysBefore = last26DaysIchimokuList.get(25).getKijunSen();
					}
					else
					{
						tenkanSen26DaysBefore=0;
						kijunSen26DaysBefore=0;
					}
					double senkouSpanA = (tenkanSen26DaysBefore + kijunSen26DaysBefore) / 2.0;
					senkouSpanA = ImageLinks.getRoundedValue(senkouSpanA, 5);
					ichimoku.setSenkouSpanA(senkouSpanA);

					// since ssb is avg of previous 52 days and the value is placed after 26 days
					List<Double> last78HighDaysList = hSession.createQuery("select high from Ichimoku where mutualFundId=? and tradeDate <= ? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, ichimokuDate).setParameter(2, 0).setMaxResults(77).list();
					List<Double> last78LowDaysList = hSession.createQuery("select low from Ichimoku where mutualFundId=? and tradeDate <= ? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, ichimokuDate).setParameter(2, 0).setMaxResults(77).list();
					// since the last78DaysList is in descending order we use sublist from 25(inclusive) to 77(exclusive)
					double senkouSpanB=0;
					if(last78HighDaysList!=null && last78HighDaysList.size()==77)
					{
						List<Double> first52Of78HighDaysList = last78HighDaysList.subList(25, 77);
						List<Double> first52Of78LowDaysList = last78LowDaysList.subList(25, 77);
						double maxFrom52Days = Collections.max(first52Of78HighDaysList);
						double minFrom52Days = Collections.min(first52Of78LowDaysList);
						senkouSpanB = (maxFrom52Days + minFrom52Days) / 2.0;
						senkouSpanB = ImageLinks.getRoundedValue(senkouSpanB, 5);
						ichimoku.setSenkouSpanB(senkouSpanB);
					}
					else
					{
						ichimoku.setSenkouSpanB(senkouSpanB);
					}
					double kumoTop = senkouSpanA > senkouSpanB ? senkouSpanA : senkouSpanB;
					double kumoBottom = senkouSpanA < senkouSpanB ? senkouSpanA : senkouSpanB;
					ichimoku.setKumoTop(kumoTop);
					ichimoku.setKumoBottom(kumoBottom);
					hSession.saveOrUpdate(ichimoku);
					
				}
				tx.commit();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}

	}
}
