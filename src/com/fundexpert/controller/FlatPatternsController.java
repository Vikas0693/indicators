package com.fundexpert.controller;


import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.FlatPatterns;
import com.fundexpert.dao.Ichimoku;
import com.fundexpert.dao.Fractal;

public class FlatPatternsController {

	
	public void createSSBFlatPatterns(String flatdays,String cType) throws Exception
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			int flatDays=Integer.parseInt(flatdays);
			int candleType=Integer.parseInt(cType);
			if(candleType!=1 && candleType!=0)
			{
				throw new Exception("Invalid candleType");
			}
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Long> mfList=hSession.createQuery("select distinct mutualFundId from EveryDayCandle").list();
			Iterator itr=mfList.iterator();
			while(itr.hasNext())
			{
				tx=hSession.beginTransaction();
				Long mutualFundId=(Long)itr.next();
				List<Ichimoku> ichimokuList=hSession.createQuery("from Ichimoku where mutualFundId=? and candleType=? and senkouSpanB!=0 order by tradeDate").setParameter(0, mutualFundId).setParameter(1,candleType).list();
				
				for(int i=0;i<ichimokuList.size();i++)
				{
					Ichimoku ichimoku=ichimokuList.get(i);
					double senkouSpanB=ichimoku.getSenkouSpanB();
					Date maxTradeDate=null;
					int count=1;
					int j=i+1;
					if(j>ichimokuList.size()-1)
						break;
					Ichimoku nextIchimoku=ichimokuList.get(j);
					double nextSenkouSpanB=nextIchimoku.getSenkouSpanB();
					
					while(senkouSpanB==nextSenkouSpanB)
					{
						count++;
						j++;
						if(j>ichimokuList.size()-1)
							break;
						i=j-1;
						
						//System.out.println("i="+i+"\tj="+j+"\tichimokuSize="+ichimokuList.size());
						nextIchimoku=ichimokuList.get(j);
						//System.out.println(ichimoku.getTradeDate()+"\t"+nextIchimoku.getTradeDate()+"\t"+nextIchimoku.getSenkouSpanB());
						nextSenkouSpanB=nextIchimoku.getSenkouSpanB();
						
					}
					if(count>=flatDays)
					{
						Ichimoku lastIchimoku=ichimokuList.get(i);
						maxTradeDate=lastIchimoku.getTradeDate();
						System.out.println("SSB"+candleType+"\tMutalFundId="+mutualFundId+"\tFlat found startDate="+ichimoku.getTradeDate()+"  endDate="+maxTradeDate+"  count="+count);
						FlatPatterns flatPatterns=new FlatPatterns();
						flatPatterns.setMutualFundId(mutualFundId);
						flatPatterns.setCandleType(candleType);
						flatPatterns.setPatternName("SSB");
						flatPatterns.setPatternValue(ichimoku.getSenkouSpanB());
						flatPatterns.setFlatDays(flatDays);
						flatPatterns.setPatternStartDate(ichimoku.getTradeDate());
						flatPatterns.setPatternEndDate(maxTradeDate);
						flatPatterns.setNumberOfDays(count);
						hSession.saveOrUpdate(flatPatterns);
						
					}
					
				}
				tx.commit();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}
	//remember to use period(column if fractal) parameter  in query since we generate fractal for period=1,2 but we use tcfl for period=2
	public void createTCFL1FlatPatterns(String flatdays,String cType)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			int flatDays=Integer.parseInt(flatdays);
			int candleType=Integer.parseInt(cType);
			if(candleType!=1 && candleType!=0)
			{
				throw new Exception("Invalid candleType");
			}
			
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Long> mfList=hSession.createQuery("select distinct mutualFundId from EveryDayCandle").list();
			Iterator itr=mfList.iterator();
			while(itr.hasNext())
			{
				tx=hSession.beginTransaction();
				Long mutualFundId=(Long)itr.next();
				List<Fractal> fractalList=hSession.createQuery("from Fractal where mutualFundId=? and candleType=? and tcfl1!=0 and period =? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,candleType).setParameter(2,2).list();
				
				for(int i=0;i<fractalList.size();i++)
				{
					Fractal fractal=fractalList.get(i);
					double tcfl1=fractal.getTcfl1();
					Date maxTradeDate=null;
					int count=1;
					int j=i+1;
					if(j>fractalList.size()-1)
						break;
					Fractal nextFractal=fractalList.get(j);
					double nextTcfl1=nextFractal.getTcfl1();
					
					while(tcfl1==nextTcfl1)
					{
						count++;
						j++;
						if(j>fractalList.size()-1)
							break;
						i=j-1;
						
						//System.out.println("i="+i+"\tj="+j+"\tfractalSize="+fractalList.size());
						nextFractal=fractalList.get(j);
						nextTcfl1=nextFractal.getTcfl1();
						
					}
					if(count>=flatDays)
					{
						Fractal lastFractal=fractalList.get(i);
						maxTradeDate=lastFractal.getTradeDate();
						System.out.println("TCFL1"+candleType+"\tMutalFundId="+mutualFundId+"\tFlat found startDate="+fractal.getTradeDate()+"  endDate="+maxTradeDate+"  count="+count);
						FlatPatterns flatPatterns=new FlatPatterns();
						flatPatterns.setMutualFundId(mutualFundId);
						flatPatterns.setCandleType(candleType);
						flatPatterns.setPatternName("TCFL1");
						flatPatterns.setPatternValue(fractal.getTcfl1());
						flatPatterns.setFlatDays(flatDays);
						flatPatterns.setPatternStartDate(fractal.getTradeDate());
						flatPatterns.setPatternEndDate(maxTradeDate);
						flatPatterns.setNumberOfDays(count);
						hSession.saveOrUpdate(flatPatterns);
						
					}
					
				}
				tx.commit();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}
	
	public void createTCFL3FlatPatterns(String flatdays,String cType)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			int flatDays=Integer.parseInt(flatdays);
			int candleType=Integer.parseInt(cType);
			if(candleType!=1 && candleType!=0)
			{
				throw new Exception("Invalid candleType");
			}
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Long> mfList=hSession.createQuery("select distinct mutualFundId from EveryDayCandle").list();
			Iterator itr=mfList.iterator();
			while(itr.hasNext())
			{
				tx=hSession.beginTransaction();
				Long mutualFundId=(Long)itr.next();
				List<Fractal> fractalList=hSession.createQuery("from Fractal where mutualFundId=? and candleType=? and tcfl3!=0 and period=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,candleType).setParameter(2,2).list();
				
				for(int i=0;i<fractalList.size();i++)
				{
					Fractal fractal=fractalList.get(i);
					double tcfl3=fractal.getTcfl3();
					Date maxTradeDate=null;
					int count=1;
					int j=i+1;
					if(j>fractalList.size()-1)
						break;
					Fractal nextFractal=fractalList.get(j);
					double nextTcfl3=nextFractal.getTcfl3();
					
					while(tcfl3==nextTcfl3)
					{
						count++;
						j++;
						if(j>fractalList.size()-1)
							break;
						i=j-1;
						
						//System.out.println("i="+i+"\tj="+j+"\tfractalSize="+fractalList.size());
						nextFractal=fractalList.get(j);
						nextTcfl3=nextFractal.getTcfl3();
						
					}
					if(count>=flatDays)
					{
						Fractal lastFractal=fractalList.get(i);
						maxTradeDate=lastFractal.getTradeDate();
						System.out.println("TCFL3"+candleType+"\tMutalFundId="+mutualFundId+"\tFlat found startDate="+fractal.getTradeDate()+"  endDate="+maxTradeDate+"  count="+count);
						FlatPatterns flatPatterns=new FlatPatterns();
						flatPatterns.setMutualFundId(mutualFundId);
						flatPatterns.setCandleType(candleType);
						flatPatterns.setPatternName("TCFL3");
						flatPatterns.setPatternValue(fractal.getTcfl3());
						flatPatterns.setFlatDays(flatDays);
						flatPatterns.setPatternStartDate(fractal.getTradeDate());
						flatPatterns.setPatternEndDate(maxTradeDate);
						flatPatterns.setNumberOfDays(count);
						hSession.saveOrUpdate(flatPatterns);
						
					}
					
				}
				tx.commit();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}
	
	public void createLatestSSBFlatPatterns(String flatdays,String cType) throws Exception
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			int flatDays=Integer.parseInt(flatdays);
			int candleType=Integer.parseInt(cType);
			if(candleType!=1 && candleType!=0)
			{
				throw new Exception("Invalid candleType");
			}
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Long> mfList=hSession.createQuery("select distinct mutualFundId from Ichimoku").list();
			Iterator itr=mfList.iterator();
			while(itr.hasNext())
			{
				tx=hSession.beginTransaction();
				Long mutualFundId=(Long)itr.next();
				System.out.println("Flat pattern SSB for mfId="+mutualFundId);
				Date maxEndDate=(Date)hSession.createQuery("select max(patternEndDate) from FlatPatterns where mutualFundId=? and patternName like '%SSB%' and candleType=?").setParameter(0,mutualFundId).setParameter(1, candleType).setMaxResults(1).uniqueResult();
				if(maxEndDate==null)
				{
					maxEndDate=(Date)hSession.createQuery("select min(tradeDate) from Ichimoku where mutualFundId=? and candleType=?").setParameter(0,mutualFundId).setParameter(1, candleType).setMaxResults(1).uniqueResult();
				}
				List<Ichimoku> ichimokuList=hSession.createQuery("from Ichimoku where mutualFundId=? and candleType=? and senkouSpanB!=0 and tradeDate>? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,candleType).setParameter(2, maxEndDate).list();
				
				for(int i=0;i<ichimokuList.size();i++)
				{
					Ichimoku ichimoku=ichimokuList.get(i);
					double senkouSpanB=ichimoku.getSenkouSpanB();
					Date maxTradeDate=null;
					int count=1;
					int j=i+1;
					if(j>ichimokuList.size()-1)
						break;
					Ichimoku nextIchimoku=ichimokuList.get(j);
					double nextSenkouSpanB=nextIchimoku.getSenkouSpanB();
					
					while(senkouSpanB==nextSenkouSpanB)
					{
						count++;
						j++;
						if(j>ichimokuList.size()-1)
							break;
						i=j-1;
						
						//System.out.println("i="+i+"\tj="+j+"\tichimokuSize="+ichimokuList.size());
						nextIchimoku=ichimokuList.get(j);
						//System.out.println(ichimoku.getTradeDate()+"\t"+nextIchimoku.getTradeDate()+"\t"+nextIchimoku.getSenkouSpanB());
						nextSenkouSpanB=nextIchimoku.getSenkouSpanB();
						
					}
					if(count>=flatDays)
					{
						Ichimoku lastIchimoku=ichimokuList.get(i);
						maxTradeDate=lastIchimoku.getTradeDate();
						System.out.println("SSB"+candleType+"\tMutalFundId="+mutualFundId+"\tFlat found startDate="+ichimoku.getTradeDate()+"  endDate="+maxTradeDate+"  count="+count);
						FlatPatterns flatPatterns=new FlatPatterns();
						flatPatterns.setMutualFundId(mutualFundId);
						flatPatterns.setCandleType(candleType);
						flatPatterns.setPatternName("SSB");
						flatPatterns.setPatternValue(ichimoku.getSenkouSpanB());
						flatPatterns.setFlatDays(flatDays);
						flatPatterns.setPatternStartDate(ichimoku.getTradeDate());
						flatPatterns.setPatternEndDate(maxTradeDate);
						flatPatterns.setNumberOfDays(count);
						hSession.saveOrUpdate(flatPatterns);
						
					}
					
				}
				tx.commit();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}
	//remember to use period(column if fractal) parameter  in query since we generate fractal for period=1,2 but we use tcfl for period=2(or generate tcfl1,2,3 only for period 2)
	public void createLatestTCFL1FlatPatterns(String flatdays,String cType)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			int flatDays=Integer.parseInt(flatdays);
			int candleType=Integer.parseInt(cType);
			if(candleType!=1 && candleType!=0)
			{
				throw new Exception("Invalid candleType");
			}
			
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Long> mfList=hSession.createQuery("select distinct mutualFundId from Fractal").list();
			Iterator itr=mfList.iterator();
			while(itr.hasNext())
			{
				tx=hSession.beginTransaction();
				Long mutualFundId=(Long)itr.next();
				Date maxEndDate=(Date)hSession.createQuery("select max(patternEndDate) from FlatPatterns where mutualFundId=? and patternName like '%TCFL1%' and candleType=?").setParameter(0,mutualFundId).setParameter(1, candleType).setMaxResults(1).uniqueResult();
				if(maxEndDate==null)
				{
					maxEndDate=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=?").setParameter(0, mutualFundId).setParameter(1, candleType).setMaxResults(1).uniqueResult();
				}
				List<Fractal> fractalList=hSession.createQuery("from Fractal where mutualFundId=? and candleType=? and tcfl1!=0 and period =? and tradeDate>? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,candleType).setParameter(2,2).setParameter(3, maxEndDate).list();
				
				for(int i=0;i<fractalList.size();i++)
				{
					Fractal fractal=fractalList.get(i);
					double tcfl1=fractal.getTcfl1();
					Date maxTradeDate=null;
					int count=1;
					int j=i+1;
					if(j>fractalList.size()-1)
						break;
					Fractal nextFractal=fractalList.get(j);
					double nextTcfl1=nextFractal.getTcfl1();
					
					while(tcfl1==nextTcfl1)
					{
						count++;
						j++;
						if(j>fractalList.size()-1)
							break;
						i=j-1;
						
						//System.out.println("i="+i+"\tj="+j+"\tfractalSize="+fractalList.size());
						nextFractal=fractalList.get(j);
						nextTcfl1=nextFractal.getTcfl1();
						
					}
					if(count>=flatDays)
					{
						Fractal lastFractal=fractalList.get(i);
						maxTradeDate=lastFractal.getTradeDate();
						System.out.println("TCFL1 "+candleType+"\tMutalFundId="+mutualFundId+"\tFlat found startDate="+fractal.getTradeDate()+"  endDate="+maxTradeDate+"  count="+count);
						FlatPatterns flatPatterns=new FlatPatterns();
						flatPatterns.setMutualFundId(mutualFundId);
						flatPatterns.setCandleType(candleType);
						flatPatterns.setPatternName("TCFL1");
						flatPatterns.setPatternValue(fractal.getTcfl1());
						flatPatterns.setFlatDays(flatDays);
						flatPatterns.setPatternStartDate(fractal.getTradeDate());
						flatPatterns.setPatternEndDate(maxTradeDate);
						flatPatterns.setNumberOfDays(count);
						hSession.saveOrUpdate(flatPatterns);
						
					}
					
				}
				tx.commit();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}
	
	public void createLatestTCFL3FlatPatterns(String flatdays,String cType)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			int flatDays=Integer.parseInt(flatdays);
			int candleType=Integer.parseInt(cType);
			if(candleType!=1 && candleType!=0)
			{
				throw new Exception("Invalid candleType");
			}
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Long> mfList=hSession.createQuery("select distinct mutualFundId from EveryDayCandle").list();
			Iterator itr=mfList.iterator();
			while(itr.hasNext())
			{
				tx=hSession.beginTransaction();
				Long mutualFundId=(Long)itr.next();
				System.out.println("Flat pattern TCFL3 for mfId="+mutualFundId);
				Date maxEndDate=(Date)hSession.createQuery("select max(patternEndDate) from FlatPatterns where mutualFundId=? and patternName like '%TCFL3%' and candleType=?").setParameter(0,mutualFundId).setParameter(1, candleType).setMaxResults(1).uniqueResult();
				if(maxEndDate==null)
				{
					maxEndDate=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=?").setParameter(0, mutualFundId).setParameter(1, candleType).setMaxResults(1).uniqueResult();
				}
				List<Fractal> fractalList=hSession.createQuery("from Fractal where mutualFundId=? and candleType=? and tcfl3!=0 and period=? and tradeDate>? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,candleType).setParameter(2,2).setParameter(3,maxEndDate).list();
				
				for(int i=0;i<fractalList.size();i++)
				{
					Fractal fractal=fractalList.get(i);
					double tcfl3=fractal.getTcfl3();
					Date maxTradeDate=null;
					int count=1;
					int j=i+1;
					if(j>fractalList.size()-1)
						break;
					Fractal nextFractal=fractalList.get(j);
					double nextTcfl3=nextFractal.getTcfl3();
					
					while(tcfl3==nextTcfl3)
					{
						count++;
						j++;
						if(j>fractalList.size()-1)
							break;
						i=j-1;
						
						//System.out.println("i="+i+"\tj="+j+"\tfractalSize="+fractalList.size());
						nextFractal=fractalList.get(j);
						nextTcfl3=nextFractal.getTcfl3();
						
					}
					if(count>=flatDays)
					{
						Fractal lastFractal=fractalList.get(i);
						maxTradeDate=lastFractal.getTradeDate();
						System.out.println("TCFL3 "+candleType+"\tMutalFundId="+mutualFundId+"\tFlat found startDate="+fractal.getTradeDate()+"  endDate="+maxTradeDate+"  count="+count);
						FlatPatterns flatPatterns=new FlatPatterns();
						flatPatterns.setMutualFundId(mutualFundId);
						flatPatterns.setCandleType(candleType);
						flatPatterns.setPatternName("TCFL3");
						flatPatterns.setPatternValue(fractal.getTcfl3());
						flatPatterns.setFlatDays(flatDays);
						flatPatterns.setPatternStartDate(fractal.getTradeDate());
						flatPatterns.setPatternEndDate(maxTradeDate);
						flatPatterns.setNumberOfDays(count);
						hSession.saveOrUpdate(flatPatterns);
						
					}
					
				}
				tx.commit();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}

}
