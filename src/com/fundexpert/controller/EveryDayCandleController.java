package com.fundexpert.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.EveryDayCandle;
import com.fundexpert.dao.Holidays;
import com.fundexpert.dao.Nav;

public class EveryDayCandleController
{
	//updateCandles is used to update historical data and should not be called unless you want to start from scratch
	//delete last weekly candle for every mutualFund as explained below
	public void updateCandles() throws Exception
	{
		System.out.println("You want to update the data for candles from scratch,  contact tech team");
		throw new Exception("You want to update the data for candles from scratch,  contact tech team");
		/*Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			//tx = hSession.beginTransaction();
			EveryDayCandle everyDayCandle = new EveryDayCandle();
			// List list = hSession.createQuery("from Nav").list();
			List mfList = hSession.createQuery("select distinct mutualfundId from Nav where mutualfundId in (select id from MutualFund where category=? and optionType=? order by subcategory) order by mutualfundId").setParameter(0, 1).setParameter(1,1).list();

			//Iterator itr1 = list.iterator();
			Iterator itr2 = mfList.iterator();
			//System.out.println("mfList.size=" + mfList.size());
			int sumOfSize=0;
			boolean change=true;
			Date fridayDate=new Date();
			Date currentDate=null;
			Nav currentNav=null;
			Date start=null;
			Calendar cal=null;
			long mfId=0;
			List<Nav> week=null;
			List<Nav> newList=null;
			while (itr2.hasNext())
			{
				mfId = (long) itr2.next();
				System.out.println(mfId);
				newList = hSession.createQuery("from Nav where mutualfundId=? order by tradeDate asc").setParameter(0, mfId).list();

				week=new ArrayList<Nav>();
				
				change=true;
				//this initialization is to pass the first if condition inside for loop
				fridayDate=new Date();
				currentDate=null;
				currentNav=null;
				start=null;
				cal=null;
				for(int i=0;i <newList.size(); i++)
				{	
					currentNav=newList.get(i);
					
					currentDate=currentNav.getTradeDate();
					System.out.println("   "+new SimpleDateFormat("dd-MM-yyyy").format(currentDate));
					if(currentDate.compareTo(fridayDate)>0)
					{
						storeCandle(week,hSession);
						change=true;
						week=new ArrayList<Nav>();
					}					
					if(change==true)
					{
						start=newList.get(i).getTradeDate();
						cal=Calendar.getInstance();
						cal.setTime(start);
						int currentDay=cal.get(Calendar.DAY_OF_WEEK);
						int fridayDay=Calendar.FRIDAY;
						int leftDays=fridayDay-currentDay;
						cal.add(Calendar.DATE,leftDays);
						fridayDate=cal.getTime();
					}					
					if(currentDate.compareTo(fridayDate)<=0)
					{
						week.add(currentNav);
						change=false;
					}						
				}
				if(week.size()!=0)
				{
					
					storeCandle(week,hSession);
				}
			}
			//since made changes in storeCandle function regarding holidays,then update historical data if needed considering that table
			System.out.println("Candles Daily/Weekly Updated for everyDayCandle");		
		}
		catch (Exception e)
		{
			e.printStackTrace();			
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}*/
	}

	int getIncrement(Date date)
	{
		//System.out.println("(Date)getIncrement=" + date);
		int i = 0;
		switch (new SimpleDateFormat("EEEEE").format(date))
		{
			// assuming database has no date for saturday or sunday
			case "Monday":
				i = 4;
				break;
			case "Tuesday":
				i = 3;
				break;
			case "Wednesday":
				i = 2;
				break;
			case "Thursday":
				i = 1;
				break;
			case "Friday":
				i = 0;
				break;
		}
		return i;
	}

	int storeCandle(List listOfWeekNav, Session hSession) throws Exception
	{
		double dOpen, dHigh, dClose = -1, dLow;
		double wOpen, wHigh, wClose, wLow;
		Transaction tx = null;
		if(listOfWeekNav.isEmpty())
		{
			return 0;
		}
		else
		{	
			try
			{
				//tx = hSession.beginTransaction();
				EveryDayCandle everyDayCandle = null;
				List<Double> listOfNav = new ArrayList();
				//System.out.println("MFID\t" + "ope\t" + "clos\t" + "low\t" + "hig\t" + "TradeDate");
				Iterator itr = listOfWeekNav.iterator();
				Nav nav = null;
				//Date currentDate=((Nav)listOfWeekNav.get(0)).getTradeDate();
				Date currentDate;
				Date previousDate=null;
				
				//Date nextDate=calendar.getTime();
				while (itr.hasNext())
				{					
					everyDayCandle = new EveryDayCandle();
					nav = (Nav) itr.next();
					currentDate=nav.getTradeDate();
					Calendar calendar;
					//uncomment below when move to live
				/*	if(previousDate!=null)
					{
						calendar=Calendar.getInstance();
						calendar.setTime(previousDate);
						calendar.add(Calendar.DAY_OF_MONTH, 1);
						
						if(calendar.getTime().compareTo(currentDate)==0)
						{
							listOfNav.add(nav.getNav());
							dOpen = nav.getNav();
							dClose = dOpen;
							dHigh = dOpen;
							dLow = dOpen;
							everyDayCandle.setMutualFundId(nav.getMutualfundId());
							everyDayCandle.setTradeDate(currentDate);
							everyDayCandle.setCandleType(1);
							everyDayCandle.setOpen(dOpen);
							everyDayCandle.setHigh(dHigh);
							everyDayCandle.setTodaysClosePrice(dClose);
							everyDayCandle.setLow(dLow);
							System.out.println(nav.getMutualfundId() + "\t" + dOpen + "\t" + dClose + "\t" + dLow + "\t" + dHigh + "\t" + nav.getTradeDate() + "\t::Daily");
							hSession.saveOrUpdate(everyDayCandle);
							previousDate=currentDate;
						}
						else
						{
							//since currentDate is not previousDate+1 we need to check for holiday previousDate+1 in which case we have already set previousDate = previousDate+1
							long milliSec=currentDate.getTime()-previousDate.getTime();
							long days=(milliSec/(1000*60*60*24))-1;
							for(int i=0;i<days;i++)
							{
								Calendar newCalendar=Calendar.getInstance();
								newCalendar.setTime(previousDate);
								newCalendar.add(Calendar.DAY_OF_MONTH,1);
								previousDate=newCalendar.getTime();
								Holidays previousDateHoliday=(Holidays)hSession.createQuery("from Holidays where holiday=?").setParameter(0, previousDate).uniqueResult();
								if(previousDateHoliday==null)
								{
									//tx.rollback();
									throw new Exception("Data while updating daily candle was missed on "+previousDate+" for mutualFundId="+nav.getMutualfundId());
								}
							}
							listOfNav.add(nav.getNav());
							dOpen = nav.getNav();
							dClose = dOpen;
							dHigh = dOpen;
							dLow = dOpen;
							everyDayCandle.setMutualFundId(nav.getMutualfundId());
							everyDayCandle.setTradeDate(currentDate);
							everyDayCandle.setCandleType(1);
							everyDayCandle.setOpen(dOpen);
							everyDayCandle.setHigh(dHigh);
							everyDayCandle.setTodaysClosePrice(dClose);
							everyDayCandle.setLow(dLow);
							System.out.println(nav.getMutualfundId() + "\t" + dOpen + "\t" + dClose + "\t" + dLow + "\t" + dHigh + "\t" + nav.getTradeDate() + "\t::Daily");
							hSession.saveOrUpdate(everyDayCandle);
							previousDate=currentDate;
							
						}
					}				
					else
					{
						listOfNav.add(nav.getNav());
						dOpen = nav.getNav();
						dClose = dOpen;
						dHigh = dOpen;
						dLow = dOpen;
						everyDayCandle.setMutualFundId(nav.getMutualfundId());
						everyDayCandle.setTradeDate(currentDate);
						everyDayCandle.setCandleType(1);
						everyDayCandle.setOpen(dOpen);
						everyDayCandle.setHigh(dHigh);
						everyDayCandle.setTodaysClosePrice(dClose);
						everyDayCandle.setLow(dLow);
						System.out.println(nav.getMutualfundId() + "\t" + dOpen + "\t" + dClose + "\t" + dLow + "\t" + dHigh + "\t" + nav.getTradeDate() + "\t::Daily");
						hSession.saveOrUpdate(everyDayCandle);
						previousDate=currentDate;
					}*/					
					listOfNav.add(nav.getNav());
					dOpen = nav.getNav();
					dClose = dOpen;
					dHigh = dOpen;
					dLow = dOpen;
					everyDayCandle.setMutualFundId(nav.getMutualfundId());
					everyDayCandle.setTradeDate(currentDate);
					everyDayCandle.setCandleType(1);
					everyDayCandle.setOpen(dOpen);
					everyDayCandle.setHigh(dHigh);
					everyDayCandle.setTodaysClosePrice(dClose);
					everyDayCandle.setLow(dLow);
				//	System.out.println(nav.getMutualfundId() + "\t" + dOpen + "\t" + dClose + "\t" + dLow + "\t" + dHigh + "\t" + nav.getTradeDate() + "\t::Daily");
					hSession.saveOrUpdate(everyDayCandle);
				}
				//below code makes Nav at last index as week end and generate weekly candle
				Nav lastNav=(Nav)listOfWeekNav.get(listOfWeekNav.size()-1);
				Date lastNavOfWeekDate=lastNav.getTradeDate();
				
				//comment below persising code when run on live
				everyDayCandle = new EveryDayCandle();
				wOpen = listOfNav.get(0);
				wClose = listOfNav.get(listOfNav.size() - 1);
				wHigh = Collections.max(listOfNav);
				wLow = Collections.min(listOfNav);
				//System.out.println(nav.getMutualfundId() + "\t" + wOpen + "\t" + wClose + "\t" + wLow + "\t" + wHigh + "\t" + nav.getTradeDate() + "\t::WEEKLY");
				everyDayCandle.setMutualFundId(nav.getMutualfundId());
				everyDayCandle.setTradeDate(lastNavOfWeekDate);
				everyDayCandle.setCandleType(0);
				everyDayCandle.setOpen(wOpen);
				everyDayCandle.setHigh(wHigh);
				everyDayCandle.setTodaysClosePrice(wClose);
				everyDayCandle.setLow(wLow);

				hSession.saveOrUpdate(everyDayCandle);
				/*if(new SimpleDateFormat("EEEEE").format(lastNavOfWeekDate).equalsIgnoreCase("Friday"))
				{
					everyDayCandle = new EveryDayCandle();

					wOpen = listOfNav.get(0);
					wClose = listOfNav.get(listOfNav.size() - 1);
					wHigh = Collections.max(listOfNav);
					wLow = Collections.min(listOfNav);
					System.out.println(nav.getMutualfundId() + "\t" + wOpen + "\t" + wClose + "\t" + wLow + "\t" + wHigh + "\t" + nav.getTradeDate() + "\t::WEEKLY");
					everyDayCandle.setMutualFundId(nav.getMutualfundId());
					everyDayCandle.setTradeDate(lastNavOfWeekDate);
					everyDayCandle.setCandleType(0);
					everyDayCandle.setOpen(wOpen);
					everyDayCandle.setHigh(wHigh);
					everyDayCandle.setTodaysClosePrice(wClose);
					everyDayCandle.setLow(wLow);

					hSession.saveOrUpdate(everyDayCandle);
					
				}
				else if(new SimpleDateFormat("EEEEE").format(lastNavOfWeekDate).equalsIgnoreCase("Thursday"))
				{
					//since lastNavOfWeekDate is thursday ,so get Friday date and check it with holidays table
					Calendar cal=Calendar.getInstance();
					cal.setTime(lastNavOfWeekDate);
					cal.add(Calendar.DAY_OF_MONTH, 1);
					Date fridayDate=cal.getTime();
					Holidays isFridayHoliday=(Holidays)hSession.createQuery("from Holidays where holiday=?").setParameter(0, fridayDate).uniqueResult();
					if(isFridayHoliday!=null)
					{
						//means there is no friday data so make thursday as week end date
						everyDayCandle = new EveryDayCandle();

						wOpen = listOfNav.get(0);
						wClose = listOfNav.get(listOfNav.size() - 1);
						wHigh = Collections.max(listOfNav);
						wLow = Collections.min(listOfNav);
						System.out.println(nav.getMutualfundId() + "\t" + wOpen + "\t" + wClose + "\t" + wLow + "\t" + wHigh + "\t" + nav.getTradeDate() + "\t::WEEKLY");
						everyDayCandle.setMutualFundId(nav.getMutualfundId());
						everyDayCandle.setTradeDate(lastNavOfWeekDate);
						everyDayCandle.setCandleType(0);
						everyDayCandle.setOpen(wOpen);
						everyDayCandle.setHigh(wHigh);
						everyDayCandle.setTodaysClosePrice(wClose);
						everyDayCandle.setLow(wLow);

						hSession.saveOrUpdate(everyDayCandle);
					}
					else
					{
						List navsAfterFridayDate=hSession.createQuery("from Nav where mutualfundId=? and tradeDate>?").setParameter(0, nav.getMutualfundId()).setParameter(1,fridayDate).setMaxResults(2).list();
						if(navsAfterFridayDate!=null && navsAfterFridayDate.size()>0)
						{
							
							//tx.rollback();
							throw new Exception("Friday is not holiday on "+fridayDate+" but after friday we have data, not find nav on that for mf "+nav.getMutualfundId()+" day so not storing any data");
						}
						else
						{
							
							//we will not store weekly candle since friday is not holiday and we don't have its data
						}
							
					}
				}
				else if(new SimpleDateFormat("EEEEE").format(lastNavOfWeekDate).equalsIgnoreCase("Wednesday"))
				{
					//since lastNavOfWeekDate is wednesday ,so get Friday date,thursday date and check it with holidays table
					Calendar cal=Calendar.getInstance();
					cal.setTime(lastNavOfWeekDate);
					cal.add(Calendar.DAY_OF_MONTH, 2);
					Date fridayDate=cal.getTime();
					Holidays isFridayHoliday=(Holidays)hSession.createQuery("from Holidays where holiday=?").setParameter(0, fridayDate).uniqueResult();
					if(isFridayHoliday!=null)
					{
						//means there is no friday data(since its holiday) so make get thursdayDate and check for it's holiday
						cal.add(Calendar.DAY_OF_MONTH, -1);
						Date thursdayDate=cal.getTime();
						
						Holidays isThursdayHoliday=(Holidays)hSession.createQuery("from Holidays where holiday=?").setParameter(0, thursdayDate).uniqueResult();
						
						if(isThursdayHoliday!=null)
						{
							everyDayCandle = new EveryDayCandle();
							wOpen = listOfNav.get(0);
							wClose = listOfNav.get(listOfNav.size() - 1);
							wHigh = Collections.max(listOfNav);
							wLow = Collections.min(listOfNav);
							System.out.println(nav.getMutualfundId() + "\t" + wOpen + "\t" + wClose + "\t" + wLow + "\t" + wHigh + "\t" + nav.getTradeDate() + "\t::WEEKLY");
							everyDayCandle.setMutualFundId(nav.getMutualfundId());
							everyDayCandle.setTradeDate(lastNavOfWeekDate);
							everyDayCandle.setCandleType(0);
							everyDayCandle.setOpen(wOpen);
							everyDayCandle.setHigh(wHigh);
							everyDayCandle.setTodaysClosePrice(wClose);
							everyDayCandle.setLow(wLow);
	
							hSession.saveOrUpdate(everyDayCandle);
						}
						else
						{
							List navsAfterThursdayDate=hSession.createQuery("from Nav where mutualfundId=? and tradeDate>?").setParameter(0, nav.getMutualfundId()).setParameter(1,thursdayDate).setMaxResults(2).list();
							if(navsAfterThursdayDate!=null && navsAfterThursdayDate.size()>0)
							{
								//below persisting need to be changed					
								
								//tx.rollback();
								//throw new Exception("Thursday is not holiday on "+thursdayDate+" but after friday we have data, not find nav on that for mf "+nav.getMutualfundId()+" day so not storing any data");
							}
							else
							{
								//we will not store weekly candle since above thursday is not holiday and we don't have its data
								
							}
						}
					}
					else
					{
						List navsAfterFridayDate=hSession.createQuery("from Nav where mutualfundId=? and tradeDate>?").setParameter(0, nav.getMutualfundId()).setParameter(1,fridayDate).setMaxResults(2).list();
						if(navsAfterFridayDate!=null && navsAfterFridayDate.size()>0)
						{						
							//tx.rollback();
							//throw new Exception("Friday is not holiday on "+fridayDate+" but after friday we have data, not find nav on that for mf "+nav.getMutualfundId()+" day so not storing any data");
						}
						else
						{
							//we will not store weekly candle since friday is not holiday and we don't have its data

						}						
					}
				}
				else if(new SimpleDateFormat("EEEEE").format(lastNavOfWeekDate).equalsIgnoreCase("Tuesday"))
				{
					//since lastNavOfWeekDate is tuesday ,so get Friday,thursday,wednesday date and check it with holidays table
					Calendar cal=Calendar.getInstance();
					cal.setTime(lastNavOfWeekDate);
					cal.add(Calendar.DAY_OF_MONTH, 3);
					Date fridayDate=cal.getTime();
					Holidays isFridayHoliday=(Holidays)hSession.createQuery("from Holidays where holiday=?").setParameter(0, fridayDate).uniqueResult();
					if(isFridayHoliday!=null)
					{
						//means there is no friday data so make get thursdayDate and check for it's holiday
						cal.add(Calendar.DAY_OF_MONTH, -1);
						Date thursdayDate=cal.getTime();
						
						Holidays isThursdayHoliday=(Holidays)hSession.createQuery("from Holidays where holiday=?").setParameter(0, thursdayDate).uniqueResult();
						
						if(isThursdayHoliday!=null)
						{
							//means there is no friday data so make get thursdayDate and check for it's holiday
							cal.add(Calendar.DAY_OF_MONTH, -1);
							Date wednesdayDate=cal.getTime();
							
							Holidays isWednesdayHoliday=(Holidays)hSession.createQuery("from Holidays where holiday=?").setParameter(0, wednesdayDate).uniqueResult();
							
							if(isWednesdayHoliday!=null)
							{
								
								everyDayCandle = new EveryDayCandle();
								wOpen = listOfNav.get(0);
								wClose = listOfNav.get(listOfNav.size() - 1);
								wHigh = Collections.max(listOfNav);
								wLow = Collections.min(listOfNav);
								System.out.println(nav.getMutualfundId() + "\t" + wOpen + "\t" + wClose + "\t" + wLow + "\t" + wHigh + "\t" + nav.getTradeDate() + "\t::WEEKLY");
								everyDayCandle.setMutualFundId(nav.getMutualfundId());
								everyDayCandle.setTradeDate(lastNavOfWeekDate);
								everyDayCandle.setCandleType(0);
								everyDayCandle.setOpen(wOpen);
								everyDayCandle.setHigh(wHigh);
								everyDayCandle.setTodaysClosePrice(wClose);
								everyDayCandle.setLow(wLow);
		
								hSession.saveOrUpdate(everyDayCandle);
							}
							else
							{
								List navsAfterWednesdayDate=hSession.createQuery("from Nav where mutualfundId=? and tradeDate>?").setParameter(0, nav.getMutualfundId()).setParameter(1,wednesdayDate).setMaxResults(2).list();
								if(navsAfterWednesdayDate!=null && navsAfterWednesdayDate.size()>0)
								{
									//tx.rollback();
									throw new Exception("Wednesday is not holiday on "+wednesdayDate+" but after friday we have data, not find nav on that for mf "+nav.getMutualfundId()+" day so not storing any data");
								}
								else
								{
									//we will not store weekly candle since wednesday is not holiday and we don't have its data
								}	
							}
							
						}
						else
						{
							List navsAfterThursdayDate=hSession.createQuery("from Nav where mutualfundId=? and tradeDate>?").setParameter(0, nav.getMutualfundId()).setParameter(1,thursdayDate).setMaxResults(2).list();
							if(navsAfterThursdayDate!=null && navsAfterThursdayDate.size()>0)
							{
								//tx.rollback();
								throw new Exception("Thursday is not holiday on "+thursdayDate+" but after friday we have data, not find nav on that for mf "+nav.getMutualfundId()+" day so not storing any data");
							}
							else
							{
								//we will not store weekly candle since above thursday is not holiday and we don't have its data
							}
						}
					}
					else
					{
						List navsAfterFridayDate=hSession.createQuery("from Nav where mutualfundId=? and tradeDate>?").setParameter(0, nav.getMutualfundId()).setParameter(1,fridayDate).setMaxResults(2).list();
						if(navsAfterFridayDate!=null && navsAfterFridayDate.size()>0)
						{
							//tx.rollback();
							throw new Exception("Friday is not holiday on "+fridayDate+" but after friday we have data, not find nav on that for mf "+nav.getMutualfundId()+" day so not storing any data");
						}
						else
						{
							//we will not store weekly candle since above friday is not holiday and we don't have its data
						}
					}
				}*/
				//tx.commit();
				return 1;
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new Exception(e.getMessage());
			}
		}
	}

	public int generateCurrentCandleForEveryDayCandle() throws Exception
	{
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			tx = hSession.beginTransaction();
			Date currentDate = (Date) hSession.createQuery("select max(tradeDate) from CurrentDayCandle").uniqueResult();
			Date latestDateFromEveryDayCandle = (Date) hSession.createQuery("select max(tradeDate) from EveryDayCandle").uniqueResult();
			if(latestDateFromEveryDayCandle != null)
			{
				
				//int r = hSession.createQuery("delete from EveryDayCandle where tradeDate=?").setParameter(0, currentDate).executeUpdate();
				if(currentDate.compareTo(latestDateFromEveryDayCandle) == 1)
				{
					SQLQuery query = hSession.createSQLQuery("insert into EveryDayCandle select * from CurrentDayCandle");
					int i = query.executeUpdate();
				}
				else if(currentDate.compareTo(latestDateFromEveryDayCandle) == 0)
				{
					SQLQuery query = hSession.createSQLQuery("insert into EveryDayCandle select * from CurrentDayCandle");
					int i = query.executeUpdate();
					/*throw new Exception("Current Day for Every Day Candle table is already updated or Current Day Table is not updated");*/
				}
				else if(currentDate.compareTo(latestDateFromEveryDayCandle) < 0)
				{
					throw new Exception("Current Day Candle need to be updated (i.e EveryDayCandle Already updated much)");
				}
				else if(currentDate.compareTo(latestDateFromEveryDayCandle)>1)
				{
					throw new Exception("Difference between last date of currentCandle is greater than last of EveryDayCandle (i.e data is not populated correctly:update using datewise)");
				}
			}
			else
			{
				throw new Exception("EveryDayCandle table is Empty");
			}

			tx.commit();
			return 1;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
			throw new Exception(ex.getMessage());
		}
		finally
		{
			hSession.close();
			
		}
		
	}

	public void updateEveryDayCandleAfterStartDate() throws Exception
	{
		Session hSession = null;
		String noAvailableData="";
		String differenceIsMore="";
		Transaction tx=null;
		try
		{
			
			// since oneWeekNavList contains nav for whole one week starting from monday we need to adjust firstDate(below) so that it represents the start date of that week
			// difference b/w startDate and firstDate is startDate can be any within the range of monday-friday but firstDate is the date of Monday
			//Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(sDate);
			hSession = HibernateBridge.getSessionFactory().openSession();
			Date lastDateInEveryDayCandle=null;
			//counts the difference between max(tradeDate)as m from Everydaycandle and min(tradeDate) > m from Nav
			
			List mfList = hSession.createQuery("select distinct mutualfundId from Nav where mutualfundId in (select id from MutualFund where category=1 and optionType=1 and direct!=1 order by subcategory) order by mutualfundId").list();
			Iterator itr = mfList.iterator();
			while (itr.hasNext())
			{
				long mfId = (long) itr.next();
				
				lastDateInEveryDayCandle=(Date)hSession.createQuery("select max(tradeDate) from EveryDayCandle where mutualFundId=?").setParameter(0,mfId).setMaxResults(1).uniqueResult();
				System.out.println("Last date for mfid="+mfId+"  "+lastDateInEveryDayCandle);
				tx=hSession.beginTransaction();
				if(lastDateInEveryDayCandle!=null)
				{
					Date afterLastDateInEverDayCandleInNav=(Date)hSession.createQuery("select min(tradeDate) from Nav where mutualfundId=? and tradeDate>?").setParameter(0, mfId).setParameter(1, lastDateInEveryDayCandle).setMaxResults(1).uniqueResult();
					if(afterLastDateInEverDayCandleInNav==null)
					{
						continue;
					}
					/*long diff=(afterLastDateInEverDayCandleInNav.getTime()-lastDateInEveryDayCandle.getTime())/(1000*60*60*24)-1;
					if(diff >= 4)
					{
						differenceIsMore=differenceIsMore+String.valueOf(mfId);
						continue;
					}*/				
				}
				if(lastDateInEveryDayCandle==null)
				{
					System.out.println("No Date in Everydaycandle so use min(tradeDate) from nav for mfid="+mfId);
					lastDateInEveryDayCandle=(Date)hSession.createQuery("select min(tradeDate) from Nav where mutualfundId=?").setParameter(0,mfId).setMaxResults(1).uniqueResult();
				}
				if(lastDateInEveryDayCandle==null)
				{
					noAvailableData=noAvailableData+","+String.valueOf(mfId);
					continue;
				}
				List navListTemp = hSession.createQuery("from Nav where mutualfundId=? and tradeDate>=? order by tradeDate asc").setParameter(0, mfId).setParameter(1, lastDateInEveryDayCandle).list();
				
				Nav nav1 = (Nav) navListTemp.get(0);
				Date tempFirstDate = nav1.getTradeDate();
				int decrementToGetMondayDateTemp = getIncrement(tempFirstDate) - 4;
				Calendar firstDateCalendarTemp = Calendar.getInstance();
				firstDateCalendarTemp.setTime(tempFirstDate);
				firstDateCalendarTemp.add(Calendar.DATE, decrementToGetMondayDateTemp);
				tempFirstDate = firstDateCalendarTemp.getTime();

				List navList = hSession.createQuery("from Nav where mutualfundId=? and tradeDate>=? order by tradeDate asc").setParameter(0, mfId).setParameter(1, tempFirstDate).list();
				//lastDateToBeDeleted is that date which is weekly day after generating candles
				Date lastDateToBeDeleted=null;
				for (int i = 0; i < navList.size(); i++)
				{
					Nav nav = (Nav) navList.get(i);
					Date firstDate = nav.getTradeDate();
					int incrementToGetFridayDate = getIncrement(firstDate);

					Calendar fridayDateCalendar = Calendar.getInstance();
					fridayDateCalendar.setTime(firstDate);
					fridayDateCalendar.add(Calendar.DATE, incrementToGetFridayDate);
					Date fridayDate = fridayDateCalendar.getTime();

					List<Nav> oneWeekNavList = hSession.createQuery("from Nav where mutualfundId=? and tradeDate>=? and tradeDate<=? order by tradeDate").setParameter(0, mfId).setParameter(1, firstDate).setParameter(2, fridayDate).list();
					Iterator itr2 = oneWeekNavList.iterator();
					
					i += oneWeekNavList.size() - 1;
					if(i == -1)
						break;
					//System.out.println("i=" + i);
					storeCandle(oneWeekNavList, hSession);
					if(i==navList.size()-1)
					{
						Nav nav2=(Nav)oneWeekNavList.get(oneWeekNavList.size()-1);
						lastDateToBeDeleted=nav2.getTradeDate();
						//System.out.println("Last date to be deleted="+lastDateToBeDeleted+"  mfId="+mfId);
						if(!new SimpleDateFormat("EEEEE").format(lastDateToBeDeleted).equalsIgnoreCase("Friday"))
						{
							//System.out.println("last date not equal to friday");
							Holidays holidays=(Holidays)hSession.createQuery("from Holidays where holiday=?").setParameter(0, fridayDate).setMaxResults(1).uniqueResult();
							if(holidays==null)
							{
								int deleteResult=hSession.createQuery("DELETE from EveryDayCandle where mutualFundId=? and tradeDate=? and candleType=0").setParameter(0, mfId).setParameter(1, lastDateToBeDeleted).executeUpdate();					
								//System.out.println("deleteResult="+deleteResult);
							}
						}
					}
				}			
				hSession.flush();
				tx.commit();
			}	
			if(noAvailableData.length()!=0)
			{
				throw new Exception("the difference between max(tradeDate) from Everdaycandle and tradeDate > max(tradeDate) from Nav is >= 4 days for mf="+differenceIsMore+" and "+"(While updating EveryDayCandle table) There is no extra nav's for "+noAvailableData);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}

	public void deleteEveryDayCandleAfterStartDate(String sDate) throws ParseException
	{
		Session hSession = null;
		Transaction tx = null;
		Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(sDate);
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			tx = hSession.beginTransaction();
			int n = hSession.createQuery("delete from EveryDayCandle where tradeDate>=?").setParameter(0, startDate).executeUpdate();

			tx.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public int delete(String currentOrAll) throws Exception
	{
		Session hSession = null;
		Transaction tx = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			tx = hSession.beginTransaction();
			if(currentOrAll.equalsIgnoreCase("all"))
			{
				//do not execute this query since everydaycandle till 26th july is updated correctly
				//instead use dateWise delete query
				//int n = hSession.createQuery("delete from EveryDayCandle").executeUpdate();
			}
			else if(currentOrAll.equalsIgnoreCase("current"))
			{
				Date currentDate = (Date) hSession.createQuery("select tradeDate from CurrentDayCandle").setMaxResults(1).uniqueResult();
				Date lastEveryDayCandleDate = (Date) hSession.createQuery("select tradeDate from EveryDayCandle order by tradeDate desc").setMaxResults(1).uniqueResult();
				if(currentDate != null && lastEveryDayCandleDate.compareTo(currentDate) == 0)
				{
					int n = hSession.createQuery("delete from EveryDayCandle where tradeDate=?").setParameter(0, currentDate).executeUpdate();
				}
				else if(currentDate == null)
				{
					throw new Exception("CurrentDayCandle table is currently empty (deletion is made on the basis of tradeDate in CurrentDayCandle table)");
				}
				else if(lastEveryDayCandleDate.compareTo(currentDate) < 0)
				{
					throw new Exception("EveryDayCandle table does not contain CurrentDayCandle table tradeDate");
				}
				else if(lastEveryDayCandleDate.compareTo(currentDate) > 0)
				{
					throw new Exception("CurrentDayCandle is not properly updated(EveryDayCandle have extra greater dates)");
				}
			}
			tx.commit();
			return 1;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}
	
	
	


}
