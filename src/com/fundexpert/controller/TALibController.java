package com.fundexpert.controller;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.apidoc.util.ImageLinks;
import com.fundexpert.dao.EveryDayCandle;

public class TALibController
{
	public void updateEveryDayAfterDate() throws Exception
	{
		//call updateEveryDayCandleAfterDate() of EveryDayCandleController and then update the talibIndicators
		Session hSession = null;
		Transaction tx = null;
		String missedMfs = "";
		Date lastUpdatedDate=null;
		try
		{			
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List mfList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where candleType=?").setParameter(0, 1).list();
			Iterator itr = mfList.iterator();
			while (itr.hasNext())
			{
				long mfId = (long) itr.next();			
				
				tx = hSession.beginTransaction();
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from EveryDayCandle where candleType=1 and mutualFundId=? and ema20!=0 and rsi!=0").setParameter(0, mfId).setMaxResults(1).uniqueResult();
				if(lastUpdatedDate==null)
				{
					//reason of 37 in setMaxResults() is because in updateAllEveryDayCandle() we started the loop from 36th index i.e 37th day
					List days36NextToMinTradeDate=hSession.createQuery("select tradeDate from EveryDayCandle where candleType=1 and mutualFundId=? order by tradeDate").setParameter(0, mfId).setMaxResults(37).list();
					//the value 37 denotes that atlest 37 past values are needed to calculate ema's , upperband , macd etc..
					if(days36NextToMinTradeDate.size()<37)
						continue;
					lastUpdatedDate=(Date)days36NextToMinTradeDate.get(36);
				}
				System.out.println("mfId="+mfId+" lastUpdatedDate="+lastUpdatedDate+" Daily");
				List everyDayOfMfList = hSession.createQuery("from EveryDayCandle where mutualFundId=? and tradeDate>=? and candleType=? order by tradeDate").setParameter(0, mfId).setParameter(1, lastUpdatedDate).setParameter(2, 1).list();
				Iterator itr1 = everyDayOfMfList.iterator();
				
				while (itr1.hasNext())
				{
					double atr = 0, ema20 = 0, ema50 = 0, ema200 = 0;
					double bands[] = null;
					float psar = 0, adx, macd, macdSignal, macdHistogram, rsi;
					EveryDayCandle edc1 = (EveryDayCandle) itr1.next();
					Date tradeDate = edc1.getTradeDate();
					

					List date200DaysData = hSession.createQuery("select tradeDate from EveryDayCandle where mutualFundId=? and tradeDate<=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, tradeDate).setParameter(2, 1).setMaxResults(200).list();
					Date fromDate = (Date) date200DaysData.get(date200DaysData.size() - 1);
					Date tillDate = (Date) date200DaysData.get(0);
					Calendar fromCalendar = Calendar.getInstance();
					fromCalendar.setTime(fromDate);
					Calendar tillCalendar = Calendar.getInstance();
					tillCalendar.setTime(tillDate);
					

					NewTALibEngine newTALibEngine = new NewTALibEngine(mfId, fromCalendar, tillCalendar);
					psar = newTALibEngine.calculatePsarCustom(mfId, fromCalendar, tillCalendar, .02, .2);
					edc1.setPsar(ImageLinks.getRoundedFloatValue(psar, 2));

					bands = newTALibEngine.calculateBBandsCustom(mfId, fromCalendar, tillCalendar, 20, 2.0, 2.0);
					if(bands != null)
					{
						edc1.setUpperBand(ImageLinks.getRoundedValue(bands[0], 2));
						edc1.setMiddleBand(ImageLinks.getRoundedValue(bands[1], 2));
						edc1.setLowerBand(ImageLinks.getRoundedValue(bands[2], 2));
					}

					atr = newTALibEngine.calculateAtr(mfId, fromCalendar, tillCalendar, 14);
					edc1.setAtr(atr);

					adx = newTALibEngine.calculateADXCustom(mfId, fromCalendar, tillCalendar, 14);
					edc1.setAdx(adx);

					macd = newTALibEngine.calculateMacdCustom(mfId, fromCalendar, tillCalendar, 12, 26, 9);
					edc1.setMacd(ImageLinks.getRoundedFloatValue(macd, 4));

					macdSignal = newTALibEngine.calculateMacdSignalCustom(mfId, fromCalendar, tillCalendar, 12, 26, 9);
					edc1.setMacdSignal(ImageLinks.getRoundedFloatValue(macdSignal, 4));

					macdHistogram = newTALibEngine.calculateMacdHistogramCustom(mfId, fromCalendar, tillCalendar, 12, 26, 9);
					edc1.setMacdHistogram(ImageLinks.getRoundedFloatValue(macdHistogram, 4));

					rsi = newTALibEngine.calculateRSICustom(mfId, fromCalendar, tillCalendar, 14);
					edc1.setRsi(ImageLinks.getRoundedFloatValue(rsi, 1));

					ema20 = newTALibEngine.calculateEMACustom(mfId, fromCalendar, tillCalendar, 20);
					edc1.setEma20(ImageLinks.getRoundedValue(ema20, 2));					
					
					if(date200DaysData.size()>=50)
					{
						ema50 = newTALibEngine.calculateEMACustom(mfId, fromCalendar, tillCalendar, 50);
						edc1.setEma50(ImageLinks.getRoundedValue(ema50, 2));
					}
					else
					{
						edc1.setEma50(0.0d);
					}
					
					if(date200DaysData.size()==200)
					{
						ema200 = newTALibEngine.calculateEMACustom(mfId, fromCalendar, tillCalendar, 200);
						edc1.setEma200(ImageLinks.getRoundedValue(ema200, 2));
					}
					else
					{
						edc1.setEma200(0.0d);
					}
					hSession.saveOrUpdate(edc1);
				}
				tx.commit();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	public void updateWeeklyEveryDayAfterDate() throws Exception
	{
		// before this function call updateEveryDayCandleAfterDate() of EveryDayCandleController and then update the talibIndicators
		Session hSession = null;
		Transaction tx = null;
		String missedMfs = "";
		Date lastUpdatedDate=null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			
			List mfList = hSession.createQuery("select distinct(mutualFundId) from EveryDayCandle where candleType=?").setParameter(0, 0).list();
			Iterator itr = mfList.iterator();
			while (itr.hasNext())
			{
				tx = hSession.beginTransaction();
				long mfId = (long) itr.next();
				
				lastUpdatedDate=(Date)hSession.createQuery("select max(tradeDate) from EveryDayCandle where mutualFundId=? and candleType=0 and ema20!=0 and rsi!=0 and atr!=0").setParameter(0, mfId).setMaxResults(1).uniqueResult();
				if(lastUpdatedDate==null)
				{
					//reason of 37 in setMaxResults() is because in updateAllEveryDayCandle() we started the loop from 36th index i.e 37th day
					List days36NextToMinTradeDate=hSession.createQuery("select tradeDate from EveryDayCandle where candleType=0 and mutualFundId=? order by tradeDate").setParameter(0, mfId).setMaxResults(37).list();
					if(days36NextToMinTradeDate.size()<37)
						continue;
					lastUpdatedDate=(Date)days36NextToMinTradeDate.get(36);
				}
				System.out.println("mfId="+mfId+" lastUpdatedDate="+lastUpdatedDate+" Weekly");
				List everyDayOfMfList = hSession.createQuery("from EveryDayCandle where mutualFundId=? and tradeDate>=? and candleType=0 order by tradeDate").setParameter(0, mfId).setParameter(1, lastUpdatedDate).list();
				Iterator itr1 = everyDayOfMfList.iterator();
				while (itr1.hasNext())
				{
					double atr = 0, ema20 = 0, ema50 = 0, ema200 = 0;
					double bands[] = null;
					float psar = 0, adx, macd, macdSignal, macdHistogram, rsi;
					EveryDayCandle edc1 = (EveryDayCandle) itr1.next();
					Date tradeDate = edc1.getTradeDate();


					List date200DaysData = hSession.createQuery("select tradeDate from EveryDayCandle where mutualFundId=? and tradeDate<=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, tradeDate).setParameter(2, 0).setMaxResults(200).list();
					Date fromDate = (Date) date200DaysData.get(date200DaysData.size() - 1);
					Date tillDate = (Date) date200DaysData.get(0);
					Calendar fromCalendar = Calendar.getInstance();
					fromCalendar.setTime(fromDate);
					Calendar tillCalendar = Calendar.getInstance();
					tillCalendar.setTime(tillDate);

					

					NewTALibEngine newTALibEngine = new NewTALibEngine(mfId, fromCalendar, tillCalendar);
					psar = newTALibEngine.calculateWeeklyPsarCustom(mfId, fromCalendar, tillCalendar, .02, .2);
					edc1.setPsar(ImageLinks.getRoundedFloatValue(psar, 2));
					
					bands = newTALibEngine.calculateWeeklyBBandsCustom(mfId, fromCalendar, tillCalendar, 20, 2.0, 2.0);
					if(bands != null)
					{
						edc1.setUpperBand(ImageLinks.getRoundedValue(bands[0], 2));
						edc1.setMiddleBand(ImageLinks.getRoundedValue(bands[1], 2));
						edc1.setLowerBand(ImageLinks.getRoundedValue(bands[2], 2));
					}

					atr = newTALibEngine.calculateWeeklyAtr(mfId, fromCalendar, tillCalendar, 14);
					edc1.setAtr(atr);

					// System.out.println("klo");
					adx = newTALibEngine.calculateWeeklyADXCustom(mfId, fromCalendar, tillCalendar, 14);
					// System.out.println(adx + " value=" + NewTALibEngine.calculateADXCustom(mfId, fromCalendar, tillCalendar, 14));
					edc1.setAdx(adx);

					macd = newTALibEngine.calculateWeeklyMacdCustom(mfId, fromCalendar, tillCalendar, 12, 26, 9);
					edc1.setMacd(ImageLinks.getRoundedFloatValue(macd, 4));

					macdSignal = newTALibEngine.calculateWeeklyMacdSignalCustom(mfId, fromCalendar, tillCalendar, 12, 26, 9);
					edc1.setMacdSignal(ImageLinks.getRoundedFloatValue(macdSignal, 4));

					macdHistogram = newTALibEngine.calculateWeeklyMacdHistogramCustom(mfId, fromCalendar, tillCalendar, 12, 26, 9);
					edc1.setMacdHistogram(ImageLinks.getRoundedFloatValue(macdHistogram, 4));

					rsi = newTALibEngine.calculateWeeklyRSICustom(mfId, fromCalendar, tillCalendar, 14);
					edc1.setRsi(ImageLinks.getRoundedFloatValue(rsi, 1));

					

					if(date200DaysData.size()>=20)
					{
						ema20 = newTALibEngine.calculateWeeklyEMACustom(mfId, fromCalendar, tillCalendar, 20);
						edc1.setEma20(ImageLinks.getRoundedValue(ema20, 2));
					}
					else
					{
						edc1.setEma20(0.0d);
					}
					
					if(date200DaysData.size()>=50)
					{
						ema50 = newTALibEngine.calculateWeeklyEMACustom(mfId, fromCalendar, tillCalendar, 50);
						edc1.setEma50(ImageLinks.getRoundedValue(ema50, 2));
					}
					else
					{
						edc1.setEma50(0.0d);
					}
					if(date200DaysData.size()==200)
					{
						ema200 = newTALibEngine.calculateWeeklyEMACustom(mfId, fromCalendar, tillCalendar, 200);
						edc1.setEma200(ImageLinks.getRoundedValue(ema200, 2));
					}
					else
					{
						edc1.setEma200(0.0d);
					}
					hSession.saveOrUpdate(edc1);
				}
				tx.commit();
			}		
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
			
		}
		finally
		{
			hSession.close();
		}

	}
}
