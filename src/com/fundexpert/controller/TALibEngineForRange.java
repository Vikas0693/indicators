package com.gumptionlabs.stock.operations.core;

import com.gumptionlabs.stock.dao.core.technical.*;
import com.gumptionlabs.stock.helpers.ImageLinks;
import com.gumptionlabs.stock.util.StrategyLabsException;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MAType;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

import java.util.*;

/**
 * User: Vaishakh
 * Date: 12/19/12
 * Time: 10:53 AM
 */
public class TALibEngineForRange {

    public static Map<Date, Rsi> calculateRSIForRange(String symbol, int optInTimePeriod, float[] closeValues, Date[] tradeDates, int requiredDays) throws StrategyLabsException {

        Core core = new Core();

        float inReal[] = closeValues;

        int total_days_data = inReal.length;
        int startIdx1 = optInTimePeriod;
        int endIdx1 = total_days_data - 1;

        MInteger outBegIdx1 = new MInteger();
        MInteger outNbElement1 = new MInteger();
        double outReal1[] = new double[inReal.length];


        RetCode code1 = core.rsi(startIdx1, endIdx1, inReal, optInTimePeriod, outBegIdx1, outNbElement1, outReal1);

        if (code1 != RetCode.Success) {
            return null;
        }


        if (outNbElement1.value > 0) {

            Map<Date, Rsi> dataList = new LinkedHashMap<Date, Rsi>(requiredDays);
            Rsi rsi;
            int lengthOfTradeDates = tradeDates.length;

            for (int i = outNbElement1.value - 1; i >= 0; i--) {
                lengthOfTradeDates--;

                if (requiredDays != -1 && dataList.size() == requiredDays) {
                    break;
                }

                rsi = new Rsi();
                rsi.setPeriod(optInTimePeriod);
                rsi.setSymbol(symbol);
                rsi.setRsi(ImageLinks.getRoundedValue(outReal1[i],2));
                rsi.setTradeDate(tradeDates[lengthOfTradeDates]);

                dataList.put(tradeDates[lengthOfTradeDates],rsi);
            }
            return dataList;
        }

        return null;
    }

    public static Map<Date, WilliamR> calculateWilliamRForRange(String symbol, int optInTimePeriod, float[] highValues, float[] lowValues, float[] closeValues, Date[] tradeDates, int requiredDays) {

        Core core = new Core();

        float inHigh[] = highValues;
        float inLow[] = lowValues;
        float inClose[] = closeValues;


        int startIdx = optInTimePeriod;
        int total_days_data = inHigh.length;
        int endIdx = total_days_data - 1;


        MInteger outBegIdxA = new MInteger();
        MInteger outNbElementA = new MInteger();

        double outReal[] = new double[inHigh.length];

        RetCode code = core.willR(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdxA, outNbElementA, outReal);

        if (code != RetCode.Success) {
            return null;
        }


        if (outNbElementA.value > 0) {

            Map<Date, WilliamR> dataList = new LinkedHashMap<Date, WilliamR>(requiredDays);
            WilliamR williamR;
            int lengthOfTradeDates = tradeDates.length;

            for (int i = outNbElementA.value - 1; i >= 0; i--) {
                lengthOfTradeDates--;

                if (requiredDays != -1 && dataList.size() == requiredDays) {
                    break;
                }

                williamR = new WilliamR();
                williamR.setPeriod(optInTimePeriod);
                williamR.setSymbol(symbol);
                williamR.setWilliamR(ImageLinks.getRoundedValue(outReal[i],2));
                williamR.setTradeDate(tradeDates[lengthOfTradeDates]);

                dataList.put(tradeDates[lengthOfTradeDates], williamR);
            }
            return dataList;
        }

        return null;

    }

    public static Map<Date, Mfi> calculateMFIForRange(String symbol, int optInTimePeriod, float[] highValues, float[] lowValues, float[] closeValues, float[] volumeValues, Date[] tradeDates, int requiredDays) {
        Core core = new Core();

        float inHigh[] = highValues;
        float inLow[] = lowValues;
        float inClose[] = closeValues;
        float inVolume[] = volumeValues;

        int startIdx = optInTimePeriod;
        int total_days_data = inHigh.length;
        int endIdx = total_days_data - 1;


        MInteger outBegIdxA = new MInteger();
        MInteger outNbElementA = new MInteger();


        double outReal[] = new double[inHigh.length];

        RetCode code = core.mfi(startIdx, endIdx, inHigh, inLow, inClose, inVolume, optInTimePeriod, outBegIdxA, outNbElementA, outReal);

        if (code != RetCode.Success) {
            return null;
        }


        if (outNbElementA.value > 0) {

            Map<Date, Mfi> dataList = new LinkedHashMap<Date, Mfi>(requiredDays);
            Mfi mfi;
            int lengthOfTradeDates = tradeDates.length;

            for (int i = outNbElementA.value - 1; i >= 0; i--) {
                lengthOfTradeDates--;

                if (requiredDays != -1 && dataList.size() == requiredDays) {
                    break;
                }

                mfi = new Mfi();
                mfi.setPeriod(optInTimePeriod);
                mfi.setSymbol(symbol);
                mfi.setMfi(ImageLinks.getRoundedValue(outReal[i],2));
                mfi.setTradeDate(tradeDates[lengthOfTradeDates]);

                dataList.put(tradeDates[lengthOfTradeDates], mfi);
            }
            return dataList;
        }
        return null;

    }

    public static Map<Date, Cci> calculateCCIForRange(String symbol, int optInTimePeriod, float[] highValues, float[] lowValues, float[] closeValues, Date[] tradeDates, int requiredDays) {

        Core core = new Core();

        float[] inHigh = highValues;
        float[] inLow = lowValues;
        float[] inClose = closeValues;

        int startIdx = optInTimePeriod;
        int total_days_data = inHigh.length;
        int endIdx = total_days_data - 1;


        MInteger outBegIdxA = new MInteger();
        MInteger outNbElementA = new MInteger();


        double outReal[] = new double[inHigh.length];

        RetCode code = core.cci(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdxA, outNbElementA, outReal);

        if (code != RetCode.Success) {
            return null;
        }


        if (outNbElementA.value > 0) {

            Map<Date, Cci> dataList = new LinkedHashMap<Date, Cci>(requiredDays);
            Cci cci;
            int lengthOfTradeDates = tradeDates.length;

            for (int i = outNbElementA.value - 1; i >= 0; i--) {
                lengthOfTradeDates--;

                if (requiredDays != -1 && dataList.size() == requiredDays) {
                    break;
                }

                cci = new Cci();
                cci.setPeriod(optInTimePeriod);
                cci.setSymbol(symbol);
                cci.setCci(ImageLinks.getRoundedValue(outReal[i],2));
                cci.setTradeDate(tradeDates[lengthOfTradeDates]);

                dataList.put(tradeDates[lengthOfTradeDates], cci);
            }
            return dataList;
        }

        return null;

    }

    public static Map<Date, Psar> calculatePsarForRange(String symbol, int optInTimePeriod, float[] highValues, float[] lowValues, Date[] tradeDates, int requiredDays) {

        Core core = new Core();

        float[] inHigh = highValues;
        float[] inLow = lowValues;

        int startIdx = 0;
        int total_days_data = inHigh.length;
        int endIdx = total_days_data - 1;


        MInteger outBegIdxA = new MInteger();
        MInteger outNbElementA = new MInteger();


        double outReal[] = new double[inHigh.length];
        double inAcc = 0.02;
        double inMax = .2;
        RetCode code = core.sar(startIdx, endIdx, inHigh, inLow, inAcc, inMax, outBegIdxA, outNbElementA, outReal);

        if (code != RetCode.Success) {
            return null;
        }

        if (outNbElementA.value > 0) {

            Map<Date, Psar> dataList = new LinkedHashMap<Date, Psar>(requiredDays);
            Psar psar;
            int lengthOfTradeDates = tradeDates.length;

            for (int i = outNbElementA.value - 1; i >= 0; i--) {
                lengthOfTradeDates--;

                if (requiredDays != -1 && dataList.size() == requiredDays) {
                    break;
                }

                psar = new Psar();
                psar.setPeriod(optInTimePeriod);
                psar.setSymbol(symbol);
                psar.setpSar(ImageLinks.getRoundedValue(outReal[i],2));
                psar.setTradeDate(tradeDates[lengthOfTradeDates]);

                dataList.put(tradeDates[lengthOfTradeDates], psar);
            }
            return dataList;
        }
        return null;

    }

    public static Map<Date, Adx> calculateADXForRange(String symbol, int optInTimePeriod, float[] highValues, float[] lowValues, float[] closeValues, Date[] tradeDates, int requiredDays) {

        Core core = new Core();

        float[] inHigh = highValues;
        float[] inLow = lowValues;
        float[] inClose = closeValues;

        int startIdx = (2 * optInTimePeriod) - 1;
        int total_days_data = inHigh.length;
        int endIdx = total_days_data - 1;

        MInteger outBegIdx = new MInteger();
        MInteger outNbElement = new MInteger();
        MInteger outBegIdxA = new MInteger();
        MInteger outNbElementA = new MInteger();
        MInteger outBegIdxB = new MInteger();
        MInteger outNbElementB = new MInteger();


        double outReal[] = new double[inHigh.length];
        double outReal2[] = new double[inHigh.length];
        double outReal3[] = new double[inHigh.length];

        RetCode code = core.adx(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdxA, outNbElementA, outReal);

        if (code != RetCode.Success) {
            return null;
        }
        RetCode code2 = core.minusDI(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdx, outNbElement, outReal2);
        if (code2 != RetCode.Success) {
            return null;
        }
        RetCode code3 = core.plusDI(startIdx, endIdx, inHigh, inLow, inClose, optInTimePeriod, outBegIdxB, outNbElementB, outReal3);
        if (code3 != RetCode.Success) {
            return null;
        }

        if (outNbElementA.value > 0) {

            Map<Date, Adx> dataList = new LinkedHashMap<Date, Adx>(requiredDays);
            Adx adx;
            int lengthOfTradeDates = tradeDates.length;

            for (int i = outNbElementA.value - 1; i >= 0; i--) {
                lengthOfTradeDates--;

                if (requiredDays != -1 && dataList.size() == requiredDays) {
                    break;
                }

                adx = new Adx();
                adx.setPeriod(optInTimePeriod);
                adx.setSymbol(symbol);
                adx.setTradeDate(tradeDates[lengthOfTradeDates]);
                adx.setAdx(ImageLinks.getRoundedValue(outReal[i],2));
                adx.setMinusDi(ImageLinks.getRoundedValue(outReal2[i],2));
                adx.setPlusDi(ImageLinks.getRoundedValue(outReal3[i],2));

                dataList.put(tradeDates[lengthOfTradeDates], adx);
            }
            return dataList;
        }

        return null;

    }

    public static Map<Date, Atr> calculateATRForRange(String symbol, int optInTimePeriod, float[] highValues, float[] lowValues, float[] closeValues, Date[] tradeDates, int requiredDays) {
        Core core = new Core();

        float[] inHigh = highValues;
        float[] inLow = lowValues;
        float[] inReal = closeValues;


        int total_days_data = inReal.length;

        //        int startIdx1 = optInTimePeriod - 1;
        int startIdx1 = optInTimePeriod;
        int endIdx1 = total_days_data - 1;


        MInteger outBegIdx1 = new MInteger();
        MInteger outNbElement1 = new MInteger();
        double outReal1[] = new double[inReal.length];

        RetCode code1 = core.atr(startIdx1, endIdx1, inHigh, inLow, inReal, optInTimePeriod, outBegIdx1, outNbElement1, outReal1);
        if (code1 != RetCode.Success) {
            return null;
        }


        if (outNbElement1.value > 0) {

            Map<Date, Atr> dataList = new LinkedHashMap<Date, Atr>(requiredDays);
            Atr atr;
            int lengthOfTradeDates = tradeDates.length;

            for (int i = outNbElement1.value - 1; i >= 0; i--) {
                lengthOfTradeDates--;

                if (requiredDays != -1 && dataList.size() == requiredDays) {
                    break;
                }

                atr = new Atr();
                atr.setPeriod(optInTimePeriod);
                atr.setSymbol(symbol);
                atr.setAtr(ImageLinks.getRoundedValue(outReal1[i],2));
                atr.setTradeDate(tradeDates[lengthOfTradeDates]);

                dataList.put(tradeDates[lengthOfTradeDates], atr);
            }
            return dataList;
        }

        return null;
    }

    public static Map<Date, BBands> calculateBBandsForRange(String symbol, int optInTimePeriod, double upMultiplier, double downMultiplier, float[] closeValues, Date[] tradeDates, int requiredDays) {
        Core core = new Core();

        float inReal[] = closeValues;


        int total_days_data = inReal.length;

        int startIdx1 = optInTimePeriod;
        int endIdx1 = total_days_data - 1;


        MInteger outBegIdx1 = new MInteger();
        MInteger outNbElement1 = new MInteger();
        double outUpper[] = new double[inReal.length];
        double outMiddle[] = new double[inReal.length];
        double outLower[] = new double[inReal.length];

        RetCode code1 = core.bbands(startIdx1, endIdx1, inReal, optInTimePeriod, upMultiplier, downMultiplier, MAType.Sma, outBegIdx1, outNbElement1, outUpper, outMiddle, outLower);

        if (code1 != RetCode.Success) {
            return null;
        }

        if (outNbElement1.value > 0) {

            Map<Date, BBands> dataList = new LinkedHashMap<Date, BBands>(requiredDays);
            BBands bBands;
            int lengthOfTradeDates = tradeDates.length;

            for (int i = outNbElement1.value - 1; i >= 0; i--) {
                lengthOfTradeDates--;

                if (requiredDays != -1 && dataList.size() == requiredDays) {
                    break;
                }

                bBands = new BBands();
                bBands.setPeriod(optInTimePeriod);
                bBands.setSymbol(symbol);
                bBands.setTradeDate(tradeDates[lengthOfTradeDates]);
                bBands.setUpperBand(ImageLinks.getRoundedValue(outUpper[i],2));
                bBands.setMiddleBand(ImageLinks.getRoundedValue(outMiddle[i],2));
                bBands.setLowerBand(ImageLinks.getRoundedValue(outLower[i],2));

                dataList.put(tradeDates[lengthOfTradeDates], bBands);
            }
            return dataList;
        }

        return null;

    }

    public static Map<Date, Double> calculateBBandWidthForRange(String symbol, int optInTimePeriod, double upMultiplier, double downMultiplier, float[] closeValues, Date[] tradeDates, int requiredDays) {
        Core core = new Core();

        float inReal[] = closeValues;


        int total_days_data = inReal.length;

        int startIdx1 = optInTimePeriod;
        int endIdx1 = total_days_data - 1;


        MInteger outBegIdx1 = new MInteger();
        MInteger outNbElement1 = new MInteger();
        double outUpper[] = new double[inReal.length];
        double outMiddle[] = new double[inReal.length];
        double outLower[] = new double[inReal.length];

        RetCode code1 = core.bbands(startIdx1, endIdx1, inReal, optInTimePeriod, upMultiplier, downMultiplier, MAType.Sma, outBegIdx1, outNbElement1, outUpper, outMiddle, outLower);

        if (code1 != RetCode.Success) {
            return null;
        }

        if (outNbElement1.value > 0) {

            Map<Date, Double> dataList = new LinkedHashMap<Date, Double>(requiredDays);
            int lengthOfTradeDates = tradeDates.length;

            for (int i = outNbElement1.value - 1; i >= 0; i--) {
                lengthOfTradeDates--;

                if (requiredDays != -1 && dataList.size() == requiredDays) {
                    break;
                }

                if(closeValues[lengthOfTradeDates] > 0) {
                    dataList.put(tradeDates[lengthOfTradeDates], ImageLinks.getRoundedValue(((outUpper[i] - outLower[i]) / closeValues[lengthOfTradeDates]) * 100));
                }
            }
            return dataList;
        }

        return null;

    }

    public static Map<Date, Macd> calculateMacdForRange(String symbol, int optInFastPeriod, int optInSlowPeriod, int optInSignalPeriod, float[] closeValues, Date[] tradeDates, int requiredDays) {

        Core core = new Core();
        int minNOD = optInSlowPeriod - 1 + optInSignalPeriod - 1 + 1;

        float inReal[] = closeValues;

        int total_days_data = inReal.length;
        int startIdxM = minNOD;
        //int endIdxM =startIdxM;
        int endIdxM = total_days_data - 1;
        MInteger outBegIdxA = new MInteger();
        MInteger outNbElementA = new MInteger();
        double outMACD[] = new double[inReal.length];
        double outMACDSignal[] = new double[inReal.length];
        double outMACDHist[] = new double[inReal.length];
        RetCode code = core.macd(startIdxM, endIdxM, inReal, optInFastPeriod, optInSlowPeriod, optInSignalPeriod, outBegIdxA, outNbElementA, outMACD, outMACDSignal, outMACDHist);

        if (code != RetCode.Success) {
            return null;
        }

        if (outNbElementA.value > 0) {

            Map<Date, Macd> dataList = new LinkedHashMap<Date, Macd>(requiredDays);
            Macd macd;
            int lengthOfTradeDates = tradeDates.length;

            for (int i = outNbElementA.value - 1; i >= 0; i--) {
                lengthOfTradeDates--;

                if (requiredDays != -1 && dataList.size() == requiredDays) {
                    break;
                }

                macd = new Macd();
                macd.setFastPeriod(optInFastPeriod);
                macd.setSignalPeriod(optInSignalPeriod);
                macd.setSlowPeriod(optInSlowPeriod);

                macd.setSymbol(symbol);
                macd.setTradeDate(tradeDates[lengthOfTradeDates]);

                macd.setMacd(ImageLinks.getRoundedValue(outMACD[i],2));
                macd.setMacdSignal(ImageLinks.getRoundedValue(outMACDSignal[i],2));
                macd.setMacdHistogram(ImageLinks.getRoundedValue(outMACDHist[i],2));

                dataList.put(tradeDates[lengthOfTradeDates], macd);
            }
            return dataList;
        }

        return null;
    }

    public static Map<Date, Stoch> calculateStochForRange(String symbol, int optInFastK_Period, int optInSlowK_Period, int optInSlowD_Period, float[] highValues, float[] lowValues, float[] closeValues, Date[] tradeDates, int requiredDays) {

        Core core = new Core();

        float inHigh[] = highValues;
        float inLow[] = lowValues;
        float inClose[] = closeValues;

        int total_days_data = inHigh.length;
//		System.out.println(inHigh.length);


        int startIdx = optInFastK_Period + optInSlowK_Period + optInSlowK_Period - 3;
        int endIdx = total_days_data - 1;


        MInteger outBegIdxA = new MInteger();
        MInteger outNBElement = new MInteger();
        double outSlowK[] = new double[inHigh.length];
        double outSlowD[] = new double[inHigh.length];


        // This also can be configured - Will do LAter //AMIT
        MAType optInSlowK_MAType = MAType.Sma;
        MAType optInSlowD_MAType = MAType.Sma;


        RetCode code = core.stoch(startIdx, endIdx, inHigh, inLow, inClose, optInFastK_Period, optInSlowK_Period, optInSlowK_MAType, optInSlowD_Period, optInSlowD_MAType, outBegIdxA, outNBElement, outSlowK, outSlowD);
        if (code != RetCode.Success) {
            return null;
        }

        if (outNBElement.value > 0) {

            Map<Date, Stoch> dataList = new LinkedHashMap<Date, Stoch>(requiredDays);
            Stoch stoch;
            int lengthOfTradeDates = tradeDates.length;

            for (int i = outNBElement.value - 1; i >= 0; i--) {
                lengthOfTradeDates--;

                if (requiredDays != -1 && dataList.size() == requiredDays) {
                    break;
                }

                stoch = new Stoch();
                stoch.setFastkPeriod(optInFastK_Period);
                stoch.setSlowdPeriod(optInSlowD_Period);
                stoch.setSlowkPeriod(optInSlowK_Period);

                stoch.setSymbol(symbol);
                stoch.setTradeDate(tradeDates[lengthOfTradeDates]);

                stoch.setSlowK(ImageLinks.getRoundedValue((outSlowK[i]), 2));
                stoch.setSlowD(ImageLinks.getRoundedValue((outSlowD[i]), 2));

                dataList.put(tradeDates[lengthOfTradeDates], stoch);
            }
            return dataList;
        }

        return null;

    }

    public static Map<Date, MovingAverage> calculateWMAForRange(String symbol, int optInTimePeriod, int type, int averageOf, float[] values, Date[] tradeDates, int requiredDays) {
        Core core = new Core();

        float inReal[] = values;

        int total_days_data = inReal.length;


        int startIdx1 = optInTimePeriod - 1;
        int endIdx1 = total_days_data - 1;


        MInteger outBegIdx1 = new MInteger();
        MInteger outNbElement1 = new MInteger();
        double outReal1[] = new double[inReal.length];

        RetCode code1 = null;
        if (type == MovingAverage.TYPE_SMA) {
            code1 = core.sma(startIdx1, endIdx1, inReal, optInTimePeriod, outBegIdx1, outNbElement1, outReal1);
        } else if (type == MovingAverage.TYPE_EMA) {
            code1 = core.ema(startIdx1, endIdx1, inReal, optInTimePeriod, outBegIdx1, outNbElement1, outReal1);
        } else if (type == MovingAverage.TYPE_WMA) {
            code1 = core.wma(startIdx1, endIdx1, inReal, optInTimePeriod, outBegIdx1, outNbElement1, outReal1);
        }

        if (code1 == null || code1 != RetCode.Success) {
            return null;
        }

        if (outNbElement1.value > 0) {

            Map<Date, MovingAverage> dataList = new LinkedHashMap<Date, MovingAverage>(requiredDays);
            MovingAverage movingAverage;
            int lengthOfTradeDates = tradeDates.length;

            for (int i = outNbElement1.value - 1; i >= 0; i--) {
                lengthOfTradeDates--;

                if (requiredDays != -1 && dataList.size() == requiredDays) {
                    break;
                }

                movingAverage = new MovingAverage();
                movingAverage.setPeriod(optInTimePeriod);
                movingAverage.setSymbol(symbol);
                movingAverage.setTradeDate(tradeDates[lengthOfTradeDates]);
                movingAverage.setAverageOf(averageOf);
                movingAverage.setType(type);
                movingAverage.setAverageValue(ImageLinks.getRoundedValue(outReal1[i],3));

                dataList.put(tradeDates[lengthOfTradeDates], movingAverage);
            }
            return dataList;
        }
        return null;


    }

    public static Map<Date, Obv> calculateOBV(String symbol, float[] closeValues, float[] volumeValues, Date[] tradeDates,int requiredDays) throws StrategyLabsException {

            Core core = new Core();

            float inReal[] = closeValues;
            float inVolume[] = volumeValues;

            if(inReal.length == 0) {
                return null;
            }
            int startIdx1 = 0;
            int endIdx1 = inReal.length - 1;


            MInteger outBegIdx1 = new MInteger();
            MInteger outNbElement1 = new MInteger();
            double outReal1[] = new double[inReal.length];

            RetCode code1 = core.obv(startIdx1, endIdx1, inReal, inVolume, outBegIdx1, outNbElement1, outReal1);

            if (outNbElement1.value > 0) {

                Map<Date, Obv> dataList = new LinkedHashMap<Date, Obv>(requiredDays);
                Obv obv;
                int lengthOfTradeDates = tradeDates.length;

                for (int i = outNbElement1.value - 1; i >= 0; i--) {
                    lengthOfTradeDates--;

                    if (requiredDays != -1 && dataList.size() == requiredDays) {
                        break;
                    }

                    obv = new Obv();
                    obv.setSymbol(symbol);
                    obv.setObv(ImageLinks.getRoundedValue(outReal1[i], 2));
                    obv.setTradeDate(tradeDates[lengthOfTradeDates]);

                    dataList.put(tradeDates[lengthOfTradeDates],obv);
                }
                return dataList;
            }

        return  null;
    }

    public static Map<Date, Trix> calculateTrix(String symbol, int optInTimePeriod, float[] closeValues, Date[] tradeDates,int requiredDays) throws StrategyLabsException {

        Core core = new Core();

        float inReal[] = closeValues;

        if(inReal.length == 0) {
            return null;
        }
        int startIdx1 = 0;
        int endIdx1 = inReal.length - 1;


        MInteger outBegIdx1 = new MInteger();
        MInteger outNbElement1 = new MInteger();
        double outReal1[] = new double[inReal.length];

        RetCode code1 = core.trix(startIdx1, endIdx1, inReal, optInTimePeriod, outBegIdx1, outNbElement1, outReal1);

        if (outNbElement1.value > 0) {

            Map<Date, Trix> dataList = new LinkedHashMap<Date, Trix>(requiredDays);
            Trix trix;
            int lengthOfTradeDates = tradeDates.length;

            for (int i = outNbElement1.value - 1; i >= 0; i--) {
                lengthOfTradeDates--;

                if (requiredDays != -1 && dataList.size() == requiredDays) {
                    break;
                }

                trix = new Trix();
                trix.setSymbol(symbol);
                trix.setTrix((float) ImageLinks.getRoundedValue(outReal1[i], 3));
                trix.setTradeDate(tradeDates[lengthOfTradeDates]);
                trix.setPeriod(optInTimePeriod);

                dataList.put(tradeDates[lengthOfTradeDates],trix);
            }
            return dataList;
        }

        return  null;
    }

    public static Map<Date, Float> calculateRoc(String symbol, int optInTimePeriod,float[] closeValues, Date[] tradeDates,int requiredDays) throws StrategyLabsException {

        Core core = new Core();

        float inReal[] = closeValues;

        if(inReal.length == 0) {
            return null;
        }
        int startIdx1 = 0;
        int endIdx1 = inReal.length - 1;


        MInteger outBegIdx1 = new MInteger();
        MInteger outNbElement1 = new MInteger();
        double outReal1[] = new double[inReal.length];

        RetCode code1 = core.roc(startIdx1, endIdx1, inReal, optInTimePeriod, outBegIdx1, outNbElement1, outReal1);

        if (outNbElement1.value > 0) {

            Map<Date, Float> dataList = new LinkedHashMap<Date, Float>(requiredDays);
            int lengthOfTradeDates = tradeDates.length;

            for (int i = outNbElement1.value - 1; i >= 0; i--) {
                lengthOfTradeDates--;

                if (requiredDays != -1 && dataList.size() == requiredDays) {
                    break;
                }

                dataList.put(tradeDates[lengthOfTradeDates],((Double)ImageLinks.getRoundedValue(outReal1[i], 2)).floatValue());
            }
            return dataList;
        }

        return  null;
    }

    public static Map<Date, FastStoch> calculateFastStochForRange(String symbol, int optInFastK_Period, int optInFastD_Period, float[] highValues, float[] lowValues, float[] closeValues, Date[] tradeDates, int requiredDays) {

        Core core = new Core();

        float inHigh[] = highValues;
        float inLow[] = lowValues;
        float inClose[] = closeValues;

        int total_days_data = inHigh.length;
//		System.out.println(inHigh.length);


        int startIdx = optInFastK_Period + optInFastD_Period - 3;
        int endIdx = total_days_data - 1;


        MInteger outBegIdxA = new MInteger();
        MInteger outNBElement = new MInteger();
        double outFastK[] = new double[inHigh.length];
        double outFastD[] = new double[inHigh.length];


        // This also can be configured - Will do LAter //AMIT
        MAType optInFastD_MAType = MAType.Sma;


        RetCode code = core.stochF(startIdx, endIdx, inHigh, inLow, inClose, optInFastK_Period, optInFastD_Period, optInFastD_MAType, outBegIdxA, outNBElement, outFastK, outFastD);
        if (code != RetCode.Success) {
            return null;
        }

        if (outNBElement.value > 0) {

            Map<Date, FastStoch> dataList = new LinkedHashMap<Date, FastStoch>(requiredDays);
            FastStoch fastStoch;
            int lengthOfTradeDates = tradeDates.length;

            for (int i = outNBElement.value - 1; i >= 0; i--) {
                lengthOfTradeDates--;

                if (requiredDays != -1 && dataList.size() == requiredDays) {
                    break;
                }

                fastStoch = new FastStoch();
                fastStoch.setFastKPeriod(optInFastK_Period);
                fastStoch.setFastDPeriod(optInFastD_Period);
                fastStoch.setSymbol(symbol);
                fastStoch.setTradeDate(tradeDates[lengthOfTradeDates]);

                fastStoch.setFastK(ImageLinks.getRoundedValue((outFastK[i]), 2));
                fastStoch.setFastD(ImageLinks.getRoundedValue((outFastD[i]), 2));

                dataList.put(tradeDates[lengthOfTradeDates], fastStoch);
            }
            return dataList;
        }

        return null;

    }

}
