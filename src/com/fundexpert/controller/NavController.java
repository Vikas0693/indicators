package com.fundexpert.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Holidays;
import com.fundexpert.dao.Nav;

public class NavController
{
	public double[] getNav(long mfId, int nod, Date tillDate)
	{
		Session hSession = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			// List<EveryDayCandle> edcList = hSession.createQuery("select TodaysClosePrice from EveryDayCandle where mutualfundId=? and tradeDate<=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, tillDate).setParameter(2, 1).setMaxResults(nod).list();

			/*Iterator itr = edcList.iterator();
			List<Double> price = new ArrayList<Double>();
			while (itr.hasNext())
			{
				EveryDayCandle edc = (EveryDayCandle) itr.next();
				price.add(edc.getTodaysClosePrice());
				// System.out.println(na.getTradeDate() + " DESCENDING :  " + na.getNav());
			}*/
			List<Double> price = hSession.createQuery("select todaysClosePrice from EveryDayCandle where mutualfundId=? and tradeDate<=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, tillDate).setParameter(2, 1).setMaxResults(nod).list();
			Collections.reverse(price);
			double nav[] = price.stream().mapToDouble(i -> i).toArray();
			/*for (double n : nav)
			{
				System.out.println(n);
			}
			System.out.println("---------------------------------------------------------");*/
			return nav;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}

	public double[] getWeeklyNav(long mfId, int nod, Date tillDate)
	{
		Session hSession = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			/*List<EveryDayCandle> edcList = hSession.createQuery("from EveryDayCandle where mutualfundId=? and tradeDate<=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, tillDate).setParameter(2, 0).setMaxResults(nod).list();
			// Double nav[] = navPrice.toArray(new Double[navPrice.size()]);
			Iterator itr = edcList.iterator();
			List<Double> price = new ArrayList<Double>();
			while (itr.hasNext())
			{
				EveryDayCandle edc = (EveryDayCandle) itr.next();
				price.add(edc.getTodaysClosePrice());
				// System.out.println(na.getTradeDate() + " DESCENDING :  " + na.getNav());
			}*/
			List<Double> price = hSession.createQuery("select todaysClosePrice from EveryDayCandle where mutualfundId=? and tradeDate<=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, tillDate).setParameter(2, 0).setMaxResults(nod).list();
			Collections.reverse(price);
			double nav[] = price.stream().mapToDouble(i -> i).toArray();

			/*for (double n : nav)
			{
				System.out.println(n);
			}*/

			return nav;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}

	public double[] getWeeklyHigh(long mfId, int nod, Date tillDate)
	{
		Session hSession = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			/*List<EveryDayCandle> edcList = hSession.createQuery("from EveryDayCandle where mutualfundId=? and tradeDate<=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, tillDate).setParameter(2, 0).setMaxResults(nod).list();
			// Double nav[] = navPrice.toArray(new Double[navPrice.size()]);
			Iterator itr = edcList.iterator();
			List<Double> highNav = new ArrayList<Double>();
			while (itr.hasNext())
			{
				EveryDayCandle edc = (EveryDayCandle) itr.next();
				highNav.add(edc.getHigh());
				// System.out.println(na.getTradeDate() + " DESCENDING :  " + na.getNav());
			}*/
			List<Double> highNav = hSession.createQuery("select high from EveryDayCandle where mutualfundId=? and tradeDate<=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, tillDate).setParameter(2, 0).setMaxResults(nod).list();
			Collections.reverse(highNav);
			double high[] = highNav.stream().mapToDouble(i -> i).toArray();
			/*	for (double n : nav)
				{
					System.out.println(n);
				}*/
			return high;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}

	public double[] getWeeklyLow(long mfId, int nod, Date tillDate)
	{
		Session hSession = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			/*List<EveryDayCandle> edcList = hSession.createQuery("from EveryDayCandle where mutualfundId=? and tradeDate<=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, tillDate).setParameter(2, 0).setMaxResults(nod).list();
			// Double nav[] = navPrice.toArray(new Double[navPrice.size()]);
			Iterator itr = edcList.iterator();
			List<Double> lowNav = new ArrayList<Double>();
			while (itr.hasNext())
			{
				EveryDayCandle edc = (EveryDayCandle) itr.next();
				lowNav.add(edc.getLow());
				// System.out.println(na.getTradeDate() + " DESCENDING :  " + na.getNav());
			}*/
			List<Double> lowNav = hSession.createQuery("select low from EveryDayCandle where mutualfundId=? and tradeDate<=? and candleType=? order by tradeDate desc").setParameter(0, mfId).setParameter(1, tillDate).setParameter(2, 0).setMaxResults(nod).list();
			Collections.reverse(lowNav);
			double low[] = lowNav.stream().mapToDouble(i -> i).toArray();
			/*	for (double n : nav)
				{
					System.out.println(n);
				}*/
			return low;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}

	public int getWorkingDaysBetween(long mfId, Date fromDate, Date tillDate)
	{
		Session hSession = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			int nod = ((Long) hSession.createQuery("select count(*) from EveryDayCandle where mutualfundId=? and tradeDate>=? and tradeDate<? and candleType=?").setParameter(0, mfId).setParameter(1, fromDate).setParameter(2, tillDate).setParameter(3, 1).uniqueResult()).intValue();
			return nod;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return 0;
		}
		finally
		{
			hSession.close();
		}
	}

	public int getWeeklyWorkingDaysBetween(long mfId, Date fromDate, Date tillDate)
	{
		Session hSession = null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			int nod = ((Long) hSession.createQuery("select count(*) from EveryDayCandle where mutualfundId=? and tradeDate>=? and tradeDate<=? and candleType=?").setParameter(0, mfId).setParameter(1, fromDate).setParameter(2, tillDate).setParameter(3, 0).uniqueResult()).intValue();
			return nod;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return 0;
		}
		finally
		{
			hSession.close();
		}
	}

	public JSONArray getMissedNavJson()
	{
		Session hSession=null;
		try
		{
			JSONObject jsonObject=null;
			JSONArray jsonArray=new JSONArray();
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Object> idList=hSession.createQuery("select mf.id,mf.amfiiCode,max(n.tradeDate) from MutualFund mf,Nav n where mf.id=n.mutualfundId and mf.category=1 and mf.optionType=1 and mf.direct!=1 and n.tradeDate<'2017-09-01' group by mf.id").list();
			
			Long mfId;
			String amfiiCode;
			Date maxDate;
			//Date sDate=null;
			Iterator itr=idList.iterator();
			while(itr.hasNext())
			{
				Object[] obj=(Object[])itr.next();
				mfId=(Long)obj[0];
				amfiiCode=(String)obj[1];
				maxDate=(Date)obj[2];
				jsonObject=new JSONObject();
				//System.out.println(sDate);
				jsonObject.put("sDate",new SimpleDateFormat("yyyy-MM-dd").format(maxDate));
				jsonObject.put("amfiiCode", amfiiCode.toString());
				jsonObject.put("eDate",new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
				jsonArray.put(jsonObject);
			} 
			//System.out.println(jsonObject.toString(1)+"\n\n\ndone");
			return jsonArray;
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}

	/*public JSONArray updateTempNav(JSONArray jsonArray) throws Exception
	{
		Session hSession=null;
		Transaction tx=null;
		JSONObject jsonObject=null;
		Long amfiiCode=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			
			for(int i=0;i<jsonArray.length();i++)
			{
				tx=hSession.beginTransaction();
				jsonObject=jsonArray.getJSONObject(i);
				if(!jsonObject.has("amfiiCode"))
					continue;
				amfiiCode=jsonObject.getLong("amfiiCode");
				List mutualFundIdList=hSession.createQuery("select id from MutualFund where amfiiCode=?").setParameter(0, String.valueOf(amfiiCode)).list();
				if(mutualFundIdList!=null)
				{
					JSONArray dataArray=jsonObject.getJSONArray("data");
					for(int j=0;j<dataArray.length();j++)
					{
						JSONObject navJson=dataArray.getJSONObject(j);
						String d=navJson.getString("navDate");
						Date date=new SimpleDateFormat("yyyy-MM-dd").parse(d);
						Double nav=navJson.getDouble("nav");
				
						Iterator itr=mutualFundIdList.iterator();
						while(itr.hasNext())
						{					
							Long id=(Long)itr.next();
							TempNav tempNav=new TempNav();
							
							tempNav.setTradeDate(date);
							tempNav.setNav(nav);
							tempNav.setMutualfundId(id);
							//System.out.println(id+"   "+date+"   "+nav);
							hSession.saveOrUpdate(tempNav);
						}					
					}
				}
				tx.commit();
			}		
			return updateNavFromTempNav();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
		
	}
	
	//update Nav table from TempNav and return amfiiCode of those which have missing dates in between 
	public JSONArray updateNavFromTempNav() throws Exception
	{
		Session hSession=null;
		Transaction tx=null;
		JSONObject jsonObject=null;
		try
		{
			System.out.println("Opening Session from updateNavFromTempNav");
			JSONArray missedDateJsonArray=new JSONArray();
			JSONObject missedDateJsonObject=null;
			hSession=HibernateBridge.getSessionFactory().openSession();
			List mfList=hSession.createQuery("select distinct mutualfundId from TempNav").list();
			Iterator itr=mfList.iterator();
			while(itr.hasNext())
			{
				Date currentDate,previousDate=null;
				Long mfId=(Long)itr.next();
				System.out.println("mfId="+mfId);
				List tempNavList=hSession.createQuery("from TempNav where mutualfundId=? order by tradeDate").setParameter(0,mfId).list();
				boolean missedDateFlag=false;
				tx=hSession.beginTransaction();
				Iterator itr1=tempNavList.iterator();
				while(itr1.hasNext())
				{
					TempNav tempNav=(TempNav)itr1.next();
					currentDate=tempNav.getTradeDate();
					Calendar calendar;
					if(previousDate!=null)
					{
						calendar=Calendar.getInstance();
						calendar.setTime(previousDate);
						calendar.add(Calendar.DAY_OF_MONTH, 1);
						if(calendar.getTime().compareTo(currentDate)==0)
						{
							Nav nav=new Nav();
							nav.setMutualfundId(tempNav.getMutualfundId());
							nav.setNav(tempNav.getNav());
							nav.setTradeDate(tempNav.getTradeDate());
							hSession.saveOrUpdate(nav);
							previousDate=currentDate;
						}
						else
						{
							//since currentDate is not previousDate+1 we need to check for holiday previousDate+1 in which case we have already set previousDate = previousDate+1
							long milliSec=currentDate.getTime()-previousDate.getTime();
							long days=(milliSec/(1000*60*60*24))-1;
							for(int i=0;i<days;i++)
							{
								Calendar newCalendar=Calendar.getInstance();
								newCalendar.setTime(previousDate);
								newCalendar.add(Calendar.DAY_OF_MONTH,1);
								previousDate=newCalendar.getTime();
								Holidays previousDateHoliday=(Holidays)hSession.createQuery("from Holidays where holiday=?").setParameter(0, previousDate).uniqueResult();
								if(previousDateHoliday==null)
								{
									String day=new SimpleDateFormat("EEEEE").format(previousDate);
									if(day.equalsIgnoreCase("Saturday") || day.equalsIgnoreCase("Sunday"))
									{
										continue;
									}
									missedDateJsonObject=new JSONObject();
									String amfiiCode=(String)hSession.createQuery("select amfiiCode from MutualFund where id=? and isRobo=1").setParameter(0, mfId).setMaxResults(1).uniqueResult();
									missedDateJsonObject.put("date",new SimpleDateFormat("yyyy-MM-dd").format(previousDate));
									missedDateJsonObject.put("amfiiCode",amfiiCode);
									missedDateJsonArray.put(missedDateJsonObject);
									
								}									
							}
							
							Nav nav=new Nav();
							nav.setMutualfundId(tempNav.getMutualfundId());
							nav.setNav(tempNav.getNav());
							nav.setTradeDate(tempNav.getTradeDate());
							hSession.saveOrUpdate(nav);
							previousDate=currentDate;
						}						
					}
					else
					{
						Nav nav=new Nav();
						nav.setMutualfundId(tempNav.getMutualfundId());
						nav.setNav(tempNav.getNav());
						nav.setTradeDate(tempNav.getTradeDate());
						hSession.saveOrUpdate(nav);
						previousDate=currentDate;					
					}
					
				}
															
			}			
			return missedDateJsonArray;
		}
		catch(Exception e)
		{
			//e.printStackTrace();
			if(tx!=null)
				tx.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}
*/
	public void updateNav(JSONArray jsonArray,String amfii) throws Exception
	{
		Session hSession=null;
		Transaction tx=null;
		JSONObject jsonObject=null;
		
		try
		{
			Long amfiiCode=Long.valueOf(amfii);
			hSession=HibernateBridge.getSessionFactory().openSession();			
			tx=hSession.beginTransaction();				
			List mutualFundIdList=hSession.createQuery("select id from MutualFund where amfiiCode=?").setParameter(0, String.valueOf(amfiiCode)).list();
			if(mutualFundIdList!=null)
			{
				JSONObject navJson=null;
				for(int j=0;j<jsonArray.length();j++)
				{
					navJson=jsonArray.getJSONObject(j);
					String d=navJson.getString("date");
					Date date=new SimpleDateFormat("yyyy-MM-dd").parse(d);
					Double navPrice=navJson.getDouble("nav");
			
					Iterator itr=mutualFundIdList.iterator();
					while(itr.hasNext())
					{					
						Long id=(Long)itr.next();
						Nav nav=new Nav();
						
						nav.setTradeDate(date);
						nav.setNav(navPrice);
						nav.setMutualfundId(id);
						System.out.println(id+"   "+date+"   "+navPrice);
						hSession.saveOrUpdate(nav);
					}					
				}
			}
			tx.commit();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
		
	}
}
