package com.fundexpert.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.validation.ValidationException;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Consumer;
import com.fundexpert.dao.Sessionn;

public class LoginController 
{
	public Long checkLoginDetails(String appId, String secretKey, String ipAddress)
	{
		Long consumerId=null;
		Session hSession=HibernateBridge.getSessionFactory().openSession();
		try{
			Consumer c=(Consumer)hSession.createQuery("from Consumer where appId=? and secretKey=?")
					.setString(0, appId).setString(1, secretKey).uniqueResult();
			if(c!=null)
			{
				consumerId=c.getId();
				if(c.isIpHardcoded())
				{
					if(c.getIpAddress().equals(ipAddress) && c.getSecretKey().equals(secretKey))
						return consumerId;
				}
				else if(!c.isIpHardcoded())
				{
					if(c.getSecretKey().equals(secretKey))
						return consumerId;
				}
			}
			else
				throw new ValidationException("Not a valid Consumer! OR Enter Valid AppId and SecretKey .");
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			hSession.close();
		}
		return consumerId;
	}
	
	public String getSessionClient(String ipAddress, Long consumerId) throws ParseException
	{
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String sessionId="";
		
		Calendar cal=Calendar.getInstance();
		String str=sdf.format(cal.getTime());
		java.util.Date d=sdf.parse(str);
		java.sql.Date dateDB = new java.sql.Date(d.getTime());
		
		Calendar limitCal=Calendar.getInstance();
		Calendar nowCal=Calendar.getInstance();
		nowCal.add(Calendar.MINUTE, -15);
		System.out.println(limitCal.getTime()+", 	Now Cal before 15 minutes: "+sdf.format(nowCal.getTime()));
		
		Session session=HibernateBridge.getSessionFactory().openSession();
		Transaction tx=null;
		try{
			tx=session.beginTransaction();
			Sessionn sess=(Sessionn) session.createQuery("from Sessionn where ipAddress=? and consumerId=? order by id DESC")
					.setString(0, ipAddress).setLong(1, consumerId).setMaxResults(1).uniqueResult();
			// List<Sessionn> list=session.createQuery("from Sessionn where consumerId=?").setLong(0, consumerId).list();
			
			if(sess!=null)
			{
				System.out.println("inside session!=null : "+sess.getId()+",  "+sess.getLastUsedOn());
				if((sess.getLastUsedOn()).compareTo(nowCal.getTime())>0)
				{	
					System.out.println("Inside Loop.");
					sessionId= sess.getSessionId();
					sess.setLastUsedOn(dateDB);
					session.update(sess);
					tx.commit();		
					System.out.println("Sessionn updated Successfully." +sess.getId());	
					// System.out.println("loop SI: "+sess.getSessionId()+", CI "+sess.getConsumerId()+", Co: "+sess.getCreatedOn()+", LUO: "+sess.getLastUsedOn());
				}
				else if((sess.getLastUsedOn()).compareTo(nowCal.getTime())<=0)
				{
					Sessionn s=new Sessionn();
					s.setConsumerId(consumerId);
					s.setIpAddress(ipAddress);
					
					s.setCreatedOn(dateDB);
					s.setLastUsedOn(dateDB);
					
					UUID uuid=UUID.randomUUID();
					sessionId=uuid.toString().substring(0,8);
					s.setSessionId(sessionId);
					session.save(s);
					 tx.commit();
					System.out.println("Sesionn Saved Successfully, SID : "+sessionId+",  "+s.getId());
				}
			}
			else
			{
				Sessionn s=new Sessionn();
				s.setConsumerId(consumerId);
				s.setIpAddress(ipAddress);
				
				s.setCreatedOn(dateDB);
				s.setLastUsedOn(dateDB);
				
				UUID uuid=UUID.randomUUID();
				sessionId=uuid.toString().substring(0,8);
				s.setSessionId(sessionId);
				session.save(s);
				tx.commit();
				System.out.println("Sesionn Saved Successfully, and Session Id : "+sessionId+",  "+s.getId());
			}
		}
		catch(ValidationException e)
		{
			tx.rollback();
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return sessionId;
	}
}
			/* Sessionn ses=(Sessionn) session.createQuery("from Sessionn where ipAddress=? and consumerId=? and lastUsedOn>=? and lastUsedOn<=? order by id")
						.setString(0, ipAddress).setLong(1, consumerId).setDate(2, nowCal.getTime()).setDate(3, limitCal.getTime()).setMaxResults(1).uniqueResult();			
				//	ipAddress=? and consumerId=? and      lastUsedOn >= now() - interval 15 minute  			//
			if(ses!=null)
			{		
				System.out.println(ses.getSessionId()+",  "+ses.getLastUsedOn());
				sessionId= ses.getSessionId();
				ses.setLastUsedOn(dateDB);
				session.update(ses);
				//tx.commit();		
				System.out.println("Sessionn updated Successfully." +ses.getId());	
			}
			else
			{
				Sessionn s=new Sessionn();
				s.setConsumerId(consumerId);
				s.setIpAddress(ipAddress);
				
				s.setCreatedOn(dateDB);
				s.setLastUsedOn(dateDB);
				
				UUID uuid=UUID.randomUUID();
				sessionId=uuid.toString().substring(0,8);
				s.setSessionId(sessionId);
				session.save(s);
				//tx.commit();
				System.out.println("Sesionn Saved Successfully, and Session Id : "+sessionId+",  "+s.getId());
			}*/
		