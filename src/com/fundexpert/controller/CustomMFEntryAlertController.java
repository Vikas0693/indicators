package com.fundexpert.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.CustomMFEntryAlert;
import com.fundexpert.dao.CustomMFExitAlert;
import com.fundexpert.dao.EveryDayCandle;
import com.fundexpert.dao.FlatPatterns;
import com.fundexpert.dao.Fractal;
import com.fundexpert.dao.Nav;
import com.robo.config.Config;

public class CustomMFEntryAlertController {

	public List<CustomMFEntryAlert> getEntryAlerts(long mutualFundId,String sDate,String eDate)
	{
		Session hSession=null;
		
		try
		{
			Date startDate=new SimpleDateFormat("yyyy-MM-dd").parse(sDate);
			Date endDate=new SimpleDateFormat("yyyy-MM-dd").parse(eDate);
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<CustomMFEntryAlert> entryList=hSession.createQuery("from CustomMFEntryAlert where mutualFundId=? and tradeDate between ? and ? and strategyName in (:list) order by tradeDate").setParameter(0, mutualFundId).setParameter(1, startDate).setParameter(2, endDate).setParameterList("list", getValidEntryList(mutualFundId,hSession)).list();
			return entryList;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
	
	
	List getValidEntryList(long mutualFundId,Session hSession)
	{
		String entry="";
		List<String> entryList=new ArrayList<String>();
		try
		{
			Config config=new Config();
			entry=config.getProperty("entry");
			
			if(!entry.isEmpty())
			{		
				String[] entryArray=entry.split(",");
				for(int i=0;i<entryArray.length;i++)
				{
					entryArray[i]="EntryStrategy"+entryArray[i];
				}
				Collections.addAll(entryList,entryArray);
				return entryList;
			}	
			else
			{
				return entryList;
			}			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public void updateAllEntryAlertsAfterDate() throws Exception
	{		
		//we do not want to catch the exception here bcoz some strategies are dependent on others
		updateEntry1AfterDate();
		//updateEntry2AfterDate();
		updateEntry3AfterDate();
		updateEntry4AfterDate();
		updateEntry5AfterDate();
		updateEntry6AfterDate();
		updateEntry7AfterDate();
		updateEntry8AfterDate();
		updateEntry9AfterDate();
		updateEntry10AfterDate();
		updateEntry11AfterDate();
		updateEntry12AfterDate();
		updateEntry13AfterDate();
		updateEntry14AfterDate();
		updateEntry15AfterDate();
		updateEntry16AfterDate();
		
	}


	private void updateEntry16AfterDate() throws Exception {
		Session hSession=null;
		Transaction transaction=null;
		System.out.println("Updating entry 16");
		try
		{
			String strategyName="EntryStrategy16";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			while(iterator.hasNext())
			{
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				
				hSession=HibernateBridge.getSessionFactory().openSession();
				transaction=hSession.beginTransaction();
				Date previousDate=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy16'").setParameter(0,mutualFundId).uniqueResult();
				List alerts=hSession.createQuery("from CustomMFEntryAlert where mutualFundId=? and (strategyName='EntryStrategy6' or strategyName='EntryStrategy11' or strategyName='EntryStrategy12' or strategyName='EntryStrategy13') and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,previousDate).list();
				for(int i=0;i<alerts.size();i++)
				{
					CustomMFEntryAlert entryAlert=(CustomMFEntryAlert)alerts.get(i);
					Date alertDate=entryAlert.getTradeDate();
					Calendar cal=Calendar.getInstance();
					cal.setTime(alertDate);
					cal.add(Calendar.MONTH,-4);
					//System.out.println("previousDate="+previousDate+"Current="+alertDate+" previous4MonthDate="+cal.getTime()+"AlertType="+entryAlert.getStrategyName());
					
					//System.out.println("List"+alert16Within6Month+"mf="+mutualFundId);
					
					
					if(previousDate!=null)
					{
						if(previousDate.compareTo(cal.getTime())<0)
						{
							System.out.println("Entry 16 Alert for mfid="+mutualFundId+" on alertDate="+alertDate);
							CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
							customMFEntryAlert.setMutualFundId(mutualFundId);
							customMFEntryAlert.setStrategyName(strategyName);
							customMFEntryAlert.setTradeDate(alertDate);
							customMFEntryAlert.setEntryPrice(entryAlert.getEntryPrice());
							customMFEntryAlert.setReasonForAlert(1);
							hSession.saveOrUpdate(customMFEntryAlert);
							previousDate=alertDate;
						}
					}
					else
					{
						System.out.println("Entry 16 alert found for mfid="+mutualFundId+" on alertDate="+alertDate+" with previousDate="+previousDate);
						CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
						customMFEntryAlert.setMutualFundId(mutualFundId);
						customMFEntryAlert.setStrategyName(strategyName);
						customMFEntryAlert.setTradeDate(alertDate);
						customMFEntryAlert.setEntryPrice(entryAlert.getEntryPrice());
						customMFEntryAlert.setReasonForAlert(1);
						hSession.saveOrUpdate(customMFEntryAlert);
						previousDate=alertDate;
					}			

				}
				transaction.commit();
				
			}	
			BufferedWriter bw=null;
			try
			{
				File f=new File("/opt/strategy");
	        	if(!f.isDirectory())
	        	{
	        		f.mkdirs();
	        	}
	        	
	        	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
				String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
				System.out.println("Saving entry 16");
				bw.write(entry);
				bw.newLine();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				bw.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(transaction!=null)
				transaction.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}


	private void updateEntry15AfterDate() throws Exception {
		Session hSession=null;
		Transaction transaction=null;
		System.out.println("Updating entry 15");
		try {
				String strategyName="EntryStrategy15";
				MutualFundController mfc=new MutualFundController();
				List<Object[]> mfList=mfc.getMutualFundList();
				Iterator iterator=mfList.iterator();
				int count=0;
				hSession=HibernateBridge.getSessionFactory().openSession();
				while(iterator.hasNext())
				{				
					transaction=hSession.beginTransaction();
					Object object[]=(Object[])iterator.next();
					Long mutualFundId=(Long)object[0];
					Date previousAlertDate=null;
					Date lastEntry15Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy15'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
					List exit1Alert=null;
					if(lastEntry15Date==null)
					{
						exit1Alert=hSession.createQuery("from CustomMFExitAlert where strategyName='ExitStrategy1' and mutualFundId=?").setParameter(0,mutualFundId).list();
					}
					else
					{
						exit1Alert=hSession.createQuery("from CustomMFExitAlert where strategyName='ExitStrategy1' and mutualFundId=? and tradeDate>=?").setParameter(0,mutualFundId).setParameter(1,lastEntry15Date).list();
					}
					
					
					for(int i=0;i<exit1Alert.size();i++)
					{
						CustomMFExitAlert alert1=(CustomMFExitAlert)exit1Alert.get(i);
						double exit1Price=alert1.getExitPrice();
						Date alert1Date=alert1.getTradeDate();
						if(previousAlertDate!=null)
						{
							if(alert1Date.compareTo(previousAlertDate)<0)
								continue;
						}
						Calendar cal=Calendar.getInstance();
						cal.setTime(alert1Date);
						cal.add(Calendar.MONTH,4);
						
						List navFor4Months=hSession.createQuery("from Nav where mutualfundId=? and tradeDate between ? and ? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,alert1Date).setParameter(2,cal.getTime()).list();
						if(navFor4Months!=null)
						{
							for(int j=0;j<navFor4Months.size();j++)
							{
								Nav nav=(Nav)navFor4Months.get(j);
								if(nav.getNav()>exit1Price*1.03)
								{
									
									System.out.println("EntryStrategy15	"+mutualFundId+"\t"+new SimpleDateFormat("yyyy-MM-dd").format(nav.getTradeDate())+"\t1\t"+nav.getNav());
									CustomMFEntryAlert customMFEntryAlert = new CustomMFEntryAlert();
	                                customMFEntryAlert.setMutualFundId(mutualFundId);
	                                customMFEntryAlert.setStrategyName(strategyName);
	                                customMFEntryAlert.setTradeDate(nav.getTradeDate());
	                                customMFEntryAlert.setReasonForAlert(1);
	                                customMFEntryAlert.setEntryPrice(nav.getNav());
	                               	hSession.saveOrUpdate(customMFEntryAlert);
	                               	previousAlertDate=nav.getTradeDate();
	                               	break;
								}
							}
						}
					}				
					transaction.commit();
				}
	            BufferedWriter bw=null;
	            try
	            {
	            	File f=new File("/opt/strategy");
	            	if(!f.isDirectory())
	            	{
	            		f.mkdirs();
	            	}
	            	
	            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
	            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	            	System.out.println("Saving enntry 15");
	            	bw.write(entry);
	            	bw.newLine();
	            }
	            catch(Exception e)
	            {
	            	e.printStackTrace();
	            }
	            finally
	            {
	            	bw.close();
	            }
	        } 
			catch (Exception e)
			{
				e.printStackTrace();
				if(transaction!=null)
					transaction.rollback();
				throw new Exception(e.getMessage());
	        }
			finally
			{
				hSession.close();
			}
	}


	private void updateEntry14AfterDate() throws Exception {
		Session hSession=null;
		Transaction tx=null;
		System.out.println("Updating entry 14");
		try {
				String strategyName="EntryStrategy14";
			
				MutualFundController mfc=new MutualFundController();
				List<Object[]> mfList=mfc.getMutualFundList();
				Iterator iterator=mfList.iterator();
				int count=0;
				hSession=HibernateBridge.getSessionFactory().openSession();
				while(iterator.hasNext())
				{				
					Object object[]=(Object[])iterator.next();
					Long mutualFundId=(Long)object[0];
					List entry6Alerts=null;
					tx=hSession.beginTransaction();
					Date lastEntry14Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy14'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
					if(lastEntry14Date==null)
					{
						entry6Alerts=hSession.createQuery("from CustomMFEntryAlert where strategyName = 'EntryStrategy6' and mutualFundId=?").setParameter(0,mutualFundId).list();
					}
					else
					{
						entry6Alerts=hSession.createQuery("from CustomMFEntryAlert where strategyName = 'EntryStrategy6' and mutualFundId=? and tradeDate>=?").setParameter(0,mutualFundId).setParameter(1,lastEntry14Date).list();
					}
						
					if(entry6Alerts!=null && entry6Alerts.size()>1)
					{
						for(int i=1;i<entry6Alerts.size();i++)
						{
							CustomMFEntryAlert alert6Current=(CustomMFEntryAlert)entry6Alerts.get(i);
							Date currentAlertDate=alert6Current.getTradeDate();
							CustomMFEntryAlert alert6Previous=(CustomMFEntryAlert)entry6Alerts.get(i-1);
							Date previousAlertDate=alert6Previous.getTradeDate();
							Calendar cal=Calendar.getInstance();
							cal.setTime(currentAlertDate);
							cal.add(Calendar.MONTH,-2);
							Date previous2MonthDate=cal.getTime();
							if(previous2MonthDate.compareTo(previousAlertDate)>0)
							{
								System.out.println("Entry 14 Alert for= "+mutualFundId+" tradeDate="+currentAlertDate);
								CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
								customMFEntryAlert.setMutualFundId(mutualFundId);
								customMFEntryAlert.setStrategyName(strategyName);
								customMFEntryAlert.setTradeDate(currentAlertDate);
								customMFEntryAlert.setEntryPrice(alert6Current.getEntryPrice());
								customMFEntryAlert.setReasonForAlert(1);
								hSession.saveOrUpdate(customMFEntryAlert);
							}						
						}
					}
					tx.commit();
	            }
	            BufferedWriter bw=null;
	            try
	            {
	            	File f=new File("/opt/strategy");
	            	if(!f.isDirectory())
	            	{
	            		f.mkdirs();
	            	}
	            	
	            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
	            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	            	System.out.println("Saving entry 14");
	            	bw.write(entry);
	            	bw.newLine();
	            }
	            catch(Exception e)
	            {
	            	e.printStackTrace();
	            	
	            }
	            finally
	            {
	            	bw.close();
	            }
	        } 
			catch (Exception e)
			{
				e.printStackTrace();
				if(tx!=null)
					tx.rollback();
				throw new Exception(e.getMessage());
	        }
			finally
			{
				hSession.close();
			}		
	}


	private void updateEntry13AfterDate() throws Exception {
		Session hSession=null;
		Transaction transaction=null;
		System.out.println("Updating entry 13");
		try
		{
			String strategyName="EntryStrategy13";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			while(iterator.hasNext())
			{
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				
				hSession=HibernateBridge.getSessionFactory().openSession();
				transaction=hSession.beginTransaction();
			
				List custom8EntryAlertList=null;
				Date lastEntry13Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy13'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
				
				if(lastEntry13Date==null)
				{
					custom8EntryAlertList=hSession.createQuery("from CustomMFEntryAlert where strategyName like '%EntryStrategy8%' and mutualFundId=?").setParameter(0,mutualFundId).list();
				}
				else
				{
					custom8EntryAlertList=hSession.createQuery("from CustomMFEntryAlert where strategyName like '%EntryStrategy8%' and mutualFundId=? and tradeDate>=?").setParameter(0,mutualFundId).setParameter(1,lastEntry13Date).list();
				}
				

				if(custom8EntryAlertList!=null)
				{
					//System.out.println("CustomListSize for mutualFundId="+mutualFundId+" ="+custom8EntryAlertList.size());
					Iterator itr=custom8EntryAlertList.iterator();
					//the point of doing this(below if clause) is: before first enrtyStrategy10 there is no another entryStratrgy10 alert so skip 1st entry10 alert
					if(itr.hasNext())
					{
						itr.next();
					}
					while(itr.hasNext())
					{
						CustomMFEntryAlert entry8Alert=(CustomMFEntryAlert)itr.next();
						Date currentEntry8Date=entry8Alert.getTradeDate();
						Date previousEntry8Date=(Date)hSession.createQuery("select tradeDate from CustomMFEntryAlert where strategyName like '%EntryStrategy8%' and mutualFundId=? and tradeDate<? order by tradeDate desc").setParameter(0,mutualFundId).setParameter(1,currentEntry8Date).setMaxResults(1).uniqueResult();
						if(previousEntry8Date!=null)
						{
							List exit5AlertList=hSession.createQuery("from CustomMFExitAlert where strategyName like '%ExitStrategy5%' and mutualFundId=? and tradeDate between ? and ?").setParameter(0,mutualFundId).setParameter(1,previousEntry8Date).setParameter(2,currentEntry8Date).list();
							if(exit5AlertList!=null && !(exit5AlertList.isEmpty()))
							{
								CustomMFExitAlert alert5=(CustomMFExitAlert)exit5AlertList.get(0);
								System.out.println("Entry 13 alert for ="+mutualFundId+"  on ="+currentEntry8Date);
								CustomMFEntryAlert alert13=new CustomMFEntryAlert();
								alert13.setMutualFundId(mutualFundId);
								alert13.setTradeDate(currentEntry8Date);
								alert13.setStrategyName(strategyName);
								alert13.setEntryPrice(entry8Alert.getEntryPrice());
								alert13.setReasonForAlert(1);
								hSession.saveOrUpdate(alert13);
							}
						}
					}				
				}		
				hSession.flush();
				transaction.commit();
			}	
			BufferedWriter bw=null;
			try
			{
				File f=new File("/opt/strategy");
	        	if(!f.isDirectory())
	        	{
	        		f.mkdirs();
	        	}
	        	
	        	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
				String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
				System.out.println("Saving entry 13");
				bw.write(entry);
				bw.newLine();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				bw.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(transaction!=null)
				transaction.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}


	private void updateEntry12AfterDate() throws Exception {
		Session hSession=null;
		Transaction transaction=null;
		System.out.println("Updating entry 12");
		try
		{
			String strategyName="EntryStrategy12";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			while(iterator.hasNext())
			{
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				
				hSession=HibernateBridge.getSessionFactory().openSession();
				transaction=hSession.beginTransaction();

				Date lastEntry12Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy12'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();

				List custom10EntryAlertList=null;
				if(lastEntry12Date==null)
				{
					custom10EntryAlertList=hSession.createQuery("from CustomMFEntryAlert where strategyName like '%EntryStrategy10%' and mutualFundId=?").setParameter(0,mutualFundId).list();
				}
				else
				{
					custom10EntryAlertList=hSession.createQuery("from CustomMFEntryAlert where strategyName like '%EntryStrategy10%' and mutualFundId=? and tradeDate>=?").setParameter(0,mutualFundId).setParameter(1,lastEntry12Date).list();
				}
				

				if(custom10EntryAlertList!=null)
				{
					Iterator itr=custom10EntryAlertList.iterator();
					//the point of doing this(below if clause) is: before first enrtyStrategy10 there is no another entryStratrgy10 alert so skip 1st entry10 alert
					if(itr.hasNext())
					{
						itr.next();
					}
					while(itr.hasNext())
					{
						CustomMFEntryAlert entry10Alert=(CustomMFEntryAlert)itr.next();
						Date currentEntry10Date=entry10Alert.getTradeDate();
						Date previousEntry10Date=(Date)hSession.createQuery("select tradeDate from CustomMFEntryAlert where strategyName like '%EntryStrategy10%' and mutualFundId=? and tradeDate<? order by tradeDate desc").setParameter(0,mutualFundId).setParameter(1,currentEntry10Date).setMaxResults(1).uniqueResult();
						if(previousEntry10Date!=null)
						{
							List exit5AlertList=hSession.createQuery("from CustomMFExitAlert where strategyName like '%ExitStrategy5%' and mutualFundId=? and tradeDate between ? and ?").setParameter(0,mutualFundId).setParameter(1,previousEntry10Date).setParameter(2,currentEntry10Date).list();
							if(exit5AlertList!=null && !(exit5AlertList.isEmpty()))
							{
								CustomMFExitAlert alert5=(CustomMFExitAlert)exit5AlertList.get(0);
								System.out.println("Entry 12 alert for ="+mutualFundId+"  on ="+currentEntry10Date+"  previous10AlertDate="+previousEntry10Date+"  alert5Date="+alert5.getTradeDate());
								CustomMFEntryAlert alert12=new CustomMFEntryAlert();
								alert12.setMutualFundId(mutualFundId);
								alert12.setTradeDate(currentEntry10Date);
								alert12.setStrategyName(strategyName);
								alert12.setEntryPrice(entry10Alert.getEntryPrice());
								alert12.setReasonForAlert(1);
								hSession.saveOrUpdate(alert12);
							}
						}
					}
					
				}		
				hSession.flush();
				transaction.commit();
			}	
			BufferedWriter bw=null;
			try
			{
				File f=new File("/opt/strategy");
	        	if(!f.isDirectory())
	        	{
	        		f.mkdirs();
	        	}
	        	
	        	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
				String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
				System.out.println("Saving entry 12");
				bw.write(entry);
				bw.newLine();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				bw.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(transaction!=null)
				transaction.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}

	}


	private void updateEntry11AfterDate() throws Exception {
		Session hSession=null;
		Transaction transaction=null;
		System.out.println("Updating entry 11");
		try
		{
			String strategyName="EntryStrategy11";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			while(iterator.hasNext())
			{
				List strategy9List=null;
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				
				hSession=HibernateBridge.getSessionFactory().openSession();
				transaction=hSession.beginTransaction();
				Date lastEntry11Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy11'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
				if(lastEntry11Date==null)
				{
					strategy9List=hSession.createQuery("from CustomMFEntryAlert where mutualFundId=? and strategyName like ? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,"%EntryStrategy9%").list();
				}
				else
				{
					strategy9List=hSession.createQuery("from CustomMFEntryAlert where mutualFundId=? and strategyName like ? and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,"%EntryStrategy9%").setParameter(2,lastEntry11Date).list();
				}

				if(strategy9List!=null)
				{
					for(int i=1;i<strategy9List.size();i++)
					{
						double diff=0;
						CustomMFEntryAlert entryAlert0=(CustomMFEntryAlert)strategy9List.get(i-1);
						
						CustomMFEntryAlert entryAlert1=(CustomMFEntryAlert)strategy9List.get(i);
					
						diff=entryAlert1.getTradeDate().getTime()-entryAlert0.getTradeDate().getTime();
						if((diff/(1000*60*60*24))>30.0)
						{
							System.out.println("Entry 11 alert for  mfId="+mutualFundId+"  on"+entryAlert1.getTradeDate());
							CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
							customMFEntryAlert.setStrategyName(strategyName);
							customMFEntryAlert.setMutualFundId(mutualFundId);
							customMFEntryAlert.setTradeDate(entryAlert1.getTradeDate());
							customMFEntryAlert.setEntryPrice(entryAlert1.getEntryPrice());
							customMFEntryAlert.setReasonForAlert(1);
							hSession.saveOrUpdate(customMFEntryAlert);
						}
					}
				}
				hSession.flush();
				transaction.commit();
			
			}
			BufferedWriter bw=null;
			try
			{
				File f=new File("/opt/strategy");
	        	if(!f.isDirectory())
	        	{
	        		f.mkdirs();
	        	}
	        	
	        	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
				String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
				System.out.println("Saving entry 11");
				bw.write(entry);
				bw.newLine();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				bw.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(transaction!=null)
				transaction.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}

	}


	private void updateEntry10AfterDate() throws Exception {
		Session hSession=null;
		Transaction transaction=null;
		System.out.println("Updating entry 10");
		try
		{
			String strategyName="EntryStrategy10";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			while(iterator.hasNext())
			{
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				
				hSession=HibernateBridge.getSessionFactory().openSession();
				Date lastEntry10Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy10'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
				if(lastEntry10Date==null)
				{
					lastEntry10Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=0 and period=2").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				}
				if(lastEntry10Date==null)
					continue;
				List<Fractal> fractalList=hSession.createQuery("from Fractal where mutualFundId=? and period=2 and candleType=0 and tcfl1!=? and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,0d).setParameter(2,lastEntry10Date).list();
				transaction=hSession.beginTransaction();
				if(fractalList!=null && fractalList.size()!=0)
				{
					for(int i=2;i<fractalList.size();i++)
					{
						Fractal currentFractal=fractalList.get(i);
						double currentClosePrice=currentFractal.getClosePrice();
						Date currentTradeDate=currentFractal.getTradeDate();
						if(currentClosePrice>currentFractal.getTcfl1() && currentClosePrice>currentFractal.getTcfl2() && currentClosePrice>currentFractal.getTcfl3())
						{
							Fractal previousFractal=fractalList.get(i-1);
							double previousClosePrice=previousFractal.getClosePrice();
							if(previousFractal.getTcfl1()!=0 && previousClosePrice>previousFractal.getTcfl1() && previousClosePrice>previousFractal.getTcfl2() && previousClosePrice>previousFractal.getTcfl3())
							{
								Fractal pPreviousFractal=fractalList.get(i-2);
								double pPreviousClosePrice=pPreviousFractal.getClosePrice();
								double maxOfTCFL=Math.max(Math.max(pPreviousFractal.getTcfl1(),pPreviousFractal.getTcfl2()),pPreviousFractal.getTcfl3());
								if(pPreviousClosePrice<maxOfTCFL)
								{
										System.out.println("Entry 10 alert for ="+mutualFundId+"  on ="+currentTradeDate);
										CustomMFEntryAlert customMFEntryAlert = new CustomMFEntryAlert();
										customMFEntryAlert.setMutualFundId(mutualFundId);
										customMFEntryAlert.setStrategyName(strategyName);
										customMFEntryAlert.setTradeDate(currentTradeDate);
										customMFEntryAlert.setReasonForAlert(1);
										customMFEntryAlert.setEntryPrice(currentClosePrice);
		                               	hSession.saveOrUpdate(customMFEntryAlert);								
								}
							}
						}
					}
				}
				hSession.flush();
				transaction.commit();
			
			}
			BufferedWriter bw=null;
			try
			{
				File f=new File("/opt/strategy");
	        	if(!f.isDirectory())
	        	{
	        		f.mkdirs();
	        	}
	        	
	        	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
				String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
				System.out.println("Saving entry 10");
				bw.write(entry);
				bw.newLine();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				bw.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(transaction!=null)
				transaction.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}


	private void updateEntry9AfterDate() throws Exception {
		Session hSession=null;
		Transaction transaction=null;
		System.out.println("Updating entry 9");
		try
		{
			String strategyName="EntryStrategy9";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			while(iterator.hasNext())
			{
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				
				hSession=HibernateBridge.getSessionFactory().openSession();
				Date lastEntry9Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy9'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
				if(lastEntry9Date==null)
				{
					lastEntry9Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=1 and period=2").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				}
				if(lastEntry9Date==null)
					continue;
				
				List<Fractal> fractalList=hSession.createQuery("from Fractal where mutualFundId=? and period=2 and candleType=1 and tcfl1!=? and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,0d).setParameter(2,lastEntry9Date).list();
				transaction=hSession.beginTransaction();
				if(fractalList!=null && fractalList.size()!=0)
				{
					for(int i=2;i<fractalList.size();i++)
					{
						Fractal currentFractal=fractalList.get(i);
						double currentClosePrice=currentFractal.getClosePrice();
						Date currentTradeDate=currentFractal.getTradeDate();
						if(currentClosePrice>currentFractal.getTcfl1() && currentClosePrice>currentFractal.getTcfl2() && currentClosePrice>currentFractal.getTcfl3())
						{
							Fractal previousFractal=fractalList.get(i-1);
							double previousClosePrice=previousFractal.getClosePrice();
							if(previousFractal.getTcfl1()!=0 && previousClosePrice>previousFractal.getTcfl1() && previousClosePrice>previousFractal.getTcfl2() && previousClosePrice>previousFractal.getTcfl3())
							{
								Fractal pPreviousFractal=fractalList.get(i-2);
								double pPreviousClosePrice=pPreviousFractal.getClosePrice();
								double maxOfTCFL=Math.max(Math.max(pPreviousFractal.getTcfl1(),pPreviousFractal.getTcfl2()),pPreviousFractal.getTcfl3());
								if(pPreviousClosePrice<maxOfTCFL)
								{
										System.out.println("Entry 9 alert for ="+mutualFundId+"  on ="+currentTradeDate);
										CustomMFEntryAlert customMFEntryAlert = new CustomMFEntryAlert();
										customMFEntryAlert.setMutualFundId(mutualFundId);
										customMFEntryAlert.setStrategyName(strategyName);
										customMFEntryAlert.setTradeDate(currentTradeDate);
										customMFEntryAlert.setReasonForAlert(1);
										customMFEntryAlert.setEntryPrice(currentClosePrice);
		                               	hSession.saveOrUpdate(customMFEntryAlert);								
								}
							}
						}
					}
				}
				hSession.flush();
				transaction.commit();
			
			}
			BufferedWriter bw=null;
			try
			{
				File f=new File("/opt/strategy");
	        	if(!f.isDirectory())
	        	{
	        		f.mkdirs();
	        	}
	        	
	        	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
				String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
				System.out.println("Saving entry 9");
				bw.write(entry);
				bw.newLine();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				bw.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(transaction!=null)
				transaction.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}


	private void updateEntry8AfterDate() throws Exception {
		Session hSession=null;
		Transaction tx=null;
		System.out.println("Updating entry 8");
		try {
				String strategyName="EntryStrategy8";
			
				MutualFundController mfc=new MutualFundController();
				List<Object[]> mfList=mfc.getMutualFundList();
				Iterator iterator=mfList.iterator();
				int count=0;
				hSession=HibernateBridge.getSessionFactory().openSession();
				while(iterator.hasNext())
				{				
					Object object[]=(Object[])iterator.next();
					Long mutualFundId=(Long)object[0];
					Date lastEntry8Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy8'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
					if(lastEntry8Date==null)
					{
						lastEntry8Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=0 and period=2").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
					}
					if(lastEntry8Date==null)
						continue;
					List<Fractal> weeklyTcflCandleList = hSession.createQuery("from Fractal where mutualFundId=? and candleType=0 and period=2 and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,lastEntry8Date).list();
					//List<Fractal> weeklyUpFractalCandleList = HibernateConnection.getFractalStrategy(mutualFundId,0,1);
					
					
					tx=hSession.beginTransaction();
					for(int i=1;i<weeklyTcflCandleList.size();i++)
					{
						Fractal currentTcflFractal=(Fractal)weeklyTcflCandleList.get(i);
						//Fractal currentUpFractal=(Fractal)weeklyUpFractalCandleList.get(i);
						
						Date currentTradeDate=currentTcflFractal.getTradeDate();
						double closePrice=currentTcflFractal.getClosePrice();
										
						Double currentTcfl1=currentTcflFractal.getTcfl1();
						Double currentTcfl2=currentTcflFractal.getTcfl2();
						Double currentTcfl3=currentTcflFractal.getTcfl3(); 
						
						//double upFractal=currentUpFractal.getUpFractal();
						
						boolean flatPatternSSBFound=false;
						boolean flatPatternTCFL1Found=false;
						boolean flatPatternTCFL3Found=false;
						//System.out.println("0 condition="+currentTradeDate+"mf="+mutualFundId);
						if(currentTcfl1!=null  && closePrice<currentTcfl1 && closePrice<currentTcfl2 && closePrice<currentTcfl3)
						{		
							//System.out.println("First condition="+currentTradeDate+"mf="+mutualFundId);
							List previous2DaysPeriod1List=hSession.createQuery("from Fractal where mutualFundId=? and period=? and candleType=? and tradeDate<=? order by tradeDate desc").setParameter(0,mutualFundId).setParameter(1,1).setParameter(2,0).setParameter(3, currentTradeDate).setMaxResults(2).list();
							Fractal currentFractalWithPeriod1=(Fractal)previous2DaysPeriod1List.get(0);
							Fractal previousFractalWithPeriod1=(Fractal)previous2DaysPeriod1List.get(1);
							if(currentFractalWithPeriod1.getTradeDate().compareTo(currentTradeDate)!=0)
							{
								throw new Exception("currentTradeDate does not exist in fractal of period 1");
							}
							if(currentFractalWithPeriod1.getClosePrice()>currentFractalWithPeriod1.getUpFractal() &&  previousFractalWithPeriod1.getClosePrice()<previousFractalWithPeriod1.getUpFractal())
							{											
								//System.out.println("Second condition="+currentTradeDate+"mf="+mutualFundId);
								Calendar calendar=Calendar.getInstance();
								calendar.setTime(currentTradeDate);
								calendar.add(Calendar.MONTH,-8);
								Date previousEightMonthDate=calendar.getTime(); 
								
								List flatPatternBeforeTradeDateSsb=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"SSB").setParameter(1,mutualFundId).setParameter(2,15).setParameter(3, 0).setParameter(4,currentTradeDate).setMaxResults(1).list();
								if(flatPatternBeforeTradeDateSsb.size()!=0)
								{
									FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateSsb.get(0);
									Date lastFlatPatternDate=flatPattern.getPatternStartDate();
									
									if(lastFlatPatternDate.compareTo(previousEightMonthDate)<0)
									{
										flatPatternSSBFound=true;
									//	System.out.println("pattern start date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
									}
									else
									{
										double ssb=flatPattern.getPatternValue();
										if(ssb>(closePrice*1.05))
										{
											//System.out.println("ssb true "+mutualFundId+"tradeDate="+tradeDate);
											//System.out.println("pattern date found(Nav checked) alert for mf="+mutualFundId+" for "+tradeDate+"  lastFlatDate="+lastFlatPatternDate);
											flatPatternSSBFound=true;
										}
										else
										{
											//System.out.print("ssb pattern date found ( nav not verified)");
											flatPatternSSBFound=false;
											continue;
										}
									}
								}
								else
								{
									//flatPatternSSBFound=false;
									//System.out.println("No pattern date found alert for mf="+mutualFundId+" for "+tradeDate);
									flatPatternSSBFound=true;
								}
								List flatPatternBeforeTradeDateTcfl1=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"TCFL1").setParameter(1,mutualFundId).setParameter(2,15).setParameter(3, 0).setParameter(4,currentTradeDate).setMaxResults(1).list();
								if(flatPatternSSBFound=true)
								{
									if(flatPatternBeforeTradeDateTcfl1.size()!=0)
									{
										FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateTcfl1.get(0);
										Date lastFlatPatternDate=flatPattern.getPatternStartDate();
										
										if(lastFlatPatternDate.compareTo(previousEightMonthDate)<0)
										{
											flatPatternTCFL1Found=true;
											//System.out.println("pattern end date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
										}
										else
										{
											double tcfl1=flatPattern.getPatternValue();
											if(tcfl1>(closePrice*1.05))
											{
												//System.out.println("tcfl1 true "+mutualFundId+"tradeDate="+tradeDate);
												flatPatternTCFL1Found=true;
											}
											else
											{
												flatPatternTCFL1Found=false;
												continue;
											}
										}	
									}
									else
									{
										flatPatternTCFL1Found=true;
									}
								}
								else
								{
									continue;
								}
								List flatPatternBeforeTradeDateTcfl3=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"TCFL3").setParameter(1,mutualFundId).setParameter(2,15).setParameter(3,0).setParameter(4,currentTradeDate).setMaxResults(1).list();
								if(flatPatternSSBFound=true && flatPatternTCFL1Found==true)
								{
									if(flatPatternBeforeTradeDateTcfl3.size()!=0)
									{
										FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateTcfl3.get(0);
										Date lastFlatPatternDate=flatPattern.getPatternStartDate();
										
										if(lastFlatPatternDate.compareTo(previousEightMonthDate)<0)
										{
											flatPatternTCFL3Found=true;
											//System.out.println("tcfl 3 pattern end date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
										}
										else
										{
											double tcfl3=flatPattern.getPatternValue();
											if(tcfl3>(closePrice*1.05))
											{
												//System.out.println("tcfl3 true "+mutualFundId+"tradeDate="+tradeDate);
												flatPatternTCFL3Found=true;
											}
											else
											{
												flatPatternTCFL3Found=false;
												continue;
											}
										}
									}
									else
									{
										//System.out.print("tcfl 3 no pattern date found  tradeDate="+tradeDate);
										flatPatternTCFL3Found=true;
									}
								}
								else
								{								
									continue;
								} 								
								if(flatPatternSSBFound==true && flatPatternTCFL1Found==true && flatPatternTCFL3Found==true)
								{
									System.out.println("Entry 8 Alert="+mutualFundId+" currentTradeDate="+currentTradeDate);
									CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
									customMFEntryAlert.setMutualFundId(mutualFundId);
									customMFEntryAlert.setStrategyName(strategyName);
									customMFEntryAlert.setTradeDate(currentTradeDate);
									customMFEntryAlert.setEntryPrice(closePrice);
									customMFEntryAlert.setReasonForAlert(1);
									hSession.saveOrUpdate(customMFEntryAlert);
									//System.out.println(mutualFundId+" \t\t "+currentFractal.getTradeDate());
								}
								
							}
						}	
					}	
					tx.commit();
	            }
	            BufferedWriter bw=null;
	            try
	            {
	            	File f=new File("/opt/strategy");
	            	if(!f.isDirectory())
	            	{
	            		f.mkdirs();
	            	}
	            	
	            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
	            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	            	System.out.println("saving entry 8");
	            	bw.write(entry);
	            	bw.newLine();
	            }
	            catch(Exception e)
	            {
	            	e.printStackTrace();
	            }
	            finally
	            {
	            	bw.close();
	            }
	        } 
			catch (Exception e)
			{
				e.printStackTrace();
				if(tx!=null)
					tx.rollback();
				throw new Exception(e.getMessage());
	        }
			finally
			{
				hSession.close();
			}		
	}


	private void updateEntry7AfterDate() throws Exception {
		Session hSession=null;
		Transaction tx=null;
		System.out.println("Updating entry 7");
		try {
				String strategyName="EntryStrategy7";
			
				MutualFundController mfc=new MutualFundController();
				List<Object[]> mfList=mfc.getMutualFundList();
				Iterator iterator=mfList.iterator();
				int count=0;
				hSession=HibernateBridge.getSessionFactory().openSession();
				while(iterator.hasNext())
				{				
					Object object[]=(Object[])iterator.next();
					Long mutualFundId=(Long)object[0];
					
					//System.out.println("mf="+mutualFundId);
					Date lastEntry7Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy7'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
					if(lastEntry7Date==null)
					{
						lastEntry7Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=0 and period=2").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
					}
					if(lastEntry7Date==null)
						continue;
					List<Fractal> dailyCandleList = hSession.createQuery("from Fractal where mutualFundId=? and candleType=0 and period=2 and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,lastEntry7Date).list();
					
					tx=hSession.beginTransaction();
					for(int i=1;i<dailyCandleList.size();i++)
					{
						Fractal currentFractal=(Fractal)dailyCandleList.get(i);
						Date currentTradeDate=currentFractal.getTradeDate();
						double closePrice=currentFractal.getClosePrice();
						double upFractal=currentFractal.getUpFractal();
						Double tcfl1=currentFractal.getTcfl1();
						Double tcfl2=currentFractal.getTcfl2();
						Double tcfl3=currentFractal.getTcfl3();
						
						if(tcfl1!=null  && closePrice<tcfl1 && closePrice<tcfl2 && closePrice<tcfl3)
						{
							List previous2DaysPeriod1List=hSession.createQuery("from Fractal where mutualFundId=? and period=? and candleType=? and tradeDate<=? order by tradeDate desc").setParameter(0,mutualFundId).setParameter(1,1).setParameter(2,0).setParameter(3, currentTradeDate).setMaxResults(2).list();
							Fractal currentFractalWithPeriod1=(Fractal)previous2DaysPeriod1List.get(0);
							Fractal previousFractalWithPeriod1=(Fractal)previous2DaysPeriod1List.get(1);
							
							if(currentFractalWithPeriod1.getTradeDate().compareTo(currentTradeDate)!=0)
							{
								throw new Exception("currentTradeDate does not exist in fractal of period 1 for="+mutualFundId+" currentTradeDate"+currentTradeDate);
							}
							if(currentFractalWithPeriod1.getClosePrice()>currentFractalWithPeriod1.getUpFractal() &&  previousFractalWithPeriod1.getClosePrice()<previousFractalWithPeriod1.getUpFractal())
							{
								CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
								customMFEntryAlert.setMutualFundId(mutualFundId);
								customMFEntryAlert.setStrategyName(strategyName);
								customMFEntryAlert.setTradeDate(currentTradeDate);
								customMFEntryAlert.setEntryPrice(closePrice);
								customMFEntryAlert.setReasonForAlert(1);
								hSession.saveOrUpdate(customMFEntryAlert);
								System.out.println("Entry 7 alert "+mutualFundId+" \t\t "+currentFractal.getTradeDate());
							}
						}	
					}	
					tx.commit();
	            }
	            BufferedWriter bw=null;
	            try
	            {
	            	File f=new File("/opt/strategy");
	            	if(!f.isDirectory())
	            	{
	            		f.mkdirs();
	            	}
	            	
	            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
	            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	            	System.out.println("saving entry 7");
	            	bw.write(entry);
	            	bw.newLine();
	            }
	            catch(Exception e)
	            {
	            	e.printStackTrace();
	            }
	            finally
	            {
	            	bw.close();
	            }
	        } 
			catch (Exception e)
			{
				e.printStackTrace();
	           	if(tx!=null)
	        		tx.rollback();
	           	throw new Exception(e.getMessage());
	        }
			finally
			{
				hSession.close();
			}
	}


	private void updateEntry6AfterDate() throws Exception {
		Session hSession=null;
		Transaction tx=null;
		System.out.println("Updating entry 6");
		try {
				String strategyName="EntryStrategy6";
			
				MutualFundController mfc=new MutualFundController();
				List<Object[]> mfList=mfc.getMutualFundList();
				Iterator iterator=mfList.iterator();
				int count=0;
				hSession=HibernateBridge.getSessionFactory().openSession();
				while(iterator.hasNext())
				{				
					Object object[]=(Object[])iterator.next();
					Long mutualFundId=(Long)object[0];
					//System.out.println("mf="+mutualFundId);
					Date lastEntry6Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy6'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
					if(lastEntry6Date==null)
					{
						lastEntry6Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=1 and period=2").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
					}
					if(lastEntry6Date==null)
						continue;
					List<Fractal> dailyCandleList = hSession.createQuery("from Fractal where mutualFundId=? and candleType=1 and period=2 and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,lastEntry6Date).list();
					
					tx=hSession.beginTransaction();
					for(int i=1;i<dailyCandleList.size();i++)
					{
						Fractal currentFractal=(Fractal)dailyCandleList.get(i);
						double closePrice=currentFractal.getClosePrice();
						double upFractal=currentFractal.getUpFractal();
						Date tradeDate=currentFractal.getTradeDate();
						
						Double currentTcfl1=currentFractal.getTcfl1();
						Double currentTcfl2=currentFractal.getTcfl2();
						Double currentTcfl3=currentFractal.getTcfl3(); 
						
						boolean flatPatternSSBFound=false;
						boolean flatPatternTCFL1Found=false;
						boolean flatPatternTCFL3Found=false;
						//System.out.println("0 condition="+tradeDate+"mf="+mutualFundId);
						if(currentTcfl1!=null  && closePrice<currentTcfl1 && closePrice<currentTcfl2 && closePrice<currentTcfl3 && closePrice>upFractal)
						{
							Fractal previousFractal=dailyCandleList.get(i-1);
							if(previousFractal.getClosePrice()<previousFractal.getUpFractal())
							{
								//System.out.println("Second condition="+tradeDate+"mf="+mutualFundId);
								Calendar calendar=Calendar.getInstance();
								calendar.setTime(tradeDate);
								calendar.add(Calendar.MONTH,-4);
								Date previousFourMonthDate=calendar.getTime(); 
								
								List flatPatternBeforeTradeDateSsb=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"SSB").setParameter(1,mutualFundId).setParameter(2,20).setParameter(3, 1).setParameter(4,tradeDate).setMaxResults(1).list();
								
								if(flatPatternBeforeTradeDateSsb.size()!=0)
								{
									FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateSsb.get(0);
									Date lastFlatPatternDate=flatPattern.getPatternStartDate();
																
									if(lastFlatPatternDate.compareTo(previousFourMonthDate)<0)
									{
										flatPatternSSBFound=true;
									}
									else
									{
										double ssb=flatPattern.getPatternValue();
										if(ssb>(closePrice*1.05))
										{
											//System.out.println("ssb true "+mutualFundId+"tradeDate="+tradeDate);
											//System.out.println("pattern date found(Nav checked) alert for mf="+mutualFundId+" for "+tradeDate+"  lastFlatDate="+lastFlatPatternDate);
											flatPatternSSBFound=true;
										}
										else
										{
											//System.out.print("ssb pattern date found ( nav not verified)");
											flatPatternSSBFound=false;
											continue;
										}
									}
								}
								else
								{
									//flatPatternSSBFound=false;
									//System.out.println("No pattern date found alert for mf="+mutualFundId+" for "+tradeDate);
									flatPatternSSBFound=true;
								}
								List flatPatternBeforeTradeDateTcfl1=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"TCFL1").setParameter(1,mutualFundId).setParameter(2,15).setParameter(3, 1).setParameter(4,tradeDate).setMaxResults(1).list();
								if(flatPatternSSBFound==true)
								{
									if(flatPatternBeforeTradeDateTcfl1.size()!=0)
									{
										FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateTcfl1.get(0);
										Date lastFlatPatternDate=flatPattern.getPatternStartDate();
										
										if(lastFlatPatternDate.compareTo(previousFourMonthDate)<0)
										{
											flatPatternTCFL1Found=true;
											//System.out.println("pattern end date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
										}
										else
										{
											double tcfl1=flatPattern.getPatternValue();
											if(tcfl1>(closePrice*1.05))
											{
												//System.out.println("tcfl1 true "+mutualFundId+"tradeDate="+tradeDate);
												flatPatternTCFL1Found=true;
											}
											else
											{
												flatPatternTCFL1Found=false;
												continue;
											}
										}	
									}
									else
									{
										flatPatternTCFL1Found=true;
									}
								}
								else
								{
									continue;
								}
								List flatPatternBeforeTradeDateTcfl3=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"TCFL3").setParameter(1,mutualFundId).setParameter(2,15).setParameter(3,1).setParameter(4,tradeDate).setMaxResults(1).list();
								if(flatPatternSSBFound==true && flatPatternTCFL1Found==true)
								{
									if(flatPatternBeforeTradeDateTcfl3.size()!=0)
									{
										FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateTcfl3.get(0);
										Date lastFlatPatternDate=flatPattern.getPatternStartDate();
										
										if(lastFlatPatternDate.compareTo(previousFourMonthDate)<0)
										{
											flatPatternTCFL3Found=true;
											//System.out.println("tcfl 3 pattern end date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
										}
										else
										{
											double tcfl3=flatPattern.getPatternValue();
											if(tcfl3>(closePrice*1.05))
											{
												//System.out.println("tcfl3 true "+mutualFundId+"tradeDate="+tradeDate);
												flatPatternTCFL3Found=true;
											}
											else
											{
												flatPatternTCFL3Found=false;
												continue;
											}
										}
									}
									else
									{
										//System.out.print("tcfl 3 no pattern date found  tradeDate="+tradeDate);
										flatPatternTCFL3Found=true;
									}
								}
								else
								{								
									continue;
								} 								
								if(flatPatternSSBFound==true && flatPatternTCFL1Found==true && flatPatternTCFL3Found==true)
								{
									System.out.println("Entry 6 Alert="+mutualFundId+" tradeDate="+tradeDate);
									CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
									customMFEntryAlert.setMutualFundId(mutualFundId);
									customMFEntryAlert.setStrategyName(strategyName);
									customMFEntryAlert.setTradeDate(tradeDate);
									customMFEntryAlert.setEntryPrice(closePrice);
									customMFEntryAlert.setReasonForAlert(1);
									hSession.saveOrUpdate(customMFEntryAlert);
								}							
							}
						}	
					}	
					tx.commit();
	            }
	            BufferedWriter bw=null;
	            try
	            {
	            	File f=new File("/opt/strategy");
	            	if(!f.isDirectory())
	            	{
	            		f.mkdirs();
	            	}
	            	
	            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
	            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	            	System.out.println("Saving entry 6");
	            	bw.write(entry);
	            	bw.newLine();
	            }
	            catch(Exception e)
	            {
	            	e.printStackTrace();
	            }
	            finally
	            {
	            	bw.close();
	            }
	        } 
			catch (Exception e)
			{
				e.printStackTrace();
				if(tx!=null)
					tx.rollback();
				throw new Exception(e.getMessage());
	        }
			finally
			{
				hSession.close();
			}		
	}


	private void updateEntry5AfterDate() throws Exception {
		Session hSession=null;
		Transaction tx=null;
		System.out.println("Updating entry 5");
		try {
				String strategyName="EntryStrategy5";
			
				MutualFundController mfc=new MutualFundController();
				List<Object[]> mfList=mfc.getMutualFundList();
				Iterator iterator=mfList.iterator();
				int count=0;
				hSession=HibernateBridge.getSessionFactory().openSession();
				while(iterator.hasNext())
				{				
					Object object[]=(Object[])iterator.next();
					Long mutualFundId=(Long)object[0];
					Date lastEntry5Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy5'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
					if(lastEntry5Date==null)
					{
						lastEntry5Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=0 and period=2").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
					}
					if(lastEntry5Date==null)
						continue;
					List<Fractal> dailyCandleList = hSession.createQuery("from Fractal where mutualFundId=? and candleType=0 and period=2 and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,lastEntry5Date).list();
					tx=hSession.beginTransaction();
					for(int i=1;i<dailyCandleList.size();i++)
					{
						Fractal currentFractal=(Fractal)dailyCandleList.get(i);
						double closePrice=currentFractal.getClosePrice();
						double upFractal=currentFractal.getUpFractal();
						Double tcfl1=currentFractal.getTcfl1();
						Double tcfl2=currentFractal.getTcfl2();
						Double tcfl3=currentFractal.getTcfl3();
						if(tcfl1!=null  && closePrice<tcfl1 && closePrice<tcfl2 && closePrice<tcfl3 && closePrice>upFractal)
						{
							Fractal previousFractal=dailyCandleList.get(i-1);
							if(previousFractal.getClosePrice()<previousFractal.getUpFractal())
							{
								CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
								customMFEntryAlert.setMutualFundId(mutualFundId);
								customMFEntryAlert.setStrategyName(strategyName);
								customMFEntryAlert.setTradeDate(currentFractal.getTradeDate());
								customMFEntryAlert.setEntryPrice(currentFractal.getClosePrice());
								customMFEntryAlert.setReasonForAlert(1);
								hSession.saveOrUpdate(customMFEntryAlert);
								System.out.println("Entry 5 Alert"+mutualFundId+" \t\t "+currentFractal.getTradeDate());
							}
						}	
					}	
					tx.commit();
	            }
	            BufferedWriter bw=null;
	            try
	            {
	            	File f=new File("/opt/strategy");
	            	if(!f.isDirectory())
	            	{
	            		f.mkdirs();
	            	}
	            	
	            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
	            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	            	System.out.println("Saving entry 5");
	            	bw.write(entry);
	            	bw.newLine();
	            }
	            catch(Exception e)
	            {
	            	e.printStackTrace();
	            }
	            finally
	            {
	            	bw.close();
	            }
	        } 
			catch (Exception e)
			{
	           e.printStackTrace();
	           if(tx!=null)
	        	   tx.rollback();
	           throw new Exception(e.getMessage());
	        }
			finally
			{
				hSession.close();
			}
	}


	private void updateEntry4AfterDate() throws Exception {
		Session hSession=null;
		Transaction tx=null;
		System.out.println("Updating entry 4");
		try {
				String strategyName="EntryStrategy4";
			
				MutualFundController mfc=new MutualFundController();
				List<Object[]> mfList=mfc.getMutualFundList();
				Iterator iterator=mfList.iterator();
				int count=0;
				hSession=HibernateBridge.getSessionFactory().openSession();
				while(iterator.hasNext())
				{				
					Object object[]=(Object[])iterator.next();
					Long mutualFundId=(Long)object[0];
					Date lastEntry4Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy4'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
					if(lastEntry4Date==null)
					{
						lastEntry4Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=1 and period=2").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
					}
					if(lastEntry4Date==null)
						continue;
					List<Fractal> dailyCandleList = hSession.createQuery("from Fractal where mutualFundId=? and candleType=1 and period=2 and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,lastEntry4Date).list();
					
					tx=hSession.beginTransaction();
					for(int i=1;i<dailyCandleList.size();i++)
					{
						Fractal currentFractal=(Fractal)dailyCandleList.get(i);
						double closePrice=currentFractal.getClosePrice();
						double upFractal=currentFractal.getUpFractal();
						Double tcfl1=currentFractal.getTcfl1();
						Double tcfl2=currentFractal.getTcfl2();
						Double tcfl3=currentFractal.getTcfl3();
						
						if(tcfl1!=null  && closePrice<tcfl1 && closePrice<tcfl2 && closePrice<tcfl3 && closePrice>upFractal)
						{
							Fractal previousFractal=dailyCandleList.get(i-1);
							if(previousFractal.getClosePrice()<previousFractal.getUpFractal())
							{
								CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
								customMFEntryAlert.setMutualFundId(mutualFundId);
								customMFEntryAlert.setStrategyName(strategyName);
								customMFEntryAlert.setTradeDate(currentFractal.getTradeDate());
								customMFEntryAlert.setEntryPrice(currentFractal.getClosePrice());
								customMFEntryAlert.setReasonForAlert(1);
								hSession.saveOrUpdate(customMFEntryAlert);
								System.out.println("Entry 4 alert "+mutualFundId+" \t\t "+currentFractal.getTradeDate());
							}
						}	
					}	
					tx.commit();
	            }
	            BufferedWriter bw=null;
	            try
	            {
	            	File f=new File("/opt/strategy");
	            	if(!f.isDirectory())
	            	{
	            		f.mkdirs();
	            	}
	            	
	            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
	            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	            	System.out.println("Saving entry 4");
	            	bw.write(entry);
	            	bw.newLine();
	            }
	            catch(Exception e)
	            {
	            	e.printStackTrace();
	            }
	            finally
	            {
	            	bw.close();
	            }
	        } 
			catch (Exception e)
			{
				e.printStackTrace();
				if(tx!=null)
					tx.rollback();
				throw new Exception(e.getMessage());
	        }
			finally
			{
				hSession.close();
			}
	}


	private void updateEntry3AfterDate() throws Exception {
		Session hSession=null;
		Transaction tx=null;
		System.out.println("Updating entry 3");
		try {
				String strategyName="EntryStrategy3";
				MutualFundController mfc=new MutualFundController();
				List<Object[]> mfList=mfc.getMutualFundList();
				Iterator iterator=mfList.iterator();
				int count=0;
				hSession=HibernateBridge.getSessionFactory().openSession();
				while(iterator.hasNext())
				{				
					tx=hSession.beginTransaction();
					Object object[]=(Object[])iterator.next();
					Long mutualFundId=(Long)object[0];
					Date lastEntry3Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy3'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
					if(lastEntry3Date==null)
					{
						lastEntry3Date=(Date)hSession.createQuery("select min(tradeDate) from EveryDayCandle where mutualFundId=? and candleType=0").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
					}
					if(lastEntry3Date==null)
						continue;
					
					List<EveryDayCandle> weeklyCandleList=hSession.createQuery("from EveryDayCandle where mutualFundId=? and candleType=? and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,0).setParameter(2, lastEntry3Date).list(); 
					// This you need to write on your own, i need Weekly RSI 70 value for the symbol
					// CustomMFExitAlert is the object in which we are persisting Exit dates for different exit startegy 
		            boolean watchActivatedCond1Met = false; // we have to persist this when this code moves to DataController
		            int countWatchCond1 = 0;
		            int countWatchTotalCond1 = 0;
		            boolean watchActivatedCond2 = false;
					
		            int countWatchTotalCond2 = 0;
		
		            if (weeklyCandleList != null && weeklyCandleList.size() != 0) {
		                for (EveryDayCandle weeklyCandle : weeklyCandleList) {
		                	//System.out.println("Mf="+mutualFundId+"\t\t"+weeklyCandle.getRsi());
		                    if (watchActivatedCond2)  // Checking for final condition
		                    {
		                        if (weeklyCandle.getRsi() > 40) {
		                            countWatchTotalCond2++;
		                            if (countWatchTotalCond2 >= 4) {
		                                watchActivatedCond2 = false; //restting again for next check
		                                watchActivatedCond1Met = false;//restting again for next check
		                                // EXIT CONDIITON MET, TO Populate Database
		                                //strategyName - "ExitStrategy1"
		                                // symbol - symbol
		                                // tradeDate - weeklyCandleList.get(index).getTradDate()
		                                //reasonForAlert - "as per ExitStrategy1"
		                                // exitPrice  -  weeklyCandleList.get(index).getClosePrice()
		                                //nftyPrice - for Nifty same as weeklyCandleList.get(index).getClosePrice()
		                                CustomMFEntryAlert customMFEntryAlert = new CustomMFEntryAlert();
		                                customMFEntryAlert.setMutualFundId(mutualFundId);
		                                customMFEntryAlert.setStrategyName(strategyName);
		                                customMFEntryAlert.setTradeDate(weeklyCandle.getTradeDate());
		                                customMFEntryAlert.setReasonForAlert(1);
		                                customMFEntryAlert.setEntryPrice(weeklyCandle.getTodaysClosePrice());
		                                //customMFExitAlert.setNiftyEntryPrice(weeklyCandle.getClosePrice());
		                               	hSession.saveOrUpdate(customMFEntryAlert);
		                                System.out.println("Found Entry 3 alert for="+mutualFundId+"  on "+weeklyCandle.getTradeDate());
		
		                            }
		                        } else {
		                            // Second COunter to be reset
		                            countWatchTotalCond2 = 0;
		                        }
		                    }
		
		                    if (!watchActivatedCond1Met && weeklyCandle.getRsi() < 40) {
		                        countWatchCond1++;
		                        if (countWatchCond1 == 1) {
		                            countWatchTotalCond1 = 0;
		                        } else if (countWatchCond1 >= 3) {
		                            watchActivatedCond2 = true;
		                            watchActivatedCond1Met = true;
		                            countWatchTotalCond1 = 0;
		                            countWatchCond1 = 0;
		                        }
		                    }
		
		                    if (!watchActivatedCond1Met)   // Not 30
		                    {
		                        countWatchTotalCond1++;
		                    }
		
		                    if (!watchActivatedCond1Met && countWatchTotalCond1 >= 4) {
		                        watchActivatedCond1Met = false;
		                        countWatchCond1 = 0;
		                        countWatchTotalCond1 = 0;
		                    }
		
		                }
		            }
		            tx.commit();
	            }
	            BufferedWriter bw=null;
	            try
	            {
	            	File f=new File("/opt/strategy");
	            	if(!f.isDirectory())
	            	{
	            		f.mkdirs();
	            	}
	            	
	            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
	            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	            	System.out.println("Saving entry 3");
	            	bw.write(entry);
	            	bw.newLine();
	            }
	            catch(Exception e)
	            {
	            	e.printStackTrace();
	            }
	            finally
	            {
	            	bw.close();
	            }
	        } 
			catch (Exception e)
			{
	           e.printStackTrace();
	           if(tx!=null)
	        	   tx.rollback();
	           throw new Exception(e.getMessage());
	        }
			finally
			{
				hSession.close();
			}
	}


	private void updateEntry2AfterDate() throws Exception {
		Session hSession=null;
		Transaction tx=null;
		System.out.println("Updating entry 2");
		try
		{
			int count=0;
			String strategyName="EntryStrategy2";
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Long> mfList = hSession.createQuery("select id from MutualFund where id in (select distinct mutualfundId from Nav) and category=1 and optionType=1 order by id desc").list();
			Iterator iterator=mfList.iterator();
			while(iterator.hasNext())
			{				
				tx=hSession.beginTransaction();
				Long mutualFundId=(Long)iterator.next();
				
				Date lastEntry2Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy2'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
				//Date lastEntry2Date=new SimpleDateFormat("yyyy-MM-dd").parse("2013-12-06");
				if(lastEntry2Date!=null)
				{			
	            	List list=hSession.createQuery("from EveryDayCandle where mutualFundId=? and candleType=1 and tradeDate>=? and rsi>0 order by tradeDate").setParameter(0,mutualFundId).setParameter(1,lastEntry2Date).list();	
	            	int alertReason=0;
	            	Iterator itr=list.iterator();
	            	while(itr.hasNext())
	            	{
	            		alertReason=0;
	            		EveryDayCandle edc=((EveryDayCandle)itr.next());
	            		Date tradeDate=edc.getTradeDate();
	            		double entryPrice=edc.getTodaysClosePrice();            		
						
	            		List<Float> rsi = hSession.createQuery("SELECT rsi FROM EveryDayCandle  WHERE mutualFundId = ? and tradeDate <= ? and candleType=1 and rsi>0 ORDER BY tradeDate DESC ")
	                    	                .setParameter(0, mutualFundId)
											.setParameter(1,tradeDate)
	                            	        .setMaxResults(60)
	                                	    .list();
	                    if(rsi!=null)
	                    {
	                       	boolean firstPatternFound = false;
	                       	boolean secondPatternFound = false;
	                       	int j =0;//first pattern index
	                       	int a = 0 ; //second pattern index;

	                       	// Now get the valid W Pattern
	                       	for(int i=0;i<rsi.size()-2;i++)
	                       	{

	                           	if(!firstPatternFound)
	                           	{
	                               	j=i+1;
	                               	int k=i+2;
	                               	if((rsi.get(j) < rsi.get(i)) && (rsi.get(j) < rsi.get(k)))
	                               	{
	                                    firstPatternFound=true;
	                                    i++;
	                                    //System.out.println("First  Pattern FOund");
	                               	}
	                               	if(firstPatternFound && rsi.get(j) >50)
									{
										//System.out.println("First  Pattern FOund----------- "+rsi.get(j));
	                                    break;
									}
									else if(firstPatternFound && rsi.get(j) <50)
									{
										//System.out.println("First  Pattern FOund-----------VAILDATED **** "+firstPatternFound);
									}
	                            }
	                            if(firstPatternFound  && !secondPatternFound)
	                            {
									//System.out.println("***************** NOW SECOND check");
	                                a=i+1;
	                                int b = i+2;
									//System.out.println(" NOW trying to check for Second Pattern ");
	                                if(rsi.get(a)<rsi.get(i) && (rsi.get(a)<rsi.get(b)))
	                                {
	                      	        	secondPatternFound=true;

	                                }
	                                if(secondPatternFound)
	                                {
	                                    //System.out.println("Second Pattern Found *************");
	                                    if(rsi.get(a) < rsi.get(j))
	                                    	alertReason=7;
	                                    else
	                                        break;

	                                  }
	                              }
	                          }
	                      }
	                    if(alertReason==7)
	                    {
	                    	System.out.println("1 Entry 2 alert for mfId="+mutualFundId+"Date="+tradeDate+"   SAVE");
	                     	CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
	                    	customMFEntryAlert.setEntryPrice(entryPrice);
	                    	customMFEntryAlert.setStrategyName(strategyName);
	                    	customMFEntryAlert.setMutualFundId(mutualFundId);
	                    	customMFEntryAlert.setReasonForAlert(alertReason);
	                    	customMFEntryAlert.setTradeDate(tradeDate);
	                    	hSession.saveOrUpdate(customMFEntryAlert); 
	            			        	
	        	        }
	            	}//while loop
				
				}
				if(lastEntry2Date==null)
				{
					List<Date> dateList=hSession.createQuery("select tradeDate from EveryDayCandle where mutualFundId=? and candleType=? and rsi>? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,1).setParameter(2,0f).setMaxResults(1).list();
					if(dateList.size()!=0)
					{
						Date nonZeroFirstDate=(Date)dateList.get(0);
						List nonZeroSixtyDateList=hSession.createQuery("select tradeDate from EveryDayCandle where mutualFundId=? and candleType=? and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,1).setParameter(2,nonZeroFirstDate).setMaxResults(60).list();
		                if(nonZeroSixtyDateList!=null)
		                {
		                	Date nonZeroSixtyDate=(Date)nonZeroSixtyDateList.get(nonZeroSixtyDateList.size()-1);
		                	List list=hSession.createQuery("from EveryDayCandle where mutualFundId=? and candleType=1 and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,nonZeroSixtyDate).list();	
		                	int alertReason=0;
		                	Iterator itr=list.iterator();
		                	while(itr.hasNext())
		                	{
		                		alertReason=0;
		                		EveryDayCandle edc=((EveryDayCandle)itr.next());
		                		Date tradeDate=edc.getTradeDate();
		                		double entryPrice=edc.getTodaysClosePrice();
		                		
								
		                		List<Float> rsi = hSession.createQuery("SELECT rsi FROM EveryDayCandle  WHERE mutualFundId = ? and tradeDate <= ? and candleType=1 ORDER BY tradeDate DESC ")
		                        	                .setParameter(0, mutualFundId)
													.setParameter(1,tradeDate)
		                                	        .setMaxResults(60)
		                                    	    .list();
		                        if(rsi!=null)
		                        {
		                           	boolean firstPatternFound = false;
		                           	boolean secondPatternFound = false;
		                           	int j =0;//first pattern index
		                           	int a = 0 ; //second pattern index;

		                           	// Now get the valid W Pattern
		                           	for(int i=0;i<rsi.size()-2;i++)
		                           	{
			
		                               	if(!firstPatternFound)
		                               	{
		                                   	j=i+1;
		                                   	int k=i+2;
		                                   	if((rsi.get(j) < rsi.get(i)) && (rsi.get(j) < rsi.get(k)))
		                                   	{
		                                        firstPatternFound=true;
		                                        i++;
		                                        //System.out.println("First  Pattern FOund");
		                                   	}
		                                   	if(firstPatternFound && rsi.get(j) >50)
											{
												//System.out.println("First  Pattern FOund----------- "+rsi.get(j));
		                                        break;
											}
											else if(firstPatternFound && rsi.get(j) <50)
											{
												//System.out.println("First  Pattern FOund-----------VAILDATED **** "+firstPatternFound);
											}
		                                }
		                                if(firstPatternFound  && !secondPatternFound)
		                                {
											//System.out.println("***************** NOW SECOND check");
		                                    a=i+1;
		                                    int b = i+2;
											//System.out.println(" NOW trying to check for Second Pattern ");
		                                    if(rsi.get(a)<rsi.get(i) && (rsi.get(a)<rsi.get(b)))
		                                    {
		                          	        	secondPatternFound=true;

		                                    }
		                                    if(secondPatternFound)
		                                    {
		                                        //System.out.println("Second Pattern Found *************");
		                                        if(rsi.get(a) < rsi.get(j))
		                                        	alertReason=7;
		                                        else
		                                            break;

		                                      }
		                                  }
		                              }
		                          }
		                        if(alertReason==7)
		                        {
		                        	System.out.println("Entry 2 alert for mfId="+mutualFundId+"Date="+tradeDate+"   SAVE");
		                         	CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
		                        	customMFEntryAlert.setEntryPrice(entryPrice);
		                        	customMFEntryAlert.setStrategyName(strategyName);
		                        	customMFEntryAlert.setMutualFundId(mutualFundId);
		                        	customMFEntryAlert.setReasonForAlert(alertReason);
		                        	customMFEntryAlert.setTradeDate(tradeDate);
		                        	hSession.saveOrUpdate(customMFEntryAlert); 
		                			        	
		            	        }
		                	}//while loop
		                	
		                }//if close
					}//if close	
			
				}
				
				//tx.commit();
			}
			BufferedWriter bw=null;
			try
			{
				File f=new File("/opt/strategy");
	        	if(!f.isDirectory())
	        	{
	        		f.mkdirs();
	        	}
	        	
	        	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
				String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
				System.out.println("Saving entry 2");
				bw.write(entry);
				bw.newLine();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				bw.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}


	private void updateEntry1AfterDate() throws Exception {
		Session hSession=null;
		Transaction transaction=null;
		System.out.println("Updating entry 1");
		try {
				String strategyName="EntryStrategy1";
				MutualFundController mfc=new MutualFundController();
				List<Object[]> mfList=mfc.getMutualFundList();
				Iterator iterator=mfList.iterator();
				int count=0;
				hSession=HibernateBridge.getSessionFactory().openSession();
				while(iterator.hasNext())
				{				
					Object object[]=(Object[])iterator.next();
					Long mutualFundId=(Long)object[0];
					Date lastEntry1Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy1'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
					if(lastEntry1Date==null)
					{
						lastEntry1Date=(Date)hSession.createQuery("select min(tradeDate) from EveryDayCandle where mutualFundId=? and candleType=0").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
					}
					if(lastEntry1Date==null)
						continue;
					//System.out.println("mfid="+mutualFundId+"  lastDate="+lastEntry1Date);
					List<EveryDayCandle> weeklyCandleList = hSession.createQuery("from EveryDayCandle where mutualFundId=? and candleType=0 and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,lastEntry1Date).list();
					// This you need to write on your own, i need Weekly RSI 70 value for the symbol
					// CustomMFExitAlert is the object in which we are persisting Exit dates for different exit startegy 
		            boolean watchActivatedCond1Met = false; // we have to persist this when this code moves to DataController
		            int countWatchCond1 = 0;
		            int countWatchTotalCond1 = 0;
		            boolean watchActivatedCond2 = false;
		
		            int countWatchTotalCond2 = 0;
					transaction=hSession.beginTransaction();
		            if (weeklyCandleList != null && weeklyCandleList.size() != 0) {
		                for (EveryDayCandle weeklyCandle : weeklyCandleList) {
		                    if (watchActivatedCond2)  // Checking for final condition
		                    {
		                        if (weeklyCandle.getRsi() > 30) {
		                            countWatchTotalCond2++;
		                            if (countWatchTotalCond2 >= 4) {
		                                watchActivatedCond2 = false; //restting again for next check
		                                watchActivatedCond1Met = false;//restting again for next check
		                                // EXIT CONDIITON MET, TO Populate Database
		                                //strategyName - "ExitStrategy1"
		                                // symbol - symbol
		                                // tradeDate - weeklyCandleList.get(index).getTradDate()
		                                //reasonForAlert - "as per ExitStrategy1"
		                                // exitPrice  -  weeklyCandleList.get(index).getClosePrice()
		                                //nftyPrice - for Nifty same as weeklyCandleList.get(index).getClosePrice()
		                                CustomMFEntryAlert customMFEntryAlert = new CustomMFEntryAlert();
		                                customMFEntryAlert.setMutualFundId(mutualFundId);
		                                customMFEntryAlert.setStrategyName(strategyName);
		                                customMFEntryAlert.setTradeDate(weeklyCandle.getTradeDate());
		                                customMFEntryAlert.setReasonForAlert(1);
		                                customMFEntryAlert.setEntryPrice(weeklyCandle.getTodaysClosePrice());
		                                //customMFExitAlert.setNiftyEntryPrice(weeklyCandle.getClosePrice());
		                                hSession.saveOrUpdate(customMFEntryAlert);
										System.out.println("Entry 1 alert found for="+mutualFundId+" on="+weeklyCandle.getTradeDate());
		                            }
		                        } else {
		                            // Second COunter to be reset
		                            countWatchTotalCond2 = 0;
		                        }
		                    }
		
		                    if (!watchActivatedCond1Met && weeklyCandle.getRsi() < 30) {
		                        countWatchCond1++;
		                        if (countWatchCond1 == 1) {
		                            countWatchTotalCond1 = 0;
		                        } else if (countWatchCond1 >= 3) {
		                            watchActivatedCond2 = true;
		                            watchActivatedCond1Met = true;
		                            countWatchTotalCond1 = 0;
		                            countWatchCond1 = 0;
		                        }
		                    }
		
		                    if (!watchActivatedCond1Met)   // Not 30
		                    {
		                        countWatchTotalCond1++;
		                    }
		
		                    if (!watchActivatedCond1Met && countWatchTotalCond1 >= 4) {
		                        watchActivatedCond1Met = false;
		                        countWatchCond1 = 0;
		                        countWatchTotalCond1 = 0;
		                    }
		
		                }
		            }
		            transaction.commit();
	            }
	            BufferedWriter bw=null;
	            try
	            {
	            	File f=new File("/opt/strategy");
	            	if(!f.isDirectory())
	            	{
	            		f.mkdirs();
	            	}
	            	
	            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
	            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	            	System.out.println("Saving entry 1");
	            	bw.write(entry);
	            	bw.newLine();
	            }
	            catch(Exception e)
	            {
	            	e.printStackTrace();
	            }
	            finally
	            {
	            	bw.close();
	            }
	        } 
			catch (Exception e)
			{
	           e.printStackTrace();
	           if(transaction!=null)
	        	   transaction.rollback();
	           throw new Exception(e.getMessage());
			}
			finally
			{
				hSession.close();
			}		
	}
	
	
	

	
	
	
}
