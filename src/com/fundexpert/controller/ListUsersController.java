package com.fundexpert.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.User;

public class ListUsersController 
{
	public List<User> getList(String appId,  String sessionId)	
	{
		List<String> list=new ArrayList<String>();
		List<User> userList=new ArrayList<User>();
		Session hSession =HibernateBridge.getSessionFactory().openSession();
		try{
			Long consumerId = (Long) hSession.createQuery("select id from Consumer where appId=?").setString(0, appId).uniqueResult();
			if(consumerId!=null)
			{
				userList = hSession.createQuery("from User where consumerId=? ").setLong(0, consumerId).list();
				if(userList.size()!=0)
				{
					return userList;
				}
				else
					throw new ValidationException("Not a valid Consumer ID.");
			}
			else
				throw new ValidationException("Not a valid Consumer! or enter a valid AppID.");
		}catch(Exception e){
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return userList;
	}
}