package com.fundexpert.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.validation.ValidationException;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Consumer;
import com.fundexpert.dao.Sessionn;
import com.fundexpert.dao.User; 

public class RegisterUserController
{
	public String checkUserLogin(String appId, String sessionId, String ipAddress)
	{
		String userId=null;
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Session session=HibernateBridge.getSessionFactory().openSession();

		Transaction tx=session.beginTransaction();
		
		try{
			Consumer consumer=(Consumer)session.createQuery("from Consumer where appId=?").setString(0, appId).uniqueResult();
			if(consumer!=null)
			{
				Calendar cal=Calendar.getInstance();
				String str=sdf.format(cal.getTime());
				java.util.Date d=sdf.parse(str);
				java.sql.Date dateDB = new java.sql.Date(d.getTime());
				
				Calendar nowCal=Calendar.getInstance();
				nowCal.add(Calendar.MINUTE, -15);
				
				if(consumer.getIpAddress().equals(ipAddress))
				{
					Sessionn ses=(Sessionn)session.createQuery("from Sessionn where sessionId=? and consumerId=? order by id DESC")
											.setString(0,sessionId).setLong(1, consumer.getId()).setMaxResults(1).uniqueResult();			
					if(ses!=null)
					{
						System.out.println("inside session!=null : "+ses.getId()+",  "+ses.getLastUsedOn()+",  "+nowCal.getTime());
						if((ses.getLastUsedOn()).compareTo(nowCal.getTime())>0)
						{
							User user=new User();
							user.setConsumerId(consumer.getId());
							userId="user"+ses.getId();
							user.setUserId(userId);
							
							user.setCreatedOn(dateDB);
							user.setActive(true);
							session.save(user);
							
							consumer.setTokensConsumed(consumer.getTokensConsumed()+1);
							session.update(consumer);
							System.out.println("Consumer has been updated Successfully.");
							tx.commit();
							System.out.println("New User has been Saved Successfully.");
						}
						else
							 throw new ValidationException("Session Expired.");
					}
				}
			}else
				System.out.println("Invalid AppID or Not valid Consumer to access this.");
		}catch(Exception e){
			tx.rollback();
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return userId;
	}
}