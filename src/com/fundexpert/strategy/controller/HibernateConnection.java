package com.fundexpert.strategy.controller;

import java.text.SimpleDateFormat;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.EveryDayCandle;
import com.fundexpert.dao.Fractal;

public class HibernateConnection {

	public static void saveOrUpdate(Object object)
	{
		Session hSession=null;
		Transaction transaction=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			System.out.println("Inside Saving");
			transaction=hSession.beginTransaction();
			hSession.saveOrUpdate(object);
			transaction.commit();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(transaction!=null)
				transaction.rollback();
		}
		finally
		{
			hSession.close();
		}
	}
	
	public static List<EveryDayCandle> getIntraDayFeeds(Long mutualFundId,int candleType,int days)
	{
		//if days == 0 that means for all dates
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<EveryDayCandle> list=null;
			if(days==0)
			{
				list=hSession.createQuery("from EveryDayCandle where mutualFundId=? and candleType=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,candleType).list();
			}
			else
			{
				//TODO when days >0 list last 'days' of everydaycandle in ascending order so modify below query  
				list=hSession.createQuery("from EveryDayCandle where mutualFundId=? and candleType=? order by tradeDate desc").setParameter(0, mutualFundId).setParameter(1,candleType).setMaxResults(days).list();
			}
			return list;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
	public static List<Fractal> getFractalStrategy(Long mutualFundId,int candleType,int period)
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Fractal> list=null;
			
			list=hSession.createQuery("from Fractal where mutualFundId=? and candleType=? and period=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,candleType).setParameter(2, period).list();
			return list;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}

}
