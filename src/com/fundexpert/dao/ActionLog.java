package com.fundexpert.dao;

import java.io.Serializable;
import java.util.Date;

public class ActionLog implements Serializable
{
	private static final long serialVersionUID=1L;
	private Long id, sessionId, userId;
	private String apiName;
	private Date createdOn;
	private String inputJSON, outputJSON;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getSessionId() {
		return sessionId;
	}
	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getApiName() {
		return apiName;
	}
	public void setApiName(String apiName) {
		this.apiName = apiName;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getInputJSON() {
		return inputJSON;
	}
	public void setInputJSON(String inputJSON) {
		this.inputJSON = inputJSON;
	}
	public String getOutputJSON() {
		return outputJSON;
	}
	public void setOutputJSON(String outputJSON) {
		this.outputJSON = outputJSON;
	}
	
} 

