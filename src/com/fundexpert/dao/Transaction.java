package com.fundexpert.dao;

import java.io.Serializable; 
import java.util.Date;

public class Transaction implements Serializable
{
	private static final long serialVersionUID=1L;
	private Long id, userId, holdingId;
	private int action;
	private double units, nav, amount;
	private boolean reinvest;
	private Date transactionDate, createdOn;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getHoldingId() {
		return holdingId;
	}
	public void setHoldingId(Long holdingId) {
		this.holdingId = holdingId;
	}
	public int getAction() {
		return action;
	}
	public void setAction(int action) {
		this.action = action;
	}
	public double getUnits() {
		return units;
	}
	public void setUnits(double units) {
		this.units = units;
	}
	public double getNav() {
		return nav;
	}
	public void setNav(double nav) {
		this.nav = nav;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public boolean isReinvest() {
		return reinvest;
	}
	public void setReinvest(boolean reinvest) {
		this.reinvest = reinvest;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
