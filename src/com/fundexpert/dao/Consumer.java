package com.fundexpert.dao;

import java.io.Serializable; 
import java.util.Date; 
import java.util.List;
import java.util.Set;

public class Consumer implements Serializable
{
	private static final long serialVersionUID=1L;
	private Long id;
	private String email, password, companyName, appId, secretKey;
	private int noOfTokens, tokensConsumed;
	private boolean active;
	private Date createdOn;
	private String businesName, address, ipAddress;
	private String phone;
	private boolean ipHardcoded; 
	List<Sessionn> sessionList;
	List<User> userList; 
	public List<Sessionn> getSessionList() {
		return sessionList;
	}
	public void setSessionList(List<Sessionn> sessionList) {
		this.sessionList = sessionList;
	}
	public List<User> getUserList() {
		return userList;
	}
	public void setUserList(List<User> userList) {
		this.userList = userList;
	} 
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public int getNoOfTokens() {
		return noOfTokens;
	}
	public void setNoOfTokens(int noOfTokens) {
		this.noOfTokens = noOfTokens;
	}
	public int getTokensConsumed() {
		return tokensConsumed;
	}
	public void setTokensConsumed(int tokensConsumed) {
		this.tokensConsumed = tokensConsumed;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getBusinesName() {
		return businesName;
	}
	public void setBusinesName(String businesName) {
		this.businesName = businesName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public boolean isIpHardcoded() {
		return ipHardcoded;
	}
	public void setIpHardcoded(boolean ipHardcoded) {
		this.ipHardcoded = ipHardcoded;
	}
}
