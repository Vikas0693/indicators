package com.fundexpert.dao;

import java.io.Serializable;
import java.util.Date;


public class Fractal implements Serializable
{
	private long mutualFundId;
	private Date tradeDate;
	private int candleType;
	private int period;//stores number of days ahead of currentDays for .eg period=2 means considering 2 days ahead and 2 days back while calculating fractal of current day
	private double closePrice;
	private double high;
	private double low;
	private double open;
	private double upFractal;
	private Date maxFractalDate;
	private double lowFractal;
	private Date minFractalDate;
	private Double swingHigh;
	private Date swingHighDate;
	private Double swingLow;
	private Date swingLowDate;

	private String swingTrend;
	private Double tcfl1, tcfl2, tcfl3;

	public long getMutualFundId()
	{
		return mutualFundId;
	}

	public void setMutualFundId(long mutualFundId)
	{
		this.mutualFundId = mutualFundId;
	}

	public Date getTradeDate()
	{
		return tradeDate;
	}

	public void setTradeDate(Date tradeDate)
	{
		this.tradeDate = tradeDate;
	}

	public int getCandleType()
	{
		return candleType;
	}

	public void setCandleType(int candleType)
	{
		this.candleType = candleType;
	}

	public int getPeriod()
	{
		return period;
	}

	public void setPeriod(int period)
	{
		this.period = period;
	}

	public double getUpFractal()
	{
		return upFractal;
	}

	public void setUpFractal(double upFractal)
	{
		this.upFractal = upFractal;
	}

	public double getLowFractal()
	{
		return lowFractal;
	}

	public void setLowFractal(double lowFractal)
	{
		this.lowFractal = lowFractal;
	}

	public Double getSwingHigh()
	{
		return swingHigh;
	}

	public void setSwingHigh(Double swingHigh)
	{
		this.swingHigh = swingHigh;
	}

	public Date getSwingHighDate()
	{
		return swingHighDate;
	}

	public void setSwingHighDate(Date swingHighDate)
	{
		this.swingHighDate = swingHighDate;
	}

	public Date getMaxFractalDate()
	{
		return maxFractalDate;
	}

	public void setMaxFractalDate(Date maxFractalDate)
	{
		this.maxFractalDate = maxFractalDate;
	}

	public Double getSwingLow()
	{
		return swingLow;
	}

	public void setSwingLow(Double swingLow)
	{
		this.swingLow = swingLow;
	}

	public Date getSwingLowDate()
	{
		return swingLowDate;
	}

	public void setSwingLowDate(Date swingLowDate)
	{
		this.swingLowDate = swingLowDate;
	}

	public Date getMinFractalDate()
	{
		return minFractalDate;
	}

	public void setMinFractalDate(Date minFractalDate)
	{
		this.minFractalDate = minFractalDate;
	}

	public String getSwingTrend()
	{
		return swingTrend;
	}

	public void setSwingTrend(String swingTrend)
	{
		this.swingTrend = swingTrend;
	}

	public Double getTcfl1()
	{
		return tcfl1;
	}

	public void setTcfl1(Double tcfl1)
	{
		this.tcfl1 = tcfl1;
	}

	public Double getTcfl2()
	{
		return tcfl2;
	}

	public void setTcfl2(Double tcfl2)
	{
		this.tcfl2 = tcfl2;
	}

	public Double getTcfl3()
	{
		return tcfl3;
	}

	public void setTcfl3(Double tcfl3)
	{
		this.tcfl3 = tcfl3;
	}

	public double getClosePrice()
	{
		return closePrice;
	}

	public void setClosePrice(double closePrice)
	{
		this.closePrice = closePrice;
	}

	public double getHigh()
	{
		return high;
	}

	public void setHigh(double high)
	{
		this.high = high;
	}

	public double getLow()
	{
		return low;
	}

	public void setLow(double low)
	{
		this.low = low;
	}

	public double getOpen()
	{
		return open;
	}

	public void setOpen(double open)
	{
		this.open = open;
	}
}
