package com.fundexpert.dao;

import java.io.Serializable; 
import java.util.Date;

public class Holding  implements Serializable
{
	private static final long serialVersionUID=1L;
	private Long id, userId, mutualfundId;
	private String folioNumber;
	private double units, avgNav;
	private Date createdOn;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getMutualfundId() {
		return mutualfundId;
	}
	public void setMutualfundId(Long mutualfundId) {
		this.mutualfundId = mutualfundId;
	}
	public String getFolioNumber() {
		return folioNumber;
	}
	public void setFolioNumber(String folioNumber) {
		this.folioNumber = folioNumber;
	}
	public double getUnits() {
		return units;
	}
	public void setUnits(double units) {
		this.units = units;
	}
	public double getAvgNav() {
		return avgNav;
	}
	public void setAvgNav(double avgNav) {
		this.avgNav = avgNav;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
