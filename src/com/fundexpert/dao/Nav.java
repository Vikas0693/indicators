package com.fundexpert.dao;

import java.io.Serializable; 
import java.util.Date;

public class Nav implements Serializable
{
	private static final long serialVersionUID=1L;
	private Long id, mutualfundId;
	private Date tradeDate;
	private double nav;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMutualfundId() {
		return mutualfundId;
	}
	public void setMutualfundId(Long mutualfundId) {
		this.mutualfundId = mutualfundId;
	}
	public Date getTradeDate() {
		return tradeDate;
	}
	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}
	public double getNav() {
		return nav;
	}
	public void setNav(double nav) {
		this.nav = nav;
	}
}

