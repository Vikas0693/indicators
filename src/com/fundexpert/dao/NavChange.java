package com.fundexpert.dao;

import java.io.Serializable;  
import java.util.Date;

public class NavChange implements Serializable
{
	private static final long serialVersionUID=1L;
	private Long id, mutualfundId;
	private double day, month, year, quarter, threeYear, fiveYear;
	private Date tradeDate;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMutualfundId() {
		return mutualfundId;
	}
	public void setMutualfundId(Long mutualfundId) {
		this.mutualfundId = mutualfundId;
	}
	public double getDay() {
		return day;
	}
	public void setDay(double day) {
		this.day = day;
	}
	public double getMonth() {
		return month;
	}
	public void setMonth(double month) {
		this.month = month;
	}
	public double getYear() {
		return year;
	}
	public void setYear(double year) {
		this.year = year;
	}
	public double getQuarter() {
		return quarter;
	}
	public void setQuarter(double quarter) {
		this.quarter = quarter;
	}
	public double getThreeYear() {
		return threeYear;
	}
	public void setThreeYear(double threeYear) {
		this.threeYear = threeYear;
	}
	public double getFiveYear() {
		return fiveYear;
	}
	public void setFiveYear(double fiveYear) {
		this.fiveYear = fiveYear;
	}
	public Date getTradeDate() {
		return tradeDate;
	}
	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}
}
