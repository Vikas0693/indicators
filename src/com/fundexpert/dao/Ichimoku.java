package com.fundexpert.dao;

import java.io.Serializable;
import java.util.Date;


public class Ichimoku implements Serializable
{
	private long mutualFundId;
	private Date tradeDate;
	private int candleType;

	private double close;
	private double high;
	private double low;
	private double open;
	private double tenkanSen;
	private double kijunSen;

	private double senkouSpanA;
	private double senkouSpanB;
	private double kumoTop;
	private double kumoBottom;

	public long getMutualFundId()
	{
		return mutualFundId;
	}

	public void setMutualFundId(long mutualFundId)
	{
		this.mutualFundId = mutualFundId;
	}

	public Date getTradeDate()
	{
		return tradeDate;
	}

	public void setTradeDate(Date tradeDate)
	{
		this.tradeDate = tradeDate;
	}

	public int getCandleType()
	{
		return candleType;
	}

	public void setCandleType(int candleType)
	{
		this.candleType = candleType;
	}

	public double getOpen()
	{
		return open;
	}

	public void setOpen(double open)
	{
		this.open = open;
	}

	public double getClose()
	{
		return close;
	}

	public void setClose(double close)
	{
		this.close = close;
	}

	public double getHigh()
	{
		return high;
	}

	public void setHigh(double high)
	{
		this.high = high;
	}

	public double getLow()
	{
		return low;
	}

	public void setLow(double low)
	{
		this.low = low;
	}

	public double getTenkanSen()
	{
		return tenkanSen;
	}

	public void setTenkanSen(double tenkanSen)
	{
		this.tenkanSen = tenkanSen;
	}

	public double getKijunSen()
	{
		return kijunSen;
	}

	public void setKijunSen(double kijunSen)
	{
		this.kijunSen = kijunSen;
	}

	public double getSenkouSpanA()
	{
		return senkouSpanA;
	}

	public void setSenkouSpanA(double senkouSpanA)
	{
		this.senkouSpanA = senkouSpanA;
	}

	public double getSenkouSpanB()
	{
		return senkouSpanB;
	}

	public void setSenkouSpanB(double senkouSpanB)
	{
		this.senkouSpanB = senkouSpanB;
	}

	public double getKumoTop()
	{
		return kumoTop;
	}

	public void setKumoTop(double kumoTop)
	{
		this.kumoTop = kumoTop;
	}

	public double getKumoBottom()
	{
		return kumoBottom;
	}

	public void setKumoBottom(double kumoBottom)
	{
		this.kumoBottom = kumoBottom;
	}




}
