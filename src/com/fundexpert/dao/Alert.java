package com.fundexpert.dao;

import java.io.Serializable; 
import java.util.Date;

public class Alert implements Serializable
{
	private static final long serialVersionUID=1L;
	private Long id, mutualfundId;
	private Date tradeDate;
	private double nav;
	private int strength, type;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMutualfundId() {
		return mutualfundId;
	}
	public void setMutualfundId(Long mutualfundId) {
		this.mutualfundId = mutualfundId;
	}
	public Date getTradeDate() {
		return tradeDate;
	}
	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}
	public double getNav() {
		return nav;
	}
	public void setNav(double nav) {
		this.nav = nav;
	}
	public int getStrength() {
		return strength;
	}
	public void setStrength(int strength) {
		this.strength = strength;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
}

