package com.fundexpert.dao;
import java.io.Serializable;
import java.util.Date;

public class CustomMFEntryAlert implements Serializable {
    private String strategyName;
    private Long mutualFundId;
    private Date tradeDate;
    private int reasonForAlert;
    private double entryPrice;
    
    
	public String getStrategyName() {
		return strategyName;
	}
	public void setStrategyName(String strategyName) {
		this.strategyName = strategyName;
	}
	public Long getMutualFundId() {
		return mutualFundId;
	}
	public void setMutualFundId(Long mutualFundId) {
		this.mutualFundId = mutualFundId;
	}
	public Date getTradeDate() {
		return tradeDate;
	}
	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}
	public int getReasonForAlert() {
		return reasonForAlert;
	}
	public void setReasonForAlert(int reasonForAlert) {
		this.reasonForAlert = reasonForAlert;
	}
	public double getEntryPrice() {
		return entryPrice;
	}
	public void setEntryPrice(double entryPrice) {
		this.entryPrice = entryPrice;
	}    
}