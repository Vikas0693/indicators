package com.fundexpert.dao;

import java.util.Date;

public class Sessionn
{
	private Long id, consumerId;
	private String sessionId;
	private String ipAddress;
	private Date createdOn, lastUsedOn;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(Long consumerId) {
		this.consumerId = consumerId;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getLastUsedOn() {
		return lastUsedOn;
	}
	public void setLastUsedOn(Date lastUsedOn) {
		this.lastUsedOn = lastUsedOn;
	}
	
	
}
