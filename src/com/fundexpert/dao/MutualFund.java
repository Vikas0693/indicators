package com.fundexpert.dao;

import java.io.Serializable; 
import java.util.List;
import java.util.Set;

public class MutualFund implements Serializable
{
	private static final long serialVersionUID=1L;
	private Long id, regularFundId, growthFundId;
	private String name, amfiiCode, isin;
	private int category, subcategory, optionType, sector;
	private boolean direct;
	private float ourRating;
	private List exitStrategies; 
	private List entryStrategies;
	private MutualFund liquidFund;
	private int isRobo;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getRegularFundId() {
		return regularFundId;
	}
	public void setRegularFundId(Long regularFundId) {
		this.regularFundId = regularFundId;
	}
	public Long getGrowthFundId() {
		return growthFundId;
	}
	public void setGrowthFundId(Long growthFundId) {
		this.growthFundId = growthFundId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAmfiiCode() {
		return amfiiCode;
	}
	public void setAmfiiCode(String amfiiCode) {
		this.amfiiCode = amfiiCode;
	}
	public String getIsin() {
		return isin;
	}
	public void setIsin(String isin) {
		this.isin = isin;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public int getSubcategory() {
		return subcategory;
	}
	public void setSubcategory(int subcategory) {
		this.subcategory = subcategory;
	}
	public int getOptionType() {
		return optionType;
	}
	public void setOptionType(int optionType) {
		this.optionType = optionType;
	}
	public int getSector() {
		return sector;
	}
	public void setSector(int sector) {
		this.sector = sector;
	}
	public boolean isDirect() {
		return direct;
	}
	public void setDirect(boolean direct) {
		this.direct = direct;
	}	
	public float getOurRating() {
		return ourRating;
	}
	public void setOurRating(float ourRating) {
		this.ourRating = ourRating;
	}
	public List getExitStrategies() {
		return exitStrategies;
	}
	public void setExitStrategies(List exitStrategies) {
		this.exitStrategies = exitStrategies;
	}
	public List getEntryStrategies() {
		return entryStrategies;
	}
	public void setEntryStrategies(List entryStrategies) {
		this.entryStrategies = entryStrategies;
	}
	public MutualFund getLiquidFund() {
		return liquidFund;
	}
	public void setLiquidFund(MutualFund liquidFund) {
		this.liquidFund = liquidFund;
	}
	public int getIsRobo() {
		return isRobo;
	}
	public void setIsRobo(int isRobo) {
		this.isRobo = isRobo;
	}

	
	
}

