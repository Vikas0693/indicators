package com.fundexpert.dao;

import java.io.Serializable; 
import java.util.Date;
import java.util.List;

public class User implements Serializable
{
	private static final long serialVersionUID=1L;
	private Long id, consumerId;
	private String userId;
	private int riskAppetite;
	private boolean active;
	private Date createdOn;
	List<Transaction> tList;
	List<Holding> holdingList;
	List<PortfolioSubscription> pfList;
	public List<Holding> getHoldingList() {
		return holdingList;
	}
	public void setHoldingList(List<Holding> holdingList) {
		this.holdingList = holdingList;
	}
	public List<PortfolioSubscription> getPfLits() {
		return pfList;
	}
	public void setPfLits(List<PortfolioSubscription> pfLits) {
		this.pfList = pfLits;
	}
	public List<Transaction> gettList() {
		return tList;
	}
	public void settList(List<Transaction> tList) {
		this.tList = tList;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(Long consumerId) {
		this.consumerId = consumerId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getRiskAppetite() {
		return riskAppetite;
	}
	public void setRiskAppetite(int riskAppetite) {
		this.riskAppetite = riskAppetite;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}

