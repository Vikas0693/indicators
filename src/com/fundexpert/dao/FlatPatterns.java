package com.fundexpert.dao;

import java.io.Serializable;
import java.util.Date;

public class FlatPatterns implements Serializable
{
	private Long mutualFundId;
	private int candleType;
	private String patternName;
	private int flatDays;
	private Date patternStartDate;
	private Date patternEndDate;
	private int numberOfDays;
	private double patternValue;
	

	public Long getMutualFundId() {
		return mutualFundId;
	}
	public void setMutualFundId(Long mutualFundId) {
		this.mutualFundId = mutualFundId;
	}
	public String getPatternName() {
		return patternName;
	}
	public void setPatternName(String patternName) {
		this.patternName = patternName;
	}
	public int getFlatDays() {
		return flatDays;
	}
	public void setFlatDays(int flatDays) {
		this.flatDays = flatDays;
	}
	public Date getPatternStartDate() {
		return patternStartDate;
	}
	public void setPatternStartDate(Date patternStartDate) {
		this.patternStartDate = patternStartDate;
	}
	public Date getPatternEndDate() {
		return patternEndDate;
	}
	public void setPatternEndDate(Date patternEndDate) {
		this.patternEndDate = patternEndDate;
	}
	public int getNumberOfDays() {
		return numberOfDays;
	}
	public void setNumberOfDays(int numberOfDays) {
		this.numberOfDays = numberOfDays;
	}	
	public double getPatternValue() {
		return patternValue;
	}
	public void setPatternValue(double patternValue) {
		this.patternValue = patternValue;
	}
	public int getCandleType() {
		return candleType;
	}
	public void setCandleType(int candleType) {
		this.candleType = candleType;
	}
	
}