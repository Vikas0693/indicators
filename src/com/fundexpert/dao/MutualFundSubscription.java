package com.fundexpert.dao;

import java.io.Serializable; 
import java.util.Date;

public class MutualFundSubscription implements Serializable
{
	private static final long serialVersionUID=1L;
	private Long id, mutualfundId,userId;
	private Date startDate, endDate;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMutualfundId() {
		return mutualfundId;
	}
	public void setMutualfundId(Long mutualfundId) {
		this.mutualfundId = mutualfundId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}

