package com.fundexpert.dao;

import java.io.Serializable;

public class StrategyProfile implements Serializable {

	private String strategy;
	private int riskProfile;    //all/A/B/C-0/1/2/3
	private int timeFrame;    	//long/medium/short-7/8/9
	private int severity;		//0-no opinion//10%,20% etc
	
	public String getStrategy() {
		return strategy;
	}
	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}
	public int getRiskProfile() {
		return riskProfile;
	}
	public void setRiskProfile(int riskProfile) {
		this.riskProfile = riskProfile;
	}
	public int getTimeFrame() {
		return timeFrame;
	}
	public void setTimeFrame(int timeFrame) {
		this.timeFrame = timeFrame;
	}
	public int getSeverity() {
		return severity;
	}
	public void setSeverity(int severity) {
		this.severity = severity;
	}
}
