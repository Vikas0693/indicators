package com.apidoc.util;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.fundexpert.dao.MutualFund;

public class MappingLiquidFundId {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			MutualFund liquidMf=(MutualFund)hSession.createQuery("from MutualFund where id=4776").setMaxResults(1).uniqueResult();
			System.out.println("LiquidFund="+liquidMf.getName());
			List equityFund=hSession.createQuery("from MutualFund where (name like 'UTI%') and category=1 and optionType=1 and direct!=1").list();
			tx=hSession.beginTransaction();
			Iterator itr=equityFund.iterator();
			while(itr.hasNext())
			{
				MutualFund equity=(MutualFund)itr.next();
				equity.setLiquidFund(liquidMf);
				hSession.saveOrUpdate(equity);
				System.out.println(equity.getName()+"\\\t"+((MutualFund)equity.getLiquidFund()).getName());
			}
			tx.commit();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

}
