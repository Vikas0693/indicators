//branch in Nav branch of Fundexpert
package com.apidoc.util;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.fundexpert.dao.MutualFund;

//this class needs to be run on Fundexpert Project with new dao(apimf) since we need Fundxpert mutualFund table for that
public class ApiMf
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		Session hSession = null;
		Transaction tx = null;
		String name = "", amfiiCode = "", isin = "";
		int category, subcategory, optionType, sector = -1;
		boolean direct;
		long growthFundId, regularFundId;
		float ourRating;



		String sectorName = "";
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			tx = hSession.beginTransaction();
			List mfList = hSession.createQuery("from MutualFund where settlementType not like ? and name not like ? and  name not like ? and name not like ?").setParameter(0, "%L%").setParameter(1, "%fmp%").setParameter(2, "%fixed%").setParameter(3, "%series%").list();
			Iterator itr = mfList.iterator();
			ApiMf test = new ApiMf();
			while (itr.hasNext())
			{
				ApiMf apiMf = new ApiMf();
				MutualFund mf = (MutualFund) itr.next();
				System.out.println("MfId=" + mf.getId());
				name = mf.getName();
				apiMf.setName(name);
				apiMf.setOurRating((float) mf.getOurRating());
				amfiiCode = mf.getAmfiiCode() == null ? null : Long.toString(mf.getAmfiiCode());

				if(amfiiCode != null)
					apiMf.setAmfiiCode(amfiiCode);

				isin = mf.getIsin();
				if(amfiiCode == null && isin == null)
				{
					continue;
				}
				apiMf.setIsin(isin);

				category = test.getCategory(mf.getSchemeType().toUpperCase());
				if(category == 8 || category == 10)
				{
					// 3 represent DEBT from function getCategory() and 8,10 represent GILT and MIP from schemeType of Mutual Fund
					category = 3;
				}
				if(category != -1)
				{
					// save the mutual fund for api
					apiMf.setCategory(category);
				}
				optionType = test.getOptionType(mf.getOptionType().toUpperCase());
				apiMf.setOptionType(optionType);
				direct = name.toLowerCase().contains("direct") ? true : false;
				apiMf.setDirect(direct);
				subcategory = test.getSubcategory(mf.getType().toUpperCase());
				if(subcategory != -1)
				{
					apiMf.setSubcategory(subcategory);
				}
				if(subcategory == 5)
				{
					if(mf.getType().toUpperCase().contains("SECTOR"))
					{
						System.out.println("Sector=" + mf.getType() + "indexOf(-)=" + (mf.getType().indexOf("-")));
						sectorName = mf.getType().substring(mf.getType().indexOf("-") + 1).toUpperCase();
					}
					else
					{
						sectorName = mf.getType().toUpperCase();
					}
					sector = test.getSector(sectorName);
				}
				if(sector != -1)
				{
					apiMf.setSector(sector);
				}
				System.out.println("Sector=" + sector);
				hSession.save(apiMf);


			}
			tx.commit();


		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null)
			{
				tx.rollback();
			}
		}
		finally
		{
			hSession.close();
		}
	}

	int getCategory(String schemeType)
	{
		final int EQUITY = 1, ELSS = 2, DEBT = 3, LIQUID = 4, BALANCED = 5, ETF = 6, STP = 7, GILT = 8, FOF = 9, MIP = 10, HYBRID = 11, BOND = 12, INCOME = 13, EQUITYG = 14, EQUITYS = 15;
		switch (schemeType)
		{
			case "EQUITY":
				return EQUITY;
			case "ELSS":
				return ELSS;
			case "DEBT":
				return DEBT;
			case "LIQUID":
				return LIQUID;
			case "BALANCED":
				return BALANCED;
			case "ETF":
				return ETF;
			case "STP":
				return STP;
			case "GILT":
				return GILT;
			case "FOF":
				return FOF;
			case "MIP":
				return MIP;
			case "HYBRID":
				return HYBRID;
			case "BOND":
				return BOND;
			case "INCOME":
				return INCOME;
			case "EQUITYG":
				return EQUITYG;
			case "EQUITYS":
				return EQUITYS;
			default:
				return -1;
		}

	}

	int getOptionType(String optionType)
	{
		final int GROWTH = 1, PAYOUT = 2, REINVEST = 3, SWEEP = 4;
		switch (optionType)
		{
			case "GROWTH":
				return GROWTH;
			case "PAYOUT":
				return PAYOUT;
			case "REINVEST":
				return REINVEST;
			case "SWEEP":
				return SWEEP;
			default:
				return -1;
		}
	}

	int getSubcategory(String type)
	{
		final int LARGE_CAP = 1;
		final int MID_CAP = 2;
		final int DIVERSIFIED = 4;
		final int LIQUID = 13;
		final int EQUITY = 14;
		final int BALANCED = 15;
		final int SMALL_CAP = 3;
		final int ARBITRAGE = 6;
		final int SHORT_TERM = 9;
		final int INDEX = 7;
		final int ULTRA_SHORT_TERM = 10;
		final int LONG_TERM = 8;
		final int HYBRID = 16;

		final int GLOBAL_USA = 17;
		final int GLOBAL_GENERAL = 18;
		final int GLOBAL_AGRI_COMMODITY = 19;
		final int GLOBAL_EQUITY = 20;
		final int SECTOR = 5;


		switch (type)
		{
			case "LARGE CAP":
				return LARGE_CAP;
			case "MID CAP":
				return MID_CAP;
			case "DIVERSIFIED":
				return DIVERSIFIED;
			case "LIQUID":
				return LIQUID;
			case "EQUITY":
				return EQUITY;
			case "BALANCED":
				return BALANCED;
			case "SMALL CAP":
				return SMALL_CAP;
			case "ARBITRAGE":
				return ARBITRAGE;
			case "SHORT TERM":
				return SHORT_TERM;
			case "INDEX":
				return INDEX;
			case "ULTRA SHORT TERM":
				return ULTRA_SHORT_TERM;
			case "LONG TERM":
				return LONG_TERM;
			case "HYBRID":
				return HYBRID;
			case "GLOBAL-USA":
				return GLOBAL_USA;
			case "GLOBAL-GENERAL":
				return GLOBAL_GENERAL;
			case "GLOBAL-AGRI COMMODITY":
				return GLOBAL_AGRI_COMMODITY;
			case "GLOBAL-EQUITY":
				return GLOBAL_EQUITY;

		}
		if(type.contains("SECTOR"))
		{
			return SECTOR;

		}
		else if(type.equals(""))
		{
			return -1;
		}
		else
		{
			return SECTOR;
		}
	}

	int getSector(String sectorName)
	{
		final int PRECIOUS_METALS = 1, BANKING = 2, FMCG = 3, INFOTECH = 4, MEDIA = 5, PHARMA = 6, INFRASTRUCTURE = 7, UTILITY = 8, OTHER = 9, NATURAL_RESOURCES = 10, FLEXI = 11, SERVICES = 12, MANUFACTURING = 13, ENTERTAINMENT = 14;
		switch (sectorName)
		{
			case "PRECIOUS METALS":
				return PRECIOUS_METALS;
			case "UTILITY":
				return UTILITY;
			case "BANKING":
				return BANKING;
			case "FMCG":
				return FMCG;
			case "OTHER":
				return OTHER;
			case "INFOTECH":
				return INFOTECH;
			case "FINANCIAL":
				return BANKING;
			case "MEDIA":
				return MEDIA;
			case "HEALTHCARE":
				return PHARMA;
			case "PHARMA":
				return PHARMA;
			case "INFRASTRUCTURE":
				return INFRASTRUCTURE;
			case "NATURAL RESOURCES":
				return NATURAL_RESOURCES;
			case "FLEXI":
				return FLEXI;
			case "SERVICES":
				return SERVICES;
			case "MANUFACTURING":
				return MANUFACTURING;
			case "ENTERTAINMENT":
				return ENTERTAINMENT;
			default:
				return -1;
		}
	}
}
