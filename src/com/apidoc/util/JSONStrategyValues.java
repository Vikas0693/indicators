package com.apidoc.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;

public class JSONStrategyValues {

	
	public JSONArray getExitStrategyAlertDates(String mfId,String strategyName) 
	{
		Session hSession=null;
		
		try
		{
			Long mutualFundId=Long.parseLong(mfId);
			String sName="";
			if(strategyName.equalsIgnoreCase("ExitStrategy1"))
				sName="ex1";
			else if(strategyName.equalsIgnoreCase("ExitStrategy2"))
				sName="ex2";
			else if(strategyName.equalsIgnoreCase("ExitStrategy3"))
				sName="ex3";
			else if(strategyName.equalsIgnoreCase("ExitStrategy4"))
				sName="ex4";
			else if(strategyName.equalsIgnoreCase("ExitStrategy5"))
				sName="ex5";
			else if(strategyName.equalsIgnoreCase("ExitStrategy6"))
				sName="ex6";
			else if(strategyName.equalsIgnoreCase("ExitStrategy7"))
				sName="ex7";
			
			hSession=HibernateBridge.getSessionFactory().openSession();
		
			List dates=hSession.createQuery("select tradeDate from CustomMFExitAlert where strategyName like ? and mutualFundId=? order by tradeDate").setString(0,strategyName).setParameter(1, mutualFundId).list();
		
			JSONArray innerArray=null;
			JSONArray outerArray=new JSONArray();
			Iterator itr=dates.iterator();
			while(itr.hasNext())
			{
				innerArray=new JSONArray();
				Date exitTradeDate=(Date)itr.next();
				innerArray.put(0,exitTradeDate);
				innerArray.put(1,sName);
				outerArray.put(innerArray);
			}
			//System.out.println(outerArray.toString(1));
			return outerArray;		
		}
		catch(Exception e)
		{
			e.printStackTrace();
			//throw new Exception("ExitStrategy1 exception for mutualFundId="+mfId);
		}
		finally
		{
			hSession.close();
			
		}		
		return null;
	}
	public static JSONArray getExitStrategyAlertDates(String mfId,String[] strategyName,String sDate,String eDate) 
	{
		Session hSession=null;
		
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			Date startDate=new SimpleDateFormat("yyyy-MM-dd").parse(sDate);
			Date endDate=new SimpleDateFormat("yyyy-MM-dd").parse(eDate);
			Long mutualFundId=Long.parseLong(mfId);
			JSONArray outerJsonArray=new JSONArray();
		
			for(int i=0;i<strategyName.length;i++)
			{
				String strategy=strategyName[i];
			
				String sName="";
				if(strategy.equalsIgnoreCase("ExitStrategy1"))
					sName="ex1";
				else if(strategy.equalsIgnoreCase("ExitStrategy2"))
					sName="ex2";
				else if(strategy.equalsIgnoreCase("ExitStrategy3"))
					sName="ex3";
				else if(strategy.equalsIgnoreCase("ExitStrategy4"))
					sName="ex4";
				else if(strategy.equalsIgnoreCase("ExitStrategy5"))
					sName="ex5";
				else if(strategy.equalsIgnoreCase("ExitStrategy6"))
					sName="ex6";
				else if(strategy.equalsIgnoreCase("ExitStrategy7"))
					sName="ex7";			
			
				List dates=hSession.createQuery("select tradeDate from CustomMFExitAlert where strategyName like ? and mutualFundId=? and tradeDate between ? and ? order by tradeDate").setString(0,strategy).setParameter(1, mutualFundId).setParameter(2, startDate).setParameter(3,endDate).list();
			
				JSONArray innerArray=null;
				Iterator itr1=dates.iterator();
				while(itr1.hasNext())
				{
					innerArray=new JSONArray();
					Date exitTradeDate=(Date)itr1.next();
					innerArray.put(0,exitTradeDate);
					innerArray.put(1,sName);
					outerJsonArray.put(innerArray);
				}
			}
			//System.out.println("ExitAlertDates"+outerJsonArray.toString(1));
			return outerJsonArray;		
		}
		catch(Exception e)
		{
			e.printStackTrace();
			//throw new Exception("ExitStrategy1 exception for mutualFundId="+mfId);
		}
		finally
		{
			hSession.close();
			
		}		
		return null;
	}
	
	public JSONArray getEntryStrategyAlertDates(String mfId,String strategyName) 
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			Long mutualFundId=Long.parseLong(mfId);
			String sName="";
			if(strategyName.equalsIgnoreCase("EntryStrategy1"))
				sName="en1";
			if(strategyName.equalsIgnoreCase("EntryStrategy2"))
				sName="en2";
			if(strategyName.equalsIgnoreCase("EntryStrategy3"))
				sName="en3";
			if(strategyName.equalsIgnoreCase("EntryStrategy4"))
				sName="en4";
			if(strategyName.equalsIgnoreCase("EntryStrategy5"))
				sName="en5";
			if(strategyName.equalsIgnoreCase("EntryStrategy6"))
				sName="en6";
			if(strategyName.equalsIgnoreCase("EntryStrategy7"))
				sName="en7";
			if(strategyName.equalsIgnoreCase("EntryStrategy8"))
				sName="en8";
			if(strategyName.equalsIgnoreCase("EntryStrategy9"))
				sName="en9";
			if(strategyName.equalsIgnoreCase("EntryStrategy10"))
				sName="en10";
			if(strategyName.equalsIgnoreCase("EntryStrategy11"))
				sName="en11";
			if(strategyName.equalsIgnoreCase("EntryStrategy12"))
				sName="en12";
			if(strategyName.equalsIgnoreCase("EntryStrategy13"))
				sName="en13";
			if(strategyName.equalsIgnoreCase("EntryStrategy14"))
				sName="en14";
			if(strategyName.equalsIgnoreCase("EntryStrategy15"))
				sName="en15";
			if(strategyName.equalsIgnoreCase("EntryStrategy16"))
				sName="en16";
			List dates=hSession.createQuery("select tradeDate from CustomMFEntryAlert where strategyName like ? and mutualFundId=? order by tradeDate").setString(0,strategyName).setParameter(1, mutualFundId).list();
		
			JSONArray innerArray=null;
			JSONArray outerArray=new JSONArray();
			Iterator itr=dates.iterator();
			while(itr.hasNext())
			{
				innerArray=new JSONArray();
				Date entryTradeDate=(Date)itr.next();
				innerArray.put(0,entryTradeDate);
				innerArray.put(1,sName);
				outerArray.put(innerArray);
			}
			//System.out.println(outerArray.toString(1));
			return outerArray;		
		}
		catch(Exception e)
		{
			e.printStackTrace();
			//throw new Exception("ExitStrategy1 exception for mutualFundId="+mfId);
		}
		finally
		{
			hSession.close();
			
		}		
		return null;
	}
	
	public static JSONArray getEntryStrategyAlertDates(String mfId,String[] strategyArray,String sDate,String eDate) 
	{
		Session hSession=null;
		Date startDate=null;
		Date endDate=null;
		try
		{
			JSONArray outerJsonArray=new JSONArray();
		
			startDate=new SimpleDateFormat("yyyy-MM-dd").parse(sDate);
			endDate=new SimpleDateFormat("yyyy-MM-dd").parse(eDate);
			Long mutualFundId=Long.parseLong(mfId);
			hSession=HibernateBridge.getSessionFactory().openSession();
			for(int i=0;i<strategyArray.length;i++)
			{
				String strategyName=strategyArray[i];
			
				String sName="";
				if(strategyName.equalsIgnoreCase("EntryStrategy1"))
					sName="en1";
				if(strategyName.equalsIgnoreCase("EntryStrategy2"))
					sName="en2";
				if(strategyName.equalsIgnoreCase("EntryStrategy3"))
					sName="en3";
				if(strategyName.equalsIgnoreCase("EntryStrategy4"))
					sName="en4";
				if(strategyName.equalsIgnoreCase("EntryStrategy5"))
					sName="en5";
				if(strategyName.equalsIgnoreCase("EntryStrategy6"))
					sName="en6";
				if(strategyName.equalsIgnoreCase("EntryStrategy7"))
					sName="en7";
				if(strategyName.equalsIgnoreCase("EntryStrategy8"))
					sName="en8";
				if(strategyName.equalsIgnoreCase("EntryStrategy9"))
					sName="en9";
				if(strategyName.equalsIgnoreCase("EntryStrategy10"))
					sName="en10";
				if(strategyName.equalsIgnoreCase("EntryStrategy11"))
					sName="en11";
				if(strategyName.equalsIgnoreCase("EntryStrategy12"))
					sName="en12";
				if(strategyName.equalsIgnoreCase("EntryStrategy13"))
					sName="en13";
				if(strategyName.equalsIgnoreCase("EntryStrategy14"))
					sName="en14";
				if(strategyName.equalsIgnoreCase("EntryStrategy15"))
					sName="en15";
				if(strategyName.equalsIgnoreCase("EntryStrategy16"))
					sName="en16";
						
				List dates=hSession.createQuery("select tradeDate from CustomMFEntryAlert where strategyName like ? and mutualFundId=? and tradeDate between ? and ? order by tradeDate").setString(0,strategyName).setParameter(1, mutualFundId).setParameter(2, startDate).setParameter(3, endDate).list();
			
				JSONArray innerArray=null;
				
				Iterator itr=dates.iterator();
				while(itr.hasNext())
				{
					innerArray=new JSONArray();
					Date entryTradeDate=(Date)itr.next();
					innerArray.put(0,entryTradeDate);
					innerArray.put(1,sName);
					outerJsonArray.put(innerArray);
				}
			}
			//System.out.println("EntryStrategyDates"+outerJsonArray.toString(1));
			return outerJsonArray;		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
			
		}		
		return null;
	}
	
}
