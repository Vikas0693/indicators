package com.apidoc.util;

public class ImageLinks
{

	public static double getRoundedValue(double value)
	{
		return (Math.round((value * 100)) / 100.00);
	}

	public static double getRoundedValue(double value, int precision)
	{
		if(precision == 0)
		{
			return (double) Math.round(value * 1) / 1;
		}
		else if(precision == 1)
		{
			return (double) Math.round(value * 10) / 10;
		}
		else if(precision == 2)
		{
			return (double) Math.round(value * 100) / 100;
		}
		else if(precision == 3)
		{
			return (double) Math.round(value * 1000) / 1000;
		}
		else if(precision == 4)
		{
			return (double) Math.round(value * 10000) / 10000;
		}
		else if(precision == 5)
		{
			return (double) Math.round(value * 100000) / 100000;
		}

		return (double) Math.round(value * 100) / 100;
		// return (Math.round((value * 100)) / 100.00);
	}

	/*public static void main(String[] args) {
	    System.out.println(getRoundedFloatValue(58.7115826261502f, 5));
	    System.out.println(getRoundedFloatValue(58.7114426261502f, 4));
	    System.out.println(getRoundedFloatValue(58.7115826261502f, 3));
	    System.out.println(getRoundedFloatValue(58.7115826261502f, 2));
	    System.out.println(getRoundedFloatValue(58.7115826261502f, 1));
	}*/


	public static float getRoundedFloatValue(float value)
	{
		return (float) (Math.round((value * 100)) / 100.00);
	}

	public static float getRoundedFloatValue(float value, int precision)
	{
		if(precision == 0)
		{
			return (float) Math.round(value * 1) / 1;
		}
		else if(precision == 1)
		{
			return (float) Math.round(value * 10) / 10;
		}
		else if(precision == 2)
		{
			return (float) Math.round(value * 100) / 100;
		}
		else if(precision == 3)
		{
			return (float) Math.round(value * 1000) / 1000;
		}
		else if(precision == 4)
		{
			return (float) Math.round(value * 10000) / 10000;
		}
		else if(precision == 5)
		{
			return (float) Math.round(value * 100000) / 100000;
		}

		return (float) Math.round(value * 100) / 100;
	}
}
