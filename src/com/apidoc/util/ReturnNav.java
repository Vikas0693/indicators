package com.apidoc.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class ReturnNav
 */
@WebServlet("/ReturnNav")
public class ReturnNav extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReturnNav() {
        super();
        // TODO Auto-generated constructor stub
    }
    

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter pw=response.getWriter();
		InputStream is=request.getInputStream();
		System.out.println("Hellow");
		BufferedReader br=new BufferedReader(new InputStreamReader(is));
		String line=br.readLine();
		
		StringBuilder builder=new StringBuilder();
		while(line!=null)
		{
			System.out.println(line);
			builder.append(line);
			line=br.readLine();
		}
		try 
		{
			String amfiiCode;
			Date sDate,eDate;
			JSONArray jArray=new JSONArray(builder.toString());
			/*for(int i=0;i<jArray.length();i++)
			{
				JSONObject jObject=(JSONObject)jArray.get(i);
				sDate=new SimpleDateFormat("yyyy-MM-dd").parse((String)jObject.get("sDate"));
				eDate=new SimpleDateFormat("yyyy-MM-dd").parse((String)jObject.get("seDate"));
			}*/
		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		/*catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			System.out.println("Not able to parse one of the dates");
			e.printStackTrace();
		}*/
		
		String input = "[{\"amfiiCode\":\"141429\",\"data\":[{\"navDate\":\"2017-09-14\",\"nav\":\"5.09\"},{\"navDate\":\"2017-08-14\",\"nav\":\"7.09\"}]}]";
		pw.write(input);
		pw.flush();
	}

}
