package com.apidoc.util;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.json.JSONArray;


public class JSONArrayValues
{


	// since nav is common we kept it separate
	JSONArray nav, ohlc;
	JSONArray upFractal, lowFractal, swingHigh, swingLow, tcfl1, tcfl2, tcfl3;
	JSONArray tenkanSen, kijunSen, senkouSpanA, senkouSpanB, kumoTop, kumoBottom;
	JSONArray ema20, ema50, ema200, macd, macdSignal, macdHistogram, upperBand, middleBand, lowerBand, rsi, psar, adx, atr;


	public void setFractalJson(String mfId, String dailyOrWeekly) throws Exception
	{
		nav = new JSONArray();
		upFractal = new JSONArray();
		lowFractal = new JSONArray();
		swingHigh = new JSONArray();
		swingLow = new JSONArray();
		tcfl1 = new JSONArray();
		tcfl2 = new JSONArray();
		tcfl3 = new JSONArray();
		ohlc = new JSONArray();

		Session hSession = null;
		JSONArray innerArray = null;
		// Transaction tx = null;
		int period=2;
		try
		{
			long mutualFundId = Long.valueOf(mfId);
			int candleType = 1;
			if(dailyOrWeekly == null)
			{
				candleType = 1;
				// throw new Exception("Select daily or weekly candletype");
			}
			else if(dailyOrWeekly.equalsIgnoreCase("daily"))
			{
				candleType = 1;
			}
			else if(dailyOrWeekly.equalsIgnoreCase("weekly"))
			{
				candleType = 0;
			}
			//System.out.println("Fractal CandleType=" + candleType);
			hSession = HibernateBridge.getSessionFactory().openSession();
			List<Object[]> upFractalList = hSession.createQuery("select tradeDate,closePrice,upFractal,lowFractal,swingHigh,swingLow,tcfl1,tcfl2,tcfl3,open,high,low from Fractal where mutualFundId=? and candleType=? and period=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1, candleType).setParameter(2, period)
					.list();
			Date tradeDate = null;
			for (int i = 0; i < upFractalList.size(); i++)
			{
				Object fractal[] = (upFractalList.get(i));
				innerArray = new JSONArray();
				// System.out.println((Double) fractal[0] + "\tdate=" + (Date) fractal[1]);
				tradeDate = (Date) fractal[0];
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) fractal[1]);
				nav.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) fractal[2]);
				upFractal.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) fractal[3]);
				lowFractal.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) fractal[4]);
				swingHigh.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) fractal[5]);
				swingLow.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) fractal[6]);
				tcfl1.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) fractal[7]);
				tcfl2.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) fractal[8]);
				tcfl3.put(innerArray);
				if(candleType == 0)
				{
					innerArray = new JSONArray();
					innerArray.put(0, tradeDate);
					innerArray.put(1, (Double) fractal[9]);
					innerArray.put(2, (Double) fractal[10]);
					innerArray.put(3, (Double) fractal[11]);
					innerArray.put(4, (Double) fractal[1]);
					ohlc.put(innerArray);
				}
			}
			// System.out.println(swingHigh.toString(2));

		}
		catch (Exception e1)
		{
			// System.out.println("error=" + e1.getMessage());
			throw new Exception(e1.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}

	public void setIchimokuJson(String mfId, String dailyOrWeekly)
	{
		nav = new JSONArray();
		tenkanSen = new JSONArray();
		kijunSen = new JSONArray();
		senkouSpanA = new JSONArray();
		senkouSpanB = new JSONArray();
		kumoTop = new JSONArray();
		kumoBottom = new JSONArray();
		ohlc = new JSONArray();

		Session hSession = null;
		JSONArray innerArray = null;
		// Transaction tx = null;

		try
		{
			long mutualFundId = Long.valueOf(mfId);
			int candleType = 1;
			if(dailyOrWeekly == null)
			{
				candleType = 1;
				// throw new Exception("Select daily or weekly candletype");
			}
			else if(dailyOrWeekly.equalsIgnoreCase("daily"))
			{
				candleType = 1;
			}
			else if(dailyOrWeekly.equalsIgnoreCase("weekly"))
			{
				candleType = 0;
			}
			System.out.println("Ichimoku CandleType=" + candleType);
			hSession = HibernateBridge.getSessionFactory().openSession();
			List<Object[]> ichimokuList = hSession.createQuery("select tradeDate,close,tenkanSen,kijunSen,senkouSpanA,senkouSpanB,kumoTop,kumoBottom from Ichimoku where mutualFundId=? and candleType=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1, candleType).list();
			Date tradeDate = null;
			//System.out.println(ichimokuList.size());
			for (int i = 0; i < ichimokuList.size(); i++)
			{
				Object ichimoku[] = (ichimokuList.get(i));
				//System.out.println(ichimoku.length);
				innerArray = new JSONArray();
				// System.out.println((Double) fractal[0] + "\tdate=" + (Date) fractal[1]);
				tradeDate = (Date) ichimoku[0];
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) ichimoku[1]);
				nav.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) ichimoku[2]);
				tenkanSen.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) ichimoku[3]);
				kijunSen.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) ichimoku[4]);
				senkouSpanA.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) ichimoku[5]);
				senkouSpanB.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) ichimoku[6]);
				kumoTop.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) ichimoku[7]);
				kumoBottom.put(innerArray);

			}
			// System.out.println(kumoBottom.toString(2));

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	}

	public void setTalibJson(String mfId, String dailyOrWeekly)
	{
		nav = new JSONArray();
		ema20 = new JSONArray();
		ema50 = new JSONArray();
		ema200 = new JSONArray();
		macd = new JSONArray();
		macdSignal = new JSONArray();
		macdHistogram = new JSONArray();
		upperBand = new JSONArray();
		middleBand = new JSONArray();
		lowerBand = new JSONArray();
		rsi = new JSONArray();
		psar = new JSONArray();
		adx = new JSONArray();
		atr = new JSONArray();
		ohlc = new JSONArray();

		Session hSession = null;
		JSONArray innerArray = null;
		try
		{
			long mutualFundId = Long.valueOf(mfId);
			int candleType = 1;
			if(dailyOrWeekly == null)
			{
				candleType = 1;
				// throw new Exception("Select daily or weekly candletype");
			}
			else if(dailyOrWeekly.equalsIgnoreCase("daily"))
			{
				candleType = 1;
			}
			else if(dailyOrWeekly.equalsIgnoreCase("weekly"))
			{
				candleType = 0;
			}
			System.out.println("Talib CandleType=" + candleType);
			hSession = HibernateBridge.getSessionFactory().openSession();
			List<Object[]> talibList = hSession.createQuery("select tradeDate,todaysClosePrice,ema20,ema50,ema200,macd,macdSignal,macdHistogram,upperBand,middleBand,lowerBand,rsi,psar,adx,atr from EveryDayCandle where mutualFundId=? and candleType=? order by tradeDate")
					.setParameter(0, mutualFundId).setParameter(1, candleType).list();
			Date tradeDate = null;
			System.out.println(talibList.size());
			for (int i = 0; i < talibList.size(); i++)
			{
				Object talib[] = (talibList.get(i));
				//System.out.println(talib.length);

				// System.out.println((Double) fractal[0] + "\tdate=" + (Date) fractal[1]);
				tradeDate = (Date) talib[0];

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) talib[1]);
				nav.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) talib[2]);
				ema20.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) talib[3]);
				ema50.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) talib[4]);
				ema200.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Float) talib[5]);
				macd.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Float) talib[6]);
				macdSignal.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Float) talib[7]);
				macdHistogram.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) talib[8]);
				upperBand.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) talib[9]);
				middleBand.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) talib[10]);
				lowerBand.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Float) talib[11]);
				rsi.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Float) talib[12]);
				psar.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Float) talib[13]);
				adx.put(innerArray);

				innerArray = new JSONArray();
				innerArray.put(0, tradeDate);
				innerArray.put(1, (Double) talib[14]);
				atr.put(innerArray);
			}
			// System.out.println(atr.toString(2));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	}

	public JSONArray getNav()
	{
		return nav;
	}

	public JSONArray getOhlc()
	{
		return ohlc;
	}

	public JSONArray getUpFractal()
	{
		return upFractal;
	}

	public JSONArray getLowFractal()
	{
		return lowFractal;
	}

	public JSONArray getSwingHigh()
	{
		return swingHigh;
	}

	public JSONArray getSwingLow()
	{
		return swingLow;
	}

	public JSONArray getTcfl1()
	{
		return tcfl1;
	}

	public JSONArray getTcfl2()
	{
		return tcfl2;
	}

	public JSONArray getTcfl3()
	{
		return tcfl3;
	}

	public JSONArray getTenkanSen()
	{
		return tenkanSen;
	}

	public JSONArray getKijunSen()
	{
		return kijunSen;
	}

	public JSONArray getSenkouSpanA()
	{
		return senkouSpanA;
	}

	public JSONArray getSenkouSpanB()
	{
		return senkouSpanB;
	}

	public JSONArray getKumoTop()
	{
		return kumoTop;
	}

	public JSONArray getKumoBottom()
	{
		return kumoBottom;
	}

	public JSONArray getEma20()
	{
		return ema20;
	}

	public JSONArray getEma50()
	{
		return ema50;
	}

	public JSONArray getEma200()
	{
		return ema200;
	}

	public JSONArray getMacd()
	{
		return macd;
	}

	public JSONArray getMacdSignal()
	{
		return macdSignal;
	}

	public JSONArray getMacdHistogram()
	{
		return macdHistogram;
	}

	public JSONArray getUpperBand()
	{
		return upperBand;
	}

	public JSONArray getMiddleBand()
	{
		return middleBand;
	}

	public JSONArray getLowerBand()
	{
		return lowerBand;
	}

	public JSONArray getRsi()
	{
		return rsi;
	}

	public JSONArray getPsar()
	{
		return psar;
	}

	public JSONArray getAdx()
	{
		return adx;
	}

	public JSONArray getAtr()
	{
		return atr;
	}




}
