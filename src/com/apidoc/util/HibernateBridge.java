package com.apidoc.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateBridge 
{
	 private static volatile SessionFactory sessionFactory;

	    private HibernateBridge() {
	    }

	    public static SessionFactory getSessionFactory() {
	        if (sessionFactory == null) {
	            synchronized (HibernateBridge.class) {
	                if (sessionFactory == null) {
	                	System.out.println("Open Connection to Indicators");
	                    sessionFactory = (new Configuration().configure("hibernate.cfg.xml")).buildSessionFactory();
	                }
	            }
	        }
	        return sessionFactory;
	    }
	    
	    public static void buildSessionFactory(){
	    	sessionFactory.close();
	    	sessionFactory=null;
	    	sessionFactory = (new Configuration().configure()).buildSessionFactory();
	    }
}
