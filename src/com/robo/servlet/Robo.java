package com.robo.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.robo.controller.Algo;

/**
 * Servlet implementation class Robo
 */
@WebServlet("/Robo")
public class Robo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Robo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		PrintWriter out=response.getWriter();
		JSONArray roboArray=new JSONArray();
		JSONObject roboObject=new JSONObject();
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String amfiiCode=request.getParameter("amfiiCode");
		String investAmount=request.getParameter("investAmount");
		Algo algo=new Algo();
		try
		{
			if(investAmount==null)
			{
				System.out.println("No investment");
				algo.ourRoboAlgo(amfiiCode, startDate, endDate, 100000l);
			}
			else
			{
				Long investment=Long.parseLong(investAmount);
				algo.ourRoboAlgo(amfiiCode, startDate, endDate, investment);
			}
			//algo.ourRoboAlgo(amfiiCode, startDate, endDate, 100000l);
			roboArray=algo.getOuterJsonArray();
			//System.out.println(roboArray.toString(1));
			out.print(roboArray.toString(1));
		}
		catch(Exception e)
		{
			e.printStackTrace();
			try 
			{
				roboObject.put("success",false);
				roboObject.put("error",e.getMessage());
				out.write(roboObject.toString(1));
			} 
			catch (JSONException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}		
		}		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
