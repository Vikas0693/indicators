package com.robo.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.CustomMFExitAlert;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.Nav;
import com.robo.config.Config;

public class Algo {

	//short exitNo=2,entryNo=4;
	String exit="",entry="";
	double condition3SwitchPercentage=100.0d;
	double condition4SwitchPercentage=50.0d;
	double condition1EquitySwitchPercentage=20.0d;
	short noOfMonthsForReverseSwitch=12;
	short configurableBestDate=25;
	
	JSONArray equityJsonArray=new JSONArray();
	JSONArray roboJsonArray=new JSONArray();
	
	JSONArray conditionJsonArray=new JSONArray();
	JSONArray outerEquityAmountJsonArray=new JSONArray();
	JSONArray outerLiquidAmountJsonArray=new JSONArray();
	
	JSONArray outerActionJsonArray=new JSONArray();
	JSONArray outerJsonArray=new JSONArray();
	
	public Algo() throws IOException
	{
		Config config=new Config();
		exit=config.getProperty("exit");
		entry=config.getProperty("entry");
		
		condition3SwitchPercentage=Double.valueOf(config.getProperty("condition3SwitchPercentage"));
		condition4SwitchPercentage=Double.valueOf(config.getProperty("condition4SwitchPercentage"));
		condition1EquitySwitchPercentage=Double.valueOf(config.getProperty("condition1EquitySwitchPercentage"));
		noOfMonthsForReverseSwitch=Short.valueOf(config.getProperty("noOfMonthsForReverseSwitch"));
		configurableBestDate=Short.valueOf(config.getProperty("configurableBestDate"));
		//System.out.println(condition3SwitchPercentage+" "+condition4SwitchPercentage+" "+condition1EquitySwitchPercentage+" "+noOfMonthsForReverseSwitch+" "+configurableBestDate);
	}
	//as per pseudocode sent in mail
	public void ourRoboAlgo(long mutualFundId,String sDate,String eDate,Long investedAmount) throws IOException
	{		
		
		JSONArray innerEquityJsonArray=new JSONArray();
		JSONArray innerRoboJsonArray=new JSONArray();
		
		JSONArray innerEquityAmountJsonArray=new JSONArray();
		JSONArray innerLiquidAmountJsonArray=new JSONArray();
		
		
		
		double currentEquityNav;
		double currentLiquidNav;
		double currentEquityUnits;
		double currentLiquidUnits;
		double totalAmountEquity;
		double totalAmountLiquid;
		double totalAmount=investedAmount;
		
		//1:buy,0:sell
		short lastSignal=1;
		double navAtLastSignal=0;
		boolean isReverseSwitchOn=false;
		short noOfMonthReverseSwitchPending=0;
		double totalDesiredAmountOfReverseSwitchPending=0;
		double monthlyAmountOfReverseSwitch=0;
		
		double equityUnitsAllocatedInitially=0;
		double liquidUnitsAllocatedInitially=0;
		
		Date currentDate=null;
		
		//we choose -1 bcoz 0 represent January
		int currentMonth=-1,previousMonth=-1,currentDay=0;
		Date bestDate=null;
		boolean bestDateOccurredFlag=false;
		
		Session hSession=null;
		
		Date startDate=null,endDate=null;
		JSONArray innerJsonArray=null;
		//assuming growth fund is dhfl and liquid fund is dhfl insta cash flow
		try
		{			
			hSession=HibernateBridge.getSessionFactory().openSession();
			startDate=new SimpleDateFormat("yyyy-MM-dd").parse(sDate);
			if(eDate==null)
				endDate=new Date();
			else
				endDate=new SimpleDateFormat("yyyy-MM-dd").parse(eDate);
			MutualFund equityFund=(MutualFund)hSession.createQuery("from MutualFund where id=?").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
			MutualFund liquidFund=equityFund.getLiquidFund();
			long liquidFundId=liquidFund.getId();
			List<Nav> equityNavList=hSession.createQuery("from Nav where mutualfundId=? and tradeDate between ? and ? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,startDate).setParameter(2, endDate).list();
			List<Nav> liquidNavList=null;

			Nav equityNav=null;
			Nav liquidNav=null;
			
			System.out.println("mutualFundId="+mutualFundId+"   liquidId="+liquidFundId+"   startDate="+startDate+"  endDate="+endDate);
			if(equityNavList!=null && equityNavList.size()>0)
			{
				equityNav=(Nav)equityNavList.get(0);
				Date startEquityDate=equityNav.getTradeDate();
				
				//System.out.println(startEquityDate+" startEDate");
				//System.out.println("liquidFundId "+liquidFundId);
				liquidNavList=hSession.createQuery("from Nav where mutualFundId=? and tradeDate>=? order by tradeDate").setParameter(0,liquidFundId).setParameter(1,startEquityDate).list();
				//System.out.println("liquidNavList.size()="+liquidNavList.size());
				
				equityUnitsAllocatedInitially=((investedAmount).doubleValue())/(equityNav.getNav());
				
				liquidNav=(Nav)liquidNavList.get(0);
				liquidUnitsAllocatedInitially=((investedAmount).doubleValue())/(liquidNav.getNav());

				Date entryAlertDate=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and tradeDate<=? and strategyName in (:entryList)").setParameter(0, mutualFundId).setParameter(1,startDate).setParameterList("entryList",getValidEntryList(mutualFundId,hSession)).setMaxResults(1).uniqueResult();
				Date exitAlertDate=(Date)hSession.createQuery("select max(tradeDate) from CustomMFExitAlert where mutualFundId=? and tradeDate<=? and strategyName in (:exitList)").setParameter(0, mutualFundId).setParameter(1,startDate).setParameterList("exitList",getValidExitList(mutualFundId, hSession)).setMaxResults(1).uniqueResult();
				
				//System.out.println("En="+entryAlertDate+"   exitDate="+exitAlertDate);
				if((entryAlertDate!=null && exitAlertDate!=null && entryAlertDate.compareTo(exitAlertDate)>0) || (exitAlertDate==null && entryAlertDate!=null))
				{
					currentEquityNav=equityNav.getNav();
					currentLiquidNav=liquidNav.getNav();
					
					totalAmountEquity=totalAmount*(0.8);
					totalAmountLiquid=totalAmount-totalAmountEquity;
					
					currentEquityUnits=totalAmountEquity/currentEquityNav;
					currentLiquidUnits=totalAmountLiquid/currentLiquidNav;
					lastSignal=1;
					//System.out.println(totalAmountEquity+"\t"+currentEquityNav+"\t"+totalAmountLiquid+"\t"+currentLiquidNav+"\t"+currentEquityUnits+"\t"+currentLiquidUnits);
					
					equityNavList.remove(0);
					liquidNavList.remove(0);
				
				}
				else
				{
					currentEquityNav=equityNav.getNav();
					currentLiquidNav=liquidNav.getNav();
					
					totalAmountEquity=totalAmount*(0.5);
					totalAmountLiquid=totalAmount-totalAmountEquity;
					
					currentEquityUnits=totalAmountEquity/currentEquityNav;
					currentLiquidUnits=totalAmountLiquid/currentLiquidNav;
					lastSignal=0;
					//System.out.println(totalAmountEquity+"\t"+totalAmountLiquid+"\t"+currentEquityUnits+"\t"+currentLiquidUnits);
				
					equityNavList.remove(0);
					liquidNavList.remove(0);
				}
				innerEquityJsonArray.put(0,startEquityDate);
				innerEquityJsonArray.put(1,equityUnitsAllocatedInitially*currentEquityNav);
				equityJsonArray.put(innerEquityJsonArray);
				
				innerRoboJsonArray.put(0,startEquityDate);
				innerRoboJsonArray.put(1,totalAmount);
				roboJsonArray.put(innerRoboJsonArray);
				
				innerEquityAmountJsonArray.put(0,currentEquityNav);
				innerEquityAmountJsonArray.put(1,totalAmountEquity);
				outerEquityAmountJsonArray.put(innerEquityAmountJsonArray);
				
				innerLiquidAmountJsonArray.put(0,currentLiquidNav);
				innerLiquidAmountJsonArray.put(1,totalAmountLiquid);
				outerLiquidAmountJsonArray.put(innerLiquidAmountJsonArray);				
				int j=0;
				for(int i=0;i<equityNavList.size();i++)
				{
					
					equityNav=(Nav)equityNavList.get(i);
					currentDate=equityNav.getTradeDate();
					
					if(j>=liquidNavList.size()-1)
					{
						j=liquidNavList.size()-1;
						liquidNav=liquidNavList.get(liquidNavList.size()-1);
						j--;
					}
					else
					{
						liquidNav=liquidNavList.get(j);
					}
					Date currentLiquidDate=liquidNav.getTradeDate();
					if(currentLiquidDate.compareTo(currentDate)<0)
					{
						while(currentLiquidDate.compareTo(currentDate)<0 && j<liquidNavList.size()-1)
						{
							j++;
							liquidNav=liquidNavList.get(j);
							currentLiquidDate=liquidNav.getTradeDate();
							
						}
						//System.out.println("currentDate="+currentDate+"   currentLiquidDate1="+currentLiquidDate);
					}
					if(currentLiquidDate.compareTo(currentDate)>0)
					{
						liquidNav=liquidNavList.get(--j);
						currentLiquidDate=liquidNav.getTradeDate();
						//System.out.println("currentDate="+currentDate+"   currentLiquidDate2="+currentLiquidDate);
					}
					else
					{
						j++;
					}		
					//System.out.println("finally   :::   currentDate="+currentDate+"   currentLiquidDate3="+currentLiquidDate);
					currentLiquidNav=liquidNav.getNav();
					currentEquityNav=equityNav.getNav();				
					
					Calendar cal1=Calendar.getInstance();
					cal1.setTime(currentDate);
					currentMonth=cal1.get(Calendar.MONTH);
					
					//System.out.println(equityNav.getTradeDate()+"totalA="+totalAmount+"totalE"+totalAmountEquity);
					
					//6)
					totalAmountEquity=currentEquityNav*currentEquityUnits;
					totalAmountLiquid=currentLiquidNav*currentLiquidUnits;
					totalAmount=totalAmountEquity+totalAmountLiquid;
					//System.out.println(totalAmount+" \t "+equityUnitsAllocatedInitially*currentEquityNav+" cEDate="+currentDate+" cLDate="+currentLiquidDate);
					//System.out.println("condition6  "+totalAmount+" totAmtEquity="+totalAmountEquity+"  totAmtLiquid="+String.format("%.3f",totalAmountLiquid)+" cEUnits="+currentEquityUnits+" cLUnits="+String.format("%.3f", currentLiquidUnits)+"  Enav"+currentEquityNav+" Lnav="+currentLiquidNav+"  "+currentDate);
					
					//System.out.println(totalAmount+"\t"+equityUnitsAllocatedInitially*currentEquityNav);
					
					//1)
					if(isCurrentExit(mutualFundId,currentDate,hSession) && lastSignal==1 && totalAmountEquity > (totalAmount*(condition1EquitySwitchPercentage/100.0)) )
					{
						lastSignal=0;
					    navAtLastSignal=currentEquityNav;
						double excessAmountOfEquityOver20Percent=totalAmountEquity-(totalAmount*(condition1EquitySwitchPercentage/100.0));
					    double excessUnitsOfEquityOver20Percent=excessAmountOfEquityOver20Percent/currentEquityNav;

					    currentEquityUnits=currentEquityUnits-excessUnitsOfEquityOver20Percent;
					    totalAmountEquity=currentEquityUnits*currentEquityNav;
					   // totalAmountLiquid=totalAmount-totalAmountEquity;
					    currentLiquidUnits=currentLiquidUnits+(excessAmountOfEquityOver20Percent/currentLiquidNav);
					    totalAmountLiquid=currentLiquidUnits*currentLiquidNav;
					    totalAmount=totalAmountEquity+totalAmountLiquid;
					    
					    noOfMonthReverseSwitchPending=noOfMonthsForReverseSwitch;
					    totalDesiredAmountOfReverseSwitchPending=totalAmountLiquid;
					    monthlyAmountOfReverseSwitch=totalDesiredAmountOfReverseSwitchPending/noOfMonthReverseSwitchPending;
					    isReverseSwitchOn=true;
					   // System.out.println("condition1  "+totalAmount+"  currentDate="+currentDate+" ceUnits="+currentEquityUnits+" cLUnits="+String.format("%.3f",currentLiquidUnits)+"tAmtEquity="+String.format("%.3f",totalAmountEquity)+" tAmtLiquid="+totalAmountLiquid);
					    //System.out.println("condition1  "+totalDesiredAmountOfReverseSwitchPending+"  noOfMnthRSPending="+noOfMonthReverseSwitchPending+"  monthlyAmountOfReverseSwitch="+monthlyAmountOfReverseSwitch);
					
					    innerJsonArray=new JSONArray();
					    innerJsonArray.put(0,currentDate);
					    innerJsonArray.put(1,"cond1");
					    conditionJsonArray.put(innerJsonArray);
					}
					//3)
					List<String> entryList=hSession.createQuery("select strategyName from CustomMFEntryAlert where mutualFundId=? and tradeDate=?").setParameter(0,mutualFundId).setParameter(1,currentDate).list();
					if(entryList.contains("EntryStrategy3") || (lastSignal==0 && (entryList.contains("EntryStrategy11") || entryList.contains("EntryStrategy15") || entryList.contains("EntryStrategy16")) && navAtLastSignal!=0 && currentEquityNav>navAtLastSignal) || (lastSignal==1 && entryList.contains("EntryStrategy16")))
					{
						lastSignal=1;
						navAtLastSignal=currentEquityNav;
						//switching 100% to equity
						double switchAmount=totalAmountLiquid*(condition3SwitchPercentage/100.0);
						//totalAmountEquity=totalAmountEquity+switchAmount;
						currentEquityUnits=currentEquityUnits+(switchAmount/currentEquityNav);
						totalAmountEquity=currentEquityUnits*currentEquityNav;
						//totalAmountLiquid=totalAmountLiquid-switchAmount;
						//System.out.println("cLu="+currentLiquidUnits+" switchAmount="+switchAmount+" cLN="+currentLiquidNav);
					    currentLiquidUnits=currentLiquidUnits-(switchAmount/currentLiquidNav);
					   
					    if(currentLiquidUnits<.001)
					    	currentLiquidUnits=0;
					    
					    //System.out.println("cond3="+currentDate+"\t"+currentLiquidUnits);
					    
					    totalAmountLiquid=currentLiquidUnits*currentLiquidNav;
					   
					    totalAmount=totalAmountEquity+totalAmountLiquid;
					    
					    isReverseSwitchOn=false;
					    noOfMonthReverseSwitchPending=0;
					    totalDesiredAmountOfReverseSwitchPending=0;
					    //System.out.println("condition3  "+totalAmount+"  currentDate="+currentDate+" tAEquity="+totalAmountEquity+" tALiquid"+String.format("%.3f",totalAmountLiquid)+" currentEUnits="+currentEquityUnits+" cLUnits="+String.format("%.2f",currentLiquidUnits));
					    innerJsonArray=new JSONArray();
					    innerJsonArray.put(0,currentDate);
					    innerJsonArray.put(1,"cond3");
					    conditionJsonArray.put(innerJsonArray);
					}
					
					//4)
					if(lastSignal==0 && (entryList.contains("EntryStrategy11") || entryList.contains("EntryStrategy15") || entryList.contains("EntryStrategy16")) && navAtLastSignal!=0 && currentEquityNav < navAtLastSignal)
					{
						lastSignal=1;
						navAtLastSignal=currentEquityNav;
						
						double switchAmount=totalAmountLiquid*(condition4SwitchPercentage/100.0);
						//totalAmountEquity=totalAmountEquity+switchAmount;
						//currentEquityUnits=totalAmountEquity/currentEquityNav;
						currentEquityUnits=currentEquityUnits+(switchAmount/currentEquityNav);
						totalAmountEquity=currentEquityUnits*currentEquityNav;
						//totalAmountLiquid=totalAmountLiquid-switchAmount;
					    currentLiquidUnits=currentLiquidUnits-(switchAmount/currentLiquidNav);
					    totalAmountLiquid=currentLiquidUnits*currentLiquidNav;
					    totalAmount=totalAmountEquity+totalAmountLiquid;
					    
					    if(totalDesiredAmountOfReverseSwitchPending>switchAmount)
					    {
					    	totalDesiredAmountOfReverseSwitchPending=totalDesiredAmountOfReverseSwitchPending-switchAmount;
					    	isReverseSwitchOn=true;
					    	noOfMonthReverseSwitchPending=3;
					    	//System.out.println("condition4.1  "+totalAmount+"  currentDate="+currentDate);
					    }
					    else
					    {
					    	totalDesiredAmountOfReverseSwitchPending=0;
					    	isReverseSwitchOn=false;
					    	noOfMonthReverseSwitchPending=0;
					    	//System.out.println("condition4.2  "+totalAmount+"  currentDate="+currentDate);
					    }
					    innerJsonArray=new JSONArray();
					    innerJsonArray.put(0,currentDate);
					    innerJsonArray.put(1,"cond4");
					    conditionJsonArray.put(innerJsonArray);
					}
					if(previousMonth!=currentMonth)
					{
						Calendar cal=Calendar.getInstance();
						cal.setTime(currentDate);
						String month=Integer.toString(cal.get(Calendar.MONTH)+1);
						String year=Integer.toString(cal.get(Calendar.YEAR));
						String date=year+"-"+month+"-"+Short.toString(configurableBestDate);
						bestDate=new SimpleDateFormat("yyyy-MM-dd").parse(date);
						bestDateOccurredFlag=false;
						previousMonth=currentMonth;
					}
					
					//5) to be run on 25th of every month
					if(currentDate.compareTo(bestDate)>=0 && bestDateOccurredFlag==false)
					{
						bestDateOccurredFlag=true;
						if(isReverseSwitchOn==true)
						{
							noOfMonthReverseSwitchPending--;
							totalDesiredAmountOfReverseSwitchPending=totalDesiredAmountOfReverseSwitchPending-monthlyAmountOfReverseSwitch;
							
							//totalAmountEquity=totalAmountEquity+monthlyAmountOfReverseSwitch;
							currentEquityUnits=currentEquityUnits+monthlyAmountOfReverseSwitch/currentEquityNav;
							totalAmountEquity=currentEquityUnits*currentEquityNav;
							//totalAmountLiquid=totalAmountLiquid-monthlyAmountOfReverseSwitch;
							currentLiquidUnits=currentLiquidUnits-monthlyAmountOfReverseSwitch/currentLiquidNav;
							totalAmountLiquid=currentLiquidUnits*currentLiquidNav;
							
							totalAmount=totalAmountEquity+totalAmountLiquid;
							
							if(noOfMonthReverseSwitchPending==0)
							{
								isReverseSwitchOn=false;
								noOfMonthReverseSwitchPending=0;
								totalDesiredAmountOfReverseSwitchPending=0;
								//System.out.println("condition5.1  "+totalAmount+"  bestDate:"+bestDate+"  currentDate:"+currentDate);
							}
							else
							{
								//System.out.println("condition5.2  "+totalAmount+"  bestDate:"+bestDate+" tAEquity="+String.format("%.3f",totalAmountEquity)+" tALiquid="+String.format("%.3f",totalAmountLiquid)+" monthlySwitchAmount="+monthlyAmountOfReverseSwitch+" totalDesiredAmountOfSwitchPending="+totalDesiredAmountOfReverseSwitchPending);
							}
						}
						else
						{
							//System.out.println("condition5.3  "+totalAmount+"  bestDate:"+bestDate+"  currentDate:"+currentDate+"  tAEquity="+totalAmountEquity+" tALiquid="+String.format("%.3f",totalAmountLiquid));
						}
					}
					
					innerEquityJsonArray=new JSONArray();
					innerEquityJsonArray.put(0,currentDate);
					innerEquityJsonArray.put(1,equityUnitsAllocatedInitially*currentEquityNav);
					equityJsonArray.put(innerEquityJsonArray);
					
					innerRoboJsonArray=new JSONArray();
					innerRoboJsonArray.put(0,currentDate);
					innerRoboJsonArray.put(1,totalAmount);
					roboJsonArray.put(innerRoboJsonArray);	
					
					innerEquityAmountJsonArray=new JSONArray();
					innerEquityAmountJsonArray.put(0,currentEquityNav);
					innerEquityAmountJsonArray.put(1,totalAmountEquity);
					outerEquityAmountJsonArray.put(innerEquityAmountJsonArray);
					
					innerLiquidAmountJsonArray=new JSONArray();
					innerLiquidAmountJsonArray.put(0,currentLiquidNav);
					innerLiquidAmountJsonArray.put(1,totalAmountLiquid);
					outerLiquidAmountJsonArray.put(innerLiquidAmountJsonArray);
					
					//System.out.println("Date="+currentDate+" RoboAmount="+totalAmount+"currLiquidDate="+currentLiquidDate+" liquidNavListsize="+liquidNavList.size());
				}				
			}
			else
			{
				throw new Exception("missing data for nav dates");
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	}
	public void ourRoboAlgo(String amfiiCode,String sDate,String eDate,Long investedAmount) throws Exception
	{		
		//for robo amount/date
		JSONObject innerRoboJson=null;
		JSONArray outerRoboJsonArray=new JSONArray();
		
		//for date/condition
		JSONObject innerActionJson=null;
		//for date/nav/liquidNav/units/liquidUnits/action/actionAmount/actionUnits
		JSONObject innerJson=null;
		
		double currentEquityNav;
		double currentLiquidNav;
		double currentEquityUnits;
		double currentLiquidUnits;
		double totalAmountEquity;
		double totalAmountLiquid;
		double totalAmount=investedAmount;
		
		//1:buy,0:sell
		short lastSignal=1;
		double navAtLastSignal=0;
		boolean isReverseSwitchOn=false;
		short noOfMonthReverseSwitchPending=0;
		double totalDesiredAmountOfReverseSwitchPending=0;
		double monthlyAmountOfReverseSwitch=0;
		
		double equityUnitsAllocatedInitially=0;
		double liquidUnitsAllocatedInitially=0;
		
		Date currentDate=null;
		
		//we choose -1 bcoz 0 represent January
		int currentMonth=-1,previousMonth=-1,currentDay=0;
		Date bestDate=null;
		boolean bestDateOccurredFlag=false;
		
		Session hSession=null;
		
		Date startDate=null,endDate=null;
		//assuming growth fund is dhfl and liquid fund is dhfl insta cash flow
		try
		{			
			hSession=HibernateBridge.getSessionFactory().openSession();
			
			
			
			
			MutualFund mutualFund=(MutualFund)hSession.createQuery("from MutualFund where amfiiCode=? and isRobo=1").setParameter(0, amfiiCode).setMaxResults(1).uniqueResult();
			MutualFund liquidFund=mutualFund.getLiquidFund();
			long mutualFundId=mutualFund.getId();
			long liquidFundId=liquidFund.getId();
			
			if(sDate==null)
			{
				startDate=(Date)hSession.createQuery("select min(tradeDate) from Nav where mutualFundId=?").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
			}
			else
			{
				startDate=new SimpleDateFormat("yyyy-MM-dd").parse(sDate);
			}
			
			if(eDate==null)
				endDate=new Date();
			else
				endDate=new SimpleDateFormat("yyyy-MM-dd").parse(eDate);
			if(endDate.compareTo(startDate)<0)
				throw new Exception("End Date is smaller.");
			
			List<Nav> equityNavList=hSession.createQuery("from Nav where mutualfundId=? and tradeDate between ? and ? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,startDate).setParameter(2, endDate).list();
			List<Nav> liquidNavList=null;

			Nav equityNav=null;
			Nav liquidNav=null;
			
			System.out.println("mutualFundId="+mutualFundId+"   liquidId="+liquidFundId+"   startDate="+startDate+"  endDate="+endDate);
			if(equityNavList!=null && equityNavList.size()>0)
			{
				equityNav=(Nav)equityNavList.get(0);
				Date startEquityDate=equityNav.getTradeDate();
				
				//System.out.println(startEquityDate+" startEDate");
				//System.out.println("liquidFundId "+liquidFundId);
				liquidNavList=hSession.createQuery("from Nav where mutualFundId=? and tradeDate>=? order by tradeDate").setParameter(0,liquidFundId).setParameter(1,startEquityDate).list();
				if(liquidNavList!=null && liquidNavList.size()==0)
					throw new Exception("no data");
				//System.out.println("liquidNavList.size="+liquidNavList.size());
				equityUnitsAllocatedInitially=((investedAmount).doubleValue())/(equityNav.getNav());
				
				liquidNav=(Nav)liquidNavList.get(0);
				liquidUnitsAllocatedInitially=((investedAmount).doubleValue())/(liquidNav.getNav());

				Date entryAlertDate=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and tradeDate<=? and strategyName in (:entryList)").setParameter(0, mutualFundId).setParameter(1,startDate).setParameterList("entryList",getValidEntryList(mutualFundId,hSession)).setMaxResults(1).uniqueResult();
				Date exitAlertDate=(Date)hSession.createQuery("select max(tradeDate) from CustomMFExitAlert where mutualFundId=? and tradeDate<=? and strategyName in (:exitList)").setParameter(0, mutualFundId).setParameter(1,startDate).setParameterList("exitList",getValidExitList(mutualFundId, hSession)).setMaxResults(1).uniqueResult();
				
				//System.out.println("En="+entryAlertDate+"   exitDate="+exitAlertDate);
				if((entryAlertDate!=null && exitAlertDate!=null && entryAlertDate.compareTo(exitAlertDate)>0) || (exitAlertDate==null && entryAlertDate!=null))
				{
					currentEquityNav=equityNav.getNav();
					currentLiquidNav=liquidNav.getNav();
					
					totalAmountEquity=totalAmount*(0.8);
					totalAmountLiquid=totalAmount-totalAmountEquity;
					
					currentEquityUnits=totalAmountEquity/currentEquityNav;
					currentLiquidUnits=totalAmountLiquid/currentLiquidNav;
					lastSignal=1;
					//System.out.println(totalAmountEquity+"\t"+currentEquityNav+"\t"+totalAmountLiquid+"\t"+currentLiquidNav+"\t"+currentEquityUnits+"\t"+currentLiquidUnits);
					
					equityNavList.remove(0);
					liquidNavList.remove(0);
				
				}
				else
				{
					currentEquityNav=equityNav.getNav();
					currentLiquidNav=liquidNav.getNav();
					
					totalAmountEquity=totalAmount*(0.5);
					totalAmountLiquid=totalAmount-totalAmountEquity;
					
					currentEquityUnits=totalAmountEquity/currentEquityNav;
					currentLiquidUnits=totalAmountLiquid/currentLiquidNav;
					lastSignal=0;
					//System.out.println(totalAmountEquity+"\t"+totalAmountLiquid+"\t"+currentEquityUnits+"\t"+currentLiquidUnits);
				
					equityNavList.remove(0);
					liquidNavList.remove(0);
				}
					
				innerRoboJson=new JSONObject();
				innerRoboJson.put("date", startEquityDate);
				innerRoboJson.put("amount",totalAmount);
				outerRoboJsonArray.put(innerRoboJson);
				
				
				
				boolean flag=false;
				int j=0;
				for(int i=0;i<equityNavList.size();i++)
				{
					
					equityNav=(Nav)equityNavList.get(i);
					currentDate=equityNav.getTradeDate();			
					if(j>=liquidNavList.size()-1)
					{
						j=liquidNavList.size()-1;
						liquidNav=liquidNavList.get(liquidNavList.size()-1);
						j--;
					}
					else
					{
						liquidNav=liquidNavList.get(j);
					}
					Date currentLiquidDate=liquidNav.getTradeDate();
					if(currentLiquidDate.compareTo(currentDate)<0)
					{
						while(currentLiquidDate.compareTo(currentDate)<0 && j<liquidNavList.size()-1)
						{
							j++;
							liquidNav=liquidNavList.get(j);
							currentLiquidDate=liquidNav.getTradeDate();
							
						}
					}
					if(currentLiquidDate.compareTo(currentDate)>0)
					{
						liquidNav=liquidNavList.get(--j);
						currentLiquidDate=liquidNav.getTradeDate();
					}
					else
					{
						j++;
					}	
					
					currentLiquidNav=liquidNav.getNav();
					currentEquityNav=equityNav.getNav();				
					
					Calendar cal1=Calendar.getInstance();
					cal1.setTime(currentDate);
					currentMonth=cal1.get(Calendar.MONTH);
					
					//System.out.println(equityNav.getTradeDate()+"totalA="+totalAmount+"totalE"+totalAmountEquity);
					flag=false;
					//6)
					totalAmountEquity=currentEquityNav*currentEquityUnits;
					totalAmountLiquid=currentLiquidNav*currentLiquidUnits;
					totalAmount=totalAmountEquity+totalAmountLiquid;
					
					innerJson=new JSONObject();
					innerJson.put("mfValue",equityUnitsAllocatedInitially*currentEquityNav);
					
					//System.out.println(totalAmount+" \t "+equityUnitsAllocatedInitially*currentEquityNav+" cEDate="+currentDate+" cLDate="+currentLiquidDate);
					//System.out.println("condition6  "+totalAmount+" totAmtEquity="+totalAmountEquity+"  totAmtLiquid="+String.format("%.3f",totalAmountLiquid)+" cEUnits="+currentEquityUnits+" cLUnits="+String.format("%.3f", currentLiquidUnits)+"  Enav"+currentEquityNav+" Lnav="+currentLiquidNav+"  "+currentDate);
					
					//System.out.println(totalAmount+"\t"+equityUnitsAllocatedInitially*currentEquityNav);
					
					//1)
					if(isCurrentExit(mutualFundId,currentDate,hSession) && lastSignal==1 && totalAmountEquity > (totalAmount*(condition1EquitySwitchPercentage/100.0)) )
					{
						flag=true;
						lastSignal=0;
					    navAtLastSignal=currentEquityNav;
						
					    double excessAmountOfEquityOver20Percent=totalAmountEquity-(totalAmount*(condition1EquitySwitchPercentage/100.0));
					    double exitAmount=excessAmountOfEquityOver20Percent;
						
					    double excessUnitsOfEquityOver20Percent=excessAmountOfEquityOver20Percent/currentEquityNav;
						double exitUnits=excessUnitsOfEquityOver20Percent;
						
					    currentEquityUnits=currentEquityUnits-excessUnitsOfEquityOver20Percent;
					    totalAmountEquity=currentEquityUnits*currentEquityNav;
					   // totalAmountLiquid=totalAmount-totalAmountEquity;
					    currentLiquidUnits=currentLiquidUnits+(excessAmountOfEquityOver20Percent/currentLiquidNav);
					    totalAmountLiquid=currentLiquidUnits*currentLiquidNav;
					    totalAmount=totalAmountEquity+totalAmountLiquid;
					    
					    noOfMonthReverseSwitchPending=noOfMonthsForReverseSwitch;
					    totalDesiredAmountOfReverseSwitchPending=totalAmountLiquid;
					    monthlyAmountOfReverseSwitch=totalDesiredAmountOfReverseSwitchPending/noOfMonthReverseSwitchPending;
					    isReverseSwitchOn=true;
					   // System.out.println("condition1  "+totalAmount+"  currentDate="+currentDate+" ceUnits="+currentEquityUnits+" cLUnits="+String.format("%.3f",currentLiquidUnits)+"tAmtEquity="+String.format("%.3f",totalAmountEquity)+" tAmtLiquid="+totalAmountLiquid);
					   // System.out.println("condition1  "+totalDesiredAmountOfReverseSwitchPending+"  noOfMnthRSPending="+noOfMonthReverseSwitchPending+"  monthlyAmountOfReverseSwitch="+monthlyAmountOfReverseSwitch);
					
					    innerActionJson=new JSONObject();
					    innerActionJson.put("action","EXIT");
					    innerActionJson.put("date",currentDate);
					    outerActionJsonArray.put(innerActionJson);
					    
					    
					    
						innerJson.put("date",new SimpleDateFormat("yyyy-MM-dd").format(currentDate));
						innerJson.put("nav",currentEquityNav);
						innerJson.put("liquidNav",currentLiquidNav);
						innerJson.put("units",currentEquityUnits);
						innerJson.put("liquidUnits",currentLiquidUnits);
						innerJson.put("action","EXIT");
						innerJson.put("entryAmount",exitAmount);
						innerJson.put("entryUnits",exitUnits);
						innerJson.put("roboValue",totalAmount);
						outerJsonArray.put(innerJson);
					}
					//3)
					List<String> entryList=hSession.createQuery("select strategyName from CustomMFEntryAlert where mutualFundId=? and tradeDate=?").setParameter(0,mutualFundId).setParameter(1,currentDate).list();
					if(entryList.contains("EntryStrategy3") || (lastSignal==0 && (entryList.contains("EntryStrategy11") || entryList.contains("EntryStrategy15") || entryList.contains("EntryStrategy16")) && navAtLastSignal!=0 && currentEquityNav>navAtLastSignal) || (lastSignal==1 && entryList.contains("EntryStrategy16")))
					{
						flag=true;
						lastSignal=1;
						navAtLastSignal=currentEquityNav;
						//switching 100% to equity
						double switchAmount=totalAmountLiquid*(condition3SwitchPercentage/100.0);
						double entryAmount=switchAmount;
						//totalAmountEquity=totalAmountEquity+switchAmount;
						double entryUnits=switchAmount/currentEquityNav;
						currentEquityUnits=currentEquityUnits+entryUnits;
						totalAmountEquity=currentEquityUnits*currentEquityNav;
						//totalAmountLiquid=totalAmountLiquid-switchAmount;
					    currentLiquidUnits=currentLiquidUnits-(switchAmount/currentLiquidNav);
					    totalAmountLiquid=currentLiquidUnits*currentLiquidNav;
					    totalAmount=totalAmountEquity+totalAmountLiquid;
					    
					    isReverseSwitchOn=false;
					    noOfMonthReverseSwitchPending=0;
					    totalDesiredAmountOfReverseSwitchPending=0;
					   // System.out.println("condition3  "+totalAmount+"  currentDate="+currentDate+" tAEquity="+totalAmountEquity+" tALiquid"+String.format("%.3f",totalAmountLiquid)+" currentEUnits="+currentEquityUnits+" cLUnits="+String.format("%.2f",currentLiquidUnits));
					    innerActionJson=new JSONObject();
					    innerActionJson.put("action","AUTOCORRECT");
					    innerActionJson.put("date",currentDate);
					    outerActionJsonArray.put(innerActionJson);
					    
					    
						innerJson.put("date",new SimpleDateFormat("yyyy-MM-dd").format(currentDate));
						innerJson.put("nav",currentEquityNav);
						innerJson.put("liquidNav",currentLiquidNav);
						innerJson.put("units",currentEquityUnits);
						innerJson.put("liquidUnits",currentLiquidUnits);
						innerJson.put("action","AUTOCORRECT");
						innerJson.put("entryAmount",entryAmount);
						innerJson.put("entryUnits",entryUnits);
						innerJson.put("roboValue",totalAmount);
						outerJsonArray.put(innerJson);
					}
					
					//4)
					if(lastSignal==0 && (entryList.contains("EntryStrategy11") || entryList.contains("EntryStrategy15") || entryList.contains("EntryStrategy16")) && navAtLastSignal!=0 && currentEquityNav < navAtLastSignal)
					{
						flag=true;
						lastSignal=1;
						navAtLastSignal=currentEquityNav;
						
						
						double switchAmount=totalAmountLiquid*(condition4SwitchPercentage/100.0);
						double entryAmount=switchAmount;
						//totalAmountEquity=totalAmountEquity+switchAmount;
						
						//currentEquityUnits=totalAmountEquity/currentEquityNav;
						double entryUnits=switchAmount/currentEquityNav;
						currentEquityUnits=currentEquityUnits+entryUnits;
						
						totalAmountEquity=currentEquityUnits*currentEquityNav;
						//totalAmountLiquid=totalAmountLiquid-switchAmount;
					    currentLiquidUnits=currentLiquidUnits-(switchAmount/currentLiquidNav);
					    totalAmountLiquid=currentLiquidUnits*currentLiquidNav;
					    totalAmount=totalAmountEquity+totalAmountLiquid;
					    
					    if(totalDesiredAmountOfReverseSwitchPending>switchAmount)
					    {
					    	totalDesiredAmountOfReverseSwitchPending=totalDesiredAmountOfReverseSwitchPending-switchAmount;
					    	isReverseSwitchOn=true;
					    	noOfMonthReverseSwitchPending=3;
					    	//System.out.println("condition4.1  "+totalAmount+"  currentDate="+currentDate);
					    }
					    else
					    {
					    	totalDesiredAmountOfReverseSwitchPending=0;
					    	isReverseSwitchOn=false;
					    	noOfMonthReverseSwitchPending=0;
					    	//System.out.println("condition4.2  "+totalAmount+"  currentDate="+currentDate);
					    }
					    innerActionJson=new JSONObject();
					    innerActionJson.put("action","ENTRY");
					    innerActionJson.put("date",currentDate);
					    outerActionJsonArray.put(innerActionJson);
					    
					    
						innerJson.put("date",new SimpleDateFormat("yyyy-MM-dd").format(currentDate));
						innerJson.put("nav",currentEquityNav);
						innerJson.put("liquidNav",currentLiquidNav);
						innerJson.put("units",currentEquityUnits);
						innerJson.put("liquidUnits",currentLiquidUnits);
						innerJson.put("action","ENTRY");
						innerJson.put("entryAmount",entryAmount);
						innerJson.put("entryUnits",entryUnits);
						innerJson.put("roboValue",totalAmount);
						outerJsonArray.put(innerJson);
					}
					if(previousMonth!=currentMonth)
					{
						Calendar cal=Calendar.getInstance();
						cal.setTime(currentDate);
						String month=Integer.toString(cal.get(Calendar.MONTH)+1);
						String year=Integer.toString(cal.get(Calendar.YEAR));
						String date=year+"-"+month+"-"+Short.toString(configurableBestDate);
						bestDate=new SimpleDateFormat("yyyy-MM-dd").parse(date);
						bestDateOccurredFlag=false;
						previousMonth=currentMonth;
					}
					
					//5) to be run on 25th of every month
					if(currentDate.compareTo(bestDate)>=0 && bestDateOccurredFlag==false)
					{
						flag=true;
						bestDateOccurredFlag=true;
						double entryAmount=monthlyAmountOfReverseSwitch;
						double entryUnits=-1;
						if(isReverseSwitchOn==true)
						{
							noOfMonthReverseSwitchPending--;
							totalDesiredAmountOfReverseSwitchPending=totalDesiredAmountOfReverseSwitchPending-monthlyAmountOfReverseSwitch;
							
							//totalAmountEquity=totalAmountEquity+monthlyAmountOfReverseSwitch;
							currentEquityUnits=currentEquityUnits+monthlyAmountOfReverseSwitch/currentEquityNav;
							entryUnits=monthlyAmountOfReverseSwitch/currentEquityNav;
							totalAmountEquity=currentEquityUnits*currentEquityNav;
							//totalAmountLiquid=totalAmountLiquid-monthlyAmountOfReverseSwitch;
							currentLiquidUnits=currentLiquidUnits-monthlyAmountOfReverseSwitch/currentLiquidNav;
							totalAmountLiquid=currentLiquidUnits*currentLiquidNav;
							totalAmount=totalAmountEquity+totalAmountLiquid;
							
							innerJson.put("date",new SimpleDateFormat("yyyy-MM-dd").format(currentDate));
							innerJson.put("nav",currentEquityNav);
							innerJson.put("liquidNav",currentLiquidNav);
							innerJson.put("units",currentEquityUnits);
							innerJson.put("liquidUnits",currentLiquidUnits);
							innerJson.put("action","MONTHLY ENTRY");
							innerJson.put("entryAmount",monthlyAmountOfReverseSwitch);
							innerJson.put("entryUnits",entryUnits);
							innerJson.put("roboValue",totalAmount);
							outerJsonArray.put(innerJson);
								
							if(noOfMonthReverseSwitchPending==0)
							{
								isReverseSwitchOn=false;
								noOfMonthReverseSwitchPending=0;
								totalDesiredAmountOfReverseSwitchPending=0;
								//System.out.println("condition5.1  "+totalAmount+"  bestDate:"+bestDate+"  currentDate:"+currentDate);
							}
							else
							{
								//System.out.println("condition5.2  "+totalAmount+"  bestDate:"+bestDate+" tAEquity="+String.format("%.3f",totalAmountEquity)+" tALiquid="+String.format("%.3f",totalAmountLiquid)+" monthlySwitchAmount="+monthlyAmountOfReverseSwitch+" totalDesiredAmountOfSwitchPending="+totalDesiredAmountOfReverseSwitchPending);
							}
						}
						else
						{
							//System.out.println("condition5.3  "+totalAmount+"  bestDate:"+bestDate+"  currentDate:"+currentDate+"  tAEquity="+totalAmountEquity+" tALiquid="+String.format("%.3f",totalAmountLiquid));
						}
						innerActionJson=new JSONObject();
					    innerActionJson.put("action","MONTHLY ENTRY");
					    innerActionJson.put("date",currentDate);
					    outerActionJsonArray.put(innerActionJson);
					    
					   
					}
					innerRoboJson=new JSONObject();
					innerRoboJson.put("date",currentDate);
					innerRoboJson.put("amount", totalAmount);
					outerRoboJsonArray.put(innerRoboJson);
					
					if(flag==false)
					{
						innerJson.put("date",currentDate);
						innerJson.put("nav",currentEquityNav);
						innerJson.put("liquidNav",currentLiquidNav);
						innerJson.put("units",currentEquityUnits);
						innerJson.put("liquidUnits",currentLiquidUnits);
						innerJson.put("roboValue",totalAmount);
						outerJsonArray.put(innerJson);
					}
					
					
					System.out.println("Date="+currentDate+" RoboAmount="+totalAmount+"currLiquidDate="+currentLiquidDate);
				}		
				roboJsonArray=outerRoboJsonArray;
			}
			else
			{
				throw new Exception("missing data for nav dates");
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}

	boolean isCurrentExit(long mutualFundId,Date date,Session hSession) throws Exception
	{
		try
		{
			List validExitList=getValidExitList(mutualFundId,hSession);
			List exitList=hSession.createQuery("from CustomMFExitAlert where mutualFundId=? and tradeDate=? and strategyName in (:exitlist)").setParameter(0, mutualFundId).setParameter(1, date).setParameterList("exitlist", validExitList).list();
			if(exitList!=null && exitList.size()>0)
			{
				//System.out.println("Exit list is not null");
				return true;
			}
			else
				return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
	List getValidExitList(long mutualFundId,Session hSession)
	{
		List<String> exitList=new ArrayList<String>();
		if(!exit.isEmpty())
		{		
			String[] exitArray=exit.split(",");
			for(int i=0;i<exitArray.length;i++)
			{
				exitArray[i]="ExitStrategy"+exitArray[i];
			}
			Collections.addAll(exitList,exitArray);
			return exitList;
		}	
		else
		{
			return exitList;
		}
		
		/*try
		{
			MutualFund mf=(MutualFund)hSession.createQuery("from MutualFund where id=?").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
			return mf.getExitStrategies();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;*/
	}
	List getValidEntryList(long mutualFundId,Session hSession)
	{
		List<String> entryList=new ArrayList<String>();
		if(!entry.isEmpty())
		{		
			String[] entryArray=entry.split(",");
			for(int i=0;i<entryArray.length;i++)
			{
				entryArray[i]="EntryStrategy"+entryArray[i];
			}
			Collections.addAll(entryList,entryArray);
			return entryList;
		}	
		else
		{
			return entryList;
		}
/*		try
		{
			MutualFund mf=(MutualFund)hSession.createQuery("from MutualFund where id=?").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
			return mf.getEntryStrategies();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
*/	}
	public JSONArray getEquityJsonArray() throws JSONException 
	{
		return equityJsonArray;
	}
	public JSONArray getRoboJsonArray() {
		return roboJsonArray;
	}
	public JSONArray getConditionJsonArray()
	{
		return conditionJsonArray;
	}
	public JSONArray getOuterEquityJsonArray()
	{
		return outerEquityAmountJsonArray;
	}
	public JSONArray getOuterLiquidJsonArray()
	{
		return outerLiquidAmountJsonArray;
	}
	public JSONArray getOuterActionJsonArray()
	{
		return outerActionJsonArray;
	}
	public JSONArray getOuterJsonArray()
	{
		return outerJsonArray;
	}

}
