package com.robo.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Config {

	String fileName="config.prop";
	Properties properties;
	
	public Config() throws IOException
	{
		properties=new Properties();
		FileInputStream fis=new FileInputStream(Config.class.getResource(fileName).getPath());
		properties.load(fis);
	}
	public String getProperty(String key)
	{
		return properties.getProperty(key);
	}
	
}
