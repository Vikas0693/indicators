<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="com.fundexpert.dao.EveryDayCandle" %>

<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.strategy.controller.HibernateConnection" %>
<%@ page import="java.util.Iterator" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<% 
	try {
			String strategyName="EntryStrategy3";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			int count=0;
			while(iterator.hasNext())
			{				
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				List<EveryDayCandle> weeklyCandleList = HibernateConnection.getIntraDayFeeds(mutualFundId,0,0); 
				// This you need to write on your own, i need Weekly RSI 70 value for the symbol
				// CustomMFExitAlert is the object in which we are persisting Exit dates for different exit startegy 
	            boolean watchActivatedCond1Met = false; // we have to persist this when this code moves to DataController
	            int countWatchCond1 = 0;
	            int countWatchTotalCond1 = 0;
	            boolean watchActivatedCond2 = false;
				
	            int countWatchTotalCond2 = 0;
	
	            if (weeklyCandleList != null && weeklyCandleList.size() != 0) {
	                for (EveryDayCandle weeklyCandle : weeklyCandleList) {
	                	System.out.println("Mf="+mutualFundId+"\t\t"+weeklyCandle.getRsi());
	                    if (watchActivatedCond2)  // Checking for final condition
	                    {
	                        if (weeklyCandle.getRsi() > 40) {
	                            countWatchTotalCond2++;
	                            if (countWatchTotalCond2 >= 4) {
	                                watchActivatedCond2 = false; //restting again for next check
	                                watchActivatedCond1Met = false;//restting again for next check
	                                // EXIT CONDIITON MET, TO Populate Database
	                                //strategyName - "ExitStrategy1"
	                                // symbol - symbol
	                                // tradeDate - weeklyCandleList.get(index).getTradDate()
	                                //reasonForAlert - "as per ExitStrategy1"
	                                // exitPrice  -  weeklyCandleList.get(index).getClosePrice()
	                                //nftyPrice - for Nifty same as weeklyCandleList.get(index).getClosePrice()
	                                CustomMFEntryAlert customMFEntryAlert = new CustomMFEntryAlert();
	                                customMFEntryAlert.setMutualFundId(mutualFundId);
	                                customMFEntryAlert.setStrategyName(strategyName);
	                                customMFEntryAlert.setTradeDate(weeklyCandle.getTradeDate());
	                                customMFEntryAlert.setReasonForAlert(1);
	                                customMFEntryAlert.setEntryPrice(weeklyCandle.getTodaysClosePrice());
	                                //customMFExitAlert.setNiftyEntryPrice(weeklyCandle.getClosePrice());
	                                HibernateConnection.saveOrUpdate(customMFEntryAlert);
	                                out.print("Fount alert [" + weeklyCandle.getTradeDate() + "] .<br>");
	                                out.flush();
	
	                            }
	                        } else {
	                            // Second COunter to be reset
	                            countWatchTotalCond2 = 0;
	                        }
	                    }
	
	                    if (!watchActivatedCond1Met && weeklyCandle.getRsi() < 40) {
	                        countWatchCond1++;
	                        if (countWatchCond1 == 1) {
	                            countWatchTotalCond1 = 0;
	                        } else if (countWatchCond1 >= 3) {
	                            watchActivatedCond2 = true;
	                            watchActivatedCond1Met = true;
	                            countWatchTotalCond1 = 0;
	                            countWatchCond1 = 0;
	                        }
	                    }
	
	                    if (!watchActivatedCond1Met)   // Not 30
	                    {
	                        countWatchTotalCond1++;
	                    }
	
	                    if (!watchActivatedCond1Met && countWatchTotalCond1 >= 4) {
	                        watchActivatedCond1Met = false;
	                        countWatchCond1 = 0;
	                        countWatchTotalCond1 = 0;
	                    }
	
	                }
	            }
            }
            out.print("Finished processing all data.<br>");
            out.flush();
        } 
		catch (Exception e)
		{
           e.printStackTrace(response.getWriter());
           e.printStackTrace();
        }
	%>
	
</body>
</html>