<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.apidoc.util.HibernateBridge"%>
<%@ page import="java.util.List" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.strategy.controller.HibernateConnection" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Entry 6</title>
</head>
<body>
	<% 
	Session hSession=null;
	Transaction tx=null;
	try {
			String strategyName="EntryStrategy6";
		
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			int count=0;
			hSession=HibernateBridge.getSessionFactory().openSession();
			while(iterator.hasNext())
			{				
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				List<Fractal> dailyCandleList = HibernateConnection.getFractalStrategy(mutualFundId,1,2);
				
				tx=hSession.beginTransaction();
				for(int i=1;i<dailyCandleList.size();i++)
				{
					Fractal currentFractal=(Fractal)dailyCandleList.get(i);
					double closePrice=currentFractal.getClosePrice();
					double upFractal=currentFractal.getUpFractal();
					Date tradeDate=currentFractal.getTradeDate();
					
					Double currentTcfl1=currentFractal.getTcfl1();
					Double currentTcfl2=currentFractal.getTcfl2();
					Double currentTcfl3=currentFractal.getTcfl3(); 
					
					boolean flatPatternSSBFound=false;
					boolean flatPatternTCFL1Found=false;
					boolean flatPatternTCFL3Found=false;
					//System.out.println("0 condition="+tradeDate+"mf="+mutualFundId);
					if(currentTcfl1!=null  && closePrice<currentTcfl1 && closePrice<currentTcfl2 && closePrice<currentTcfl3 && closePrice>upFractal)
					{
						Fractal previousFractal=dailyCandleList.get(i-1);
						if(previousFractal.getClosePrice()<previousFractal.getUpFractal())
						{
							//System.out.println("Second condition="+tradeDate+"mf="+mutualFundId);
							Calendar calendar=Calendar.getInstance();
							calendar.setTime(tradeDate);
							calendar.add(Calendar.MONTH,-4);
							Date previousFourMonthDate=calendar.getTime(); 
							
							List flatPatternBeforeTradeDateSsb=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"SSB").setParameter(1,mutualFundId).setParameter(2,20).setParameter(3, 1).setParameter(4,tradeDate).setMaxResults(1).list();
							
							if(flatPatternBeforeTradeDateSsb.size()!=0)
							{
								FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateSsb.get(0);
								Date lastFlatPatternDate=flatPattern.getPatternStartDate();
															
								if(lastFlatPatternDate.compareTo(previousFourMonthDate)<0)
								{
									flatPatternSSBFound=true;
									System.out.println("TradeDate="+tradeDate+" FourMonthBackDate="+previousFourMonthDate+"  lastFlatPatternDate="+lastFlatPatternDate);
								}
								else
								{
									double ssb=flatPattern.getPatternValue();
									if(ssb>(closePrice*1.05))
									{
										System.out.println("TradeDate="+tradeDate+" SSB Second");
										//System.out.println("ssb true "+mutualFundId+"tradeDate="+tradeDate);
										//System.out.println("pattern date found(Nav checked) alert for mf="+mutualFundId+" for "+tradeDate+"  lastFlatDate="+lastFlatPatternDate);
										flatPatternSSBFound=true;
									}
									else
									{
										//System.out.print("ssb pattern date found ( nav not verified)");
										flatPatternSSBFound=false;
										continue;
									}
								}
							}
							else
							{
								System.out.println("TradeDate="+tradeDate+" SSB Three");
								//flatPatternSSBFound=false;
								//System.out.println("No pattern date found alert for mf="+mutualFundId+" for "+tradeDate);
								flatPatternSSBFound=true;
							}
							List flatPatternBeforeTradeDateTcfl1=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"TCFL1").setParameter(1,mutualFundId).setParameter(2,15).setParameter(3, 1).setParameter(4,tradeDate).setMaxResults(1).list();
							if(flatPatternSSBFound==true)
							{
								if(flatPatternBeforeTradeDateTcfl1.size()!=0)
								{
									FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateTcfl1.get(0);
									Date lastFlatPatternDate=flatPattern.getPatternStartDate();
									
									if(lastFlatPatternDate.compareTo(previousFourMonthDate)<0)
									{
										System.out.println("TradeDate="+tradeDate+" TCFL1 First");
										flatPatternTCFL1Found=true;
										//System.out.println("pattern end date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
									}
									else
									{
										double tcfl1=flatPattern.getPatternValue();
										if(tcfl1>(closePrice*1.05))
										{
											System.out.println("TradeDate="+tradeDate+" TCFL1 Second");
											//System.out.println("tcfl1 true "+mutualFundId+"tradeDate="+tradeDate);
											flatPatternTCFL1Found=true;
										}
										else
										{
											flatPatternTCFL1Found=false;
											continue;
										}
									}	
								}
								else
								{
									System.out.println("TradeDate="+tradeDate+" TCFL1 Third");
									flatPatternTCFL1Found=true;
								}
							}
							else
							{
								continue;
							}
							List flatPatternBeforeTradeDateTcfl3=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"TCFL3").setParameter(1,mutualFundId).setParameter(2,15).setParameter(3,1).setParameter(4,tradeDate).setMaxResults(1).list();
							if(flatPatternSSBFound==true && flatPatternTCFL1Found==true)
							{
								if(flatPatternBeforeTradeDateTcfl3.size()!=0)
								{
									FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateTcfl3.get(0);
									Date lastFlatPatternDate=flatPattern.getPatternStartDate();
									
									if(lastFlatPatternDate.compareTo(previousFourMonthDate)<0)
									{
										System.out.println("TradeDate="+tradeDate+" TCFL3 First");
										flatPatternTCFL3Found=true;
										//System.out.println("tcfl 3 pattern end date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
									}
									else
									{
										double tcfl3=flatPattern.getPatternValue();
										if(tcfl3>(closePrice*1.05))
										{
											System.out.println("TradeDate="+tradeDate+" TCFL3 Second");
											//System.out.println("tcfl3 true "+mutualFundId+"tradeDate="+tradeDate);
											flatPatternTCFL3Found=true;
										}
										else
										{
											flatPatternTCFL3Found=false;
											continue;
										}
									}
								}
								else
								{
									System.out.println("TradeDate="+tradeDate+" TCFL3 Three");
									//System.out.print("tcfl 3 no pattern date found  tradeDate="+tradeDate);
									flatPatternTCFL3Found=true;
								}
							}
							else
							{								
								continue;
							} 								
							if(flatPatternSSBFound==true && flatPatternTCFL1Found==true && flatPatternTCFL3Found==true)
							{
								System.out.println("Alert="+mutualFundId+" tradeDate="+tradeDate);
								CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
								customMFEntryAlert.setMutualFundId(mutualFundId);
								customMFEntryAlert.setStrategyName(strategyName);
								customMFEntryAlert.setTradeDate(tradeDate);
								customMFEntryAlert.setEntryPrice(closePrice);
								customMFEntryAlert.setReasonForAlert(1);
								hSession.saveOrUpdate(customMFEntryAlert);
								//HibernateConnection.saveOrUpdate(customMFEntryAlert);
								//System.out.println(mutualFundId+" \t\t "+currentFractal.getTradeDate());
							}							
						}
					}	
				}	
				tx.commit();
            }
            out.print("Finished processing all data.<br>");
            out.flush();
        } 
		catch (Exception e)
		{
           e.printStackTrace(response.getWriter());
           if(tx!=null)
        	   tx.rollback();
        }
		finally
		{
			hSession.close();
		}		
	%>	
</body>
</html>