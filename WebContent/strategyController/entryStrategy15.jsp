<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="com.fundexpert.dao.CustomMFExitAlert" %>
<%@ page import="com.fundexpert.dao.Nav" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Exit Strategy 15</title>
</head>
<body>
	<% 
	Session hSession=null;
	Transaction transaction=null;
	try {
			String strategyName="EntryStrategy15";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			int count=0;
			hSession=HibernateBridge.getSessionFactory().openSession();
			while(iterator.hasNext())
			{				
				transaction=hSession.beginTransaction();
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				List exit1Alert=hSession.createQuery("from CustomMFExitAlert where strategyName='ExitStrategy1' and mutualFundId=?").setParameter(0,mutualFundId).list();
				
				for(int i=0;i<exit1Alert.size();i++)
				{
					CustomMFExitAlert alert1=(CustomMFExitAlert)exit1Alert.get(i);
					double exit1Price=alert1.getExitPrice();
					Date alert1Date=alert1.getTradeDate();
					Calendar cal=Calendar.getInstance();
					cal.setTime(alert1Date);
					cal.add(Calendar.MONTH,4);
					List navFor4Months=hSession.createQuery("from Nav where mutualfundId=? and tradeDate between ? and ? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,alert1Date).setParameter(2,cal.getTime()).list();
					if(navFor4Months!=null)
					{
						for(int j=0;j<navFor4Months.size();j++)
						{
							Nav nav=(Nav)navFor4Months.get(j);
							if(nav.getNav()>exit1Price*1.03)
							{
								System.out.println("Exit alert for ="+mutualFundId+"  on ="+nav.getTradeDate());
								CustomMFEntryAlert customMFEntryAlert = new CustomMFEntryAlert();
                                customMFEntryAlert.setMutualFundId(mutualFundId);
                                customMFEntryAlert.setStrategyName(strategyName);
                                customMFEntryAlert.setTradeDate(nav.getTradeDate());
                                customMFEntryAlert.setReasonForAlert(1);
                                customMFEntryAlert.setEntryPrice(nav.getNav());
                               	hSession.saveOrUpdate(customMFEntryAlert);
                               	break;
							}
						}
					}
				}				
				transaction.commit();
			}
			out.print("Finished processing all data.<br>");
            out.flush();
        } 
		catch (Exception e)
		{
           e.printStackTrace(response.getWriter());
           if(transaction!=null)
        	   transaction.rollback();
        }
		finally
		{
			hSession.close();
		}
	%>
	
</body>
</html>