<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="com.fundexpert.dao.CustomMFExitAlert" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Iterator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Entry Strategy 16</title>
</head>
<body>

<%
	Session hSession=null;
	Transaction transaction=null;
	try
	{
		String strategyName="EntryStrategy16";
		MutualFundController mfc=new MutualFundController();
		List<Object[]> mfList=mfc.getMutualFundList();
		Iterator iterator=mfList.iterator();
		while(iterator.hasNext())
		{
			Object object[]=(Object[])iterator.next();
			Long mutualFundId=(Long)object[0];
			
			hSession=HibernateBridge.getSessionFactory().openSession();
			transaction=hSession.beginTransaction();
			Date previousDate=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy16'").setParameter(0,mutualFundId).uniqueResult();
			List alerts=hSession.createQuery("from CustomMFEntryAlert where mutualFundId=? and (strategyName='EntryStrategy6' or strategyName='EntryStrategy11' or strategyName='EntryStrategy12' or strategyName='EntryStrategy13') order by tradeDate").setParameter(0,mutualFundId).list();
			for(int i=0;i<alerts.size();i++)
			{
				CustomMFEntryAlert entryAlert=(CustomMFEntryAlert)alerts.get(i);
				Date alertDate=entryAlert.getTradeDate();
				Calendar cal=Calendar.getInstance();
				cal.setTime(alertDate);
				cal.add(Calendar.MONTH,-4);
				//System.out.println("previousDate="+previousDate+"Current="+alertDate+" previous4MonthDate="+cal.getTime()+"AlertType="+entryAlert.getStrategyName());
				
				//System.out.println("List"+alert16Within6Month+"mf="+mutualFundId);
				
				
				if(previousDate!=null)
				{
					if(previousDate.compareTo(cal.getTime())<0)
					{
						System.out.println("Alert 16found for mfid="+mutualFundId+" on alertDate="+alertDate);
						CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
						customMFEntryAlert.setMutualFundId(mutualFundId);
						customMFEntryAlert.setStrategyName(strategyName);
						customMFEntryAlert.setTradeDate(alertDate);
						customMFEntryAlert.setEntryPrice(entryAlert.getEntryPrice());
						customMFEntryAlert.setReasonForAlert(1);
						hSession.saveOrUpdate(customMFEntryAlert);
						previousDate=alertDate;
					}
				}
				else
				{
					System.out.println("Alert 16found for mfid="+mutualFundId+" on alertDate="+alertDate+" with previousDate="+previousDate);
					CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
					customMFEntryAlert.setMutualFundId(mutualFundId);
					customMFEntryAlert.setStrategyName(strategyName);
					customMFEntryAlert.setTradeDate(alertDate);
					customMFEntryAlert.setEntryPrice(entryAlert.getEntryPrice());
					customMFEntryAlert.setReasonForAlert(1);
					hSession.saveOrUpdate(customMFEntryAlert);
					previousDate=alertDate;
				}
			
				
			}
			transaction.commit();
			
		}	
		out.write("Finished Processing.");
	}
	catch(Exception e)
	{
		e.printStackTrace();
		e.printStackTrace(response.getWriter());
		if(transaction!=null)
			transaction.rollback();
	}
	finally
	{
		hSession.close();
	}

%>
</body>
</html>