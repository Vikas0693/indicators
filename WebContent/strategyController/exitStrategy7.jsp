<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.dao.CustomMFExitAlert" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Iterator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Exit Strategy 7</title>
</head>
<body>

<%
	Session hSession=null;
	Transaction transaction=null;
	try
	{
		String strategyName="ExitStrategy7";
		MutualFundController mfc=new MutualFundController();
		List<Object[]> mfList=mfc.getMutualFundList();
		Iterator iterator=mfList.iterator();
		while(iterator.hasNext())
		{
			Object object[]=(Object[])iterator.next();
			Long mutualFundId=(Long)object[0];
			
			hSession=HibernateBridge.getSessionFactory().openSession();
			transaction=hSession.beginTransaction();
			Date previousAlert7Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFExitAlert where mutualFundId=? and strategyName='ExitStrategy7'").setParameter(0,mutualFundId).uniqueResult();
			List alerts=hSession.createQuery("from CustomMFExitAlert where mutualFundId=? and (strategyName='ExitStrategy3' or strategyName='ExitStrategy5' or strategyName='ExitStrategy6') order by tradeDate").setParameter(0,mutualFundId).list();
			for(int i=0;i<alerts.size();i++)
			{
				CustomMFExitAlert exitAlert=(CustomMFExitAlert)alerts.get(i);
				Date alertDate=exitAlert.getTradeDate();
				Calendar cal=Calendar.getInstance();
				cal.setTime(alertDate);
				cal.add(Calendar.MONTH,-4);
				//System.out.println("previousDate="+previousDate+"Current="+alertDate+" previous4MonthDate="+cal.getTime()+"AlertType="+entryAlert.getStrategyName());
				
				//System.out.println("List"+alert16Within6Month+"mf="+mutualFundId);			
				if(previousAlert7Date!=null)
				{
					if(previousAlert7Date.compareTo(cal.getTime())<0)
					{
						System.out.println("AlertExit 7 found for mfid="+mutualFundId+" on alertDate="+alertDate);
						CustomMFExitAlert customMFExitAlert=new CustomMFExitAlert();
						customMFExitAlert.setMutualFundId(mutualFundId);
						customMFExitAlert.setStrategyName(strategyName);
						customMFExitAlert.setTradeDate(alertDate);
						customMFExitAlert.setExitPrice(exitAlert.getExitPrice());
						customMFExitAlert.setReasonForAlert(1);
						hSession.saveOrUpdate(customMFExitAlert);
						previousAlert7Date=alertDate;
					}
				}
				else
				{
					System.out.println("AlertExit 7 found for mfid="+mutualFundId+" on alertDate="+alertDate+"                    with previousDate="+previousAlert7Date);
					CustomMFExitAlert customMFExitAlert=new CustomMFExitAlert();
					customMFExitAlert.setMutualFundId(mutualFundId);
					customMFExitAlert.setStrategyName(strategyName);
					customMFExitAlert.setTradeDate(alertDate);
					customMFExitAlert.setExitPrice(exitAlert.getExitPrice());
					customMFExitAlert.setReasonForAlert(1);
					hSession.saveOrUpdate(customMFExitAlert);
					previousAlert7Date=alertDate;
				}				
			}
			transaction.commit();
			
		}	
		out.write("Finished Processing.");
	}
	catch(Exception e)
	{
		e.printStackTrace();
		e.printStackTrace(response.getWriter());
		if(transaction!=null)
			transaction.rollback();
	}
	finally
	{
		hSession.close();
	}

%>
</body>
</html>