<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.apidoc.util.HibernateBridge" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.strategy.controller.HibernateConnection" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="com.fundexpert.dao.EveryDayCandle" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Date" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Entry2</title>
</head>
<body>
<%
	Session hSession=null;
	Transaction tx=null;
	try
	{
		int count=0;
		hSession=HibernateBridge.getSessionFactory().openSession();
		List<Long> mfList = hSession.createQuery("select id from MutualFund where id in (select distinct mutualfundId from Nav) and category=1 and optionType=1 order by id desc").list();
		Iterator iterator=mfList.iterator();
		while(iterator.hasNext())
		{				
			tx=hSession.beginTransaction();
			Long mutualFundId=(Long)iterator.next();
			if(mutualFundId!=2)
				continue;
			List<Date> dateList=hSession.createQuery("select tradeDate from EveryDayCandle where mutualFundId=? and candleType=? and rsi>? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,1).setParameter(2,0f).setMaxResults(1).list();
			if(dateList.size()!=0)
			{
				Date nonZeroFirstDate=(Date)dateList.get(0);
				List nonZeroSixtyDateList=hSession.createQuery("select tradeDate from EveryDayCandle where mutualFundId=? and candleType=? and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,1).setParameter(2,nonZeroFirstDate).setMaxResults(60).list();
                if(dateList.size()!=0 || nonZeroSixtyDateList!=null)
                {
                	Date nonZeroSixtyDate=(Date)nonZeroSixtyDateList.get(nonZeroSixtyDateList.size()-1);
                	List list=hSession.createQuery("from EveryDayCandle where mutualFundId=? and candleType=1 and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,nonZeroSixtyDate).list();	
                	int alertReason=0;
                	Iterator itr=list.iterator();
                	while(itr.hasNext())
                	{
                		alertReason=0;
                		EveryDayCandle edc=((EveryDayCandle)itr.next());
                		Date tradeDate=edc.getTradeDate();
                		double entryPrice=edc.getTodaysClosePrice();
                		
						
                		List<Float> rsi = hSession.createQuery("SELECT rsi FROM EveryDayCandle  WHERE mutualFundId = ? and tradeDate <= ? and candleType=1 ORDER BY tradeDate DESC ")
                        	                .setParameter(0, mutualFundId)
											.setParameter(1,tradeDate)
                                	        .setMaxResults(60)
                                    	    .list();
                        if(rsi!=null)
                        {
                           	boolean firstPatternFound = false;
                           	boolean secondPatternFound = false;
                           	int j =0;//first pattern index
                           	int a = 0 ; //second pattern index;

                           	// Now get the valid W Pattern
                           	for(int i=0;i<rsi.size()-2;i++)
                           	{
	
                               	if(!firstPatternFound)
                               	{
                                   	j=i+1;
                                   	int k=i+2;
                                   	if((rsi.get(j) < rsi.get(i)) && (rsi.get(j) < rsi.get(k)))
                                   	{
                                        firstPatternFound=true;
                                        i++;
                                        //System.out.println("First  Pattern FOund");
                                   	}
                                   	if(firstPatternFound && rsi.get(j) >50)
									{
										//System.out.println("First  Pattern FOund----------- "+rsi.get(j));
                                        break;
									}
									else if(firstPatternFound && rsi.get(j) <50)
									{
										//System.out.println("First  Pattern FOund-----------VAILDATED **** "+firstPatternFound);
									}
                                }
                                if(firstPatternFound  && !secondPatternFound)
                                {
									//System.out.println("***************** NOW SECOND check");
                                    a=i+1;
                                    int b = i+2;
									//System.out.println(" NOW trying to check for Second Pattern ");
                                    if(rsi.get(a)<rsi.get(i) && (rsi.get(a)<rsi.get(b)))
                                    {
                          	        	secondPatternFound=true;

                                    }
                                    if(secondPatternFound)
                                    {
                                        //System.out.println("Second Pattern Found *************");
                                        if(rsi.get(a) < rsi.get(j))
                                        	alertReason=7;
                                        else
                                            break;

                                      }
                                  }
                              }
                          }
                        if(alertReason==7)
                        {
                        	System.out.print("Entry 2 alert for mfId="+mutualFundId+"Date="+tradeDate+"   SAVE");
                         	CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
                        	customMFEntryAlert.setEntryPrice(entryPrice);
                        	customMFEntryAlert.setStrategyName("EntryStrategy2");
                        	customMFEntryAlert.setMutualFundId(mutualFundId);
                        	customMFEntryAlert.setReasonForAlert(alertReason);
                        	customMFEntryAlert.setTradeDate(tradeDate);
                        	HibernateConnection.saveOrUpdate(customMFEntryAlert); 
                			        	
            	        }
                	}//while loop
                	//tx.commit();
                }//if close
			}//if close			
		}
		System.out.println("Done");
	}
	catch(Exception e)
	{
		e.printStackTrace(response.getWriter());
		if(tx!=null)
		tx.rollback();
	}
	finally
	{
		hSession.close();
	}
%>
</body>
</html>