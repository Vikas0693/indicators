<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="com.fundexpert.dao.CustomMFExitAlert" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Iterator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Entry Strategy 13</title>
</head>
<body>

<%
	Session hSession=null;
	Transaction transaction=null;
	try
	{
		String strategyName="EntryStrategy13";
		MutualFundController mfc=new MutualFundController();
		List<Object[]> mfList=mfc.getMutualFundList();
		Iterator iterator=mfList.iterator();
		while(iterator.hasNext())
		{
			Object object[]=(Object[])iterator.next();
			Long mutualFundId=(Long)object[0];
			
			hSession=HibernateBridge.getSessionFactory().openSession();
			transaction=hSession.beginTransaction();
		
			List custom8EntryAlertList=hSession.createQuery("from CustomMFEntryAlert where strategyName like '%EntryStrategy8%' and mutualFundId=?").setParameter(0,mutualFundId).list();
			if(custom8EntryAlertList!=null)
			{
				//System.out.println("CustomListSize for mutualFundId="+mutualFundId+" ="+custom8EntryAlertList.size());
				Iterator itr=custom8EntryAlertList.iterator();
				//the point of doing this(below if clause) is: before first enrtyStrategy10 there is no another entryStratrgy10 alert so skip 1st entry10 alert
				if(itr.hasNext())
				{
					itr.next();
				}
				while(itr.hasNext())
				{
					CustomMFEntryAlert entry8Alert=(CustomMFEntryAlert)itr.next();
					Date currentEntry8Date=entry8Alert.getTradeDate();
					Date previousEntry8Date=(Date)hSession.createQuery("select tradeDate from CustomMFEntryAlert where strategyName like '%EntryStrategy8%' and mutualFundId=? and tradeDate<? order by tradeDate desc").setParameter(0,mutualFundId).setParameter(1,currentEntry8Date).setMaxResults(1).uniqueResult();
					if(previousEntry8Date!=null)
					{
						System.out.println("previousEntry8Date="+previousEntry8Date);
						List exit5AlertList=hSession.createQuery("from CustomMFExitAlert where strategyName like '%ExitStrategy5%' and mutualFundId=? and tradeDate between ? and ?").setParameter(0,mutualFundId).setParameter(1,previousEntry8Date).setParameter(2,currentEntry8Date).list();
						if(exit5AlertList!=null && !(exit5AlertList.isEmpty()))
						{
							CustomMFExitAlert alert5=(CustomMFExitAlert)exit5AlertList.get(0);
							System.out.println("Entry alert for ="+mutualFundId+"  on ="+currentEntry8Date);
							CustomMFEntryAlert alert13=new CustomMFEntryAlert();
							alert13.setMutualFundId(mutualFundId);
							alert13.setTradeDate(currentEntry8Date);
							alert13.setStrategyName(strategyName);
							alert13.setEntryPrice(entry8Alert.getEntryPrice());
							alert13.setReasonForAlert(1);
							hSession.saveOrUpdate(alert13);
						}
					}
				}				
			}		
			hSession.flush();
			transaction.commit();
		}	
		out.write("Finished Processing.");
	}
	catch(Exception e)
	{
		e.printStackTrace();
		e.printStackTrace(response.getWriter());
		if(transaction!=null)
			transaction.rollback();
	}
	finally
	{
		hSession.close();
	}

%>
</body>
</html>