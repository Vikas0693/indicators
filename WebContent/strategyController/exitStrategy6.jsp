<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.dao.EveryDayCandle" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.dao.CustomMFExitAlert" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Iterator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Exit Strategy 6</title>
</head>
<body>

<%
	Session hSession=null;
	Transaction transaction=null;
	try
	{
		String strategyName="ExitStrategy6";
		MutualFundController mfc=new MutualFundController();
		List<Object[]> mfList=mfc.getMutualFundList();
		Iterator iterator=mfList.iterator();
		while(iterator.hasNext())
		{
			Object object[]=(Object[])iterator.next();
			Long mutualFundId=(Long)object[0];
			
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Fractal> fractalList=hSession.createQuery("from Fractal where mutualFundId=? and period=? and candleType=? and tcfl1!=0 order by tradeDate").setParameter(0,mutualFundId).setParameter(1,2).setParameter(2,1).list();
			transaction=hSession.beginTransaction();
			for(int i=3;i<fractalList.size();i++)
			{
				Fractal currentFractal=fractalList.get(i);
				double currentClosePrice=currentFractal.getClosePrice();
				Date currentTradeDate=currentFractal.getTradeDate();
				EveryDayCandle edc=(EveryDayCandle)hSession.createQuery("from EveryDayCandle where mutualFundId=? and tradeDate=? and candleType=?").setParameter(0, mutualFundId).setParameter(1, currentTradeDate).setParameter(2, 1).uniqueResult();
				double ema200=edc.getEma200();
				if(currentClosePrice<currentFractal.getTcfl1() && currentClosePrice<currentFractal.getTcfl2() && currentClosePrice<currentFractal.getTcfl3() && currentClosePrice<currentFractal.getLowFractal() && (ema200==0 || currentClosePrice<ema200))
				{
					Fractal previousFractal=fractalList.get(i-1);
					double previousClosePrice=previousFractal.getClosePrice();
					if(previousClosePrice<previousFractal.getTcfl1() && previousClosePrice<previousFractal.getTcfl2() && previousClosePrice<previousFractal.getTcfl3())
					{
						Fractal pPreviousFractal=fractalList.get(i-2);
						double pPreviousClosePrice=pPreviousFractal.getClosePrice();
						double minTCFL=Math.min(Math.min(pPreviousFractal.getTcfl1(),pPreviousFractal.getTcfl2()),pPreviousFractal.getTcfl3());
						if(pPreviousFractal.getTcfl1()!=0 && pPreviousClosePrice>minTCFL)
						{
							boolean flatPatternSSBFound=false;
							Calendar calendar=Calendar.getInstance();
							calendar.setTime(currentTradeDate);
							calendar.add(Calendar.MONTH,-3);
							Date previousThreeMonthDate=calendar.getTime(); 
							
							List flatPatternBeforeTradeDateSsb=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"SSB").setParameter(1,mutualFundId).setParameter(2,20).setParameter(3, 1).setParameter(4,currentTradeDate).setMaxResults(1).list();
							if(flatPatternBeforeTradeDateSsb.size()!=0)
							{
								FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateSsb.get(0);
								Date lastFlatPatternDate=flatPattern.getPatternStartDate();
								
								
								if(lastFlatPatternDate.compareTo(previousThreeMonthDate)<0)
								{
									flatPatternSSBFound=true;
								//	System.out.println("pattern end date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
								}
								else
								{
									double ssb=flatPattern.getPatternValue();
									if(ssb>(currentClosePrice*1.03))
									{
										//System.out.println("ssb true "+mutualFundId+"tradeDate="+tradeDate);
										//System.out.println("pattern date found(Nav checked) alert for mf="+mutualFundId+" for "+tradeDate+"  lastFlatDate="+lastFlatPatternDate);
										flatPatternSSBFound=true;
									}
									else
									{
										//System.out.print("ssb pattern date found ( nav not verified)");
										flatPatternSSBFound=false;
										continue;
									}
								}
							}
							else
							{
								//flatPatternSSBFound=false;
								//System.out.println("No pattern date found alert for mf="+mutualFundId+" for "+tradeDate);
								flatPatternSSBFound=true;
							}
							if(flatPatternSSBFound==true)
							{
								System.out.println("Exit alert for ="+mutualFundId+"  on ="+currentTradeDate);
								CustomMFExitAlert customMFExitAlert = new CustomMFExitAlert();
                                customMFExitAlert.setMutualFundId(mutualFundId);
                                customMFExitAlert.setStrategyName(strategyName);
                                customMFExitAlert.setTradeDate(currentTradeDate);
                                customMFExitAlert.setReasonForAlert(1);
                                customMFExitAlert.setExitPrice(currentClosePrice);
                               	hSession.saveOrUpdate(customMFExitAlert);
							}
							
						}
					}
				}
			}
			transaction.commit();
		
		}
	}
	catch(Exception e)
	{
		e.printStackTrace(response.getWriter());
		if(transaction!=null)
			transaction.rollback();
	}
	finally
	{
		hSession.close();
	}

%>
</body>
</html>