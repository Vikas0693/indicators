<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.apidoc.util.HibernateBridge"%>
<%@ page import="java.util.List" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.strategy.controller.HibernateConnection" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Entry Strategy 14</title>
</head>
<body>
	<% 
	Session hSession=null;
	Transaction tx=null;
	try {
			String strategyName="EntryStrategy14";
		
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			int count=0;
			hSession=HibernateBridge.getSessionFactory().openSession();
			while(iterator.hasNext())
			{				
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				
				tx=hSession.beginTransaction();
				List entry6Alerts=hSession.createQuery("from CustomMFEntryAlert where strategyName = 'EntryStrategy6' and mutualFundId=?").setParameter(0,mutualFundId).list();	
				if(entry6Alerts!=null && entry6Alerts.size()>1)
				{
					for(int i=1;i<entry6Alerts.size();i++)
					{
						CustomMFEntryAlert alert6Current=(CustomMFEntryAlert)entry6Alerts.get(i);
						Date currentAlertDate=alert6Current.getTradeDate();
						CustomMFEntryAlert alert6Previous=(CustomMFEntryAlert)entry6Alerts.get(i-1);
						Date previousAlertDate=alert6Previous.getTradeDate();
						Calendar cal=Calendar.getInstance();
						cal.setTime(currentAlertDate);
						cal.add(Calendar.MONTH,-2);
						Date previous2MonthDate=cal.getTime();
						if(previous2MonthDate.compareTo(previousAlertDate)>0)
						{
							System.out.println("Alert="+mutualFundId+" tradeDate="+currentAlertDate);
							CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
							customMFEntryAlert.setMutualFundId(mutualFundId);
							customMFEntryAlert.setStrategyName(strategyName);
							customMFEntryAlert.setTradeDate(currentAlertDate);
							customMFEntryAlert.setEntryPrice(alert6Current.getEntryPrice());
							customMFEntryAlert.setReasonForAlert(1);
							hSession.saveOrUpdate(customMFEntryAlert);
						}						
					}
				}
				tx.commit();
            }
            out.print("Finished processing all data.<br>");
            out.flush();
        } 
		catch (Exception e)
		{
           e.printStackTrace(response.getWriter());
           if(tx!=null)
        	   tx.rollback();
        }
		finally
		{
			hSession.close();
		}		
	%>	
</body>
</html>