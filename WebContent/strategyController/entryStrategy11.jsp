<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Entry Strategy 11</title>
</head>
<body>

<%
	Session hSession=null;
	Transaction transaction=null;
	try
	{
		String strategyName="EntryStrategy11";
		MutualFundController mfc=new MutualFundController();
		List<Object[]> mfList=mfc.getMutualFundList();
		Iterator iterator=mfList.iterator();
		while(iterator.hasNext())
		{
			Object object[]=(Object[])iterator.next();
			Long mutualFundId=(Long)object[0];
			
			hSession=HibernateBridge.getSessionFactory().openSession();
			transaction=hSession.beginTransaction();
			List strategy9List=hSession.createQuery("from CustomMFEntryAlert where mutualFundId=? and strategyName like ? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,"%EntryStrategy9%").list();
			if(strategy9List!=null)
			{
				for(int i=1;i<strategy9List.size();i++)
				{
					double diff=0;
					CustomMFEntryAlert entryAlert0=(CustomMFEntryAlert)strategy9List.get(i-1);
					
					CustomMFEntryAlert entryAlert1=(CustomMFEntryAlert)strategy9List.get(i);
					System.out.println("              \t\t\t\t"+entryAlert1.getTradeDate()+"    "+entryAlert0.getTradeDate()+"  mfId="+mutualFundId);
				
					diff=entryAlert1.getTradeDate().getTime()-entryAlert0.getTradeDate().getTime();
					System.out.println("days="+diff/(1000*60*60*24));
					if((diff/(1000*60*60*24))>30.0)
					{
						System.out.println(entryAlert1.getTradeDate()+"    "+entryAlert0.getTradeDate()+"  mfId="+mutualFundId);
						CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
						customMFEntryAlert.setStrategyName(strategyName);
						customMFEntryAlert.setMutualFundId(mutualFundId);
						customMFEntryAlert.setTradeDate(entryAlert1.getTradeDate());
						customMFEntryAlert.setEntryPrice(entryAlert1.getEntryPrice());
						customMFEntryAlert.setReasonForAlert(1);
						hSession.saveOrUpdate(customMFEntryAlert);
					}
				}
			}
			hSession.flush();
			transaction.commit();
		
		}
	}
	catch(Exception e)
	{
		e.printStackTrace(response.getWriter());
		if(transaction!=null)
			transaction.rollback();
	}
	finally
	{
		hSession.close();
	}

%>
</body>
</html>