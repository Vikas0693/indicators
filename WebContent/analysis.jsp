<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<%@ page import="com.apidoc.util.HibernateBridge" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Calendar" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="jquery-3.2.1.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Analysis Page</title>

<style>
div.Strategies{
	position:relative;
	left:600px;
	top:20px;
}
div.Valid{
	position:fixed;
	right: 10px;
	top:50px;
}
</style>
</head>
<body>
	<%
	
	String exitStrategy1=null,exitStrategy2=null,exitStrategy3=null,exitStrategy4=null;
	String entryStrategy1=null,entryStrategy2=null,entryStrategy3=null,entryStrategy4=null,entryStrategy5=null,entryStrategy6=null,entryStrategy7=null,entryStrategy8=null,entryStrategy9=null,entryStrategy10=null;
	String exitQuery="";
	String entryQuery="";
	
		String submit=request.getParameter("submit");
		MutualFundController mfc=new MutualFundController();
		List<Object[]> mfList=mfc.getMutualFundList();
		if(submit==null)
		{
		%>
			<form action="analysis.jsp" method="get" name="form">
			<select name="mutualfund">
			<%
			Iterator<Object[]> itr=mfList.iterator();
			while(itr.hasNext())
			{
				Object obj[]=(Object[])itr.next();
				long id=(Long)obj[0];
				String mfName=(String)obj[1];
			%>	
				<option name="mutualFundName" id="mutualFundName" value="<%=id%>" ><%=mfName+" "+id%></option>
			<%
			}	
			%>
			</select>
			<div class="Strategies" id="strategies">
			ex1:<input name="exitstrategy1" type="checkbox">
			ex2:<input name="exitstrategy2" type="checkbox" >
			ex3:<input name="exitstrategy3" type="checkbox" >
			ex4:<input name="exitstrategy4" type="checkbox" >
			en1:<input name="entrystrategy1" type="checkbox">
			en2:<input name="entrystrategy2" type="checkbox" >
			en3:<input name="entrystrategy3" type="checkbox" >
			en4:<input name="entrystrategy4" type="checkbox" >
			en5:<input name="entrystrategy5" type="checkbox" >
			en6:<input name="entrystrategy6" type="checkbox" >
			en7:<input name="entrystrategy7" type="checkbox" >
			en8:<input name="entrystrategy8" type="checkbox" >
			en9:<input name="entrystrategy9" type="checkbox" >
			en10:<input name="entrystrategy10" type="checkbox">
			</div>
			<div class="Valid">
			<span id="validation2"></span>
			</div>
			<br>
			Alert Start:<input type="date" name="startDate" id="s"><span id="validation"></span>
			Alert End:<input type="date" name="endDate" id="e">
			Period:<select name="period">
				<option value="1">1</option>
				<option value="3">3</option>
				<option value="6">6</option>
				<option value="9">9</option>
				<option value="12">12</option>
				<option value="24">24</option>
				<option value="36">36</option>			
			</select>
			<br>
			<input type="submit" name="submit">
		</form>
		<%
		}
		else
		{
			String mutualFundName="";
			
			String mutualFund=request.getParameter("mutualfund");
			Long mutualFundId=Long.parseLong(mutualFund);
						
			String sDate=request.getParameter("startDate");
			String eDate=request.getParameter("endDate");
			Date alertStartDate=new SimpleDateFormat("yyyy-MM-dd").parse(sDate);
			Date alertEndDate=new SimpleDateFormat("yyyy-MM-dd").parse(eDate);
			
			String analysisPeriod=request.getParameter("period");
			Integer period=Integer.parseInt(analysisPeriod);
					
			exitStrategy1=request.getParameter("exitstrategy1");
			exitStrategy2=request.getParameter("exitstrategy2");
			exitStrategy3=request.getParameter("exitstrategy3");
			exitStrategy4=request.getParameter("exitstrategy4");
			if(exitStrategy1!=null)
			{
				exitQuery="(strategyName like '%exitStrategy1%'";
			}
			if(exitStrategy2!=null)
			{
				if(exitQuery.length()!=0)
					exitQuery+=" or strategyName like '%exitStrategy2%'";
				else
					exitQuery+="(strategyName like '%exitStrategy2%'";
			}
			if(exitStrategy3!=null)
			{
				if(exitQuery.length()!=0)
					exitQuery+=" or strategyName like '%exitStrategy3%'";
				else
					exitQuery+="(strategyName like '%exitStrategy3%'";
			}
			if(exitStrategy4!=null)
			{
				if(exitQuery.length()!=0)
					exitQuery+=" or strategyName like '%exitStrategy4%'";
				else
					exitQuery+="(strategyName like '%exitStrategy4%'";
			}
			if(exitQuery.length()>0)
				exitQuery+=")";
			entryStrategy1=request.getParameter("entrystrategy1");
			entryStrategy2=request.getParameter("entrystrategy2");
			entryStrategy3=request.getParameter("entrystrategy3");
			entryStrategy4=request.getParameter("entrystrategy4");
			entryStrategy5=request.getParameter("entrystrategy5");
			entryStrategy6=request.getParameter("entrystrategy6");
			entryStrategy7=request.getParameter("entrystrategy7");
			entryStrategy8=request.getParameter("entrystrategy8");
			entryStrategy9=request.getParameter("entrystrategy9");
			entryStrategy10=request.getParameter("entrystrategy10");
			
			if(entryStrategy1!=null)
			{
				entryQuery="(strategyName like '%entrystrategy1'";
			}
			if(entryStrategy2!=null)
			{
				if(entryQuery.length()!=0)
					entryQuery+=" or strategyName like '%entrystrategy2'";
				else
					entryQuery+="(strategyName like '%entrystrategy2'";
			}
			if(entryStrategy3!=null)
			{
				if(entryQuery.length()!=0)
					entryQuery+=" or strategyName like '%entrystrategy3'";
				else
					entryQuery+="(strategyName like '%entrystrategy3'";
			}
			if(entryStrategy4!=null)
			{
				if(entryQuery.length()!=0)
					entryQuery+=" or strategyName like '%entrystrategy4'";
				else
					entryQuery+="(strategyName like '%entrystrategy4'";
			}
			if(entryStrategy5!=null)
			{
				if(entryQuery.length()!=0)
					entryQuery+=" or strategyName like '%entrystrategy5'";
				else
					entryQuery+="(strategyName like '%entrystrategy5'";
			}
			if(entryStrategy6!=null)
			{
				if(entryQuery.length()!=0)
					entryQuery+=" or strategyName like '%entrystrategy6'";
				else
					entryQuery+="(strategyName like '%entrystrategy6'";
			}
			if(entryStrategy7!=null)
			{
				if(entryQuery.length()!=0)
					entryQuery+=" or strategyName like '%entrystrategy%'";
				else
					entryQuery+="(strategyName like '%entrystrategy%'";
			}
			if(entryStrategy8!=null)
			{
				if(entryQuery.length()!=0)
					entryQuery+=" or strategyName like '%entrystrategy8'";
				else
					entryQuery+="(strategyName like '%entrystrategy8'";
			}
			if(entryStrategy9!=null)
			{
				if(entryQuery.length()!=0)
					entryQuery+=" or strategyName like '%entrystrategy9'";
				else
					entryQuery+="(strategyName like '%entrystrategy9'";
			}
			if(entryStrategy10!=null)
			{
				if(entryQuery.length()!=0)
					entryQuery+=" or strategyName like '%entrystrategy10'";
				else
					entryQuery+="(strategyName like '%entrystrategy10'";
			}
			if(entryQuery.length()>0)
				entryQuery+=")";
			
			
		%>
			<form action="analysis.jsp" method="get" name="form">
			<select name="mutualfund">
			<%
			System.out.println("lol"+" mutualFUnId="+mutualFundId);
			Iterator<Object[]> itr=mfList.iterator();
			while(itr.hasNext())
			{
				Object obj[]=(Object[])itr.next();
				Long id=(Long)obj[0];
				String mfName=(String)obj[1];
				System.out.println("id="+id);
				if((id+"").equals(mutualFund))
				{
					mutualFundName=mfName;
				} 			
				
				
			%>	
				<option name="mutualFundName" id="mutualFundName" value="<%=id%>" <%= request.getParameter("mutualfund")!=null?(request.getParameter("mutualfund").toString().equals(id+"")?"selected='selected'":""):"" %> ><%=mfName+" "+id%></option>
			<%
			}	
			%>
			</select>
			<div class="Strategies" id="strategies">
			ex1:<input name="exitstrategy1" type="checkbox"  <%=exitStrategy1!=null?(exitStrategy1.equals("on")?"checked='checked'":""):"" %>>
			ex2:<input name="exitstrategy2" type="checkbox" <%=exitStrategy2!=null?(exitStrategy2.equals("on")?"checked='checked'":""):"" %>>
			ex3:<input name="exitstrategy3" type="checkbox"  <%=exitStrategy3!=null?(exitStrategy3.equals("on")?"checked='checked'":""):"" %>>
			ex4:<input name="exitstrategy4" type="checkbox"  <%=exitStrategy4!=null?(exitStrategy4.equals("on")?"checked='checked'":""):"" %>>
			en1:<input name="entrystrategy1" type="checkbox" <%=entryStrategy1!=null?(entryStrategy1.equals("on")?"checked='checked'":""):"" %>>
			en2:<input name="entrystrategy2" type="checkbox" <%=entryStrategy2!=null?(entryStrategy2.equals("on")?"checked='checked'":""):"" %>>
			en3:<input name="entrystrategy3" type="checkbox" <%=entryStrategy3!=null?(entryStrategy3.equals("on")?"checked='checked'":""):"" %>>
			en4:<input name="entrystrategy4" type="checkbox" <%=entryStrategy4!=null?(entryStrategy4.equals("on")?"checked='checked'":""):"" %>>
			en5:<input name="entrystrategy5" type="checkbox" <%=entryStrategy5!=null?(entryStrategy5.equals("on")?"checked='checked'":""):"" %>>
			en6:<input name="entrystrategy6" type="checkbox" <%=entryStrategy6!=null?(entryStrategy6.equals("on")?"checked='checked'":""):"" %>>
			en7:<input name="entrystrategy7" type="checkbox" <%=entryStrategy7!=null?(entryStrategy7.equals("on")?"checked='checked'":""):"" %>>
			en8:<input name="entrystrategy8" type="checkbox" <%=entryStrategy8!=null?(entryStrategy8.equals("on")?"checked='checked'":""):"" %>>
			en9:<input name="entrystrategy9" type="checkbox" <%=entryStrategy9!=null?(entryStrategy9.equals("on")?"checked='checked'":""):"" %>>
			en10:<input name="entrystrategy10" type="checkbox" <%=entryStrategy10!=null?(entryStrategy10.equals("on")?"checked='checked'":""):"" %>>
			</div>
			<div class="Valid">
			<span id="validation2"></span>
			</div>
			<br>
			Alert Start:<input type="date" name="startDate" value=<%=sDate!=null?sDate:"" %> id="s"><span id="validation"></span>
			Alert End:<input type="date" name="endDate" value=<%=eDate!=null?eDate:"" %> id="e">
			Period:<select name="period">
				<option value="1" <%=analysisPeriod!=null?(analysisPeriod.equals("1")?"selected='selected'":""):"" %>>1</option>
				<option value="3" <%=analysisPeriod!=null?(analysisPeriod.equals("3")?"selected='selected'":""):"" %>>3</option>
				<option value="6" <%=analysisPeriod!=null?(analysisPeriod.equals("6")?"selected='selected'":""):"" %>>6</option>
				<option value="9" <%=analysisPeriod!=null?(analysisPeriod.equals("9")?"selected='selected'":""):"" %>>9</option>
				<option value="12" <%=analysisPeriod!=null?(analysisPeriod.equals("12")?"selected='selected'":""):"" %>>12</option>
				<option value="24" <%=analysisPeriod!=null?(analysisPeriod.equals("24")?"selected='selected'":""):"" %>>24</option>
				<option value="36" <%=analysisPeriod!=null?(analysisPeriod.equals("36")?"selected='selected'":""):"" %>>36</option>			
			</select>
			<br>
			<input type="submit" name="submit">
		</form>
		<%
		Session hSession=null;
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			List<Object[]> columnList=null;
			List<Object[]> columnList1=null;
			System.out.println("Entry Query="+entryQuery);
			if(exitQuery.length()>0)
			{
				columnList=hSession.createQuery("select mf.name,ex.tradeDate,ex.strategyName,ex.exitPrice from CustomMFExitAlert ex,MutualFund mf where ex.mutualFundId=mf.id and ex.mutualFundId=? and ex.tradeDate between ? and ? and "+exitQuery+" order by strategyName,tradeDate").setParameter(0,mutualFundId).setParameter(1,alertStartDate).setParameter(2,alertEndDate).list();
			}
			if(entryQuery.length()>0)
			{
				columnList1=hSession.createQuery("select mf.name,en.tradeDate,en.strategyName,en.entryPrice from CustomMFEntryAlert en,MutualFund mf where en.mutualFundId=mf.id and en.mutualFundId=? and en.tradeDate between ? and ? and "+entryQuery+" order by strategyName,tradeDate").setParameter(0,mutualFundId).setParameter(1,alertStartDate).setParameter(2,alertEndDate).list();
			}
			//System.out.print("columnList="+columnList.size());
			
			
			
			//List<Object[]> maxMinList=hSession.createQuery("select max(nav),min(nav) from Nav where mutualfundId=? and tradeDate between ? and ?").setParameter(0,mutualFundId).setParameter(1,alertStartDate).setParameter(2,cal.getTime()).list();
			
			
			
			%>
			
			<table border="1">
			<tr>
			<caption><%=mutualFundName %></caption>
			<th>Alert Date</th>
			<th>Strategy</th>
			<th>Alert Price</th>
			<th>Max</th>
			<th>Min</th>
			<th>Max %</th>
			<th>Min %</th>			
			</tr>
			<%
			Double max=0d,min=0d;	
			Double maxPercent=0d,minPercent=0d;
			if(columnList!=null)
			{
				Iterator iterator=columnList.iterator();
				while(iterator.hasNext())
				{
					Object[] object=(Object[])iterator.next();
					String name=(String)object[0];
					Date tradeDate=(Date)object[1];
					String strategy=(String)object[2];
					double price=(Double)object[3];
					
					Calendar cal=Calendar.getInstance();
					cal.setTime(tradeDate);
					cal.add(Calendar.MONTH,period);
					
					List<Object[]> maxMinList=hSession.createQuery("select max(nav),min(nav) from Nav where mutualfundId=? and tradeDate between ? and ?").setParameter(0,mutualFundId).setParameter(1,tradeDate).setParameter(2,cal.getTime()).list();
					if(maxMinList!=null)
					{
						
						Object obj[]=(Object[])maxMinList.get(0);
						max=(Double)obj[0];
						min=(Double)obj[1];
						System.out.print(max+"   "+min);
					}
					maxPercent=((max-price)/price)*100d;
					minPercent=((price-min)/price)*100d;
					if(minPercent>maxPercent)
					{
					%>
					<tr bgcolor="#92ff32"><!-- green -->
					<%
					}
					else
					{
					%>
					<tr bgcolor="#ff5444"><!-- red -->
					<%	
					}
					%>
					
					<td><%=new SimpleDateFormat("dd-MM-yyyy").format(tradeDate) %></td>
					<td><%=strategy %></td>
					<td><%=price %></td>
					<td><%=max %></td>
					<td><%=min %></td>
					<td><%=maxPercent %></td>
					<td><%=minPercent %></td>
					</tr>
					<%
				}
			}
			if(columnList1!=null)
			{
				Iterator iterator=columnList1.iterator();
				while(iterator.hasNext())
				{
					Object[] object=(Object[])iterator.next();
					String name=(String)object[0];
					Date tradeDate=(Date)object[1];
					String strategy=(String)object[2];
					double price=(Double)object[3];
					
					Calendar cal=Calendar.getInstance();
					cal.setTime(tradeDate);
					cal.add(Calendar.MONTH,period);
					List<Object[]> maxMinList=hSession.createQuery("select max(nav),min(nav) from Nav where mutualfundId=? and tradeDate between ? and ?").setParameter(0,mutualFundId).setParameter(1,tradeDate).setParameter(2,cal.getTime()).list();
					if(maxMinList!=null)
					{
						
						Object obj[]=(Object[])maxMinList.get(0);
						max=(Double)obj[0];
						min=(Double)obj[1];
						System.out.print(max+"   "+min);
					}
					maxPercent=((max-price)/price)*100d;
					minPercent=((price-min)/price)*100d;
					if(minPercent>maxPercent)
					{
					%>
					<tr bgcolor="#ff5444"><!-- red -->
					<%
					}
					else
					{
					%>
					<tr bgcolor="#92ff32"><!-- green -->
					<%	
					}
					%>
					<td><%=new SimpleDateFormat("dd-MM-yyyy").format(tradeDate) %></td>
					<td><%=strategy %></td>
					<td><%=price %></td>
					<td><%=max %></td>
					<td><%=min %></td>
					<td><%=maxPercent %></td>
					<td><%=minPercent %></td>
					</tr>
					<%
				}
			}
			%>
			</table>
			<%
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
			e.printStackTrace(response.getWriter());
		}
		finally
		{
			hSession.close();
		}
	}
	%>
<script>
$(document).ready(function(){
	$(":submit").click(function(event){
		console.log("submit");
		var startDate=new Date($("#s").val());
		var endDate=new Date($("#e").val());
		if(startDate=='Invalid Date'  )
		{
			$("#validation").text("Provide Start Date").css('color','red');
		}
		if(startDate=='Invalid Date'  )
		{
			$("#validation").text("Provide End Date").css('color','red');
		}
		if(startDate.getTime()>=endDate.getTime())
		{
			$("#validation").text("Start Date is greater or equal to End Date").css('color','red');	
			event.preventDefault();
		}
		var arrChecks=[];
		$('#strategies :checked').each(function()
		{
			arrChecks.push($(this).val());		
		});
		if(arrChecks.length===0)
		{
			$("#validation2").text("select atleast one checkbox").css('color','red');
			event.preventDefault();
		}
	});

});
</script>
</body>
</html>