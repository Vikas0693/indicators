<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.apidoc.util.JSONArrayValues" %>
<%@ page import="org.json.JSONArray"%>
<%@ page import="org.json.JSONObject"%>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Charts</title>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script src="https://code.highcharts.com/stock/highstock.js"></script>
<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
<script src="scripts/moment.min.js"></script>
</head>
<body>

<%
	String submit=request.getParameter("submit");
	String candleType=null;
	String mutualFundId="";
	String typeOfIndicatorFractal="";
	String typeOfIndicatorIchimoku="";
	String typeOfIndicatorTalib="";
	
	JSONArray navJson=null;
	JSONArray upFractalJson=null,lowFractalJson=null,swingHighJson=null,swingLowJson=null,tcfl1Json=null,tcfl2Json=null,tcfl3Json=null;
	JSONArray tenkanSenJson=null,kijunSenJson=null,senkouSpanAJson=null,senkouSpanBJson=null,kumoTopJson=null,kumoBottomJson=null;
	JSONArray ema20Json=null,ema50Json=null,ema200Json=null,macdJson=null,macdSignalJson=null,macdHistogramJson=null,upperBandJson=null,middleBandJson=null,lowerBandJson=null,rsiJson=null,psarJson=null,adxJson=null,atrJson=null;
	
	JSONObject resp=new JSONObject();
	
	try
	{
		MutualFundController mfc=new MutualFundController();
		List<Object[]> mfList=mfc.getMutualFundList();
		System.out.print(mfList.size());
		//JSONArray ohlcJson=null;
		if(submit==null)
		{
	%>
		<form id="form" name="form" action="updated-charts-flags.jsp" method="get">
		Fractal:<input type="checkbox" id="Fractal" name="Fractal"/>
		Ichimoku:<input type="checkbox" id="Ichimoku" name="Ichimoku"/>
		Talib:<input type="checkbox" id="Talib" name="Talib"/>
		
		
		<div id="fractalDiv" style="display:none">
			upFractal:<input type="checkbox" name="upFractal"/>
			lowFractal:<input type="checkbox" name="lowFractal"/>
			swingHigh<input type="checkbox" name="swingHigh"/>
			swingLow<input type="checkbox" name="swingLow"/>
			tcfl1<input type="checkbox" name="tcfl1"/>
			tcfl2<input type="checkbox" name="tcfl2"/>
			tcfl3<input type="checkbox" name="tcfl3"/>
		</div>
		<div id="ichimokuDiv" style="display:none">
			tenkanSen:<input type="checkbox" name="tenkanSen"/>
			kijunSen:<input type="checkbox" name="kijunSen"/>
			senkouSpanA:<input type="checkbox" name="senkouSpanA"/>
			senkouSpanB:<input type="checkbox" name="senkouSpanB"/>
			kumoTop:<input type="checkbox" name="kumoTop"/>
			kumoBottom:<input type="checkbox" name="kumoBottom"/>
		</div>
		<div id="talibDiv" style="display:none">
			ema20:<input type="checkbox" name="ema20"/>
			ema50:<input type="checkbox" name="ema50"/>
			ema200:<input type="checkbox" name="ema200"/>
			macd:<input type="checkbox" name="macd"/>
			macdSignal:<input type="checkbox" name="macdSignal"/>
			macdHistogram:<input type="checkbox" name="macdHistogram"/>
			upperBand:<input type="checkbox" name="upperBand"/>
			middleBand:<input type="checkbox" name="middleBand"/>
			lowerBand:<input type="checkbox" name="lowerBand"/>
			rsi:<input type="checkbox" name="rsi"/>
			psar:<input type="checkbox" name="psar"/>
			adx:<input type="checkbox" name="adx"/>
			atr:<input type="checkbox" name="atr"/>
		</div>
		
		 <select id="dropdown" name="mutualFundId">
		<%
			Iterator<Object[]> itr=mfList.iterator();
			while(itr.hasNext())
			{
				Object obj[]=(Object[])itr.next();
				long id=(Long)obj[0];
				String mfName=(String)obj[1];
		%>	
				<option name="mutualFundName" id="mutualFundName" value="<%=id%>" <%= request.getParameter("mutualFundId")!=null?(request.getParameter("mutualFundId").toString().equals(id+"")?"selected='selected'":""):"" %> ><%=mfName+" "+id%></option>
		<%
			}	
		%>
		</select> 
		Daily:<input type="radio" name="candle" value="Daily"/>
		Weekly:<input type="radio" name="candle" value="Weekly"/>	
		
		<input type="submit" id="submit" name="submit" value="submit"/>
		</form>
		
		<%
		}
		else if(submit!=null)
		{
			
			System.out.print("Submitted");
		%>
		
		<form id="form" name="form" action="updated-charts-flags.jsp" method="get">
		Fractal:<input type="checkbox" id="Fractal" name="Fractal" <%=request.getParameter("Fractal")!=null?(request.getParameter("Fractal").toString().equals("on")?"checked='checked'":""):"" %>/>
		Ichimoku:<input type="checkbox" id="Ichimoku" name="Ichimoku" <%=request.getParameter("Ichimoku")!=null?(request.getParameter("Ichimoku").toString().equals("on")?"checked='checked'":""):"" %>/>
		Talib:<input type="checkbox" id="Talib" name="Talib" <%=request.getParameter("Talib")!=null?(request.getParameter("Talib").toString().equals("on")?"checked='checked'":""):"" %>/>
		
		
		<div id="fractalDiv">
			upFractal:<input type="checkbox" name="upFractal" <%=request.getParameter("upFractal")!=null?(request.getParameter("upFractal").toString().equals("on")?"checked='checked'":""):"" %>>
			lowFractal:<input type="checkbox" name="lowFractal" <%=request.getParameter("lowFractal")!=null?(request.getParameter("lowFractal").toString().equals("on")?"checked='checked'":""):"" %>/>
			swingHigh<input type="checkbox" name="swingHigh" <%=request.getParameter("swingHigh")!=null?(request.getParameter("swingHigh").toString().equals("on")?"checked='checked'":""):"" %>>
			swingLow<input type="checkbox" name="swingLow" <%=request.getParameter("swingLow")!=null?(request.getParameter("swingLow").toString().equals("on")?"checked='checked'":""):"" %>/>
			tcfl1<input type="checkbox" name="tcfl1" <%=request.getParameter("tcfl1")!=null?(request.getParameter("tcfl1").toString().equals("on")?"checked='checked'":""):"" %>/>
			tcfl2<input type="checkbox" name="tcfl2" <%=request.getParameter("tcfl2")!=null?(request.getParameter("tcfl2").toString().equals("on")?"checked='checked'":""):"" %>/>
			tcfl3<input type="checkbox" name="tcfl3" <%=request.getParameter("tcfl3")!=null?(request.getParameter("tcfl3").toString().equals("on")?"checked='checked'":""):"" %>/>
		</div>
		<div id="ichimokuDiv" style="display:none">
			tenkanSen:<input type="checkbox" name="tenkanSen" <%=request.getParameter("tenkanSen")!=null?(request.getParameter("tenkanSen").toString().equals("on")?"checked='checked'":""):"" %>/>
			kijunSen:<input type="checkbox" name="kijunSen" <%=request.getParameter("kijunSen")!=null?(request.getParameter("kijunSen").toString().equals("on")?"checked='checked'":""):"" %>/>
			senkouSpanA:<input type="checkbox" name="senkouSpanA" <%=request.getParameter("senkouSpanA")!=null?(request.getParameter("senkouSpanA").toString().equals("on")?"checked='checked'":""):"" %>/>
			senkouSpanB:<input type="checkbox" name="senkouSpanB" <%=request.getParameter("senkouSpanB")!=null?(request.getParameter("senkouSpanB").toString().equals("on")?"checked='checked'":""):"" %>/>
			kumoTop:<input type="checkbox" name="kumoTop" <%=request.getParameter("kumoTop")!=null?(request.getParameter("kumoTop").toString().equals("on")?"checked='checked'":""):"" %>/>
			kumoBottom:<input type="checkbox" name="kumoBottom" <%=request.getParameter("kumoBottom")!=null?(request.getParameter("kumoBottom").toString().equals("on")?"checked='checked'":""):"" %>/>
		</div>
		<div id="talibDiv" style="display:none">
			ema20:<input type="checkbox" name="ema20" <%=request.getParameter("ema20")!=null?(request.getParameter("ema20").toString().equals("on")?"checked='checked'":""):"" %>/>
			ema50:<input type="checkbox" name="ema50" <%=request.getParameter("ema50")!=null?(request.getParameter("ema50").toString().equals("on")?"checked='checked'":""):"" %>/>
			ema200:<input type="checkbox" name="ema200" <%=request.getParameter("ema200")!=null?(request.getParameter("ema200").toString().equals("on")?"checked='checked'":""):"" %>/>
			macd:<input type="checkbox" name="macd" <%=request.getParameter("macd")!=null?(request.getParameter("macd").toString().equals("on")?"checked='checked'":""):"" %>/>
			macdSignal:<input type="checkbox" name="macdSignal" <%=request.getParameter("macdSignal")!=null?(request.getParameter("macdSignal").toString().equals("on")?"checked='checked'":""):"" %>/>
			macdHistogram:<input type="checkbox" name="macdHistogram" <%=request.getParameter("macdHistogram")!=null?(request.getParameter("macdHistogram").toString().equals("on")?"checked='checked'":""):"" %>/>
			upperBand:<input type="checkbox" name="upperBand" <%=request.getParameter("upperBand")!=null?(request.getParameter("upperBand").toString().equals("on")?"checked='checked'":""):"" %>/>
			middleBand:<input type="checkbox" name="middleBand" <%=request.getParameter("middleBand")!=null?(request.getParameter("middleBand").toString().equals("on")?"checked='checked'":""):"" %>/>
			lowerBand:<input type="checkbox" name="lowerBand" <%=request.getParameter("lowerBand")!=null?(request.getParameter("lowerBand").toString().equals("on")?"checked='checked'":""):"" %>/>
			rsi:<input type="checkbox" name="rsi" <%=request.getParameter("rsi")!=null?(request.getParameter("rsi").toString().equals("on")?"checked='checked'":""):"" %>/>
			psar:<input type="checkbox" name="psar" <%=request.getParameter("psar")!=null?(request.getParameter("psar").toString().equals("on")?"checked='checked'":""):"" %>/>
			adx:<input type="checkbox" name="adx" <%=request.getParameter("adx")!=null?(request.getParameter("adx").toString().equals("on")?"checked='checked'":""):"" %>/>
			atr:<input type="checkbox" name="atr" <%=request.getParameter("atr")!=null?(request.getParameter("atr").toString().equals("on")?"checked='checked'":""):"" %>/>
			
		</div>
		
		
		<select id="dropdown" name="mutualFundId">
		<%
			Iterator<Object[]> itr=mfList.iterator();
			while(itr.hasNext())
			{
				Object obj[]=(Object[])itr.next();
				long id=(Long)obj[0];
				String mfName=(String)obj[1];
		%>
				<option name="mutualFundName" id="mutualFundName" value="<%=id%>" <%=request.getParameter("mutualFundId")!=null?(request.getParameter("mutualFundId").toString().equals(id+"")?"selected='selected'":""):"" %>><%=mfName+" "+id%></option>
		<%
			}	
			System.out.print("After submitted and id=dropdown");
		%>
		</select> 
		
		Daily:<input type="radio" name="candle" value="Daily"/>
		Weekly:<input type="radio" name="candle" value="Weekly"/>
		
		<input type="submit" id="submit" name="submit" value="submit"/>
		</form>
		
		<%
			candleType=request.getParameter("candle");
			mutualFundId=request.getParameter("mutualFundId");
			
			typeOfIndicatorFractal=request.getParameter("Fractal");
			typeOfIndicatorIchimoku=request.getParameter("Ichimoku");
			typeOfIndicatorTalib=request.getParameter("Talib");
			
			if(typeOfIndicatorFractal!=null && typeOfIndicatorFractal.equalsIgnoreCase("on"))
			{		
				JSONArrayValues json=new JSONArrayValues();
				//below function will generate format of [[date,value],[date,value]] for Fractal table
				try
				{
					json.setFractalJson(mutualFundId,candleType);
				}
				catch(Exception e2)
				{
					resp.put("success", false);
					resp.put("error","did not get jsonArrayValues for fractal function working");
				}
				navJson=json.getNav(); 
				upFractalJson=json.getUpFractal();			
				lowFractalJson=json.getLowFractal();
				swingHighJson=json.getSwingHigh();			
				swingLowJson=json.getSwingLow();			
				tcfl1Json=json.getTcfl1();			
				tcfl2Json=json.getTcfl2();
				tcfl3Json=json.getTcfl3();
				System.out.println("Fractal 1:");
			}
			
			if(typeOfIndicatorIchimoku!=null && typeOfIndicatorIchimoku.equalsIgnoreCase("on"))
			{
				JSONArrayValues json=new JSONArrayValues();
				//below function will generate format of [[date,value],[date,value]] for Ichimoku table
				try
				{
					json.setIchimokuJson(mutualFundId,candleType);
				}
				catch(Exception e)
				{
					resp.put("success", false);
					resp.put("error","did not get jsonArrayValues Ichimoku function working");
				}				
				navJson=json.getNav();
				tenkanSenJson=json.getTenkanSen();
				kijunSenJson=json.getKijunSen();
				senkouSpanAJson=json.getSenkouSpanA();
				senkouSpanBJson=json.getSenkouSpanB();
				kumoTopJson=json.getKumoTop();
				kumoBottomJson=json.getKumoBottom();
			}
			
			
			if(typeOfIndicatorTalib!=null && typeOfIndicatorTalib.equalsIgnoreCase("on"))
			{
				JSONArrayValues json=new JSONArrayValues();
				//below function will generate format of [[date,value],[date,value]] for Ichimoku table
				try
				{
					json.setTalibJson(mutualFundId,candleType);
				}
				catch(Exception e)
				{
					System.out.print("Talib Exception");
					resp.put("success", false);
					resp.put("error","did not get jsonArrayValues Talib function working");
				}				
				navJson=json.getNav();
				ema20Json=json.getEma20();
				ema50Json=json.getEma50();
				ema200Json=json.getEma200();
				macdJson=json.getMacd();
				macdSignalJson=json.getMacdSignal();
				macdHistogramJson=json.getMacdHistogram();
				upperBandJson=json.getUpperBand();
				middleBandJson=json.getMiddleBand();
				lowerBandJson=json.getLowerBand();
				rsiJson=json.getRsi();
				psarJson=json.getPsar();
				adxJson=json.getAdx();
				atrJson=json.getAtr();
			}
		}
	}
	catch(Exception e)
	{
		resp.put("success",false);
		resp.put("error","bad");
		out.print(resp);
	}	
	
	%> 
	
<div id="container" style="height:725px">
</div>
<!-- <div id="containerMacd">
</div>
<div id="containerRsi">
</div>
<div id="containerAdx">
</div>
<div id="containerAtr">
</div> -->
<!-- <br>
<div id="containerWeekly" style="height:500px;min-width:310px">
</div> -->

<script>

$(document).ready(function()
{
	
	var mutualFundName;
	
	var candleType="<%=candleType%>";
	if(candleType==='null')
	{
		//console.log(candleType);
		candleType="Daily";
	}
	//only for talib	
	var seriesArray=[];
	//only for fractalSeries Date
	var seriesFractalArray=[];
	//onlt for talibSeries Date
	var seriesTalibArray=[];
	
	<%System.out.println("inside jQuery");%>
		
		//var indicatorJqueryType=$("#indicator :selected").text();
		console.log("first");
		$("#Fractal").change(function()
		{
			if($("#Fractal").is(':checked'))
			{
				console.log("Fractal checked");
				$("#fractalDiv").show();
			}
			else
			{
				console.log("Fractal Unchecked");
				$('#fractalDiv :checkbox').prop("checked", false);
				$("#fractalDiv").hide();
				
			}
		});
		$("#Ichimoku").change(function()
		{		
			if($("#Ichimoku").is(':checked'))
			{
				console.log("Ichimoku checked");
				$("#ichimokuDiv").show();
			}
			else
			{
				console.log("Ichimoku Unchecked");
				$('#ichimokuDiv :checkbox').prop("checked", false);
				$("#ichimokuDiv").hide();
				
			}
		});
		$("#Talib").change(function()
		{			
			if($("#Talib").is(':checked'))
			{
				console.log("Talib checked");
				$("#talibDiv").show();
			}
			else
			{
				console.log("Talib Unchecked");
				$('#talibDiv :checkbox').prop("checked", false);
				$("#talibDiv").hide();
				
			}
		});
		//below if(indicatorJqueryType1==='Fractal') is used when submit is pressed and on next page we keep selected those checkboxes
		if($("#Fractal").is(':checked'))
		{
			console.log("Fractal checked");
			$("#fractalDiv").show();
		}
		else
		{
			console.log("Fractal Unchecked");
			$("#fractalDiv").hide();
			$('#fractalDiv :checkbox').prop("checked", false);
		}
		if($("#Ichimoku").is(':checked'))
		{
			console.log("Ichimoku checked");
			$("#ichimokuDiv").show();
		}
		else
		{
			console.log("Ichimoku Unchecked");
			$("#ichimokuDiv").hide();
			$('#ichimokuDiv :checkbox').prop("checked", false);
		}
		if($("#Talib").is(':checked'))
		{
			console.log("Talib checked");
			$("#talibDiv").show();
		}
		else
		{
			console.log("Talib Unchecked");
			$("#talibDiv").hide();
			$('#talibDiv :checkbox').prop("checked", false);
		}
	
	
	//dateValueNavPair is common for all three types of indicators(i.e fractal,ichimoku,talib) which strores [date,nav] json
	var dateValueNavPair=<%=navJson%>
	
	//fractal indicators
	var dateValueUpFractalPair=<%=upFractalJson%>
	var dateValueLowFractalpair=<%=lowFractalJson%>
	var dateValueSwingHighPair=<%=swingHighJson%>
	var dateValueSwingLowPair=<%=swingLowJson%>
	var dateValueTcfl1Pair=<%=tcfl1Json%>
	var dateValueTcfl2Pair=<%=tcfl2Json%>
	var dateValueTcfl3Pair=<%=tcfl3Json%>
	
	//ichimoku indicators
	var tenkanSen=<%=tenkanSenJson%>
	var kijunSen=<%=kijunSenJson%>
	var senkouSpanA=<%=senkouSpanAJson%>
	var senkouSpanB=<%=senkouSpanBJson%>
	var kumoTop=<%=kumoTopJson%>
	var kumoBottom=<%=kumoBottomJson%>
	
	//talib indicators
	var ema20=<%=ema20Json%>
	var ema50=<%=ema50Json%>
	var ema200=<%=ema200Json%>
	var macd=<%=macdJson%>
	var macdSignal=<%=macdSignalJson%>
	var macdHistogram=<%=macdHistogramJson%>
	var upperBand=<%=upperBandJson%>
	var middleBand=<%=middleBandJson%>
	var lowerBand=<%=lowerBandJson%>
	var rsi=<%=rsiJson%>
	var psar=<%=psarJson%>
	var adx=<%=adxJson%>
	var atr=<%=atrJson%>
	
	/* //increment is used to assign yAxis attribute in series array (eg. yAxis_fractal,yAxis_ichimoku etc.)
	var increment=0; */
	
	//below variables represent checkboxes
	var fractalCheckbox="<%=request.getParameter("Fractal")%>";
	var ichimokuCheckbox="<%=request.getParameter("Ichimoku")%>";
	var talibCheckbox="<%=request.getParameter("Talib")%>";
	
	var u="<%=request.getParameter("upFractal")%>";
	var l="<%=request.getParameter("lowFractal")%>";
	var sh="<%=request.getParameter("swingHigh")%>";
	var sl="<%=request.getParameter("swingLow")%>";
	var tcfl1="<%=request.getParameter("tcfl1")%>";
	var tcfl2="<%=request.getParameter("tcfl2")%>";
	var tcfl3="<%=request.getParameter("tcfl3")%>";
	
	
	var tS="<%=request.getParameter("tenkanSen")%>"
	var kS="<%=request.getParameter("kijunSen")%>"
	var sSA="<%=request.getParameter("senkouSpanA")%>"
	var sSB="<%=request.getParameter("senkouSpanB")%>"
	var kT="<%=request.getParameter("kumoTop")%>"
	var kB="<%=request.getParameter("kumoBottom")%>"
	
	//below var (e20,macdS) are flags for checkBoxes since we need a separate type of charts for Talib
	var e20="<%=request.getParameter("ema20")%>"
	var e50="<%=request.getParameter("ema50")%>"
	var e200="<%=request.getParameter("ema200")%>"
	var macD="<%=request.getParameter("macd")%>"
	var macdS="<%=request.getParameter("macdSignal")%>"
	var macdH="<%=request.getParameter("macdHistogram")%>"
	var uB="<%=request.getParameter("upperBand")%>"
	var mB="<%=request.getParameter("middleBand")%>"
	var lB="<%=request.getParameter("lowerBand")%>"
	var rsI="<%=request.getParameter("rsi")%>"
	var psaR="<%=request.getParameter("psar")%>"
	var adX="<%=request.getParameter("adx")%>"
	var atR="<%=request.getParameter("atr")%>"
	
	var fractalTop=0,ichimokuTop=0,talibTop=0,macdTop=0,rsiTop=0,adxTop=0,atrTop=0;
	var overlayFractalHeight=0,overlayIchimokuHeight=0,overlayTalibHeight=0,macdHeight=0,rsiHeight=0,adxHeight=0,atrHeight=0;
	//this function takes indicator values and convert it to [[UTC Date,val],[UTC Date,val],[UTC Date,val]] i.e jsonArrays inside jsonArray
	insertJsonIntoSeriesArray();		
		
	mutualFundName=$("#dropdown option:selected").text();
		
	
	
	if(seriesArray.length!=0)
	{
		Highcharts.stockChart('container', {
			title:{
				text:mutualFundName
			},
			subtitle:{
				text:candleType
			},
			tooltip: {
	            pointFormat:'<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>', 
	            valueDecimals: 3,
	            split:true             
	       },
		    xAxis: {
		        type: 'datetime',
		        dateTimeLabelFormats: { // don't display the dummy year
		            month: '%e. %b',
		            year: '%b'
		        },
		        title: {
		            text: 'Date',
		            align: 'middle'
		        	}
		    },
		    yAxis:[
		           <%
		           if(request.getParameter("Fractal")!=null)
		           {
		           %>
		           		{		           		
			           		top:fractalTop,
			           		height:overlayFractalHeight,		           		
					        lineWidth: 2,
					        offset: 0,
					        labels: {
					        	formatter: function(){
					       		return this.value+" Rs.";
					       		}
					        },     
					        title: {
					            text: 'Basic Fractal',
					            align: 'middle'
					        	}				    		           		
		           		},	
		           	<%
		           }
		           if(request.getParameter("Ichimoku")!=null)
		           {
		        	 %>
		        	 {		           		
			           		top:ichimokuTop,
			           		height:overlayIchimokuHeight,		           		
					        lineWidth: 2,
					        offset: 0,
					        labels: {
					        	formatter: function(){
					       		return this.value+" Rs.";
					       		}
					        },     
					        title: {
					            text: 'Basic Ichimoku',
					            align: 'middle'
					        	}				    		           		
		           		},
		        	 <%
		           }
		           if((request.getParameter("Talib")!=null && request.getParameter("Ichimoku")==null && request.getParameter("Fractal")==null) || (request.getParameter("Talib")!=null && (request.getParameter("ema20")!=null || request.getParameter("ema50")!=null || request.getParameter("ema200")!=null || request.getParameter("psar")!=null || request.getParameter("upperBand")!=null || request.getParameter("middleBand")!=null || request.getParameter("lowerBand")!=null)))
		           {
		        	  %>
		        	  {		           		
			           		top:talibTop,
			           		height:overlayTalibHeight,		           		
					        lineWidth: 2,
					        offset: 0,
					        labels: {
					        	formatter: function(){
					       		return this.value+" Rs.";
					       		}
					        },     
					        title: {
					            text: 'Basic Talibs',
					            align: 'middle'
					        	}				    		           		
		           		},		        	  
		        	  <%
		           }
		           if(request.getParameter("macd")!=null || request.getParameter("macdSignal")!=null || request.getParameter("macdHistogram")!=null )
		           {
		           	   %>
		           		{
		           			height: macdHeight,
		           			//top specifies y pixel from top of screen to specify chart
		           			top:macdTop,
		           			
					        lineWidth: 2,
					        offset: 0,
					        labels: {
					        	formatter: function(){
					       		return this.value+" Rs.";
					       		}
					        },       
					    	title: {
				            text: 'macd types',
				            align: 'middle'
				        	}
		           		},
		           	 	<%
		           	}
		           	if(request.getParameter("rsi")!=null)
		           	{
		           	%>
		           		
			           	{
		           			height:rsiHeight,
		           			//top specifies y pixel from top of screen to specify chart
		           			top:rsiTop,
		           			
					        lineWidth: 2,
					        offset: 0,
					        labels: {
					        	formatter: function(){
					       		return this.value+" Rs.";
					       		}
					        },       
					    	title: {
				            text: 'rsi types',
				            align: 'middle'
				        	}
		           		},	           			
		           		
		           	<%
		           	}
		           	if(request.getParameter("adx")!=null)
		           	{
		           	%>
			           	{
		           			height: adxHeight,
		           			//top specifies y pixel from top of screen to specify chart
		           			top:adxTop,
		           			
					        lineWidth: 2,
					        offset: 0,
					        labels: {
					        	formatter: function(){
					       		return this.value+" Rs.";
					       		}
					        },       
					    	title: {
				            text: 'adx types',
				            align: 'middle'
				        	}
		           		},	     
		           	
		           	<%
		           	}
		           	if(request.getParameter("atr")!=null)
		           	{
		           	%>
			           	{
		           			height: atrHeight,
		           			//top specifies y pixel from top of screen to specify chart
		           			top:atrTop,
		           			
					        lineWidth: 2,
					        offset: 0,
					        labels: {
					        	formatter: function(){
					       		return this.value+" Rs.";
					       		}
					        },       
					    	title: {
				            text: 'atr types',
				            align: 'middle'
				        	}
		           		},	
		           	<%
		           	}
		           	%>		       
		    ],		
		    //can be enabled later  
	        series:seriesArray,
	            
		    		
		});
	}			
	
	function getJsonArray(dateValuePair,type,yAxis)
	{	
		outerArray=[];
		innerArrays=[];
		for(var i=0;i<dateValuePair.length;i++)
		{
			var ithJsonArray=dateValuePair[i];
			var year=parseInt(moment(ithJsonArray[0]).format("YYYY"));
			var month=parseInt(moment(ithJsonArray[0]).format("MM")-1);
			var date=parseInt(moment(ithJsonArray[0]).format("DD"));
			
			var amount=ithJsonArray[1];
			
			innerArrays=[];
			innerArrays[0]=Date.UTC(year,month,date);
			innerArrays[1]=amount;
			outerArray.push(innerArrays);
			
			//console.log("Amount="+amount+"  y/m/d="+year+""+month+""+date);
		}
		seriesArray.push(
		{
				type:'line',
				yAxis: yAxis,
		        animation: false,
		        stack: 0,
				name: type,
				data: outerArray,
			    
		})		
		
	} 
	
	 
	function insertJsonIntoSeriesArray()
	{		
		var bigCount=75;
		var yAxis=-1;
		//since we show everything about indicator(upFractal,lowFractal,swings,tcfl) and we get all the values in jsp above we assume that all the values gets generated
		if(fractalCheckbox!=null && fractalCheckbox=='on' && dateValueNavPair!=null)
		{
			fractalTop=bigCount+10;
			overlayFractalHeight=100;
			bigCount=fractalTop;
			yAxis++;
			console.log(fractalTop+"=fractalTop");
			console.log(overlayFractalHeight+"=ofHeight yAxis"+yAxis);
			
			getJsonArray(dateValueNavPair,"Nav",yAxis);
			if(u!=null && u==='on')
				getJsonArray(dateValueUpFractalPair,"UpFractal",yAxis);
			if(l!=null && l==='on')
				getJsonArray(dateValueLowFractalpair,"LowFractal",yAxis);
			if(sh!=null && sh==='on')
				getJsonArray(dateValueSwingHighPair,"SwingHigh",yAxis);
			if(sl!=null && sl==='on')
				getJsonArray(dateValueSwingLowPair,"SwingLow",yAxis);
			if(tcfl1!=null && tcfl1==='on')
				getJsonArray(dateValueTcfl1Pair, "TCFL1",yAxis);
			if(tcfl3!=null && tcfl2==='on')
				getJsonArray(dateValueTcfl2Pair, "TCFL2",yAxis);
			if(tcfl3!=null && tcfl3==='on')
				getJsonArray(dateValueTcfl3Pair, "TCFL3",yAxis);			
		}
		else
		{
			overlayFractalHeight=0;
		}
		if(ichimokuCheckbox!=null && ichimokuCheckbox=='on' && dateValueNavPair!=null)
		{
			ichimokuTop=bigCount+overlayFractalHeight+10;//where 10 is space between two charts in terms of pixel
			overlayIchimokuHeight=100;
			bigCount=ichimokuTop;
			yAxis++;
			console.log(ichimokuTop+"=ichimokuTop");
			console.log(overlayIchimokuHeight+"=ichimokuHeight  yAxis="+yAxis);
			if(fractalCheckbox!='on')
			{
				getJsonArray(dateValueNavPair,"Nav",yAxis);
			}
			if(tS!=null && tS==='on')
				getJsonArray(tenkanSen,"tenkanSen",yAxis);
			if(kS!=null && kS==='on')
				getJsonArray(kijunSen,"kijunSen",yAxis);
			if(sSA!=null && sSA==='on')
				getJsonArray(senkouSpanA,"senkouSpanA",yAxis);
			if(sSB!=null && sSB==='on')
				getJsonArray(senkouSpanB,"senkouSpanB",yAxis);
			if(kT!=null && kT==='on')
				getJsonArray(kumoTop,"kumoTop",yAxis);
			if(kB!=null && kB==='on')
				getJsonArray(kumoBottom,"kumoBottom",yAxis);
		}
		else
		{
			overlayIchimokuHeight=overlayFractalHeight;
		}
		if(talibCheckbox!=null && talibCheckbox=='on' && dateValueNavPair!=null)
		{
			var talibs=bigCount;
			var flag=0;
			yAxis++;
			
			if(fractalCheckbox!='on' && fractalCheckbox!=null)
			{
				if(ichimokuCheckbox!='on' && ichimokuCheckbox!=null)
				{
					talibTop=talibs+10;
					overlayTalibHeight=100;
					talibs=talibTop;
					flag=1;
					getJsonArray(dateValueNavPair,"Nav",yAxis);
				}
				else
				{		
					console.log("flag=0");
					talibTop=talibs+overlayIchimokuHeight+10;
					overlayTalibHeight=0;//overlayIchimokuHeight;
					talibs=talibTop;
					console.log("talibs+overlayIchimokuHeight="+talibs+"overlayTalibHeight="+overlayTalibHeight);
				}				
			}	
			else
			{		
				talibTop=talibs+overlayFractalHeight+10;
				overlayTalibHeight=0;
				talibs=talibTop;
			}
			console.log(talibTop+"=talibTop");
			console.log(overlayTalibHeight+"=overlaytalibHeight  yAxis="+yAxis);
			if(e20==='on' || e50==='on' || e200==='on' || uB==='on' || lB==='on' || mB==='on' || psaR==='on')
			{
				
				talibTop=talibs+overlayIchimokuHeight+10;
				overlayTalibHeight=100;
				talibs=talibTop;
				console.log(talibTop+"=talibTop");
				console.log(overlayTalibHeight+"=otalibHeight  yAxis="+yAxis);
				//don't need to yAxis++?already done in above;
			}
			else
			{
				if(flag==0)
				{
					
					//this condition occurs when atleast one of (fractalCheckbox or ichimokuCheckbox has occurred)
					if(ichimokuCheckbox=='on')
					{
						console.log("Yes");
						overlayTalibHeight=0;//overlayIchimokuHeight;
						yAxis--;
					}
					else
					{
						if(fractalCheckbox=='on')
						{
							overlayTalibHeight=0;//overlayFractalHeight;
							yAxis--;
						}
					}					
				}
			}
			
			if(e20!=null && e20==='on')
				getJsonArray(ema20,"ema20",yAxis);
			if(e50!=null && e50==='on')
				getJsonArray(ema50,"ema50",yAxis);
			if(e200!=null && e200==='on')
				getJsonArray(ema200,"ema200",yAxis);
			if(uB!=null && uB==='on')
				getJsonArray(upperBand,"upperBand",yAxis);
			if(mB!=null && mB==='on')
				getJsonArray(middleBand,"middleBand",yAxis);
			if(lB!=null && lB==='on')
				getJsonArray(lowerBand,"lowerband",yAxis);
			if(psaR!=null && psaR==='on')
				getJsonArray(psar,"psar",yAxis);
			
			if(macD==='on' || macdS==='on' || macdH==='on')
			{
				macdTop=talibs+overlayTalibHeight+10;
				macdHeight=50;
				talibs=macdTop;		
				yAxis++;
				console.log("macdTop="+macdTop+" macdHeight="+macdHeight+"  yAxis="+yAxis+"talibs="+talibs+" overlayTalibH="+overlayTalibHeight);
				
			}
			else
			{
				macdHeight=overlayTalibHeight;
			}
			if(macD!=null && macD==='on')
				getJsonArray(macd,"macd",yAxis);
				//getMacdJsonArray(macd,"macd",yAxis);
			if(macdS!=null && macdS==='on')
				getJsonArray(macdSignal,"macdSignal",yAxis);
				//getMacdJsonArray(macdSignal,"macdSignal",yAxis);
			if(macdH!=null && macdH==='on')
				getJsonArray(macdHistogram,"macdHistogram",yAxis);
				//getMacdJsonArray(macdHistogram,"macdHistogram",yAxis);
			if(rsI==='on')
			{
				rsiTop=talibs+macdHeight+10;
				rsiHeight=50;
				talibs=rsiTop;		
				yAxis++;
				console.log("rsiTop="+rsiTop+" rsiHeight="+rsiHeight+" yAxis="+yAxis);
			}
			else
			{
				rsiHeight=macdHeight;
			}
			if(rsI!=null && rsI==='on')
				getJsonArray(rsi,"rsi",yAxis);
			if(adX==='on')
			{
				adxTop=talibs+rsiHeight+10;
				adxHeight=50;
				talibs=adxTop;
				yAxis++;
				console.log("adxTop="+adxTop+" adxHeight="+adxHeight+" yAxis="+yAxis);
			}
			else
			{
				adxHeight=rsiHeight;
			}
			if(adX!=null && adX==='on')
				getJsonArray(adx,"adx",yAxis);
			if(atR==='on')
			{
				atrTop=talibs+adxHeight+10;
				atrHeight=50;
				talibs=atrTop;
				yAxis++;
				console.log("atrTop="+atrTop+" atrHeight="+atrHeight+" yAxis="+yAxis);
			}
			else
			{
				atrHeight=adxHeight;
			}
			if(atR!=null && atR==='on')
				getJsonArray(atr,"atr",yAxis);				
		}				
	}
	
});
</script> 
</body>
</html>