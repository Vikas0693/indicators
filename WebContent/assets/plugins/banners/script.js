var base="https://www.fundexpert.in/",
cookieScriptUrl=base+"assets/plugins/cookie/js.cookie.js",
styleFileUrl=base+"assets/plugins/banners/style.css",
banners=base+"tcbanners/",
modalCookieName="femodal",
modalTarget=base+"feature/traders-cockpit.jsp?ref=tcmodal",
modalBannerUrl=banners+"modal-banner.png";

var ads=[
	{
		name:"tceasyinvest",
		url:base+"?ref=tceasy",
		imageBase:base+"tcbanners/investeasy/"
	},
	{
		name:"tcsmallinvest",
		url:base+"?ref=tcsmallinv",
		imageBase:base+"tcbanners/investsmall/"
	},
	{
		name:"tctrack",
		url:base+"?ref=tctrack",
		imageBase:base+"tcbanners/trackportfolio/"
	},
	{
		name:"tcmanage",
		url:base+"?ref=tcmanage",
		imageBase:base+"tcbanners/manageportfolio/"
	},
	{
		name:"tctaxsaving",
		url:base+"?ref=tctaxsaving",
		imageBase:base+"tcbanners/taxsaving/"
	}
];


function loadScript(url, callback)
{
    // adding the script tag to the head as suggested before
   var head = document.getElementsByTagName('head')[0];
   var script = document.createElement('script');
   script.type = 'text/javascript';
   script.src = url;

   // then bind the event to the callback function 
   // there are several events for cross browser compatibility
   script.onreadystatechange = callback;
   script.onload = callback;

   // fire the loading
   head.appendChild(script);
}

function loadImage(containerId, url, callback)
{
    // adding the script tag to the head as suggested before
   var head = document.getElementById(containerId);
   var script = document.createElement('img');
   script.src = url;

   // then bind the event to the callback function 
   // there are several events for cross browser compatibility
   script.onreadystatechange = callback;
   script.onload = callback;

   // fire the loading
   head.appendChild(script);
}

if(typeof $=="undefined"){
	console.log("JQuery not installed.");
}else{
	$(document).ready(function(){
		var styleScript=$("<link/>").attr("href",styleFileUrl).attr("rel","stylesheet");
		$("head").append(styleScript);
		loadScript(cookieScriptUrl,function(){
			
			console.log("cookie script fully loaded");
			if(Cookies.get(modalCookieName)!="true"){
				var overlay=$("<div/>").attr("id","fe-overlay");
				var imageLink=$("<a/>").attr("href",modalTarget).attr("id","fe-modal-link").attr("target","_blank");
				var modal=$("<div/>").attr("id","fe-modal").append(imageLink);
				$("body").prepend(modal);
				
				$("body").prepend(overlay);
				
				$(overlay).click(function(){
					$("#fe-overlay").remove();
					$("#fe-modal").remove();
				});
				loadImage("fe-modal-link",modalBannerUrl,function(){
					$("#fe-overlay").show();					
					$("#fe-modal").show();					
				});
				Cookies.set(modalCookieName,"true",{expires:2});
			}else{
				console.log("cookie set");
			}
		});
		
		var ad300x600=$(".fe-ad-300x600");
		if(ad300x600.length>0){
			for(var i=0;i<ad300x600.length;i++){
				var adContainer=$(ad300x600[i])
				var ad=getRandomAd();
				placeAdd(adContainer, ad.imageBase+"300x600.png",ad.url);				
			}
		}
		
		var ad336x280=$(".fe-ad-336x280");
		if(ad336x280.length>0){
			for(var i=0;i<ad336x280.length;i++){
				var adContainer=$(ad336x280[i])
				var ad=getRandomAd();
				placeAdd(adContainer, ad.imageBase+"336x280.png",ad.url);				
			}
		}
		
		var ad728x90=$(".fe-ad-728x90");
		if(ad728x90.length>0){
			for(var i=0;i<ad728x90.length;i++){
				var adContainer=$(ad728x90[i])
				var ad=getRandomAd();
				placeAdd(adContainer, ad.imageBase+"728x90.png",ad.url);				
			}
		}
		
		
	});
	
	function placeAdd(container, imageUrl, targetUrl){
		var a=$("<a/>").attr("href",targetUrl).attr("target","_blank");
		a.append($("<img/>").attr("src",imageUrl));
		$(container).append(a);		
	}
	
	function getRandomAd(){
		var adNumber=Math.round(Math.random()*(ads.length-1))
		return ads[adNumber];
	}
}