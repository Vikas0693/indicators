<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.apidoc.util.JSONArrayValues" %>
<%@ page import="org.json.JSONArray"%>
<%@ page import="org.json.JSONObject"%>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.apidoc.util.JSONStrategyValues" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Charts</title>
		<!-- <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
		<script src="https://code.highcharts.com/stock/highstock.js"></script>
		<script src="https://code.highcharts.com/stock/modules/exporting.js"></script> -->
		<script src="chart/highstock.js"></script>
		<script src="chart/exporting.js"></script>
		<script src="scripts/moment.min.js"></script>
		<!-- Bootstrap Core CSS -->
		<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<!-- Plugins CSS -->
		<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="assets/plugins/animate-css/animate.min.css" rel="stylesheet" type="text/css">
		
		<!-- Component CSS -->
		<link href="assets/css/component/component.css" rel="stylesheet" type="text/css">
		<link href="assets/css/component/colors/red.css" rel="stylesheet" type="text/css">
		
		<!-- Main CSS -->
		<!-- <link href="assets/css/style.css" rel="stylesheet" type="text/css"> -->
		<link href="assets/css/app-style.css" rel="stylesheet" type="text/css">
		<link href="assets/css/rinjani.css" rel="stylesheet" type="text/css">
		<link href="assets/css/colors/red.css" rel="stylesheet" type="text/css">
		<link href="assets/css/style.css" rel="stylesheet" type="text/css">
		<style>
		div.Strategies {
		    position: fixed;
			right: 10px;
			top:10px;		
		}
		
		</style>
	</head>
	<body>
		<nav id="navigation" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		        <div class="container">
		            <div class="navbar-header page-scroll">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-rj-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>                
		                <a class="navbar-brand" href="https://www.fundexpert.in/app/#/portfolio">FundExpert</a>
		            </div>
		            <!-- //.navbar-header -->
		            <div class="navbar-collapse collapse navbar-rj-collapse">
			            <ul class="nav navbar-nav navbar-right" style="font-size:13px">
			        		
			        		
			        		<li class="dropdown">
				           		<a id="dLabel" role="button" data-target="#" href="#">
				                	080-33509639
					            </a>
			        		</li>
			        		
			        		<%-- <li class="page-scroll">
		                        <%
				        			boolean login = false;
					        		if(session.getAttribute("id")==null){
					        			login = false;
					        			%>
					        				<a href="#login" class="show-side-panel-link" id="loginClick" nextpage="<%=myLink %>" login="<%=userLoggedIn %>">Login / Register</a>
					        			<%
									}
					        		else{
										User user=(User)session.getAttribute("user");
										login = true;
										%>
				        					<a href="#login" nextpage="../app" class="show-side-panel-link" id="loginClick" login="<%=userLoggedIn %>">My Portfolio</a>
				        				<%
									}
				        		%>
		                    </li> --%>
			        	</ul>
			        </div>
		            <!-- //.navbar-collapse -->
	    	    </div>
	        	<!-- //.container -->
    	</nav>
    	<!-- //End Navbar -->
    	<section id="result" class="section">
	        <div class="section-inner">
	            <div class="section-content">
	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="section-title text-center">
	
	                            <h3 class="sub-title m-t-sm">Updated Charts</h3>
	
	                            <span class="section-line"></span>
	                        </div>
	                        
	                        <!-- //.section-title -->
	                        <div class="section-inner">
	                        	<div class="container">

								<%
									String submit=request.getParameter("submit");
									String candleType=null;
									String mutualFundId="";
									String typeOfIndicatorFractal="";
									String typeOfIndicatorIchimoku="";
									String typeOfIndicatorTalib="";
									String exitStrategy1,exitStrategy2,exitStrategy3,exitStrategy4,exitStrategy5,exitStrategy6,exitStrategy7;
									String entryStrategy1,entryStrategy2,entryStrategy3,entryStrategy4,entryStrategy5,entryStrategy6,entryStrategy7,entryStrategy8,entryStrategy9,entryStrategy10,entryStrategy11,entryStrategy12,entryStrategy13,entryStrategy14,entryStrategy15,entryStrategy16;
									
									JSONArray navJson=null;
									JSONArray upFractalJson=null,lowFractalJson=null,swingHighJson=null,swingLowJson=null,tcfl1Json=null,tcfl2Json=null,tcfl3Json=null;
									JSONArray tenkanSenJson=null,kijunSenJson=null,senkouSpanAJson=null,senkouSpanBJson=null,kumoTopJson=null,kumoBottomJson=null;
									JSONArray ema20Json=null,ema50Json=null,ema200Json=null,macdJson=null,macdSignalJson=null,macdHistogramJson=null,upperBandJson=null,middleBandJson=null,lowerBandJson=null,rsiJson=null,psarJson=null,adxJson=null,atrJson=null;
									
									JSONArray exitStrategyDateJson=new JSONArray();
									JSONArray entryStrategyDateJson=new JSONArray();
									
									JSONObject resp=new JSONObject();
									
									try
									{
										MutualFundController mfc=new MutualFundController();
										List<Object[]> mfList=mfc.getMutualFundList();
										System.out.print(mfList.size());
										//JSONArray ohlcJson=null;
										if(submit==null)
										{
									%>		
									<br>
									<div class="col-sm-12 col-xs-12" style="background-color:white">
										<form id="form" name="form" action="updated-charts-flags.jsp" method="get" class="m-t-md m-b-sm">
			
											<div class="col-sm-5 col-xs-12">
												<div class="col-sm-12 col-xs-12" style="height:120px;border:1px solid #eee">
													<div class="col-sm-3 no-padding">
														<label><input name="exitstrategy1" type="checkbox"> Exit 1</label>
													</div>
													<div class="col-sm-3 no-padding">
														<label><input name="exitstrategy2" type="checkbox"> Exit 2</label>
													</div>
													<div class="col-sm-3 no-padding">
														<label><input name="exitstrategy3" type="checkbox"> Exit 3</label>
													</div>
													<div class="col-sm-3 no-padding">
														<label><input name="exitstrategy4" type="checkbox"> Exit 4</label>
													</div>
													<div class="col-sm-3 no-padding">
														<label><input name="exitstrategy5" type="checkbox"> Exit 5</label>
													</div>
													<div class="col-sm-3 no-padding">
														<label><input name="exitstrategy6" type="checkbox"> Exit 6</label>
													</div>
													<div class="col-sm-3 no-padding">
														<label><input name="exitstrategy7" type="checkbox"> Exit 7</label>
													</div>
												</div>
											</div>
											<div class="col-sm-7 col-xs-12">
												<div class="col-xs-12 col-sm-12" style="height:120px;border:1px solid #eee;">
													<div class="col-sm-3">
														<label><input name="entrystrategy1" type="checkbox"> Entry 1</label>
													</div>
													<div class="col-sm-3">
														<label><input name="entrystrategy2" type="checkbox"> Entry 2</label>
													</div>
													<div class="col-sm-3">
														<label><input name="entrystrategy3" type="checkbox"> Entry 3</label>
													</div>
													<div class="col-sm-3">
														<label><input name="entrystrategy4" type="checkbox"> Entry 4</label>
													</div>
													<div class="col-sm-3">
														<label><input name="entrystrategy5" type="checkbox"> Entry 5</label>
													</div>
													<div class="col-sm-3">
														<label><input name="entrystrategy6" type="checkbox"> Entry 6</label>
													</div>
													<div class="col-sm-3">
														<label><input name="entrystrategy7" type="checkbox"> Entry 7</label>
													</div>
													<div class="col-sm-3">
														<label><input name="entrystrategy8" type="checkbox"> Entry 8</label>
													</div>
													<div class="col-sm-3">
														<label><input name="entrystrategy9" type="checkbox"> Entry 9</label>
													</div>
													<div class="col-sm-3">
														<label><input name="entrystrategy10" type="checkbox"> Entry 10</label>
													</div>
													<div class="col-sm-3">
														<label><input name="entrystrategy11" type="checkbox"> Entry 11</label>
													</div>
													<div class="col-sm-3">
														<label><input name="entrystrategy12" type="checkbox"> Entry 12</label>
													</div>
													<div class="col-sm-3">
														<label><input name="entrystrategy13" type="checkbox"> Entry 13</label>
													</div>
													<div class="col-sm-3">
														<label><input name="entrystrategy14" type="checkbox"> Entry 14</label>
													</div>
													<div class="col-sm-3">
														<label><input name="entrystrategy15" type="checkbox"> Entry 15</label>
													</div>
													<div class="col-sm-3">
														<label><input name="entrystrategy16" type="checkbox"> Entry 16</label>
													</div>
												</div>
											</div>
											<div class="col-sm-12 col-xs-12 m-t-sm">
												<div class="col-sm-12 col-xs-12" style="border:1px solid #eee">
													<div class="col-sm-4 col-xs-12">
														<label><input type="checkbox" id="Fractal" name="Fractal"/> Fractal</label>
														<div id="fractalDiv" style="display:none">
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="upFractal"/> TENKANSEN</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="lowFractal"/> LOW FRACTAL</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="swingHigh"/> SWING HIGH</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="swingLow"/> SWING LOW</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="tcfl1"/> TCFL 1</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="tcfl2"/> TCFL 2</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="tcfl3"/> TCFL3</small>
															</div>
														</div>
													</div>
													<div class="col-sm-4 col-xs-12">
														<label><input type="checkbox" id="Ichimoku" name="Ichimoku"/> Ichimoku</label>
														<div id="ichimokuDiv" style="display:none">
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="tenkanSen"/> TENKANSEN</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="kijunSen"/> KIJUNSEN</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="senkouSpanA"/> SENKOU SPAN A</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="senkouSpanB"/> SENKOU SPAN B</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="kumoTop"/> KUMO TOP</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="kumoBottom"/> KUMO BOTTOM</small>
															</div>
														</div>
													</div>
													<div class="col-sm-4 col-xs-12">
														<label><input type="checkbox" id="Talib" name="Talib"/> Talib</label>
														<div id="talibDiv" style="display:none">
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="ema20"/> EMA 20</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="ema50"/> EMA 50</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="ema200"/>EMA 200</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="macd"/> MACD</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="macdSignal"/>MACD SIGNAL</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="macdHistogram"/>MACD HISTOGRAM</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="upperBand"/>UPPER BAND</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="middleBand"/>MIDDLE BAND</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="lowerBand"/>LOWER BAND</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="rsi"/>RSI</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="psar"/>PSAR</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="adx"/>ADX</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="atr"/>ATR</small>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-12 col-xs-12 m-t-sm m-b-sm">
												<div class="col-xs-12 col-sm-12" style="border:1px solid #eee;padding:10px;margin-bottom:10px">
													<div class="col-sm-8 col-xs-12">
														<div class="form-group">
															<select id="dropdown" name="mutualFundId" class="form-control">
																<%
																	Iterator<Object[]> itr=mfList.iterator();
																	while(itr.hasNext())
																	{
																		Object obj[]=(Object[])itr.next();
																		long id=(Long)obj[0];
																		String mfName=(String)obj[1];
																%>	
																		<option name="mutualFundName" id="mutualFundName" value="<%=id%>" ><%=mfName+" "+id%></option>
																<%
																	}	
																%>
															</select>
														</div>
													</div>
													<div class="col-sm-2 col-xs-12">
														<div class="col-sm-6 col-xs-12 no-padding">
															<label><input type="radio" name="candle" value="Daily"/> Daily</label>
														</div>
														<div class="col-sm-6 col-xs-12 no-padding">
															<label><input type="radio" name="candle" value="Weekly"/> Weekly</label>
														</div>
													</div>
													<div class="col-sm-2 col-xs-12">
														<input type="submit" id="submit" name="submit" value="submit" class="btn btn-rj btn-block"/>
													</div>
												</div>
											</div>
										</form>
									</div>
		
									<%
										}
										else if(submit!=null)
										{
											
											System.out.print("Submitted");
									%>
									<br>
									<div class="col-sm-12 col-xs-12" style="background-color:white">
										<form id="form" name="form" action="updated-charts-flags.jsp" method="get" class="m-t-md m-b-sm">
											<div class="col-sm-5 col-xs-12">
												<div class="col-sm-12 col-xs-12" style="height:120px;border:1px solid #eee">
													<div class="col-sm-3 no-padding">
														<label>
															<input name="exitstrategy1" type="checkbox" <%=request.getParameter("exitstrategy1")!=null?(request.getParameter("exitstrategy1").equals("on")?"checked='checked'":""):"" %>>
															Exit 1
														</label>
													</div>
													<div class="col-sm-3 no-padding">
														<label>
															<input name="exitstrategy2" type="checkbox" <%=request.getParameter("exitstrategy2")!=null?(request.getParameter("exitstrategy2").equals("on")?"checked='checked'":""):"" %>>
															Exit 2
														</label>
													</div>
													<div class="col-sm-3 no-padding">
														<label>
															<input name="exitstrategy3" type="checkbox" <%=request.getParameter("exitstrategy3")!=null?(request.getParameter("exitstrategy3").equals("on")?"checked='checked'":""):"" %>>
															Exit 3
														</label>
													</div>
													<div class="col-sm-3 no-padding">
														<label>
															<input name="exitstrategy4" type="checkbox" <%=request.getParameter("exitstrategy4")!=null?(request.getParameter("exitstrategy4").equals("on")?"checked='checked'":""):"" %>>
															Exit 4
														</label>
													</div>
													<div class="col-sm-3 no-padding">
														<label>
															<input name="exitstrategy5" type="checkbox" <%=request.getParameter("exitstrategy5")!=null?(request.getParameter("exitstrategy5").equals("on")?"checked='checked'":""):"" %>>
															Exit 5
														</label>
													</div>
													<div class="col-sm-3 no-padding">
														<label>
															<input name="exitstrategy6" type="checkbox" <%=request.getParameter("exitstrategy6")!=null?(request.getParameter("exitstrategy6").equals("on")?"checked='checked'":""):"" %>>
															Exit 6
														</label>
													</div>
													<div class="col-sm-3 no-padding">
														<label>
															<input name="exitstrategy7" type="checkbox" <%=request.getParameter("exitstrategy7")!=null?(request.getParameter("exitstrategy7").equals("on")?"checked='checked'":""):"" %>>
															Exit 7
														</label>
													</div>
												</div>
											</div>
											<div class="col-sm-7 col-xs-12" style="height:120px;border:1px solid #eee">
												<div class="col-sm-3 no-padding">
													<label>
														<input name="entrystrategy1" type="checkbox" <%=request.getParameter("entrystrategy1")!=null?(request.getParameter("entrystrategy1").equals("on")?"checked='checked'":""):"" %>>
														Entry 1
													</label>
												</div>
												<div class="col-sm-3 no-padding">
													<label>
														<input name="entrystrategy2" type="checkbox" <%=request.getParameter("entrystrategy2")!=null?(request.getParameter("entrystrategy2").equals("on")?"checked='checked'":""):"" %>>
														Entry 2
													</label>
												</div>
												<div class="col-sm-3 no-padding">
													<label>
														<input name="entrystrategy3" type="checkbox" <%=request.getParameter("entrystrategy3")!=null?(request.getParameter("entrystrategy3").equals("on")?"checked='checked'":""):"" %>>
														Entry 3
													</label>
												</div>
												<div class="col-sm-3 no-padding">
													<label>
														<input name="entrystrategy4" type="checkbox" <%=request.getParameter("entrystrategy4")!=null?(request.getParameter("entrystrategy4").equals("on")?"checked='checked'":""):"" %>>
														Entry 4
													</label>
												</div>
												<div class="col-sm-3 no-padding">
													<label>
														<input name="entrystrategy5" type="checkbox" <%=request.getParameter("entrystrategy5")!=null?(request.getParameter("entrystrategy5").equals("on")?"checked='checked'":""):"" %>>
														Entry 5
													</label>
												</div>
												<div class="col-sm-3 no-padding">
													<label>
														<input name="entrystrategy6" type="checkbox" <%=request.getParameter("entrystrategy6")!=null?(request.getParameter("entrystrategy6").equals("on")?"checked='checked'":""):"" %>>
														Entry 6
													</label>
												</div>
												<div class="col-sm-3 no-padding">
													<label>
														<input name="entrystrategy7" type="checkbox" <%=request.getParameter("entrystrategy7")!=null?(request.getParameter("entrystrategy7").equals("on")?"checked='checked'":""):"" %>>
														Entry 7
													</label>
												</div>
												<div class="col-sm-3 no-padding">
													<label>
														<input name="entrystrategy8" type="checkbox" <%=request.getParameter("entrystrategy8")!=null?(request.getParameter("entrystrategy8").equals("on")?"checked='checked'":""):"" %>>
														Entry 8
													</label>
												</div>
												<div class="col-sm-3 no-padding">
													<label>
														<input name="entrystrategy9" type="checkbox" <%=request.getParameter("entrystrategy9")!=null?(request.getParameter("entrystrategy9").equals("on")?"checked='checked'":""):"" %>>
														Entry 9
													</label>
												</div>
												<div class="col-sm-3 no-padding">
													<label>
														<input name="entrystrategy10" type="checkbox" <%=request.getParameter("entrystrategy10")!=null?(request.getParameter("entrystrategy10").equals("on")?"checked='checked'":""):"" %>>
														Entry 10
													</label>
												</div>
												<div class="col-sm-3 no-padding">
													<label>
														<input name="entrystrategy11" type="checkbox" <%=request.getParameter("entrystrategy11")!=null?(request.getParameter("entrystrategy11").equals("on")?"checked='checked'":""):"" %>>
														Entry 11
													</label>
												</div>
												<div class="col-sm-3 no-padding">
													<label>
														<input name="entrystrategy12" type="checkbox" <%=request.getParameter("entrystrategy12")!=null?(request.getParameter("entrystrategy12").equals("on")?"checked='checked'":""):"" %>>
														Entry 12
													</label>
												</div>
												<div class="col-sm-3 no-padding">
													<label>
														<input name="entrystrategy13" type="checkbox" <%=request.getParameter("entrystrategy13")!=null?(request.getParameter("entrystrategy13").equals("on")?"checked='checked'":""):"" %>>
														Entry 13
													</label>
												</div>
												<div class="col-sm-3 no-padding">
													<label>
														en14:<input name="entrystrategy14" type="checkbox" <%=request.getParameter("entrystrategy14")!=null?(request.getParameter("entrystrategy14").equals("on")?"checked='checked'":""):"" %>>
														Entry 14
													</label>
												</div>
												<div class="col-sm-3 no-padding">
													<label>
														<input name="entrystrategy15" type="checkbox" <%=request.getParameter("entrystrategy15")!=null?(request.getParameter("entrystrategy15").equals("on")?"checked='checked'":""):"" %>>
														Entry 15
													</label>
												</div>
												<div class="col-sm-3 no-padding">
													<label>
														<input name="entrystrategy16" type="checkbox" <%=request.getParameter("entrystrategy16")!=null?(request.getParameter("entrystrategy16").equals("on")?"checked='checked'":""):"" %>>
														Entry 16
													</label>
												</div>
											</div>
											<div class="col-sm-12 col-xs-12 m-t-sm">
												<div class="col-sm-12 col-xs-12" style="border:1px solid #eee">
													<div class="col-sm-4 col-xs-12">
														<label>
															<input type="checkbox" id="Fractal" name="Fractal" <%=request.getParameter("Fractal")!=null?(request.getParameter("Fractal").toString().equals("on")?"checked='checked'":""):"" %>/>
															Fractal
														</label>
														<div id="fractalDiv" style="display:none">
															<div class="col-sm-6 no-padding">
																<small>
																	<input type="checkbox" name="upFractal" <%=request.getParameter("upFractal")!=null?(request.getParameter("upFractal").toString().equals("on")?"checked='checked'":""):"" %>>
																	UP FRACTAL
																</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small>
																	<input type="checkbox" name="lowFractal" <%=request.getParameter("lowFractal")!=null?(request.getParameter("lowFractal").toString().equals("on")?"checked='checked'":""):"" %>/>
																	LOW FRACTAL
																</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small>
																	<input type="checkbox" name="swingHigh" <%=request.getParameter("swingHigh")!=null?(request.getParameter("swingHigh").toString().equals("on")?"checked='checked'":""):"" %>>
																	SWING HIGH
																</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small>
																	<input type="checkbox" name="swingLow" <%=request.getParameter("swingLow")!=null?(request.getParameter("swingLow").toString().equals("on")?"checked='checked'":""):"" %>/>
																	SWING LOW
																</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small>
																	<input type="checkbox" name="tcfl1" <%=request.getParameter("tcfl1")!=null?(request.getParameter("tcfl1").toString().equals("on")?"checked='checked'":""):"" %>/>
																	TCFL 1
																</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small>
																	<input type="checkbox" name="tcfl2" <%=request.getParameter("tcfl2")!=null?(request.getParameter("tcfl2").toString().equals("on")?"checked='checked'":""):"" %>/>
																	TCFL 2
																</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small>
																	<input type="checkbox" name="tcfl3" <%=request.getParameter("tcfl3")!=null?(request.getParameter("tcfl3").toString().equals("on")?"checked='checked'":""):"" %>/>
																	TCFL 3
																</small>
															</div>
														</div>
													</div>
													<div class="col-sm-4 col-xs-12">
														<label>
															<input type="checkbox" id="Ichimoku" name="Ichimoku" <%=request.getParameter("Ichimoku")!=null?(request.getParameter("Ichimoku").toString().equals("on")?"checked='checked'":""):"" %>/>
															Ichimoku
														</label>
														<div id="ichimokuDiv" style="display:none">
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="tenkanSen" <%=request.getParameter("tenkanSen")!=null?(request.getParameter("tenkanSen").toString().equals("on")?"checked='checked'":""):"" %>/> TENKANSEN</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="kijunSen" <%=request.getParameter("kijunSen")!=null?(request.getParameter("kijunSen").toString().equals("on")?"checked='checked'":""):"" %>/> KIJUNSEN</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="senkouSpanA" <%=request.getParameter("senkouSpanA")!=null?(request.getParameter("senkouSpanA").toString().equals("on")?"checked='checked'":""):"" %>/> SENKOU SPAN A</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="senkouSpanB" <%=request.getParameter("senkouSpanB")!=null?(request.getParameter("senkouSpanB").toString().equals("on")?"checked='checked'":""):"" %>/> SENKOU SPAN B</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="kumoTop" <%=request.getParameter("kumoTop")!=null?(request.getParameter("kumoTop").toString().equals("on")?"checked='checked'":""):"" %>/> KUMO TOP</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="kumoBottom" <%=request.getParameter("kumoBottom")!=null?(request.getParameter("kumoBottom").toString().equals("on")?"checked='checked'":""):"" %>/> KUMO BOTTOM</small>
															</div>
														</div>
													</div>
													<div class="col-sm-4 col-xs-12">
														<label><input type="checkbox" id="Talib" name="Talib" <%=request.getParameter("Talib")!=null?(request.getParameter("Talib").toString().equals("on")?"checked='checked'":""):"" %>/> Talib</label>
														<div id="talibDiv" style="display:none">
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="ema20" <%=request.getParameter("ema20")!=null?(request.getParameter("ema20").toString().equals("on")?"checked='checked'":""):"" %>/> EMA 20</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="ema50" <%=request.getParameter("ema50")!=null?(request.getParameter("ema50").toString().equals("on")?"checked='checked'":""):"" %>/> EMA 50</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="ema200" <%=request.getParameter("ema200")!=null?(request.getParameter("ema200").toString().equals("on")?"checked='checked'":""):"" %>/> EMA 200</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="macd" <%=request.getParameter("macd")!=null?(request.getParameter("macd").toString().equals("on")?"checked='checked'":""):"" %>/> MACD</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="macdSignal" <%=request.getParameter("macdSignal")!=null?(request.getParameter("macdSignal").toString().equals("on")?"checked='checked'":""):"" %>/> MACD SIGNAL</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="macdHistogram" <%=request.getParameter("macdHistogram")!=null?(request.getParameter("macdHistogram").toString().equals("on")?"checked='checked'":""):"" %>/> MACD HISTOGRAM</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="upperBand" <%=request.getParameter("upperBand")!=null?(request.getParameter("upperBand").toString().equals("on")?"checked='checked'":""):"" %>/> UPPER BAND</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="middleBand" <%=request.getParameter("middleBand")!=null?(request.getParameter("middleBand").toString().equals("on")?"checked='checked'":""):"" %>/> MIDDLE BAND</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="lowerBand" <%=request.getParameter("lowerBand")!=null?(request.getParameter("lowerBand").toString().equals("on")?"checked='checked'":""):"" %>/> LOWER BAND</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="rsi" <%=request.getParameter("rsi")!=null?(request.getParameter("rsi").toString().equals("on")?"checked='checked'":""):"" %>/> RSI</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="psar" <%=request.getParameter("psar")!=null?(request.getParameter("psar").toString().equals("on")?"checked='checked'":""):"" %>/> PSAR</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="adx" <%=request.getParameter("adx")!=null?(request.getParameter("adx").toString().equals("on")?"checked='checked'":""):"" %>/> ADX</small>
															</div>
															<div class="col-sm-6 no-padding">
																<small><input type="checkbox" name="atr" <%=request.getParameter("atr")!=null?(request.getParameter("atr").toString().equals("on")?"checked='checked'":""):"" %>/> ATR</small>
															</div>
														</div>
													</div>	
												</div>
											</div>
											<div class="col-sm-12 col-xs-12 m-t-sm m-b-sm">
												<div class="col-xs-12 col-sm-12" style="border:1px solid #eee;padding:10px;margin-bottom:10px">
													<div class="col-sm-8 col-xs-12">
														<div class="form-group">
															<select id="dropdown" name="mutualFundId" class="form-control">
																<%
																	Iterator<Object[]> itr=mfList.iterator();
																	while(itr.hasNext())
																	{
																		Object obj[]=(Object[])itr.next();
																		long id=(Long)obj[0];
																		String mfName=(String)obj[1];
																%>
																		<option name="mutualFundName" id="mutualFundName" value="<%=id%>" <%=request.getParameter("mutualFundId")!=null?(request.getParameter("mutualFundId").toString().equals(id+"")?"selected='selected'":""):"" %>><%=mfName+" "+id%></option>
																<%
																	}	
																	System.out.print("After submitted and id=dropdown");
																%>
															</select>
														</div>
													</div>
													<div class="col-sm-2 col-xs-12">
														<div class="col-sm-6 col-xs-12 no-padding">
															<label><input type="radio" name="candle" value="Daily"/> Daily</label>
														</div>
														<div class="col-sm-6 col-xs-12 no-padding">
															<label><input type="radio" name="candle" value="Weekly"/> Weekly</label>
														</div>
													</div>
													<div class="col-sm-2 col-xs-12">
														<input type="submit" id="submit" name="submit" value="submit" class="btn btn-rj btn-block"/>
													</div>
												</div>
											</div>
										</form>
									</div>
	
									<%
										candleType=request.getParameter("candle");
										mutualFundId=request.getParameter("mutualFundId");
										
										typeOfIndicatorFractal=request.getParameter("Fractal");
										typeOfIndicatorIchimoku=request.getParameter("Ichimoku");
										typeOfIndicatorTalib=request.getParameter("Talib");
										
										exitStrategy1=request.getParameter("exitstrategy1");
										exitStrategy2=request.getParameter("exitstrategy2");
										exitStrategy3=request.getParameter("exitstrategy3");
										exitStrategy4=request.getParameter("exitstrategy4");
										exitStrategy5=request.getParameter("exitstrategy5");
										exitStrategy6=request.getParameter("exitstrategy6");
										exitStrategy7=request.getParameter("exitstrategy7");
			
										entryStrategy1=request.getParameter("entrystrategy1");
										entryStrategy2=request.getParameter("entrystrategy2");
										entryStrategy3=request.getParameter("entrystrategy3");
										entryStrategy4=request.getParameter("entrystrategy4");
										entryStrategy5=request.getParameter("entrystrategy5");
										entryStrategy6=request.getParameter("entrystrategy6");
										entryStrategy7=request.getParameter("entrystrategy7");
										entryStrategy8=request.getParameter("entrystrategy8");
										entryStrategy9=request.getParameter("entrystrategy9");
										entryStrategy10=request.getParameter("entrystrategy10");
										entryStrategy11=request.getParameter("entrystrategy11");
										entryStrategy12=request.getParameter("entrystrategy12");
										entryStrategy13=request.getParameter("entrystrategy13");
										entryStrategy14=request.getParameter("entrystrategy14");
										entryStrategy15=request.getParameter("entrystrategy15");
										entryStrategy16=request.getParameter("entrystrategy16");
			
										if(typeOfIndicatorFractal!=null && typeOfIndicatorFractal.equalsIgnoreCase("on"))
										{		
											JSONArrayValues json=new JSONArrayValues();
											//below function will generate format of [[date,value],[date,value]] for Fractal table
											try
											{
												
												json.setFractalJson(mutualFundId,candleType);
											}
											catch(Exception e2)
											{
												resp.put("success", false);
												resp.put("error","did not get jsonArrayValues for fractal function working");
											}
											navJson=json.getNav(); 
											upFractalJson=json.getUpFractal();			
											lowFractalJson=json.getLowFractal();
											swingHighJson=json.getSwingHigh();			
											swingLowJson=json.getSwingLow();			
											tcfl1Json=json.getTcfl1();			
											tcfl2Json=json.getTcfl2();
											tcfl3Json=json.getTcfl3();
											System.out.println("Fractal 1:");
										}
			
										if(typeOfIndicatorIchimoku!=null && typeOfIndicatorIchimoku.equalsIgnoreCase("on"))
										{
											JSONArrayValues json=new JSONArrayValues();
											//below function will generate format of [[date,value],[date,value]] for Ichimoku table
											try
											{
												json.setIchimokuJson(mutualFundId,candleType);
											}
											catch(Exception e)
											{
												resp.put("success", false);
												resp.put("error","did not get jsonArrayValues Ichimoku function working");
											}				
											navJson=json.getNav();
											tenkanSenJson=json.getTenkanSen();
											kijunSenJson=json.getKijunSen();
											senkouSpanAJson=json.getSenkouSpanA();
											senkouSpanBJson=json.getSenkouSpanB();
											kumoTopJson=json.getKumoTop();
											kumoBottomJson=json.getKumoBottom();
										}
										
										
										if(typeOfIndicatorTalib!=null && typeOfIndicatorTalib.equalsIgnoreCase("on"))
										{
											JSONArrayValues json=new JSONArrayValues();
											//below function will generate format of [[date,value],[date,value]] for Ichimoku table
											try
											{
												json.setTalibJson(mutualFundId,candleType);
											}
											catch(Exception e)
											{
												System.out.print("Talib Exception");
												resp.put("success", false);
												resp.put("error","did not get jsonArrayValues Talib function working");
											}				
											navJson=json.getNav();
											ema20Json=json.getEma20();
											ema50Json=json.getEma50();
											ema200Json=json.getEma200();
											macdJson=json.getMacd();
											macdSignalJson=json.getMacdSignal();
											macdHistogramJson=json.getMacdHistogram();
											upperBandJson=json.getUpperBand();
											middleBandJson=json.getMiddleBand();
											lowerBandJson=json.getLowerBand();
											rsiJson=json.getRsi();
											psarJson=json.getPsar();
											adxJson=json.getAdx();
											atrJson=json.getAtr();
										}
			
										if(exitStrategy1!=null && exitStrategy1.equalsIgnoreCase("on"))
										{
											System.out.println("ex1");
											JSONStrategyValues exitAlert1=new JSONStrategyValues();
											JSONArray exitDatesArray=exitAlert1.getExitStrategyAlertDates(mutualFundId,"ExitStrategy1");
											for(int j=0;j<exitDatesArray.length();j++)
												exitStrategyDateJson.put(exitDatesArray.get(j));
											System.out.println("Exit1="+exitAlert1.getExitStrategyAlertDates(mutualFundId,"ExitStrategy1").toString(1));
										}
										if(exitStrategy2!=null && exitStrategy2.equalsIgnoreCase("on"))
										{
											System.out.println("ex2");
											JSONStrategyValues exitAlert2=new JSONStrategyValues();
											JSONArray exitDatesArray = exitAlert2.getExitStrategyAlertDates(mutualFundId,"ExitStrategy2");
											for(int j=0;j<exitDatesArray.length();j++)
												exitStrategyDateJson.put(exitDatesArray.get(j));
											System.out.println("Exit2="+exitAlert2.getExitStrategyAlertDates(mutualFundId,"ExitStrategy2").toString(1));
										}
										
										if(exitStrategy3!=null && exitStrategy3.equalsIgnoreCase("on"))
										{
											System.out.println("ex3");
											JSONStrategyValues exitAlert3=new JSONStrategyValues();
											JSONArray exitDatesArray = exitAlert3.getExitStrategyAlertDates(mutualFundId,"ExitStrategy3");
											for(int j=0;j<exitDatesArray.length();j++)
												exitStrategyDateJson.put(exitDatesArray.get(j));
											System.out.println("Exit3="+exitAlert3.getExitStrategyAlertDates(mutualFundId,"ExitStrategy3").toString(1));
										}
										
										if(exitStrategy4!=null && exitStrategy4.equalsIgnoreCase("on"))
										{
											System.out.println("ex4");
											JSONStrategyValues exitAlert4=new JSONStrategyValues();
											JSONArray exitDatesArray = exitAlert4.getExitStrategyAlertDates(mutualFundId,"ExitStrategy4");
											for(int j=0;j<exitDatesArray.length();j++)
												exitStrategyDateJson.put(exitDatesArray.get(j));
											System.out.println("Exit4="+exitAlert4.getExitStrategyAlertDates(mutualFundId,"ExitStrategy4").toString(1));
										}
										
										if(exitStrategy5!=null && exitStrategy5.equalsIgnoreCase("on"))
										{
											System.out.println("ex5");
											JSONStrategyValues exitAlert5=new JSONStrategyValues();
											JSONArray exitDatesArray = exitAlert5.getExitStrategyAlertDates(mutualFundId,"ExitStrategy5");
											for(int j=0;j<exitDatesArray.length();j++)
												exitStrategyDateJson.put(exitDatesArray.get(j));
											System.out.println("Exit5="+exitAlert5.getExitStrategyAlertDates(mutualFundId,"ExitStrategy5").toString(1));
										}
										
										if(exitStrategy6!=null && exitStrategy6.equalsIgnoreCase("on"))
										{
											System.out.println("ex6");
											JSONStrategyValues exitAlert6=new JSONStrategyValues();
											JSONArray exitDatesArray = exitAlert6.getExitStrategyAlertDates(mutualFundId,"ExitStrategy6");
											for(int j=0;j<exitDatesArray.length();j++)
												exitStrategyDateJson.put(exitDatesArray.get(j));
											System.out.println("Exit6="+exitAlert6.getExitStrategyAlertDates(mutualFundId,"ExitStrategy6").toString(1));
										}
										
										if(exitStrategy7!=null && exitStrategy7.equalsIgnoreCase("on"))
										{
											System.out.println("ex7");
											JSONStrategyValues exitAlert7=new JSONStrategyValues();
											JSONArray exitDatesArray = exitAlert7.getExitStrategyAlertDates(mutualFundId,"ExitStrategy7");
											for(int j=0;j<exitDatesArray.length();j++)
												exitStrategyDateJson.put(exitDatesArray.get(j));
											System.out.println("Exit7="+exitAlert7.getExitStrategyAlertDates(mutualFundId,"ExitStrategy7").toString(1));
										}
			
										if(entryStrategy1!=null && entryStrategy1.equalsIgnoreCase("on"))
										{
											System.out.println("en1");
											JSONStrategyValues entryAlert1=new JSONStrategyValues();
											JSONArray entryDatesArray=entryAlert1.getEntryStrategyAlertDates(mutualFundId,"EntryStrategy1");
											for(int j=0;j<entryDatesArray.length();j++)
											entryStrategyDateJson.put(entryDatesArray.get(j));
										}
										
										if(entryStrategy2!=null && entryStrategy2.equalsIgnoreCase("on"))
										{
											System.out.println("en2");
											JSONStrategyValues entryAlert2=new JSONStrategyValues();
											JSONArray entryDatesArray=entryAlert2.getEntryStrategyAlertDates(mutualFundId,"EntryStrategy2");
											for(int j=0;j<entryDatesArray.length();j++)
											entryStrategyDateJson.put(entryDatesArray.get(j));
										}
										
										if(entryStrategy3!=null && entryStrategy3.equalsIgnoreCase("on"))
										{
											System.out.println("en3");
											JSONStrategyValues entryAlert3=new JSONStrategyValues();
											JSONArray entryDatesArray=entryAlert3.getEntryStrategyAlertDates(mutualFundId,"EntryStrategy3");
											for(int j=0;j<entryDatesArray.length();j++)
											entryStrategyDateJson.put(entryDatesArray.get(j));
										}
										
										if(entryStrategy4!=null && entryStrategy4.equalsIgnoreCase("on"))
										{
											System.out.println("en4");
											JSONStrategyValues entryAlert4=new JSONStrategyValues();
											JSONArray entryDatesArray=entryAlert4.getEntryStrategyAlertDates(mutualFundId,"EntryStrategy4");
											for(int j=0;j<entryDatesArray.length();j++)
											entryStrategyDateJson.put(entryDatesArray.get(j));
										}
										
										if(entryStrategy5!=null && entryStrategy5.equalsIgnoreCase("on"))
										{
											System.out.println("en5");
											JSONStrategyValues entryAlert5=new JSONStrategyValues();
											JSONArray entryDatesArray=entryAlert5.getEntryStrategyAlertDates(mutualFundId,"EntryStrategy5");
											for(int j=0;j<entryDatesArray.length();j++)
											entryStrategyDateJson.put(entryDatesArray.get(j));
										}
										
										if(entryStrategy6!=null && entryStrategy6.equalsIgnoreCase("on"))
										{
											System.out.println("en6");
											JSONStrategyValues entryAlert6=new JSONStrategyValues();
											JSONArray entryDatesArray=entryAlert6.getEntryStrategyAlertDates(mutualFundId,"EntryStrategy6");
											for(int j=0;j<entryDatesArray.length();j++)
											entryStrategyDateJson.put(entryDatesArray.get(j));
										}
										
										if(entryStrategy7!=null && entryStrategy7.equalsIgnoreCase("on"))
										{
											System.out.println("en7");
											JSONStrategyValues entryAlert7=new JSONStrategyValues();
											JSONArray entryDatesArray=entryAlert7.getEntryStrategyAlertDates(mutualFundId,"EntryStrategy7");
											for(int j=0;j<entryDatesArray.length();j++)
											entryStrategyDateJson.put(entryDatesArray.get(j));
										}
									
										if(entryStrategy8!=null && entryStrategy8.equalsIgnoreCase("on"))
										{
											System.out.println("en8");
											JSONStrategyValues entryAlert8=new JSONStrategyValues();
											JSONArray entryDatesArray=entryAlert8.getEntryStrategyAlertDates(mutualFundId,"EntryStrategy8");
											for(int j=0;j<entryDatesArray.length();j++)
											entryStrategyDateJson.put(entryDatesArray.get(j));
										}
										
										if(entryStrategy9!=null && entryStrategy9.equalsIgnoreCase("on"))
										{
											System.out.println("en9");
											JSONStrategyValues entryAlert9=new JSONStrategyValues();
											JSONArray entryDatesArray=entryAlert9.getEntryStrategyAlertDates(mutualFundId,"EntryStrategy9");
											for(int j=0;j<entryDatesArray.length();j++)
											entryStrategyDateJson.put(entryDatesArray.get(j));
										}
									
										if(entryStrategy10!=null && entryStrategy10.equalsIgnoreCase("on"))
										{
											System.out.println("en10");
											JSONStrategyValues entryAlert10=new JSONStrategyValues();
											JSONArray entryDatesArray=entryAlert10.getEntryStrategyAlertDates(mutualFundId,"EntryStrategy10");
											for(int j=0;j<entryDatesArray.length();j++)
											entryStrategyDateJson.put(entryDatesArray.get(j));
										}
										
										if(entryStrategy11!=null && entryStrategy11.equalsIgnoreCase("on"))
										{
											System.out.println("en11");
											JSONStrategyValues entryAlert11=new JSONStrategyValues();
											JSONArray entryDatesArray=entryAlert11.getEntryStrategyAlertDates(mutualFundId,"EntryStrategy11");
											for(int j=0;j<entryDatesArray.length();j++)
											entryStrategyDateJson.put(entryDatesArray.get(j));
										}
										
										if(entryStrategy12!=null && entryStrategy12.equalsIgnoreCase("on"))
										{
											System.out.println("en12");
											JSONStrategyValues entryAlert12=new JSONStrategyValues();
											JSONArray entryDatesArray=entryAlert12.getEntryStrategyAlertDates(mutualFundId,"EntryStrategy12");
											for(int j=0;j<entryDatesArray.length();j++)
											{
												entryStrategyDateJson.put(entryDatesArray.get(j));
											}
										}
										
										if(entryStrategy13!=null && entryStrategy13.equalsIgnoreCase("on"))
										{
											System.out.println("en13");
											JSONStrategyValues entryAlert13=new JSONStrategyValues();
											JSONArray entryDatesArray=entryAlert13.getEntryStrategyAlertDates(mutualFundId,"EntryStrategy13");
											for(int j=0;j<entryDatesArray.length();j++)
											entryStrategyDateJson.put(entryDatesArray.get(j));
										}
										
										if(entryStrategy14!=null && entryStrategy14.equalsIgnoreCase("on"))
										{
											System.out.println("en14");
											JSONStrategyValues entryAlert14=new JSONStrategyValues();
											JSONArray entryDatesArray=entryAlert14.getEntryStrategyAlertDates(mutualFundId,"EntryStrategy14");
											for(int j=0;j<entryDatesArray.length();j++)
											entryStrategyDateJson.put(entryDatesArray.get(j));
										}
										
										if(entryStrategy15!=null && entryStrategy15.equalsIgnoreCase("on"))
										{
											System.out.println("en15");
											JSONStrategyValues entryAlert15=new JSONStrategyValues();
											JSONArray entryDatesArray=entryAlert15.getEntryStrategyAlertDates(mutualFundId,"EntryStrategy15");
											for(int j=0;j<entryDatesArray.length();j++)
											entryStrategyDateJson.put(entryDatesArray.get(j));
										}
										
										if(entryStrategy16!=null && entryStrategy16.equalsIgnoreCase("on"))
										{
											System.out.println("en16");
											JSONStrategyValues entryAlert16=new JSONStrategyValues();
											JSONArray entryDatesArray=entryAlert16.getEntryStrategyAlertDates(mutualFundId,"EntryStrategy16");
											for(int j=0;j<entryDatesArray.length();j++)
											entryStrategyDateJson.put(entryDatesArray.get(j));
										}
			
									}
								}
								catch(Exception e)
								{
									resp.put("success",false);
									resp.put("error","bad");
									out.print(resp);
								}	
		
							%> 
						
						
					
							<div class="col-sm-12 col-xs-12 m-t-md" style="background-color:white">
							
								<div id="container">
								</div>
								</div>
							</div>
							</div>
						</div>
					</div>		
				</div>
			</div>
		</section>
		<script src="assets/plugins/jquery.min.js"></script>
	    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/plugins/jquery-easing/jquery.easing.min.js"></script>
	    <script src="assets/plugins/jquery-sticky/jquery.sticky.js"></script>
	    
		<script src="assets/js/validator.js"></script>
		<script>

$(document).ready(function()
{
	
	var mutualFundName;
	
	var candleType="<%=candleType%>";
	if(candleType==='null')
	{
		//console.log(candleType);
		candleType="Daily";
	}
	//only for talib	
	var seriesArray=[];
	//only for fractalSeries Date
	var seriesFractalArray=[];
	//onlt for talibSeries Date
	var seriesTalibArray=[];
	//exit strategy array
	var seriesExitStrategyArray=[];
	//entry strategy array
	var seriesEntryStrategyArray=[];
	
	<%System.out.println("inside jQuery");%>
		
		//var indicatorJqueryType=$("#indicator :selected").text();
		console.log("first");
		$("#Fractal").change(function()
		{
			if($("#Fractal").is(':checked'))
			{
				console.log("Fractal checked");
				$("#fractalDiv").show();
			}
			else
			{
				console.log("Fractal Unchecked");
				$('#fractalDiv :checkbox').prop("checked", false);
				$("#fractalDiv").hide();
				
			}
		});
		$("#Ichimoku").change(function()
		{		
			if($("#Ichimoku").is(':checked'))
			{
				console.log("Ichimoku checked");
				$("#ichimokuDiv").show();
			}
			else
			{
				console.log("Ichimoku Unchecked");
				$('#ichimokuDiv :checkbox').prop("checked", false);
				$("#ichimokuDiv").hide();
				
			}
		});
		$("#Talib").change(function()
		{			
			if($("#Talib").is(':checked'))
			{
				console.log("Talib checked");
				$("#talibDiv").show();
			}
			else
			{
				console.log("Talib Unchecked");
				$('#talibDiv :checkbox').prop("checked", false);
				$("#talibDiv").hide();
				
			}
		});
		//below if(indicatorJqueryType1==='Fractal') is used when submit is pressed and on next page we keep selected those checkboxes
		if($("#Fractal").is(':checked'))
		{
			console.log("Fractal checked");
			$("#fractalDiv").show();
		}
		else
		{
			console.log("Fractal Unchecked");
			$("#fractalDiv").hide();
			$('#fractalDiv :checkbox').prop("checked", false);
		}
		if($("#Ichimoku").is(':checked'))
		{
			console.log("Ichimoku checked");
			$("#ichimokuDiv").show();
		}
		else
		{
			console.log("Ichimoku Unchecked");
			$("#ichimokuDiv").hide();
			$('#ichimokuDiv :checkbox').prop("checked", false);
		}
		if($("#Talib").is(':checked'))
		{
			console.log("Talib checked");
			$("#talibDiv").show();
		}
		else
		{
			console.log("Talib Unchecked");
			$("#talibDiv").hide();
			$('#talibDiv :checkbox').prop("checked", false);
		}
	
	
	//dateValueNavPair is common for all three types of indicators(i.e fractal,ichimoku,talib) which strores [date,nav] json
	var dateValueNavPair=<%=navJson%>
	
	//fractal indicators
	var dateValueUpFractalPair=<%=upFractalJson%>
	var dateValueLowFractalpair=<%=lowFractalJson%>
	var dateValueSwingHighPair=<%=swingHighJson%>
	var dateValueSwingLowPair=<%=swingLowJson%>
	var dateValueTcfl1Pair=<%=tcfl1Json%>
	var dateValueTcfl2Pair=<%=tcfl2Json%>
	var dateValueTcfl3Pair=<%=tcfl3Json%>
	
	//ichimoku indicators
	var tenkanSen=<%=tenkanSenJson%>
	var kijunSen=<%=kijunSenJson%>
	var senkouSpanA=<%=senkouSpanAJson%>
	var senkouSpanB=<%=senkouSpanBJson%>
	var kumoTop=<%=kumoTopJson%>
	var kumoBottom=<%=kumoBottomJson%>
	
	//talib indicators
	var ema20=<%=ema20Json%>
	var ema50=<%=ema50Json%>
	var ema200=<%=ema200Json%>
	var macd=<%=macdJson%>
	var macdSignal=<%=macdSignalJson%>
	var macdHistogram=<%=macdHistogramJson%>
	var upperBand=<%=upperBandJson%>
	var middleBand=<%=middleBandJson%>
	var lowerBand=<%=lowerBandJson%>
	var rsi=<%=rsiJson%>
	var psar=<%=psarJson%>
	var adx=<%=adxJson%>
	var atr=<%=atrJson%>
	
	
	
	
	
	
	//below variables represent checkboxes
	var fractalCheckbox="<%=request.getParameter("Fractal")%>";
	var ichimokuCheckbox="<%=request.getParameter("Ichimoku")%>";
	var talibCheckbox="<%=request.getParameter("Talib")%>";
	
	var u="<%=request.getParameter("upFractal")%>";
	var l="<%=request.getParameter("lowFractal")%>";
	var sh="<%=request.getParameter("swingHigh")%>";
	var sl="<%=request.getParameter("swingLow")%>";
	var tcfl1="<%=request.getParameter("tcfl1")%>";
	var tcfl2="<%=request.getParameter("tcfl2")%>";
	var tcfl3="<%=request.getParameter("tcfl3")%>";
	
	
	var tS="<%=request.getParameter("tenkanSen")%>"
	var kS="<%=request.getParameter("kijunSen")%>"
	var sSA="<%=request.getParameter("senkouSpanA")%>"
	var sSB="<%=request.getParameter("senkouSpanB")%>"
	var kT="<%=request.getParameter("kumoTop")%>"
	var kB="<%=request.getParameter("kumoBottom")%>"
	
	//below var (e20,macdS) are flags for checkBoxes since we need a separate type of charts for Talib
	var e20="<%=request.getParameter("ema20")%>"
	var e50="<%=request.getParameter("ema50")%>"
	var e200="<%=request.getParameter("ema200")%>"
	var macD="<%=request.getParameter("macd")%>"
	var macdS="<%=request.getParameter("macdSignal")%>"
	var macdH="<%=request.getParameter("macdHistogram")%>"
	var uB="<%=request.getParameter("upperBand")%>"
	var mB="<%=request.getParameter("middleBand")%>"
	var lB="<%=request.getParameter("lowerBand")%>"
	var rsI="<%=request.getParameter("rsi")%>"
	var psaR="<%=request.getParameter("psar")%>"
	var adX="<%=request.getParameter("adx")%>"
	var atR="<%=request.getParameter("atr")%>"
	
	

	
	var fractalTop=0,ichimokuTop=0,talibTop=0,macdTop=0,rsiTop=0,adxTop=0,atrTop=0;
	var overlayFractalHeight=0,overlayIchimokuHeight=0,overlayTalibHeight=0,macdHeight=0,rsiHeight=0,adxHeight=0,atrHeight=0;
	
	//strategy dates json
	var exitStrategyDateJson=<%=exitStrategyDateJson%>
	var entryStrategyDateJson=<%=entryStrategyDateJson%>
	if(exitStrategyDateJson!=null || entryStrategyDateJson!=null)
	{
		console.log("called createStrategyDateJson()");
		//console.log("exitJSON"+JSON.stringify(exitStrategyDateJson));
		createStrategyDateJson();
	}
	//this function takes indicator values and convert it to [[UTC Date,val],[UTC Date,val],[UTC Date,val]] i.e jsonArrays inside jsonArray
	var divHeight=145;
	insertJsonIntoSeriesArray();		
		
	mutualFundName=$("#dropdown option:selected").text();	
	
	if(seriesArray.length!=0)
	{
		Highcharts.stockChart('container', {
			title:{
				text:mutualFundName
			},
			subtitle:{
				text:candleType
			},
			tooltip: {
	            pointFormat:'<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>', 
	            valueDecimals: 3,
	            split:true             
	       },
		    xAxis: {
		        type: 'datetime',
		        dateTimeLabelFormats: { // don't display the dummy year
		            month: '%e. %b',
		            year: '%b'
		        },
		        title: {
		            text: 'Date',
		            align: 'middle'
		        	}
		    },
		    yAxis:[
		           <%
		           if(request.getParameter("Fractal")!=null || request.getParameter("Ichimoku")!=null || request.getParameter("Talib")!=null)
		           {
		           %>
		           		{		           		
			           		top:fractalTop,
			           		height:overlayFractalHeight,		           		
					        //lineWidth: 2,
					        //offset: 0,
					        labels: {
					        	formatter: function(){
					       		return this.value+" Rs.";
					       		}
					        },
					             
					        title: {
					            text: 'Overlay Indicators',
					            align: 'middle'
					        	}				    		           		
		           		},	
		           	<%
		           }
		          /* if(request.getParameter("Ichimoku")!=null)
		           {
		        	 %>
		        	 {		           		
			           		top:ichimokuTop,
			           		height:overlayIchimokuHeight,		           		
					        lineWidth: 2,
					        offset: 0,
					        labels: {
					        	formatter: function(){
					       		return this.value+" Rs.";
					       		}
					        },     
					        title: {
					            text: 'Basic Ichimoku',
					            align: 'middle'
					        	}				    		           		
		           		},
		        	 <%
		           } 
		           if((request.getParameter("Talib")!=null && request.getParameter("Ichimoku")==null && request.getParameter("Fractal")==null) || (request.getParameter("Talib")!=null && (request.getParameter("ema20")!=null || request.getParameter("ema50")!=null || request.getParameter("ema200")!=null || request.getParameter("psar")!=null || request.getParameter("upperBand")!=null || request.getParameter("middleBand")!=null || request.getParameter("lowerBand")!=null)))
		           {
		        	  %>
		        	  {		           		
			           		top:talibTop,
			           		height:overlayTalibHeight,		           		
					        lineWidth: 2,
					        offset: 0,
					        labels: {
					        	formatter: function(){
					       		return this.value+" Rs.";
					       		}
					        },     
					        title: {
					            text: 'Basic Talibs',
					            align: 'middle'
					        	}				    		           		
		           		},		        	  
		        	  <%
		           }*/
		           if(request.getParameter("macd")!=null || request.getParameter("macdSignal")!=null || request.getParameter("macdHistogram")!=null )
		           {
		           	   %>
		           		{
		           			height: macdHeight,
		           			//top specifies y pixel from top of screen to specify chart
		           			top:macdTop,
		           			
					        lineWidth: 2,
					        offset: 0,
					        labels: {
					        	formatter: function(){
					       		return this.value+" Rs.";
					       		}
					        },       
					    	title: {
				            text: 'macd types',
				            align: 'middle'
				        	}
		           		},
		           	 	<%
		           	}
		           	if(request.getParameter("rsi")!=null)
		           	{
		           	%>
		           		
			           	{
		           			height:rsiHeight,
		           			//top specifies y pixel from top of screen to specify chart
		           			top:rsiTop,
		           			
					        lineWidth: 2,
					        offset: 0,
					        labels: {
					        	formatter: function(){
					       		return this.value+" Rs.";
					       		}
					        },       
					    	title: {
				            text: 'rsi types',
				            align: 'middle'
				        	}
		           		},	           			
		           		
		           	<%
		           	}
		           	if(request.getParameter("adx")!=null)
		           	{
		           	%>
			           	{
		           			height: adxHeight,
		           			//top specifies y pixel from top of screen to specify chart
		           			top:adxTop,
		           			
					        lineWidth: 2,
					        offset: 0,
					        labels: {
					        	formatter: function(){
					       		return this.value+" Rs.";
					       		}
					        },       
					    	title: {
				            text: 'adx types',
				            align: 'middle'
				        	}
		           		},	     
		           	
		           	<%
		           	}
		           	if(request.getParameter("atr")!=null)
		           	{
		           	%>
			           	{
		           			height: atrHeight,
		           			//top specifies y pixel from top of screen to specify chart
		           			top:atrTop,
		           			
					        lineWidth: 2,
					        offset: 0,
					        labels: {
					        	formatter: function(){
					       		return this.value+" Rs.";
					       		}
					        },       
					    	title: {
				            text: 'atr types',
				            align: 'middle'
				        	}
		           		},	
		           	<%
		           	}
		           	%>	        		       
		    ],				         
	        series:seriesArray,      		    		
		});
	}	
	function createStrategyDateJson()
	{		
		if(exitStrategyDateJson!=null)
		{
			outerArray=[];
			var innerJson;
			for(var i=0;i<exitStrategyDateJson.length;i++)
			{
				
				//console.log("strategy="+strategyDateJson[i][0]);
				var year=parseInt(moment(exitStrategyDateJson[i][0]).format("YYYY"));
				var month=parseInt(moment(exitStrategyDateJson[i][0]).format("MM")-1);
				var date=parseInt(moment(exitStrategyDateJson[i][0]).format("DD"));
				innerJson={"x":Date.UTC(year,month,date),"title":exitStrategyDateJson[i][1]};
				outerArray.push(innerJson);
			}
			seriesExitStrategyArray=outerArray;				
		}
		
		if(entryStrategyDateJson!=null)
		{
			outerArray=[];
			var innerJson;
			for(var i=0;i<entryStrategyDateJson.length;i++)
			{
				var year=parseInt(moment(entryStrategyDateJson[i][0]).format("YYYY"));
				var month=parseInt(moment(entryStrategyDateJson[i][0]).format("MM")-1);
				var date=parseInt(moment(entryStrategyDateJson[i][0]).format("DD"));
				console.log("Year="+year+"Month="+month+"date="+date);
				innerJson={"x":Date.UTC(year,month,date),"title":entryStrategyDateJson[i][1]};
				outerArray.push(innerJson);
			}
			seriesEntryStrategyArray=outerArray;
		}
		console.log("seriesEntryStrategyArray="+JSON.stringify(seriesEntryStrategyArray));
	}
	
	function getJsonArray(dateValuePair,type,yAxis)
	{	
		outerArray=[];
		innerArrays=[];
		for(var i=0;i<dateValuePair.length;i++)
		{
			var ithJsonArray=dateValuePair[i];
			//console.log("NavDate="+ithJsonArray[0]);
			var year=parseInt(moment(ithJsonArray[0]).format("YYYY"));
			var month=parseInt(moment(ithJsonArray[0]).format("MM")-1);
			var date=parseInt(moment(ithJsonArray[0]).format("DD"));
			
			
			var amount=ithJsonArray[1];
			
			innerArrays=[];
			innerArrays[0]=Date.UTC(year,month,date);
			innerArrays[1]=amount;
			outerArray.push(innerArrays);
			
			//console.log("Amount="+amount+"  y/m/d="+year+""+month+""+date);
		}
		
		if(type=="Nav")
		{
			seriesArray.push(
					{
							type:'line',
							yAxis: yAxis,
					        animation: false,
					        stack: 0,
					        id:'navseries',
							name: type,
							data: outerArray,					
						   
					});		
			
			var exitFlagJson={
							type:'flags',
							name:'exit',
							data:seriesExitStrategyArray,
							onSeries:'navseries',
							shape:'squarepin',
							fillColor: '#FBAB51',
							
						};
			var entryFlagJson={
							type:'flags',
							name:'entry',
							data:seriesEntryStrategyArray,
							onSeries:'navseries',
							shape:'squarepin',
							fillColor: '#00FF3E',
							
						};
				
			seriesArray.push(entryFlagJson);
			seriesArray.push(exitFlagJson);
			console.log(JSON.stringify(seriesArray));
			
			//console.log("entryFlagJson="+JSON.stringify(entryFlagJson));
			//console.log("entryFlagJson.length="+seriesEntryStrategyArray.length);
		}
		else
		{
			seriesArray.push(
					{
							type:'line',
							yAxis: yAxis,
					        animation: false,
					        stack: 0,
							name: type,
							data: outerArray,
						    
					});
		}
		
	}
	
	function insertJsonIntoSeriesArray()
	{		
		var bigCount=75;
		var yAxis=-1;
		//since we show everything about indicator(upFractal,lowFractal,swings,tcfl) and we get all the values in jsp above we assume that all the values gets generated
		if(fractalCheckbox!=null && fractalCheckbox=='on' && dateValueNavPair!=null)
		{
			fractalTop=bigCount+10;
			 
			overlayFractalHeight=250;
			//divHeight=divHeight+fractalTop+overlayFractalHeight;
			
			bigCount=fractalTop;
			yAxis++;
			console.log(fractalTop+"=fractalTop");
			console.log(overlayFractalHeight+"=ofHeight yAxis"+yAxis);
			
			getJsonArray(dateValueNavPair,"Nav",yAxis);
			if(u!=null && u==='on')
				getJsonArray(dateValueUpFractalPair,"UpFractal",yAxis);
			if(l!=null && l==='on')
				getJsonArray(dateValueLowFractalpair,"LowFractal",yAxis);
			if(sh!=null && sh==='on')
				getJsonArray(dateValueSwingHighPair,"SwingHigh",yAxis);
			if(sl!=null && sl==='on')
				getJsonArray(dateValueSwingLowPair,"SwingLow",yAxis);
			if(tcfl1!=null && tcfl1==='on')
				getJsonArray(dateValueTcfl1Pair, "TCFL1",yAxis);
			if(tcfl3!=null && tcfl2==='on')
				getJsonArray(dateValueTcfl2Pair, "TCFL2",yAxis);
			if(tcfl3!=null && tcfl3==='on')
				getJsonArray(dateValueTcfl3Pair, "TCFL3",yAxis);			
		}
		else
		{
			overlayFractalHeight=250;
			fractalTop=bigCount+10;
		}
		if(ichimokuCheckbox!=null && ichimokuCheckbox=='on' && dateValueNavPair!=null)
		{
			//ichimokuTop=bigCount+overlayFractalHeight+10;//where 10 is space between two charts in terms of pixel
			//overlayIchimokuHeight=200;
			console.log("ichimokuCheckbox found");
			ichimokuTop=fractalTop;
			overlayIchimokuHeight=overlayFractalHeight;
			//divHeight=divHeight+ichimokuTop+overlayIchimokuHeight;
			
			bigCount=ichimokuTop;
			if(fractalCheckbox!='on')
			{
				console.log("ichimokuCheckbox found with yAxis");	
				yAxis++;
			}
			
			console.log(ichimokuTop+"=ichimokuTop");
			console.log(overlayIchimokuHeight+"=ichimokuHeight  yAxis="+yAxis);
			if(fractalCheckbox!='on')
			{
				getJsonArray(dateValueNavPair,"Nav",yAxis);
			}
			if(tS!=null && tS==='on')
				getJsonArray(tenkanSen,"tenkanSen",yAxis);
			if(kS!=null && kS==='on')
				getJsonArray(kijunSen,"kijunSen",yAxis);
			if(sSA!=null && sSA==='on')
				getJsonArray(senkouSpanA,"senkouSpanA",yAxis);
			if(sSB!=null && sSB==='on')
				getJsonArray(senkouSpanB,"senkouSpanB",yAxis);
			if(kT!=null && kT==='on')
				getJsonArray(kumoTop,"kumoTop",yAxis);
			if(kB!=null && kB==='on')
				getJsonArray(kumoBottom,"kumoBottom",yAxis);
		}
		else
		{
			overlayIchimokuHeight=overlayFractalHeight;
			ichimokuTop=fractalTop;
			
		}
		var talibs=bigCount;
		if(talibCheckbox!=null && talibCheckbox=='on' && dateValueNavPair!=null)
		{
			
			var flag=0;
			
			
			if(fractalCheckbox!='on' && fractalCheckbox!=null)
			{
				if(ichimokuCheckbox!='on' && ichimokuCheckbox!=null)
				{
					yAxis++;
					talibTop=talibs+10;
					overlayTalibHeight=250;
					talibs=talibTop;
					flag=1;
					getJsonArray(dateValueNavPair,"Nav",yAxis);
				}
				else
				{		
					//talibTop=talibs+overlayIchimokuHeight+10;
					talibTop=talibs;					
					overlayTalibHeight=overlayIchimokuHeight;//overlayIchimokuHeight;
					talibs=talibTop;
					console.log("talibs+overlayIchimokuHeight="+talibs+"overlayTalibHeight="+overlayTalibHeight);
				}				
			}	
			else
			{		
				//talibTop=talibs+overlayFractalHeight+10;
				talibTop=talibs;
				overlayTalibHeight=overlayFractalHeight;
				talibs=talibTop;
			}
			console.log(talibTop+"=talibTop");
			console.log(overlayTalibHeight+"=overlaytalibHeight  yAxis="+yAxis);
			if(e20==='on' || e50==='on' || e200==='on' || uB==='on' || lB==='on' || mB==='on' || psaR==='on')
			{
				
				//when fractal,ichimoku are on 0 yAxis and talib on 1 yAxis
				/* talibTop=talibs+10;//+overlayIchimokuHeight+10;
				overlayTalibHeight=200;
				talibs=talibTop; */
				
				//when all three are on 0 yAxis
				console.log(talibTop+"=talibTop");
				console.log(overlayTalibHeight+"=otalibHeight  yAxis="+yAxis);
				//don't need to yAxis++?already done in above;
			}
			/* else
			{
				if(flag==0)
				{
					
					//this condition occurs when atleast one of (fractalCheckbox or ichimokuCheckbox has occurred)
					if(ichimokuCheckbox=='on')
					{
						console.log("Yes");
						overlayTalibHeight=0;//overlayIchimokuHeight;
						yAxis--;
					}
					else
					{
						if(fractalCheckbox=='on')
						{
							overlayTalibHeight=0;//overlayFractalHeight;
							yAxis--;
						}
					}					
				}
			} */
			
			if(e20!=null && e20==='on')
				getJsonArray(ema20,"ema20",yAxis);
			if(e50!=null && e50==='on')
				getJsonArray(ema50,"ema50",yAxis);
			if(e200!=null && e200==='on')
				getJsonArray(ema200,"ema200",yAxis);
			if(uB!=null && uB==='on')
				getJsonArray(upperBand,"upperBand",yAxis);
			if(mB!=null && mB==='on')
				getJsonArray(middleBand,"middleBand",yAxis);
			if(lB!=null && lB==='on')
				getJsonArray(lowerBand,"lowerband",yAxis);
			if(psaR!=null && psaR==='on')
				getJsonArray(psar,"psar",yAxis);
			
			if(macD==='on' || macdS==='on' || macdH==='on')
			{
				macdTop=talibs+overlayTalibHeight+10;
				macdHeight=50;
				talibs=macdTop;		
				yAxis++;
				console.log("macdTop="+macdTop+" macdHeight="+macdHeight+"  yAxis="+yAxis+"talibs="+talibs+" overlayTalibH="+overlayTalibHeight);
				
			}
			else
			{
				macdHeight=overlayTalibHeight;
			}
			if(macD!=null && macD==='on')
				getJsonArray(macd,"macd",yAxis);
				//getMacdJsonArray(macd,"macd",yAxis);
			if(macdS!=null && macdS==='on')
				getJsonArray(macdSignal,"macdSignal",yAxis);
				//getMacdJsonArray(macdSignal,"macdSignal",yAxis);
			if(macdH!=null && macdH==='on')
				getJsonArray(macdHistogram,"macdHistogram",yAxis);
				//getMacdJsonArray(macdHistogram,"macdHistogram",yAxis);
			if(rsI==='on')
			{
				rsiTop=talibs+macdHeight+10;
				rsiHeight=50;
				talibs=rsiTop;		
				yAxis++;
				console.log("rsiTop="+rsiTop+" rsiHeight="+rsiHeight+" yAxis="+yAxis);
			}
			else
			{
				rsiHeight=macdHeight;
			}
			if(rsI!=null && rsI==='on')
				getJsonArray(rsi,"rsi",yAxis);
			if(adX==='on')
			{
				adxTop=talibs+rsiHeight+10;
				adxHeight=50;
				talibs=adxTop;
				yAxis++;
				console.log("adxTop="+adxTop+" adxHeight="+adxHeight+" yAxis="+yAxis);
			}
			else
			{
				adxHeight=rsiHeight;
			}
			if(adX!=null && adX==='on')
				getJsonArray(adx,"adx",yAxis);
			if(atR==='on')
			{
				atrTop=talibs+adxHeight+10;
				atrHeight=50;
				talibs=atrTop;
				yAxis++;
				//reason to below instruction is we need to make dynamic div so we need total height taken by each charts i.e talibs
				console.log("atrTop="+atrTop+" atrHeight="+atrHeight+" yAxis="+yAxis);
			}
			else
			{
				atrHeight=adxHeight;
			}
			if(atR!=null && atR==='on')
				getJsonArray(atr,"atr",yAxis);				
		}
		else
		{
			talibs=talibs+overlayIchimokuHeight;
		}
		divHeight=divHeight+talibs+atrHeight+10;
		console.log("divHeight"+divHeight);
		$("#container").height(divHeight);
	}

	
});
</script> 
</body>
</html>