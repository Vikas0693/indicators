<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.fundexpert.controller.EveryDayCandleController"%>
<%@ page import="org.json.*" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException" %>
<%@ page import="java.sql.SQLException"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Generate Candles</title>
<script type="text/javascript" src="jquery-3.2.1.js"></script>
</head>
<body>
	<%
		String submit=request.getParameter("submit");
		
		if(submit==null)
		{
	%>
	<form action="generate-candle.jsp" method="post">
		<!-- Current Table:<input type="radio" name="tabletogenerate" value="current"/>
		Last Week End Date:<input type="date" name="weekEndDate"> -->		
		<br>EveryDay Table:<input type="radio" name="tabletogenerate"  value="everyday"/>
		
		<br>
		<select id="dropdown" name="currentOrAllOrDate">
			<!-- <option name="current" value="current" selected="selected">CurrentDate</option> -->
			<!-- <option name="all" value="all">All</option> -->
			<option name="date" value="date">Start Date</option>
		</select>
		<br>
		<input type="submit" id="submit" name="submit" value="Submit"/>
	</form>
	
	<% 
		}
		else
		{
			String tableToGenerate=request.getParameter("tabletogenerate");
			JSONObject jsonObject=new JSONObject();
	
			int r=0;
			System.out.print("Table to generate="+tableToGenerate);
			if(tableToGenerate!=null)
			{				
				try
				{					
					if(tableToGenerate.equalsIgnoreCase("current"))
					{
						String weekEndDate=request.getParameter("weekEndDate");
						//CurrentDayCandleController cdcc=new CurrentDayCandleController();
						//cdcc.updateCandle(weekEndDate);

						/*EveryDayCandleController edcc=new EveryDayCandleController();
						edcc.generateCurrentCandleForEveryDayCandle(); */
					}
					else if(tableToGenerate.equalsIgnoreCase("everyday"))
					{
						System.out.print("Generate EveryDay");
						EveryDayCandleController edcc=new EveryDayCandleController();
						String currentOrAllOrDate=request.getParameter("currentOrAllOrDate");
						if(currentOrAllOrDate.equalsIgnoreCase("current"))
						{								
							//assuming currentDayCadle already generated now update current Values for EveryDayCandle
							//edcc.generateCurrentCandleForEveryDayCandle();
						}
						else if(currentOrAllOrDate.equalsIgnoreCase("all"))
						{
							//edcc.updateCandles();							
						}
						else if(currentOrAllOrDate.equalsIgnoreCase("date"))
						{
							edcc.updateEveryDayCandleAfterStartDate();							
						}
					}
					jsonObject.put("success",true);
					jsonObject.put("error","");
					out.print(jsonObject);					
				}			
				catch(Exception e)
				{
					e.printStackTrace();
					
					jsonObject.put("success",false);
					jsonObject.put("error",e.getMessage());
					out.print(jsonObject);					
				}
				
			}
		}
	%>


<script>
$(document).ready(function(event)
{
	$(":Submit").click(function(e)
	{		
		var currentDate=new Date();
		var day=currentDate.getDay();
		var table=$("input[name=tabletogenerate]:checked").val();
		var date=$("#dropdown option:selected").val();
		console.log(date+"date");
		console.log(day);
		console.log(table);
		if(table==='' || typeof table==="undefined")
		{
			alert("please select any one radio box!!");
			e.preventDefault();	
		}
		/* console.log(date);
		if(date==='date')
		{
			var startDate=$("#sDate").val();
			if(startDate==='' || typeof startDate==="undefined")
			{
				alert("select startDate");
				e.preventDefault();
			}
		} */
		/* if(day===2 && typeof holiday==="undefined" && table==="current")
		{	
			if(!confirm("Are you sure today is not holiday"))
			{
				e.preventDefault();
			}
		}
		else if(day===2 && holiday==='true')
		{	
			if(!confirm("Are you sure today is holiday"))
			{
				e.preventDefault();
			}
		} */
		
		
	});
});


</script>
</body>
</html>