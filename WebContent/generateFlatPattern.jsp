<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.controller.FlatPatternsController" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Generate Flat Pattern</title>
</head>
<body>
<%
	FlatPatternsController fpc=new FlatPatternsController();
	try
	{
		//daily patterns
		fpc.createLatestSSBFlatPatterns(String.valueOf(20), String.valueOf(1));
		fpc.createLatestTCFL1FlatPatterns(String.valueOf(15),String.valueOf(1));
		fpc.createLatestTCFL3FlatPatterns(String.valueOf(15),String.valueOf(1));
		
		//weekly patterns
		fpc.createLatestSSBFlatPatterns(String.valueOf(15),String.valueOf(0));
		fpc.createLatestTCFL1FlatPatterns(String.valueOf(15),String.valueOf(0));
		fpc.createLatestTCFL3FlatPatterns(String.valueOf(15),String.valueOf(0));
		out.println("Finished Processing Flat Patterns");
	}
	catch(Exception e)
	{
		e.printStackTrace(response.getWriter());
	}

%>

</body>
</html>