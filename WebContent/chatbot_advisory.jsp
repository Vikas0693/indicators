<%@page import="com.apidoc.util.HibernateBridge"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>chat bot advisory</title>
</head>
<body>
	<%
		String amfiiCode=request.getParameter("amfiiCode");
		String amountInvested=request.getParameter("amountInvested");
		//String amfiiCode="125305";
		Session hSession=null;
		JSONObject json=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			if(amfiiCode!=null)
			{
				Date entryType=(Date)hSession.createQuery("select max(c.tradeDate) from CustomMFEntryAlert c,MutualFund mf where mf.amfiiCode=? and c.mutualFundId=mf.id").setParameter(0,amfiiCode).setMaxResults(1).uniqueResult();
				Date exitType=(Date)hSession.createQuery("select max(c.tradeDate) from CustomMFExitAlert c,MutualFund mf where mf.amfiiCode=? and c.mutualFundId=mf.id").setParameter(0,amfiiCode).setMaxResults(1).uniqueResult();
				json=new JSONObject();
				if(entryType.compareTo(exitType)>0)				
					json.put("suggestion", "yes");					
				else
					json.put("suggestion","no");			
				out.print(json.toString());
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	%>
</body>
</html>