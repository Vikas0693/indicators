
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.dao.Nav" %>
<%@ page import="com.fundexpert.dao.Holidays" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Missed Nav's</title>
</head>
<body>
<%
	Session hSession=null;
	try
	{
		hSession=HibernateBridge.getSessionFactory().openSession();
		MutualFundController mfc=new MutualFundController();
		List<Object[]> mfList=mfc.getMutualFundList();
		Iterator iterator=mfList.iterator();
		int count=0;
		Date maxEveryDayCandleTableDate=(Date)hSession.createQuery("select max(tradeDate) from EveryDayCandle").setMaxResults(1).uniqueResult();
		Date maxNavDate=(Date)hSession.createQuery("select max(tradeDate) from Nav").setMaxResults(1).uniqueResult();
		
		String maxDate=new SimpleDateFormat("yyyy-MM-dd").format(maxEveryDayCandleTableDate);
		String maxNDate=new SimpleDateFormat("yyyy-MM-dd").format(maxNavDate);
		%>
		<h2>Missing Nav's after : <%=maxDate %></h2>
		<h2>Last updated Nav : <%=maxNDate %></h2>
		there will be no missing nav's if both above dates are equal
		<br>if there is difference between two dates then there might be some missing nav dates or might not<br>
		<%
		while(iterator.hasNext())
		{				
			Object object[]=(Object[])iterator.next();
			Long mutualFundId=(Long)object[0];
			String mutualFundName=(String)object[1];
			
			System.out.println(mutualFundId);
			List listNav=hSession.createQuery("from Nav where tradeDate>=? and mutualfundId=? order by tradeDate").setParameter(0,maxEveryDayCandleTableDate).setParameter(1, mutualFundId).list();
			if(listNav!=null && !listNav.isEmpty())
			{
				Nav previousNav=(Nav)listNav.get(0);
				Nav currentNav=null;
				Date previousDate=previousNav.getTradeDate();
				Date currentDate=null;
				//System.out.println("List Size="+listNav.size());
				for(int i=1;i<listNav.size();i++)
				{
					currentNav=(Nav)listNav.get(i);
					
					currentDate=currentNav.getTradeDate();					
					long diffOfDays=((currentDate.getTime()-previousDate.getTime())/(1000*60*60*24))-1;
					if(diffOfDays==0)
					{
						previousDate=currentDate;
						
						//System.out.println(mutualFundId+"\t\t"+currentDate+"Difference is 0");
						if(currentNav.getNav()>=previousNav.getNav()*1.25 || currentNav.getNav()<=-previousNav.getNav()*1.25)
						{
							//System.out.println("Consecutive change is more than 50% for mfId="+mutualFundId+" on currentDate="+currentDate);
							out.print("<font color='red'>Change more than 25% for MF="+mutualFundName+"-"+mutualFundId+" currentDate="+currentDate+"</font><br>");
						}
						previousNav=currentNav;
						continue;
					}						
					else
					{
						for(int j=0;j<diffOfDays;j++)
						{
							Calendar cal=Calendar.getInstance();
							cal.setTime(previousDate);
							cal.add(Calendar.DAY_OF_MONTH,1);
							previousDate=cal.getTime();
						
							if(!new SimpleDateFormat("EEEEE").format(previousDate).equalsIgnoreCase("Saturday") && !new SimpleDateFormat("EEEEE").format(previousDate).equalsIgnoreCase("Sunday"))
							{
								Holidays holidays=(Holidays)hSession.createQuery("from Holidays where holiday=?").setParameter(0,previousDate).uniqueResult();
								if(holidays==null)
								{
									//System.out.println("MissedDate for mutualFundId="+mutualFundId+" on ="+previousDate);
									out.print("MissedDate for MutualFund="+mutualFundName+"-"+mutualFundId+" on ="+previousDate+"<br>");
								}
								else
								{
									//System.out.println("Holiday on ="+previousDate);
								}
							}
							else
							{
								//System.out.println("SAT/SUN on ="+previousDate);
							}
						
						}
						previousDate=currentDate;
						if(currentNav.getNav()==previousNav.getNav()*1.25 || currentNav.getNav()==-previousNav.getNav()*1.25)
						{
							//System.out.println("Consecutive change is more than 50% for mfId="+mutualFundId+" on currentDate="+currentDate);
							out.print("<font color='red'>Change more than 25% for MF="+mutualFundName+"-"+mutualFundId+" currentDate="+currentDate+"</font><br>");
						}
						previousNav=currentNav;
						//System.out.println(mutualFundId+"\t\t"+currentDate);
					}
				}
			}
		}
	}
	catch(Exception e)
	{
		e.printStackTrace(response.getWriter());
		e.printStackTrace();
	}
	finally
	{
		hSession.close();
	}
		
%>
</body>
</html>