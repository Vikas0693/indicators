<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.File" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="java.io.IOException" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
<h1 align="center">Update Nav of missing funds from Excel</h1>
	<%
	String submit=request.getContentType();
	JSONObject resp;
	if(submit==null)
	{
	%>
	<form name="form" method="post" action="navExcelUpload.jsp" enctype="multipart/form-data" accept-charset="utf-8">
	<input type="file" name="file">
	<input type="submit" name="submit">
	<%
	}
	else if(submit!=null)
	{		
		resp=new JSONObject();
		String path="/opt/excel";		
		boolean isMultiPart = false;
		System.out.print("contentLength="+request.getContentLength());
		isMultiPart = ServletFileUpload.isMultipartContent(request);
		// userId contains the current loggedIn user on fundexpert
		if(!new File(path).exists())
		{
			File f = new File(path);
			f.mkdir();
			System.out.println("Absolute Path of the file : " + f.getAbsolutePath());
		}
		System.out.print("isMultiPart+"+isMultiPart);
		if(isMultiPart == true)
		{	
				DiskFileItemFactory diskFactory = new DiskFileItemFactory();
				ServletFileUpload sfu = new ServletFileUpload(diskFactory);
				List list = sfu.parseRequest(request);
				Iterator iterator = list.iterator();
				File navExcel;
				
				while (iterator.hasNext())
				{
					FileItem fileItem = (FileItem) iterator.next();
					if(!fileItem.isFormField())
					{
						System.out.println(path);
						System.out.println(fileItem.getName()+"  File Name");
						path=path+"/"+fileItem.getName();
						final String fileName = fileItem.getName();
						
						try
						{
							navExcel = new File(path);
							fileItem.write(navExcel);
							System.out.println("path="+path);
							Thread t=new Thread(new Runnable()
							{
								public void run()
								{
									try
									{
										System.out.println("fileName="+fileName);
										MutualFundController mfc=new MutualFundController();
										mfc.updateNav(fileName);
									}
									catch(Exception e)
									{
										e.printStackTrace();
									}
								}
							});
							System.out.print("Thread start");
							t.start();
							%>
							Close the page.
							<%
						}
						catch (Exception e)
						{
							resp.put("success",false);
							resp.put("error","Not able to save file");
						}		
					}
				}			
		}
		else
		{
			resp.put("success",false);
			resp.put("error","No File Selected");
		}
		out.print(resp);
	}	
	%>
</form>
</body>
</html>