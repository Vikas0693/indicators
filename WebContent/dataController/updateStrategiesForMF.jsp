<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.apidoc.util.JSONArrayValues" %>
<%@ page import="org.json.JSONArray"%>
<%@ page import="org.json.JSONObject"%>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.apidoc.util.JSONStrategyValues" %>
<%@ page import="com.fundexpert.dao.MutualFund" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.apidoc.util.HibernateBridge"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


	<%
	Session hSession=null;
	Transaction tx=null;
	try
	{
		
	
	String submit=request.getParameter("submit");
	String delete=request.getParameter("delete");
	MutualFundController mfc=new MutualFundController();
	List<Object[]> mfList=mfc.getMutualFundList();
	List mutualFundIdList=new ArrayList();
	if(submit==null && delete==null)
	{
		%>
		<form action="updateStrategiesForMF.jsp" method="get">
			<select id="dropdown" name="mutualFundId">
			<%
			Iterator<Object[]> itr=mfList.iterator();
			while(itr.hasNext())
			{
				Object obj[]=(Object[])itr.next();
				long id=(Long)obj[0];
				mutualFundIdList.add(id);
				String mfName=(String)obj[1];
			%>
				<option name="mutualFundName" id="mutualFundName" value="<%=id%>" <%=request.getParameter("mutualFundId")!=null?(request.getParameter("mutualFundId").toString().equals(id+"")?"selected='selected'":""):"" %>><%=mfName+" "+id%></option>
			<%
			}	
			%>
			</select>
			Entry:<input name="entryStrategy" type="text"> 
			Exit:<input name="exitStrategy" type="text">
			<input type="submit" name="submit" value="submit">
			<input type="submit" name="delete" value="delete">
		</form>
		
		<table border="1">
			<tr>
			<th>MutualFundForEntryExitStrategy</th>
			</tr>
			<%		
			hSession=HibernateBridge.getSessionFactory().openSession();
			List doneMf=hSession.createQuery("from MutualFund where id in (:listId)").setParameterList("listId", mutualFundIdList).list();
			Iterator itr1=doneMf.iterator();
			while(itr1.hasNext())
			{
				MutualFund mf1=(MutualFund)itr1.next();
				if(mf1.getExitStrategies().size()==0 || mf1.getEntryStrategies().size()==0)
				{
				%>
					<tr>	
					<td><%=mf1.getId() %></td>
					</tr>	
				<%
				}				
			}	
			%>
		</table>
		
		<%
	}
	else if(submit!=null && delete==null)
	{
		
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			String mutualFund=request.getParameter("mutualFundId");
			Long mutualFundId=Long.parseLong(mutualFund);
			
			System.out.print(mutualFundId);
			
			String exitSt=request.getParameter("exitStrategy");
			String[] splittedExitStrategies=exitSt.split(",");
			String entrySt=request.getParameter("entryStrategy");
			String[] splittedEntryStrategies=entrySt.split(",");
			
			
			List exitStrategyList=new ArrayList();
			List entryStrategyList=new ArrayList();
			MutualFund mf=(MutualFund)hSession.createQuery("from MutualFund where id=?").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
			//mf.setId(mutualFundId);
			System.out.println("MutName="+mf.getName());
			exitStrategyList=mf.getExitStrategies();
			if(exitSt.length()!=0)
			{
				for(int i=0;i<splittedExitStrategies.length;i++)
				{
					String s=splittedExitStrategies[i];
					if(s.matches("[0-9]+"))
					{					
						String name="ExitStrategy"+s;
						exitStrategyList.add(name);
						System.out.print(name);
					}
				}
			}	
			
			mf.setExitStrategies(exitStrategyList);
			exitStrategyList=null;
			entryStrategyList=mf.getEntryStrategies();
			System.out.println("EntrySt length="+entrySt.length());
			if(entrySt.length()!=0)
			{
				for(int i=0;i<splittedEntryStrategies.length;i++)
				{
					String s=splittedEntryStrategies[i];
					if(s.matches("[0-9]+"))
					{
						String name="EntryStrategy"+s;
						entryStrategyList.add(name);
						//System.out.print(name);
					}
				}
			}
			mf.setEntryStrategies(entryStrategyList);
			entryStrategyList=null;
			hSession.update(mf);	
			tx.commit();
			%>
			<form action="updateStrategiesForMF.jsp" method="get">
				<select id="dropdown" name="mutualFundId">
				<%
				Iterator<Object[]> itr=mfList.iterator();
				while(itr.hasNext())
				{
					Object obj[]=(Object[])itr.next();
					long id=(Long)obj[0];
					mutualFundIdList.add(id);
					String mfName=(String)obj[1];
				%>
					<option name="mutualFundName" id="mutualFundName" value="<%=id%>" <%=request.getParameter("mutualFundId")!=null?(request.getParameter("mutualFundId").toString().equals(id+"")?"selected='selected'":""):"" %>><%=mfName+" "+id%></option>
				<%
				}	
				%>
				</select>
				Entry:<input name="entryStrategy" type="text"> 
				Exit:<input name="exitStrategy" type="text">
				
				<input type="submit" name="submit" value="submit">
				<input type="submit" name="delete" value="delete">
			</form>
			<!-- <table border="1" width="500">
			<tr>
			<td> -->
				<table border="1" width="250">
				<tr>
				<th>MutualFund</th>
				<th>EntryList</th>
				<th>ExitList</th>
				</tr> 
				
				<%		
					List doneMf=hSession.createQuery("from MutualFund where id in (:listId)").setParameterList("listId", mutualFundIdList).list();
					Iterator itr1=doneMf.iterator();
					while(itr1.hasNext())
					{
					MutualFund mf1=(MutualFund)itr1.next();
					List entryList=mf1.getEntryStrategies();
					List exitList=mf1.getExitStrategies();
					if(mf1.getExitStrategies().size()!=0 || mf1.getEntryStrategies().size()!=0)
					{
						String entrySignals="",exitSignals="";
						if(entryList!=null)
						{
							Iterator entryIterator=entryList.iterator();
							while(entryIterator.hasNext())
							{
								String entryStrategy=(String)entryIterator.next();
								entrySignals=entrySignals+entryStrategy+",";
								System.out.println("EntryStrategy"+entryStrategy);
							}
						}
						if(exitList!=null)
						{
							Iterator exitIterator=exitList.iterator();
							while(exitIterator.hasNext())
							{
								String exitStrategy=(String)exitIterator.next();
								exitSignals=exitSignals+exitStrategy+",";
								System.out.println("exitStraetgies="+exitStrategy);
							}
						}
						%>
						<tr>	
						<td><%=mf1.getId() %></td>
						<td><%=entrySignals %></td>
						<td><%=exitSignals %></td>
						</tr>
					<%
					}				
				}	
				%>					
			</table>			
			<!-- </td>
			<td>
				<table border="1" width="250">
				<tr>
				<th>MutualFundEmptyStrategy</th>
				</tr>
				<%		
					/* //List doneMf=hSession.createQuery("from MutualFund where id in (:listId)").setParameterList("listId", mutualFundIdList).list();
					Iterator itr2=doneMf.iterator();
					while(itr2.hasNext())
					{
						MutualFund mf1=(MutualFund)itr2.next();
						
						if(mf1.getExitStrategies().size()==0 || mf1.getEntryStrategies().size()==0)
						{	*/
						%>
							<%-- <tr>	
							<td><%=mf1.getId() %></td>
							</tr> --%>	
						<%
						//}				
					//}	 
				%>				
				<!-- </table>
			</td>
			</tr>
			</table> -->
			<%
		}	
		else if(delete!=null && submit==null)
		{
			
			//below code deletes the strategy for particular mutualFund
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			String mutualFund=request.getParameter("mutualFundId");
			Long mutualFundId=Long.parseLong(mutualFund);
			
			System.out.print("deleting"+mutualFundId);
			
			String exitSt=request.getParameter("exitStrategy");
			String[] splittedExitStrategies=exitSt.split(",");
			String entrySt=request.getParameter("entryStrategy");
			String[] splittedEntryStrategies=entrySt.split(",");
			
			MutualFund mf1=(MutualFund)hSession.createQuery("from MutualFund where id=?").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();

			List entryList=mf1.getEntryStrategies();
			List exitList=mf1.getExitStrategies();
				
			List entryRemoveList=new ArrayList();
			List exitRemoveList=new ArrayList();
				
			if(entryList.size()!=0)
			{
				for(int i=0;i<splittedEntryStrategies.length;i++)
				{
					if(splittedEntryStrategies[i].matches("[0-9]+"))
					{
						System.out.println("entryRemove="+"EntryStrategy"+splittedEntryStrategies[i]);
						entryRemoveList.add("EntryStrategy"+splittedEntryStrategies[i]);
					}
				}
			}
			if(exitList.size()!=0)
			{
				for(int i=0;i<splittedExitStrategies.length;i++)
				{
					if(splittedExitStrategies[i].matches("[0-9]+"))
					{
						System.out.println("exitRemove="+"ExitStrategy"+splittedExitStrategies[i]);
						exitRemoveList.add("ExitStrategy"+splittedExitStrategies[i]);
					}
				}
			}
			entryList.removeAll(entryRemoveList);
			exitList.removeAll(exitRemoveList);
			mf1.setEntryStrategies(entryList);
			mf1.setExitStrategies(exitList);
			Iterator i=entryList.iterator();
			while(i.hasNext())
			{
				String n=(String)i.next();
				System.out.println("AfterRemoved="+n);
			}
			
			hSession.saveOrUpdate(mf1);
			tx.commit();
			%>			
			<form action="updateStrategiesForMF.jsp" method="get">
				<select id="dropdown" name="mutualFundId">
				<%
				Iterator<Object[]> itr=mfList.iterator();
				while(itr.hasNext())
				{
					Object obj[]=(Object[])itr.next();
					long id=(Long)obj[0];
					mutualFundIdList.add(id);
					String mfName=(String)obj[1];
				%>
					<option name="mutualFundName" id="mutualFundName" value="<%=id%>" <%=request.getParameter("mutualFundId")!=null?(request.getParameter("mutualFundId").toString().equals(id+"")?"selected='selected'":""):"" %>><%=mfName+" "+id%></option>
				<%
				}	
				%>
				</select>
				Entry:<input name="entryStrategy" type="text"> 
				Exit:<input name="exitStrategy" type="text">
				
				<input type="submit" name="submit" value="submit">
				<input type="submit" name="delete" value="delete">
			</form>
			
				<table border="1" width="250">
				<tr>
				<th>MutualFund</th>
				<th>EntryList</th>
				<th>ExitList</th>
				</tr> 
				
				<%		
				List doneMf=hSession.createQuery("from MutualFund where id in (:listId)").setParameterList("listId", mutualFundIdList).list();
				Iterator itr1=doneMf.iterator();
				while(itr1.hasNext())
				{
					MutualFund mf=(MutualFund)itr1.next();
					List entryList1=mf.getEntryStrategies();
					List exitList1=mf.getExitStrategies();
					if(mf.getExitStrategies().size()!=0 || mf.getEntryStrategies().size()!=0)
					{					
						String entrySignals="",exitSignals="";
						if(entryList1!=null)
						{
							Iterator entryIterator=entryList1.iterator();
							while(entryIterator.hasNext())
							{
								String entryStrategy=(String)entryIterator.next();
								entrySignals=entrySignals+entryStrategy+",";
								System.out.println("EntryStrategy"+entryStrategy);
							}
						}
						if(exitList1!=null)
						{
							Iterator exitIterator=exitList1.iterator();
							while(exitIterator.hasNext())
							{
								String exitStrategy=(String)exitIterator.next();
								exitSignals=exitSignals+exitStrategy+",";
								System.out.println("exitStraetgies="+exitStrategy);
							}
						}
						%>
						<tr>	
						<td><%=mf.getId() %></td>
						<td><%=entrySignals %></td>
						<td><%=exitSignals %></td>
						</tr>
					<%
					}				
				}	
				%>					
			</table>
			<%				
			}		
	}
	catch(Exception e)
	{
		e.printStackTrace();
		if(tx!=null)
			tx.rollback();
	}
	finally
	{
		hSession.close();
	}		
	%>
</body>
</html>