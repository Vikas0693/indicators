<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page import="java.util.List" %>
<%@ page import="java.lang.StringBuilder" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.controller.NavController" %>
<%@ page import="com.fundexpert.dao.Nav" %>
<%@ page import="com.fundexpert.dao.Holidays" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.InputStreamReader" %>
<%@ page import="java.net.URL" %>
<%@ page import="java.net.URLConnection" %>
<%@ page import="org.json.JSONObject"%>
<%@ page import="org.json.JSONArray"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Missed Nav's</title>
</head>
<body>

<%
	Session hSession=null;
	NavController navController=new NavController();
	try
	{
		hSession=HibernateBridge.getSessionFactory().openSession();
		MutualFundController mfc=new MutualFundController();
		List<Object[]> mfList=mfc.getMutualFundList();
		Iterator iterator=mfList.iterator();
		int count=0;
		Date maxEveryDayCandleTableDate=(Date)hSession.createQuery("select max(tradeDate) from EveryDayCandle").setMaxResults(1).uniqueResult();
		String maxDate=new SimpleDateFormat("yyyy-MM-dd").format(maxEveryDayCandleTableDate);
		%>
		<h2>Update missed Nav data after <%=maxDate %></h2>
		<%
		while(iterator.hasNext())
		{			
			Date firstMissedDate=null;
			boolean missedDate=false;
			Object object[]=(Object[])iterator.next();
			Long mutualFundId=(Long)object[0];
			//Long mutualFundId=2l;
			//System.out.println(mutualFundId);			
			List listNav=hSession.createQuery("from Nav where tradeDate>=? and mutualfundId=? order by tradeDate").setParameter(0,maxEveryDayCandleTableDate).setParameter(1, mutualFundId).list();
			if(listNav!=null && !listNav.isEmpty())
			{
				Nav previousNav=(Nav)listNav.get(0);
				Nav currentNav=null;
				Date previousDate=previousNav.getTradeDate();
				Date currentDate=null;
				//System.out.println("List Size="+listNav.size());
				
				for(int i=1;i<listNav.size();i++)
				{
					currentNav=(Nav)listNav.get(i);
					
					currentDate=currentNav.getTradeDate();					
					long diffOfDays=((currentDate.getTime()-previousDate.getTime())/(1000*60*60*24))-1;
					if(diffOfDays==0)
					{
						//System.out.println("No difference="+previousDate);
						previousDate=currentDate;
						
						continue;
					}						
					else
					{						
						for(int j=0;j<diffOfDays;j++)
						{
							//System.out.println("More than 1 difference in dates="+previousDate);
							Calendar cal=Calendar.getInstance();
							cal.setTime(previousDate);
							cal.add(Calendar.DAY_OF_MONTH,1);
							previousDate=cal.getTime();
						
							if(!new SimpleDateFormat("EEEEE").format(previousDate).equalsIgnoreCase("Saturday") && !new SimpleDateFormat("EEEEE").format(previousDate).equalsIgnoreCase("Sunday"))
							{
								Holidays holidays=(Holidays)hSession.createQuery("from Holidays where holiday=?").setParameter(0,previousDate).uniqueResult();
								if(holidays==null)
								{
									//System.out.println("MissedDate for mutualFundId="+mutualFundId+" on ="+previousDate);
									missedDate=true;
									firstMissedDate=previousDate;
									previousDate=currentDate;
									break;
								}
								else
								{
									//System.out.println("Holiday on ="+previousDate);
								}
							}
							else
							{
								//System.out.println("SAT/SUN on ="+previousDate);
							}
						
						}
						previousDate=currentDate;
					}
					if(missedDate==true)
					{
						System.out.println("missedDate for "+mutualFundId);
						break;
					}
				}
			}
			if(missedDate==true)
			{
				//System.out.println("firstMissedDates for mfId="+mutualFundId+"="+firstMissedDate);
				String amfiiCode=(String)hSession.createQuery("select amfiiCode from MutualFund where id=?").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				String startDate=new SimpleDateFormat("yyyy-MM-dd").format(firstMissedDate);
				String eDate=new SimpleDateFormat("yyyy-MM-dd").format(new Date());
				//String path="http://localhost:8080/fundexpert/app/nav?action=get&amfiiCode="+amfiiCode+"&startDate="+startDate+"&endDate="+eDate;
				String path="https://www.fundexpert.in/app/nav?action=get&amfiiCode="+amfiiCode+"&startDate="+startDate+"&endDate="+eDate;
				URL url = new URL(path);
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setDoInput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(50000);
				con.setReadTimeout(50000);
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
			
				StringBuilder builder=new StringBuilder();
				while(line!=null)
				{
					builder.append(line);
					line=br.readLine();
				}
				//System.out.println("Received data="+builder.toString());	
				JSONArray j=new JSONArray(builder.toString());			
				navController.updateNav(j,amfiiCode);
				out.println("Succesfully Stored data for "+amfiiCode+"<br>");
				br.close();
			}
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
		e.printStackTrace(response.getWriter());
	}
	finally
	{
		hSession.close();
	}


%>
</body>
</html>