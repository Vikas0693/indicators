<%@page import="org.json.JSONArray"%>
<%@page import="org.codehaus.jackson.JsonParser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.InputStreamReader" %>
<%@ page import="java.io.OutputStreamWriter" %>
<%@ page import="java.net.URL" %>
<%@ page import="java.net.URLConnection" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.fundexpert.controller.NavController" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Updating Nav</title>
</head>
<body>
<%

	StringBuilder builder=null;
	//String input = "[{\"amfiiCode\":\"125305\",\"sDate\":\"2017-09-09\",\"eDate\":\"2017-09-30\"}]";
				
	JSONObject errorObject=null;
	NavController navController=new NavController();

	//System.out.println("JSON Object: ");
	JSONArray jsonArray=navController.getMissedNavJson();
	//JSONArray jsonArray= new JSONArray(input);
	System.out.println(jsonArray.toString(1));
	try
	{
		for(int i=0;i<jsonArray.length();i++)
		{ 		
			JSONObject jsonO=(JSONObject)jsonArray.get(i);
			String amfiiCode=jsonO.getString("amfiiCode");
			//System.out.println(amfiiCode+"amfiiCode");
			String sDate=jsonO.getString("sDate");
			String eDate=jsonO.getString("eDate");
			//String path="http://localhost:8080/fundexpert/app/nav?action=get&amfiiCode="+amfiiCode+"&startDate="+sDate+"&endDate="+eDate;
			String path="https://www.fundexpert.in/app/nav?action=get&amfiiCode="+amfiiCode+"&startDate="+sDate+"&endDate="+eDate;
			URL url = new URL(path);
			URLConnection con=url.openConnection();
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestProperty("Content-Type", "application/json");
			con.setConnectTimeout(50000);
			con.setReadTimeout(50000);
			System.out.println("After opening connection to URL");
			
			//OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
			//writer.write(jsonArray.toString());
			//writer.close(); 
			
		
			BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
			String line=br.readLine();
		
			builder=new StringBuilder();
			while(line!=null)
			{
				builder.append(line);
				line=br.readLine();
			}
			//System.out.println("Received data="+builder.toString());		
			JSONArray j=new JSONArray(builder.toString());			
			navController.updateNav(j,amfiiCode);
			out.println("Succesfully Stored data for "+amfiiCode);
			br.close();
		} 
	}
	catch (Exception e)
	{
		out.print(e.getMessage());
	}
%>
</body>
</html>