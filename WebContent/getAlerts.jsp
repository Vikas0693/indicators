<%@page import="com.apidoc.util.HibernateBridge"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="java.io.*" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Alert Api called</title>
</head>
<body>
	<%
	//changed mapping from amfiiCode to isin
	
	//receiving format:{"maxDate":"2017-01-01"}							//max date in alert table of fundexpert
	//sending format: {"success":"true/false","alerts":[{"isin":"","alertType":"","alertDate":"","sType":"strategyName"},{..},{..}]};
	
	Session hSession=null;
	JSONArray outerArray=new JSONArray();
	
	JSONObject innerJsonObject=null;
	JSONArray outerJsonArray=new JSONArray();
	PrintWriter pw=response.getWriter();
	try
	{
		hSession=HibernateBridge.getSessionFactory().openSession();
		InputStream is=request.getInputStream();
		BufferedReader br=new BufferedReader(new InputStreamReader(is));
		String line=br.readLine();
		
		StringBuilder builder=new StringBuilder();
		while(line!=null)
		{
			//System.out.println(line);
			builder.append(line);
			line=br.readLine();
		}
		JSONObject outerJsonObject=new JSONObject();

		JSONObject receivedJsonObject=new JSONObject(builder.toString());
		String maxDate=receivedJsonObject.getString("maxDate");
		if(maxDate!=null)
		{
			System.out.println("Max date is not null in Indicators");
			Date maxAlertDateGeneratedOnFE=new SimpleDateFormat("yyyy-MM-dd").parse(maxDate);
			List<Object[]> entryAlertList=hSession.createQuery("select distinct mf.amfiiCode,c.tradeDate,c.strategyName from CustomMFEntryAlert c,MutualFund mf where mf.id=c.mutualFundId and c.tradeDate>? group by mf.amfiiCode,c.tradeDate,c.strategyName").setParameter(0,maxAlertDateGeneratedOnFE).list();
			System.out.println("size="+entryAlertList.size());
			
			Iterator itr=entryAlertList.iterator();
			while(itr.hasNext())
			{
				Object[] obj=(Object[])itr.next();
				Date date=(Date)obj[1];
				String strategy=(String)obj[2];
				String amfiiCode=(String)obj[0]; 	
				
				innerJsonObject=new JSONObject();
				innerJsonObject.put("amfiiCode",amfiiCode);
				innerJsonObject.put("alertDate",new SimpleDateFormat("yyyy-MM-dd").format(date));
				innerJsonObject.put("alertType","1");
				innerJsonObject.put("sType",strategy);
				outerJsonArray.put(innerJsonObject);
			}
			List<Object[]> exitAlertList=hSession.createQuery("select distinct mf.amfiiCode,c.tradeDate,c.strategyName from CustomMFExitAlert c,MutualFund mf where mf.id=c.mutualFundId and c.tradeDate>? group by mf.amfiiCode,c.tradeDate,c.strategyName").setParameter(0,maxAlertDateGeneratedOnFE).list();
			itr=exitAlertList.iterator();
			while(itr.hasNext())
			{
				Object[] obj=(Object[])itr.next();
				Date date=(Date)obj[1];
				String strategy=(String)obj[2];
				String amfiiCode=(String)obj[0]; 				
				innerJsonObject=new JSONObject();
				innerJsonObject.put("amfiiCode",amfiiCode);
				innerJsonObject.put("alertDate",new SimpleDateFormat("yyyy-MM-dd").format(date));
				innerJsonObject.put("alertType","2");
				innerJsonObject.put("sType",strategy);
				outerJsonArray.put(innerJsonObject);
			} 
			outerJsonObject.put("success","true");
			outerJsonObject.put("alerts",outerJsonArray);
		}
		else
		{
			outerJsonObject.put("success","false");
			outerJsonObject.put("error","maxDate field is empty");
		}
		
		//System.out.println("getAlerts"+outerJsonObject.toString(1));
		response.setStatus(200); 
		pw.write(outerJsonObject.toString());
		pw.flush();		
	}
	catch(Exception e)
	{		
		e.printStackTrace();
		innerJsonObject=new JSONObject();
		innerJsonObject.put("success","false");
		innerJsonObject.put("error","401-exception occurred");
		response.setStatus(401);
		pw.write(innerJsonObject.toString());
		pw.flush();		
	}
	finally
	{
		hSession.close();
	}
	%>
</body>
</html>