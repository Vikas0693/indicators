<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.fundexpert.controller.CustomMFExitAlertController" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update All Exit Alerts</title>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
</head>
<body>
<%
	try
	{
		CustomMFExitAlertController customMFExitAlert=new CustomMFExitAlertController();
		customMFExitAlert.updateAllExitAlertsAfterDate();
	}
	catch(Exception e)
	{
		e.printStackTrace(response.getWriter());
		e.printStackTrace();
	}
%>

<!-- <ul id="list">
    <li><a href="http://localhost:8080/Indicators/strategyAfterDate/exitStrategy1.jsp" >Exit 1</a></li> 
    <li><a href="http://localhost:8080/Indicators/strategyAfterDate/exitStrategy2.jsp" >Exit 2</a></li>
    <li><a href="http://localhost:8080/Indicators/strategyAfterDate/exitStrategy3.jsp" >Exit 3</a></li>
    <li><a href="http://localhost:8080/Indicators/strategyAfterDate/exitStrategy4.jsp" >Exit 4</a></li>
    <li><a href="http://localhost:8080/Indicators/strategyAfterDate/exitStrategy5.jsp" >Exit 5</a></li>
    <li><a href="http://localhost:8080/Indicators/strategyAfterDate/exitStrategy6.jsp" >Exit 6</a></li>
    <li><a href="http://localhost:8080/Indicators/strategyAfterDate/exitStrategy7.jsp" >Exit 7</a></li> -->
    
    <!-- <li><a href="http://test.fundexpert.in/Indicators/strategyAfterDate/exitStrategy1.jsp" >Exit 1</a></li> 
    <li><a href="http://test.fundexpert.in/Indicators/strategyAfterDate/exitStrategy2.jsp" >Exit 2</a></li>
    <li><a href="http://test.fundexpert.in/Indicators/strategyAfterDate/exitStrategy3.jsp" >Exit 3</a></li>
    <li><a href="http://test.fundexpert.in/Indicators/strategyAfterDate/exitStrategy4.jsp" >Exit 4</a></li>
    <li><a href="http://test.fundexpert.in/Indicators/strategyAfterDate/exitStrategy5.jsp" >Exit 5</a></li>
    <li><a href="http://test.fundexpert.in/Indicators/strategyAfterDate/exitStrategy6.jsp" >Exit 6</a></li>
    <li><a href="http://test.fundexpert.in/Indicators/strategyAfterDate/exitStrategy7.jsp" >Exit 7</a></li> 
</ul>
<button value="open" id="openallbutton" name="all">Open</button>

<script>
$(document).ready(function()
{
	
		$('#openallbutton').click(function()
		{
			console.log("sff");
			$('a').each(function()
			{
				window.open($(this).attr('href'));
			});
				
		});
	
});

</script> -->

</body>
</html>