<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="org.json.JSONArray" %>
<%@ page import="com.robo.controller.Algo" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="com.fundexpert.dao.CustomMFExitAlert" %>
<%@ page import="com.fundexpert.controller.CustomMFEntryAlertController" %>
<%@ page import="com.fundexpert.controller.CustomMFExitAlertController" %>
<%@ page import="com.apidoc.util.JSONStrategyValues" %>
<%@ page import="com.robo.config.Config" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Robo</title>
<!-- <script src="https://code.jquery.com/jquery-3.2.1.js"></script> -->
<!-- <script src="https://code.highcharts.com/stock/highstock.js"></script>
<script src="https://code.highcharts.com/stock/modules/exporting.js"></script> -->
<script src="chart/highstock.js"></script>
<script src="chart/exporting.js"></script>
<script src="scripts/moment.min.js"></script>
<!-- Bootstrap Core CSS -->
<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<!-- Plugins CSS -->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="assets/plugins/animate-css/animate.min.css" rel="stylesheet" type="text/css">

<!-- Component CSS -->
<link href="assets/css/component/component.css" rel="stylesheet" type="text/css">
<link href="assets/css/component/colors/red.css" rel="stylesheet" type="text/css">

<!-- Main CSS -->
<!-- <link href="assets/css/style.css" rel="stylesheet" type="text/css"> -->
<link href="assets/css/app-style.css" rel="stylesheet" type="text/css">
<link href="assets/css/rinjani.css" rel="stylesheet" type="text/css">
<link href="assets/css/colors/red.css" rel="stylesheet" type="text/css">
<link href="assets/css/style.css" rel="stylesheet" type="text/css">
</head>
<body class="">
	<nav id="navigation" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	        <div class="container">
	            <div class="navbar-header page-scroll">
	                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-rj-collapse">
	                    <span class="sr-only">Toggle navigation</span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                </button>                
	                <a class="navbar-brand" href="https://www.fundexpert.in/app/#/portfolio">FundExpert</a>
	            </div>
	            <!-- //.navbar-header -->
	            <div class="navbar-collapse collapse navbar-rj-collapse">
		            <ul class="nav navbar-nav navbar-right" style="font-size:13px">
		        		
		        		
		        		<li class="dropdown">
			           		<a id="dLabel" role="button" data-target="#" href="#">
			                	080-33509639
				            </a>
		        		</li>
		        		
		        		<%-- <li class="page-scroll">
	                        <%
			        			boolean login = false;
				        		if(session.getAttribute("id")==null){
				        			login = false;
				        			%>
				        				<a href="#login" class="show-side-panel-link" id="loginClick" nextpage="<%=myLink %>" login="<%=userLoggedIn %>">Login / Register</a>
				        			<%
								}
				        		else{
									User user=(User)session.getAttribute("user");
									login = true;
									%>
			        					<a href="#login" nextpage="../app" class="show-side-panel-link" id="loginClick" login="<%=userLoggedIn %>">My Portfolio</a>
			        				<%
								}
			        		%>
	                    </li> --%>
		        	</ul>
		        </div>
	            <!-- //.navbar-collapse -->
    	    </div>
        	<!-- //.container -->
    	</nav>
    	<!-- //End Navbar -->
    	<section id="result" class="section">
        <div class="section-inner">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title text-center">

                            <h3 class="sub-title m-t-sm">Robo Alerts</h3>

                            <span class="section-line"></span>
                        </div>
                        
                        <!-- //.section-title -->
                        <div class="section-inner">
                        	<div class="container">
							<%
								Date d = new Date();
								String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(d);
								JSONArray navDatePair=null;
								JSONArray roboNavDatePair=null;
								
								JSONArray exitStrategyDateJson=null,entryStrategyDateJson=null,conditionDateJsonArray=null,equityAmountJsonArray=null,liquidAmountJsonArray=null;
								String submit=request.getParameter("submit");
								MutualFundController mfc=new MutualFundController();
								System.out.println("Submit="+submit);
								if(submit==null)
								{
							%>
							<div class="col-sm-12 col-xs-12 shadow-container">
								<form action="robo.jsp" method="get">
									<div class="col-sm-6">
										<div class="form-group m-t-sm">
											<label class="control-label">Total Invested Amount</label>
											<input type="text" name="investedAmount" id="ia" value="100000" class="form-control"/>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group m-t-sm">
											<label class="control-label">Select Fund</label>
											<select id="mutualFunds" name="mutualFunds" class="form-control">
					
				
											<%
											
												List<Object[]> list=mfc.getMutualFundList();
												Iterator itr=list.iterator();
												
												while(itr.hasNext())
												{
													Object[] object=(Object[])itr.next();
													Long id=(Long)object[0];
													String name=(String)object[1];
													/* if(id==43 || id==4452)
													{ */
											%>
												<option name="mutualFundName" value="<%=id%>"><%=name%></option>
											<%	
												//}
											}
											%>
											</select>
										</div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div class="form-group m-t-sm">
											<label class="control-label">Start Date</label>
											<input type="date" name="startDate" value="2011-03-10" class="form-control"/>
										</div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div class="form-group m-t-sm">
											<label class="control-label">End Date</label>
											<input type="date" name="endDate" value="<%=currentDate %>" class="form-control"/>
										</div>
									</div>
									<div class="col-sm-offset-4 col-sm-4 col-xs-12 text-center">
										<div class="form-group m-t-sm">
											<input class="btn btn-rj btn-block" type="submit" name="submit" value="submit"/>
										</div>
									</div>
			
								</form>
							</div>
							<%
								}
								else
								{
									try
									{
										String mutualFund=request.getParameter("mutualFunds");
										System.out.println(mutualFund);
										long mutualFundId=Long.parseLong(mutualFund);
										String startDate=request.getParameter("startDate");
										String endDate=request.getParameter("endDate");
										String invest=request.getParameter("investedAmount");
										System.out.println(invest);
										long investedAmount=Long.parseLong(invest);
			
										CustomMFEntryAlertController entryController=new CustomMFEntryAlertController();
										List<CustomMFEntryAlert> entryList=entryController.getEntryAlerts(mutualFundId,startDate,endDate);
										CustomMFExitAlertController exitController=new CustomMFExitAlertController();
										List<CustomMFExitAlert> exitList=exitController.getExitAlerts(mutualFundId,startDate,endDate);
									
										Config config1=new Config();
										String exit=config1.getProperty("exit");
										String exitStrategyArray[]=exit.split(",");
										String entry=config1.getProperty("entry");
										String entryStrategyArray[]=entry.split(",");
										for(int i=0;i<exitStrategyArray.length;i++)
										{
											exitStrategyArray[i]="ExitStrategy"+exitStrategyArray[i];
										}
										for(int i=0;i<entryStrategyArray.length;i++)
										{
											entryStrategyArray[i]="EntryStrategy"+entryStrategyArray[i];
										}	
			
										exitStrategyDateJson=JSONStrategyValues.getExitStrategyAlertDates(String.valueOf(mutualFundId),exitStrategyArray, startDate, endDate);
										//System.out.println("Exit Strategy="+exitStrategyDateJson.toString(1));
										entryStrategyDateJson=JSONStrategyValues.getEntryStrategyAlertDates(String.valueOf(mutualFundId),entryStrategyArray, startDate, endDate);
									%>
									<div class="col-sm-12 col-xs-12 shadow-container">
										<form action="robo.jsp" method="get">
											<div class="col-sm-6">
												<div class="form-group m-t-sm">
													<label class="control-label">Total Invested Amount</label>
													<input type="text" name="investedAmount" id="ia" value="100000" class="form-control"/>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group m-t-sm">
													<label class="control-label">Select Fund</label>
													<select id="mutualFunds" name="mutualFunds" class="form-control">
													<%
														List<Object[]> list=mfc.getMutualFundList();
														Iterator itr=list.iterator();
														
														while(itr.hasNext())
														{
															Object[] object=(Object[])itr.next();
															Long id=(Long)object[0];
															String name=(String)object[1];
															/* if(id==43 || id==4452)
															{ */
													%>
													<option name="mutualFundName" value="<%=id%>" <%=(request.getParameter("mutualFunds")!=null?(request.getParameter("mutualFunds").toString().equalsIgnoreCase(id+"")?"selected='selected'":""):"")%>><%=name %></option>
													<%	
														//}
														}
													%>
													</select>
												</div>
											</div>
											<div class="col-sm-6 col-xs-12">
												<div class="form-group">
													<label class="control-label">Start Date</label>
													<input type="date" name="startDate" value="2011-03-10" class="form-control"/>
												</div>
											</div>
											<div class="col-sm-6 col-xs-12">
												<div class="form-group">
													<label class="control-label">End Date</label>
													<input type="date" name="endDate" value="<%=currentDate %>" class="form-control"/>
												</div>
											</div>
											<div class="col-sm-offset-4 col-sm-4 col-xs-12 text-center">
												<div class="form-group m-t-sm">
													<input class="btn btn-rj btn-block" type="submit" name="submit" value="submit"/>
												</div>
											</div>
										</form>
									</div>
			
									<%			
										/* long liquidFundId=0;
										if(mutualFundId==4452)
										{
											liquidFundId=4639;
										}
										else if(mutualFundId==43)
										{
											liquidFundId=250;
										} */
										//navDatePair=mfc.getNavJsonArray(mutualFundId,startDate,investedAmount);
										Algo algo=new Algo();
										algo.ourRoboAlgo(mutualFundId, startDate,endDate, investedAmount);
										navDatePair=algo.getEquityJsonArray();
										roboNavDatePair=algo.getRoboJsonArray();
										conditionDateJsonArray=algo.getConditionJsonArray();
										equityAmountJsonArray=algo.getOuterEquityJsonArray();
										liquidAmountJsonArray=algo.getOuterLiquidJsonArray();
										
										//System.out.println(conditionDateJsonArray.toString(1));
									%>
									<!-- CHART CONTAINER -->
									<div class="col-sm-12 col-xs-12 shadow-container m-t-md">
										<div id="container">
										</div>
									</div>
									
									<!-- TABLES -->
									<div id="strategyDiv" class="col-sm-12 col-xs-12 shadow-container m-t-md">
										<div class="col-xs-12 col-sm-6 text-center">
											<h3 class="section-sub-title">Entry Alerts</h3>
											<table class="table table-condensed table-bordered table-responsive">
											<tr>
											<th>Strategy</th>
											<th>Date</th>
											</tr>
											<%
												Iterator itr1=entryList.iterator();
												while(itr1.hasNext())
												{
													CustomMFEntryAlert entryAlert=(CustomMFEntryAlert)itr1.next();
													String strategyName=entryAlert.getStrategyName();
													Date date=entryAlert.getTradeDate();
													%>
													<tr>
													<td><%=strategyName %></td>
													<td><%=date %></td>
													</tr>
													<%
												}
											%>
											</table>
										</div>
										<div class="col-xs-12 col-sm-6 text-center">
											<h3 class="section-sub-title">Entry Alerts</h3>
											<table class="table table-condensed table-bordered table-responsive">
											<tr>
											<th>Strategy</th>
											<th>Date</th>
											</tr>
											<%
												Iterator itr2=exitList.iterator();
												while(itr2.hasNext())
												{
													CustomMFExitAlert exitAlert=(CustomMFExitAlert)itr2.next();
													String strategyName=exitAlert.getStrategyName();
													Date date=exitAlert.getTradeDate();
													%>
													<tr>
													<td><%=strategyName %></td>
													<td><%=date %></td>
													</tr>
													<%
												}
											%>
											</table>
										</div>
										</div>
									<%
		
	
											//System.out.println(navDatePair.toString(1));
											}
											catch(Exception e)
											{
												e.printStackTrace();
											}
										}
									%>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
	<script src="assets/plugins/jquery.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/plugins/jquery-easing/jquery.easing.min.js"></script>
    <script src="assets/plugins/jquery-sticky/jquery.sticky.js"></script>
    
	
<script>

$(document).ready(function(event){
	var mutualFundName=$("#mutualFunds option:selected").text();
	console.log("mfname="+mutualFundName);
	$(":submit").click(function(event)
	{		
		if(isNaN($("#ia").val()))
		{
			console.log($("#ia").val());	
			alert("Invest field should be a number")
			event.preventDefault();
		}
	});
		//series Array contains data of nav,tradeDate pair
		var seriesArray=[];
		var seriesExitStrategyArray=[];
		var seriesEntryStrategyArray=[];
		var seriesConditionArray=[];
		
		var navDatePair=<%=navDatePair%>
		var roboNavDatePair=<%=roboNavDatePair%>

		var exitStrategyDateJson=<%=exitStrategyDateJson%>
		var entryStrategyDateJson=<%=entryStrategyDateJson%>
		var conditionDateJson=<%=conditionDateJsonArray%>	
		
		var equityAmountJsonArray=<%=equityAmountJsonArray%>
		var liquidAmountJsonArray=<%=liquidAmountJsonArray%>
		
		if(exitStrategyDateJson!=null || entryStrategyDateJson!=null)
		{
			createStrategyDateJson();
		}
		if(conditionDateJson!=null)
		{
			createConditionDateJson();		
		}
		
		insertJsonIntoSeriesArray();
		console.log(JSON.stringify(seriesArray));	

		if(seriesArray.length!=0)
		{
			Highcharts.stockChart('container', {
				title:{
					text:mutualFundName
				},
				credits: {
			    	enabled: false
			    },
				subtitle:{
					text:'Performance'
				},
				plotOptions:{
		            series:{
		                turboThreshold:100000//set it to a larger threshold, it is by default to 1000
		            }
		        },
				/*  tooltip: {
					
		            pointFormat:'<span style="color:{series.color}">{series.name}</span>: <b>{this.y}</b> <br/>', 
		            valueDecimals: 3,
		            split:true             
		       },  */
		         tooltip: {
		        	valueDecimals: 3,
		            pointFormatter: function() {
		            	//console.log("Formatter : ");
		    	   		//console.log(this);
		    	   	// pointFormat:'<span style="color:{series.color}">{series.name}</span>: <b>{this.y}</b> <br/>'
		    	   	 
		                //return 'Amount <b>' + this.y + '</b>';
		    	   		//return this.series.name + this.y + '</b>';
		    	   		if(this.series.name==='Robo')
		    	   		{
		            		return '<span style="color:'+this.series.color+'">'+this.series.name+'</span><b>:'+this.y+'</b><br/>EA:'+this.equityAmount+'<br/>LA:'+this.liquidAmount+'<br/>ENav:'+this.eNav+'<br/>LNav:'+this.lNav+'<br/>';
		            
		    	   		 }
		    	   		else 
		    	   		{
		    	   			return '<span style="color:'+this.series.color+'">'+this.series.name+'</span><b>:'+this.y+'</b><br/>';
		    	   		} 
		            }
		       		
		        }, 
			    xAxis: {
			        type: 'datetime',
			        dateTimeLabelFormats: { // don't display the dummy year
			            month: '%e. %b',
			            year: '%b'
			        },
			        title: {
			            text: 'Date',
			            align: 'middle'
			        	}
			    },
			    yAxis:	[			          	
			        {	    				        
			        title: {
			            text: 'Amount Growth',
			            align: 'middle'
			        	},
					}
			    	
		       	],	
		       	series:seriesArray,     		    		
			});
		}
		function createStrategyDateJson()
		{
			if(exitStrategyDateJson!=null)
			{
				outerArray=[];
				var innerJson;
				for(var i=0;i<exitStrategyDateJson.length;i++)
				{
					
					//console.log("strategy="+strategyDateJson[i][0]);
					var year=parseInt(moment(exitStrategyDateJson[i][0]).format("YYYY"));
					var month=parseInt(moment(exitStrategyDateJson[i][0]).format("MM")-1);
					var date=parseInt(moment(exitStrategyDateJson[i][0]).format("DD"));
					innerJson={"x":Date.UTC(year,month,date),"title":exitStrategyDateJson[i][1]};
					outerArray.push(innerJson);
				}
				seriesExitStrategyArray=outerArray;	
				//console.log("ExitStrategy"+JSON.stringify(seriesExitStrategyArray));
			}
			if(entryStrategyDateJson!=null)
			{
				outerArray=[];
				var innerJson;
				for(var i=0;i<entryStrategyDateJson.length;i++)
				{
					var year=parseInt(moment(entryStrategyDateJson[i][0]).format("YYYY"));
					var month=parseInt(moment(entryStrategyDateJson[i][0]).format("MM")-1);
					var date=parseInt(moment(entryStrategyDateJson[i][0]).format("DD"));
					//console.log("Year="+year+"Month="+month+"date="+date);
					innerJson={"x":Date.UTC(year,month,date),"title":entryStrategyDateJson[i][1]};
					outerArray.push(innerJson);
				}
				seriesEntryStrategyArray=outerArray;
				//console.log("Entry Strategy="+JSON.stringify(seriesEntryStrategyArray));
			}
		}
		function createConditionDateJson()
		{
			if(conditionDateJson!=null)
			{
				outerArray=[];
				var innerJson;
				for(var i=0;i<conditionDateJson.length;i++)
				{
					var year=parseInt(moment(conditionDateJson[i][0]).format("YYYY"));
					var month=parseInt(moment(conditionDateJson[i][0]).format("MM")-1);
					var date=parseInt(moment(conditionDateJson[i][0]).format("DD"));
					//console.log("Year="+year+"Month="+month+"date="+date);
					innerJson={"x":Date.UTC(year,month,date),"title":conditionDateJson[i][1]};
					outerArray.push(innerJson);
				}
				seriesConditionArray=outerArray;
				//console.log("Condition Occurred="+JSON.stringify(seriesConditionArray));
			}
		}
		function getJsonArray(navDatePair,name)
		{
			outerArray=[];
			var innerJson;
			if(navDatePair!=null && name==='Robo')
			{
				for(var i=0;i<navDatePair.length;i++)
				{
					var ithJsonArray=navDatePair[i];
					var year=parseInt(moment(ithJsonArray[0]).format("YYYY"));
					var month=parseInt(moment(ithJsonArray[0]).format("MM")-1);
					var date=parseInt(moment(ithJsonArray[0]).format("DD"));
					var amount=ithJsonArray[1];
					var equityNav=equityAmountJsonArray[i][0];
					var liquidNav=liquidAmountJsonArray[i][0];
					var equityAmount=equityAmountJsonArray[i][1];
					var liquidAmount=liquidAmountJsonArray[i][1];
					//console.log(year+" "+month+" "+date+" "+amount);
					innerJson={"x":Date.UTC(year,month,date),"y":amount,"equityAmount":equityAmount,"liquidAmount":liquidAmount,"eNav":equityNav,"lNav":liquidNav};
					/* innerArray=[];		
					innerArray[0]=Date.UTC(year,month,date);
					innerArray[1]=amount;  
					outerArray.push(innerArray); */
					outerArray.push(innerJson);
				}
				//console.log(JSON.stringify(outerArray));				
			}
			else if(navDatePair!=null && name==='Equity')
			{
				for(var i=0;i<navDatePair.length;i++)
				{
					var ithJsonArray=navDatePair[i];
					var year=parseInt(moment(ithJsonArray[0]).format("YYYY"));
					var month=parseInt(moment(ithJsonArray[0]).format("MM")-1);
					var date=parseInt(moment(ithJsonArray[0]).format("DD"));
					var amount=ithJsonArray[1];
					
					//console.log(year+" "+month+" "+date+" "+amount);
					innerJson={"x":Date.UTC(year,month,date),"y":amount};
					
					/* innerArray=[];		
					innerArray[0]=Date.UTC(year,month,date);
					innerArray[1]=amount;  
					outerArray.push(innerArray); */
					outerArray.push(innerJson);
				}
			}
		
			//console.log("Outer Array :");
			//console.log(outerArray);
			//seriesArray=outerArray;
			if(name==='Equity')
			{
				seriesArray.push(
				{
					type:'line',
					id:"equity",
					yAxis: 0,
			        animation: false,
			        stack: 0,
					name: name,
					data: outerArray, 
					color:'#111111',
					    
				});				
				var exitFlagJson={
						type:"flags",
						name:'exit',
						data:seriesExitStrategyArray,
						onSeries:'equity',
						shape:'squarepin',
						fillColor: '#FBAB51',								
					};
				
		
				//console.log("Entered EntryStrategyDateJSON");
				var entryFlagJson={
						type:"flags",
						name:'entry',
						data:seriesEntryStrategyArray,
						onSeries:'equity',
						shape:'squarepin',
						fillColor: '#00FF3E',							
				};	
				
		
				seriesArray.push(exitFlagJson);	
				seriesArray.push(entryFlagJson);
				
							
			}
			else
			{
				seriesArray.push(
						{
							type:'line',
							yAxis: 0,
							id:'robo',
					        animation: false,
					        stack: 0,
							name: name,
							data: outerArray, 
							color:'#310FE4',
						});
				var conditionFlagJson={
						type:"flags",
						name:'conditions',
						data:seriesConditionArray,
						onSeries:'robo',
						shape:'squarepin',
						fillColor:'#e3e8ef',
						
				};
				seriesArray.push(conditionFlagJson);
			}
			
		}
		function insertJsonIntoSeriesArray()
		{
			if(navDatePair!=null)
			{
				getJsonArray(navDatePair,"Equity");		
			}
			if(roboNavDatePair!=null)
			{
				getJsonArray(roboNavDatePair,"Robo");		
			}	
			
		}
	
	
});
</script>
</body>
</html>