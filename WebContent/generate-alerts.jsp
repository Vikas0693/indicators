<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.fundexpert.controller.EveryDayCandleController"%>
<%@ page import="com.fundexpert.controller.FractalIndicatorController"%>
<%@ page import="com.fundexpert.controller.IchimokuController"%>
<%@ page import="com.fundexpert.controller.TALibController"%>
<%@ page import="com.fundexpert.controller.FlatPatternsController" %>
<%@ page import="com.fundexpert.controller.CustomMFExitAlertController" %>
<%@ page import="com.fundexpert.controller.CustomMFEntryAlertController" %>
<%@ page import="org.json.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Alerts on indicators</title>
</head>
<body>
<%
	JSONObject jsonObject=new JSONObject();
	try
	{
		//used to generate daily and weekly candles only
		EveryDayCandleController edcc=new EveryDayCandleController();
		edcc.updateEveryDayCandleAfterStartDate();
		
		//below two instructions generate fractal,upFractal,lowFractal
		FractalIndicatorController fic1=new FractalIndicatorController();	
		fic1.updateFractalAfterDate();
		fic1.updateWeeklyFractalAfterDate();
		
		//below two instructions generate swingHigh,swingLow,tcfl's
		fic1.updateSwingAfterDate();
		fic1.updateWeeklySwingAfterDate();						
		
		//below two instructions generate fractal(only upFractal and lowFractal) with period=1  
		fic1.updateWeeklyFractalPeriod1AfterDate();
		fic1.updateWeeklyUpLowFractalPeriod1AfterDate();
		
		//below three instructions are used to generate all Ichimoku indicators
		IchimokuController ic=new IchimokuController();
		ic.updateIchimokuAfterDate();
		ic.updateWeeklyIchimokuAfterDate();
		
		//below three instructions are used to generate all TalibIndicators
		TALibController tALibController=new TALibController();
		tALibController.updateEveryDayAfterDate();
		tALibController.updateWeeklyEveryDayAfterDate();
		
		//below seven instructions are used to generate fractal patterns 
		//daily patterns
		FlatPatternsController fpc=new FlatPatternsController();
		fpc.createLatestSSBFlatPatterns(String.valueOf(20), String.valueOf(1));
		fpc.createLatestTCFL1FlatPatterns(String.valueOf(15),String.valueOf(1));
		fpc.createLatestTCFL3FlatPatterns(String.valueOf(15),String.valueOf(1));		
		//weekly patterns
		fpc.createLatestSSBFlatPatterns(String.valueOf(15),String.valueOf(0));
		fpc.createLatestTCFL1FlatPatterns(String.valueOf(15),String.valueOf(0));
		fpc.createLatestTCFL3FlatPatterns(String.valueOf(15),String.valueOf(0));
		
		//below two instrctions are used to generate exit alerts
		CustomMFExitAlertController customMFExitAlert=new CustomMFExitAlertController();
		customMFExitAlert.updateAllExitAlertsAfterDate();
		
		//below two instructions are used to generate entry alerts
		CustomMFEntryAlertController customMFEntryAlert=new CustomMFEntryAlertController();
		customMFEntryAlert.updateAllEntryAlertsAfterDate();		
		
		jsonObject.put("success","true");
		jsonObject.put("error","");
		out.print(jsonObject);
	}
	catch(Exception e)
	{
		e.printStackTrace();
		jsonObject.put("success","false");
		jsonObject.put("error",e.getMessage());
		out.print(jsonObject);
	}

%>
</body>
</html>