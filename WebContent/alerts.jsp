<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.apidoc.util.HibernateBridge" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="jquery-3.2.1.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Alerts JSP</title>
</head>
<body>
<%
	String submit=request.getParameter("submit");
	if(submit==null)
	{
		Date cDate=new Date();
		Calendar c=Calendar.getInstance();
		c.setTime(cDate);
		c.add(Calendar.MONTH,-3);
		Date oneMonthBackDate=c.getTime();
		String a=new SimpleDateFormat("yyyy-MM-dd").format(oneMonthBackDate);
		String b=new SimpleDateFormat("yyyy-MM-dd").format(cDate);
	%>
		<form name="submit" action="alerts.jsp" method="get">
		start:<input type="date" name="startDate" value=<%=a%>><span id="error"></span>
		<br>end:<input type="date" name="endDate" value=<%=b%>>
		
		<br>entry:<input type="radio" name="alertType" value="entry">
		exit:<input type="radio" name="alertType" value="exit">
		<br><input type="submit" name="submit">
		</form>
	<%
	}
	else
	{
		String sDate=request.getParameter("startDate");
		String eDate=request.getParameter("endDate");
		String alertType=request.getParameter("alertType");
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			Date startDate=new SimpleDateFormat("yyyy-MM-dd").parse(sDate);
			Date endDate=new SimpleDateFormat("yyyy-MM-dd").parse(eDate);
			List<Object[]> alertsList=null;
			System.out.println("AlertType="+alertType);
			if(alertType.equalsIgnoreCase("exit"))
				alertsList=hSession.createQuery("SELECT ex.strategyName,mf.name,ex.exitPrice,ex.tradeDate FROM CustomMFExitAlert ex,MutualFund mf where ex.mutualFundId=mf.id and strategyName like ? and tradeDate between ? and ? order by mutualFundId,tradeDate").setParameter(0,"%"+alertType+"%").setParameter(1,startDate).setParameter(2,endDate).list();
			else
				alertsList=hSession.createQuery("SELECT ex.strategyName,mf.name,ex.entryPrice,ex.tradeDate FROM CustomMFEntryAlert ex,MutualFund mf  where ex.mutualFundId=mf.id and strategyName like ? and tradeDate between ? and ? order by mutualFundId,tradeDate").setParameter(0,"%"+alertType+"%").setParameter(1,startDate).setParameter(2,endDate).list();
			String strategyName="",mutualFundName="";
			Date alertDate=null;
			double alertPrice=0;
			%>
			<table border ="1" cellpadding="4">
			<tr>
			<th>Mutual Fund</th>
			<th>StrategyName</th>
			<th>AlertDate</th>
			<th>AlertPrice</th>
			</tr>
			<%
			for(Object[] o:alertsList)
			{
				strategyName=(String)o[0];
				mutualFundName=(String)o[1];
				alertPrice=(Double)o[2];
				alertDate=(Date)o[3];
				//System.out.println("mutualFundName="+mutualFundName+"   strategyName="+strategyName+"   alertDate="+alertDate+"  alertPrice="+alertPrice);
			%>
			<tr>
			<td><%=mutualFundName %></td>
			<td><%=strategyName %></td>
			<td><%=alertDate %></td>
			<td><%=alertPrice %></td>
			</tr>
			
			<%
			}
			%>
			</table>
			<%
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			e.printStackTrace(response.getWriter());
		}
		finally
		{
			hSession.close();
		}
	}
%>
<script>
$(document).ready(function()
{
	$(":submit").click(function(event)
	{
		console.log("submitted the form");
		var s=$('input[name="startDate"]').val();
		console.log("s="+s);
		var e=$("input[name='endDate']").val();
		console.log("e="+e);
		var sDate=new Date(s);
		var eDate=new Date(e);
		if(sDate.getTime()>=eDate.getTime())
		{
			console.log("oooops");
			$("#error").text("Start Date should be less than End Date");
			event.preventDefault();
			
		}
		
		
	});

});
</script>
</body>
</html>