<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.fundexpert.controller.EveryDayCandleController"%>
<%@ page import="com.fundexpert.controller.FractalIndicatorController"%>
<%@ page import="com.fundexpert.controller.IchimokuController"%>
<%@ page import="org.json.JSONObject" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Drop Tables</title>
<script type="text/javascript" src="jquery-3.2.1.js"></script>
</head>
<body>
<%
	
	String submit=request.getParameter("submit");
	String howToDelete=request.getParameter("howtodelete");
	System.out.print("Submit value="+submit);
	if(submit==null)
	{
%>
	<form id="form" action="delete.jsp" method="post">
	<!-- we are not deleting taLib indicators bcoz they are already included in EveryDayCandle -->
		Current Day Table :<input type="radio" name="tabletodelete" value="current"/>
		<br>
		Every Day Table :<input type="radio" name="tabletodelete" value="everyday"/>
		<br>
		Fractal Table :<input type="radio" name="tabletodelete" value="fractal"/>
		<br>
		Ichimoku Table :<input type="radio" name="tabletodelete" value="ichimoku"/>
		
		<br>
		Select how you wanna delete the table:<br> 
		All :<input type="radio" name="howtodelete" value="all"/> Datewise :<input type="radio" name="howtodelete" value="datewise"/>
		Current :<input type="radio" name="howtodelete" value="current"/>
		<br>
		<input type="date" name="startDate"/>
		<br>
		<input id="submit" type="submit" value="Submit" name="submit"/>
	</form>
<%
	}
	else
	{
		JSONObject resp=new JSONObject();
		String tableToDelete=request.getParameter("tabletodelete");
		String q=null;
		
		try
		{
			System.out.println("table to delete="+tableToDelete);
			if(tableToDelete!=null)
			{
			 	if(tableToDelete.equalsIgnoreCase("everyday"))
				{
					q=request.getParameter("howtodelete");
					if(q!=null)
					{
						EveryDayCandleController edc=new EveryDayCandleController();
						if(q.equalsIgnoreCase("all") || q.equalsIgnoreCase("current"))
						{
							System.out.print("q="+q);
							//edc.delete(q);
						}
						else if(q.equalsIgnoreCase("datewise"))
						{
							String startDate=request.getParameter("startDate");
							if(startDate.equals(""))
							{
								throw new Exception("proper date field not provided in startDate");
							}
							else
							{
								//edc.deleteEveryDayCandleAfterStartDate(startDate);
							}
						}							
					}
					else
					{
						resp.put("success",false);
						resp.put("error","select proper radio button :All,Datewise,Current");
						out.print(resp);
					}
				}
				if(tableToDelete.equalsIgnoreCase("current"))
				{
					//cdc.delete(); 
				}
				if(tableToDelete.equalsIgnoreCase("fractal"))
				{
					q=request.getParameter("howtodelete");
					if(q!=null)
					{
						FractalIndicatorController fic=new FractalIndicatorController();
						if(q.equalsIgnoreCase("all") || q.equalsIgnoreCase("current"))
						{
							//fic.delete(q);
						}
						else if(q.equalsIgnoreCase("datewise"))
						{
							String startDate=request.getParameter("startDate");
							if(startDate.equals(""))
							{
								throw new Exception("proper date field not provided in startDate");
							}
							else
							{
								//fic.deleteFractalIndicatorsAfterStartDate(startDate);
							}
						}							
					}
					else
					{
						resp.put("success",false);
						resp.put("error","select proper radio button :All,Datewise,Current");
						out.print(resp);
					}					
				}
				if(tableToDelete.equalsIgnoreCase("ichimoku"))
				{
					q=request.getParameter("howtodelete");
					if(q!=null)
					{
						IchimokuController ic=new IchimokuController();
						if(q.equalsIgnoreCase("all") || q.equalsIgnoreCase("current"))
						{
							//ic.delete(q);
						}
						else if(q.equalsIgnoreCase("datewise"))
						{
							String startDate=request.getParameter("startDate");
							if(startDate.equals(""))
							{
								throw new Exception("proper date field not provided in startDate");
							}
							else
							{
								//ic.deleteIchimokuIndicatorsAfterStartDate(startDate);
							}
						}
					}
					else
					{
						resp.put("success",false);
						resp.put("error","select proper radio button :All,Datewise,Current");
						out.print(resp);
					}
				}
				resp.put("success",true);
				resp.put("error","");
				out.print(resp);

			}
			else
			{
				resp.put("success",false);
				resp.put("error","select one of four table to delete");
				out.print(resp);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			resp.put("success",false);
			resp.put("error",e.getMessage());
			out.print(resp);
		}
	}
%>
<!-- <script src="scripts/pop.js"></script> -->
<script>
$(document).ready(function()
{	console.log("dfdf");
	$("#submit").click(function(e)
	{
		//e.preventDefault();
		var table=$("input[name=tabletodelete]:checked").val();
		var how=$("input[name=howtodelete]:checked").val();
		console.log(how);
		console.log(table);
		if(typeof table=='undefined')
		{
			alert("select any one of the tables (current/everyday/fractal/ichimoku)");		
			e.preventDefault();
		}
		if(typeof how==='undefined')
		{
			alert("select any one of these (all/datewise/current)");
			e.preventDefault();
		}
		if(how=='datewise' && table)
		{
			var date=$("input[name=startDate]").val();
			console.log(date);
			if(typeof date==="undefined")
			{
				alert("select start date");		
			}
			if(!confirm("confirm to delete data after "+date+" from "+table+" table"))
			{
				e.preventDefault();
			}
		}
		 else if(how && table)
		{
			if(!confirm("confirm to delete "+how+" data from "+table+" table"))
			{
				e.preventDefault();
			} 
		}	
	});
	
});
</script>
</body>
</html>