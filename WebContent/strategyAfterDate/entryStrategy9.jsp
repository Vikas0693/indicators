<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Entry Strategy 9</title>
</head>
<body>

<%
	Session hSession=null;
	Transaction transaction=null;
	try
	{
		String strategyName="EntryStrategy9";
		MutualFundController mfc=new MutualFundController();
		List<Object[]> mfList=mfc.getMutualFundList();
		Iterator iterator=mfList.iterator();
		while(iterator.hasNext())
		{
			Object object[]=(Object[])iterator.next();
			Long mutualFundId=(Long)object[0];
			
			hSession=HibernateBridge.getSessionFactory().openSession();
			Date lastEntry9Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy9'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
			if(lastEntry9Date==null)
			{
				lastEntry9Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=1 and period=2").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
			}
			if(lastEntry9Date==null)
				continue;
			
			List<Fractal> fractalList=hSession.createQuery("from Fractal where mutualFundId=? and period=2 and candleType=1 and tcfl1!=? and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,0d).setParameter(2,lastEntry9Date).list();
			transaction=hSession.beginTransaction();
			if(fractalList!=null && fractalList.size()!=0)
			{
				for(int i=2;i<fractalList.size();i++)
				{
					Fractal currentFractal=fractalList.get(i);
					double currentClosePrice=currentFractal.getClosePrice();
					Date currentTradeDate=currentFractal.getTradeDate();
					if(currentClosePrice>currentFractal.getTcfl1() && currentClosePrice>currentFractal.getTcfl2() && currentClosePrice>currentFractal.getTcfl3())
					{
						Fractal previousFractal=fractalList.get(i-1);
						double previousClosePrice=previousFractal.getClosePrice();
						if(previousFractal.getTcfl1()!=0 && previousClosePrice>previousFractal.getTcfl1() && previousClosePrice>previousFractal.getTcfl2() && previousClosePrice>previousFractal.getTcfl3())
						{
							Fractal pPreviousFractal=fractalList.get(i-2);
							double pPreviousClosePrice=pPreviousFractal.getClosePrice();
							double maxOfTCFL=Math.max(Math.max(pPreviousFractal.getTcfl1(),pPreviousFractal.getTcfl2()),pPreviousFractal.getTcfl3());
							if(pPreviousClosePrice<maxOfTCFL)
							{
									System.out.println("Entry 9 alert for ="+mutualFundId+"  on ="+currentTradeDate);
									CustomMFEntryAlert customMFEntryAlert = new CustomMFEntryAlert();
									customMFEntryAlert.setMutualFundId(mutualFundId);
									customMFEntryAlert.setStrategyName(strategyName);
									customMFEntryAlert.setTradeDate(currentTradeDate);
									customMFEntryAlert.setReasonForAlert(1);
									customMFEntryAlert.setEntryPrice(currentClosePrice);
	                               	hSession.saveOrUpdate(customMFEntryAlert);								
							}
						}
					}
				}
			}
			hSession.flush();
			transaction.commit();
		
		}
		out.write("Finished Processing.");
		BufferedWriter bw=null;
		try
		{
			File f=new File("/opt/strategy");
        	if(!f.isDirectory())
        	{
        		f.mkdirs();
        	}
        	
        	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
			String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			bw.write(entry);
			bw.newLine();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			bw.close();
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
		e.printStackTrace(response.getWriter());
		if(transaction!=null)
			transaction.rollback();
	}
	finally
	{
		hSession.close();
	}

%>
</body>
</html>