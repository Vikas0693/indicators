<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Entry Strategy 16</title>
</head>
<body>

<%
	Session hSession=null;
	Transaction transaction=null;
	try
	{
		String strategyName="EntryStrategy16";
		MutualFundController mfc=new MutualFundController();
		List<Object[]> mfList=mfc.getMutualFundList();
		Iterator iterator=mfList.iterator();
		while(iterator.hasNext())
		{
			Object object[]=(Object[])iterator.next();
			Long mutualFundId=(Long)object[0];
			
			hSession=HibernateBridge.getSessionFactory().openSession();
			transaction=hSession.beginTransaction();
			Date previousDate=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy16'").setParameter(0,mutualFundId).uniqueResult();
			List alerts=hSession.createQuery("from CustomMFEntryAlert where mutualFundId=? and (strategyName='EntryStrategy6' or strategyName='EntryStrategy11' or strategyName='EntryStrategy12' or strategyName='EntryStrategy13') and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,previousDate).list();
			for(int i=0;i<alerts.size();i++)
			{
				CustomMFEntryAlert entryAlert=(CustomMFEntryAlert)alerts.get(i);
				Date alertDate=entryAlert.getTradeDate();
				Calendar cal=Calendar.getInstance();
				cal.setTime(alertDate);
				cal.add(Calendar.MONTH,-4);
				//System.out.println("previousDate="+previousDate+"Current="+alertDate+" previous4MonthDate="+cal.getTime()+"AlertType="+entryAlert.getStrategyName());
				
				//System.out.println("List"+alert16Within6Month+"mf="+mutualFundId);
				
				
				if(previousDate!=null)
				{
					if(previousDate.compareTo(cal.getTime())<0)
					{
						System.out.println("Entry 16 Alert for mfid="+mutualFundId+" on alertDate="+alertDate);
						CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
						customMFEntryAlert.setMutualFundId(mutualFundId);
						customMFEntryAlert.setStrategyName(strategyName);
						customMFEntryAlert.setTradeDate(alertDate);
						customMFEntryAlert.setEntryPrice(entryAlert.getEntryPrice());
						customMFEntryAlert.setReasonForAlert(1);
						hSession.saveOrUpdate(customMFEntryAlert);
						previousDate=alertDate;
					}
				}
				else
				{
					System.out.println("Entry 16 alert found for mfid="+mutualFundId+" on alertDate="+alertDate+" with previousDate="+previousDate);
					CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
					customMFEntryAlert.setMutualFundId(mutualFundId);
					customMFEntryAlert.setStrategyName(strategyName);
					customMFEntryAlert.setTradeDate(alertDate);
					customMFEntryAlert.setEntryPrice(entryAlert.getEntryPrice());
					customMFEntryAlert.setReasonForAlert(1);
					hSession.saveOrUpdate(customMFEntryAlert);
					previousDate=alertDate;
				}			

			}
			transaction.commit();
			
		}	
		out.write("Finished Processing.");
		BufferedWriter bw=null;
		try
		{
			File f=new File("/opt/strategy");
        	if(!f.isDirectory())
        	{
        		f.mkdirs();
        	}
        	
        	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
			String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			bw.write(entry);
			bw.newLine();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			bw.close();
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
		e.printStackTrace(response.getWriter());
		if(transaction!=null)
			transaction.rollback();
	}
	finally
	{
		hSession.close();
	}

%>
</body>
</html>