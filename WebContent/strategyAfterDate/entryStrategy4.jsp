<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.strategy.controller.HibernateConnection" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Entry Strategy 4</title>
</head>
<body>
	<% 
	Session hSession=null;
	Transaction tx=null;
	try {
			String strategyName="EntryStrategy4";
		
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			int count=0;
			hSession=HibernateBridge.getSessionFactory().openSession();
			while(iterator.hasNext())
			{				
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				Date lastEntry4Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy4'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
				if(lastEntry4Date==null)
				{
					lastEntry4Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=1 and period=2").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				}
				if(lastEntry4Date==null)
					continue;
				List<Fractal> dailyCandleList = hSession.createQuery("from Fractal where mutualFundId=? and candleType=1 and period=2 and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,lastEntry4Date).list();
				
				tx=hSession.beginTransaction();
				for(int i=1;i<dailyCandleList.size();i++)
				{
					Fractal currentFractal=(Fractal)dailyCandleList.get(i);
					double closePrice=currentFractal.getClosePrice();
					double upFractal=currentFractal.getUpFractal();
					Double tcfl1=currentFractal.getTcfl1();
					Double tcfl2=currentFractal.getTcfl2();
					Double tcfl3=currentFractal.getTcfl3();
					
					if(tcfl1!=null  && closePrice<tcfl1 && closePrice<tcfl2 && closePrice<tcfl3 && closePrice>upFractal)
					{
						Fractal previousFractal=dailyCandleList.get(i-1);
						if(previousFractal.getClosePrice()<previousFractal.getUpFractal())
						{
							CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
							customMFEntryAlert.setMutualFundId(mutualFundId);
							customMFEntryAlert.setStrategyName(strategyName);
							customMFEntryAlert.setTradeDate(currentFractal.getTradeDate());
							customMFEntryAlert.setEntryPrice(currentFractal.getClosePrice());
							customMFEntryAlert.setReasonForAlert(1);
							hSession.saveOrUpdate(customMFEntryAlert);
							System.out.println("Entry 4 alert "+mutualFundId+" \t\t "+currentFractal.getTradeDate());
						}
					}	
				}	
				tx.commit();
            }
            out.print("Finished processing all data.<br>");
            out.flush();
            BufferedWriter bw=null;
            try
            {
            	File f=new File("/opt/strategy");
            	if(!f.isDirectory())
            	{
            		f.mkdirs();
            	}
            	
            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            	bw.write(entry);
            	bw.newLine();
            }
            catch(Exception e)
            {
            	e.printStackTrace();
            }
            finally
            {
            	bw.close();
            }
        } 
		catch (Exception e)
		{
           e.printStackTrace(response.getWriter());
           if(tx!=null)
        	   tx.rollback();
        }
		finally
		{
			hSession.close();
		}
		
	%>
	
</body>
</html>