<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.strategy.controller.HibernateConnection" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Entry 8</title>
</head>
<body>
	<% 
	Session hSession=null;
	Transaction tx=null;
	try {
			String strategyName="EntryStrategy8";
		
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			int count=0;
			hSession=HibernateBridge.getSessionFactory().openSession();
			while(iterator.hasNext())
			{				
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				Date lastEntry8Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy8'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
				if(lastEntry8Date==null)
				{
					lastEntry8Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=0 and period=2").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				}
				if(lastEntry8Date==null)
					continue;
				List<Fractal> weeklyTcflCandleList = hSession.createQuery("from Fractal where mutualFundId=? and candleType=0 and period=2 and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,lastEntry8Date).list();
				//List<Fractal> weeklyUpFractalCandleList = HibernateConnection.getFractalStrategy(mutualFundId,0,1);
				
				
				tx=hSession.beginTransaction();
				for(int i=1;i<weeklyTcflCandleList.size();i++)
				{
					Fractal currentTcflFractal=(Fractal)weeklyTcflCandleList.get(i);
					//Fractal currentUpFractal=(Fractal)weeklyUpFractalCandleList.get(i);
					
					Date currentTradeDate=currentTcflFractal.getTradeDate();
					double closePrice=currentTcflFractal.getClosePrice();
									
					Double currentTcfl1=currentTcflFractal.getTcfl1();
					Double currentTcfl2=currentTcflFractal.getTcfl2();
					Double currentTcfl3=currentTcflFractal.getTcfl3(); 
					
					//double upFractal=currentUpFractal.getUpFractal();
					
					boolean flatPatternSSBFound=false;
					boolean flatPatternTCFL1Found=false;
					boolean flatPatternTCFL3Found=false;
					//System.out.println("0 condition="+currentTradeDate+"mf="+mutualFundId);
					if(currentTcfl1!=null  && closePrice<currentTcfl1 && closePrice<currentTcfl2 && closePrice<currentTcfl3)
					{		
						//System.out.println("First condition="+currentTradeDate+"mf="+mutualFundId);
						List previous2DaysPeriod1List=hSession.createQuery("from Fractal where mutualFundId=? and period=? and candleType=? and tradeDate<=? order by tradeDate desc").setParameter(0,mutualFundId).setParameter(1,1).setParameter(2,0).setParameter(3, currentTradeDate).setMaxResults(2).list();
						Fractal currentFractalWithPeriod1=(Fractal)previous2DaysPeriod1List.get(0);
						Fractal previousFractalWithPeriod1=(Fractal)previous2DaysPeriod1List.get(1);
						if(currentFractalWithPeriod1.getTradeDate().compareTo(currentTradeDate)!=0)
						{
							throw new Exception("currentTradeDate does not exist in fractal of period 1");
						}
						if(currentFractalWithPeriod1.getClosePrice()>currentFractalWithPeriod1.getUpFractal() &&  previousFractalWithPeriod1.getClosePrice()<previousFractalWithPeriod1.getUpFractal())
						{											
							//System.out.println("Second condition="+currentTradeDate+"mf="+mutualFundId);
							Calendar calendar=Calendar.getInstance();
							calendar.setTime(currentTradeDate);
							calendar.add(Calendar.MONTH,-8);
							Date previousEightMonthDate=calendar.getTime(); 
							
							List flatPatternBeforeTradeDateSsb=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"SSB").setParameter(1,mutualFundId).setParameter(2,15).setParameter(3, 0).setParameter(4,currentTradeDate).setMaxResults(1).list();
							if(flatPatternBeforeTradeDateSsb.size()!=0)
							{
								FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateSsb.get(0);
								Date lastFlatPatternDate=flatPattern.getPatternStartDate();
								
								if(lastFlatPatternDate.compareTo(previousEightMonthDate)<0)
								{
									flatPatternSSBFound=true;
								//	System.out.println("pattern start date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
								}
								else
								{
									double ssb=flatPattern.getPatternValue();
									if(ssb>(closePrice*1.05))
									{
										//System.out.println("ssb true "+mutualFundId+"tradeDate="+tradeDate);
										//System.out.println("pattern date found(Nav checked) alert for mf="+mutualFundId+" for "+tradeDate+"  lastFlatDate="+lastFlatPatternDate);
										flatPatternSSBFound=true;
									}
									else
									{
										//System.out.print("ssb pattern date found ( nav not verified)");
										flatPatternSSBFound=false;
										continue;
									}
								}
							}
							else
							{
								//flatPatternSSBFound=false;
								//System.out.println("No pattern date found alert for mf="+mutualFundId+" for "+tradeDate);
								flatPatternSSBFound=true;
							}
							List flatPatternBeforeTradeDateTcfl1=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"TCFL1").setParameter(1,mutualFundId).setParameter(2,15).setParameter(3, 0).setParameter(4,currentTradeDate).setMaxResults(1).list();
							if(flatPatternSSBFound=true)
							{
								if(flatPatternBeforeTradeDateTcfl1.size()!=0)
								{
									FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateTcfl1.get(0);
									Date lastFlatPatternDate=flatPattern.getPatternStartDate();
									
									if(lastFlatPatternDate.compareTo(previousEightMonthDate)<0)
									{
										flatPatternTCFL1Found=true;
										//System.out.println("pattern end date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
									}
									else
									{
										double tcfl1=flatPattern.getPatternValue();
										if(tcfl1>(closePrice*1.05))
										{
											//System.out.println("tcfl1 true "+mutualFundId+"tradeDate="+tradeDate);
											flatPatternTCFL1Found=true;
										}
										else
										{
											flatPatternTCFL1Found=false;
											continue;
										}
									}	
								}
								else
								{
									flatPatternTCFL1Found=true;
								}
							}
							else
							{
								continue;
							}
							List flatPatternBeforeTradeDateTcfl3=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"TCFL3").setParameter(1,mutualFundId).setParameter(2,15).setParameter(3,0).setParameter(4,currentTradeDate).setMaxResults(1).list();
							if(flatPatternSSBFound=true && flatPatternTCFL1Found==true)
							{
								if(flatPatternBeforeTradeDateTcfl3.size()!=0)
								{
									FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateTcfl3.get(0);
									Date lastFlatPatternDate=flatPattern.getPatternStartDate();
									
									if(lastFlatPatternDate.compareTo(previousEightMonthDate)<0)
									{
										flatPatternTCFL3Found=true;
										//System.out.println("tcfl 3 pattern end date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
									}
									else
									{
										double tcfl3=flatPattern.getPatternValue();
										if(tcfl3>(closePrice*1.05))
										{
											//System.out.println("tcfl3 true "+mutualFundId+"tradeDate="+tradeDate);
											flatPatternTCFL3Found=true;
										}
										else
										{
											flatPatternTCFL3Found=false;
											continue;
										}
									}
								}
								else
								{
									//System.out.print("tcfl 3 no pattern date found  tradeDate="+tradeDate);
									flatPatternTCFL3Found=true;
								}
							}
							else
							{								
								continue;
							} 								
							if(flatPatternSSBFound==true && flatPatternTCFL1Found==true && flatPatternTCFL3Found==true)
							{
								System.out.println("Entry 8 Alert="+mutualFundId+" currentTradeDate="+currentTradeDate);
								CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
								customMFEntryAlert.setMutualFundId(mutualFundId);
								customMFEntryAlert.setStrategyName(strategyName);
								customMFEntryAlert.setTradeDate(currentTradeDate);
								customMFEntryAlert.setEntryPrice(closePrice);
								customMFEntryAlert.setReasonForAlert(1);
								hSession.saveOrUpdate(customMFEntryAlert);
								//System.out.println(mutualFundId+" \t\t "+currentFractal.getTradeDate());
							}
							
						}
					}	
				}	
				tx.commit();
            }
            out.print("Finished processing all data.for EntryStrategy8<br>");
            out.flush();
            BufferedWriter bw=null;
            try
            {
            	File f=new File("/opt/strategy");
            	if(!f.isDirectory())
            	{
            		f.mkdirs();
            	}
            	
            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            	bw.write(entry);
            	bw.newLine();
            }
            catch(Exception e)
            {
            	e.printStackTrace();
            }
            finally
            {
            	bw.close();
            }
        } 
		catch (Exception e)
		{
			e.printStackTrace();
           e.printStackTrace(response.getWriter());
           if(tx!=null)
        	   tx.rollback();
        }
		finally
		{
			hSession.close();
		}		
	%>	
</body>
</html>