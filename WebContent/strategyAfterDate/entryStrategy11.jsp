<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Entry Strategy 11</title>
</head>
<body>

<%
	Session hSession=null;
	Transaction transaction=null;
	try
	{
		String strategyName="EntryStrategy11";
		MutualFundController mfc=new MutualFundController();
		List<Object[]> mfList=mfc.getMutualFundList();
		Iterator iterator=mfList.iterator();
		while(iterator.hasNext())
		{
			List strategy9List=null;
			Object object[]=(Object[])iterator.next();
			Long mutualFundId=(Long)object[0];
			
			hSession=HibernateBridge.getSessionFactory().openSession();
			transaction=hSession.beginTransaction();
			Date lastEntry11Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy11'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
			if(lastEntry11Date==null)
			{
				strategy9List=hSession.createQuery("from CustomMFEntryAlert where mutualFundId=? and strategyName like ? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,"%EntryStrategy9%").list();
			}
			else
			{
				strategy9List=hSession.createQuery("from CustomMFEntryAlert where mutualFundId=? and strategyName like ? and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,"%EntryStrategy9%").setParameter(2,lastEntry11Date).list();
			}

			if(strategy9List!=null)
			{
				for(int i=1;i<strategy9List.size();i++)
				{
					double diff=0;
					CustomMFEntryAlert entryAlert0=(CustomMFEntryAlert)strategy9List.get(i-1);
					
					CustomMFEntryAlert entryAlert1=(CustomMFEntryAlert)strategy9List.get(i);
				
					diff=entryAlert1.getTradeDate().getTime()-entryAlert0.getTradeDate().getTime();
					if((diff/(1000*60*60*24))>30.0)
					{
						System.out.println("Entry 11 alert for  mfId="+mutualFundId+"  on"+entryAlert1.getTradeDate());
						CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
						customMFEntryAlert.setStrategyName(strategyName);
						customMFEntryAlert.setMutualFundId(mutualFundId);
						customMFEntryAlert.setTradeDate(entryAlert1.getTradeDate());
						customMFEntryAlert.setEntryPrice(entryAlert1.getEntryPrice());
						customMFEntryAlert.setReasonForAlert(1);
						hSession.saveOrUpdate(customMFEntryAlert);
					}
				}
			}
			hSession.flush();
			transaction.commit();
		
		}
		out.write("Finished Processing.");
		BufferedWriter bw=null;
		try
		{
			File f=new File("/opt/strategy");
        	if(!f.isDirectory())
        	{
        		f.mkdirs();
        	}
        	
        	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
			String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			bw.write(entry);
			bw.newLine();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			bw.close();
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
		e.printStackTrace(response.getWriter());
		if(transaction!=null)
			transaction.rollback();
	}
	finally
	{
		hSession.close();
	}

%>
</body>
</html>