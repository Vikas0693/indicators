<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="com.fundexpert.dao.CustomMFExitAlert" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Entry Strategy 12</title>
</head>
<body>

<%
	Session hSession=null;
	Transaction transaction=null;
	try
	{
		String strategyName="EntryStrategy12";
		MutualFundController mfc=new MutualFundController();
		List<Object[]> mfList=mfc.getMutualFundList();
		Iterator iterator=mfList.iterator();
		while(iterator.hasNext())
		{
			Object object[]=(Object[])iterator.next();
			Long mutualFundId=(Long)object[0];
			
			hSession=HibernateBridge.getSessionFactory().openSession();
			transaction=hSession.beginTransaction();

			Date lastEntry12Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy12'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();

			List custom10EntryAlertList=null;
			if(lastEntry12Date==null)
			{
				custom10EntryAlertList=hSession.createQuery("from CustomMFEntryAlert where strategyName like '%EntryStrategy10%' and mutualFundId=?").setParameter(0,mutualFundId).list();
			}
			else
			{
				custom10EntryAlertList=hSession.createQuery("from CustomMFEntryAlert where strategyName like '%EntryStrategy10%' and mutualFundId=? and tradeDate>=?").setParameter(0,mutualFundId).setParameter(1,lastEntry12Date).list();
			}
			

			if(custom10EntryAlertList!=null)
			{
				Iterator itr=custom10EntryAlertList.iterator();
				//the point of doing this(below if clause) is: before first enrtyStrategy10 there is no another entryStratrgy10 alert so skip 1st entry10 alert
				if(itr.hasNext())
				{
					itr.next();
				}
				while(itr.hasNext())
				{
					CustomMFEntryAlert entry10Alert=(CustomMFEntryAlert)itr.next();
					Date currentEntry10Date=entry10Alert.getTradeDate();
					Date previousEntry10Date=(Date)hSession.createQuery("select tradeDate from CustomMFEntryAlert where strategyName like '%EntryStrategy10%' and mutualFundId=? and tradeDate<? order by tradeDate desc").setParameter(0,mutualFundId).setParameter(1,currentEntry10Date).setMaxResults(1).uniqueResult();
					if(previousEntry10Date!=null)
					{
						List exit5AlertList=hSession.createQuery("from CustomMFExitAlert where strategyName like '%ExitStrategy5%' and mutualFundId=? and tradeDate between ? and ?").setParameter(0,mutualFundId).setParameter(1,previousEntry10Date).setParameter(2,currentEntry10Date).list();
						if(exit5AlertList!=null && !(exit5AlertList.isEmpty()))
						{
							CustomMFExitAlert alert5=(CustomMFExitAlert)exit5AlertList.get(0);
							System.out.println("Entry 12 alert for ="+mutualFundId+"  on ="+currentEntry10Date+"  previous10AlertDate="+previousEntry10Date+"  alert5Date="+alert5.getTradeDate());
							CustomMFEntryAlert alert12=new CustomMFEntryAlert();
							alert12.setMutualFundId(mutualFundId);
							alert12.setTradeDate(currentEntry10Date);
							alert12.setStrategyName(strategyName);
							alert12.setEntryPrice(entry10Alert.getEntryPrice());
							alert12.setReasonForAlert(1);
							hSession.saveOrUpdate(alert12);
						}
					}
				}
				
			}		
			hSession.flush();
			transaction.commit();
		}	
		out.write("Finished Processing.");
		BufferedWriter bw=null;
		try
		{
			File f=new File("/opt/strategy");
        	if(!f.isDirectory())
        	{
        		f.mkdirs();
        	}
        	
        	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
			String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			bw.write(entry);
			bw.newLine();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			bw.close();
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
		e.printStackTrace(response.getWriter());
		if(transaction!=null)
			transaction.rollback();
	}
	finally
	{
		hSession.close();
	}

%>
</body>
</html>