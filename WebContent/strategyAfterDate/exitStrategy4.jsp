<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.dao.CustomMFExitAlert" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ExitStrategy4</title>
</head>
<body>

<%
	Session hSession=null;
	Transaction transaction=null;
	try
	{
		String strategyName="ExitStrategy4";
		MutualFundController mfc=new MutualFundController();
		List<Object[]> mfList=mfc.getMutualFundList();
		Iterator iterator=mfList.iterator();
		while(iterator.hasNext())
		{
			Object object[]=(Object[])iterator.next();
			Long mutualFundId=(Long)object[0];
			
			hSession=HibernateBridge.getSessionFactory().openSession();
			Date lastExit4Date=(Date)hSession.createQuery("SELECT max(tradeDate) from CustomMFExitAlert where mutualFundId=? and strategyName='ExitStrategy4'").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
			if(lastExit4Date==null)
			{
				lastExit4Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=0 and period=2 and tcfl1!=0").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
			}
			if(lastExit4Date==null)
			{
				continue;
			}	
			
			List<Fractal> fractalList=hSession.createQuery("from Fractal where mutualFundId=? and period=? and candleType=? and tcfl1!=? and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,2).setParameter(2,0).setParameter(3,0d).setParameter(4,lastExit4Date).list();
			transaction=hSession.beginTransaction();
			if(fractalList!=null && fractalList.size()!=0)
			{
				for(int i=2;i<fractalList.size();i++)
				{
					Fractal currentFractal=fractalList.get(i);
					double currentClosePrice=currentFractal.getClosePrice();
					Date currentTradeDate=currentFractal.getTradeDate();
					if(currentClosePrice<currentFractal.getTcfl1() && currentClosePrice<currentFractal.getTcfl2() && currentClosePrice<currentFractal.getTcfl3())
					{
						Fractal previousFractal=fractalList.get(i-1);
						double previousClosePrice=previousFractal.getClosePrice();
						if(previousFractal.getTcfl1()!=0 && previousClosePrice<previousFractal.getTcfl1() && previousClosePrice<previousFractal.getTcfl2() && previousClosePrice<previousFractal.getTcfl3())
						{
							Fractal pPreviousFractal=fractalList.get(i-2);
							double pPreviousClosePrice=pPreviousFractal.getClosePrice();
							double minTCFL=Math.min(Math.min(pPreviousFractal.getTcfl1(),pPreviousFractal.getTcfl2()),pPreviousFractal.getTcfl3());
							if(pPreviousFractal.getTcfl1()!=0 && pPreviousClosePrice>minTCFL)
							{
									System.out.println("Exit 4 alert for ="+mutualFundId+"  on ="+currentTradeDate);
									CustomMFExitAlert customMFExitAlert = new CustomMFExitAlert();
	                                customMFExitAlert.setMutualFundId(mutualFundId);
	                                customMFExitAlert.setStrategyName(strategyName);
	                                customMFExitAlert.setTradeDate(currentTradeDate);
	                                customMFExitAlert.setReasonForAlert(1);
	                                customMFExitAlert.setExitPrice(currentClosePrice);
	                               	hSession.saveOrUpdate(customMFExitAlert);								
							}
						}
					}
				}
			}
			hSession.flush();
			transaction.commit();
		
		}
		out.write("Finished Processing.");
		  BufferedWriter bw=null;
          try
          {
        	  File f=new File("/opt/strategy");
            	if(!f.isDirectory())
            	{
            		f.mkdirs();
            	}
            	
            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/exit.txt"),true));
          	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
          	bw.write(entry);
          	bw.newLine();
          }
          catch(Exception e)
          {
          	e.printStackTrace();
          }
          finally
          {
          	bw.close();
          }
	}
	catch(Exception e)
	{
		System.out.println("Exception Occurred");
		e.printStackTrace();
		e.printStackTrace(response.getWriter());
		if(transaction!=null)
			transaction.rollback();
	}
	finally
	{
		hSession.close();
	}

%>
</body>
</html>