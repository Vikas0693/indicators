<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ page import="com.fundexpert.dao.CustomMFExitAlert" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.apidoc.util.HibernateBridge" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Exit Strategy3</title>
</head>

	<%
	Session hSession=null;
	Transaction tx=null;
	try
	{
		String strategyName="ExitStrategy3";
		MutualFundController mfc=new MutualFundController();
		List<Object[]> mfList=mfc.getMutualFundList();
		Iterator iterator=mfList.iterator();
		hSession=HibernateBridge.getSessionFactory().openSession();
		while(iterator.hasNext())
		{
			
			Object object[]=(Object[])iterator.next();
			Long mutualFundId=(Long)object[0];
			System.out.println("MfId="+mutualFundId);
			Date lastExit3Date=(Date)hSession.createQuery("SELECT max(tradeDate) from CustomMFExitAlert where mutualFundId=? and strategyName='ExitStrategy3'").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
			if(lastExit3Date==null)
			{
				lastExit3Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=1 and period=2 and tcfl1!=0").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
			}
			if(lastExit3Date==null)
			{
				continue;
			}	
			List<Fractal> fractalList=hSession.createQuery("from Fractal where mutualFundId=? and period=? and candleType=? and tcfl1!=? and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1, 2).setParameter(2,1).setParameter(3,0d).setParameter(4,lastExit3Date).list();
			if(fractalList!=null)
			{
				tx=hSession.beginTransaction();
				for(int i=1;i<fractalList.size();i++)
				{
					Fractal currentFractal=fractalList.get(i);
					Date currentTradeDate=currentFractal.getTradeDate();
					double currentClosePrice=currentFractal.getClosePrice();
					System.out.println("Mf-"+mutualFundId+"CurrentDate="+currentTradeDate);
					if(currentFractal.getTcfl1()!=0 && currentClosePrice<currentFractal.getTcfl1() && currentClosePrice<currentFractal.getTcfl2() && currentClosePrice<currentFractal.getTcfl3())
					{
						//System.out.println("Date1="+currentTradeDate+"    \t"+mutualFundId);
						List flatPatternSSBList=hSession.createQuery("from FlatPatterns where mutualFundId=? and patternName=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,mutualFundId).setParameter(1,"SSB").setParameter(2,20).setParameter(3,1).setParameter(4,currentTradeDate).setMaxResults(1).list();
						if((flatPatternSSBList!=null && flatPatternSSBList.size()!=0))
						{
							double ssbPatternValue=((FlatPatterns)flatPatternSSBList.get(0)).getPatternValue();
							Date lastFlatPatternDate=((FlatPatterns)flatPatternSSBList.get(0)).getPatternStartDate();
							
							Calendar calendar=Calendar.getInstance();
							calendar.setTime(currentTradeDate);
							calendar.add(Calendar.MONTH,-4);
							Date previousThreeMonthDate=calendar.getTime(); 
														
							if(lastFlatPatternDate.compareTo(previousThreeMonthDate)>0 && currentClosePrice<ssbPatternValue)
							{
								//System.out.println("Date2="+currentTradeDate);
								Fractal previousFractal=fractalList.get(i-1);
								if(previousFractal.getClosePrice()>ssbPatternValue)
								{
									System.out.println("Exit 3 Alert for="+mutualFundId+" for date="+currentTradeDate);
									CustomMFExitAlert customMFExitAlert=new CustomMFExitAlert();
									customMFExitAlert.setMutualFundId(mutualFundId);
									customMFExitAlert.setStrategyName(strategyName);
									customMFExitAlert.setReasonForAlert(1);
									customMFExitAlert.setTradeDate(currentTradeDate);
									customMFExitAlert.setExitPrice(currentClosePrice);
									hSession.saveOrUpdate(customMFExitAlert);
								}
							}
						}
					}
				}
				hSession.flush();
				tx.commit();
			}
		}
		out.write("Finished Processing.");
		  BufferedWriter bw=null;
          try
          {
        	  	File f=new File("/opt/strategy");
            	if(!f.isDirectory())
            	{
            		f.mkdirs();
            	}
            	
            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/exit.txt"),true));
          		String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
          		bw.write(entry);
          		bw.newLine();
          }
          catch(Exception e)
          {
          	e.printStackTrace();
          }
          finally
          {
          	bw.close();
          }
	}
	catch(Exception e)
	{
		e.printStackTrace();
		e.printStackTrace(response.getWriter());
		if(tx!=null)
			tx.rollback();
	}
	finally
	{
		hSession.close();
	}
	%>
</body>
</html>