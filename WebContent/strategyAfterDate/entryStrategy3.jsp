<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="com.fundexpert.dao.EveryDayCandle" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>

<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.strategy.controller.HibernateConnection" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Entry Strategy 3</title>
</head>
<body>
	<% 
	Session hSession=null;
	Transaction tx=null;
	try {
			String strategyName="EntryStrategy3";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			int count=0;
			hSession=HibernateBridge.getSessionFactory().openSession();
			while(iterator.hasNext())
			{				
				tx=hSession.beginTransaction();
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				Date lastEntry3Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy3'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
				if(lastEntry3Date==null)
				{
					lastEntry3Date=(Date)hSession.createQuery("select min(tradeDate) from EveryDayCandle where mutualFundId=? and candleType=0").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				}
				if(lastEntry3Date==null)
					continue;
				
				List<EveryDayCandle> weeklyCandleList=hSession.createQuery("from EveryDayCandle where mutualFundId=? and candleType=? and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,0).setParameter(2, lastEntry3Date).list(); 
				// This you need to write on your own, i need Weekly RSI 70 value for the symbol
				// CustomMFExitAlert is the object in which we are persisting Exit dates for different exit startegy 
	            boolean watchActivatedCond1Met = false; // we have to persist this when this code moves to DataController
	            int countWatchCond1 = 0;
	            int countWatchTotalCond1 = 0;
	            boolean watchActivatedCond2 = false;
				
	            int countWatchTotalCond2 = 0;
	
	            if (weeklyCandleList != null && weeklyCandleList.size() != 0) {
	                for (EveryDayCandle weeklyCandle : weeklyCandleList) {
	                	//System.out.println("Mf="+mutualFundId+"\t\t"+weeklyCandle.getRsi());
	                    if (watchActivatedCond2)  // Checking for final condition
	                    {
	                        if (weeklyCandle.getRsi() > 40) {
	                            countWatchTotalCond2++;
	                            if (countWatchTotalCond2 >= 4) {
	                                watchActivatedCond2 = false; //restting again for next check
	                                watchActivatedCond1Met = false;//restting again for next check
	                                // EXIT CONDIITON MET, TO Populate Database
	                                //strategyName - "ExitStrategy1"
	                                // symbol - symbol
	                                // tradeDate - weeklyCandleList.get(index).getTradDate()
	                                //reasonForAlert - "as per ExitStrategy1"
	                                // exitPrice  -  weeklyCandleList.get(index).getClosePrice()
	                                //nftyPrice - for Nifty same as weeklyCandleList.get(index).getClosePrice()
	                                CustomMFEntryAlert customMFEntryAlert = new CustomMFEntryAlert();
	                                customMFEntryAlert.setMutualFundId(mutualFundId);
	                                customMFEntryAlert.setStrategyName(strategyName);
	                                customMFEntryAlert.setTradeDate(weeklyCandle.getTradeDate());
	                                customMFEntryAlert.setReasonForAlert(1);
	                                customMFEntryAlert.setEntryPrice(weeklyCandle.getTodaysClosePrice());
	                                //customMFExitAlert.setNiftyEntryPrice(weeklyCandle.getClosePrice());
	                               	hSession.saveOrUpdate(customMFEntryAlert);
	                                out.print("Fount Entry 3 alert [" + weeklyCandle.getTradeDate() + "] .<br>");
	                                out.flush();
	                                System.out.println("Found Entry 3 alert for="+mutualFundId+"  on "+weeklyCandle.getTradeDate());
	
	                            }
	                        } else {
	                            // Second COunter to be reset
	                            countWatchTotalCond2 = 0;
	                        }
	                    }
	
	                    if (!watchActivatedCond1Met && weeklyCandle.getRsi() < 40) {
	                        countWatchCond1++;
	                        if (countWatchCond1 == 1) {
	                            countWatchTotalCond1 = 0;
	                        } else if (countWatchCond1 >= 3) {
	                            watchActivatedCond2 = true;
	                            watchActivatedCond1Met = true;
	                            countWatchTotalCond1 = 0;
	                            countWatchCond1 = 0;
	                        }
	                    }
	
	                    if (!watchActivatedCond1Met)   // Not 30
	                    {
	                        countWatchTotalCond1++;
	                    }
	
	                    if (!watchActivatedCond1Met && countWatchTotalCond1 >= 4) {
	                        watchActivatedCond1Met = false;
	                        countWatchCond1 = 0;
	                        countWatchTotalCond1 = 0;
	                    }
	
	                }
	            }
	            tx.commit();
            }
            out.print("Finished processing all data.<br>");
            out.flush();
            BufferedWriter bw=null;
            try
            {
            	File f=new File("/opt/strategy");
            	if(!f.isDirectory())
            	{
            		f.mkdirs();
            	}
            	
            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            	bw.write(entry);
            	bw.newLine();
            }
            catch(Exception e)
            {
            	e.printStackTrace();
            }
            finally
            {
            	bw.close();
            }
        } 
		catch (Exception e)
		{
           e.printStackTrace(response.getWriter());
           e.printStackTrace();
           if(tx!=null)
        	   tx.rollback();
        }
		finally
		{
			hSession.close();
		}
	%>
	
</body>
</html>