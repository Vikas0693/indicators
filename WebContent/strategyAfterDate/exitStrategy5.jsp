<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.dao.CustomMFExitAlert" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Exit Strategy 5</title>
</head>
<body>

<%
	Session hSession=null;
	Transaction transaction=null;
	try
	{
		String strategyName="ExitStrategy5";
		MutualFundController mfc=new MutualFundController();
		List<Object[]> mfList=mfc.getMutualFundList();
		Iterator iterator=mfList.iterator();
		while(iterator.hasNext())
		{
			Object object[]=(Object[])iterator.next();
			Long mutualFundId=(Long)object[0];
			hSession=HibernateBridge.getSessionFactory().openSession();
			Date lastExit5Date=(Date)hSession.createQuery("SELECT max(tradeDate) from CustomMFExitAlert where mutualFundId=? and strategyName='ExitStrategy5'").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
			if(lastExit5Date==null)
			{
				lastExit5Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=0 and period=2 and tcfl1!=0").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
			}
			if(lastExit5Date==null)
			{
				continue;
			}
			
			List<Fractal> fractalList=hSession.createQuery("from Fractal where mutualFundId=? and period=? and candleType=? and tcfl1!=0 and tradeDate>=? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,2).setParameter(2,0).setParameter(3,lastExit5Date).list();
			transaction=hSession.beginTransaction();
			for(int i=3;i<fractalList.size();i++)
			{
				Fractal currentFractal=fractalList.get(i);
				double currentClosePrice=currentFractal.getClosePrice();
				Date currentTradeDate=currentFractal.getTradeDate();
				if(currentClosePrice<currentFractal.getTcfl1() && currentClosePrice<currentFractal.getTcfl2() && currentClosePrice<currentFractal.getTcfl3())
				{
					Fractal previousFractal=fractalList.get(i-1);
					double previousClosePrice=previousFractal.getClosePrice();
					if(previousClosePrice<previousFractal.getTcfl1() && previousClosePrice<previousFractal.getTcfl2() && previousClosePrice<previousFractal.getTcfl3())
					{
						
						Fractal pPreviousFractal=fractalList.get(i-2);
						double pPreviousClosePrice=pPreviousFractal.getClosePrice();
						double minTCFL=Math.min(Math.min(pPreviousFractal.getTcfl1(),pPreviousFractal.getTcfl2()),pPreviousFractal.getTcfl3());
						if(pPreviousFractal.getTcfl1()!=0 && pPreviousClosePrice>minTCFL)
						{
							Fractal currentPeriod1Fractal=(Fractal)hSession.createQuery("from Fractal where mutualFundId=? and candleType=? and period=? and tradeDate=?").setParameter(0,mutualFundId).setParameter(1,0).setParameter(2,1).setParameter(3,currentTradeDate).setMaxResults(1).uniqueResult();
							if(currentPeriod1Fractal.getClosePrice()<currentPeriod1Fractal.getLowFractal())
							{
								boolean flatPatternSSBFound=false;
								Calendar calendar=Calendar.getInstance();
								calendar.setTime(currentTradeDate);
								calendar.add(Calendar.MONTH,-3);
								Date previousThreeMonthDate=calendar.getTime();
								List flatPatternBeforeTradeDateSsb=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate").setParameter(0,"SSB").setParameter(1,mutualFundId).setParameter(2,15).setParameter(3, 0).setParameter(4,currentTradeDate).setMaxResults(1).list();
								if(flatPatternBeforeTradeDateSsb.size()!=0)
								{
									FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateSsb.get(0);
									Date lastFlatPatternDate=flatPattern.getPatternEndDate();
									
									
									if(lastFlatPatternDate.compareTo(previousThreeMonthDate)<0)
									{
										flatPatternSSBFound=true;
									//	System.out.println("pattern end date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
									}
									else
									{
										double ssb=flatPattern.getPatternValue();
										if(ssb>(currentClosePrice*1.03))
										{
											//System.out.println("ssb true "+mutualFundId+"tradeDate="+tradeDate);
											//System.out.println("pattern date found(Nav checked) alert for mf="+mutualFundId+" for "+tradeDate+"  lastFlatDate="+lastFlatPatternDate);
											flatPatternSSBFound=true;
										}
										else
										{
											//System.out.print("ssb pattern date found ( nav not verified)");
											flatPatternSSBFound=false;
											continue;
										}
									}
								}
								else
								{
									//flatPatternSSBFound=false;
									//System.out.println("No pattern date found alert for mf="+mutualFundId+" for "+tradeDate);
									flatPatternSSBFound=true;
								}
								if(flatPatternSSBFound==true)
								{
									System.out.println("Exit 5 alert for ="+mutualFundId+"  on ="+currentTradeDate);
									CustomMFExitAlert customMFExitAlert = new CustomMFExitAlert();
	                                customMFExitAlert.setMutualFundId(mutualFundId);
	                                customMFExitAlert.setStrategyName(strategyName);
	                                customMFExitAlert.setTradeDate(currentTradeDate);
	                                customMFExitAlert.setReasonForAlert(1);
	                                customMFExitAlert.setExitPrice(currentClosePrice);
	                              	hSession.saveOrUpdate(customMFExitAlert);
								}
							}
						}
					}
				}
			}
			transaction.commit();		
		}
		out.write("Finished Processing.");
		  BufferedWriter bw=null;
          try
          {
        	  File f=new File("/opt/strategy");
          	if(!f.isDirectory())
          	{
          		f.mkdirs();
          	}
          	
          	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/exit.txt"),true));
          	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
          	bw.write(entry);
          	bw.newLine();
          }
          catch(Exception e)
          {
          	e.printStackTrace();
          }
          finally
          {
          	bw.close();
          }
	}
	catch(Exception e)
	{
		System.out.println("Exception Occurred");
		e.printStackTrace();
		e.printStackTrace(response.getWriter());
		if(transaction!=null)
			transaction.rollback();
	}
	finally
	{
		hSession.close();
	}

%>
</body>
</html>