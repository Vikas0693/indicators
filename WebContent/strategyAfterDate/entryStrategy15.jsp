<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="com.fundexpert.dao.CustomMFExitAlert" %>
<%@ page import="com.fundexpert.dao.Nav" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Exit Strategy 15</title>
</head>
<body>
	<% 
	Session hSession=null;
	Transaction transaction=null;
	try {
			String strategyName="EntryStrategy15";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			int count=0;
			hSession=HibernateBridge.getSessionFactory().openSession();
			while(iterator.hasNext())
			{				
				transaction=hSession.beginTransaction();
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				Date previousAlertDate=null;
				Date lastEntry15Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy15'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
				List exit1Alert=null;
				if(lastEntry15Date==null)
				{
					exit1Alert=hSession.createQuery("from CustomMFExitAlert where strategyName='ExitStrategy1' and mutualFundId=?").setParameter(0,mutualFundId).list();
				}
				else
				{
					exit1Alert=hSession.createQuery("from CustomMFExitAlert where strategyName='ExitStrategy1' and mutualFundId=? and tradeDate>=?").setParameter(0,mutualFundId).setParameter(1,lastEntry15Date).list();
				}
				
				
				for(int i=0;i<exit1Alert.size();i++)
				{
					CustomMFExitAlert alert1=(CustomMFExitAlert)exit1Alert.get(i);
					double exit1Price=alert1.getExitPrice();
					Date alert1Date=alert1.getTradeDate();
					if(previousAlertDate!=null)
					{
						if(alert1Date.compareTo(previousAlertDate)<0)
							continue;
					}
					Calendar cal=Calendar.getInstance();
					cal.setTime(alert1Date);
					cal.add(Calendar.MONTH,4);
					
					List navFor4Months=hSession.createQuery("from Nav where mutualfundId=? and tradeDate between ? and ? order by tradeDate").setParameter(0,mutualFundId).setParameter(1,alert1Date).setParameter(2,cal.getTime()).list();
					if(navFor4Months!=null)
					{
						for(int j=0;j<navFor4Months.size();j++)
						{
							Nav nav=(Nav)navFor4Months.get(j);
							if(nav.getNav()>exit1Price*1.03)
							{
								
								System.out.println("	EntryStrategy15	"+mutualFundId+"\t"+new SimpleDateFormat("yyyy-MM-dd").format(nav.getTradeDate())+"\t1\t"+nav.getNav());
								CustomMFEntryAlert customMFEntryAlert = new CustomMFEntryAlert();
                                customMFEntryAlert.setMutualFundId(mutualFundId);
                                customMFEntryAlert.setStrategyName(strategyName);
                                customMFEntryAlert.setTradeDate(nav.getTradeDate());
                                customMFEntryAlert.setReasonForAlert(1);
                                customMFEntryAlert.setEntryPrice(nav.getNav());
                               	hSession.saveOrUpdate(customMFEntryAlert);
                               	previousAlertDate=nav.getTradeDate();
                               	break;
							}
						}
					}
				}				
				transaction.commit();
			}
			out.print("Finished processing all data.<br>");
            out.flush();
            BufferedWriter bw=null;
            try
            {
            	File f=new File("/opt/strategy");
            	if(!f.isDirectory())
            	{
            		f.mkdirs();
            	}
            	
            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            	bw.write(entry);
            	bw.newLine();
            }
            catch(Exception e)
            {
            	e.printStackTrace();
            }
            finally
            {
            	bw.close();
            }
        } 
		catch (Exception e)
		{
			e.printStackTrace();
           e.printStackTrace(response.getWriter());
           if(transaction!=null)
        	   transaction.rollback();
        }
		finally
		{
			hSession.close();
		}
	%>
	
</body>
</html>