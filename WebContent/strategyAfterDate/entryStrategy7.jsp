<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.strategy.controller.HibernateConnection" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Entry 7</title>
</head>
<body>
	<% 
	Session hSession=null;
	Transaction tx=null;
	try {
			String strategyName="EntryStrategy7";
		
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			int count=0;
			hSession=HibernateBridge.getSessionFactory().openSession();
			while(iterator.hasNext())
			{				
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				
				System.out.println("mf="+mutualFundId);
				Date lastEntry7Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy7'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
				if(lastEntry7Date==null)
				{
					lastEntry7Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=0 and period=2").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				}
				if(lastEntry7Date==null)
					continue;
				List<Fractal> dailyCandleList = hSession.createQuery("from Fractal where mutualFundId=? and candleType=0 and period=2 and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,lastEntry7Date).list();
				
				tx=hSession.beginTransaction();
				for(int i=1;i<dailyCandleList.size();i++)
				{
					Fractal currentFractal=(Fractal)dailyCandleList.get(i);
					Date currentTradeDate=currentFractal.getTradeDate();
					double closePrice=currentFractal.getClosePrice();
					double upFractal=currentFractal.getUpFractal();
					Double tcfl1=currentFractal.getTcfl1();
					Double tcfl2=currentFractal.getTcfl2();
					Double tcfl3=currentFractal.getTcfl3();
					
					if(tcfl1!=null  && closePrice<tcfl1 && closePrice<tcfl2 && closePrice<tcfl3)
					{
						List previous2DaysPeriod1List=hSession.createQuery("from Fractal where mutualFundId=? and period=? and candleType=? and tradeDate<=? order by tradeDate desc").setParameter(0,mutualFundId).setParameter(1,1).setParameter(2,0).setParameter(3, currentTradeDate).setMaxResults(2).list();
						Fractal currentFractalWithPeriod1=(Fractal)previous2DaysPeriod1List.get(0);
						Fractal previousFractalWithPeriod1=(Fractal)previous2DaysPeriod1List.get(1);
						
						if(currentFractalWithPeriod1.getTradeDate().compareTo(currentTradeDate)!=0)
						{
							throw new Exception("currentTradeDate does not exist in fractal of period 1 for="+mutualFundId+" currentTradeDate"+currentTradeDate);
						}
						if(currentFractalWithPeriod1.getClosePrice()>currentFractalWithPeriod1.getUpFractal() &&  previousFractalWithPeriod1.getClosePrice()<previousFractalWithPeriod1.getUpFractal())
						{
							CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
							customMFEntryAlert.setMutualFundId(mutualFundId);
							customMFEntryAlert.setStrategyName(strategyName);
							customMFEntryAlert.setTradeDate(currentTradeDate);
							customMFEntryAlert.setEntryPrice(closePrice);
							customMFEntryAlert.setReasonForAlert(1);
							hSession.saveOrUpdate(customMFEntryAlert);
							//HibernateConnection.saveOrUpdate(customMFEntryAlert);
							System.out.println("Entry 7 alert "+mutualFundId+" \t\t "+currentFractal.getTradeDate());
						}
					}	
				}	
				tx.commit();
            }
            out.print("Finished processing all data.<br>");
            out.flush();
            BufferedWriter bw=null;
            try
            {
            	File f=new File("/opt/strategy");
            	if(!f.isDirectory())
            	{
            		f.mkdirs();
            	}
            	
            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            	bw.write(entry);
            	bw.newLine();
            }
            catch(Exception e)
            {
            	e.printStackTrace();
            }
            finally
            {
            	bw.close();
            }
        } 
		catch (Exception e)
		{
			e.printStackTrace();
           	e.printStackTrace(response.getWriter());
           	if(tx!=null)
        		tx.rollback();
        }
		finally
		{
			hSession.close();
		}
		
	%>
	
</body>
</html>