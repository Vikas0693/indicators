<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.apidoc.util.HibernateBridge"%>
<%@ page import="java.util.List" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="com.fundexpert.dao.CustomMFEntryAlert" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.dao.Fractal" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.strategy.controller.HibernateConnection" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Entry 6</title>
</head>
<body>
	<% 
	Session hSession=null;
	Transaction tx=null;
	try {
			String strategyName="EntryStrategy6";
		
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			int count=0;
			hSession=HibernateBridge.getSessionFactory().openSession();
			while(iterator.hasNext())
			{				
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				System.out.println("mf="+mutualFundId);
				Date lastEntry6Date=(Date)hSession.createQuery("select max(tradeDate) from CustomMFEntryAlert where mutualFundId=? and strategyName='EntryStrategy6'").setParameter(0, mutualFundId).setMaxResults(1).uniqueResult();
				if(lastEntry6Date==null)
				{
					lastEntry6Date=(Date)hSession.createQuery("select min(tradeDate) from Fractal where mutualFundId=? and candleType=1 and period=2").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				}
				if(lastEntry6Date==null)
					continue;
				List<Fractal> dailyCandleList = hSession.createQuery("from Fractal where mutualFundId=? and candleType=1 and period=2 and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,lastEntry6Date).list();
				
				tx=hSession.beginTransaction();
				for(int i=1;i<dailyCandleList.size();i++)
				{
					Fractal currentFractal=(Fractal)dailyCandleList.get(i);
					double closePrice=currentFractal.getClosePrice();
					double upFractal=currentFractal.getUpFractal();
					Date tradeDate=currentFractal.getTradeDate();
					
					Double currentTcfl1=currentFractal.getTcfl1();
					Double currentTcfl2=currentFractal.getTcfl2();
					Double currentTcfl3=currentFractal.getTcfl3(); 
					
					boolean flatPatternSSBFound=false;
					boolean flatPatternTCFL1Found=false;
					boolean flatPatternTCFL3Found=false;
					//System.out.println("0 condition="+tradeDate+"mf="+mutualFundId);
					if(currentTcfl1!=null  && closePrice<currentTcfl1 && closePrice<currentTcfl2 && closePrice<currentTcfl3 && closePrice>upFractal)
					{
						Fractal previousFractal=dailyCandleList.get(i-1);
						if(previousFractal.getClosePrice()<previousFractal.getUpFractal())
						{
							//System.out.println("Second condition="+tradeDate+"mf="+mutualFundId);
							Calendar calendar=Calendar.getInstance();
							calendar.setTime(tradeDate);
							calendar.add(Calendar.MONTH,-4);
							Date previousFourMonthDate=calendar.getTime(); 
							
							List flatPatternBeforeTradeDateSsb=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"SSB").setParameter(1,mutualFundId).setParameter(2,20).setParameter(3, 1).setParameter(4,tradeDate).setMaxResults(1).list();
							
							if(flatPatternBeforeTradeDateSsb.size()!=0)
							{
								FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateSsb.get(0);
								Date lastFlatPatternDate=flatPattern.getPatternStartDate();
															
								if(lastFlatPatternDate.compareTo(previousFourMonthDate)<0)
								{
									flatPatternSSBFound=true;
								}
								else
								{
									double ssb=flatPattern.getPatternValue();
									if(ssb>(closePrice*1.05))
									{
										//System.out.println("ssb true "+mutualFundId+"tradeDate="+tradeDate);
										//System.out.println("pattern date found(Nav checked) alert for mf="+mutualFundId+" for "+tradeDate+"  lastFlatDate="+lastFlatPatternDate);
										flatPatternSSBFound=true;
									}
									else
									{
										//System.out.print("ssb pattern date found ( nav not verified)");
										flatPatternSSBFound=false;
										continue;
									}
								}
							}
							else
							{
								//flatPatternSSBFound=false;
								//System.out.println("No pattern date found alert for mf="+mutualFundId+" for "+tradeDate);
								flatPatternSSBFound=true;
							}
							List flatPatternBeforeTradeDateTcfl1=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"TCFL1").setParameter(1,mutualFundId).setParameter(2,15).setParameter(3, 1).setParameter(4,tradeDate).setMaxResults(1).list();
							if(flatPatternSSBFound==true)
							{
								if(flatPatternBeforeTradeDateTcfl1.size()!=0)
								{
									FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateTcfl1.get(0);
									Date lastFlatPatternDate=flatPattern.getPatternStartDate();
									
									if(lastFlatPatternDate.compareTo(previousFourMonthDate)<0)
									{
										flatPatternTCFL1Found=true;
										//System.out.println("pattern end date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
									}
									else
									{
										double tcfl1=flatPattern.getPatternValue();
										if(tcfl1>(closePrice*1.05))
										{
											//System.out.println("tcfl1 true "+mutualFundId+"tradeDate="+tradeDate);
											flatPatternTCFL1Found=true;
										}
										else
										{
											flatPatternTCFL1Found=false;
											continue;
										}
									}	
								}
								else
								{
									flatPatternTCFL1Found=true;
								}
							}
							else
							{
								continue;
							}
							List flatPatternBeforeTradeDateTcfl3=hSession.createQuery("from FlatPatterns where patternName=? and mutualFundId=? and flatDays=? and candleType=? and patternStartDate<=? order by patternStartDate desc").setParameter(0,"TCFL3").setParameter(1,mutualFundId).setParameter(2,15).setParameter(3,1).setParameter(4,tradeDate).setMaxResults(1).list();
							if(flatPatternSSBFound==true && flatPatternTCFL1Found==true)
							{
								if(flatPatternBeforeTradeDateTcfl3.size()!=0)
								{
									FlatPatterns flatPattern=(FlatPatterns)flatPatternBeforeTradeDateTcfl3.get(0);
									Date lastFlatPatternDate=flatPattern.getPatternStartDate();
									
									if(lastFlatPatternDate.compareTo(previousFourMonthDate)<0)
									{
										flatPatternTCFL3Found=true;
										//System.out.println("tcfl 3 pattern end date found beyond 3 months="+tradeDate+ " lastpatternDate="+lastFlatPatternDate);
									}
									else
									{
										double tcfl3=flatPattern.getPatternValue();
										if(tcfl3>(closePrice*1.05))
										{
											//System.out.println("tcfl3 true "+mutualFundId+"tradeDate="+tradeDate);
											flatPatternTCFL3Found=true;
										}
										else
										{
											flatPatternTCFL3Found=false;
											continue;
										}
									}
								}
								else
								{
									//System.out.print("tcfl 3 no pattern date found  tradeDate="+tradeDate);
									flatPatternTCFL3Found=true;
								}
							}
							else
							{								
								continue;
							} 								
							if(flatPatternSSBFound==true && flatPatternTCFL1Found==true && flatPatternTCFL3Found==true)
							{
								System.out.println("Entry 6 Alert="+mutualFundId+" tradeDate="+tradeDate);
								CustomMFEntryAlert customMFEntryAlert=new CustomMFEntryAlert();
								customMFEntryAlert.setMutualFundId(mutualFundId);
								customMFEntryAlert.setStrategyName(strategyName);
								customMFEntryAlert.setTradeDate(tradeDate);
								customMFEntryAlert.setEntryPrice(closePrice);
								customMFEntryAlert.setReasonForAlert(1);
								hSession.saveOrUpdate(customMFEntryAlert);
								//HibernateConnection.saveOrUpdate(customMFEntryAlert);
								//System.out.println(mutualFundId+" \t\t "+currentFractal.getTradeDate());
							}							
						}
					}	
				}	
				tx.commit();
            }
            out.print("Finished processing all data.<br>");
            out.flush();
            BufferedWriter bw=null;
            try
            {
            	File f=new File("/opt/strategy");
            	if(!f.isDirectory())
            	{
            		f.mkdirs();
            	}
            	
            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/entry.txt"),true));
            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            	bw.write(entry);
            	bw.newLine();
            }
            catch(Exception e)
            {
            	e.printStackTrace();
            }
            finally
            {
            	bw.close();
            }
        } 
		catch (Exception e)
		{
			e.printStackTrace();
           e.printStackTrace(response.getWriter());
           if(tx!=null)
        	   tx.rollback();
        }
		finally
		{
			hSession.close();
		}		
	%>	
</body>
</html>