<%@page import="com.apidoc.util.HibernateBridge"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ page import="com.fundexpert.dao.CustomMFExitAlert" %>
<%@ page import="com.fundexpert.dao.EveryDayCandle" %>
<%@ page import="java.util.Date" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="org.hibernate.Transaction" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="com.fundexpert.controller.MutualFundController" %>
<%@ page import="com.fundexpert.strategy.controller.HibernateConnection" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Exit Strategy 1</title>
</head>
<body>
	<%
	Session hSession=null;
	Transaction tx=null;
	try {
			String strategyName="ExitStrategy1";
			MutualFundController mfc=new MutualFundController();
			List<Object[]> mfList=mfc.getMutualFundList();
			Iterator iterator=mfList.iterator();
			int count=0;
			hSession=HibernateBridge.getSessionFactory().openSession();
			while(iterator.hasNext())
			{
				
				tx=hSession.beginTransaction();
				count++;
				Object object[]=(Object[])iterator.next();
				Long mutualFundId=(Long)object[0];
				
				Date lastExit1Date=(Date)hSession.createQuery("SELECT max(tradeDate) from CustomMFExitAlert where mutualFundId=? and strategyName='ExitStrategy1'").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				if(lastExit1Date==null)
				{
					lastExit1Date=(Date)hSession.createQuery("select min(tradeDate) from EveryDayCandle where mutualFundId=? and candleType=0").setParameter(0,mutualFundId).setMaxResults(1).uniqueResult();
				}
				if(lastExit1Date==null)
				{
					continue;
				}
				
				
				
				List<EveryDayCandle> weeklyCandleList=hSession.createQuery("from EveryDayCandle where mutualFundId=? and candleType=? and tradeDate>=? order by tradeDate").setParameter(0, mutualFundId).setParameter(1,0).setParameter(2,lastExit1Date).list();
				// This you need to write on your own, i need Weekly RSI 70 value for the symbol
				// CustomMFExitAlert is the object in which we are persisting Exit dates for different exit startegy 
	            boolean watchActivatedCond1Met = false; // we have to persist this when this code moves to DataController
	            int countWatchCond1 = 0;
	            int countWatchTotalCond1 = 0;
	            boolean watchActivatedCond2 = false;
	
	            int countWatchTotalCond2 = 0;
				
	            if (weeklyCandleList != null && weeklyCandleList.size() != 0) {
	                for (EveryDayCandle weeklyCandle : weeklyCandleList) {
	                	//System.out.println("Date="+weeklyCandle.getTradeDate());
	                    if (watchActivatedCond2)  // Checking for final condition
	                    {
	                        if (weeklyCandle.getRsi() < 70) {
	                            countWatchTotalCond2++;
	                            
	                            if (countWatchTotalCond2 >= 4) {
	                                watchActivatedCond2 = false; //restting again for next check
	                                watchActivatedCond1Met = false;//restting again for next check
	                                // EXIT CONDIITON MET, TO Populate Database
	                                //strategyName - "ExitStrategy1"
	                                // symbol - symbol
	                                // tradeDate - weeklyCandleList.get(index).getTradDate()
	                                //reasonForAlert - "as per ExitStrategy1"
	                                // exitPrice  -  weeklyCandleList.get(index).getClosePrice()
	                                //nftyPrice - for Nifty same as weeklyCandleList.get(index).getClosePrice()
	                                Date currentDate=weeklyCandle.getTradeDate();
	                                Calendar cal=Calendar.getInstance();
	                                cal.setTime(currentDate);
	                                cal.add(Calendar.MONTH,-4);
	                                List exit1Within4Month=hSession.createQuery("from CustomMFExitAlert where strategyName = 'ExitStrategy1' and mutualFundId=? and tradeDate between ? and ?").setParameter(0,mutualFundId).setParameter(1,cal.getTime()).setParameter(2,currentDate).list();
	                                if(exit1Within4Month.isEmpty())
	                               	{
	                               		System.out.println("Found Exit 1 Alert for mfid="+mutualFundId+" on date="+currentDate);
		                                CustomMFExitAlert customMFExitAlert = new CustomMFExitAlert();
		                                customMFExitAlert.setMutualFundId(mutualFundId);
		                                customMFExitAlert.setStrategyName(strategyName);
		                                customMFExitAlert.setTradeDate(currentDate);
		                                customMFExitAlert.setReasonForAlert(1);
		                                customMFExitAlert.setExitPrice(weeklyCandle.getTodaysClosePrice());
		                             	hSession.saveOrUpdate(customMFExitAlert);
		                                //customMFExitAlert.setNiftyEntryPrice(weeklyCandle.getClosePrice());
		                                //out.print("Fount exit 1 alert [" + weeklyCandle.getTradeDate() + "] .<br>");
		                                out.flush();
	                               	}	
	                               	
	                            }
	                        } else {
	                            // Second COunter to be reset
	                            countWatchTotalCond2 = 0;
	                        }
	                    }
	
	                    if (!watchActivatedCond1Met && weeklyCandle.getRsi() > 70) {
	                        countWatchCond1++;
	                        if (countWatchCond1 == 1) {
	                            countWatchTotalCond1 = 0;
	                        } else if (countWatchCond1 >= 3) {
	                            watchActivatedCond2 = true;
	                            watchActivatedCond1Met = true;
	                            countWatchTotalCond1 = 0;
	                            countWatchCond1 = 0;
	                        }
	                    }
	
	                    if (!watchActivatedCond1Met)   // Not 70
	                    {
	                        countWatchTotalCond1++;
	                    }
	
	                    if (!watchActivatedCond1Met && countWatchTotalCond1 >= 4) {
	                        watchActivatedCond1Met = false;
	                        countWatchCond1 = 0;
	                        countWatchTotalCond1 = 0;
	                    }
	
	                }
	            }
	            tx.commit();
            }			
            out.print("Finished processing all data.<br>");
            out.flush();
           
            BufferedWriter bw=null;
            try
            {
            	File f=new File("/opt/strategy");
            	if(!f.isDirectory())
            	{
            		f.mkdirs();
            	}
            	
            	bw=new BufferedWriter(new FileWriter(new File(f.getAbsolutePath()+"/exit.txt"),true));
            	String entry=strategyName+" last updated successfully on "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            	bw.write(entry);
            	bw.newLine();
            }
            catch(Exception e)
            {
            	e.printStackTrace();
            }
            finally
            {
            	bw.close();
            }
        } 
		catch (Exception e)
		{
           e.printStackTrace(response.getWriter());
           e.printStackTrace();
           if(tx!=null)
        	   tx.rollback();
        }
		finally
		{
			hSession.close();
		}
	
	%>
	
</body>
</html>