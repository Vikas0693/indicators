<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.fundexpert.controller.FractalIndicatorController" %>
<%@ page import="com.fundexpert.controller.TALibController" %>
<%@ page import="com.fundexpert.controller.IchimokuController" %>
<%@ page import="com.fundexpert.controller.EveryDayCandleController" %>
<%@ page import="com.fundexpert.test.HibernateTest" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONArray" %>
<%@ page import="com.fundexpert.dao.FlatPatterns" %>
<%@ page import="com.fundexpert.controller.FlatPatternsController" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Indicators</title>
<script type="text/javascript" src="jquery-3.2.1.js"></script>
</head>
<body>
	<h1>Update Indicators</h1>

	<%
	String submit=request.getParameter("submit");
	if(submit==null)
	{
	%>
		<form action="update-indicators.jsp" name="updateFractal">
			Indicators :
			<select name="indicator">
				<option name="fractal" selected="selected">Fractal</option>
				<option name="ichimoku">Ichimoku</option>
				<option name="talib">Talib</option>
			</select>
			
			<br>
			<!-- Current :<input type="radio" name="indicatorstogenerate" value="current"/>
			<br>
			Everyday :<input type="radio" name="indicatorstogenerate" value="everyday"/>
			<br> -->
			Datewise :<input type="radio" name="indicatorstogenerate" value="datewise"/>
			<br>
			<input type="submit" id="submit" value="Submit" name="submit"/>	
		</form>
		<a target="_blank" href="generateFlatPattern.jsp">Generate Flat Patterns</a>
	<%
	} 
	else
	{
		String indicator=request.getParameter("indicator");
		String indicatorsToGenerate=request.getParameter("indicatorstogenerate");
		JSONObject jsonObject=new JSONObject();
		if(indicatorsToGenerate!=null)
		{
			try
			{
				/*if(candleType==null)
				{
					throw new Exception("candleType not selected(Weekly,Daily)");
				} */
				if(indicator.equalsIgnoreCase("fractal"))
				{
					FractalIndicatorController fic1=new FractalIndicatorController();	
					if(indicatorsToGenerate.equalsIgnoreCase("current"))
					{
					}
					else if(indicatorsToGenerate.equalsIgnoreCase("everyday"))
					{
						/* Thread t=new Thread(new Runnable()
						{
							public void run()
							{
								try
								{
									FractalIndicatorController fic=new FractalIndicatorController();	
									fic.updateSwing();
								}
								catch(Exception e)
								{
									e.printStackTrace();
								}
							}
						});
						System.out.print("Thread t start(daily  update swing after date)");
						
						Thread t1=new Thread(new Runnable()
						{
							public void run()
							{
								try
								{							
									FractalIndicatorController fic=new FractalIndicatorController();	
									fic.updateWeeklySwing();
								}
								catch(Exception e)
								{
									e.printStackTrace();
								}
							}
						});
						System.out.print("Thread t1 start(Weekly update swing after date)");
						*/
						
						
					}
					else if(indicatorsToGenerate.equalsIgnoreCase("datewise"))
					{	
						//below 2 instructions generate fractal,upFractal,lowFractal
						fic1.updateFractalAfterDate();
						fic1.updateWeeklyFractalAfterDate();
						//below 2 instructions generate swingHigh,swingLow,tcfl's
						fic1.updateSwingAfterDate();
						fic1.updateWeeklySwingAfterDate();						
						//below two instructions generate fractal(only upFractal and lowFractal) with period=1  
						fic1.updateWeeklyFractalPeriod1AfterDate();
						fic1.updateWeeklyUpLowFractalPeriod1AfterDate();
					}
				}
				else if(indicator.equalsIgnoreCase("ichimoku"))
				{
					IchimokuController ic=new IchimokuController();
					
					if(indicatorsToGenerate.equalsIgnoreCase("current"))
					{
					}
					else if(indicatorsToGenerate.equalsIgnoreCase("everyday"))
					{
					}
					else if(indicatorsToGenerate.equalsIgnoreCase("datewise"))
					{						
							ic.updateIchimokuAfterDate();
							ic.updateWeeklyIchimokuAfterDate();					
					}
				}
				else if(indicator.equalsIgnoreCase("talib"))
				{
					
					TALibController tALibController=new TALibController();
					if(indicatorsToGenerate.equalsIgnoreCase("current"))
					{
						
					}
					else if(indicatorsToGenerate.equalsIgnoreCase("everyday"))
					{
						
					}
					else if(indicatorsToGenerate.equalsIgnoreCase("datewise"))
					{						
						tALibController.updateEveryDayAfterDate();
						tALibController.updateWeeklyEveryDayAfterDate();
					}
				}
				else
				{
					throw new Exception("select indicator to be updated(Fractal,Ichimoku,Talib)");
				}
				jsonObject.put("success","true");
				jsonObject.put("error","");
				out.print(jsonObject);
			}
			catch(Exception e)
			{			
				jsonObject.put("success","false");
				jsonObject.put("error",e.getMessage());
				out.println(jsonObject.toString(1));
				e.printStackTrace();
			}
		}		
	}
	%>
</body>
<script>
$(document).ready(function()
{
	$("#submit").click(function(e)
	{	
		console.log("submit pressed");
		var candleType=$("input[name=candleType]:checked").val();
		var indicatorstogenerate=$("input[name=indicatorstogenerate]:checked").val();
		console.log("ct  "+candleType+"  indicatorstogenerate"+indicatorstogenerate);
		if(typeof indicatorstogenerate==="undefined")
		{
			alert("select one of (Datewise)");
			e.preventDefault();
		}		
	});	
});
</script>
</html>
