<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.fundexpert.test.HibernateTest" %>
<%@ page import="com.stocks.controller.FractalStocksController" %>
<%@ page import="com.stocks.controller.IchimokuStocksController" %>
<%@ page import="com.stocks.controller.TaLibStocksController" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONArray" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Stocks Indicators</title>
<script type="text/javascript" src="../jquery-3.2.1.js"></script>
</head>
<body>
	<h1>Update Stocks Indicators</h1>

	<%
	String submit=request.getParameter("submit");
	if(submit==null)
	{
	%>
		<form action="update-stocks-indicators.jsp" name="updateFractal">
			Indicators :
			<select name="indicator">
				<option name="fractal" selected="selected">Fractal</option>
				<option name="ichimoku">Ichimoku</option>
				<option name="talib">Talib</option>
			</select>
			
			<br>
			
			Datewise :<input type="radio" name="indicatorstogenerate" value="datewise"/>
			<br>
			<input type="submit" id="submit" value="Submit" name="submit"/>	
		</form>
	<%
	} 
	else
	{
		String indicator=request.getParameter("indicator");
		String indicatorsToGenerate=request.getParameter("indicatorstogenerate");
		JSONObject jsonObject=new JSONObject();
		if(indicatorsToGenerate!=null)
		{
			try
			{
				
				if(indicator.equalsIgnoreCase("fractal"))
				{
					FractalStocksController fic1=new FractalStocksController();	
					if(indicatorsToGenerate.equalsIgnoreCase("datewise"))
					{	
						//below 2 instructions generate fractal,upFractal,lowFractal
						fic1.updateDailyFractalStocksAfterDate();
						fic1.updateWeeklyFractalStocksAfterDate();
						//below 2 instructions generate swingHigh,swingLow,tcfl's
						fic1.updateDailySwingStocksAfterDate();
						fic1.updateWeeklySwingStocksAfterDate();						
						//below two instructions generate fractal(only upFractal and lowFractal) with period=1  
						fic1.updateWeeklyFractalStocksPeriod1AfterDate();
						fic1.updateWeeklyUpLowFractalStocksPeriod1AfterDate();
					}
				}
				else if(indicator.equalsIgnoreCase("ichimoku"))
				{
					IchimokuStocksController ic=new IchimokuStocksController();
					
					if(indicatorsToGenerate.equalsIgnoreCase("datewise"))
					{						
							ic.updateDailyIchimokuStocksAfterDate();
							ic.updateWeeklyIchimokuStocksAfterDate();					
					}
				}
				else if(indicator.equalsIgnoreCase("talib"))
				{
					
					TaLibStocksController tALibController=new TaLibStocksController();
					
					if(indicatorsToGenerate.equalsIgnoreCase("datewise"))
					{						
						tALibController.updateDailyEveryDayStocksAfterDate();
						tALibController.updateWeeklyEveryDayAfterDate();
					}
				}
				else
				{
					throw new Exception("select indicator to be updated(Fractal,Ichimoku,Talib)");
				}
				jsonObject.put("success","true");
				jsonObject.put("error","");
				out.print(jsonObject);
			}
			catch(Exception e)
			{			
				jsonObject.put("success","false");
				jsonObject.put("error",e.getMessage());
				out.println(jsonObject.toString(1));
				e.printStackTrace();
			}
		}		
	}
	%>
</body>
<script>
$(document).ready(function()
{
	$("#submit").click(function(e)
	{	
		console.log("submit pressed");
		/* var candleType=$("input[name=candleType]:checked").val(); */
		var indicatorstogenerate=$("input[name=indicatorstogenerate]:checked").val();
		console.log("  indicatorstogenerate : "+indicatorstogenerate);
		if(typeof indicatorstogenerate==="undefined")
		{
			alert("select one of (Datewise)");
			e.preventDefault();
		}		
	});	
});
</script>
</html>
