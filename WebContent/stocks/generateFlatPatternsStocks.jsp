<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.stocks.controller.FlatPatternsStocksController" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
	FlatPatternsStocksController fpc=new FlatPatternsStocksController();
	
	try
	{
		//for daily candle of SSB use flatDays=20
		fpc.createLatestStocksSSBFlatPatterns(String.valueOf(20),String.valueOf(1));
		//for daily candle of tcfl1 use flatDays=15
		fpc.createLatestStocksTCFL1FlatPatterns(String.valueOf(15),String.valueOf(1));
		//for daily candle of tcfl3 use flatDays=15
		fpc.createLatestStocksTCFL3FlatPatterns(String.valueOf(15),String.valueOf(1));
		
		//for weekly candle of ssb candles use flatDays=15
		fpc.createLatestStocksSSBFlatPatterns(String.valueOf(15),String.valueOf(0));
		//for weekly candle of tcfl1 use flatDays=15 and period=2 in query
		fpc.createLatestStocksTCFL1FlatPatterns(String.valueOf(15),String.valueOf(0));
		//for weekly candle of tcfl3 use flatDays=15 and period=2 in query
		fpc.createLatestStocksTCFL3FlatPatterns(String.valueOf(15),String.valueOf(0));
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}		
%>
</body>
</html>